Test Clinic
- Ce que nous ont appris de deux ans de pratique 

Test clinic ?
Le principe est simple, il s'agit de coder des tests logiciels en une session et en collaboration : Une personne amène un bout de code qu'elle souhaite tester et tous les autres participants vont l'aider à développer des tests.

Pourquoi les test clinics ?
Pour développer les tests automatiques dans ma structure.

2 Phases
Début janvier 2022
Début mai 2023

Déroulement de la phase 1
Format sur une heure, une fois toutes les deux semaines

Enseignements de la phase 1
Le test automatique est une compétence distincte du développement ;
Un code non prévu pour être testé est compliqué à tester ;
Une heure, il est possible d'écrire au moins un test.

Phase 2
Phase 1 améliorait les compétences des participants mais ne permettait pas de capitaliser

Déroulement de la phase 2
Format sur une heure et demi, une fois par semaine avec compte-rendu