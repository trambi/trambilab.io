---
title: A propos
subtitle: Portait rapide de l'auteur de ce blog - moi :D
comments: false
---
![Portrait](portrait.jpg) 
Mon nom est Bertrand Madet et je suis passionné par la programmation.

J'ai eu la chance de travailler en tant que développeur depuis 2003. J'ai pu officier dans différents domaines (bancaire, militaire, industriel) avec différentes langages - du C au Python.

En ce moment, je travaille sur des sites web et j'apprends le TypeScript.


