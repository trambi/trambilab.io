---
title: "Découverte du format multipart-formdata"
date: "2023-09-20"
draft: false
description: "du texte et du binaire en même temps"
tags: ["front-end"]
author: "Bertrand Madet"
---

Cette semaine, je tenais à vous partager la découverte d'un format de fichier "multipart-formdata".

## À quoi sert ce format ?

Le format est conçu pour transporter le contenu d'un formulaire `HTML`. Les données sont donc le contenu des entrées de formulaire donc du texte pour la plupart des entrées - la plupart des champs `input` les champs `textarea` - et aussi du binaire pour les entrées de type fichier - `input` de type `file` :exploding_head: . On se retrouve donc avec un format pour gérer à la fois du texte et du binaire - enfin des binaires parce le type embarqué peut être de n'importe quel type : image au format JPEG, image au format PNG, document au format PDF, exécutable ...

## À quoi cela ressemble ?

La structure de ce type de fichier est simple :

- une entête générale ;
- un séparateur de champ.
- une succession :
  - d'entête de champ,
  - de contenu de champ,
  - de séparateur de champ.

La difficulté est que n'importe quelle séquence peut apparaitre dans les champs. Si la séquence du séparateur de champ apparaît dans un contenu de champ, le programme qui est chargé d'analyser le fichier va arrêter de considérer ce champ et s'attendre à avoir l'entête du champ et vraisemblablement s'arrêter en erreur.

![Contenu schématique d'un fichier multipart](./multipart-formdata.png)

La solution proposée par le format est le séparateur de champ est décrit dans l'entête générale. Le séparateur de champ est dynamique afin que soit  une séquence qui ne figure pas dans les contenus des champs.

Les entêtes sont au format texte donc facilement lisibles et le contenu des champs varient grandement. Voici un exemple de contenu possible de ce format.

```text
Content-type: multipart/data; boundary="--unseparateur"

--unseparateur
Content-Disposition: form-data; name="name"
Content-type: text/plain; charset=UTF-8
Bertrand Madet
--unseparateur
Content-Disposition: form-data; name="passion"
Content-type: text/plain; charset=UTF-8
L'ingénerie logicielle
--unseparateur
```

Ici on aurait le résultat de l'envoi d'un formulaire HTML contenant deux champs :

- `name` ayant pour valeur `Bertrand Madet`
- `passion` ayant pour valeur `L'ingénerie logicielle` :wink:.

## Conclusion

Après plusieurs année de développement, j'ai donc un découvert un format de fichier étonnant qui mélange texte et binaire. Ce format est malgré tout très commun et des outils comme Postman ou Django le gère très bien. C'est intéressant de constater que l'on découvre toujours des détails sur le développement et en particulier dans le domaine du développement web qui englobe un très large écosystème : protocoles réseaux, serveurs et équipements réseaux et navigateurs web.

J'espère que vous avez eu plaisir à découvrir ce format et n'hésitez pas à partager vos découvertes avec moi sur [X](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
