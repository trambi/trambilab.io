---
title: "Drag and drop en JavaScript"
date: "2021-05-24"
draft: false
description: "d'un fichier csv vers un tableau"
tags: ["javascript","drag'n'drop"]
author: "Bertrand Madet"
---

Après avoir implémenté une fonctionnalité de Drag'n'drop en JavaScript, je vous propose un résumé des subtilités que j'ai pu rencontrer.

Je voulais afficher un tableau en déposant un fichier csv dans un élément HTML.

:warning: L'interaction de glisser-déposer ne semble pas bien supportée par les navigateurs sur les téléphones portables et autres tablettes. C'est vrai qu'avec un écran tactile cette interaction est peut-être trop complexe.

## Mozilla Developper Network

[Mozilla Developer Network (https://developer.mozilla.org)](https://developer.mozilla.org) est un site de référence pour le développement web, il a l'avantage d'être complet en anglais :gb: et d'exister en français :fr:.

La [section sur le glisser-déposer (https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API)](https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API) est dense.

La plupart des exemples de la section concerne les glisser d'éléments HTML. Je me suis donc plus intéressé aux éléments pour le dépôt.

## Évènements de dépôt

Il y a deux évènements pour le dépôt :

- `dragover`;
- `drop`.

L'évènement `dragover` est activé sur un élément HTML quand un objet glissable le survole. La seule chose à faire est de désactiver le comportement par défaut du navigateur. Si vous ne le faites pas, le navigateur adoptera son comportement par défaut, pour le type de fichier déposé, en général, il téléchargera le fichier que vous venez de déposer.

L'évènement `drop` est activé sur un élément HTML quand on lâche la souris après avoir glissé un objet. C'est à cet évènement qu'il faudra prendre en compte ce qui déposé et gérer l'affichage.

Pour gérer le dépôt, il suffit d'ajouter une fonction de gestion `ondragover` et une autre fonction `ondrop`.

```javascript
// Fonction à associer à ondragover
function handleDragOver(event){
  event.preventDefault();
}

// Fonction à associer à ondrop
function handleDrop(event){
  // Votre traitement
  // ici

  event.preventDefault();
}
```

## Accéder au contenu d'un fichier déposé

```mermaid
graph LR
event-->dataTransfer[event.dataTransfer]
dataTransfer-->files[dataTransfer.files]
dataTransfer-->types[dataTransfer.types]
files-->file1
files-->file2
files-->filen
types-->type1[type 1]
types-->type2[type 2]
types-->typen[type n]
file1-->content1["file1.text()"]
file2-->content2["file2.text()"]
filen-->contentn["filen.text()"]

```

Une fois le fichier déposé, on va utiliser l'objet `DataTransfer` (**sans "t"**) qui est récupérable par la propriété `dataTransfer` de l'évènement de `drop`.

L'objet DataTransfer permet d'obtenir la liste des fichiers avec la propriété `files` et la liste des types avec la propriété `types`. La liste des types est un tableau de chaînes de caractères.

:warning: Si vous filtrez sur le type, pour le même fichier csv j'ai eu `text/csv` sous Linux et sous Windows (avec Excel installé) `application/vnd.ms-excel`.

La liste des fichiers est un tableau d'objet de type `File` dont on peut lire le contenu d'un coup avec la méthode `text()` (qui renvoit une promesse de chaîne de caractère) ou en tant que flux avec la méthode `stream()`.

```javascript
// Fonction à associer à ondragover
function handleDragOver(event){
  event.preventDefault();
}

// Fonction à associer à ondrop
function handleDrop(event){
  const dataTransfer = event.dataTransfer
  for (let i = 0; i< dataTransfer.files.length; i += 1){
    if (dataTransfer.types[i] === 'text/csv' || dataTransfer.types[i] === 'application/vnd.ns-excel' ){
      dataTransfer.files[i].text().then(data => doSomething(data);)
    }
  }
  event.preventDefault();
}
```

## Et après ?

### Extraction des données CSV

J'ai fait mon extraction de données CSV en utilisant `split`, `map` et de `trim`.

Pour une extraction plus complète, il semble que l'on puisse utiliser des paquets npm comme [`papaparse` (https://www.papaparse.com/)](https://www.papaparse.com/). Une bibliothèque sans dépendance et sous licence MIT.

### Affichage

Il ne reste plus qu'à afficher. J'ai testé en JavaScript :

- d'écrire directement les éléments HTML ;
- d'utiliser la bibliothèque [DataTables (https://www.datatables.net/)](https://www.datatables.net/) ;
- d'utiliser la bibliothèque pour React - [react-table (https://react-table.tanstack.com/)](https://react-table.tanstack.com/).

DataTables et react-table permettent de trier sur les colonnes.

## Conclusion

Hormis le départ sur les évènements à gérer (`dragover` **et** `drop`) et le chemin de méthode et membre pour accéder au contenu du fichier, il est assez simple d'implémenter un dépôt de fichier CSV dans un élément HTML. J'espère que cela vous permettra de gagner du temps, si vous devez implémenter ce genre de fonctionnalité.

Si vous avez des retours sur ce billet, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
