---
title: "Point d'étape sur les coding dojos, les cliniques du test"
date: "2024-03-04"
draft: false
description: "et la suite"
tags: ["test","Software Craftmanship","dojo","tdd"]
author: "Bertrand Madet"
---

Depuis plus de cinq ans, nous organisons des Coding dojos dans les entités où je travaille. Pour augmenter la culture des tests logiciels, nous avons introduit il y a deux ans, les clinics du test. L'objectif global est d'améliorer la qualité du code que nous délivrons. Je vous propose un point d'étape dans cette approche.

## Coding dojos

Le Coding dojo est une session d'entraînement aux bonnes pratiques de développement sur un problème simple - comme un compteur de points au bowling 🎳. Les bonnes pratiques incluent le **T**est **D**riven **D**evelopment : [TDD]{{< ref "/tags/tdd">}} et le pair programming. On peut ajouter des contraintes spécifiques à une session, comme pas de `if`.

Pour plus de détails, je vous invite à vous renseigner lire mon billet 👉 [Coding dojo en 3 phrases]({{< ref "/post/fr/2021-01-31-coding-dojo-en-3-phrases" >}})

Les coding dojo permettent de mettre en oeuvre les bonnes pratiques sur des cas simples, mais :

- La barrière à l'utilisation des bonnes pratiques n'est pas forcément levée sur du code de production : TDD semble simple sur des problèmes faciles mais il reste dur à mettre en place sur des problèmes moins académiques ;
- il est difficile de passer à l'échelle sur le nombre de personnes "touchées" : le format collaboratif n'est pas compatible avec des sessions à trois cents personnes en visioconférence ;

## Test clinics

Le principe est simple, il s'agit de coder des tests logiciels en collaboration (ou `mob programming` :gb:) dans un temps limité (en général, une heure). Une personne amène un bout de code qu'elle souhaite tester et tous les autres participants vont l'aider à développer des tests. Le tout en ne modifiant pas le composant (ou alors un minimum). Le but n'est pas de faire une revue de code, mais d'aider à tester.

Pour plus de détails, je vous invite à lire mon billet 👉 [Clinique du test]({{< ref "/post/fr/2022-04-06-test-clinic" >}}).

Cette approche permet de sensibiliser et de monter en compétences sur les tests, mais elle a aussi des limites :

- Elle résout un problème de test par semaine, ce qui limite le passage à l'échelle sur le nombre de problèmes résolus ;
- Le format collaboratif n'est pas compatible avec des sessions à trois cents personnes en visioconférence, ce qui limite le passage à l'échelle sur le nombre de personnes "touchées" ;
- Des problèmes récurrents apparaissent, ce qui peut entraîner une certaine répétition.

## Complémentarités

Les Test clinics et les Coding dojos sont complémentaires :

- Le Coding dojo se place dans un contexte simple pour s'entraîner, tandis que la Test clinic se place dans un contexte existant pour appliquer les bonnes pratiques ;
- Le Coding dojo se concentre souvent sur le code, tandis que la Test clinic se concentre sur les tests ;

Cependant, il reste des limites :

- Le passage à l'échelle sur le nombre de personnes "touchées" reste difficile ;
- La barrière à l'utilisation du TDD sur du code de production n'est pas toujours levée ;
- Des problèmes récurrents apparaissent, ce qui peut entraîner une certaine répétition.

## Pistes pour la suite

Pour les problèmes de passage à l'échelle, on peut envisager plusieurs outils pour pérenniser l'approche :

- La création et l'utilisation des dépôts publics de katas ;
- La promotion de l'utilisation de sites de jeux de programmation comme [Codewars (https://www.codewars.com/)](https://www.codewars.com/) ou [CodinGame (https://www.codingame.com)](https://www.codingame.com) ;
- La création de vidéos...

Pour éviter la répétition de problèmes de même nature, je pense que la création de guides de test sur des thématiques récurrentes peut être utile, par exemple sur les composants React, les lambdas, les tâches Airflow... La Test clinic peut alors être vue comme un instrument de captation des besoins de test et donc des candidats à la documentation.

Concernant la barrière à l'utilisation du TDD sur du code de production, je tente actuellement la mise en place de sessions spécifiques tous les mois pour travailler sur une problématique de mise en oeuvre de TDD. Cela ne fonctionne pas 😢. Avec le recul je pense qu'une personne ne va pas attendre une occasion dans le mois pour coder une fonctionnalité dont elle a besoin.

## Conclusion

La problématique de la barrière à l'utilisation du TDD est la plus importante pour moi. En effet, de nombreux tests sont plus difficiles à écrire *a posteriori*. La méthodologie implique de concevoir le code pour qu'il soit testable. Il est nécessaire analyser les freins au TDD. Cela n'est pas facile pour moi car j'ai intégré cette pratique dans tous mes développements.

La dynamique est présente, il reste encore de nombreuses pistes à explorer pour pérenniser l'usage des bonnes pratiques de développement dans mon entité.

Si vous avez des expériences ou des questions sur l'animation d'une communauté de développeurs, n'hésitez pas à les partager avec moi sur [X](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
