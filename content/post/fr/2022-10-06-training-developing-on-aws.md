---
title: "Formation : Developing on AWS"
date: "2022-10-06"
draft: false
description: "du point de vue d'un développeur"
tags: ["devops","formation","cloud"]
author: "Bertrand Madet"
---

Après la formation [Migrating to AWS]({{< ref "/post/fr/2022-07-20-training-migrating-to-aws.md">}}), j'ai participé la semaine dernière à une formation intitulée `Developing on AWS`. La formation durait quatre jours et décrivait les services utiles pour le développement d'application sur AWS. Voici un bref résumé de la formation puis trois points qui m'ont paru significatifs.

## La formation

### La forme

Durant trois jours, la formation se déroule sur une alternance de présentation d'un ou plusieurs concepts suivie d'une manipulation individuelle sur un espace AWS dédié - `les labs`. Les labs présentent deux niveaux de difficulté soit avec des instructions haut niveau soit avec des instructions détaillées. Dans les instructions détaillés, il y a des choix à faire et les corrections sont proposées. L'interaction est importante. Les outils sont dans le navigateur, nous n'avons pas eu de problèmes d'interaction avec le système d'information corporate, tout se passe bien.

Le dernier jour est consacré à des challenges techniques en équipe de trois. Les challenges ressemblent aux labs mais ils ont moins d'instructions. Il y a un système de points pour chaque challenge, de points perdus pour les indices. Comme il y a plusieurs équipes, une petite compétition s'installe. C'est très sympa cela permet de renforcer nos acquis en jouant et en équipe.

### Le fond

La formation commence par des rappels fondamentaux sur AWS comme l'importance des services web et la gestion de droit et permission grâce à IAM. Ensuite la formation présente les services AWS mis en valeur pour le développement d'application cloud native :

- [S3](https://docs.aws.amazon.com/fr_fr/s3/) pour le stockage objet ;
- [DynamoDB](https://docs.aws.amazon.com/fr_fr/dynamodb/) pour la base de donnée orientée document ;
- [Lambda](https://docs.aws.amazon.com/fr_fr/lambda/) pour le traitement de tâches courtes ;
- [API Gateway](https://docs.aws.amazon.com/fr_fr/apigateway) pour proposer des interfaces web unifiées ;
- [Step functions](https://docs.aws.amazon.com/fr_fr/step-functions/) pour orchestrer des traitements d'évènements ;
- [Cognito](https://docs.aws.amazon.com/fr_fr/cognito/) pour l'authentifier et autoriser ;
- [CloudWatch](https://docs.aws.amazon.com/fr_fr/cloudwatch/) et [X-Ray](https://docs.aws.amazon.com/fr_fr/xray/) pour l'observabilité.

En passant sur les services, la formation aborde aussi des notions de développement cloud native comme les micro services ou l'observabilité.

## Ce que j'en ai retenu

### Serverless mis en avant

Comme je l'avais remarqué lors de ma formation précédente, les services serverless sont rois.

> :teacher: `Serverless` ce dit d'un service où l'on a pas besoin de s'occuper de serveur.

Tous les services présentés sont serverless. Et c'est impressionnant ce que l'on peut faire sans avoir à administrer un seul serveur. Les lambdas sont les dignes représentantes du serverless, elles sont déclenchables par des évènements liés à S3, à API Gateway ou par  Step functions. Elles permettent de développer en Java, Node.js, Python ou même Go.

### API Gateway est polymorphe

J'ai déjà utilisé le service API Gateway mais j'étais passé à côté des fonctionnalités :

- d'import ou d'export via une description [Swagger (https://swagger.io/docs/specification/2-0/basic-structure/)](https://swagger.io/docs/specification/2-0/basic-structure/) ;
- d'import ou d'export via une description [OpenAPI v3 (https://swagger.io/docs/specification/about/)](https://swagger.io/docs/specification/about/) ;
- de contrôle des paramètres d'entrée ou de sorties au format [JSON Schema (http://json-schema.org/)](http://json-schema.org/) ;
- de transformation des entrées ou des sorties avec [Velocity Template Language](https://velocity.apache.org/engine/1.6.2/user-guide.html).
- de déployer progressivement des nouvelles versions d'API grâce au déploiement `Canary`

Comme tous les services d'AWS sont basés sur des services web, on peut interroger sans lambda intermédiaire des services AWS en passant par une transformation d'entrée et une transformation de sortie, par exemple DynamoDB.

### Le SDK est bas niveau

Le **S**oftware **D**evelopment **K**it a deux niveaux d'abstraction :

- Un niveau bas basé sur le service `Client`, il reprend les commandes de l'API REST sur laquelle est basée le service ;
- Un niveau haut basé sur le service `Resource`, il fournit des classes d'abstraction pour réaliser les opérations sur les entités du service.

Par exemple, pour ajouter un document dans une table, il est possible d'utiliser les deux niveaux.

Côté bas niveau, cela pourrait être comme cela :

```python
# boto3 c'est le nom de la bibliothèque Python qui fournit le SDK
import boto3
# On utilise le service resource.
client = boto3.client('dynamodb')
# On récupère la table users
response = client.put_item(
    Item={
        'username': {
            'S': 'janedoe',
        },
        'first_name': {
            'S': 'Jane',
        },
        'last_name': {
            'S': 'Doe',
        },
        'age': {
          'N': '25'
        },
        'account_type': {
          'S':'standard_user',
        }
    },
    ReturnConsumedCapacity='TOTAL',
    TableName='users',
)
```

Côté haut niveau, le code pourrait être le suivant :

```python
# boto3 c'est le nom de la bibliothèque Python qui fournit le SDK
import boto3
# On utilise le service resource.
dynamodb = boto3.resource('dynamodb')
# On récupère la table users
table = dynamodb.Table('users')
# On ajoute un document dans la table
table.put_item(
   Item={
        'username': 'janedoe',
        'first_name': 'Jane',
        'last_name': 'Doe',
        'age': 25,
        'account_type': 'standard_user',
    }
)
```

On voit que le niveau bas est très proche des requêtes HTTP à envoyer à AWS. Le niveau haut n'est pas disponible sur tous les services et tous les langages supportés.

En JavaScript, les exemples de connexion avec Cognito utilise systématiquement une [autre bibliothèque basée sur AWS Amplify (https://docs.amplify.aws/lib/)](https://docs.amplify.aws/lib/).

## Conclusion

J'ai apprécié cette formation, qui m'a permis de revoir les notions de base et de découvrir des nouveaux services ou des fonctionnalités qui m'avait échappé sur les services que je connaissais. La formation reste centrée sur AWS et cela peut apparaître parfois comme une publicité pour AWS. J'ai, malgré cela, hâte de développer sur ces services.

Cela m'a permis de me conforter dans l'impression que l'expérience développeur n'est pas encore optimale à cause d'un niveau d'abstraction plutôt bas sur le SDK et d'une segmentation entre le SDK et les bibliothèques complémentaires de AWS Amplify.

Si vous avez des questions ou des retours d'expériences sur des développements d'application sur le cloud, je suis très intéressé. N'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
