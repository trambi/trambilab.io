---
title: "Capturer les sorties avec pytest"
date: "2024-11-26"
draft: false
description: "sortie standard ou sortie d'erreur"
tags: ["python","test", "outils"]
author: "Bertrand Madet"
---

Dans ce billet, je vous propose de tester la sortie standard ou la sortie d'erreur grâce à [pytest](https://docs.pytest.org/en/latest/).

## Fixtures de pytest capxxx

Pour cela, nous pouvons utiliser des fixtures de pytest :

- [capsys](https://docs.pytest.org/en/latest/reference/reference.html#capsys) permet de capturer les sorties texte au niveau Python ;
- [capsysbinary](https://docs.pytest.org/en/latest/reference/reference.html#capsysbinary) permet de capturer les sorties binaires au niveau Python ;
- [capfd](https://docs.pytest.org/en/latest/reference/reference.html#capfd) permet de capturer les sorties texte au niveau système ;
- [capfdbinary](https://docs.pytest.org/en/latest/reference/reference.html#capfdbinary)  permet de capturer les sorties binaires au niveau système.

⚠️ On remarquera que les noms des fixtures ne simplifient pas la mémorisation, le terme `sys` correspond à la capture des fonctions `sys.stdout` et `sys.stderr` et pas à capture du système. Le terme `fd` correspond à descripteur de fichier (🇬🇧 file descriptor).

Comment cela fonctionne, on va utiliser la fixture et la méthode `readouterr`. La méthode retourne un tuple `err` pour la sortie d'erreur et `out` pour la sortie standard.

```python
# my.py
def my():
  print('hello')

# test_my.py
import pytest
from . import my

def test_my(capsys):
  my.my()
  captured = capsys.readouterr()
  assert captured.out== 'hello\n'
```

La documentation de pytest sur le sujet [Comment capturer les sorties standard et d'erreur](https://docs.pytest.org/en/latest/how-to/capture-stdout-stderr.html) est très bien faite.

## Conclusion

En général, il est préférable d'utiliser les fonctions de journalisation 🗞️. *A priori* Pytest permet également de capturer les messages journalisés mais je n'ai pas eu l'occasion d'utiliser cette fonctionnalité.

Si vous avez des retours ou d'autres conseils sur les outils de test en Python, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
