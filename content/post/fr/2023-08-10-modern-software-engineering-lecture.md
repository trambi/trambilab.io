---
title: "Modern Software Engineering - Fiche de lecture"
date: "2023-08-10"
draft: false
description: "Livre de  David Farley sur  l'ingénierie logicielle"
tags: ["fiche de lecture", "Software Craftmanship"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Ce livre s'adresse à celles et ceux essayent de produire des logiciels de meilleure qualité.

## Ce qu'il faut retenir

- La problématique logicielle est un problème de conception pas de production ;
- La définition de l'ingénierie logicielle de l'auteur : "L'ingénierie logicielle est l'application de l'approche empirique et scientifique pour résoudre efficacement et économiquement des problèmes logiciels concrets" ;
- Les solutions doivent être évaluées selon des mesures ;
- les mesures de stabilité et de débit sont les plus corrélées avec la productivité.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: La définition de critères mesurables pour évaluer l’évolution de la qualité logicielle | :-1: Propos trop répétitifs à mon goût|
| :+1: Les concepts du DevOps appliqués à l'ingénierie logicielle sont bien abordés |:-1: L'argumentation n'est pas basée sur les critères mesurables|

Je suis très partagé sur ce livre : je suis d'accord avec le propos du livre mais la forme me gêne. Je trouve que les arguments se répêtent sans apporter d'éléments nouveaux et suffisament basés sur des mesures.

J'aurai aimé que l'auteur donne plus d'indication sur les mesures de stabilité et de débit ou qu'il illustre le processus de décision avec plus de détails dans un contexte particulier.

Pour le reste comme l'influence de la testabilité sur la modularité, la séparation des responsabilités, l'abstraction et du couplage, je suis d'accord. Je vous conseille de lire d'autres livres de [base]({{< ref "/tags/base" >}}) plutôt que celui-ci. Comme ce livre cite beaucoup le livre "Accelerate" je pense que je vais l'ajouter à ma liste de lecture.

## Détails

- Titre : Modern Software Engineering - Doing what works to build better software faster
- Auteur : David Farley
- Langue : :gb:
- ISBN : 978-0-13-731491-1
