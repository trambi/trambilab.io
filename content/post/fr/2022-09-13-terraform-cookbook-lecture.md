---
title: "Terraform cookbook - Fiche de lecture"
date: "2022-09-13"
draft: false
description: "Livre de Mikael Krief sur Terraform"
tags: ["fiche de lecture","cloud","devops","outils","terraform"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Si vous vous devez implémenter des infrastructures avec Terraform, ce livre vous apportera des astuces très concrètes sur l'utilisation de Terraform.

## Ce qu'il faut retenir

- Terraform est un langage de programmation déclaratif ;
- qui permet de gérer son infrastructure avec du code ;
- Ce livre complète bien la documentation avec des exemples très concrets.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: L'approche pédagogique systématique très progressive| :-1: Très spécifique à Terraform qui manque de recul sur l'infrastructure en tant que code|
| :+1: Très complète sur les possibilités de Terraform| :-1: La partie spécifique au fournisseur cloud utilise Azure peu transposable aux autres fournisseurs cloud|

Ce livre complète bien la documentation grâce à ses recettes très concrètes. Les recettes couvrent une grande variété de Terraform de l'installation à l'automatisation de Terraform avec Terragrunt en passant par la création de module. La structure de chaque recette permet d'ancrer les connaissances par la pratique. Chaque recette est constituée :

- des pré-requis ;
- les étapes détaillées de la procédure ;
- une explication des étapes ;
- des pistes pour aller plus loin.

Les parties sur Azure sont peu transposables à d'autres fournisseurs cloud comme AWS ou Scaleway car les ressources décrites avec le "provider Azure" Terraform sont très proches des ressources Azure. C'est le problème de la spécificité des services cloud de chaque fournisseurs et de l'aspect agnostique des providers fournis par HashiCorp.

## Détails

En anglais :gb: :

- Terraform Cookbook de Mikael Krieg
- ISBN-13 : 978-1-80020-755-4

Pas traduit en français :fr: :cry:
