---
title: "Revue avec GitLab"
date: "2021-11-17"
draft: false
description: "Comment faire des revues asynchrones"
tags: ["devops","gitlab","workflow","outils","Software Craftmanship"]
author: "Bertrand Madet"
---

Après la lecture de [Code Complete (cf. fiche de lecture)]({{< ref "/post/fr/2021-11-01-code-complete-lecture.md" >}}), j'ai été amené à réfléchir à nos processus de revue de code. Je vous propose de partager cette réflexion puis de voir l'implémentation dans GitLab.

## État des lieux

Dans notre équipe, l'inspection du code s'appuie sur la programmation par paire et sur les revues de code durant les merge requests.

La [programmation par paire]({{< ref "/post/fr/2021-09-14-pair-programming-pomodoro.md" >}}) permet une revue instantanée du code. Mais elle nécessite d'avoir :

- deux développeurs disponibles au même moment ;
- une bonne connexion en ligne ou une proximité en présentiel.

Avec la pandémie, ce n'est pas facile de réunir ces deux conditions et  nous n'arrivons pas à utiliser tout le temps la programmation par paire.

En complément, nous utilisons systématiquement le mécanisme de revue de code intégré au merge request de GitLab.

Malgré ces mécanismes, des défauts de même type sont arrivées sur la branche principale et parfois même en production.

## Amélioration

Le livre Code Complete insiste sur la notion de revue de code formelle, c'est à dire une revue de code  avec des rôles spécifiques (auteur, animateur et auditeurs) et l'utilisation d'une checklist des points d'attention.

La checklist des points d'attention est constituée en fonction du contexte de l'application :

- les priorités d'architecture ;
- les défauts déjà apparus ;
- les exigences réglementaires...

Nous avons fait une petite réunion pour établir notre checklist et l'intégrer dans le gabarit de merge request. Les différents éléments de la checklist sont répartis entre trois rôles (auteur, auditeur, personne qui déclenche la fusion de la branche).

## Implémentation dans GitLab

GitLab permet d'utiliser le format Markdown.

```
En tant qu'auteur, j'ai :
- [ ] implémenté la fonctionnalité X ;
- [ ] implémenté les tests unitaires de la fonctionnalité X ;
- [ ] testé que l'interface graphique est conforme aux maquettes sur un environnement de validation ;
- [ ] implémenté les tests end-to-end d’accès à l'API ;
...

En tant qu'auditeur, j'ai :
- [ ] vérifié que le code est conforme au guide de contribution ;
- [ ] vérifié que des tests ont été ajouté ou modifié ;
- [ ] testé que l'interface graphique est conforme aux maquettes sur un environnement de validation ;
- [ ] implémenté les tests end-to-end d’accès à l'API ;
...

En tant que personne ayant déclenché la fusion, j'ai :
- [ ] vérifié que le product owner a validé la fonctionnalité
```

Dans l'exemple ci-dessus, le texte `- [ ]` permet de faire afficher à GitLab une case à cocher, cliquable par tous les acteurs de la merge request.

Il est possible d'ajouter des gabarits de merge requests dans le répertoire `.gitlab/merge_request_templates`. La documentation est disponible [là (https://docs.gitlab.com/ee/user/project/description_templates.html#create-a-merge-request-template)](https://docs.gitlab.com/ee/user/project/description_templates.html#create-a-merge-request-template).

Pour les éditions premium, il est possible d'utiliser un gabarit par défaut (dans le menu `Settings/Merge Requests`).

## Conclusion

Voilà une semaine que nous avons mis en place cette amélioration et une demi-douzaine de merge requests ont été effectué. à titre personnel, j'ai déjà repéré des défauts grâce à l'ajout de cette checklist.

Si vous voulez partager vos expériences d'amélioration des processus de développement et ce que vous en avez retiré, partagez les à moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
