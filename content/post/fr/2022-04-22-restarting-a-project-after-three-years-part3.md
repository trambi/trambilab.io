---
title: "Reprendre un projet après 3 ans - partie 3"
date: "2022-04-22"
draft: false
description: "Bilan après avoir ajouter des fonctionnalités"
tags: ["projet perso","php","jeu"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série :

- [le premier sur les étapes pour réussir à reconstruire le logiciel]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years.md" >}}) ;
- [le deuxième sur l'ajout de fonctionnalité]({{< ref "/post/fr/2022-04-13-restarting-a-project-after-three-years-part2.md" >}}) ;
- [le quatrième sur les enseignements de l'expérience]({{< ref "/post/fr/2022-05-11-restarting-a-project-after-three-years-part4.md" >}}) ;
- [le cinquième sur la situation un an après]({{< ref "/post/fr/2023-06-07-restarting-a-project-after-three-years-part5.md" >}}) ;
- [le sixième sur la situation deux après]({{< ref "/post/fr/2024-05-22-restarting-a-project-after-three-years-part6" >}}).

Cela fait trois semaines que j'ai repris un projet personnel qui je n'avais pas touché depuis 3 ans. J'ai continué à ajouter des fonctionnalités dans la partie Administration. Les modifications pouvaient être l'export de fichiers CSV, une mise à niveau de la bibliothèque graphique - [Bootstrap (https://getbootstrap.com/)](https://getbootstrap.com/) et des modifications mineures d'interface.

Je propose un petit bilan de l'aventure.

## Bilan

### Sur le code

Comme j'ai ajouté des fonctionnalités dans les parties logique métier, interface et même dans les modèles. J'ai exploré (à nouveau) le code en profondeur. J'ai pu aussi compter le nombre de `c'est quoi ce b****l` par minute :
![La seule mesure de qualité du code](http://www.osnews.com/images/comics/wtfm.jpg)

J'ai pu me remémorer un problème dans la structure du code, avant le portage sur Symfony, j'avais utilisé deux classes :

- `DataProvider` pour récupérer les données ;
- `DataUpdater` pour mettre à jour les données.

Au moment du portage, je n'ai pas fini de migrer ces deux classes vers les modèles - le **M** du [motif d'architecture MVC](https://fr.wikipedia.org/wiki/Mod%C3%A8le-vue-contr%C3%B4leur).

Ce qui fait que la responsabilité de la gestion des données est diluée, c'est de mon point de vue, le problème le plus important.

Une autre manière d'analyser le logiciel est de regarder un graphe de dépendances
```mermaid
graph LR;
subgraph Core
  core-form[TournamentCoreBundle/Form]-->external[External namespaces]
  core[TournamentCoreBundle]-->external
  core-entity[TournamentCoreBundle/Entity]-->external
  core-entity-->core-util[TournamentCoreBundle\Util]
  core-di[TournamentCoreBundle/DependencyInjection]-->external
  core-controller[TournamentCoreBundle/Controller]-->external
  core-controller-->core-util[FantasyFootball\TournamentCoreBundle\Util]
  core-controller-->core-entity
  core-utils[TournamentCoreBundle/Util]-->core
  core-utils==>core-entity
end
subgraph Admin
  admin-form[TournamentAdminBundle/Form]-->external
  admin-form-->core-entity
  admin-form-->core-utils
  admin-services[TournamentAdminBundle/Services]-->core-utils
  admin-services-->core-entity
  admin-di[TournamentAdminBundle/DependencyInjection]-->external
  admin-controller[TournamentAdminBundle/Controller]-->external
  admin-controller-->core-util
  admin-controller-->core-entity
  admin-controller-->admin-util
  admin-controller-->admin-form
  admin-controller-->admin-services
  admin-util[TournamentAdminBundle/Util]-->external
  admin-util==>core-entity
  admin-util-->core
  admin-util-->core-util
  admin[TournamentAdminBundle]-->external
end
```

Je vois un problème de dépendances : des espaces de nom contenant la logique métier -`TournamentAdminBundle/Util` et `TournamentCoreBundle/Util` - dépendent d'un espace de nom qui contient des détails d'implémentation de stockage - `TournamentCoreBundle/Entity`.

### Sur le choix des composants

L'utilisation de PHP rend le logiciel complexe à installer, comme les organisateurs ne sont pas forcément des férus d'informatique, c'est un critère important. Avec l'explosion des solutions pour déployer des logiciels (Heroku, les cloud publiques ...), l'avantage du PHP - très facilement déployable sur les hébergements est diminué.

L'utilisation de MySQL complexifie également l'installation et l'exploitation du logiciel.

## Amélioration d'après tournoi

Le problème principal étant de faciliter l'utilisation du logiciel (installation et exploitation), je pense que ce sera la prochaine étape après le tournoi.

Un exécutable monolithique pour l'API REST et l'administration et une base de donnée intégré à cet exécutable permettrait de simplifier énormément l'utilisation du logiciel.

[Go (https://go.dev/)](https://go.dev/) ou [Rust (https://www.rust-lang.org/fr/)](https://www.rust-lang.org/fr/) permettaient de produire un exécutable monolithique.

[SQLite (https://sqlite.org/index.html)](https://sqlite.org/index.html) faciliterait l'installation puisque SQLite est sans serveur. En bonus, les données sont localisés dans un fichier qu'il suffit de copier pour effectuer une sauvegarde.

## Conclusion

Cette étape m'aura permis de me rendre compte que :

- le code n'est pas aussi propre que je l'aurai souhaité ;
- les problèmes de code sont résolvables par une série de refactoring - cf. mon billet sur [le TDD, refactoring et Git]({{< ref "/post/fr/2021-07-13-tdd-refactoring-git.md" >}}) ;
- le problème principal est un problème d'adaptation des choix de composants aux utilisateurs.

Il y a des synergies possibles entre ce projet et mon projet de [Boîte à outils pour tournoi]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}). Je verrai comment j'arrive à faire progresser le sujet.

Si vous avez des conseils pour le développement en PHP ou des propositions sur l'évolution de l'architecture de ce logiciel, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

Si vous souhaitez contribuer, le dépôt est sur Github : <https://github.com/trambi/FantasyFootballArchitecture>.

Le dessin sur `WTF` est issu d'une [page de osnews - https://www.osnews.com/story/19266/wtfsm/](https://www.osnews.com/story/19266/wtfsm/).

Ce billet fait partie d'une série :

- [le premier sur les étapes pour réussir à reconstruire le logiciel]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years.md" >}}) ;
- [le deuxième sur l'ajout de fonctionnalité]({{< ref "/post/fr/2022-04-13-restarting-a-project-after-three-years-part2.md" >}}) ;
- [le quatrième sur les enseignements de l'expérience]({{< ref "/post/fr/2022-05-11-restarting-a-project-after-three-years-part4.md" >}}) ;
- [le cinquième sur la situation un an après]({{< ref "/post/fr/2023-06-07-restarting-a-project-after-three-years-part5.md" >}}) ;
- [le sixième sur la situation deux après]({{< ref "/post/fr/2024-05-22-restarting-a-project-after-three-years-part6" >}}).
