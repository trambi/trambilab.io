---
title: "Learning Domain-Driven Design - Fiche de lecture"
date: "2023-01-03"
draft: false
description: "Livre de Vlad Khononov sur le DDD et l'architecture logicielle"
tags: ["fiche de lecture","architecture","DDD"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Aux personnes intéressées par la conception pilotée par le domaine (ou :gb: **D**omain-**D**riven **D**evelopment) et l'architecture logicielle.

## Ce qu'il faut retenir

Le `DDD` se base sur la compréhension du contexte du logiciel. Un enjeu important est la décomposition du logiciel en sous-domaines. Les sous-domaines sont le lieu pour définir un langage ubiquitaire, partagé entre les équipes de développement et les experts métiers. 

L'idée est de faire les efforts sur les sous-domaines coeur grâce auxquels l'organisation acquiert un avantage compétitif. Les sous-domaines de support - avec des complexités métier moindre - peuvent aussi nécessiter des développements spécifiques. Les sous-domaines génériques - avec des règles connues et peu différenciantes - seront souvent implémentées par des produits sur étagères. En fonction de la complexité du domaine, il existe différents gabarits d'architecture de gestion des objets métiers.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Complet sur les liens entre DDD et architecture logicielle| :-1: Le choix de placer d'architecture logicielle juste après la stratégie|
| :+1: Place la stratégie d'un logiciel en premier||
| :+1: Les quiz à la fin de chaque chapitre||

La première partie sur la conception stratégique est très intéressante et la décomposition en sous-domaines sert de fil rouge. La deuxième partie sur l'architecture logicielle et le code ne m'a pas paru à sa place. On fait le grand écart entre la stratégie d'un logiciel et les détails d'implémentation. Les parties suivantes rattrapent très bien l'ouvrage : les techniques et stratégies pour mettre en place le DDD dans la vraie vie puis la comparaison avec d'autres notions comme les microservices ou l'architecture pilotée par les évènements.

La présence de quiz à la fin de chaque chapitre permet de se poser des questions, et de relire ce sur quoi on a un doute.

Je suis satisfait après avoir lu le livre en entier car il est complet sur les liens entre DDD et architecture logicielle. Il est complémentaire du livre `Domain Modeling Made Functional` ([cf. fiche de lecture]({{< ref "/post/fr/2022-08-01-domain-modeling-made-functional-lecture" >}})) car plus orienté architecture logicielle et programmation objet. La place de la stratégie d'une organisation dans l'architecture logicielle fait aussi echo à `Building microservices` ([cf. fiche de lecture]({{< ref "/post/fr/2020-11-30-building-microservices-lecture" >}})).

## Détails

- Titre : Learning Domain-Driven Design - Aligning Software Architecture and Business Strategy
- Auteur : Vlad Khononov
- Langue : :gb: Anglais
- ISBN :  978-1098100131

Pas traduit en français :cry:
