---
title: "TDD en 3 phrases"
date: "2021-06-15"
draft: false
description: "Le développement piloté par les tests"
tags: ["méthodologie","tdd","base","3 phrases","Software Craftmanship","test"]
author: "Bertrand Madet"
---

Le TDD (ou développement piloté par les tests) est une méthodologie de développement que j'utilise depuis longtemps. Il y a souvent des confusions avec le concept des tests unitaires, la qualité du code. Je résume mon expérience en 3 phrases :

1. Le TDD est une méthodologie ;
1. Le TDD rend le code testable ;
1. Le TDD ne rend pas le code meilleur.

Explications... :teacher:

## Le TDD est une méthodologie

C'est une [méthodologie de développement (https://fr.wikipedia.org/wiki/Test_driven_development)](https://fr.wikipedia.org/wiki/Test_driven_development) qui a trois règles :

1. Vous devez écrire un test qui échoue avant de pouvoir écrire le code de production correspondant.
1. Vous devez écrire une seule assertion à la fois, qui fait échouer le test ou qui échoue à la compilation.
1. Vous devez écrire le minimum de code de production pour que l'assertion du test actuellement en échec soit satisfaite.

Ces règles impulsent le fonctionnement suivant :

```mermaid
stateDiagram-v2
    think: Réfléchir
    writeTest: Écrire un test
    testFailed: S'assurer que le test échoue
    writeCode: Écrire juste assez de code pour que le test réussisse
    testSuccessed: S'assurer que le test passe
    testsSuccessed : S'assurer que les tests passent
    refactor: Améliorer le code
    think --> writeTest
    writeTest --> testFailed
    testFailed --> writeCode: Le test échoue
    testFailed --> writeTest: Le test passe
    writeCode --> testSuccessed
    testSuccessed --> refactor: Le test passe
    testSuccessed --> writeCode: Le test échoue
    refactor --> testsSuccessed
    testsSuccessed --> writeTest: Les tests passent
    testsSuccessed --> refactor: Un des tests échouent
```

Le fonctionnement est incrémental et itératif. Il est aussi possible d'utiliser cette méthodologie pour corriger les bugs. Le principe est similaire. La phase de réflexion consiste en général à reproduire le bug puis trouver le test minimum qui illustre le bug.

## Le TDD rend le code testable

Avec cette méthodologie nous sommes obligés de penser aux tests dès le début du développement. La conséquence est que votre code sera testable unitairement. Cela peut paraître un avantage modeste. Je vous assure que si vous commencez par le code de production, vous allez être obligés de faire des acrobaties pour tester votre code. Surtout si vous écrivez des fonctionnalités complexes avant d'écrire vos tests unitaires.

Personnellement, écrire les tests unitaires, c'est un peu comme faire de la documentation c'est moins motivant que le code.  Il est plus facile de se dire pour les tests seront fait plus tard - une manière pudique de dire jamais... Pour être moins clivant, on peut dire qu'il faut une plus grande discipline pour écrire des tests unitaires sans utiliser le TDD.

Le fait d'utiliser le TDD pour les bugs permet d'avoir des tests de non régression et de détecter au plus vite le retour d'un bug.

## Le TDD ne rend pas le code meilleur

En revanche, le TDD n'est pas un remède miracle contre les tests fragiles ni contre le code sale. Les tests fragiles peuvent être la conséquence des mauvais choix lors de l'étape d'écriture des tests. Le code sale lui est la conséquence de mauvais choix lors de la phase de refactoring.

Pour lutter contre les tests fragiles, il y a d'autres principes qui sont très bien décrit dans le livre Software Engineering at Google - [ma fiche de lecture]({{< ref "/post/fr/2021-05-02-software-engineering-at-google-lecture.md" >}}). On peut citer :

- le fait de tester les interfaces et les comportements plutôt que les implémentations ;
- la limitation de l'utilisation des mocks.

Pour lutter contre le code sale, il faut refactorer. Le code testable permet d'avoir confiance lors des refactorings et donc nous donne un outil capital pour améliorer la situation.

## Conclusion

J'espère que ce billet aura dissipé quelques malentendus sur le TDD. Le TDD reste une méthodologie, il est important de la combiner avec des principes pour arriver à un travail satisfaisant. Si vous voulez discuter de TDD (ou d'autres concepts abordés dans ce billet), contactez moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
