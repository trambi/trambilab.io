---
title: "Un volume persistant pour Docker Swarm"
date: "2021-10-04"
draft: false
description: "en utilisant GlusterFS"
tags: ["stockage","glusterfs","devops","docker","dockerswarm"]
author: "Bertrand Madet"
---

Suite à l'incident que j'avais rapidement évoqué lors de l'[article sur l'échec de sauvegarde]({{< ref "/post/fr/2021-04-19-git-work-flow-and-repositories.md" >}}), nous avons décidé d'améliorer la robusterre de notre service. Nous avons identifié quatre points faibles (ou SPOF - **S**ingle **P**oint **o**f **F**ailure). L'un d'eux était notre service d'API basée sur [Strapi (strapi.io)](https://strapi.io) qui ne pouvait être déployée que sur un noeud de notre cluster Swarm. Nous avons utilisé [GlusterFS (https://www.gluster.org/)](https://www.gluster.org/) pour résoudre le problème.

## Contexte

Nous déployons nos services sous la forme de conteneur. Il faut gérer les conteneurs qui sont lancés, les ressources qui leur sont allouées (réseau,  volumes). C'est le but d'un orchestrateur de conteneur.

Actuellement, nous utilisons Docker Swarm. Docker Swarm est en perte de vitesse par rapport à la star des orchestrateurs de conteneur Kubernetes : il est moins sophistiqué mais il a l'immense avantage d'être installé avec les versions récentes de Docker.

## Problème

Notre service d'API a besoin d'un volume pour stocker les fichiers médias (en particulier les images) liés au contenu de l'API. Le service d'API puisse fonctionner sur n'importe quel noeud du cluster, il faudrait que les données soient disponibles sur chaque noeud du cluster. Sur Kubernetes, le concept de `volume persistant` adresse ce problème. Malheureusement, Docker Swarm n' pas le concept de stockage persistant.

## Solution

GlusterFS est un système de fichier distribué. Il permet de créer des volumes répliqués. L'idée est de :

- créer un cluster de fichiers GlusterFS ;
- créer un volume répliqué (axé sur la robustesse) ;
- monter le volume sur chaque noeud du cluster.
et de les monter sur différents serveurs.

```mermaid
graph LR
node1[noeud 1]-->gluster[cluster GlusterFS]
node2[noeud 2]-->gluster
node3[noeud 3]-->gluster
```

Pour l'instant et afin de limiter le nombre de serveurs utilisés, le principe est d'utiliser les noeuds du cluster Swarm pour héberger le cluster GlusterFS. 

Sur CentOS 7 et pour GlusterFS 9, il faut installer :

- glusterfs ;
- glusterfs-cli ;
- glusterfs-client-xlators ;
- glusterfs-fuse ;
- glusterfs-server ;
- libgapi0 ;
- libgfchange0 ;
- libgfrpc0 ;
- libgfxdr0 ;
- libglusterd0 ;
- libglusterfs0 ;
- userspace-rcu.

## Solutions écartées

Nous avons écartés :

- la solution conteneurisée de serveur GlusterFS ;
- l'utilisation d'une extension Docker pour monter directement des volumes GlusterFS ;
- un cluster [MinIO (https://min.io/)](https://min.io/) et l'utilisation d'une extension pour exploiter les stockages S3 à partir de notre service d'API.

Nous n'avons pas pu trouver des conteneurs "tout-fait" de serveurs GlusterFS. Cette solution pourrait être une évolution de la solution retenue en prenant le temps de créer un conteneur.

Nous n'avons pas réussi à faire fonctionner l'extension Docker pour les volumes GlusterFS.

Un cluster MinIO a été facilement installé et des collègues utilisent avec succès le cluster MinIO. Le problème est que l'extension du service d'API n'était pas maintenue : elle n'a eu pas de mises à jour depuis plus d'un an et le service est très fréquemment mis à jour. C'était un point éliminatoire pour cette solution.

## Bonus

Une option qui peut vous éviter de perdre beaucoup de temps sur la recherche d'erreur en utilisant Docker `--no-trunc`. Cette option permet d'afficher la totalité du message d'erreur pour `docker service ps <service>` (et `docker ps` me semble-t-il).

## Conclusion

Cette solution nous permet de répondre au besoin, ce n'est pas la panacée, l'utilisation de montage de répertoire est plus restreinte que l'utilisation de volume nommé dans Docker (cf. https://docs.docker.com/storage/).

Si vous avez des aventures sur les exploitations de logiciels, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
