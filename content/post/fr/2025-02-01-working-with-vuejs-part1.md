---
title: 'Travail avec Vue.js - Partie 1'
date: '2025-02-01'
draft: false
description: ''
tags: ["front-end","monorepository","vuejs","typescript","projet perso"]
author: 'Bertrand Madet'
---

Depuis mai et mon article [Premier pas avec Vue.js]({{< ref "/post/fr/2024-05-02-first-steps-with-vuejs" >}}), je continue à avancer mon application d’administration. J'ai pu avancer dans d'autres concepts et tirer certains enseignements de cette expérience. 

```mermaid
graph LR;
    orga([Organisateur])-->admin[Application d'administration web v2 - TypeScript + VueJs]
    admin-->api[API REST v2 - Go + gorilla/mux]
    api-->db[(Base de données - Sqlite ou PostgreSQL)]
```

Avant de commencer sur les aspects spécifiques à la bibliothèques, je constate que j'ai passé pas mal de temps pour arriver à un niveau de fonctionnalités équivalent à l'interface d'administration précédente. Oui, une application même avec une technologie obsolète peut présenter une richesse fonctionnelle et donc nécessiter un temps important de réécriture. Pour plus de détail, je vous propose de visiter mon [article sur les réécritures]({{< ref "/post/fr/2024-07-06-rewrite-and-consequences" >}}).

## Évènement et v-model

Intéressons-nous maintenant aux aspects propres à la bibliothèque Vue.js.

### Évènement

Un concept qui complète bien les propriétés ou `props` est l'évènement. Là où en React, la fonction de traitement d'un clic sur un bouton ou d'un autre évènement était une propriété à donner sous forme de fonction (en général préfixé de `on`), Vue.js introduit le concept d'[évènement](https://fr.vuejs.org/guide/components/events.html) pour émettre vers l'extérieur.

En Vue.js, on a donc :

- les propriétés ou `props` pour les données d'entrée d'un composant ;
- les événements ou les signaux émis par la fonctionnalité `$emit` pour les données de sortie d'un composant.

Je trouve cela plus lisible. 

Les signaux d'un composant ne sont pas forcément écoutés, cela dépend de l'appelant. L'appelant va utiliser `v-on:` ou le raccourci `@` pour écouter cet événement.

```jsx
<button v-on:click="handleClick">Click me</button>
<button @click="handleClick">Click me</button>
```

### Association avec un champ de formulaire

Il est aussi possible de faire une association avec un champ de formulaire un "modèle" ou une [liaison d'une entrée utilisateur d'un formulaire ](https://fr.vuejs.org/guide/essentials/forms.html) ou `v-model`. Le but est de gérer les entrées/sorties des champs de formulaire. Cela permet de gérer la mise à jour de la valeur d'un champ plus rapidement, on passe de ça (exemple pris de la documentation):

```jsx
<input type="text" :value="text" @input="event => text= event.target.value" />
```

à ça :

```jsx
<input type="text" v-model="text" />
```

On continue à ne pas utiliser la puissance des champs de formulaire HTML (comme en React) mais au moins on ne doit pas l'écrire.

Il est aussi possible de créer des champs de formulaire complexes et de bénéficier de la facilité d'écriture : [v-model du composant](https://fr.vuejs.org/guide/components/v-model.html).

## Limite de mon application du TDD ou tester le comportement

Dans mon implémentation précédente (en PHP et Symfony), j'avais une trentaine de vues. Pour cette implémentation, j'ai 17 vues et 42 composants soit presque le double de fichiers. Cela vient sans doute de l'orientation "composant" de Vue.js. Au lieu de faire une vue où on définit la liste des joueurs et la liste des équipes de joueurs, je vais définir une vue qui contient un composant "liste des joueurs" et un autre composant "liste des équipes de joueurs". Les composants listes sont eux-mêmes composés de boucle de composants "ligne de joueur" et "ligne d'équipe de joueurs".

Or j'ai réalisé chaque composant en [TDD -**T**est **D**riven **D**evelopment]({{< ref "/tags/tdd" >}}), donc je me retrouve avec une soixantaine de fichiers de test dont certains sont redondants 😢 . Comment ai-je pu en arriver là ? 

J'ai sans doute oublié que l'important du test est le comportement et non l'implémentation 🤌. Le fait de découper mon composant "liste" en liste de composant de type "ligne" est en détail d'implémentation - le composant "ligne" a peu ou pas de raison d'être en dehors du composant "liste". Je ne dois ajouter un test au composant "ligne" que si cela m'aide à avoir confiance dans le comportement du composant et sans être redondant avec un test de composant "liste". Autrement dit, les tests du composant "liste" peuvent couvrir les cas nominaux et d'erreur du composant "ligne".

## Conclusion

Ma découverte et mon apprentissage sur Vue.js continuent. Je ressent bien les améliorations de cette bibliothèque par rapport à React.js. Je vais essayer de régler le problème de tests redondants en soulageant des tests bas niveau (pratiquement d'implémentation) et m'assurer que la confiance dans les tests ne diminuent pas.

L'utilisation de Vue.js m'a aussi permis de faire de proposer une interface plus réactive. Des tests utilisateurs sont prévus, je vous tiendrais au courant.

Si vous avez des conseils sur l'utilisation de vue.js, n'hésitez pas à les partager avec moi sur [X](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).


