---
title: "Utilisation des services web en front-end"
date: "2022-01-26"
draft: false
description: "du point de vue d'un développeur back-end" 
author: "Bertrand Madet" 
tags: ["front-end","javascript","programmation","REST API"]
---

Cette semaine, je vous propose le fruit de mon expérience sur l'utilisation de services web dans un contexte de développement front-end.

## Services web dans un contexte de développement front-end !?

:exploding_head: Oui la phrase précédente fait mal à la tête. Je vais commencer par expliquer les termes.

### Services web

Un service web est tout simplement un service informatique qui peut être adressé avec le protocole du web : [HTTP -**H**yper**t**ext **T**ransport **P**rotocol](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol). Les ressources sont identifiées un chaîne de caractères l'[URI - **U**niversal **R**essource **I**dentifier](https://fr.wikipedia.org/wiki/Uniform_Resource_Identifier). 

Donc c'est typiquement un service que l'on peut tester un navigateur. Les types de services web le plus connus sont [SOAP](https://fr.wikipedia.org/wiki/SOAP), [REST](https://fr.wikipedia.org/wiki/Representational_state_transfer) et [GraphQL](https://fr.wikipedia.org/wiki/GraphQL).

### Développement front-end

Le développement front-end était initialement le développement logiciel lié à la couche de présentation. Aujourd'hui, cela se restreint au développement logiciel sur les technologies JavaScript, css et HTML.

## Pourquoi c'est particulier ?

Le développement front-end est particulier car il s'exécute sur le navigateur des visiteurs. Comme les navigateurs doivent exécuter le code de tous les sites web visités, ils sont dotés de mesures de sécurité spécifiques.

Ces mesures de sécurité ont une influence sur la configuration des serveurs qui fournissent les services web.

Je vous propose de comparer l'utilisation d'un service web depuis un logiciel back-end et depuis un navigateur. On va partir du principe que l'identifiant de la ressource est `http://api.mydomain.com/endpoint/acomplicatedid`.

## Contenu mixte

Ce code devrait fonctionner dans n'importe quel serveur avec python et le module `requests`.

```python

import requests

response = requests.get('http://api.mydomain.com/endpoint/acomplicatedid')
```

Si on prend le code équivalent en JavaScript :

```javascript
const response = fetch('http://api.mydomain.com/endpoint/acomplicatedid');
```

La réussite va dépendre si la page servie en HTTP ou HTTPS. Un navigateur émettra une erreur si le protocole HTTPS est utilisé et fonctionnera si le protocole HTTP est utilisé.

Cela s'appelle le `mixed content`, cela s'applique sur des fonctions JavaScript mais aussi des éléments HTML comme `img`, `video`. L'idée est d'empêcher de la fuite d'information via le protocole HTTP depuis une page web servie au protocole HTTPS.

## CORS

Le code JavaScript peut aussi échouer si l'origine de la page reçue et la ressource ciblée sont différentes. Donc la fonction `fetch` fonctionnera si la page sur laquelle le code s'exécute depuis `api.mydomain.com`. Sinon, il y aura une erreur [CORS - Cross-origin resource sharing](https://developer.mozilla.org/fr/docs/Web/HTTP/CORS).

Pour faire fonctionner le script depuis `www.mydomain.com` (au protocole HTTP), il faut que le serveur qui fournit le service web retourne un entête HTTP `Access-Control-Allow-Origin` valant soit `*` soit `http://www.domain.com`.

## Chaînes de certification

En plus de ces mécanismes très spécifiques, il y a la classique vérification des certificats des serveurs HTTPS. Les chaînes de certification de confiance sont centralisés au niveau du navigateur donc cela dépend du visiteur ou de l'organisation des visiteurs.

## Conclusion

Si vous avez des explications ou des conseils sur des spécificités d'un domaine de développement, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
