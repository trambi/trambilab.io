---
title: "The Pragmatic Programmer - Fiche de lecture"
date: "2022-03-23"
draft: false
description: "de David Thomas et Andrew Hunt - édition du vingtième anniversaire" 
author: "Bertrand Madet" 
tags: ["fiche de lecture","code","base","Software Craftmanship"]
---

## A qui s'adresse ce livre ?

Ce livre s'adresse à toutes les personnes qui programment et qui veulent améliorer leur manière de programmer.

## Ce qui faut retenir

The Pragmatic Programmer fournit un guide sur la programmation et la carrière de programmeur. Le livre regroupe 100 astuces reparties dans 53 sujets très variés allant de la lutte contre la dégradation du code à la sécurité en passant par la gestion de la concurrence.

## Mon opinion

| Points positifs | Points négatifs |
|---|---|
| :+1: Varié cela parle du métier et de la pratique pour un volume modeste (moins de 300 pages) | :-1: Certains aspects sont traités superficiellement|
| :+1: Des concepts souvent oubliés des livres généralistes sont présents et bien traités| :-1: Les sujets peuvent être décrits de manière trop abstraite|
| :+1:  Les problèmes à la fin de chaque partie et les solutions à part||

Ce livre est intéressant, l'étendue des sujets et des réflexions est vaste peut-être trop vaste. L'inconvénient est que si voulez lire un et seul livre sur le métier de développeur, je pense que Clean coder ou Software Craftmanship sont plus intéressants et que si vous voulez lire un seul et seul livre sur les pratiques de développement, je pense que Code Complete ou Clean Code feront plus l'affaire.

L'avantage est que des thèmes plus rares dans les livres de référence comme la sécurité, la concurrence et le *property based testing* sont traités.

Je conseille donc ce livre en complément des livres de référence.

## Détails

The Pragmatic Programmer de David Thomas et Andrew Hunt

ISBN: 978-0135957059

Pas disponible en :fr:
