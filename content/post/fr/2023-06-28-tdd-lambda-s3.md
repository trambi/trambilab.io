---
title: "Développer une lambda de transfert d'objet S3"
date: "2023-06-28"
draft: false
description: "en TDD grâce à moto ..."
tags: ["tdd","python","cloud","lambda"]
author: "Bertrand Madet"
---

Cette semaine, je propose l'implémentation en TDD d'une fonction qui va interagir avec des buckets S3 (ou compartiments S3 :fr:).

## Contexte

Le but de la fonction est simple : elle lit un objet dans le bucket S3 d'entrée si la vérification de contenu de l'objet réussit elle copie l'objet dans le bucket S3 de sortie sinon elle copie l'objet dans le bucket S3 d'erreur.

La fonction est donc déployée sous la forme de Lambda, cela a comme conséquence qu'elle a deux paramètres l'évènement et le contexte. J'ai utilisé une version simplifiée de l'évènement proposé dans les tests de Lambda pour un dépôt d'objet sur un bucket S3 et le paramètre contexte n'est pas utilisé.

La fonction de validation est déjà codée (en TDD). Nous verrons qu'il reste qu'en même du code à tester comme les interactions avec les buckets S3 ou le bon appel à la fonction de validation. Je propose de tester la fonction en TDD en nous basant sur deux tests :

- Si l'objet dans le bucket d'entrée est valide, l'objet doit apparaître dans le bucket de sortie ;
- Si l'objet dans le bucket d'entrée est invalide, l'objet doit apparaître dans la bucket d'erreur.

## Premier test

Pour permettre de tester en isolation et sans dépendre d'infrastructure S3, nous allons utiliser la bibliothèque [moto (http://docs.getmoto.org/en/latest/)](http://docs.getmoto.org/en/latest/).

```python
import os
import pytest
from moto import mock_s3
import boto3

@pytest.fixture
def create_bucket_configuration():
    return {"LocationConstraint":"eu-west-3"}

def test_invalid_file(create_bucket_configuration):
    """Given an invalid file
       When an S3 event pointing to this file is received
       Then file must in invalid bucket"""
    with mock_s3():
        #Given
        s3 = boto3.client("s3")
        s3.create_bucket(Bucket='input',CreateBucketConfiguration=create_bucket_configuration)
        s3.create_bucket(Bucket='invalid',CreateBucketConfiguration=create_bucket_configuration)
        s3.upload_file('./invalid.txt','input','invalid.txt')
        event = {
            "Records": [
                {
                    "s3": {
                        "bucket": {"name": "input"},
                        "object": {"key": "invalid.txt"}
                    }
                }
                
            ]
        }
        # When
        handler.s3_upload_handler(event, {})
        # Then
        s3.download_file('invalid','invalid.txt','tmp.txt')
        with open('tmp.txt','r') as result:
            with open('./invalid.txt','r') as reference:
                assert result.read() == reference.read()
        os.remove('tmp.txt')
```

:warning: Le deuxième paramètre de `create_bucket` est là pour éviter une exception avec certaines versions de boto3 : `botocore.exceptions.ClientError: An error occurred (IllegalLocationConstraintException) when calling the CreateBucket operation: "The unspecified location constraint is incompatible for the region specific endpoint this request was sent to.`.

## Première implémentation

On va faire passer le test avec cette implémentation :

```python
import os
import urllib
import boto3

def s3_upload_handler(event,context):
    s3 = boto3.client("s3")
    key = urllib.parse.unquote_plus(
        event["Records"][0]["s3"]["object"]["key"], encoding="utf-8"
    )
    with open('tmpfile','wb') as fout:
        s3.download_fileobj('input',key,fout)
    s3.upload_file('tmpfile','invalid',key)
    os.remove('tmpfile')
```

C'est naïf mais le test passe, on peut refactorer le code en utilisant la fonctionnalité de fichier temporaire.

```python
def s3_upload_handler(event, context):
    s3 = boto3.client("s3")
    key = urllib.parse.unquote_plus(
        event["Records"][0]["s3"]["object"]["key"], encoding="utf-8"
    )
    with tempfile.TemporaryFile(mode="w+b") as fout:
        s3.download_fileobj("input", key, fout)
        fout.seek(0)
        s3.upload_fileobj(fout, "invalid", key)

```

## Deuxième test

On ajoute le test pour le cas avec un fichier valide.

```python
def test_valid_file(create_bucket_configuration):
    """Given an valid file
    When an S3 event pointing to this file is received
    Then file must in valid bucket"""
    with mock_s3():
        # Given
        s3 = boto3.client("s3")
        s3.create_bucket(
            Bucket="input", CreateBucketConfiguration=create_bucket_configuration
        )
        s3.create_bucket(
            Bucket="valid", CreateBucketConfiguration=create_bucket_configuration
        )
        s3.upload_file("./valid.txt", "input", "valid.txt")
        event = {
            "Records": [
                {"s3": {"bucket": {"name": "input"}, "object": {"key": "valid.txt"}}}
            ]
        }
        # When
        handler.s3_upload_handler(event, {})
        # Then
        s3.download_file("valid", "valid.txt", "tmp.txt")
        with open("tmp.txt", "r") as result:
            with open("./valid.txt", "r") as reference:
                assert result.read() == reference.read()
        os.remove("tmp.txt")
```

## Deuxième implémentation

Pour faire passer le test, on est obligé d'appeler la fonction de vérification :

```python
import tempfile
import os
import urllib
import boto3

import validation

def s3_upload_handler(event, context):
    s3 = boto3.client("s3")
    key = urllib.parse.unquote_plus(
        event["Records"][0]["s3"]["object"]["key"], encoding="utf-8"
    )
    with tempfile.TemporaryFile(mode="w+b") as fout:
        s3.download_fileobj("input", key, fout)
        fout.seek(0)
        if validation.check(fout):
          s3.upload_fileobj(fout, "valid", key)
        else:
          s3.upload_fileobj(fout, "invalid", key)
```

## Conclusion

Nous avons un cas d'école et pourtant bien réel d'implémentation en tdd d'une fonctionnalité en Python qui interagit avec des contenants S3. Nous pouvons ensuite continuer avec un autre refactoring. Il reste encore du travail comme la mise en variables d'environnement de nom des buckets S3 ou la gestion des exceptions avant de pouvoir l'utiliser en production mais la base est là.

La bibliothèque `moto` facilite grandement l'écriture des tests en isolation. Côté Javascript, je n'ai pas encore trouvé de bibliothèque qui permet d'obtenir les mêmes facilités. L'avantage est que nous pouvons tester l'exécution directe de la Lambda.

Si vous avez d'autres exemples d'utilisation de `moto` ou des implémentations côté Javascript, partagez les avec moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
