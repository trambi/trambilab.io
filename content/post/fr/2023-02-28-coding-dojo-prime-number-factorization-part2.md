---
title: "Coding dojo : Factorisation en nombres premiers - partie 2"
date: "2023-02-28"
draft: false
description: "en testant des propriétés"
tags: ["dojo","Software Craftmanship","test","tdd"]
author: "Bertrand Madet"
---

Dans l'[article précédent]({{< ref "/post/fr/2023-02-23-coding-dojo-prime-number-factorization-part1.md">}}), j'ai proposé le parcours en faisant du [TDD]({{< ref "/tags/tdd">}}) basé sur des cas. Cette semaine, je propose un parcours TDD basé sur des tests de propriété ou `PBT` (:gb: **P**roperties **B**ased **T**esting).

## Properties based testing

Le principe du test de propriété est de tester la validité d'une propriété plutôt que la validité pour un exemple. Le moteur de test de propriété va générer les tests basés sur des exemples en générant les entrées aléatoirement et s'assurer que les tests basés sur les exemples générés répondent aux propriétés définies. La définition des propriétés est une gymnastique un peu particulière car il est plus habituel de fonctionner avec des exemples.

Le PBT vient de la bibliothèque Haskell [QuickCheck (https://www.cse.chalmers.se/~rjmh/QuickCheck/)](https://www.cse.chalmers.se/~rjmh/QuickCheck/). La bibliothèque QuickCheck est apparue en 1999 :exploding_head: ! . Mais l'approche est tellement puissante qu'il existe des bibliothèques pour beaucoup de langages dont mes préférés :

- pour Python [Hypothesis (https://hypothesis.readthedocs.io/en/latest/index.html)](https://hypothesis.readthedocs.io/en/latest/index.html) ;
- pour TypeScript [fast-check (https://github.com/dubzzz/fast-check)](https://github.com/dubzzz/fast-check) ;
- pour Go [testing/quick (https://pkg.go.dev/testing/quick)](https://pkg.go.dev/testing/quick).

## Propriétés de la factorisation en nombres premiers

Le but est toujours de fournir la liste par ordre croissant des facteurs premiers d'un nombre entier. Un nombre premier étant un nombre entier divisible uniquement par un et par lui-même et pour éviter tout débat un (1) n'est pas un nombre premier. Par convention, pour tout autre entrée qu'un nombre entier supérieur à un (1), on retournera un tableau vide.

Ainsi si on donne douze (12), on obtiendra le tableau deux, deux, trois : `[2,2,3]`.

Mais au lieu de fournir des cas ou des exemples, nous devons fournir des propriétés de la fonction à vérifier. Pour la factorisation en nombres premiers, je vous propose les propriétés suivantes :

1. Le retour de la fonction pour un nombre entier inférieur à 2 est un tableau vide ;
1. Le produit des éléments du retour de la fonction pour un nombre entier supérieur à 1 est égal au nombre fourni. Autrement la fonction est une factorisation ;
1. Le retour de la fonction pour le produit de deux nombres entiers supérieurs à 1 a une longueur supérieure ou égale à 2 ;
1. Le retour de la fonction pour le produit de deux nombres entiers supérieurs à 1 est égal à la concaténation triée par ordre croissant du retour de la fonction pour le premier nombre et du retour de la fonction pour le deuxième nombre :tired_face:.

La dernière propriété permet de s'assurer que le tableau retourné est classé par ordre croissant et que seuls des nombres premiers le composeront. :mage:

## Itérons

### Premier test : Les cas d'erreur

Nous allons utiliser la bibliothèque TypeScript fast-check. Je vais commencer par un premier test pour m'assurer que la fonction renvoie un tableau vide pour une entrée invalide. Je vais utiliser `fc.assert` pour vérifier une propriété `fc.property`, cette propriété met en jeu, une variable qui est un entier dont le maximum est 1 : `fc.interger({max:1})`.

```typescript
import * as fc from "fast-check";
import { primeFactorization } from "./prime-factorization";

describe("primeFactorization", () => {
  test("result should be an empty array for integer less than 2", () => {
    fc.assert(
        fc.property(fc.integer({ max: 1 }), (notFactorizable) => {
          expect(primeFactorization(notFactorizable)).toEqual([]);
        })
      );
  });
});
```

L'implémentation la plus simple pour passer le test est de retourner un tableau vide tout le temps :

```typescript
const primeFactorization = (input: number)=>[];
```

### Deuxième test : La factorisation

La propriété  suivante est que le produit de tous les éléments de mon retour doit être égal au nombre fourni en entrée (si c'est un entier supérieur ou égal à 2). L'utilisation est pratiquement la même que le cas de test précédent sauf que l'entier supérieur ou égal à 2 (et inférieur à 16 777 216) est codé avec `fc.integer({min: 2, max: 2**24})`.

```typescript
import * as fc from "fast-check";
import {primeFactorization} from './';

describe('primeFactorization',()=>{
    test("result should be an empty array for integer less than 2", () => {
      fc.assert(
        fc.property(fc.integer({ max: 1 }), (notFactorizable) => {
          expect(primeFactorization(notFactorizable)).toEqual([]);
        })
      );
  });
    test("result should be a factorization of input", () => {
      fc.assert(
        fc.property(fc.integer({ min: 2, max: 2**24 }), (factorizable) => {
          const result = primeFactorization(factorizable);
          expect(
            result.reduce((acc, value) => {
              return acc * value;
            }, 1)
          ).toBe(factorizable);
      })
    );
  });
});
```

Une implémentation simple pour passer ce test et le précédent est de retourner un tableau vide si le nombre en entrée est inférieur à 2 ou le nombre dans un tableau.

```typescript
const primeFactorization = (input: number)=>{
    if(input > 1){
        return [input];
    }
    return [];
}
```

### Troisième test : Factorisation d'un nombre non premier

On va ensuite s'assurer que la factorisation d'un nombre non premier a au moins deux facteurs. Le fait d'avoir deux variables est symbolisée par l'usage de deux `fc.integer` suivis d'une fonction avec deux paramètres.

```typescript
import * as fc from "fast-check";
import { primeFactorization } from "./prime-factorization";

describe("primeFactorization", () => {
  test("result should be an empty array for integer less than 2", () => {
    fc.assert(
        fc.property(fc.integer({ max: 1 }), (notFactorizable) => {
          expect(primeFactorization(notFactorizable)).toEqual([]);
        })
      );
  });
  test("result should be a factorization of input", () => {
    fc.assert(
      fc.property(fc.integer({ min: 2, max: 2**24 }), (factorizable) => {
        const result = primeFactorization(factorizable);
        expect(
          result.reduce((acc, value) => {
            return acc * value;
          }, 1)
        ).toBe(factorizable);
      })
    );
  });
  test("result of a*b should be at least of length 2", () => {
    fc.assert(
      fc.property(fc.integer({ min: 2, max: 2**24 }),fc.integer({ min: 2, max: 2**24 }), (left,right) => {
        expect(primeFactorization(left*right).length).toBeGreaterThan(1);
      })
    );
  });
});
```

L'implémentation pour passer ce test, nécessite de rechercher au moins un diviseur non trivial avec un boucle et l'opérateur modulo `%` (reste de la division entière).

```typescript
export const primeFactorization = (input: number) => {
  if (input > 1) {
    //return [input];
    const result: number[] = [];
    let divider = 2;
    for (divider; divider < input; divider++) {
      if (input % divider === 0) {
        result.push(divider);
        input = input / divider;
        break;
      }
    }
    result.push(input);
    return result;
  }
  return [];
};
```

### Quatrième test : Nombres premiers et tri dans l'ordre croissant

La dernière propriété  est de le résultat pour a*b est la concaténation triée du résultat pour a et du résultat pour b pour a, b des entiers supérieurs ou égaux à 2 :tada:. 

```typescript
import * as fc from "fast-check";
import { primeFactorization } from "./prime-factorization";

describe("primeFactorization", () => {
  test("result should be an empty array for integer less than 2", () => {
    fc.assert(
        fc.property(fc.integer({ max: 1 }), (notFactorizable) => {
          expect(primeFactorization(notFactorizable)).toEqual([]);
        })
      );
  });
  test("result should be a factorization of input", () => {
    fc.assert(
      fc.property(fc.integer({ min: 2, max: 2**24 }), (factorizable) => {
        const result = primeFactorization(factorizable);
        expect(
          result.reduce((acc, value) => {
            return acc * value;
          }, 1)
        ).toBe(factorizable);
      })
    );
  });
  test("result of a*b should be the sorted concatenation of result of a and result of b", () => {
    fc.assert(
      fc.property(fc.integer({ min: 2, max: 2**24 }),fc.integer({ min: 2, max: 2**24 }), (a,b) => {
        const resultMultiplication = primeFactorization(a*b);
        const resultConcatenation = primeFactorization(a).concat(primeFactorization(b)).sort((left,right)=>left-right);
        expect(resultMultiplication).toEqual(resultConcatenation);
      })
    );
  });
});
```

L'implémentation pour répondre à ce test nous oblige à chercher tous les diviseurs non triviaux, on s'en sort en ajoutant une boucle `while`.

```typescript
const primeFactorization = (input: number)=>{
    if(input > 1){
        let divider = 2;
        const result = [];
        while(input !=1){
            for(divider;divider<=input;divider++){
                if(input % divider === 0){
                    result.push(divider);
                    input = input/divider;
                    break;
                }
            }
        }
        return result;
    }
    return [];
}
```

## Conclusion

L'implémentation est identique avec l'approche classique sans doute car mes choix de tests sont similaires dans les deux cas. On peut donc aussi s'entraîner au TDD avec des tests de propriété. A noter que suite à la remarque d'un de mes collègues (merci François), j'ai ajouté une propriété intermédiaire entre la deuxième et la dernière propriété. l’écart d'implémentation pour répondre à la dernière propriété était trop élevé.

La différence est le niveau de confiance : mon implémentation a l'air de fonctionner pour des nombres jusqu'à 16 millions ! J'ai été obligé de mettre un maximum sinon la vérification de la dernière propriété prenait trop de temps sur ma machine. Avec ce réglage, j'arrive à faire tourner les tests en 5 secondes. Le niveau de confiance est plus grand qu'avec les tests basés sur des exemples.

Si vous souhaitez partager sur des coding dojo, le TDD ou le PBT, contactez moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169
