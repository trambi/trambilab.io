---
title: "Boîte à outils pour tournoi - partie 4"
date: "2021-04-26"
draft: false
description: "Ajout de l'appariement et refactoring"
tags: ["projet perso","Go","jeu","appariement","refactoring"]
author: "Bertrand Madet"
---
Boite à outils pour tournoi :
[partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}),
[partie 2]({{< ref "/post/fr/2020-12-09-tournament-tool-box-part-2.md" >}}),
[partie 3]({{< ref "/post/fr/2020-12-28-tournament-tool-box-part-3.md" >}}),
[partie 5]({{< ref "/post/fr/2021-06-01-tournament-tool-box-part-5.md" >}}),
[partie 6]({{< ref "/post/fr/2023-03-08-tournament-tool-box-part-6.md" >}}),
[partie 7]({{< ref "/post/fr/2023-03-20-tournament-tool-box-part-7.md" >}})

-----

Cela faisait longtemps, que je n'avais pas avancé sur mon projet perso. Aujourd'hui, je vous propose d'analyser l'ajout de l'appariement et la suite du refactoring du code.

## Le but

Le but était d'ajouter la fonctionnalité qui permet à partir d'un classement et des matchs déjà effectués de fournir la liste des prochains matchs - appelée `appariement` :fr: ou `pair` :gb:.

J'en ai profité pour continuer le refactoring lié au graphe de dépendances de mon projet [ cf. Clean Architecture]({{< ref "/post/fr/2020-12-23-clean-architecture-lecture.md" >}}).

## Développement

## Le plan

Le plan était plutôt simple : implémenter le paquet pair et passer de ce diagramme de dépendance

```mermaid
graph TD;
    cmd[CLI]-->csv[import/export en CSV]
    cmd-->core[Logique métier]
    cmd-->json[import/export en json]
    csv-->core
    json-->core
    core==>json
```

De supprimer la dépendance de la logique métier au json :

```mermaid
graph TD;
    cmd[CLI]-->csv[import/export en CSV]
    cmd-->core[Logique métier]
    cmd-->json[import/export en json]
    csv-->core
    json-->core
```

## La réalisation

### Paquet pair

J'avais écrit cet algorithme avec l'aide de Nicolas Mage et Jean-Manuel Pirao en PHP sur [FantasyFootball - un site de gestion de tournoi en PHP (https://github.com/trambi/FantasyFootball)](https://github.com/trambi/FantasyFootball). J'ai donc porté l'algorithme de PHP vers Go assez facilement.

Ce qui a été le plus pénible a été l'import du classement à partir de CSV et l'export de l'appariement en CSV. Je me suis mis à écrire des fonctions de lecture et d'écriture de fichier CSV. Et je l'avais déjà fait pour les matchs et pour les appariements (l'écriture). J'ai donc définie :

- Pour l'écriture, je n'avais besoin que de convertir la structure en tableau de tableau de chaînes de caractères l'interface `IToDoubleArrayOfString` ;
- Pour la lecture, je n'avais besoin que de convertir un tableau de tableau de chaînes de caractères en structure l'interface `IFromDoubleArrayOfString` ;
- Déplacer mes fonctions d'écriture et de lecture dans un paquet `csv` en utilisant l'interface adéquate.

Une fois ajoutée la commande au niveau du fichier `cmd/main.go`, on peut exécuter la commande suivante `./tournamenttoolbox pair id examples/ranking_to_pair.csv examples/games_to_pair.csv` pour avoir les matchs suivants dans le fichier `pairing.csv` - avec le fichier de classement `examples/ranking_to_pair.csv` en excluant les matchs déjà joués `examples/games_to_pair.csv`.

### Refactoring

Cela été surtout du déplacement de répertoire et de l'utilisation des adaptateurs pour la configuration des champs calculés, je suis passé de ce diagramme de dépendance :

```mermaid
graph TD;
    cmd[cmd/main]
    subgraph "logique métier"
        rank[pkg/rank]
        compute[pkg/compute]
        evaluator[pkg/evaluator]
        settings[pkg/settings]
    end;
    subgraph "import/export en json"
        settings_json[pkg/settings/json]
        compute_settings[pkg/compute/settings]
    end;
    subgraph "import/export en csv"
        csv[pkg/csv]
        pair_csv[pkg/pair/csv]
        rank_csv[pkg/rank/csv]
    end;
    cmd-->compute
	cmd-->rank
	cmd-->rank_csv
	cmd-->settings_json
    cmd-->csv
    cmd-->pair_csv
    rank_csv-->csv
    pair_csv-->csv
    compute-->compute_settings
    compute-->evaluator
    rank_csv-->rank
    settings-->compute_settings
    settings-->rank
    settings_json-->rank
    settings_json-->compute_settings
    settings_json-->settings
```

A celui-ci :

```mermaid
graph TD;
    cmd[cmd/main]
    subgraph "logique métier"
        pair[pkg/pair]
        rank[pkg/rank]
        compute[pkg/compute]
        evaluator[pkg/evaluator]
        settings[pkg/settings]
    end;
    subgraph "import/export en csv"
        csv[pkg/csv]
        pair_csv[pkg/csv/pair]
        rank_csv[pkg/csv/rank]
    end;
    subgraph "import/export en json"
        settings_json[pkg/json/settings]
    end;
    cmd-->compute
  	cmd-->csv
	cmd-->pair
	cmd-->pair_csv
	cmd-->rank
	cmd-->rank_csv
	cmd-->settings_json
    cmd-->settings
	compute-->evaluator
    pair_csv-->pair
    rank_csv-->rank
    settings-->rank
    settings-->compute
    settings_json-->rank
    settings_json-->settings
	settings_json-->settings
```

J'ai utilisé la commande `grep` pour rechercher les dépendances. Par exemple pour rechercher toutes les dépendances de mon dépôts des fichiers du répertoire `cmd`, j'ai utilisé `grep -R "gitlab.com/trambi/tournamenttoolbox/pkg" cmd` (et avec mermaid, c'est facile d'afficher le résultat).

#### Bonus

J'ai modifié la configuration json pour passer d'une configuration plutôt indigeste à une écriture plus naturelle (à vérifier). On est passé d'une configuration json plutôt longue :

```json
{
    "name": "points_1",
    "default_value": "0",
    "statements": [
    {
        "clause": {
        "type": "equal",
        "left": "td_1",
        "right": "td_2"
        },
        "value_if_true": "1"
    },
    {
        "clause": {
        "type": "greater",
        "to_be_greater": "td_1",
        "to_be_lesser": "td_2"
        },
        "value_if_true": "3"
    }
    ]
}
```

```json
{
    "name": "points_1",
    "default_value": "0",
    "statements": [
    {
        "clause": "{td_1} = {td_2}",
        "value_if_true": "1"
    },
    {
        "clause": "{td_1} > {td_2}",
        "value_if_true": "3"
    }
    ]
}
```

## En résumé

Je retiens de cette séquence :

- L'application a un graphe de dépendances plus simple et j'espère que l'organisation des paquets est plus simple ;
- Il reste à rendre plus configurable l'appariement pour utiliser simplifier la ligne de commande ;
- Il faudra que je fasse des tests utilisateurs pour l'utilisabilité de mon outil.

Voilà, j'espère que cela vous a plu. Si vous souhaitez partager des projets, contactez [moi sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/). J'accueille les merge requests sur [TournamentToolBox (https://www.gitlab.com/trambi/tournamenttoolbox)](https://www.gitlab.com/trambi/tournamenttoolbox) avec enthousiasme.

-----

Boite à outils pour tournoi :
[partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}),
[partie 2]({{< ref "/post/fr/2020-12-09-tournament-tool-box-part-2.md" >}}),
[partie 3]({{< ref "/post/fr/2020-12-28-tournament-tool-box-part-3.md" >}}),
[partie 5]({{< ref "/post/fr/2021-06-01-tournament-tool-box-part-5.md" >}}),
[partie 6]({{< ref "/post/fr/2023-03-08-tournament-tool-box-part-6.md" >}}),
[partie 7]({{< ref "/post/fr/2023-03-20-tournament-tool-box-part-7.md" >}})
