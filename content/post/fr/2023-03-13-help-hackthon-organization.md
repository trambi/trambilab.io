---
title: "J'ai aidé à l'organisation d'un hackathon"
date: "2023-03-13"
draft: false
description: "Et je partage l'expérience"
tags: ["experience","hackathon"]
author: "Bertrand Madet"
---

Mercredi dernier, j'ai aidé à l'organisation à un hackathon interne sur la mise en valeur de valeur business des réalisations de notre entité. Cela faisait plus de 2 ans que nous avions organisé le dernier (cf. [l'article qui lui était consacré]({{< ref "/post/fr/2021-01-11-participation-hackathon.md">}})). Et vous le verrez, nous avons opté sur un déroulement différent.

## Motivations

Au delà du sujet, nous avions besoin de nous entraîner à organiser des évènements axés sur le code et aussi (et peut-être surtout) de créer un moment de partage entre nous et en particulier avec nos collègues indiens :india: qui nous rendaient visite cette semaine.

## Préparation

Pour rendre acceptable l'évènement et compatible avec les agendas des participants, nous avons opté pour une durée d'une journée. La présence des personnes non francophones nous a obligé à faire tous les phases communes et les supports de présentation en anglais :gb:.

La partie préparation a été gérée par l'organisateur principal. Cette phase est cruciale car elle permet de réunir les participants. Elle est constituée entre autre de la réservation de la salle, du buffet et les invitations. Une quarantaine de personnes a été invitée et vingt quatre personnes étaient présentes (4 organisateurs et 20 participants).

## Déroulement

|Phase|Description|Heure théorique|Heure réalisée|
|-----|-----------|---------------|--------------|
|Accueil|Moment sympa où l'on se retrouve avec un petit déjeuner|9h30|? - Je suis arrivé à 9h34 et il y avait déjà des gens|
|Introduction|Explication du but et des modalités de l'évènement|-|10h|
|Brainstorm|Présentation de sujets puis brainstorm pour proposer 6 sujets|10h|10h05|
|Pitch|Présentation de sujets puis brainstorm pour proposer 6 sujets|11h|11h05|
|Implémentation|Constitution des équipes et début de l'implémentation|11h30|11h40|
|Point d'étape|S'assurer que les équipes n'aient de problème pour avancer et peut-être rebalancer les compétences dans les équipes|14h|14h30|
|Présentation des résultats|Présentation des résultats obtenus soit sous la forme d'une présentation, d'une maquette, d'un prototype |16h30|16h35|
|Clôture|Synthèse, remerciements et demandes de retour sur l'évènement|17h|17h05|

A partir de la consigne de mise en lumière la valeur business des réalisations de notre entité, 5 équipes ont pu présenter soit une maquette soit des proto-applications.

Durant la matinée, j'étais plus en recul de l'évènement et j'ai joué les rôles de :

- gardien du temps :hourglass: pour s'assurer la tenue du déroulement prévue ;
- photographe :camera: pour garder des traces de cet évènement ;
- petites mains :open_hands: pour aider la mise en place du buffet et répondre aux questions des participants.

L'après-midi, j'ai été codé :keyboard: dans une équipe pour les aider à avancer sur une micro-application en [React](https://reactjs.org/) et [ChartJS](https://www.chartjs.org/).

## Ressentis

C'était très agréable de se retrouver et de partager du temps du travailler avec des collègues mais nous avons passé pas mal de temps à manger en discutant plutôt qu'en codant.

Les réalisations étaient présentes mais les applications étaient peu avancées.

La plupart des personnes (et moi compris) ont eu l'impression de ne pas faire un hackathon car la part de code n'a pas été assez importante.

## Conclusion

Je pense que la combinaison idéation le matin et durée sur une journée fait que nous n'avons pas organiser un hackathon mais un moment hybride à mi-chemin entre le design sprint et le hackathon. Ce moment était agréable et c'est cela le plus important. Le format du hackathon précédent avec ses sujets beaucoup plus construits permettait d'aller vers la bidouille et le code.

Un grand merci à l'équipe d'organisateurs, qui a concocté une journée cadencée et enthousiasmante. Et merci aussi aux participants de nous avoir fait suffisamment confiance pour nous donner une journée de leur temps. J'espère que nous allons renouveler l'expérience en prenant compte en prenant compte de cette expérience.

Si vous souhaitez partager avec vos expériences d'hackathon, contactez [moi sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
