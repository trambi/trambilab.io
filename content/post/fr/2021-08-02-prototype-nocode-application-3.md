--- 
title: "Coder un prototype une application pour faire du nocode - partie 3"
date: "2021-08-02"
draft: false
description: "bilan partiel de la démonstration et du prototype"
tags: ["projet perso","javascript","nocode","démonstration"]
author: "Bertrand Madet" 
---

Comme je vous l'ai expliqué dans un [précédent billet]({{< ref "/post/fr/2021-07-22-prototype-nocode-application-1.md" >}}), un collègue et moi avons  prototypé une application `no code`. Après cette phase, nous avons décidé de faire une démonstration aux collègues de notre entité. Je vais vous présenter ce que j'ai retenu de cette expérience. 

## Même sur un prototype, Git est efficace

Mon collègue et moi avons travaillé sur la même application. Le travail sur deux parties bien distinctes, combiné à l'utilisation de `git` a permis d'avancer à notre rythme avec peu de synchronisation. Nous avions discuté du principe et de l'interface,  grâce à `git rebase` et une messagerie instantanée nous avons pu observer et intégrer les développements de manière fluide.

J'ai créé une branche basée sur sa branche puis nous avons utilisé `git rebase` pour intégrer les modifications de l'autre sur  notre branche.

Cela est certes dû à la séparation claire de nos responsabilités, de l'interface définie mais aussi à `git` et sa facilité à gérer les branches.

## Faire une démonstration au plus tôt

Faire une démonstration à des collègues précocement peut sembler risqué, dans une certaine mesure ça l'est. Ce que vous montrez à vos collègues ne va vraisemblablement pas provoquer la réaction que vous attendiez. Cela peut être frustrant mais c'est tant mieux. Le but de faire une démonstration est de recueillir des opinions alternatives. Le prototype a peu de chance de viser juste du premier coup. J'étais content de démontrer notre idée avec 9 blocs (3 entrées, 3 blocs de traitement, 3 sorties) car cela suffisait pour illustrer l'idée. Nous nous avions évoqué la possibilité de coder 30 blocs mais cela aurait eu plusieurs effets négatifs :

- diluer l'impact de la démonstration avec des détails superflus et non réfléchis ;
- augmenter le temps dédié sans retour de parties prenantes ;
- provoquer un surinvestissement dans le prototype.

Étant fervent partisan de l'agilité (au sens agile manifesto pas au  #buzzword), je pense qu'il est critique d'avoir des retours le plus tôt et donc d'organiser une démonstration le plus tôt possible. Mon collègue a organisé une super démonstration avec de la musique country :cowboy: pour attirer du monde et cela a eu son petit effet.

## Travailler en binôme c'est bien

Je suis déjà convaincu par le fait qu'on va toujours plus loin à plusieurs pour le code avec la programmation par paire. Au delà de l'aspect très sympa de travailler avec quelqu'un avec qui on a peu l'habitude de travailler, cela s'est avéré efficace pour le prototype :

- au niveau des compétences - la partie UI me paraissait complexe et la partie exécution abordable. C'était la perception opposée pour mon collègue;
- au niveau motivation - les progrès réalisés sur l'UI m'ont boosté pour finaliser l'implémentation (pas si simple cf. [précédent billet]({{< ref "/post/fr/2021-07-27-prototype-nocode-application-2.md" >}}) du moteur d'exécution;
- cela donne du courage et du recul pour la démonstration.

## Conclusion 

Voilà c'est la conclusion (provisoire sur ce prototype et définitive de ce billet). Nous verrons bien comment les choses évoluent et j'espère pouvoir vous reparler de ce prototype et ses évolutions. Si vous voulez partager vos projets personnels et les enseignements que vous en avez tiré, partagez les à moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/). 
