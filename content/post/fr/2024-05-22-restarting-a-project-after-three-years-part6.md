---
title: "Reprendre un projet après 3 ans - partie 6"
date: "2024-05-23"
draft: false
description: "DNS, TLS et corrections de bugs"
tags: ["projet perso","php","jeu"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série :

- [le premier sur les étapes pour réussir à reconstruire le logiciel]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years" >}}) ;
- [le deuxième sur l'ajout de fonctionnalité]({{< ref "/post/fr/2022-04-13-restarting-a-project-after-three-years-part2" >}}) ;
- [le troisième sur le bilan sur le logiciel]({{< ref "/post/fr/2022-04-22-restarting-a-project-after-three-years-part3" >}}) ;
- [le quatrième sur les leçons que j'en ai tiré]({{< ref "/post/fr/2022-05-11-restarting-a-project-after-three-years-part4" >}}) ;
- [le cinquième sur la situation un an après]({{< ref "/post/fr/2023-06-07-restarting-a-project-after-three-years-part5" >}}).

Ces derniers temps, j'ai passé beaucoup de temps à la réécriture de mon logiciel de gestion de tournoi. Mais à mesure que le tournoi approchait, il apparaissait évident que le nouveau logiciel serait encore trop immature pour gérer convenablement le tournoi. Pour assurer le bon déroulement de mon tournoi et poursuivre le développement du nouveau logiciel avec plus de sérénité, j'ai redéployé mon ancien logiciel. 

J'ai profité de l'occasion pour corriger certains défauts de l'ancien logiciel.

```mermaid
graph LR;
    users([Utilisateurs])-->reverseproxy[Reverse proxy]
    reverseproxy-->front[Application utilisateur]
    front-->reverseproxy
    reverseproxy-->api[API REST]
    orga([Organisateur])-->reverseproxy
    reverseproxy-->admin[Application d'administration web]
    api-->db[(Base de données - MariaDB)]
    admin-->db
```

La répartition des technologies est la suivante :

- L'application utilisateur est une application monopage en AngularJS ;
- L'API REST est un bundle Symfony3 ;
- L'application d'administration est un bundle Symfony3 ;
- [Traefik(https://traefik.io/)](https://traefik.io/)joue le rôle de reverse proxy pour orienter le traffic vers le service adéquate en fonction de l'url.

## Caddy + DNS = TLS 🔒

J'ai lu un article intitulé ["Stop going to the cloud and getting scammed. $200 infra to serve your startup till 100k monthly users in 15 minutes. Self-hosted Postgres, caddyserver and docker-compose FTW."(https://grski.pl/self-host)](https://grski.pl/self-host) et cela m'a convaincu d'utiliser [Caddy](https://caddyserver.com/) en tant que reverse proxy. Traefik est très adapté aux architectures dynamiques. En réalité, mon cas d'usage est très simple : un reverse-proxy vers trois services. Les points forts caddy sont la simplicité et le support automatique de TLS.

Si vous disposez d'un nom de domaine et que vous créez deux enregistrements DNS (nécessaires pour que les utilisateurs puissent accéder à votre service), vous pouvez obtenir un certificat pour sécuriser le trafic vers votre service. Le certificat est délivré par [Let's Encrypt](https://letsencrypt.org/) ou par [ZeroSSL](https://zerossl.com/).

Quand je dis que cela est plus simple, voici la configuration de Traefik :

```yaml
http:
  routers:
    client:
      rule: "PathPrefix(`/live`)"
      middlewares:
        - adaptToLive
      service: client
    api:
      rule: "PathPrefix(`/ws`)"
      middlewares:
        - adapt2Symfony
      service: ws
    admin:
      rule: "PathPrefix(`/admin`)"
      middlewares:
        - adapt2Symfony
      service: admin
  middlewares:
    adaptToLive:
      stripPrefix:
        prefixes:
          - "/live"
    adapt2Symfony:
      addPrefix:
        prefix: "app.php"
  services:
    client:
      loadBalancer:
        servers:
        - url: http://webclient
    ws:
      loadBalancer:
        servers:
        - url: http://webserver
    admin:
      loadBalancer:
        servers:
        - url: http://webserver
```

Voici la configuration Caddy avec HTTPS 🥰 :

```plain
{$FQDN}:443

handle_path /live/* {
        reverse_proxy webclient:80
}
handle /ws/* {
        rewrite * /app.php{uri}
        reverse_proxy webserver:80
}

handle /admin/* {
        basicauth {
                admin1 {$ADMIN1_PASSWORD}
                admin2 {$ADMIN2_PASSWORD}
        }
        rewrite * /app.php{uri}
        reverse_proxy webserver:80
}
```

C'est plus court, non ?

## Déblocage sur Symfony3

Comme je l'ai mentionné précédemment, il n'est pas possible de reconstruire la partie Symfony3 à cause de l'obsolescence de Symfony3 et de l'impossibilité de créer le projet avec `composer`. Or, cette année, il y avait une modification de l'ordre des critères de classement par équipe était nécessaire. Contrairement à ma future application, cette modification implique de changer le code de mon bundle `core`.

Heureusement, j'avais précédemment créé une image Docker et je l'avais publiée sur Docker Hub. J'ai utilisé cette image comme image de base puis j'ai copié les sources de l'application. J'ai ainsi pu obtenir un conteneur fonctionnel avec les sources mises à jour.

```dockerfile
FROM docker.io/trambi/fantasyfootball_webserver:18.2
WORKDIR /var/www/html
COPY --chown=www-data:www-data files_to_serve ws
WORKDIR /var/www/html/ws
RUN composer dump-autoload
RUN chown -R www-data:www-data ./
```

## Correction sur le front-end

Pour la partie front-end de l'application, dont je ne maîtrise pas la technologie, j'ai choisi d'adopter pour une stratégie de correctifs. Le principe consiste à apporter des modifications ciblées aux fichiers générés lors de la compilation de l'application Angular. Les modifications sont peu étendues mais comme l'application est bien pensée, il est possible d'obtenir des résultats intéressants avec quelques modifications seulement.

Pour mettre en œuvre cette stratégie, j'ai choisi d'utiliser l'outil sed de manière ciblée.

```bash
sed -i s/localStorage/sessionStorage/ /usr/share/nginx/html/scripts/services.parameters.js
```

Cette commande permet de remplacer le stockage persistant vers un stockage de session. Avec 6 modifications ciblées, on peut modifier l'expérience.

## Conclusion

J'ai choisi de déployer la solution avec une machine virtuelle, de manière classique pour reprendre l'article cité dans la partie sur Caddy.

L'implémentation actuelle du logiciel continue de fonctionner correctement. Le fait d'avoir pu déployer et corriger les petites erreurs m'a permis de prendre le temps d'avancer sur la nouvelle version. J'ai bien avancé sur la nouvelle version, je vous en dirai plus sur le sujet prochainement...

Si vous avez des conseils sur la situation, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

Ce billet fait partie d'une série :

- [le premier sur les étapes pour réussir à reconstruire le logiciel]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years" >}}) ;
- [le deuxième sur l'ajout de fonctionnalité]({{< ref "/post/fr/2022-04-13-restarting-a-project-after-three-years-part2" >}}) ;
- [le troisième sur le bilan sur le logiciel]({{< ref "/post/fr/2022-04-22-restarting-a-project-after-three-years-part3" >}}) ;
- [le quatrième sur les leçons que j'en ai tiré]({{< ref "/post/fr/2022-05-11-restarting-a-project-after-three-years-part4" >}}) ;
- [le cinquième sur la situation un an après]({{< ref "/post/fr/2023-06-07-restarting-a-project-after-three-years-part5" >}}).
