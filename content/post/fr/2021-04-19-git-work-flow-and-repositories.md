---
title: "Git : workflow et dépôts"
date: "2021-04-19"
draft: false
description: "où on l'on se pose des questions sur nos manières de travailler"
tags: ["git","workflow","monorepository"]
author: "Bertrand Madet"
---

Aujourd'hui, je souhaite partager sur la manière de travailler de mon équipe et de l'évolution de notre stratégie de définition d'un dépôt Git.

## Contexte

Le service sur lequel je travaille est structuré avec une dizaine de dépôts Git. Il y a des services Web en React, un catalogue de composants React et des applications pour gérer les back-office. La bibliothèque de composants permet de partager les composants utilisés dans nos différentes applications Web React.

```mermaid
graph BT
  webapp1[Application web React 1]-->|Utilise des composants|catalog[Catalogue de composants]
  webapp2[Application web React 2]-->|Utilise des composants|catalog
```

## Le problème avec cette organisation

Pour chaque dépôt Git, nous utilisons le flux de travail Git, classique (dit *feature branch*) suivant :

- Nous développons la fonctionnalité sur une branche ;
- La fonctionnalité est validée ;
- Le développement est revu par les autres développeurs ;
- La branche est fusionnée dans la branche principale.

Ce flux de travail est agréable et efficace quand une fonctionnalité modifie un dépôt.

Si la fonctionnalité dépend de deux dépôts, cela devient un peu compliqué. Dans le meilleur des cas, cela peut se représenter par le diagramme de séquence suivant :

```mermaid
sequenceDiagram
  participant dev as Développeur
  participant catalog as Catalogue de composants
  participant wa1 as WebApp1
  participant valid as Validation
  dev->>+catalog: Création d'une branche de développement
  dev-->>catalog: développement
  dev-->>catalog: Création d'une version provisoire
  dev->>+wa1: Création d'une branche de développement
  dev-->>wa1: Utilisation de la version intermédiaire du catalogue
  dev->>+valid: Tu peux valider ça ?
  valid->>-dev: C'est ok
  dev->>catalog: Création d'une version finale
  catalog-->>-dev: Branche fusionnée dans la branche principale
  dev-->>wa1: Utilisation de la version finale du catalogue
  wa1-->>-dev: Branche fusionnée dans la branche principale
```

Je vous laisse imaginer quand une fonctionnalité dépend de trois dépôt comme le catalogue de composant et deux applications Web.

J'y vois plusieurs inconvénients :

- :-1: les revues de code n'aident pas à garder une cohérence entre les composants - par exemple, pour la même fonctionnalité un développeur peut valider un changement sur le catalogue et un autre peut valider un changement incohérent sur l'application Web 1 ;
- :-1: les branches de travail peuvent durer plus longtemps ;
- :-1: on peut se retrouver avec une fonctionnalité dans la branche principale dans un des services Web et pas dans un autre service Web alors même que l'on veut proposer les deux mises à jours en même temps aux utilisateurs.

Est-ce que le flux de travail est en cause ? Je ne crois pas, je pense que c'est un problème de découpage des dépôts Git.

## La définition des contours d'un dépôt Git

Git est un outil agnostique et il ne préconise pas de stratégie pour définir ce qui constitue un dépôt. Il y a une tradition côté des logiciels libres de faire un dépôt par composant (exécutable ou bibliothèque) et à l'opposé l'approche "monorépo" d'un Google ou d'un Netflix (même si Google n'utilise pas Git comme système de gestion de version).

Historiquement dans mon entité, nous utilisons un dépôt Git par exécutable. On l'a vu pour notre service, ce n'est pas optimal en terme d'efficacité. J'ai tendance à penser qu'utiliser le même flux mais avec un seul dépôt Git pour tous les composants de notre service permettrait de simplifier ce flux de travail, éviter les aller-retours.

Cela paraît logique car même si les composants logiciels sont différents, nous essayons de procurer une expérience utilisateur homogène et cohérence : Les utilisateurs voient par exemple une version `V21` de notre service et pas une version `Va12` de l'exécutable a, une version `Vb17` de l'exécutable b... Le fait d'utiliser un seul dépôt Git permettrait d'assurer au niveau de la gestion de version au moins la cohérence du service.

## Conclusion

Passer d'une dizaine de dépôts à un ne va pas se faire en un jour. Il faut prévoir de migrer progressivement les composants (exécutables ou bibliothèques) vers un dépôt unique. Les impacts les plus grands seront sans doute sur l'intégration continue. Certains aspects du déploiement continue seront plus simples comme l'utilisation des fichiers de description de services (qui aujourd'hui sont dispersés entre différents dépôts).

Cela va être un processus long et j'essayerai de rendre compte des évolutions de la situation.

Si vous avez des retours sur la gestion de vos dépôts de vos projets professionnels ou personnels, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
