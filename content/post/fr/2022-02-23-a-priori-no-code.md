---
title: "A priori sur le no-code"
date: "2022-02-23"
draft: false
description: "et surtout les questions que ce concept me pose"
tags: ["nocode"]
author: "Bertrand Madet"
---

Une fois n'est pas coutume, je vous propose un billet d'opinion sur le no-code. Je connais peu (voir pas) le no-code mais je pense que cela peut-être intéressant d'avoir une vue d'un codeur sur le no-code.

## A priori

### Le no code est complémentaire du code

Je pense qu'il a beaucoup de cas d'usage qui gagnerait à être automatisé. Il n'y a pas assez de développeurs pour tous ces cas. Et le no code permet à des personnes d'automatiser des tâches à l'être. En plus, le no code permet d'automatiser avec des compétences différences de celles requises pour le développement.

### Le no code est moins libre que le développement avec code

De ma faible connaissance, j'ai l'impression que les solutions no code sont en général :

- exécutées en SaaS (**S**oftware **a**s **a** **S**ervice);
- peu interopérables.

Le SaaS est de mon point de vue (peut-être trop attaché aux logiciels libres) contradictoire avec les libertés inhérentes au logiciel libre. En effet, la configuration no code (on viendra sur cette notion) ne peut pas s'exécuter où l'on souhaite.

## Questions

### Comment appelle-t-on la configuration du no code ?

S'il n'y a pas de code, comment appelle la configuration de l'outil no code ? La `configuration` ? Le `no code` ? Pour la suite, j'utiliserai le terme `configuration` (même si j'ai un doute)

### Comment gère-t-on l'historique de la configuration ?

J'imagine que le développement no code n'est pas en mode big bang (de rien à toutes les fonctionnalités) et que l'on peut ajouter des fonctionnalités petit à petit.

Comment gère-t-on alors l'historique de cette évolution ? En développement avec code, on dispose d'outils formidables de gestion comme `git` (ou `mercurial`) qui permettent de gérer très finement l'historique du code. Mais comment fait-on en no code ?

J'utilise que cela dépend des outils no code et des exports possibles de configuration, si on peut exporter la configuration dans un format texte, on peut alors utiliser les mêmes outils que le code.

### Peut-on tester automatiquement sa configuration ?

Dans la mesure, on l'on souhaite automatiser des tâches, il faut pouvoir s'assurer que les tâches sont bien remplies dans la durée. Il me semble donc important de tester l'exécution de sa configuration automatiquement.

Une question complémentaire : Peut-on tester en no code une configuration no-code ? :exploding_head:

### Autres questions

J'ai d'autres questions qui dépendent j'imagine des outils sélectionnés.

- Quelle est la licence d'un outil développé avec une solution no code ?

- Est-ce qu'il existe des solutions d'intégration et de déploiement continues ?

- Est-ce possible de définir des environnements ?

## Conclusion

Voici donc les questions très orientées code d'un développeur sur le no code. Si vous souhaitez lire des billets plus orienté  no code, vous pouvez par exemple aller sur [le blog d'une amie (https://re-buildall.weebly.com/)](https://re-buildall.weebly.com/). Elle s'est lancé le défi d'écrire un article par jour, et je suis sûr que des retours pourrait l'aider à aller au bout de ce pari.

Si vous avez des réponses à mes questions et que vous souhaitez en discuter, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
