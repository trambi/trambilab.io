---
title: "Utilisation de Node.js en entreprise - partie 0"
date: "2021-03-08"
draft: false
description: "ou comment installer Node.js sans être admin"
tags: ["nodejs","entreprise","admin"]
author: "Bertrand Madet"
---

Dans le [billet précédent - Utilisation de Node.js en entreprise - partie 1]({{< ref "/post/fr/2021-02-28-utilisation-nodejs-en-entreprise-part-1.md" >}}), je vous expliquais comment faire en sorte que Node.js vérifie les certificats signés par des autorités de certification spécifiques. Je suis aperçu que je n'ai pas commencé par le commencement, à savoir comment installer Node.js sous Windows sans avoir de compte administrateur.

# Node.js sous Windows

C'est assez simple, il faut :
 - télécharger la version zip pour Windows ;
 - extraire l'archive dans un répertoire accessible par exemple le répertoire `bin` dans votre répertoire personnel.
 - modifier la variable d'environnement PATH pour inclure le répertoire d'extraction de l'étape précédente.

Il faut fixer la variable d'environnement PATH pour Windows et pour GitBash.
Pour éditer les variables d'environnement sous Windows pour un utilisateur, on peut ouvrir l'éditeur on peut taper sur les touches `Windows+R` puis `rundll32 sysdm.cpl,EditEnvironmentVariables`.

Pour GitBash, il faut ajouter la ligne suivante à la fin du fichier `~/.bashrc`.

```bash
export PATH=~/bin/node12:${PATH}
```
## Et Deno

Pour [Deno (https://deno.land/)](https://deno.land/), c'est vraisemblablement la même manipulation. Il est possible de télécharger une version zip pour Windows et ensuite modifier la variable `PATH`.

## Conclusion

Un gros merci à Abdel Raoof qui a publié un [billet de blog (http://abdelraoof.com/blog/2014/11/11/install-nodejs-without-admin-rights/)](http://abdelraoof.com/blog/2014/11/11/install-nodejs-without-admin-rights/) en anglais qui explique la manipulation de la variable d'environnement Windows. J'espère que ce partage d'expérience vous aidera à installer Node.js (ou à le mettre à jour). Si vous avez des retours ou d'autres conseils, n'hésitez pas à les partager sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
