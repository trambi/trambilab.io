---
title: "Terraform: Up and Running - Fiche de lecture"
date: "2024-10-31"
draft: false
description: "Troisième édition du livre sur Terraform d'Yevgeniy Brikman"
tags: ["fiche de lecture","outils","terraform"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Ce livre s'adresse aux personnes qui souhaitent utiliser Terraform ou déployer leur infrastructure à partir du code - **I**nfrastructure **a**s **C**ode 🇬🇧. 

## Ce qu'il faut retenir

Terraform est un outil qui permet de déployer une  infrastructure à partir de la description de cette infrastructure. L'outil permet aussi de détruire et de mettre à jour l'infrastructure. C'est un outil très puissant et difficile à maîtriser. 

Pour permettre de compartimenter le code, on utilise des modules - ensemble de code réutilisable. La maintenabilité du code Terraform passe par l'utilisation des modules.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Très complet de l'introduction de l'*IaC* à l'utilisation de Terraform en équipe  | :-1: Sans doute trop pour un débutant en Terraform |
| :+1: Aspect technique pris en compte mais aussi la collaboration ou la structuration d'un dépôt ||
| :+1: La partie sur les tests des modules est très complète.||

En ayant commencé à travailler avec Terraform il y a deux ans, je trouve que c'est un très bon livre. Car en plus de reposer les bases, j'ai appris beaucoup de choses. C'est la troisième édition et on sent une grande maturité malgré le côté relativement nouveau et très dynamique de Terraform. La structuration du dépôt m'a déboussolé au début mais en réfléchissant bien, elle paraît adaptée au problématique de gestion de différents environnements (développement, qualification, préproduction et production). 

La partie sur les tests est très détaillée et permet d'écrire des tests mais donne aussi des conseils sur les différents comptes à utiliser pour les différents types de test. La partie de secrets permet aussi de comprendre que si les secrets sont injectés dans lors du `Terraform plan` alors les secrets seront lisibles dans le plan. Une raison supplémentaire pour considérer l'utilisation de gestion d'autorisation temporaire "à la cloud" comme un avantage supplémentaire.

Pour les débutants, cela fait beaucoup de choses à appréhender. Il est préférable de faire les exercices de description d'infrastructure au fur et à mesure. Il est à avis plus efficace d'essayer de déployer une infrastructure d'un projet personnel après le chapitre 4 sur les modules. Pour ensuite, continuer le livre.

## Détails

- Titre : Terraform: Up & Running - Writing Infrastrcutre as Code
- Auteur : Yevgeniy Brikman
- Langue : 🇬🇧
- ISBN : 978-1098116743
