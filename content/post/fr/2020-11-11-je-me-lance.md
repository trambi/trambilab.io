---
title: "Je me lance"
description: ""
tags: ["blog"]
date: "2020-11-11"
author: "Bertrand Madet"
---
J'ai fini la lecture de `The Software Craftsman: Professionalism, Pragmatism, Pride`, un livre de Sandro Mancuso - [fiche de lecture]({{< ref "/post/fr/2020-11-11-craftman-software-lecture.md" >}}). C'est une lecture très inspirante et j'ai été convaincu par l'utilité de publier un blog sur mes expériences de développement logiciel et certains sujets connexes.
J'ai déjà publié des articles sur LinkedIn, mais je trouve la plateforme pas assez ouverte - il faut un compte LinkedIn pour les lire.

Je vais donc utiliser ce blog pour partager mes expériences sur le développement logiciel, les bases de données, l'intégration continue... L'idée est de proposer des fiches de lecture, des coups de coeur, des résumés autour de ces thèmes.

Et j'espère que cela vous plaira. Dans un premier temps, je vais proposer des articles en français.

Bonne lecture !