---
title: "Refactoring - Fiche de lecture"
date: "2021-07-06"
draft: false
description: "Le livre de Martin Fowler qui vous fait comprendre que vous faisiez pas de refactoring avant"
tags: ["fiche de lecture","refactoring"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

A celles et ceux qui veulent améliorer la qualité de leur code de manière continue en suivant un guide pratique par un des rédacteurs du manifeste Agile  : [Martin Fowler (https://martinfowler.com/)](https://martinfowler.com/).

## Ce qu'il faut retenir

- La restructuration du code est le fait d'améliorer le code. Le refactoring est un sous-ensemble de la restructuration de code basée sur des petites modifications ;
- L'avantage de l'aspect itératif est qu'il est possible d'intégrer ce processus dans vos développements et donc d'améliorer continûment la qualité du code ;
- Cette pratique nécessite un usage intensif et systématique des tests et de gestion de configuration.

La séquence de refactoring est celle-ci :

```mermaid
stateDiagram-v2
    oneRefactoringStep: Appliquer un refactoring
    testsSuccessed : S'assurer que les tests passent
    revert: Annuler les changements
    commit: Enregistrer le changement
    [*] --> oneRefactoringStep
    oneRefactoringStep --> testsSuccessed
    testsSuccessed --> commit: Les tests passent
    testsSuccessed --> revert: Un des tests échouent
    commit --> [*]
    revert --> [*]
```

Où un refactoring est une restructuration élémentaire de code comme :

- Extraire une fonction ;
- Incorporer une fonction ;
- Renommer une variable...

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Le livre est pratique et utilisable| :-1: Le catalogue des refactoring est énorme et indigeste à la lecture linéaire|
| :+1: le catalogue des refactoring est très pratique pour la consultation| :-1: Le premier chapitre n'est très lisible (en tout cas avec ma liseuse) même s'il illustre parfaitement la méthode.|
| :+1: Le chapitre sur les tests reprend la méthode pratique pour ajouter des tests||

Ce livre m'a permis de me rendre compte que je ne faisais pas de refactoring mais des restructurations de code au long terme. Récemment j'ai pris la peine d'appliquer la méthode et là où ma restructuration avait échoué la veille au bout de trois heures, l'approche incrémentale a abouti en trois ou quatre itérations (et une heure). 

L'approche peut être parfaitement intégrée au TDD ([mon billet sur le TDD : TDD en 3 phrases]({{< ref "/post/fr/2021-06-15-tdd-en-3-phrases.md" >}})). Pour ne rien gâcher, il y a une bibliographie riche avec des articles et des livres très intéressants.

## Détails

En anglais :gb: :

- Refactoring: Improving the Design of Existing Code de Martin Fowler avec l'aide de Kent Beck
- ISBN-13: 978-0134757599

En français :fr: :

- Refactoring: Comment améliorer le code existant de Martin Fowler avec l'aide de Kent Beck
- ISBN-13: 978-2100801169

*edit du 11 juillet 2021 : ajout d'un diagramme d'état.*
