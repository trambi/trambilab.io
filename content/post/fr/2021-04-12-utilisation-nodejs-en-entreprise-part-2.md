---
title: "Utilisation de Node.js en entreprise - partie 2"
date: "2021-04-12"
draft: false
description: "le cas des bibliothèques avec binaires"
tags: ["nodejs","entreprise","npm","typescript","binaire"]
author: "Bertrand Madet"
---

Après l'[installation de Node.js (partie 0)]({{< ref "/post/fr/2021-03-08-utilisation-nodejs-en-entreprise-part-0.md" >}}) et [l'utilisation des certificat (partie 1)]({{< ref "/post/fr/2021-02-28-utilisation-nodejs-en-entreprise-part-1.md" >}}), nous allons nous attaquer aux bibliothèques npm avec binaires. Certaines bibliothèques JavaScript (c'est aussi les cas de certaines bibliothèques Python) utilise des binaires. En général, ces binaires sont utilisés pour des améliorer les performances. Il y a deux problèmes :

 - Les binaires sont spécifiques au système d'exploitation ;
 - Les binaires ne sont pas inclus avec le paquet npm.

Je vais prendre le cas de deux bibliothèques pour illustrer les problèmes et les moyens de les utiliser en entreprise.

## Node-sass

node-sass est une bibliothèque qui permet de faire de programmer les feuilles de style CSS. Dans le fonctionnement classique, la bibliothèque va chercher à télécharger le binaire adapté au système d'exploitation et l'architecture depuis `https://github.com/sass/node-sass/release/download`. Les ennuis commencent si les développeurs ou la CI n'ont pas accès à GitHub.

Heureusement en fouillant dans le script d'installation `script/install.js` et le fichier utilitaire`lib/extensions.js` du paquet ou dans la partie **Binary configuration parameters** du fichier d'information `README.md`, on trouve d'options pour modifier ce comportement. Comme je trouve que les explications ne sont pas très claires, je me permets d'expliquer ce que j'ai compris.

|Variable d'environnement|Configuration npm|Argument d'installation|Usage|
|---|---|---|---|
|SASS_BINARY_NAME|sass_binary_name|--sass-binary-name|Indique le nom du fichier binaire|
|SASS_BINARY_SITE|sass_binary_site|--sass-binary-site|Indique l'url de base de téléchargement du binaire (sera suivi de `/v<version>/<binary_name>`)|
|SASS_BINARY_PATH|sass_binary_path|--sass-binary-path|Indique le chemin local où se trouve le fichier binaire|
|SASS_BINARY_DIR|sass_binary_dir|--sass-binary-dir|Indique le chemin local où se trouve le répertoire qui contient les fichiers binaires|

On trouve aussi une variable d'environnement `SKIP_SASS_BINARY_DOWNLOAD_FOR_CI` qui permet de ne pas télécharger le binaire.

Dans nos projets, nous utilisons la variable d'environnement `SASS_BINARY_PATH`. Mettre un binaire dans un dépôt Git est rarement une bonne idée (cf. [le post précédent]({{< ref "/post/fr/2021-04-04-appris-en-cassant-un-depot-node-js" >}})) et à la lecture du code source, il serait plus simple de définir l'url de base avec l'url de notre gestionnaire d'artefact et comme l'utilisation des variables d'environnement avec GitBash et Node.js sous Windows soulève toujours des questions, il serait bien de configurer cela dans un fichier de configuration de npm.

```.npmrc
sass_binary_site=https://gestionnaire-artefact.internal.corpo/
```

## Sharp

Sharp est un bibliothèque qui s'interface avec la bibliothèque de manipulation d'image `libvips`.
La documentation de la bibliothèque est mieux et il y a une partie dédiée [Custom prebuilt binaries (https://sharp.pixelplumbing.com/install#custom-prebuilt-binaries)](https://sharp.pixelplumbing.com/install#custom-prebuilt-binaries)

|Configuration npm|Usage|
|---|---|
|sharp_binary_host|URL où télécharger le binaire précompilé sharp|
|sharp_local_prebuilds|Chemin local où récupérer les binaires précompilés sharp ou libvips|
|sharp_libvips_binary_host|URL où télécharger le binaires précompilés libvips|

J'ai pu découvrir que npm propose un mécanisme pour passer en variable d'environnement les paramètres de configuration npm, il suffit de préfixe la variable par `npm_config` en remplaçant les tirets par les underscores.

Actuellement, nous utilisons pas ce mécanisme actuellement, nous recompilons les deux bibliothèques ce qui prend beaucoup de temps.

## Conclusion

Comme on peut le voir avec deux bibliothèques, il n'y a pas de recette miracle, à part lire la documentation (et parfois le code source) des paquets.

J'espère que ce partage d'expériences vous aideront à utiliser TypeScript (et JavaScript) dans un réseau d'entreprise. Si vous avez des retours ou d'autres conseils, n'hésitez pas à les partager sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
