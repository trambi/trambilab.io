---
title: "J'ai créé une page web à l'ancienne"
date: "2021-06-29"
draft: false
description: "sans Node.js, sans babel, sans React"
tags: ["javascript","vanilla javascript","old-school","front-end"]
author: "Bertrand Madet"
---

Après ma mésaventure avec Gatsby, je me suis pas mal interrogé sur les origines de l'outillage "moderne" du développeur front-end. Ou pourquoi avant de commencer à coder un blog, il faut télécharger 300 paquets (et 500 Mio). J'ai donc décidé de développer une petite application ou une page web munie de mon navigateur favori (Firefox) et de mon IDE (Visual Code) sans `Node.js`, sans `babel` et sans `React`.

## Le contexte

Comme vous le savez déjà, j'adore un jeu de plateau nommé BloodBowl et une nouvelle version est sortie. Les logiciels que nous utilisons sont plutôt "à l'ancienne" et pas adaptés à cette nouvelle version. J'ai donc décidé de proposer une application qui aide à créer une nouvelle équipe.

## La mise en place

J'ai opté pour la hiérarchie de fichiers suivante :

- `index.html`, un fichier HTML avec du contenu sémantique - `article`, `details`, `summary` ...;
- `style.css`, un fichier CSS avec très (trop ?) peu de chose ;
- `script`, un répertoire avec des fichiers JavaScript.

Pour avoir un comportement identique à celui d'un site web, j'ai utilisé `python -m http.server` depuis le répertoire courant. J'aurai pu utiliser un serveur http `Node.js` ou go.

## Les tests

J'étais parti pour utiliser [Mocha.js (https://mochajs.org/)](https://mochajs.org/) avec les tests dans un fichier HTML `test.html` (<https://mochajs.org/#running-mocha-in-the-browser>) mais j'ai eu du mal à gérer les imports et les exports. Je me suis rabattu sur [QUnit (https://qunitjs.com/)](https://qunitjs.com/) qui a la même capacité de tester dans un fichier HTML (<https://qunitjs.com/intro/#in-the-browser>) et qui m'a donné satisfaction.

## L'architecture

J'ai commencé par un code spaghetti. J'ai ensuite codé mes classes de données en TDD (test driven development cf. [ma synthèse en 3 phrases]({{< ref "/post/fr/2021-06-15-tdd-en-3-phrases.md" >}})). J'ai découpé les fonctions en suivant le pattern modèle-vue-contrôleur.

```mermaid
graph LR
    contrôleur-->vue
    contrôleur-->modèle
```

Dans cette architecture ([voir la page sur Wikipédia](https://fr.wikipedia.org/wiki/Mod%C3%A8le-vue-contr%C3%B4leur)) : 

- la vue permet d'afficher l'interface graphique - `index.html` et `script/vue.js` ;
- le modèle permet de représenter les objets métier - `script/modeles.js`;
- le contrôleur coordonne le tout et contient la logique métier `script/controller.js`.

Je ne suis pas sûr d'avoir une implémentation canonique de cette architecture mais cela m'a permis de structurer le code.

## La publication

J'ai utilisé git et [Gitlab (https://gitlab.com)](https://gitlab.com) pour la gestion de sources. Après avoir déplacé les fichiers dans le répertoire `public`, j'ai utilisé la fonctionnalité pages avec le template [plain-html (https://gitlab.com/pages/plain-html)](https://Gitlab.com/pages/plain-html) pour publier l'application. C'était hyper simple et cela m'a motivé pour mettre le projet sur Gitlab.

## Les enseignements

L'ajout d'éléments en JavaScript est très fastidieux (base de `document.createElement` et `appendChild`). Pour avoir pratiqué le développement en React.js, la syntaxe `JSX` apporte un vrai plus en terme d'expressivité.

Le formatage et l'analyse statique de code sont effectués grâce aux extensions de Visual Code (qui est une plateforme JavaScript intégrée), il faudrait voir avec le temps. Je n'ai effectué des tests que sur les modèles de données et ils ne sont pas intégrés avec Gitlab.

La problématique des imports et des exports qui doit être gérée par le bundler n'a pas été trop forte. Cela pourrait se compliquer en ajoutant des fonctionnalités, du code et donc des fichiers.

## Conclusion

Ce petit projet m'a permis de comprendre les problématiques gérées par l'outillage "JavaScript moderne". En plus de l'intérêt du logiciel, l'analyse de l'évolution dans le temps m'incite à continuer dans la même esprit en ajoutant des fonctionnalités. L'application est disponible [https://trambi.gitlab.io/teamcreator](https://trambi.gitlab.io/teamcreator).

Si vous voulez participer au développement, les contributions sont bienvenues sur Gitlab : (<https://gitlab.com/trambi/teamcreator>) et si vous avez des idées pour me faciliter le développement, partagez les à moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
