---
title: "Présentation de mon projet perso du moment"
date: "2023-11-21"
draft: false
description: "c'est plus simple de commencer par là"
tags: ["REST API", "projet perso","Go"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série :

- [Masquer les détails d'implémentation d'une API REST]({{< ref "/post/fr/2023-11-09-rewrite-side-project-api-in-go-part1" >}}) ;
- [Ajout de logique métier dans une API REST - classement des joueurs]({{< ref "/post/fr/2023-12-30-rewrite-side-project-api-in-go-part2" >}}) ;
- [Ajout de logique métier dans une API REST - calcul des points]({{< ref "/post/fr/2024-01-09-rewrite-side-project-api-in-go-part3" >}}) ;
- [Confronter une API REST à l'existant]({{< ref "/post/fr/2024-04-17-rewrite-side-project-api-in-go-part4" >}}).

Suite à mes articles sur les API REST, j'ai décidé de vous présenter plus en détail mon projet perso du moment, une API REST compacte écrite en Go.

## Contexte

Pour rappel, l'API REST en question est la réécriture complète en Go du logiciel de tournoi dont je vous ai parlé dans la série : `Reprendre un projet après 3 ans` [premier billet]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years.md" >}}). La description au format OpenAPI de l'API cible est [ici (https://gitlab.com/trambi/fantasyfootballapi/-/blob/main/api/target-api-openapi3.json)](https://gitlab.com/trambi/fantasyfootballapi/-/blob/main/api/target-api-openapi3.json).

Le principe de cette API REST est de gérer un tournoi, les participants et les matchs associés.

La solution précédente ressemblait à ça :

```mermaid
graph LR;
    users([Utilisateurs])-->front[Application utilisateur v1 - JavaScript + Angular]
    front-->api[API REST v1 -  PHP + Symfony3]
    orga([Organisateur])-->admin[Application d'administration web v1 - PHP + Symfony3]
    api-->db[(Base de données - MariaDB)]
    admin-->db
```

L'idée est de faire une solution plus modulaire et facilement installable qui ressemble à ça :

```mermaid
graph LR;
    users([Utilisateurs])-->front[Application utilisateur v2- JavaScript + ?]
    front-->api[API REST v2 - Go + gorilla/mux]
    orga([Organisateur])-->admin[Application d'administration web v2 - JavaScript + ?]
    admin-->api
    api-->db[(Base de données - Sqlite ou PostgreSQL)]
```

J'ai commencé par l'API REST. Ce qui pourrait me permettre de tester avec l'application utilisateur v1 et de migrer gentiment vers l'application utilisateur v2 plus tard avec cet état intermédiaire :

```mermaid
graph LR;
    users([Utilisateurs])-->front[Application utilisateur v1 - JavaScript + Angular]
    front-->api[API REST v2 - Go + gorilla/mux]
    orga([Organisateur])-->admin[Application d'administration web v2 - JavaScript + ?]
    admin-->api
    api-->db[(Base de données - Sqlite ou PostgreSQL)]
```

## Organisation du code

### Arborescence

L'idée est d'utiliser un seul dépôt pour gérer les deux applications web et l'API REST. J'ai commencé à implémenter l'API REST en suivant cette arborescence :

- `api`, ce répertoire contient la documentation de l'API cible, l'API actuellement développée et la liste des endpoints utilisés par l'application utilisateur v1;
- `cmd`, ce répertoire contient le code de l'application en ligne de commande pour lancer l'API REST ;
- `cover`, ce répertoire vide est destiné à accueillir les fichiers de couverture de test créés par l'intégration continue ;
- `fridge`, ce répertoire contient le code source de ma première tentative d'implémentation, il est destiné à disparaître ;
- `internal`, ce répertoire contient les paquets internes Go utilisés par l'application :
  - `api`, ce répertoire contient le paquet du même nom qui gère les endpoints,
  - `appinfo`, ce répertoire contient le paquet du même nom qui affiche la version de l'application,
  - `auth`, ce répertoire contient le paquet du même nom qui gère l'authentification et la délivrance de jeton JWT,
  - `core`, ce répertoire contient le paquet du même nom qui gère les objets métier, pour l'instant Edition, Coach et Game,
  - `logger`, ce répertoire contient le paquet du même nom qui gère les journaux,
  - `storage`, ce répertoire contient le paquet du même nom qui gère le stockage en base de données ;
- `tools`, ce répertoire contient le script de calcul de couverture.

Je me suis inspiré du dépôt [Standard Go Project Layout (https://github.com/golang-standards/project-layout/blob/master/README_fr.md)](https://github.com/golang-standards/project-layout/blob/master/README_fr.md). A terme, les sources pour les applications web devraient être dans le répertoire `web`.

### Découpage

Il y a sept paquets Go, dont les dépendances sont :

```mermaid
graph TD;
api -->appinfo
api--> logger
api -->storage
api -->mux(gorilla/mux - Utilitaire de gestion des requêtes HTTP)
api --> core
cmd --> api
cmd -->appinfo
auth-->jwt(drijalva/jwt-go - Utilitaire pour les tokens JWT)
storage --> core
storage -->gorm(gorm - ORM)
```

On constate que le module `core` ne dépend de rien et surtout pas de détail d'implémentation comme la configuration de l'ORM ou la sérialisation en JSON. Il y a des objets intermédiaires que j'ai appelé `adapter` (faute de mieux) dans les paquets `storage` et `api` pour gère ces détails d'implémentation. À titre d'exemple, l'adaptateur JSON de l'objet métier Coach.

```go
type coachAdapter struct {
  ID        uint   `json:"id"`
  Name      string `json:"name"`
  TeamName  string `json:"teamName,omitempty"`
  Email     string `json:"email"`
  NafNumber uint   `json:"nafNumber,omitempty"`
  Faction   string `json:"faction,omitempty"`
  Ready     bool   `json:"ready,omitempty"`
}

func coachAdapterFromCore(coach core.Coach) coachAdapter {
  return coachAdapter{ID: coach.ID, Name: coach.Name,
    TeamName: coach.TeamName, Email: coach.Email,
    NafNumber: coach.NafNumber, Faction: string(coach.Faction),
    Ready: coach.Ready,
  }
}

func (c coachAdapter) toCore() core.Coach {
  return core.Coach{ID: c.ID, Name: c.Name,
    TeamName: c.TeamName, Email: c.Email,
    NafNumber: c.NafNumber, Faction: core.Faction(c.Faction),
    Ready: c.Ready,
  }
}
```

Cette approche permet de séparer les objets métier des sorties de l'API. On pourrait envisager l'utilisation d'adaptateurs distincts pour les objets complets et les objets résumés apparaissant dans une liste. Cela implique l'ajout de code supplémentaire, et bien que je ne sois pas entièrement satisfait de cette implémentation pour le moment, je trouve cette méthode plutôt utile.

## Conclusion

Je suis content de vous avoir présenté ce projet, cela va me permettre d'améliorer le fichier README du projet. J'espère prochainement pouvoir vous présenter les avancées sur le code. Il reste encore beaucoup de choses à implémenter et de bonnes habitudes à prendre avec le Go.

Si vous souhaitez participer au développement, j'accueille les merge requests avec plaisir. Et si vous avez des projets perso, n'hésitez pas à les partager avec moi sur [X](https://x.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
