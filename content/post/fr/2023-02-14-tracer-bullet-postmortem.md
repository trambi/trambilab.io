---
title: "Analyse d'une balle traçante"
date: "2023-02-14"
draft: false
description: "qui s'est écrasée avant sa cible"
tags: ["Software Craftmanship","méthodologie","cloud"]
author: "Bertrand Madet"
---

En novembre, j'évoquais la possibilité d'[utiliser une balle traçante pour commencer un projet]({{< ref "/post/fr/2022-11-24-start-with-a-tracer-bullet.md">}}). Le terme `tracer bullet` apparaît dans le livre Pragmatic Programmer de David Thomas et Andrew Hunt (cf. [Fiche de lecture]({{< ref "post/fr/2022-03-23-pragmatic-programmer-lecture" >}})). J'étais en train de suivre ce conseil et je vous livre aujourd'hui un exemple d'utilisation de balle traçante.

## Contexte du projet

Le projet était un petit projet classique avec une interface utilisateur Web et des traitements en back-office.

```mermaid
graph LR;
    subgraph Front-office
        ui
        apigateway
        frontlambda
    end
    subgraph Back-office
        backfiles
        bdd
        sqs
        back
    end
    subgraph VPC
        Front-office
        Back-office
    end
    users(Utilisateurs)-->browser(Navigateur)
    browser-->apigateway[AWS API Gateway]
    browser-->authent(Solution d'authentification corporate)
    apigateway-->ui[Fichiers statiques d'application Web sur S3]
    apigateway-->frontlambda[AWS Lambdas utilisées pour le front office]
    frontlambda-->authent
    frontlambda-->backfiles[Fichiers sur S3]
    frontlambda-->bdd[(Base de données)]
    frontlambda-->sqs[(Système de messagerie)]
    sqs-->back[Cluster de traitement long]
    back-->bdd
```

Les modalités d'intégration continue et de déploiement étaient encore fraîches (comme les peintures) et peu connues de moi. L'architecture de l'application n'a pas été encore mise en place dans mon entreprise. La construction de l'infrastructure se fait par Terraform que je connaissais pas. Les services AWS : Lambda, API Gateway, S3 ont été abordés dans des cas simples lors de la formation [Developping on AWS - cf. la synthèse]({{< ref "/post/fr/2022-10-06-training-developing-on-aws.md">}}) mais pas utilisés encore dans une application. Pour les services AWS : SQS, RDS, m'étaient inconnus. L'application web devait être en React et l'usine logicielle reposait sur GitLab donc je connaissais.

## Cible de la balle traçante

On le voit les inconnues étaient multiples. Nous avons décidé d'utiliser la technique de la balle traçante. Le but de cette balle traçante était de déployer automatiquement "une page de consultation" de l'application web accessible uniquement du réseau corporate avec authentification et données issues de la base de données.

Cela permet de couvrir l'intégration et le déploiement d'une partie de la solution :

```mermaid
graph LR;
    subgraph Front-office
        ui
        apigateway
        frontlambda
    end
    subgraph Back-office
        bdd
    end
    subgraph VPC
        Front-office
        Back-office
    end
    users(Utilisateurs)-->browser(Navigateur)
    browser-->apigateway[AWS API Gateway]
    browser-->authent(Solution d'authentification corporate)
    apigateway-->ui[Fichiers statiques d'application Web sur S3]
    apigateway-->frontlambda[AWS Lambdas utilisées pour le front office]
    frontlambda-->authent
    frontlambda-->bdd[(Base de données)]
```

Cela ne couvrait pas toutes les parties mais cela permettait de lever les risques sur de nombreuses inconnues du projet. Cela implique notamment de charger des données crédibles manuellement dans la base de données. À la fin de cette balle traçante, il devrait être plus possible d'estimer les efforts à fournir pour la suite de l'application.

## Le plan

En privilégiant le fait de pouvoir exposer les progrès réalisés à nos utilisateurs au fur et à mesure, je pensais procéder comme il suit :

1. Première page épurée sur mon poste de développement :right_arrow: première ébauche de la page validée ;
1. Première page épurée construite lors de l'intégration continue :right_arrow: première version de la CI validée ;
1. Première page épurée accessible sur Internet :right_arrow: infrastructure avec API Gateway, S3 et Lambda validée ;
1. Première page épurée accessible sur Internet déployée par la CD :right_arrow: première version de la CD validée ;
1. Première page épurée accessible du réseau corporate uniquement déployée par la CD :right_arrow: infrastructure avec VPC et rattachement au réseau corporate validés ;
1. Première page accessible du réseau corporate uniquement déployée par la CD :right_arrow: deuxième version de la page validée ;
1. Première page accessible du réseau corporate uniquement après authentification déployée par la CD :right_arrow: utilisation de la solution d'authentification corporate validée ;
1. Première page avec données issues de la base de données accessible du réseau corporate uniquement après authentification déployée par la CD :right_arrow: création, déploiement et utilisation de la base de données validés ;

Cela permettait d'augmenter progressivement la complexité en gardant des éléments testables et démontrables à nos utilisateurs.

## La réalisation

Au début, la progression a été lente et pleine d'apprentissage sur Terraform, AWS notamment à cause de mon manque d'expérience sur AWS et Terraform. J'ai eu l'occasion d'écrire une petite dizaine de billets de blog et de documents internes.

La balle traçante a fini sa course à l'étape de rattachement au réseau car les obstacles n'ont plus été techniques mais organisationnels. L'échelle de temps pour la résolution de tels problèmes ne se comptait plus en jour mais en semaine.

## Conclusion

L'objectif fixé n'a pas pu être atteint, cette approche a permis de documenter et d'illustrer concrètement les changements amenés par l'utilisation de services serverless tels que fournis par AWS. 

Si le côté "balle" a échoué, le côté "traçant" est une réussite. De mon point de vue, c'est l'aspect déterminant de l'approche dite de la balle traçante : lever les incertitudes techniques ou organisationnelles sur un projet en restant dans le cadre de qualité du projet et dans un temps restreint.

Nous avons ensuite adapté notre stratégie pour essayer d'obtenir des retours de nos utilisateurs même sans possibilité à court terme de déployer dans la configuration cible.

Si vous voulez partager sur les méthodes pour améliorer progressivement et continûment un logiciel, contactez moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
