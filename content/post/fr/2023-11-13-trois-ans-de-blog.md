---
title: "Bilan des trois ans"
date: "2023-11-13"
draft: false
description: "Plus court que celui de la deuxième année !"
tags: ["blog"]
author: "Bertrand Madet"
---

J'ai lancé mon blog le 11 novembre 2020. Je résume ces trois années de publication.

## Données brutes sur les articles

|               |Première année|Deuxième année|Troisième année|Total|
|---------------|--------------|--------------|---------------|-----|
|Billets publiés|61            |51            |50             |162  |
|Fiches de lectures :book:  |13|7             |5              |25|
|Tags plus utilisés|[fiche de lecture (13)]({{< ref "/tags/fiche-de-lecture">}}), [Software Craftmanship (10)]({{< ref "/tags/software-craftmanship">}}) et [base (9)]({{< ref "/tags/base">}})|[Software Craftmanship (14)]({{< ref "/tags/software-craftmanship">}}), [outils (12)]({{< ref "/tags/outils" >}}) et [projet perso (10)]({{< ref "/tags/projet-perso" >}})|[Test (16)]({{< ref "/tags/test">}}), [Software Craftmanship (12)]({{< ref "/tags/software-craftmanship">}}) et [cloud (12)]({{< ref "/tags/cloud" >}})| [Software Craftmanship (36)]({{< ref "/tags/software-craftmanship">}}),[projet perso (26)]({{< ref "/tags/projet-perso" >}}), [test (25)]({{< ref "/tags/test" >}}) et [fiche de lecture (25)]({{< ref "/tags/fiche-de-lecture" >}})|

Je trouve que ce découpage par tag représente bien mes centres d'intérêts professionnels. On voit que j'ai pu lire et résumer 5 livres cette année :

- Learning Domain-Driven Design de Vlad Khononov ;
- Working effectively with legacy code de Michael C. Feathers ;
- The Design of Web APIs d'Arnaud Lauret ;
- Modern Software Engineering - Doing what works to build better software faster de David Farley ;
- Accelerate - Building and Scaling High Performing Technology Organizations de Nicole Forsgren, PhD, Jez Humble et Gene Kim.

## Processus de rédaction

Mon processus de rédaction a été plus chaotique cette année. Je n'ai pas réussi à me faire un stock de sujets pour faire redescendre la "pression" de la publication. Il y a des semaines, où dès le milieu de la semaine, je sais ce que je vais écrire pour cette semaine. Parfois, j'ai l'impression de n'avoir rien appris ou que j'ai appris des choses pas publiables sur ce blog (car interne à mon entreprise ou sans rapport avec le logiciel). L'exigence d'une publication va m'obliger à regarder ma semaine d'une autre manière pour trouver quoi publier et ce que j'ai appris sans m'en apercevoir. La rédaction prend en général deux heures et parfois plus quand il y a du code.

En plus de l'outil de vérification d'orthographe [aspell (http://aspell.net/)](http://aspell.net/), j'ai commencé à utiliser ChatGPT pour vérifier la qualité du billet. Ce n'est pas simple car je veux publier un article personnel et non un article "chatGPT". Pour l'instant, la meilleure méthode que j'ai trouvée consiste à faire résumer par ChatGPT chaque paragraphe et voir si cela correspond au message que je désire envoyer. C'est plutôt long et ce n'est pas la meilleure manière d'utiliser l'outil mais permet de ne pas "dénaturer" les billets.

## Processus de diffusion

Pour partager mon expérience, j'utilise principalement LinkedIn. J'ai conservé l'approche de l'année dernière : un post avec les éléments les plus prégnants et je laisse les extraits de code pour le billet de blog. Le lien vers le billet est dans le premier commentaire.

Mon utilisation de X pour la diffusion a encore diminué, je n'ai pas trouvé la forme adéquate pour les tweets. Je verrai ce que je peux faire.

J'ai commencé à exploiter mes billets de blog pour écrire certains documents pour mon entreprise en ajoutant juste les éléments "corporate" spécifiques ou en agrégeant les informations de plusieurs billets.

## Conclusion

Avec cette troisième année, l'écriture de ce blog est une discipline installée. Les outils de publication sont rodés. Il faut encore affiner la rédaction et la publication des contenus.

Merci à tous ceux qui m'ont donné l'idée de nouveaux billets, d'amélioration ou de rectification. Si vous avez des suggestions concernant mes billets passés ou futurs faîtes m'en faire part autour d'un thé :tea:, sur [X](https://twitter.com/trambi_78), [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
