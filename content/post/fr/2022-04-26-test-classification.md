---
title: "Les différents types de test"
date: "2022-04-26"
draft: false
description: "un vocabulaire commun pour mieux se comprendre"
tags: ["test","Software Craftmanship"]
author: "Bertrand Madet"
---

Avec les sessions dédiés aux tests (cf. [Test clinic]({{< ref "/post/fr/2022-04-06-test-clinic.md" >}})), je me rend compte qu'il y a des confusions sur les noms des types de tests que l'on peut réaliser sur un logiciel. Et je ne suis moi-même pas très clair là-dessus.

La première confusion, qui m'est apparu, est la confusion entre test automatique et test unitaire.

Je pense que la difficulté vient du fait que les tests ont plusieurs caractéristiques (les data scientistes diraient dimensions) et qu'il est possible de les nommer suivant ces différentes caractéristiques. Je vous propose donc d'étudier les caractéristiques importantes au moment d'écrire un test et de mettre des mots sur des concepts.

## Implémentation ou comportement

Est-ce que l'on peut teste le comportement du logiciel ou son implémentation ? Quand je parle de comportement, je veux dire le comportement observable via les interfaces de la partie logicielle testée.

Par exemple si on teste une pile avec les méthodes `push`, `pop` et `isEmpty`, un test par comportement peut-être :

```typescript
describe('Stack',() => {
  it('should implement isEmpty, push and pop',() => {
    const stack = new Stack();
    except(stack.isEmpty()).toBe(True);
    stack.push('first');
    except(stack.isEmpty()).toBe(False);
    stack.push('second');
    expect(stack.pop()).toEqual('second');
    expect(stack.pop()).toEqual('first');
    except(stack.isEmpty()).toBe(True);
  });
});
```

On ne regarde pas les détails d'implémentation de la pile juste son interface : le constructeur et les trois méthodes.

Dans la grande majorité des cas, les tests sont des tests de comportement. Il peut arriver de faire des tests d'implémentation par exemple pour des problématiques de sécurité où les détails d'implémentation influent sur la sécurité du logiciel sans avoir fait l'objet du spécification.

## Portée du test

La portée ou l'étendue du logiciel testée permet aussi de caractériser les tests.

Un `test unitaire` porte sur une et une seule classe ou une et une seule fonction.

Un `test d'intégration` porte sur plusieurs classes et fonctions.

Un `test de bout en bout` (ou `end to end` :gb:) porte sur l'intégralité du système (ou une grande partie du système).

:warning: A noter que la portée dépend de l'implémentation.

## Autres caractéristiques

### Automaticité

Qui ou quoi déclenche et réalise le test ?

|x|Une personne réalise le test|Un programme réalise le test|
|---|---|---|
|Une personne déclenche le test|`Test manuel`|`Test semi-automatique`|
|Un programme déclenche le test|`Test manuel`|`Test automatique`|

Le cas d'un programme qui demande à une personne de réaliser un test peut paraître étrange mais on peut imaginer devoir réaliser un test manuel si une version de bibliothèque externe change.

### Domaine

Ici, on va s'intéresser plus à ce que l'on teste :

- les fonctionnalités et on parlera des tests fonctionnels ;
- les performances et on parlera des tests de performance ;
- la conformité à une norme de programmation ou à une norme métier et on peut parler de tests de conformité ;
- la sécurité ...

### Utilisation

Il arrive souvent que l'on nomme les tests en fonction de leur utilisation dans le processus de fourniture du service.

Par exemple, les tests nécessaires à l'acceptation d'une fonctionnalité sont souvent appelés tests d'acceptation.

## Du côté de Google

Une dimension utilisée par Google et décrite dans [Software Engineering at Google]({{< ref "/post/fr/2021-05-02-software-engineering-at-google-lecture.md" >}}) est la taille du test exprimé en taille de T-shirt :

- `Small` ou petit ou S implique un seul processus (au sens système d'exploitation) ;
- `Medium` ou moyen ou M implique une seule machine (utilisation de réseau mais les seuls adresses possibles sont locales) ;
- `Large` ou large ou L implique plus d'une machine.

## Conclusion

On peut donc avoir à coder un test fonctionnel large d'intégration ou un petit test unitaire d'implémentation de sécurité. En programmant en TDD, on a malgré tout très souvent affaire à des tests unitaires de comportement. La variété n'est pas un problème mais cela permet d'adapter les tests à son contexte pour arriver à avoir confiance dans le logiciel créé. En particulier, lors des test clinics, c'est très utile d'avoir en tête cette palette de possibilité.

Pour aller plus loin, on peut citer l'article [The pratical Test Pyramid (https://martinfowler.com/articles/practical-test-pyramid.html)](https://martinfowler.com/articles/practical-test-pyramid.html) de Ham Vocke et plus général [les articles avec l'étiquette `testing` du site de Martin Fowler (https://martinfowler.com/tags/testing.html)](https://martinfowler.com/tags/testing.html).

Si vous avez des expériences ou des questions sur les tests logiciels, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
