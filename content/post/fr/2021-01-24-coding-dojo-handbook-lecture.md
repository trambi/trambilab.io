---
title: "The Coding dojo handbook - Fiche de lecture"
date: "2021-01-24"
draft: false
description: "Livre d'Emily Bache sur la mise en place d’entraînement au développement"
tags: ["fiche de lecture","dojo"]
author: "Bertrand Madet"
---


## A qui s'adresse ce livre

Aux personnes qui veulent améliorer leurs compétences de codage. Aux personnes qui veulent promouvoir les bonnes pratiques de développement.

## Ce qu'il faut retenir

- Il est facile d'organiser des sessions d’entraînement (kata)
- Il existe des variations pour garder l'intérêt de ces sessions
- Lancez-vous !

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Un livre concret qui contient tout ce qu'il faut pour commencer| :-1: Il manque un indicateur de difficulté pour les katas proposés|
| :+1: En plus des vingt katas proposés, l'auteure en propose d'autres sur son compte GitHub||

Ce livre est un bon complément de livres avec plus de recul comme Clean Code, Clean Coder ou encore Software Craftman . Il est hyper concret, il y a énormément de matériel pour démarrer l'aventure.

Ce livre m'a non seulement donné l'envie d'organiser un coding-dojo  dans mon entreprise : il m'a permis de le faire. J'en parlerai dans un futur billet.

## Détails

En anglais :gb: :

- The Coding Dojo handbook d'Emilie Bache
- ISBN-13 : 978-91-981180-3-2

Pas disponible en français :fr: :cry:
