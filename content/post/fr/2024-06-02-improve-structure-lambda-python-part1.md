---
title: "Améliorer la structure de vos lambdas en Python"
date: "2024-06-02"
draft: false
description: "grâce à des techniques de test et de refactoring"
tags: ["python","test","lambda"]
author: "Bertrand Madet"
---

Les lambdas AWS, au même titre que les autres **F**unctions **A**s **A** **S**ervice comme serverless functions de Scaleway par exemple, sont juste des fonctions Python classiques. Pourtant, j'ai pu remarquer des écueils dans leur implémentation, je vous propose de passer en revue ces écueils, de regarder l'impact sur la testabilité et des pistes de refactoring pour améliorer la structuration du code.

##  Initialisation de ressource en dehors de la fonction

Il peut arriver que le code de la lambda ressemble à cela :

```python
# Fichier main.py
import boto3

# Cette ligne va poser des problèmes à l'import
s3 = boto3.resource('s3')

def do_something_on_s3_handler(event, context):
  # de 1 à 100 lignes pour faire ce que l'on a à faire avec notre ressource S3
```

*A priori* rien d'exceptionnel mais la ligne d'initialisation de `boto3` va être appelée dès l'import du fichier `main.py`. L'initiation de cette ressource directement dans le module permet de l'effectuer uniquement à l'initiation de la lambda et non à chaque exécution. Si on veut tester dans ce cas, il faut prendre des mesures avant d'importer le fichier.

### Test unitaire en utilisant moto

Notre problème est de faire en sorte que l'import ne crée pas une ressource S3 qui ira se connecter au fournisseur de service S3. On va utiliser la bibliothèque [moto (http://docs.getmoto.org/en/latest/)](http://docs.getmoto.org/en/latest/) 🏍️ pour mocker le service avant d'importer le fichier.

```python
# Fichier test_main.py
import moto

def test_do_something_on_s3_handler():
  # Given
  event = {
    # Contenu de l'événement à traiter
  }
  context = {
    # Contenu du contexte
  }
  bucketname = 'something'
  # On est obligé de "mocker" S3 avant l'import du module
  with moto.mock_aws():
    conn = boto3.resource("s3", region_name="eu-west-3")
    # On a besoin de créer le bucket S3 au niveau de moto
    conn.create_bucket(Bucket=bucketname)
    import main
    # When
    result = main.do_something_on_s3_handler(event, context)
    # Then
    # ...
```

Cela permet de tester le comportement de la lambda mais on devra faire ce genre d'acrobatie 🎪, pour chaque fonctionnalité.

### Extraction de fonction et mise en paramètre de la dépendance

En admettant qu'on ait besoin de l'initialisation de la ressource S3 dans le module (par exemple pour des questions de performances), on peut faire une extraction de fonction pour simplifier les tests et la fonction. La ressource S3 devient un paramètre de la fonction. Cela permet d'être plus explicite sur le but de la fonction.

```python
# Fichier main.py
import boto3
import handler

s3 = boto3.resource('s3')

def do_something_on_s3_handler(event, context):
  handler.do_something_on_s3_handler_with_s3_resource(event, context, s3)
```

```python
# Fichier handler.py

def do_something_on_s3_handler_with_s3_resource(event, context, s3resource)
  # de 1 à 100 lignes pour faire ce que l'on a à faire avec notre ressource S3
```

Une fois cette modification effectuée, on peut :

- faire un test sur un cas simple de la lambda dans le fichier `test_main.py`;
- faire d'autres tests sur les autres cas, avec la fonction extraite dans un fichier `test_handler.py`.

```python
# Fichier test_handler.py
import moto
import handler

def test_do_something_on_s3_handler_with_s3_resource():
  # Given
  event = {
    # Contenu de l'événement à traiter
  }
  context = {
    # Contenu du contexte
  }
  bucketname = 'something'
  with moto.mock_aws():
    conn = boto3.resource("s3", region_name="eu-west-3")
    # On a besoin de créer le bucket S3 au niveau de moto
    conn.create_bucket(Bucket=bucketname)
    # When
    result = handler.do_something_on_s3_handler_with_s3_resource(event,context,conn)
    # Then
    # ...
```

Maintenant, on peut importer le fichier à tester sans avoir à mocker la ressource S3 au préalable.

## Utilisation des variables d'environnement au fil de l'eau

Il peut arriver que l'on utilise des variables d'environnement au fil de l'eau. 

```python
# Fichier main.py
import os

def get_content_of_object_based_on_event_and_environment(event):
  bucket = os.environ['INPUT_BUCKET']
  prefix = os.environ['INPUT_OBJECT_PREFIX']
  # ...

def check_pattern_in_content(content):
  patternToCheck = os.environ['PATTERN_TO_CHECK']
  # ...


def do_something_on_s3_handler(event, context):
  content = get_content_of_object_based_on_event_and_environment(event)
  check_pattern_in_content(content)

```

### Test unitaire en utilisant monkeypatch 🐒

Pour tester les fonctions, nous allons utiliser la méthode `setenv` de `monkeypatch`. J'ai déjà écrit à propos de cette méthode dans le billet [Redécouverte de pytest]({{< ref "/post/fr/2023-07-12-rediscover-pytest" >}}). Elle permet simplement de fixer les variables d'environnement pour la durée d'un test.

```python
# On est obligé d'importer pytest pour utiliser la fixture monkeypatch
import pytest
import main

def test_check_pattern_in_content(monkeypatch):
  # Given
  monkeypatch.setenv('PATTERN_TO_CHECK','valeur pour le test')
  content = 'contenu pour le test'
  # When
  result = main.check_pattern_in_content(content)
  # Then
  # assert result == ...

def test_get_content_of_object_based_on_event_and_environment(monkeypatch):
  # Given
  monkeypatch.setenv('INPUT_BUCKET','un-bucket')
  monkeypatch.setenv('INPUT_OBJECT_PREFIX','prefix-pour-le-test')
  event = {
    # événement pour le test
  }
  # When
  result = main.get_content_of_object_based_on_event_and_environment(event)
  # Then
  # assert result == ... 
```

### Creation d'une fonction de configuration

Je trouve utile de vérifier les variables d'environnement manquantes ou incorrectes avant de commencer les traitements. Cela permet d'envoyer des traces dans les journaux pour signaler le problème de mauvaise initialisation des variables d'environnement afin de corriger le problème d'un coup. Je trouve particulièrement pénible 😓, de résoudre un problème d'initialisation de variable d'environnement, déployer et s'apercevoir qu'une autre variable d'environnement est manquante.

```python
# Fichier main.py
import os

NEEDED_ENV_VARIABLES = ['INPUT_BUCKET', 'INPUT_OBJECT_PREFIX','PATTERN_TO_CHECK']

def get_settings():
  missing = []
  settings = {}
  for needed in NEEDED_ENV_VARIABLES:
    if needed not in os.environ:
      missing.append(needed)
    settings[needed] = os.environ[needed]
  if missing :
    raise Exception("Missing environment variables: " + missing.join(", ")) 
  return settings


def get_content_of_object_based_on_event_and_settings(event, settings):
  bucket = settings['INPUT_BUCKET']
  prefix = settings['INPUT_OBJECT_PREFIX']
  # ...


def check_pattern_in_content(content, settings):
  patternToCheck = settings['PATTERN_TO_CHECK']
  # ...


def do_something_on_s3_handler(event, context):
  settings = get_settings()
  content = get_content_of_object_based_on_event_and_settings(event,settings)
  check_pattern_in_content(content,settings)
```

La proposition d'implémentation de la fonction est peut-être un peu naive mais l'esprit est là. De cette manière :

1. Les intentions et les responsabilités sont claires - `get_settings` s'occupe de la configuration, les autres fonctions s'occupent du traitement.
1. Les tests sont individuellement moins étendus
    1. la fonction `get_settings` va être testée en utilisant la méthode `setenv` de monkeypatch 🐒 ;
    1. les fonctions `get_content_of_object_based_on_event_and_settings` et `check_pattern_in_content` seront testées en se concentrant sur les cas métiers 👔.

## Conclusion

Je n'ai pas abordé le cas tristement classique du code spaghetti 🍝. Sans doute, la petite quantité de code trompe notre vigilance. Mais avec l'expressivité des langages modernes et de Python en particulier, une fonction de trente lignes peut abriter plusieurs degrés d'abstraction et méritait une série de refactoring. 

C'est néanmoins pas spécifique aux lambdas et un deuxième article pourrait être utile. En attendant, je vous propose de relire la chapitre 3 "Functions" du livre Clean Code de Robert Martin - [la fiche de lecture]({{< ref "/post/fr/2020-11-16-clean-code-lecture" >}}) 😄.

Si vous souhaitez partager sur les lambdas ou les tests ou les deux, contactez moi [sur X](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169
