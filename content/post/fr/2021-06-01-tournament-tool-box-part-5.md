---
title: "Boîte à outils pour tournoi - partie 5"
date: "2021-06-01"
draft: false
description: "Prise en compte des nombres décimaux"
tags: ["projet perso","Go","jeu","nombre","decimaux"]
author: "Bertrand Madet"
---
Boite à outils pour tournoi :
[partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}),
[partie 2]({{< ref "/post/fr/2020-12-09-tournament-tool-box-part-2.md" >}}),
[partie 3]({{< ref "/post/fr/2020-12-28-tournament-tool-box-part-3.md" >}}),
[partie 4]({{< ref "/post/fr/2021-04-26-tournament-tool-box-part-4.md" >}}),
[partie 6]({{< ref "/post/fr/2023-03-08-tournament-tool-box-part-6.md" >}}),
[partie 7]({{< ref "/post/fr/2023-03-20-tournament-tool-box-part-7.md" >}})

-----

La première fois que j'ai eu besoin de mon outil, le tournoi utilisait `0.5` pour les matchs nuls, alors que j'avais tout codé en entier :cry: voici les modifications que cela a entraîné.

## Le but

Prendre en compte un classement avec une victoire à un point et un match nul à un demi point (0.5).

## Développement

### Le plan

Avec des entiers, on ne peut pas représenter des décimaux. Comme indiqué dans mon [billet sur les nombres décimaux]({{< ref "/post/fr/2021-05-17-a-propos-des-nombres-decimaux.md" >}}), il y a plusieurs manières de représenter des nombres décimaux en informatique. J'ai renoncé aux nombres flottants (*float*) parce que cela me semblait compliqué de gérer les erreurs d'affichage et d'arrondi qui interviendront quand mes utilisateurs feront des sommes de milliers avec des `0.1`. J'ai choisi d'utiliser des nombres de précision arbitraire avec la librairie standard `math/big` et le type `Float`. Il fallait donc remplacer les entiers par des structures `Float` de `math/big` dans beaucoup d'endroit.

## La réalisation

### Paquet evaluator

J'ai commencé par ajouter un test de l'évaluateur qui s'assurait que

{{< math >}}
1 + 2.5 = 3.5
{{< /math >}}

Il échouait évidemment. J'ai ensuite implémenté l'addition. J'ai procédé de la même manière pour la soustraction et la multiplication. J'ai un peu de mal à utiliser correctement [l'API de `math/big` (https://golang.org/pkg/math/big/#Float)](https://golang.org/pkg/math/big/#Float). Elle utilise des références et renvoit toujours une référence pour pouvoir chaîner les opérateurs.

J'ai été aussi obligé de modifier le découpage en jeton et la représentation du contexte de tableau associatif d'entier par des chaînes de caractères en tableau association de chaînes de caractères par chaînes de caractères.

### Paquets compute et rank

Le contexte pour l'évaluation ayant changé, j'ai pris en compte cette modification dans le module `compute`. J'ai ensuite appliqué un mécanisme simple : Recherche des fonction qui utilisent les fonctions `Atoi` et `Itoa`, pour chaque fonction :

1. Ajout d'un test avec un décimal ;
1. Lancement du test pour vérifier qu'il est bien en erreur ;
1. Modification du code (en général utilisation de la méthode `Parse`);
1. Lancement du test pour vérifier qu'il est ok.

## En résumé

Je retiens de cette séquence :

- Le premier test utilisateur m'a amené à modifier 3 paquets - :smile: ;
- C'est bien d'avoir des tests unitaires pour faire ce genre de modification en toute sérénité ;
- Certains des tests sont trop focalisés sur des détails d'implémentation au lieu des comportements.

Voilà, j'espère que cela vous a plu. Si vous souhaitez partager des projets, contactez [moi sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/). J'accueille les merge requests sur [TournamentToolBox (https://www.gitlab.com/trambi/tournamenttoolbox)](https://www.gitlab.com/trambi/tournamenttoolbox) avec enthousiasme.

-----

Boite à outils pour tournoi :
[partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}),
[partie 2]({{< ref "/post/fr/2020-12-09-tournament-tool-box-part-2.md" >}}),
[partie 3]({{< ref "/post/fr/2020-12-28-tournament-tool-box-part-3.md" >}}),
[partie 4]({{< ref "/post/fr/2021-04-26-tournament-tool-box-part-4.md" >}}),
[partie 6]({{< ref "/post/fr/2023-03-08-tournament-tool-box-part-6.md" >}}),
[partie 7]({{< ref "/post/fr/2023-03-20-tournament-tool-box-part-7.md" >}})
