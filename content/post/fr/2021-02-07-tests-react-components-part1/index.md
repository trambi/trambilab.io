---
title: "Tests de composants React - partie 1"
date: "2021-02-07"
draft: false
description: "tests unitaires et tests end-to-end"
tags: ["Software Craftmanship","front-end","test","reactjs"]
author: "Bertrand Madet"
---

Je fais partie depuis un an et demi d'une équipe qui construit principalement deux applications Web.

Durant cette période, nous sommes passés de :

> ha les tests ? j'ai pas eu le temps de les écrire, mais j'ai regardé ...

à une couverture de code supérieure à 80 % (ce n'est pas un but en soi, c'est un indicateur) et une utilisation principale du TDD (**T**est **D**riven **D**evelopment - *développement piloté par les tests* :fr:). Il reste encore du travail mais disons que nous sommes sur la bonne voie.

Nous utilisons [React(https://fr.reactjs.org/)](https://fr.reactjs.org/) pour faire les composants graphiques des "pages" à afficher. Les composants communs à nos deux applications sont stockés dans un catalogue de composant.

## Tests unitaires

Pour les tests unitaires, nous utilisons [Jest (https://jestjs.io)](https://jestjs.io) et [React Testing Library (https://testing-library.com/)](https://testing-library.com/).

Aujourd'hui, nous utilisons le plus les fonctions suivantes :

- `render` :disappointed:
- `expect()`
- `fireEvent.click()`

 Si vos composants React ont des états compliqués (cela devrait être une exception) et que vous utilisez le [hook React `useReducer`](https://fr.reactjs.org/docs/hooks-reference.html#usereducer), vous pouvez tester à part la fonction transmise (le réducteur ? :neutral_face:).

## Il n'y a pas que les tests unitaires dans la vie

Si nous avons une fonctionnalité `f` qui utilisent les composants `c1`, `c2` et `c3`.

Voici votre couverture avec des tests unitaires :

![Couverture avec juste des tests unitaires](tu_only.png)

Comme vous pouvez le voir, on n'est pas sûr que la fonctionnalité `f` appelle bien le composant `c1` ni que le composant `c2` appelle bien le composant `c3`. Cela m'est déjà arrivé de coder une nouvelle étape pour une fonctionnalité avec des tests unitaires. Malgré une revue de code, nous avons mis en production la fonctionnalité qui n'appellait pas la nouvelle étape ...:scream:

Pour couvrir ce genre de cas, il faut un autre type de tests qui couvre la fonctionnalité de bout en bout (ou *end to end* :gb:).

![Couverture avec des tests unitaires et des tests de bout en bout](tu_and_te2e.png)

Alors, je vous entends me dire mais pourquoi s'embêter à écrire des tests unitaires s'ils ne sont pas suffisants et qu'il faut écrire d'autres tests ? Parce que les tests unitaires vous donnent rapidement un retour sur votre code, que vous n'avez pas envie de déployer les composants `c2` et `c3` pour tester votre composant `c1`.

Pour aller plus loin, je vous invite à lire l'excellent article [Practical test pyramid (https://martinfowler.com/articles/practical-test-pyramid.html)](https://martinfowler.com/articles/practical-test-pyramid.html) (:gb:).

## Tests de bout en bout

Les tests de bout en bout de services web utilisent un navigateur web pour se mettre à la place de l'utilisateur, c'est le rôle de [Puppeteer (https://developers.google.com/web/tools/puppeteer/)](https://developers.google.com/web/tools/puppeteer/). `Puppeteer` va piloter le navigateur Chrome ou Chromium (ou même Firefox :smile: dans les dernières version) à partir d'une API Node.js.

La difficulté c'est qu'il y a plein de facteurs aléatoires comme la latence réseau.

Par exemple, sur deux de nos sites web,  les menus sont centralisés (et proposés via API Rest), cela permet d'être sûr d'avoir les mêmes menus. Le menu se charge dynamiquement quelques instants (~200 ms) après l'affichage de la page, on peut ne pas le remarquer mais pour un programme qui effectue des millions (ou des milliards) d'opération par seconde cela aura un impact. Il faut donc que le programme attende que le menu soit chargé avant de regarder s'il est tel qu'attendu.

Nous utilisons Jest et Puppeteer. Je me suis rendu compte que pour améliorer la reproductibilité de ces tests, nous allions être obligé d'utiliser les fonctions d'attente `page.waitForSelector` et `page.waitForFunction`. Les erreurs de tests seront plus souvent des temps écoulés (sur les fonctions `waitFor...`) que des assertions fausses (`expect`).

Par exemple, si le menu a un lien avant d'être chargé et qu'on veut verifier que le dernier lien du menu chargé est `A propos`, on peut tester cela comme ça.

```typescript
// Waiting menu to be loaded
await page.waitForFunction( () => document.querySelectorAll('nav a').length > 1);
const links = await page.$$('nav a');
expect(links[links.length-1].innerHTML).toEqual('A propos');
```

## Conclusion

J'espère que ce partage d'expériences vous aideront à implémenter vos premiers tests ou à améliorer ceux existant. Si vous avez des retours ou d'autres conseils sur les tests, n'hésitez pas à les partager sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
