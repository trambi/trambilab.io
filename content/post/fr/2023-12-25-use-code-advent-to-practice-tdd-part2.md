---
title: "Utiliser Advent of Code pour s'entraîner au TDD - partie 2"
date: "2023-12-25"
draft: false
description: "avec le jour 3"
tags: ["tdd"]
author: "Bertrand Madet"
---

Comme je vous en avais parler au début du mois, le [calendrier de l'avent du code par Eric Wastl (https://adventofcode.com/)](https://adventofcode.com/) est un excellent prétexte pour nous entraîner au [TDD]({{< ref "/tags/tdd">}}).

Pour rappel, Advent of Code (ou `AoC`) propose pour chaque jour du 1 décembre au 25 décembre, une énigme à résoudre avec un petit programme. C'est devenu un vrai institution. Il y a même des articles dans les médias généralistes : ["Une institution" : l'Advent of Code séduit plus d'amateurs de programmation informatique chaque année sur France Inter](https://www.radiofrance.fr/franceinter/une-institution-l-advent-of-code-seduit-plus-d-amateurs-de-programmation-informatique-chaque-annee-3707079).

Je vous propose d'attaquer le jour 3. Comme je l'ai trouvé plus dur que les jours 1 et 2, je propose sans divulgachage (:gb: spoiler :wink:) les tests pour la première partie et la deuxième partie.

## Tests pour la première partie du jour 3

L'épreuve du [jour 3](https://adventofcode.com/2023/day/3) consiste à extraire d'une chaîne de caractère les nombres adjacents à un symbole autre qu'un point et ensuite d'additionner les nombres extraits. On va commencer par la fonction qui extrait les nombres adjacents à un symbole autre qu'un point à partir d'un tableau de chaîne de caractère (pour gérer plus facilement les lignes).

|#|Fonction|Entrée|Sortie attendue|Explications|
|-|--------|------|---------------|------------|
|1|extractAdjacentNumbers|['.1.']|[]|La fonction renvoit un tableau vide, s'il n'y pas de symbole adjacent|
|2|extractAdjacentNumbers|['+1.']|[1]|La fonction reconnait un nombre constitué d'un chiffre avec un plus à gauche|
|3|extractAdjacentNumbers|['+12']|[12]|La fonction reconnait un nombre constitué de deux chiffres avec un plus à gauche|
|4|extractAdjacentNumbers|['.1+']|[1]|La fonction reconnait un nombre avec un plus à droite|
|5|extractAdjacentNumbers|['.+.','.1.']|[1]|La fonction reconnait un nombre avec un plus en haut|
|6|extractAdjacentNumbers|['.1.','.+.']|[1]|La fonction reconnait un nombre avec un plus en bas|
|7|extractAdjacentNumbers|['+..','.1.']|[1]|La fonction reconnait un nombre avec un plus en haut à gauche|
|8|extractAdjacentNumbers|['..+','.1.']|[1]|La fonction reconnait un nombre avec un plus en haut à droite|
|9|extractAdjacentNumbers|['.1.','+..']|[1]|La fonction reconnait un nombre avec un plus en bas à gauche|
|10|extractAdjacentNumbers|['.1.','..+']|[1]|La fonction reconnait un nombre avec un plus en bas à droite|
|11|extractAdjacentNumbers|['467..114..', '...*......', '..35..633.', '......#...', '617*......', '.....+.58.', '..592.....', '......755.', '...$.*....', '.664.598..']|[467, 35, 633, 617, 592, 755, 664, 598]|La fonction gère différents symboles|
|12|sumOfNumbersWithAdjacentSymbol|['467..114..', '...*......', '..35..633.', '......#...', '617*......', '.....+.58.', '..592.....', '......755.', '...$.*....', '.664.598..']|4361|La fonction appelle bien la fonction précédente et somme les résultats.|

On voit que la première partie impose presque autant de test que l'un des deux premiers jours. La multiplicité de tests simples permet de simplifier les implémentations minimales pour chaque test. On utilise l'exemple comme une validation des étapes précédentes.

## Tests pour la deuxième partie du jour 3

La deuxième partie impose d'extraire des numéros d'"équipement". Ces numéros d'équipement sont repérables comme deux nombres adjacents à un symbole `*`, le numéro résultant étant le produit de deux nombres adjacents. Après réflexion et à la lueur de la première partie, je me suis dit que l'on pourrait décomposer le problème en six fonctions :

```mermaid
graph LR;
    sumOfGears(sumOfGears somme les numéros d'équipement)-->getGears(getGears extrait les numéros d'équipement)
    sumOfGears-->sum[sum somme les nombres d'un tableau - préexistant dans la première partie]
    getGears-->getCoordinatesOfAdjacentNumbers(getCoordinatesOfAdjacentNumbers liste les coordonnées des nombres adjacents aux symboles)
    getGears-->extractNumber(extractNumber extrait le nombre à partir des coordonnées d'un de ses chiffres)
    getCoordinatesOfAdjacentNumbers-->isDigit[isDigit indique si un caractère est un chiffre - préexistant dans la première partie]
    extractNumber-->isDigit
```

Comme deux de ces fonctions sont le résultat du refactorisation de la première partie, on va réaliser nos tests sur les nouvelles fonctions.

| # |Fonction|Entrée|Sortie attendue|Explications|
|---|--------|------|---------------|------------|
|A1|extractNumber|'...' et 0|0|La fonction renvoit zéro en cas d'absence de nombre|
|A2|extractNumber|'2.3' et 0|2|La fonction extrait le nombre à un chiffre à la bonne colonne|
|A3|extractNumber|'2.3' et 2|3|La fonction extrait le nombre à un chiffre à la bonne colonne|
|A4|extractNumber|'45.6' et 1|45|La fonction extrait le nombre à partir d'une de ses colonnes|
|A5|extractNumber|'7.89' et 2|89|La fonction extrait le nombre à partir d'une de ses colonnes|
|B1|getCoordinatesOfAdjacentNumbers|[".*."]|[]|La fonction renvoit un tableau vide en absence de chiffres autour du symbole "*"|
|B2|getCoordinatesOfAdjacentNumbers|['1*']|[[{ line:0, column:0}]]|La fonction renvoit une coordonnée quand un chiffre est à gauche|
|B3|getCoordinatesOfAdjacentNumbers|['*2']|[[{ line:0, column:1}]]|La fonction renvoit une coordonnée quand un chiffre est à droite|
|B3|getCoordinatesOfAdjacentNumbers|[".3.",".*."]|[[ {line:0, column:1}]]|La fonction renvoit une coordonnée quand un chiffre est au dessus|
|B4|getCoordinatesOfAdjacentNumbers|[".*.",".4."]|[[{ line:1, column:1}]]|La fonction renvoit une coordonnée quand un chiffre est au dessous|
|B5|getCoordinatesOfAdjacentNumbers|["5..", ".*."]|[[{ line:0, column:0}]]|La fonction renvoit une coordonnée quand un chiffre est au dessus à gauche|
|B6|getCoordinatesOfAdjacentNumbers|[".*.", "6.."]|[[{ line:1, column:0}]]|La fonction renvoit une coordonnée quand un chiffre est au dessous à gauche|
|B7|getCoordinatesOfAdjacentNumbers|["..7", ".*."]|[[{ line:0, column:2}]]|La fonction renvoit une coordonnée quand un chiffre est au dessus à droite|
|B8|getCoordinatesOfAdjacentNumbers|[".*.", "..8"]|[[{ line:1, column:2}]]|La fonction renvoit une coordonnée quand un chiffre est au dessous à droite|
|B9|getCoordinatesOfAdjacentNumbers|[".*.", ".90"]|[[{ line:1, column:1}]]|La fonction renvoit une coordonnée quand un nombre à deux chiffres est au dessous - centre et droite|
|B10|getCoordinatesOfAdjacentNumbers|[".*.", "12."]|[[{ line:1, column:0}]]|La fonction renvoit une coordonnée quand un nombre à deux chiffres est au dessous - centre et gauche|
|B11|getCoordinatesOfAdjacentNumbers|[".*.", "3.4"]|[[{ line:1, column:0}, { line:1, column:2}]]|La fonction renvoit deux coordonnées quand deux chiffres sont dans les diagonales basses|
|B12|getCoordinatesOfAdjacentNumbers|[".*.", "567"]|[[{ line:1, column:0}]]|La fonction renvoit une coordonnée quand un chiffre est complètement en dessous|
|18|getGears|['467..114..', '...*......', '..35..633.', '......#...', '617*......', '.....+.58.', '..592.....', '......755.', '...$.*....', '.664.598..']|[16345,451490]|La fonction utilise les deux fonctions précédentes|
|19|sumOfGears|['467..114..', '...*......', '..35..633.', '......#...', '617*......', '.....+.58.', '..592.....', '......755.', '...$.*....', '.664.598..']|[467835]|La fonction utilise la fonction précédente et somme les résultats|

Il est possible de commencer par `extractNumber` ou `getCoordinatesOfAdjacentNumbers`, on se concentre sur ces fonctions plus bas niveaux car elles permettent de simplifier les implémentations minimales. Et comme pour la première partie, on utilise les exemples pour valider les fonctions plus haut niveau.

## Conclusion

L'implémentation de ce jour m'a pris du temps. Les tests ont été l'objet de refactorisations pour parvenir à ceux que je vous ai proposé. En fonction de la décomposition du problème, les tests peuvent varier.

Même si le dernier jour est sorti, il me reste encore vingt-deux exercices potentiels pour pratiquer le TDD. Comme la difficulté est sensée augmenter, je pense que les deux premiers jours seront de bons sujets pour des coding dojos d'une heure. Je verrai pour les autres.

Si vous le voulez, partagez votre progression à l'Advent of Code ou sur le TDD avec moi [sur X](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
