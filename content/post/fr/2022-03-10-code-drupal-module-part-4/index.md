---
title: "Coder un module Drupal - partie 4"
date: "2022-03-10"
draft: false
description: "Rétrospective"
tags: ["projet perso","php","jeu", "drupal"]
author: "Bertrand Madet"
---

Mon premier module personnel Drupal est enfin déployé sur mon site cible. Je vous propose un bilan de cette aventure.

Les billets précédents :

- [Première partie du développement]({{< ref "/post/fr/2022-01-12-code-drupal-module-part-1.md" >}})
- [Deuxième partie du développement]({{< ref "/post/fr/2022-02-09-code-drupal-module-part-2.md" >}})
- [Retour sur le déploiement]({{< ref "/post/fr/2022-02-16-code-drupal-module-part-3.md" >}})

## Résultats

### Administration

Le site a trois (3) interfaces d'administration pour faire des opérations basiques (liste, ajout, édition et suppression) des critères de classement, des compétitions et des parties.

![Administration des critères de classement](list_criteria.png)
![Administration des compétitions](list_competition.png)
![Administration des parties](list_game.png)

### Visualisation

Le site a un (1) bloc configurable pour afficher le classement d'un compétition et la liste des matchs. On peut même en afficher deux fois ce bloc avec une configuration différente pour pointer vers deux compétitions différentes. Comme on peut voir ci-dessous.

![Deux blocs de classement configurés pour deux compétitions](ranking_block.png)

## Chemin

Pour ce résultat, j'ai implémenté :

- un (1) champ (ou `field` pour Drupal) ;
- trois (3) objets métiers (ou `entity` pour Drupal);
- un (1) bloc (ou `block` pour Drupal).

Grâce à Git, je sais :

- qu'il a fallu trente-huit (38) commits ;
- que le développement s'est étiré sur presque deux (2) mois ;
- qu'il y a plus de soixante fichiers.

## Ce que j'ai appris

Cela m'a permis d'apprendre plein de choses et voilà les points qui me semblent le plus pertinents

### Drupal est complexe

Ce qui peut m'apparaît le plus marquant c'est le nombre de fichiers nécessaire pour implémenter cinq (5) assez élémentaire dans un système de management de contenu.

Drupal est très modulaire, cela se ressent aussi dans le code. Par exemple comme je l'expliquais dans la partie 1, pour ajouter un champ, il faut coder trois (3) classes PHP à des endroits spécifiques de l'arborescence.

Le développement et le déboggage m'aura permis d'approfondir mes connaissances sur Drupal et de lire et relire "Drupal 9 Module Development" (la [fiche de lecture]({{< ref "/post/fr/2022-01-20-drupal-9-module-development-lecture.md" >}})).

### Drupal est puissant

Tous les cas que j'ai rencontré étaient prévues dans le code, certains n'étaient pas documentés mais comme le code est ouvert, on peut l'analyser.

Il est aussi possible de créer des objets métier directement depuis l'interface d'administration et ensuite d'exporter le code pour intégrer ses objets métier dans son module. Je n'ai pas essayé cette fonctionnalité.

J'ai l'impression que tous les champs sont traductibles dans l'interface d'administration.

## Conclusion

Si je devais effectuer un développement similaire - un module Drupal, j'implémenterai les champs (ou `fields`) et je créerai les objets métier (`entities`) dans l'interface d'administration pour l'exporter dans le module. Pour le bloc (`block`), cela dépendrait de la situation mais en étant maintenant beaucoup plus à l'aise avec JavaScript, je testerai la mise en forme avec du JavaScript.

Si vous avez des conseils sur le développement Drupal, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

Si vous souhaitez contribuer, le dépôt est sur Gitlab : [https://gitlab.com/trambi/drupal_module_just_rank_games](https://gitlab.com/trambi/drupal_module_just_rank_games) et les contributions sont les bienvenues.
