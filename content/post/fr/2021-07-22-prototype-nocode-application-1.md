---
title: "Coder un prototype une application pour faire du nocode - partie 1"
date: "2021-07-22"
draft: false
description: "cela paraît paradoxal mais très instructif"
tags: ["projet perso","javascript","nocode","interpreteur"]
author: "Bertrand Madet"
---

Avec un collègue, nous avons décidé de prototyper une application `no code`. L'idée est de permettre à des personnes n'ayant pas de compétences de développement en JavaScript de réaliser des compteurs utiles pour leur métier. Mon collègue a créé l'interface de création du flux et je me suis occupé de la partie d'exécution.

## Principe

Pour le prototype, l'idée était de créer quelques composants (appelés bloc par la suite) pour tester l'utilité de laisser aux utilisateurs composer des blocs simples.

Par exemple, avec 4 blocs :

- un bloc `input` pour entrer une date;
- un bloc `aWeekAgo` pour décaler une date d'une semaine;
- un bloc `comptage` pour réaliser une comptage à partir d'une API;
- un bloc `compteur` pour afficher un nombre.

On peut faire une compteur simple :

```mermaid
graph LR
input[input]-->magic[comptage]
magic-->counter[Compteur]
```

et le même compteur d'une semaine auparavant :

```mermaid
graph LR
input[input]-->weekAgo[aWeekAgo]
weekAgo-->magic[comptage]
magic-->counter[Compteur]
```

La composition des blocs revient à composer les fonctions sous-jacentes au bloc. Et avec l'ajout de blocs, le nombre de combinaison augmente.

## Complication

Ca se complique si on veut que les blocs puissent avoir plusieurs entrées qui viennent de différents blocs. Cela fait qu'il faut considérer des ports d'entrée mais cela ouvre des possibilités.

```mermaid
graph LR
inputFilm[input nom du film]-->magic1[Compteur d'entrée pour un film et sur une période 1]
inputStartDate[input date du début]-->magic1
inputEndDate[input date de fin]-->magic1
inputStartDate-->weekAgo[aWeekAgo]
inputEndDate-->weekAgo
weekAgp-->magic2[Compteur d'entrée pour un film et sur une période 2]
inputFilm-->magic2
magic-->counter1[Compteur 1]
magic2-->counter2[Compteur 2]
```

Il y a plusieurs problèmes à régler :

- la dépendance des blocs ;
- l'association entre les ports

### Dépendance des blocs

Si on s'amuse à tracer les dépendances des blocs pour le premier compteur de l'exemple ci-dessus:

```mermaid
graph TB
counter1[Compteur 1]-->magic1[Compteur d'entrée pour un film et sur une période]
magic1-->inputFilm[input nom du film]
magic1-->inputStartDate[input date du début]
magic1-->inputEndDate[input date de fin]
```

Personnellement, cela me rappelle un `AST` - ( **A**bstract **S**yntax **T**ree  cf. Boite à outils pour tournoi [partie 2]({{< ref "/post/fr/2020-12-09-tournament-tool-box-part-2.md" >}})) et il y a une similitude certaine avec un interpréteur.

D'un point de vue implémentation, cela consiste à faire un tableau de objets dont l'objet dépend.

Pour afficher le compteur, on va avoir besoin de résoudre la valeur du compteur d'entrée. Pour déterminer la valeur du compteur d'entrée, il faut résoudre les variables des inputs du nom du film, date du début et date de fin puis exécuter la fonction compteur avec les entrées.

### Mapping des ports de sortie et des ports d'entrées

L'idée est de gérer en paramètre et en valeur de retour des objets (au sens JavaScript).

On cherche les mappings comme un tableau de structure ci-dessous.

```javascript
{
    source,//source
    inputPort,//nomDuPortDEntree
    outputPort,//nomDuPortDeSortie
}
```

Une fois qu'on a les résultats des blocs dont dépendent le bloc - (que j'appelle les sources), il faut parcourir la description des associations et pour la source extraire le champ de valeur de retour (port de sortie) et le copier dans le champ de paramètre (port d'entrée). Une fois, les résultats des sources récupérés et le paramètre d'appel à la fonction reconstitué, on peut lancer la fonction du bloc.

## Conclusion

Grâce au super travail de mon collègue sur l'interface et à des sessions de codage (un peu tardives), nous avons un embryon d'application. Il reste à travailler sur l'aspect asynchrone mais cela fera l'objet d'une deuxième partie.

Si vous voulez partager vos projets personnels et les enseignements que vous en avez tiré, partagez les à moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
