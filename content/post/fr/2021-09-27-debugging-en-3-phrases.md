---
title: "Déboguer en 3 phrases"
date: "2021-09-27"
draft: false
description: "Le plus dur est avant de corriger"
tags: ["méthodologie","debug","base","3 phrases"]
author: "Bertrand Madet"
---

La correction de bogue (ou bug :bug: en anglais :gb:) ou debugging est une activité jugée peu reluisante. Personne n'aime revenir sur le travail d'autrui (autrui pouvant soi-même d'hier ou un autre développeur il y a six mois) mais cela arrive et il faut corriger le problème. 

J'écris ce billet pour résumer en 3 phrases ma démarche pour déboguer  :

1. Commencez par reproduire le problème ;
1. Ensuite simplifiez le problème ;
1. Enfin corrigez le problème.

Explications... :teacher:

## Commencez par reproduire le problème

Nous (et moi le premier) avons souvent tendance à nous précipiter sur le code, en effet le résultat visible du monde extérieur est la correction du bogue. Mais pour éviter que le même bogue ou un bogue voisin ne réapparaisse, il est vital de l'observer dans l'environnement où a été signalé. Ce ne pas toujours possible, dans ce cas, il est nécessaire de le reproduire dans un environnement au plus proche de celui dans lequel le bogue a été observé. Cela peut être l'environnement de préproduction pour la production. Pour le développement web, l'utilisation du même navigateur est souvent un plus.

Si vous n'arrivez pas à reproduire le problème, demandez à la personne qui a signalé le bogue, si elle a réussi à le reproduire. Si la réponse est non, bon courage, il faut essayer de retracer le problème à partir des journaux de l'application, des messages d'erreur ou d'avertissement du navigateur.

## Ensuite simplifiez le problème

Une fois que vous avez réussi à reproduire le bogue, vous pouvez commencer à utiliser vos outils de débogage. Pour les problèmes web, les outils de développement (console, inspecteur HTML et réseau) des navigateurs sont très utiles. Pour React, il y a une extension (pour Firefox, Chrome ou même Edge),`React Developer Tools`, qui permet de voir les propriétés et états des composants.

Le but est de comprendre le mieux possible les conditions de l'apparition du bogue et de trouver les étapes pour reproduire le plus simplement possible le bogue. Cela va permettre de vérifier plus rapidement et de simplifier la suite.

## Enfin corrigez le problème

Vous pouvez vous plonger dans le code mais pas de le code de production dans celui des tests unitaires. Si vous n'avez pas de tests unitaires, les bogues existent et le prouvent en ce moment même, donc c'est une excellente occasion de commencer.

Vous modifiez ou ajoutez un test en vous basant sur votre travail préliminaire, vous vérifiez que le test échoue. Si le test échoue, félicitations vous avez trouvé le test qui va vous indiquer si le bogue est corrigé aujourd'hui et jusqu'à la fin du projet.

Maintenant, vous pouvez modifier le code de production pour corriger le bogue, il vous reste à vous assurez que :

- le nouveau test passe avec succès ;
- le bogue dans sa version simplifié n'apparaît plus ;
- le bogue initial n'apparaît plus.

Vous pouvez sauvegarder votre travail.

## Conclusion

J'espère que ce billet vous aura apporter un autre point de vue sur le debugging. Il y a plein de cas où la situation peut dégénérer comme un bogue non reproductible ou un bogue qui n'apparaît que dans un environnement différent de celui de développement mais la trame est là.

Comme souvent c'est une affaire de discipline et il faut prendre son temps. Si vous voulez discuter de pratique de développement, contactez moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
