---
title: "Domain Modeling Made Functional - Fiche de lecture"
date: "2022-08-01"
draft: false
description: "Livre de Scott Wlaschin sur le DDD et la programmation fonctionnelle"
tags: ["fiche de lecture","programmation fonctionnelle","DDD"]
author: "Bertrand Madet"
---


## A qui s'adresse ce livre

A celles et ceux qui s'intéressent à la conception pilotée par le domaine (ou :gb: **D**omain-**D**riven **D**evelopment). Si vous êtes intéressé par une application concrète de la  programmation fonctionnelle, la troisième partie illustre bien le paradigme de la programmation fonctionnelle.

## Ce qu'il faut retenir

Le `DDD` se base sur l'élaboration et l'utilisation d'un langage ubiquitaire :teacher: (partagé par les parties prenantes : experts du domaine et membres de l'équipe).

Le système de type du `F#` basé sur l'algèbre des types permet une modélisation lisible également pour les spécialistes du domaine. Il constitue la présentation du langage ubiquitaire.

Couplé avec le typage strict, l'approche proposée assure la validité des traitements à la compilation. Cela permet de repérer certaines erreurs avant même les tests unitaires.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Le cas concret constitue un fil rouge pour la mise en pratique du DDD de l'Event Storming à l'évolution du modèle| :-1: On perd un peu la distinction entre DDD, architecture en oignon et programmation fonctionnelle |
| :+1: La gestion des aspects complexes de la programmation fonctionnelle : gestion des erreurs, sérialisation| :-1: L'implémentation en `F#` peut perturber celles et ceux qui comme moi ne connaissent pas ce langage|

Ce livre permet d'exploser clairement le DDD en marche grâce à un cas concret déroulé entièrement. Les explications sont suffisamment claires pour m'encourager à me lancer dans des sessions de DDD par la pratique mais nous en reparlerons une autre fois.

La troisième partie permet de combler le manque que j'avais ressenti à la [lecture de Composing Software]({{< ref "/post/fr/2021-02-21-composing-software-lecture.md" >}}) : les moments où les fonctions sont impures (c'est à dire quand une fonction ne retourne pas la même sortie pour des entrées identiques). Les illustrations et le code illustrent l'utilité des adaptateurs pour gérer les erreurs ou l'adaptation des entrées ou des sorties de fonction.

Cela en fait un ouvrage très intéressant pour la compréhension de la programmation fonctionnelle.

## Détails

En anglais :gb: :

- Domain Modeling Made Functional par Scott Wlaschin
- ISBN-13 : 978-1-68050-254-1

Pas traduit en français :cry:
