---
title: "Stratégie de test - partie 1"
date: "2022-12-22"
draft: false
description: "La répartition des tests et facteurs à prendre en compte"
tags: ["test","Software Craftmanship"]
author: "Bertrand Madet"
---

Les coding dojos sont l'occasion de discussions autour des tests. Le sujet amène plein de questions :

- Quelle est la couverture à viser ?
- Que tester et ne pas tester ?
- Quels types de tests utiliser pour quelles fonctionnalités ?

Cela peut être vertigineux. :exploding_head:

Une stratégie de test va essayer de répondre à ces questions de manière cohérente pour votre logiciel. La mauvaise nouvelle est qu'il n'y a pas de stratégie facile, efficace et peu chère. La bonne nouvelle est que beaucoup de personnes ont eu le problème et qu'il y a beaucoup d'éléments de réponse pour composer une stratégie adaptée à son logiciel. Dans ce billet, je vous propose de faire un tour des répartitions possibles des types de tests et des facteurs à prendre en compte.

Il est souhaitable d'être au point sur les différents types de tests cf. [Classification des tests]({{< ref "/post/fr/2022-04-26-test-classification.md">}}) ou l'utilité des tests cf. [Tests de composants React - partie 1]({{< ref "/post/fr/2021-02-07-tests-react-components-part1/index.md">}}).

## Les répartitions de tests possibles

### La pyramide des tests

Une approche classique décrite par Martin Fowler part d'un principe très simple : tester rapidement et détecter le plus tôt possible les défauts. Il en résulte une structure pyramide avec :

- une base de nombreux tests unitaires rapides car en isolation ;
- une étage intermédiaire de tests d"intégration en quantité suffisante pour écarter les problèmes de rupture d'interface des modules ;
- une pointe de tests de bout en bout pour s'assurer que le système fonctionne.

L'article [The pratical Test Pyramid (https://martinfowler.com/articles/practical-test-pyramid.html)](https://martinfowler.com/articles/practical-test-pyramid.html) de Ham Vocke décrit très bien cette stratégie de test.

### L'approche de Kent C. Dodds

Une approche un peu plus récente qui se concentre sur l'expérience utilisateur. Sa représentation ressemble plus à un socle qu'à une pyramide :

- une base large et solide de typage statique et d'analyse de code pour détecter les erreurs au moment d'écrire le code ;
- une seconde et large couche de détection basée sur les tests unitaires ;
- les tests d'intégration permettent de détecter les problèmes d'interface des sous-systèmes ;
- les tests de bout en bout sont plus variés car ils sont plus proches de l'expérience utilisateur.

L'aspect typage statique est mis en avant car ce modèle est adapté aux applications web et donc à JavaScript. Il est évident qu'un défaut est encore plus rapidement corrigé quand il est détecté lors de la saisie du développeur. Cette répartition est illustrée (et vendue) sur le site [Testing JavaScript (https://testingjavascript.com/)](https://testingjavascript.com/).

### Les tests dépendant uniquement des spécifications

Une approche pragmatique et minimaliste, les tests sont définis uniquement pour répondre aux spécifications. La répartition des types de tests dépend entièrement du type de spécification. Il y a fort à parier que les tests de bout en bout et les tests d'intégration seront majoritaires. Cette approche peut être présente dans un projet en cycle en V mais aussi dans un projet agile.

## Les critères à prendre en compte

### Votre équipe

Le premier critère à prendre est l'appétence et la capacité de votre équipe :

- si votre équipe est novice sur les tests, vous pourriez partir sur une répartition simple basée sur des tests documentés dans votre structure ou sur Internet ;
- si votre équipe a la chance d'avoir une personne spécialisée dans les tests, ce sera elle qui donnera le tempo **sans être obligée de coder tous les tests** ;
- si votre équipe est de taille modeste, il faudra sûrement se concentrer sur les fonctionnalités critiques ou sur les parties qui amènent souvent les bugs :bug:.

### La maturité de votre logiciel

Sur un logiciel en cours de développement, vous pouvez commencer par des tests unitaires puis ajoutez des tests d'intégration au fur et à mesure que votre logiciel s'étoffera et que le besoin s'en fera sentir.

Si vous devez écrire une stratégie de test pour un logiciel déjà existant, vous ne pourrez vraisemblablement pas vous mettre à coder tous les tests de votre choix. Vous allez devoir prioriser les fonctionnalités à risque, reprendre les bugs les plus impactant et essayer de coder un test pour éviter toute régression. Vous pouvez même profiter des bugs pour écrire vos premiers tests.

### Les spécifications

Le niveau de détail des spécifications va aussi influencer directement le niveau de détail des tests. Si vous ne savez pas ce que vous devez coder, vos tests risquent d'être flous voire arbitraires. Dans ce cas, il vaut mieux prendre du temps avec votre product owner, votre ingénieur système ou votre chef de projet pour améliorer les spécifications.

### L'environnement

La nature de votre usine logiciel va aussi impacter le type de tests que vous pouvez effectuer. En général, les tests unitaires sont assez facile à intégrer dans l'intégration continue. Les tests d'intégration ou de bout en bout peuvent avoir besoin de d'infrastructure de test qui dépendent de votre environnement.

Par exemple, essayer de faire des tests de bout en bout sur un navigateur Internet Explorer 11 :sob: à partir d'un environnement Linux est très compliqué. De même que tester des solutions sur un type de processeur (M1 par exemple) sans avoir de machines virtuelles adaptée dans votre environnement.

## Conclusion

Parmi les répartitions de référence, les tests unitaires sont toujours les plus nombreux. Les tests unitaires amènent un peu plus que la validation d'une correspondance à une spécification, ils permettent de localiser les sources de problème et de refactorer en confiance.

Le typage statique ou les analyses de code sont aussi la première ligne de défense. Si la pyramide des tests ne les mentionnent pas, car elle a été formalisée à l'époque où Java régnait sur le monde logiciel donc le typage statique allait de soi. Les compilateurs en fonction de leurs options permettaient d'avoir une première analyse du code.

Et vous quelle est votre stratégie de test ? N'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
