---
title: "Pourquoi j'adore Mermaid.js ?"
date: "2020-12-13"
draft: false
description: "Boostez vos documentations avec des graphiques"
tags: ["documentation","markdown","mermaid"]
author: "Bertrand Madet"
---

Aujourd'hui je souhaite vous faire découvrir un outil qui, je l'espère, va améliorer vos documentation : [Mermaid.js (https://mermaid-js.github.io/mermaid/#/)](https://mermaid-js.github.io/mermaid/#/).

## Pourquoi je préfère du markdown pour la documentation

J'utilisais comme beaucoup des fichiers Word pour la documentation de mes projets. Mais le code et la documentation forme un tout. De mon point de vue, le meilleur moyen d'avoir une documentation à jour, c'est de l'avoir au plus près du code, dans le même flux de travail que le code.

Le format binaire de Word (xml compressé pour les dernières versions) est pénible à suivre dans les systèmes de gestion de version : vous ne profitez pas de la puissance de votre système de gestion de version. Avec le format Markdown (langage de formatage texte, simple à lire et à écrire), nous pouvons maintenant intégrer la documentation dans notre gestion de version, Git. 

## Le markdown s'est bien, avec Mermaid.js c'est mieux

Le markdown permet de formater du texte, des listes, des tableaux, d'inclure des liens, des images. 

Si vous incluez des images d'architecture dans votre documentation :

 - soit vous allez devoir stocker les sources de ces images 
 - soit que votre dépôt Git n'est pas autoporteur.

Mermaid.js est une bibliothèque JavaScript qui permet de représenter un graphique à partir d'une description texte et de l'intégrer dans du Markdown. Il permet de stocker les sources de ses schémas sous format texte.

A partir de ce texte, on affiche le schéma ci-dessous :

```
graph LR
  debut-->milieu
  milieu-->|dernier effort|fin
```

```mermaid
graph LR
  debut-->milieu
  milieu-->|dernier effort|fin
```

Mermaid.js permet de faire :

- des dessins de flux ;
- des diagrammes de séquence ;
- des diagrammes de classes ;
- des diagrammes d'état ;
- et d'autres choses...

Je ne sais pas pour vous mais pour moi, quelque chose qui permet à partir de ce code, d'afficher le diagramme de séquence ci-dessous, je trouve ça génial.

```
sequenceDiagram
    participant Alice
    participant Bertrand

    Alice->>John: Hello John, how are you?
    loop Healthcheck
        John->>John: Fight against hypochondria
    end
    Note right of John: Rational thoughts <br/>prevail!
    John-->>Alice: Great!
    John->>Bob: How about you?
    Bob-->>John: Jolly good!
```

```mermaid
sequenceDiagram
    participant Alice
    participant Bertrand

    Alice->>John: Hello John, how are you?
    loop Healthcheck
        John->>John: Fight against hypochondria
    end
    Note right of John: Rational thoughts <br/>prevail!
    John-->>Alice: Great!
    John->>Bob: How about you?
    Bob-->>John: Jolly good!
```

Comme vous pouvez le voir, le schéma est décrit de manière textuel, comme du code en fait. Et les modifications sont faciles à suivre sur Git.

## Installation de mermaid.js

Il faut savoir que le script mermaid prend le code dans des balises spécifiques et les convertit en SVG. 

Si vos descriptions sont déjà dans des balises  `<div class="mermaid">`, une simple inclusion en fin de fichier du script `<script src="chemin/vers/mermaid.js"></script>` et l'exécution de mermaid `<script>mermaid.initialize({startOnLoad:true, securityLevel: 'loose'});</script>` suffit à faire afficher tous les graphiques de la page.

### GitLab

C'est déjà intégré dans GitLab dans la visualisation et la prévisualisation des pages Markdown (avec l'extension `.md`). :+1:

## Conclusion

J'adore mermaid.js et j'espère que ce billet vous aura donner envie de l'utiliser.  Si vous avez des remarques, discutons-en sur [Twitter](https://twitter.com/trambi_78).

En bonus, j'ai créé un [billet sur l'installation de Mermaid.js dans le générateur de site statique Hugo]({{< ref "/post/fr/2020-12-13-mermaid-js-hugo.md" >}}).

*09/01/2021: reformulation de certaines phrases des première et deuxième parties.*
