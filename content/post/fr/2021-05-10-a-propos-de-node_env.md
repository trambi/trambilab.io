---
title: "À propos de NODE_ENV"
date: "2021-05-10"
draft: false
description: "l'influence de cette variable d'environnement sur npm et Strapi"
tags: ["nodejs"]
author: "Bertrand Madet"
---

La semaine dernière, nous avons eu un problème lors de la mise en production d'une application Node.js basée sur Strapi. Après une enquête, il s'avère que c'était lié à la variable d'environnement `NODE_ENV`. Je vous propose donc de faire le point sur l'influence de `NODE_ENV` sur npm et d'autres logiciels de l'écosystème Node.js.

## npm

D'après la documentation de la [ligne de commande npm (https://docs.npmjs.com/cli/v7/commands/npm-install)](https://docs.npmjs.com/cli/v7/commands/npm-install), la variable d'environnement n'a d'influence sur `install` que quand elle vaut `production`. Dans ce cas, npm va installer uniquement les dépendances `dependencies` indiquées dans votre fichier de configuration `package.json` et pas les dépendances de développement `devDependencies`.

L'option `--production` permet de modifier ce comportement et prendre le pas sur la variable d'environnement.

La [commande `ci`(https://docs.npmjs.com/cli/v7/commands/npm-ci)](https://docs.npmjs.com/cli/v7/commands/npm-ci) se comporte de la même manière.

| \ |`NODE_ENV` fixé à `production`|Autres cas|
|---|--------------------------|----------|
|sans option|Installe uniquement les dépendances|Installe les dépendances et les dépendances de développement|
|`--production=false`|Installe les dépendances et les dépendances de développement|Installe les dépendances et les dépendances de développement|
|`--production=true`|Installe uniquement les dépendances|Installe uniquement les dépendances|

## Strapi

[Strapi (https://strapi.io)](https://strapi.io) est un système de gestion de contenu (ou CMS) qui fournit des API Rest à la place des traditionnelles pages (on dit que c'est un headless CMS). Strapi utilise la variable d'environnement pour définir la configuration à utiliser (cf. <https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html#environment>). Ce qui veut dire que le fait de définir `NODE_ENV` à `myenv` indique à Strapi d'aller chercher sa configuration dans `config/env/myenv/`.

## Autres logiciels de l'écosystème

### yarn

La version 2 du gestionnaire de paquet yarn ne semble pas utiliser cette variable d'environnement.

### Gatsby

[Gatsby (https://www.gatsbyjs.com/)](https://www.gatsbyjs.com/) est un générateur de site web statique. La documentation indique juste que `NODE_ENV` est utilisé en interne de gatsby. Une rapide analyse des sources de Gatsby permet de voir que les valeurs possibles sont `test` et `production`.

## Et Deno ?

[Deno (https://deno.land/)](https://deno.land/) n'a pas cette problématique car il n'utilise pas de gestionnaire de paquet car cela n'est pas dans la philosophie de Deno. Il ne peut pas y avoir d'interaction entre les exécutables et le gestionnaire de paquet.

## Revenons au problème

Le problème venait d'une valeur différente de `NODE_ENV` sur les plateformes de production et de pré-production pour différencier les fichiers de configuration de Strapi. La plateforme de production ayant la valeur `production`, cela menait à une installation sans dépendances de développement alors que la plateforme de pré-production lançait son installation avec les dépendances de développement.

Pour résoudre le problème, je pense qu'il faudra ajouter l'option `--production=true` dans les étapes d'intégration continue pour capturer au plus tôt les problèmes qui pourraient arriver en production.

## Conclusion

Voici l'illustration de la difficulté de mettre au point les plateformes de pré-production, où il faut s'approcher le plus possible de la plateforme de production.

Si vous avez des retours sur ce billet ou des découvertes sur Node.js et npm, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
