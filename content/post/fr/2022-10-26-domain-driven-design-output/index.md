---
title: "Capitaliser les résultats du DDD"
date: "2022-10-26"
draft: false
description: "pour partager et maintenir la démarche"
tags: ["DDD","projet perso"]
author: "Bertrand Madet"
---

Suite à mon article sur les ateliers sur le DDD : [DDD par la pratique]({{< ref "/post/fr/2022-09-27-domain-driven-design-by-trying.md">}}), j'ai essayé de formaliser le résultat de la démarche pour le soumettre à d'autres camarades d'organisation de tournoi.

Voici l'état de mes réflexions sur la question.

## Lieu et format

Comme moyen d'intermédiation entre les métiers et les développeurs, cette documentation doit être accessible rapidement pour les développeurs. L'endroit le plus adéquate est au plus près du code, dans le dépôt de gestion de configuration.

Pour le format et dans la mesure où la documentation est dans le dépôt de gestion de configuration, le markdown me semble le format idéal :

- bien géré par Git ;
- expressif ;
- intègre bien les images;
- intégré dans Gitlab et GitHub.

## Les sorties

### Glossaire

La première sortie à capitaliser est le glossaire, que l'on peut formaliser comme une liste de définition. Cela se représente en markdown.

```markdown
- Bloodbowl : Jeu de plateau qui simule un sport mélange de football américain 
et de rugby dans un univers fantastique peuplé d'elfes, d'orques ;
- Coach : Personne physique qui joue à Bloodbowl ;
- Confrontation : Partie entre deux triplettes - 3 matchs ;
```

Cet extrait s'affiche :

- Bloodbowl : Jeu de plateau qui simule un sport mélange de football américain 
et de rugby dans un univers fantastique peuplé d'elfes, d'orques ;
- Coach : Personne physique qui joue à Bloodbowl ;
- Confrontation : Partie entre deux triplettes - 3 matchs ;

Il a été facile de transformer les textes dans [Excalidraw (https://excalidraw.com/)](https://excalidraw.com/) en liste avec ce format.

### Frise des évènements

La frise des évènements a été réalisée en fin d'`event storming` en ordonnant les évènements métier.

Actuellement, la frise des évènements est sous forme d'une capture du résultat des sessions de DDD par la pratique par Excalidraw. Cela ressemble à ça ![Frise des évènements](./frise-evenements.png).

Pour le formaliser de manière plus textuelle et moins foisonnante, on peut imaginer de découper la frise par contextes délimités et d'utiliser [Mermaid (https://mermaid-js.github.io/)](https://mermaid-js.github.io/).

### Contextes délimités

Les contextes délimités sont l'interprétation des domaines, il s'agit d'un choix architectural. Initialement, nous avons travaillé sur une surimpression de la frise des évènements : ![Contextes délimités](./contextes.png).

Je pense que l'on peut le formaliser avec une diagramme de graphe en tant que code.

```mermaid
graph LR;
    subscribe(Inscription)-->ready(Présence)
    ready-->exec(Exécution)
    exec-->prizes(Cérémonie des prix)
    exec-->naf(NAF)
    exec-->comm(Communication)
```

On peut parvenir à ce résultat avec mermaid et le code suivant intégré dans markdown :

```mermaid
graph LR;
  subscribe(Inscription)-->ready(Présence)
  ready-->exec(Exécution)
  exec-->prizes(Cérémonie des prix)
  exec-->naf(NAF)
  exec-->comm(Communication)
```

## Conclusion

Pour l'instant, le [document est disponible dans le dépôt GitHub : https://github.com/trambi/FantasyFootballArchitecture/blob/main/domain/README.md](https://github.com/trambi/FantasyFootballArchitecture/blob/main/domain/README.md). Cela va me permettre de partager de manière plus intelligible avec mes comparses, organisateurs de tournoi.

La documentation est loin d'être complète, il manque notamment :

- les entités métier ;
- les processus métier ;
- les traductions métier.

Je n'ai pas pris le temps de capitaliser sur ces éléments, je pense que cela sera plus pertinent avec les retours d'autres experts métier. Il faudra trouver le format adéquate pour chacun de ses éléments. L'idée est de garder la documentation à jour pour garder le langage commun en phase entre le métier et les développeurs.

Si vous avez des conseils ou des expériences à partager sur le `DDD`, contactez moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
