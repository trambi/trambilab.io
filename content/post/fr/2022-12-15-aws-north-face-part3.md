---
title: "Décrire une API Gateway avec OpenAPI et Terraform"
date: "2022-12-15"
draft: false
description: "Quand documenter permet de simplifier la description de son infrastructure"
tags: ["outils","REST API","cloud","documentation","terraform"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [Décrire une Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).

-------

Dans mon billet sur l'[accès S3 via API Gateway]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}), j'avais décrit le service API Gateway entièrement avec Terraform. Pour se rafraîchir la mémoire, il avait fallu décrire sept ressources Terraform pour décrire l'API Gateway. La bonne nouvelle c'est qu'il est possible d'utiliser un fichier OpenAPI pour décrire une API d'API Gateway via la console web, CloudFormation ou ... Terraform.

Nous allons donc voir aujourd'hui la description d'une API REST au format OpenAPI 3.0 pour la rendre utilisable par Terraform. Si vous ne connaissez pas OpenAPI (ou Swagger), je vous conseille de faire un détour par la [documentation (https://swagger.io/docs/specification/about/)](https://swagger.io/docs/specification/about/) ou par mon billet : [Documentation d'une API REST avec OpenAPI]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}).

## Pourquoi c'est bien ?

Avant de commencer à expliquer le comment, essayons de réfléchir pourquoi ce serait bien. Dans un projet mature, une API existera :

- sous la forme de code ou d'agrégat de code (avec les lambdas) qui permet d'implémenter ce que fait l'API ;
- sous la forme d'une description d'infrastructure qui permet de créer, modifier ou détruire les services accueillant l'API;
- sous la forme d'une documentation qui permet de comprendre l'API.

Même si on voit les intentions sont différentes, cela fait quatre (4) occasions d'avoir des incohérences :

- soit le code est incohérent de l'infrastructure et de la documentation ;
- soit l'infrastructure est incohérente du code et de la documentation ;
- soit la documentation est incohérente du code et de l'infrastructure ;
- soit les trois (3) sont incohérents.

Si on arrive à décrire l'infrastructure pour qu'elle soit toujours cohérente à la documentation, cela permettra :

- d'être sûr que la documentation est au moins partiellement à jour :tada: ;
- d'avoir moins de contenu à gérer :zap: ;
- qu'en cas d'incohérence, le contrat de l'API n'est pas respectée par son implémentation :memo:.

## Pourquoi c'est un peu plus compliqué que prévu

La fonctionnalité est bien mise en avant dans la documentation sur la ressource [aws_api_gateway_rest_api (https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_rest_api)](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_rest_api), malheureusement les exemples sont simplistes.

Les paramètres d'intégration de l'API Gateway (qui décrivent ce que fait l'appel à méthode sur une ressource) sont statiques et surtout le contenu est une chaîne de caractère et non un fichier. Le dernier point est embêtant, on imagine mal aller lire la documentation dans un fichier Terraform. Sans compter que nous perdrions l'intégration de Swagger/OpenAPI à GitLab qui permet de consulter un fichier de documentation de manière très sympa.

Heureusement, il y a deux (2) fonctions intégrées à Terraform :

- [file (https://developer.hashicorp.com/terraform/language/functions/file)](https://developer.hashicorp.com/terraform/language/functions/file) qui permet d'intégrer le contenu d'un fichier dans une description Terraform ;
- [templatefile (https://developer.hashicorp.com/terraform/language/functions/templatefile)](https://developer.hashicorp.com/terraform/language/functions/templatefile), qui permet d'intégrer un fichier modifier en remplaçant des variables par des valeurs, de faire des boucles.

Dans le cas de mon partage de S3, je vais utiliser des noms différents pour le bucket S3 en fonction de l'environnement, donc je vais utiliser templatefile.

```HCL
# à la place des ressources de type `aws_api_gateway_resource`, `aws_api_gateway_method`, 
# `aws_api_gateway_integration`, `aws_api_gateway_method_response` et `aws_api_gateway_method_integration_response`
resource "aws_api_gateway_rest_api" "front" {
  name = "front"
  body = templatefile("./front-openapi3.json", {
    api-gateway-role-arn                = aws_iam_role.front-api-gateway.arn,
    s3-basepath-uri    = "arn:aws:apigateway:${var.region}:s3:path/${module.mons3.id}",
  })
  put_rest_api_mode = "merge"
}
```

Le fichier de description OpenAPI est en JSON car Terraform l'attend dans ce format. Les éléments d'intégration à API Gateway sont définis dans `x-amazon-apigateway-integration`. Les variables sont précédées de `${` et terminées par `}`.

```json
{
  "openapi": "3.0.3",
  "info": {
    "title": "Front API",
    "version": "1.0.0"
  },
  "paths": {
    "/web/{item}": {
      "get": {
        "description": "Enable access to S3 item",
        "parameters": [
          {
            "name": "item",
            "in": "path",
            "required": true,
            "description": "Item to get",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Operation succeed",
            "headers": {
              "Access-Control-Allow-Origin": {
                "schema": {
                  "type": "string"
                }
              },
              "Content-Type": {
                "schema": {
                  "$ref": "#/components/schemas/ContentType"
                }
              }
            },
            "content": {}
          }
        },
        "x-amazon-apigateway-integration": {
          "credentials": "${api-gateway-role-arn}",
          "httpMethod": "GET",
          "uri": "${s3-basepath-uri}/{item}",
          "responses": {
            "default": {
              "statusCode": "200",
              "responseParameters": {
                "method.response.header.Content-Type": "integration.response.header.Content-Type",
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            }
          },
          "requestParameters": {
            "integration.request.path.file": "method.request.path.file"
          },
          "passthroughBehavior": "when_no_match",
          "timeoutInMillis": 29000,
          "type": "aws"
        }
      },
      "options": {
        "description": "Accelerate access to files",
        "parameters": [
          {
            "name": "x-amz-meta-fileinfo",
            "in": "header",
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "item",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Operation succeed",
            "headers": {
              "Access-Control-Allow-Origin": {
                "schema": {
                  "type": "string"
                }
              },
              "Access-Control-Allow-Methods": {
                "schema": {
                  "type": "string"
                }
              },
              "Access-Control-Allow-Headers": {
                "schema": {
                  "type": "string"
                }
              },
              "Content-Type": {
                "schema": {
                  "$ref": "#/components/schemas/ContentType"
                }
              }
            },
            "content": {}
          }
        },
        "x-amazon-apigateway-integration": {
          "responses": {
            "default": {
              "statusCode": "200",
              "responseParameters": {
                "method.response.header.Access-Control-Allow-Methods": "'GET,OPTIONS'",
                "method.response.header.Content-Type": "integration.response.header.Content-Type",
                "method.response.header.Access-Control-Allow-Headers": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,x-amz-meta-fileinfo'",
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            }
          },
          "requestTemplates": {
            "application/json": "        {\n        \"statusCode\" : 200\n        }\n"
          },
          "passthroughBehavior": "when_no_match",
          "timeoutInMillis": 29000,
          "type": "mock"
        }
      }
    }
  },
  "components": {
    "ContentType": {
      "type": "string",
      "description": "MIME type of a file or a S3 object"
    }
  }
}
```

J'ai enlevé un peu plus de cent trente (130) lignes de description d'API Gateway pour ajouter un peu plus de cent trente (130) lignes de documentation d'API. Je gagne une documentation d'API !

## Conclusion

Il est possible de se servir de la console web AWS pour créer les ressources, les méthodes et les intégrations associées puis d'exporter au format OpenAPI (:warning: en choisissant l'option avec les "amazon-apigateway-integration"). On va ensuite repérer ce qui peut changer dans le fichier et le remplacer par des variables. On voit que la démarche vaut le coup puisqu'on obtient une documentation et on simplifie la description de l'infrastructure. La démarche peut être adaptée à CloudFormation ou si vous n'utilisez pas l'infrastructure as code pour sauvegarder (partiellement) votre configuration API Gateway.

J'ai appliqué cette démarche avec le même bonheur pour l'appel à des lambdas. On pourrait aussi imaginer se servir de la documentation d'API pour créer des tests automatiques de déploiement.

Si vous avez des questions ou des retours d'expériences sur l'utilisation d'AWS avec Terraform, je suis intéressé. N'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

-------

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [Décrire une Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).
