---
title: "50 Algorithms Every Programmer Should Know - Fiche de lecture"
date: "2024-08-25"
draft: false
description: "Deuxième édition du livre sur les algorithmes d'Imran Ahmad, PhD"
tags: ["fiche de lecture","Software Craftmanship","base"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Aux personnes qui souhaitent faire un tour synthétique des algorithmes : du tri aux réseaux de neurones en passant par le parcours de graphe ou ceux liés à la cryptographie.

## Ce qu'il faut retenir

La sélection d'un algorithme dépend évidemment du problème à résoudre mais aussi de critères comme l'explicabilité, la précision ou la rapidité... Ces critères dépendent du contexte du problème et du déploiement de la solution.
Plus d'un tier du livre est consacré aux algorithmes d'apprentissage statistique (*Machine Learning*).

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Grande variété des algorithmes expliqués | :-1: Peut-être trop le grand écart réseau de neurones, cryptographie peut causer des maux de tête 🤯 |
| :+1: Les limites et les cas d'usage sont systématiquement intégrés aux algorithmes | :-1: Le code python utilise des bibliothèques de haut niveau comme Tensorflow. Le code illustre parfois plus l'utilisation de l'algorithme qu'il n'explique son fonctionnement.  |
| :+1: Le pseudo code, le code python, les formules mathématiques et les schémas complètent les explications||

Ce livre est complet et j'ai été étonné de trouver autant de description d'algorithmes d'apprentissage statistique. Peut-être que ma vision est biaisée par l'organisation des tâches dans notre entité. Les algorithmes expliqués sont très variés, j'ai été content d'étudier plus en détails des algorithmes déployés d'habitude pour mes collègues.

Chaque algorithme est expliqué. L'explication intègre le contexte, les cas d'usage et les limites et est complétée par les formules mathématiques, des schémas et du code Python.

Je reste sur ma faim sur la partie `Réseaux de neurones` où si les architectures sont détaillées, le code Python illustre plus l'usage que l'algorithme en lui même.. C'est sans doute lié à mon manque de compétences dans le domaine et au fait que sur ma liseuse les schémas et les formules mathématiques étaient peu lisible 👓.

Ce livre peut aussi servir de *bestiaire* des algorithmes à consulter en fonction des problèmes à régler plutôt qu'en le lisant en entier.

## Détails

- Titre : 50 Algorithms Every Programmer Should Know - Tackle computer science challenges with classic to modern algorithms in machine learning, software design, data systems, and cryptography
- Auteur : Imran Ahmad, PhD
- Langue : 🇬🇧
- ISBN : 978-1803247762
