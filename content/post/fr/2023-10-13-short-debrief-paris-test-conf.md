---
title: "Rapide debrief de Paris Test Conf 2023"
date: "2023-10-13"
draft: false
description: "La conférence sur les tests logiciels"
tags: ["test","conference"]
author: "Bertrand Madet"
---

Le 10 octobre 2023 avait lieu à Paris la [Paris Test Conf (https://paristestconf.com/edition-2023/)](https://paristestconf.com/edition-2023/). J'ai eu la chance d'y assister donc je vous propose un rapide debrief.

La Paris Test Conf est une rassemblement pour échanger sur la qualité logicielle. Les précédentes éditions avaient lieu sur Internet et cette année était le retour au présentiel. L'assistance était constituée, je dirais, de de 75% "testeurs" et de 25% de "développeurs".

## Description des présentations auxquelles j'ai assisté

Keynote en plénière "Les défis de la qualité durable" de Marc Hage Chahine et Stanilas Bobiec. Une présentation sur l'intégration de la prise en compte de la durabilité dans les normes de qualité logicielle (ISO 25010). J'ai trouvé cette présentation intéressante mais un peu trop superficielle. J'aurai préféré une présentation plus centrée sur certains points spécifiques afin d'être plus activable.

Présentation "PostgreSQL : ceux qui errent ne sont pas toujours perdus" de Laetitia Avrot dans la deuxième salle. Une présentation très bien menée sur l'organisation et le mode de fonctionnement du projet libre PostgreSQL. Une démonstration que la qualité logicielle peut venir aussi de la passion et d'un mode de fonctionnement très rigoureux. La présentation était pleine de conviction, j'aurai apprécié un peu plus de données ou même voir un peu le code (bon en même je peux le voir sur git.postgresql.org).

Présentation "Testeur et Agilité : un mode opératoire quand on est entouré de 25 développeurs" d'Anaïs Fournier dans la deuxième salle. Le récit de l'arrivée d'une testeuse qui doit faire son métier avec 5 équipes de 25 développeurs en tout. Même si c'était sa première présentation et que le stress était là, la présentation était incarnée et intéressante sur la problématique en particulier sur la priorisation des tests manuels et exploratoires.

Keynote "Tenir ses promesses !" de Cécile Roche, une présentation très maîtrisée sur les liens entre le Lean et la qualité logicielle. L'association entre la qualité et la promesse étant que là où les performances d'un produit sont une promesse, la qualité du produit est la promesse tenue. Dans les points saillants de cette présentation, je retiens qu'il faut d'abord bien cerner ce que l'on doit faire avant de le faire bien : *Do the right thing ... then do it right*.

Présentation "Découvrir le rôle du Quality Coach et sa boite à outils" d'Emna Ayadi dans la salle principale. . La présentatrice m'a semblé très stressée au début. Le partage d'expérience sur la prise de poste de "Quality Coach" a permis de circonvenir à cette sensation de stress. Je retiens dans la boîte à outils : le bilan de santé de l'équipe (ou :gb: *Squad Health Check* ) et le *SOON Funnel*.

Présentation "Comment une conception logicielle influence votre stratégie de test" de Christophe Breheret-Girardin dans la deuxième salle. Une présentation sur les différentes architecture logiciel en fonction des domaine d'emploi (au sens DDD). Chacunes des architectures logicielle ayant des répartitions entre les tests unitaires, les tests d'intégration et les tests de bout en bout différentes. La présentation était très carrée, un seul conseil ne pas se mettre au fond de la salle, si on veut avoir des goodies.

Keynote " Quality Engineering : La croissance durable par la qualité" d'Antoine Craske. Une présentation très dense sur la montée en maturité de qualité logicielle. L'intervenant détaille une approche systémique de la qualité logicielle via le cadre MAMOS (**M**ethods **A**rchitecture **M**anagement **O**rganisation **S**kills). La présentation était très intéressante mais trop dense pour moi en cette fin de journée.

## Conclusion

J'ai trouvé cette conférence sur la qualité logicielle très sympa. Le niveau d'expérience des intervenant.e.s très hétérogène mais cela a permis d'assister des présentations plus personnelles et rares. Ce genre de conférence permet de prendre du recul sur la qualité logicielle dans son organisation.

J'espère que ce billet vous permettra de sélectionner les vidéos qui ne devraient pas tarder à sur la [chaîne ParisTestConf (https://www.youtube.com/@paristestconf/videos)](https://www.youtube.com/@paristestconf/videos). Merci aux organisateurs bénévoles pour cette journée ! :tada:

Si vous avez des retours ou d'autres conseils sur des conférences sur le développement logiciel ou la qualité logiciel, n'hésitez pas à les partager sur [X](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
