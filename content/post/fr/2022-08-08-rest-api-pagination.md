---
title: "Pagination des API REST"
date: "2022-08-08"
draft: false
description: "utilité et interfaces possibles"
tags: ["REST API"]
author: "Bertrand Madet"
---

Récemment, j'ai dû modifier l'implémentation d'une pagination d'API REST pour permettre l'accès à une page arbitraire. Je vous propose de partager le fruit de cette expérience sur la pagination pour les API REST.

Tous les billets concernant les API REST sont [là]({{< ref "/tags/rest-api">}})

:teacher: La pagination consiste pour les endpoints qui renvoient des listes de ressources à renvoyer un sous ensemble du résultat, une page. Nous verrons pourquoi l'implémenter puis les différentes méthodes de pagination.

## Pourquoi utiliser la pagination ?

Imaginons que vous fournissiez une API REST avec un endpoint qui renvoie une liste de ressources correspondante à des filtres. Si un utilisateur interroge votre endpoint sans filtre, il va recevoir l'intégralité de vos objets. C'est anecdotique si vous avez cinquante ressources, mais cela l'est beaucoup moins si vous avez un million de ressources.

En effet, cela va solliciter votre infrastructure (le mécanisme de persistance, le mécanisme d'agrégation, le réseau ...) et celle de votre utilisateur. La pagination facilite le fait de limiter la charge en restreignant le nombre maximal de ressource. En vous assurant que chaque requête ait une charge homogène, vous protégez votre API REST.

:warning: Utiliser la pagination n'est pas une condition suffisante, pour avoir une charge homogène, cela dépend énormément des usages, de l'infrastructure sous jacente ...

## Méthodes d'implémentation

La pagination a un coût à la fois pour le serveur et pour le client de l'API REST. Pour illustrer ce coût dans la suite de billet, nous allons étudier une API REST avec un endpoint qui a comme une url de base `https://myrestapi.org/resources` et dont la réponse avant pagination était telle que ci-dessous.

```json
[
  {
    "id":1,
    "name":"premier"
  },
  {
    "id":2,
    "name":"second"

  }
]
```

Pour savoir combien de page sont disponibles, il faut ajouter au minimum le nombre total de ressources disponibles et séparer cette information des ressources retournées en le mettant dans des métadonnées. On peut imaginer une réponse de type :

```json
{
  "meta":{
    "total":65
  },
  "resources":
  [
    {
      "id":1,
      "name":"premier"
    },
    {
      "id":2,
      "name":"second"
    }
  ]
}
```

### Pagination par décalage

La première implémentation consiste à se rapprocher du modèle SQL et à proposer à avoir deux paramètres :

- le nombre limite de ressources à récupérer, il peut s'appeler `limit` et avoir une valeur par défaut fixée à une valeur de votre choix ;
- le décalage pour la première ressource à récupérer, il peut s'appeler `offset` et avoir une valeur par défaut nulle.

Si la valeur par défaut de `limit` vaut à vingt cinq :

- l'url `https://myrestapi.org/resources` retournera les vingt cinq premières ressources - la première page de vingt cinq ressources ;
- l'url `https://myrestapi.org/resources?offset=25` retournera les vingt cinq après la vingt cinquième ressource - la deuxième page de vingt cinq ressources ;
- l'url `https://myrestapi.org/resources?limit=20&offset=20` retournera les vingt ressources après la vingtième ressource - la deuxième page de vingt ressources.

Pour récupérer tous les ressources, le client doit :

- appeler la première fois l'url avec le paramètre `limit` qui correspond à la taille de la page qu'il souhaite ;
- extraire les ressources de cette première page **et** le nombre total de ressources disponibles ; 
- déterminer le nombre de page en divisant le nombre total de ressources disponibles par la taille d'un page ;
- appeler les autres pages en modifiant le paramètre `offset` dans l'url ;
- extraire les ressources des pages.

Côté serveur, il faut :

- compter le nombre total de ressources disponibles ;
- extraire les ressources correspondantes à `limit` et `offset` - ce qui est en général plutôt facile pour les bases de données ;
- gérer les cas d'erreur pour si on fournit :
  - autre chose que des entiers positifs pour les paramètres - normalement une `erreur 400`,
  - un entier plus que le nombre total de ressources disponible pour `offset` - peut-être une [erreur 404](https://fr.wikipedia.org/wiki/Erreur_HTTP_404);
- documenter ses paramètres et le choix de la taille par défaut des pages.

### Variante avec page et size

Il est possible de proposer des paramètres plus orientés "page" :

- le taille de la page, il peut s'appeler `size` et avoir une valeur par défaut fixée à une valeur de votre choix - oui c'est exactement le même paramètre que `limit` dans l'implémentation précédente ;
- le numéro de la page à récupérer, il peut s'appeler `page` et avoir une valeur par défaut égale à un.

Si la valeur par défaut de `size` vaut à vingt cinq :

- l'url `https://myrestapi.org/resources` retournera les vingt cinq premières ressources - la première page de vingt cinq ressources ;
- l'url `https://myrestapi.org/resources?page=2` retournera les vingt cinq après la vingt cinquième ressource - la deuxième page de vingt cinq ressources ;
- l'url `https://myrestapi.org/resources?size=20&page=2` retournera les vingt ressources après la vingtième ressource - la deuxième page de vingt ressources.

Le reste est identique à l'implémentation précédente à part que cela déplace le calcul des décalages côté serveur en simplifiant les appels côté client.

### Pagination par dernière valeur récupérée

La dernière implémentation, appelée `seek pagination`, propose deux paramètres :

- le nombre limite de ressources à récupérer, il peut s'appeler `size` et avoir une valeur par défaut fixée à une valeur de votre choix - c'est la paramètre classique de la pagination ;
- le dernier identifiant avant la première ressource à récupérer, il peut s'appeler `last_id` et avoir une valeur par défaut nulle.

Si la valeur par défaut de `size` vaut à vingt cinq :

- l'url `https://myrestapi.org/resources` retournera les vingt cinq premières ressources - la première page de vingt cinq ressources ;
- l'url `https://myrestapi.org/resources?last_id=25` retournera les vingt cinq ressources après la ressource dont l'identifiant est `25` ;
- l'url `https://myrestapi.org/resources?size=20&last_id=20` retournera les vingt ressources après la ressource dont l'identifiant est `20`.

Pour récupérer tous les ressources, le client doit :

- appeler la première fois l'url avec le paramètre `size` qui correspond à la taille de la page qu'il souhaite ;
- extraire les ressources de cette première page, le nombre total de ressources disponibles **et** l'identifiant de la dernière ressource ;
- déterminer le nombre de page en divisant le nombre total de ressources disponibles par la taille d'un page ;
- appeler séquentiellement les autres pages en modifiant le paramètre `last_id` dans l'url ;
- extraire séquentiellement les ressources de pages **et** l'identifiant de la dernière ressource.


Côté serveur, il faut :

- compter le nombre total de ressources disponibles ;
- extraire les ressources correspondantes à `size` et `last_id` - ce qui est en général plutôt facile pour les bases de données ;
- gérer les cas d'erreur pour si on fournit :
  - autre chose qu'un entier positif pour paramètre `size` - normalement une `erreur 400`,
  - un identifiant inexistant pour le paramètre `last_id` - peut-être une [erreur 404](https://fr.wikipedia.org/wiki/Erreur_HTTP_404);
- documenter ses paramètres et le choix de la taille par défaut des pages.

:warning: Si le serveur veut permettre d'accéder directement à une page arbitraire, cela nécessite d'ajouter les url pour chaque page.

```json
{
  "meta":{
    "total":65
  },
  "links":[
    "https://myrestapi.org/resources",
    "https://myrestapi.org/resources?last_id=25",
    "https://myrestapi.org/resources?last_id=50"
  ],
  "resources":
  [
    {
      "id":1,
      "name":"premier"
    },
    {
      "id":2,
      "name":"second"
    }
  ]
}
```

C'est jouable pour un petit nombre de page mais pas au delà. On peut aussi envisager de fournir une synthèse pour avoir :

- la première page ;
- la page précédente ;
- la page suivante ;
- la dernière page.

```json
{
  "meta":{
    "total":65
  },
  "links":{
    "first":"https://myrestapi.org/resources",
    "previous":"https://myrestapi.org/resources",
    "next":"https://myrestapi.org/resources?last_id=50",
    "last":"https://myrestapi.org/resources?last_id=50"
  },
  "resources":
  [
    {
      "id":26,
      "name":"vingt-sixième"
    },
    {
      "id":27,
      "name":"vingt-septième"
    }
  ]
}
```

## Conclusion

La pagination est un compromis entre la simplicité et protection du service. Il est possible de la combiner avec les quotas pour maîtriser encore plus la charge sur votre API REST. Même le choix du type de pagination nécessite d'expérimenter avec les utilisateurs de vos API pour être sûrs que cela permet de répondre à leurs besoins.

Et vous avez déjà implémenter un système de pagination pour une API REST ? Comment aviez-vous procéder ? Si vous avez des expériences ou des questions sur les API REST, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
