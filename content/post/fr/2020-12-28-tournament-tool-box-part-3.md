---
title: "Boîte à outils pour tournoi - partie 3"
date: "2020-12-28"
draft: false
description: "Modification de la fonctionnalité classement et refactoring"
tags: ["projet perso","Go","jeu","classement","refactoring","architecture"]
author: "Bertrand Madet"
---

Boite à outils pour tournoi :
[partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}),
[partie 2]({{< ref "/post/fr/2020-12-09-tournament-tool-box-part-2.md" >}}),
[partie 4]({{< ref "/post/fr/2021-04-26-tournament-tool-box-part-4.md" >}}),
[partie 5]({{< ref "/post/fr/2021-06-01-tournament-tool-box-part-5.md" >}}),
[partie 6]({{< ref "/post/fr/2023-03-08-tournament-tool-box-part-6.md" >}}),
[partie 7]({{< ref "/post/fr/2023-03-20-tournament-tool-box-part-7.md" >}})

-----
*Modifié le 10 mai 2021, pour améliorer la mise en forme.*

Dans ce billet, je vous propose d'analyser un refactoring du code de mon projet perso lors de l'ajout d'une fonctionnalité dans le classement.

## Le but

Le but était d'ajouter la possibilité d'avoir un critère du classement d'un concurrent selon les éléments de classement de ses adversaires.

Par exemple, l'appariement en ronde suisse utilise souvent les points adverses comme critère de classement. 
Dans cette configuration au bout de trois matchs, si vous et moi avons 7 points. Si mes adversaires ont 4, 2 et 6 points et que vos adversaires ont 1, 6 et 6 points. Vous serez devant moi au classement.

J'ai récemment lu le livre Clean Architecture - [fiche de lecture]({{< ref "/post/fr/2020-12-23-clean-architecture-lecture.md" >}}) - et j'ai perçu un problème dans le graphe de dépendances de mon projet. 

## Présentation des problèmes

Il y a deux problèmes :

- l'ajout de ce critère n'est pas compliqué mais elle complexifie la configuration. Il faut trouver un mécanisme pour garder la configuration des classements aussi simple que possible.
- le paquet `pkg/rank` dépend du paquet `pkg/rank/settings` alors que le paquet `pkg/rank` contient la logique métier. Ce n'est pas une bonne chose car le paquet de configuration des classements contient des détails d'implémentation spécifiques au chargement de la configuration à partir de fichier JSON.

## Développement

### Le plan

Le plan était plutôt simple : passer de ce diagramme de dépendance

```mermaid
graph TD;
    cmd[cmd/main]
    compute[pkg/compute]
    compute_settings[pkg/compute/settings]
    evaluator[pkg/evaluator]
    rank[pkg/rank]
    rank_settings[pkg/rank/settings]
    settings[pkg/settings]
    cmd-->compute-->evaluator
    cmd-->settings
    settings-->compute_settings
    settings-->rank_settings
    cmd-->rank
    rank-->rank_settings
    compute-->compute_settings
```

A celui-là

```mermaid
graph TD;
    cmd[cmd/main]
    compute[pkg/compute]
    compute_settings[pkg/compute/settings]
    evaluator[pkg/evaluator]
    rank[pkg/rank]
    rankcsv[pkg/rank/csv]
    settings[pkg/settings]
    settingsjson[pkg/settings/json]
    cmd-->compute-->evaluator
    cmd-->settings
    settings-->compute_settings
    settings-->rank
    cmd-->rank
    cmd-->rankcsv
    cmd-->settingsjson
    compute-->compute_settings
```

### La réalisation

#### Paquet rank

J'ai commencé par modifier les structures du paquet `pkg/rank` pour dissocier la logique métier "classement" des détails d'implémentation comme le chargement de la configuration du classement à partir de JSON ou l'export du classement en CSV.

```mermaid
classDiagram
Ranking "1" *-- "1" internalSettings
Ranking "1" *-- "*" RankingRow
internalSettings "1" *-- "*" columnSetting
columnSetting "1" *-- "1" Order

RankingRow: Name string
RankingRow:	ID string
RankingRow: Values []string
RankingRow: toStringArray() []string
Ranking: Len() int
Ranking: Swap(i,j int)
Ranking: Less(i, j int) bool
Ranking: ToDoubleArrayOfString() [][]string
Ranking: ToCsvFile(csvPath string)
internalSettings: configureID(settings settings.Settings, headers []string)
internalSettings: configureName(settings settings.Settings, headers []string)
internalSettings: configure(settings settings.Settings, headers []string)
columnSetting: Name string
columnSetting: Descending bool
columnSetting: Indexes [2]int
Order: ConvertTo string
Order: Descending bool

```

La fonction d'export CSV a fini dans un paquet `pkg/rank/csv`. J'ai redéfini les structures pour les classements sans aucune considération pour la manière dont j'allais importer la configuration. J'ai utilisé deux membres fonctions dans la structure `Criteria` pour permettre d'adapter facilement le comportement de comparaison et d'accumulation d'un critère de classement.

```mermaid
classDiagram
Ranking "1" *-- "1" Settings
Ranking "1" *-- "*" RankingRow
Settings "1" *-- "1" indexes
Settings "1" *-- "n" Criteria

RankingRow: Name string
RankingRow:	ID string
RankingRow: Values []string
Ranking: Len() int
Ranking: Swap(i,j int)
Ranking: Less(i, j int) bool
Settings: initID(headers []string)
Settings: initName(headers []string)
Settings: initGameScopedCriterias(headers []string)
Settings: initOpponentScopedCriterias(headers []string)
Settings: Init(headers []string)
Criteria: Name string
Criteria: FieldPrefix string
Criteria: GameScoped bool
Criteria: OpponentScoped bool
Criteria: Accumulator  
Criteria: Less
Criteria: Equal(right Criteria) bool
Settings: IDColumnName string
Settings: IDColumnPrefix string
Settings: NameColumnName string
Settings: NameColumnPrefix string
indexes: ID [2]int
indexes: Name [2]int
indexes: Criterias [][]int

```

L'ajout de la fonctionnalité a consisté en l'enregistrement des adversaires de chaque concurrent au fur et à mesure du parcours des matchs puis en une boucle sur les critères de classement orientés adversaires.

#### Paquet settings

J'ai déplacé la fonction `FromJSONFile` du paquet `pkg/settings` vers `pkg/settings/json`. Le contenu du paquet `pkg/rank/settings` a été copié vers `pkg/settings/json` puis modifié.

J'avais commencé à développer des méthodes `UnmarshalJSON` (qui permettent de charger les structures à partir de JSON). Et je me suis dit que c'était dommage de ne pas profiter de la capacité du Golang à importer facilement le JSON à partir d'annotation.

```golang
type CriteriaAdapter struct {
	Name         string `json:"name"`
	FieldPrefix  string `json:"field_prefix"`
	CriteriaType string `json:"type"`
	Order        string `json:"order"`
}
```

Les annotations sur la structure ci-dessus permettent d'importer et exporter en JSON ci-dessous.

```json
{
    "name":"un nom",
    "field_prefix": "prefix_",
    "type":"sum_per_opponent",
    "order":"ascending"
}
```

J'ai donc créé des structures faciles à importer et à écrire en JSON que j'ai appelé des adaptateurs pour les structures de configuration de classement. Il restait à coder des fonctions de création de structures de classement à partir de ces adaptateurs.

### En résumé

Je retiens de cette séquence :

- L'application des principes exposées dans Clean Architecture n'est pas difficile mais nécessite de se poser (souvent) la question de la raison d'être de votre logiciel ;
- Il faut utiliser les facilités du Golang (annotations pour le JSON, traitement des fonctions).
- Les annotations JSON pour un membre ne fonctionnent que si le membre est exporté (débute par une majuscule):warning: , oubliez cela peut vous faire perdre rapidement une demi-heure.

## La suite

Pour la suite, j'aimerai :

1. ajouter la fonctionnalité d'appariement ;
2. ré-architecturer suivant la même méthode le code de la configuration des calculs ;
3. améliorer l'écriture des conditions.

Voilà, j'espère que cela vous a plu. Si vous souhaitez partager des projets, contactez [moi sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/). J'accueille les merge requests sur [TournamentToolBox (https://www.gitlab.com/trambi/tournamenttoolbox)](https://www.gitlab.com/trambi/tournamenttoolbox)avec enthousiasme.

-----

Boite à outils pour tournoi :
[partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}),
[partie 2]({{< ref "/post/fr/2020-12-09-tournament-tool-box-part-2.md" >}}),
[partie 4]({{< ref "/post/fr/2021-04-26-tournament-tool-box-part-4.md" >}}),
[partie 5]({{< ref "/post/fr/2021-06-01-tournament-tool-box-part-5.md" >}}),
[partie 6]({{< ref "/post/fr/2023-03-08-tournament-tool-box-part-6.md" >}}),
[partie 7]({{< ref "/post/fr/2023-03-20-tournament-tool-box-part-7.md" >}})
