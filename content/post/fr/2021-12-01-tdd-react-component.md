---
title: "TDD appliqué à un composant React"
date: "2021-12-01"
draft: false
description: "une illustration concrète de la méthode"
tags: ["tdd","front-end","refactoring","methodologie","Software Craftmanship","test","reactjs"]
author: "Bertrand Madet"
---

J'ai déjà écrit sur le développement piloté par les tests (ou :gb: **t**est **d**riven **d**evelopment) :

- [TDD en 3 phrases]({{< ref "/post/fr/2021-06-15-tdd-en-3-phrases.md" >}}) ;
- [TDD, refactoring et Git]({{< ref "/post/fr/2021-07-13-tdd-refactoring-git.md" >}}).

Les billets étaient plus orientés processus. On m'a déjà demandé comment utiliser sur le TDD pour un composant front-end. Je pense que ce cas peut illustrer l'approche. Je vous propose donc de suivre l'utilisation du TDD sur un formulaire de connexion en React.

## Test du framework de test

On commence par tester la configuration du framework de test ([jest (https://jestjs.io)](https://jestjs.io)), en écrivant qui doit retourner une erreur.

```typescript
// src/components/login-form/login-form.test.tsx
describe('login-form',()=> {
  it('should call onSubmit on valid path',()=>{
    expect(false).toEqual(true);
  });
});

```

On lance les tests et on vérifie que le framework de test fonctionne et renvoie une erreur pour la ligne `expect(false).toEqual(true);`.

On continue à tester la configuration du framework de test.

```typescript
// login-form.test.tsx
describe('login-form',()=> {
  it('should call onSubmit on valid path',()=>{
    expect(true).toEqual(true);
  });
});

```

On lance les test et on vérifie que le framework de test fonctionne et ne renvoie pas d'erreur.

On a donc vérifié que notre configuration de test permet d'afficher les échecs et les réussites.

## Première itération

On va pouvoir passer à la première version du test du composant en suivant le chemin nominal. On voudrait, dans un premier temps, s'assurer que l'appuie sur le bouton `Submit` permet l'appel à la fonction passée au composant (appelée *fonction de soumission* par la suite).

```typescript
// src/components/login-form/login-form.test.tsx

import react from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'

import { LoginForm } from '.';

describe('login-form',()=> {
  it('should call onSubmit on happy path',()=>{
    const submitHandle = jest.fn();
    render(
      <LoginForm buttonText={'Submit'}
      onSubmit={submitHandle}/>
      );
      userEvent.click(screen.getByText('Submit'));
    expect(submitHandle).toHaveBeenCalled();
  });
});
```

On lance les tests : le test échoue. On écrit le minimum de code pour que le test fonctionne.

```typescript
// src/components/login-form/index.tsx
import react from 'react';

export interface LoginFormProps{
  buttonText: string;
  onSubmit: (login: string, password:string)=>void;
}

export function LoginForm(props: LoginFormProps):JSX.Element {
  return (<>
    <button onClick={props.onSubmit}>{props.buttonText}</button>
  </>);
}
```

On lance les tests : le test fonctionne. On peut commiter tranquillement ou faire des refactorings ciblés.

## Deuxième itération

On va ajouter un test sur le comportement du champ d'identifiant. En s'assurant que l'identifiant de connexion est transmis lors l'appel à la fonction de soumission.

```typescript
// src/components/login-form/login-form.test.tsx

import react from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'

import { LoginForm } from '.';

describe('login-form',()=> {
  it('should call onSubmit on valid path',()=>{
    const submitHandle = jest.fn();
    render(
      <LoginForm buttonText="Submit"
      loginLabel="login"
      onSubmit={submitHandle}/>);
    userEvent.type(screen.getByLabelText('login'),'mylogin');
    userEvent.click(screen.getByText('Submit'));
    expect(submitHandle).toHaveBeenCalled();
    expect(submitHandle.mock.calls[0][0]).toEqual('mylogin');
  });
});
```

On lance les tests, ils échouent sur la sélection du champ `login`. On code le minimum pour que le test échoue sur la dernière vérification.

```typescript
// src/components/login-form/index.tsx
import { useState }, react from 'react';

export interface LoginFormProps{
  buttonText: string;
  loginLabel: string;
  onSubmit: (login: string, password:string)=>void;
}

export function LoginForm(props: LoginFormProps):JSX.Element {
  return (<>
    <label htmlFor="login-form-login">{props.loginLabel}</label>
    <input name="login-form-login" />
    <button onClick={props.onSubmit}>{props.buttonText}</button>
  </>);
}
```

Cette fois les tests échouent bien sur la dernière vérification. On code donc le minimum pour le test réussisse.

```typescript
// src/components/login-form/index.tsx
import react, { useState } from 'react';

export interface LoginFormProps{
  buttonText: string;
  loginLabel: string;
  onSubmit: (login: string, password:string)=>void;
}

export function LoginForm(props: LoginFormProps):JSX.Element {
  const [login, setLogin] = useState('');

  const handleLoginChange = (event) =>{
    setLogin(event.target.value);
  };
  const handleButtonClick = (event) => {
    props.onSubmit(login,'');
  };

  return (<>
    <label htmlFor="login-form-login">{props.loginLabel}</label>
    <input id="login-form-login" value={login} onChange={handleLoginChange} />
    <button onClick={handleButtonClick}>{props.buttonText}</button>
  </>);
}
```

Les tests passent avec succès. La deuxième itération a permis de tester et d'implémenter le comportement du composant vis-à-vis du champ `login`. On peut donc commiter.

## Troisième itération

On va ajouter un test sur le mot de passe. En s'assurant que le mot de passe saisi est bien transmis à la fonction de soumission.

```typescript
// src/components/login-form/login-form.test.tsx

import react from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'

import { LoginForm } from '.';

describe('login-form',()=> {
  it('should call onSubmit on valid path',()=>{
    const submitHandle = jest.fn();
    render(
      <LoginForm buttonText="Submit"
      loginLabel="login"
      passwordLabel="password"
      onSubmit={submitHandle}/>);
    userEvent.type(screen.getByLabelText('login'),'mylogin');
    userEvent.type(screen.getByLabelText('password'),'mypassword');
    userEvent.click(screen.getByText('Submit'));
    expect(submitHandle).toHaveBeenCalled();
    expect(submitHandle.mock.calls[0][0]).toEqual('mylogin');
    expect(submitHandle.mock.calls[0][1]).toEqual('mypassword');
  });
});
```

On lance les tests, ils échouent sur la sélection du champ `password`. On code le minimum pour que le test échoue sur la dernière vérification.

```typescript
// src/components/login-form/index.tsx
import react, { useState } from 'react';

export interface LoginFormProps{
  buttonText: string;
  loginLabel: string;
  onSubmit: (login: string, password:string)=>void;
}

export function LoginForm(props: LoginFormProps):JSX.Element {
  const [login, setLogin] = useState('');

  const handleLoginChange = (event) =>{
    setLogin(event.target.value);
  };
  const handleButtonClick = (event) => {
    props.onSubmit(login,'');
  };

  return (<>
    <label htmlFor="login-form-login">
      {props.loginLabel}
    </label>
    <input id="login-form-login" 
           value={login}
           onChange={handleLoginChange} />
    <label htmlFor="login-form-password">
      {props.passwordLabel}
    </label>
    <input type="password" 
           id="login-form-password" />
    <button onClick={handleButtonClick}>{props.buttonText}</button>
  </>);
}
```

Cette fois les tests échouent bien sur la dernière vérification. On code donc le composant pour qu'il passe le test.

```typescript
// src/components/login-form/index.tsx
import react, { useState } from 'react';

export interface LoginFormProps{
  buttonText: string;
  loginLabel: string;
  passwordLabel: string;
  onSubmit: (login: string, password:string)=>void;
}

export function LoginForm(props: LoginFormProps):JSX.Element {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const handleLoginChange = (event) => {
    setLogin(event.target.value);
  };
  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };
  const handleButtonClick = (event) => {
    props.onSubmit(login,password);
  };

  return (<>
    <label htmlFor="login-form-login">
      {props.loginLabel}
    </label>
    <input id="login-form-login" 
           value={login}
           onChange={handleLoginChange} />
    <label htmlFor="login-form-password">
      {props.passwordLabel}
    </label>
    <input type="password"
           value={password} 
           id="login-form-password"
           onChange={handlePasswordChange} />
    <button onClick={handleButtonClick}>{props.buttonText}</button>
  </>);
}
```

Les tests passent avec succès, on peut donc commiter sereinement.

### La suite

Une fois ce début effectué, on peut implémenter la gestion des erreurs en suivant la même méthodologie ou faire des refactorings comme gérer les états avec `useReducer` (ou l'utilisation de `useCallback`). Les régressions sur les refactorings seront sécurisées par les tests. L'idée est de continuer à se concentrer sur le comportement du composant.

## Conclusion

Voilà ce billet plutôt technique se termine. L'idée était de montrer la mécanique de manière concrète. J'espère que cela vous permettra de pouvoir la transposer dans les domaines de votre choix. Cela m'aura aussi permis de montrer des petites astuces sur le test de composants React. Je pense que j'y reviendrai.

Si vous avez des questions ou des retours sur la méthode TDD et les tests unitaires, n'hésitez pas à les partager sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
