---
title: "Tests d'interaction avec une base de données - partie 1"
date: "2023-05-18"
draft: false
description: "illustré avec du NodeJs et PostgreSQL"
tags: ["nodejs","test"]
author: "Bertrand Madet"
---

Cette semaine, je vous propose différentes méthodes pour tester des composants logiciels d'interaction avec une base de données avec serveur comme MySQL :dolphin: , MariaDB :seal:, PostgreSQL :elephant: ou même MongoDB. Je précise base de données avec serveur, car il existe des bases de données embarqués comme [sqlite](https://www.sqlite.org/index.html), [duckDB](https://duckdb.org/) ou [Berkeley database](https://www.oracle.com/database/technologies/related/berkeleydb.html).

Tester une partie qui interagit avec une base de données avec serveur implique qu'au moment du test la base de données en question est fonctionnelle et joignable depuis le code à tester. Naturellement on s'orienterait plus vers des tests d'intégration pour couvrir cette partie de code. Mais les tests unitaires (ou mono processus ou sans IO) permettent de gérer très finement les cas d'erreurs rencontrées et surtout de localiser rapidement les éventuels problèmes.

Je vais considérer que la base de données ciblée sera PostgreSQL et les exemples utiliseront NodeJs donc seront en Javascript ou en TypeScript. Je vais aussi partir du principe de la partie à tester est isolé du reste du logiciel : une classe ou une fonction dédiée à l'interaction avec la base de données.

## Injecter la dépendance

Mon premier conseil est d'injecter une instance de votre bibliothèque d'interaction avec la base de données dans votre partie à tester. Concrètement, si c'est :

- une fonction vous pouvez ajouter un paramètre ou faire une fonction de fonction ;
- une classe, vous pouvez ajouter un paramètre à votre constructeur de classe.

Au lieu de faire ça dans le code à tester :

```typescript

import {Client} from 'pg';

function getStuff(){
  const client = new Client();
  client.connect()
  const result = client.query('SELECT DISTINCT stuff FROM myStuff ORDER BY stuff;');
  // Do something with result
}

class StuffRepository{
  private client: Client;
  constructor(){
    this.client = new Client();
    this.client.connect()
  }
  getById(){
    const result = this.client.query('SELECT DISTINCT stuff FROM myStuff ORDER BY stuff;');
  // Do something with result
  }
}
```

Je propose que l'on fasse cela :

```typescript
import {QueryConfig, QueryResult} from 'pg';
// ou définisser les types QueryConfig et QueryResult

type interface Client {
  query: (query: string | QueryConfig, values: any[] | undefined) => Promise<QueryResult>;
  connect: () => Promise<void>;
  end: () => Promise<void>
}

function getStuff(client: Client){
  const result = client.query('SELECT DISTINCT stuff FROM myStuff ORDER BY stuff;');
  // Do something with result
}
class StuffRepository{
  private client;
  constructor(client: Client){
    this.client = client;
    this.client.connect()
  }
  get(){
    const result = this.client.query('SELECT DISTINCT stuff FROM myStuff ORDER BY stuff;');
  // Do something with result
  }
}
```

Cela va permettre d'interagir plus simplement avec le code à tester et éviter de devoir `mocker` :clown: pendant les tests. On va utiliser une instance de la classe Client du paquet `pg` ou quelque chose ressemblant pendant les tests.

## Enregistrer les requêtes

L'idée est d'enregistrer les requêtes émises vers le serveur et de les comparer. Ainsi si on veut tester une fonction qui crée une table et insert un enregistrement, on pourrait faire comme cela.

```javascript
it('should create a table and insert one row',()=>{
  const spy = [];
  const expectedQueries = [
    'CREATE TABLE mytable (atext text,anumber int);',
    'INSERT INTO mytable VALUES ('atextA',1);',
  ]
  const client = {
    query: (params) => {
      if(typeof params === 'string'){
        spy.push(params);
      }else if (typeof params === 'object'){
        spy.push(JSON.stringify(params));
      }
    }
  };
  createTable(client);
  expect(spy).toEqual(expectedQueries);
});
```

De mon point de vue, on teste que la fonction appelle bien les bonnes requêtes dans le bon ordre et donc partiellement des détails d'implémentation. Cela peut-être très pertinent pour une création ou un chargement de base de données.

- :+1: Cela fonctionne facilement ;
- :-1: Cela fonctionne uniquement pour les requêtes sans valeur de retour ;
- :-1: Cela suppose de connaître précisément les requêtes utilisées.

## Imiter de pg manuellement

Là le principe est de se comporter comme la bibliothèque `pg`. Si on veut tester une fonction qui récupère les champs distincts d'une table.

```javascript
it('should call select distinct to get distinct stuff',()=>{
  const expected = ['stuff A','stuff B', 'stuff C'];
  const client = {
    query: (params) => {
      if(params === 'SELECT DISTINCT stuff FROM myStuff ORDER BY stuff;'){
        return {
          rowCount: 3,
          rows: [{stuff: 'stuff A'},{stuff: 'stuff B'},{stuff: 'stuff C'}]
        };
      }
      throw new Error('Unknown query :' + params);
    }
  };
  const result = getStuff(client);
  expect(result).toEqual(expected);
});
```

On va implémenter la fonction `query` pour qu'elle se comporte comme le ferait la fonction de la bibliothèque.

- :-1: C'est fastidieux s'il y a beaucoup de requêtes.
- :+1: Cela fonctionne avec les requêtes avec valeur de retour ;
- :-1: Cela suppose de connaître précisément les requêtes utilisées.

## Imiter de pg avec pgmock2

Il existe une bibliothèque [pgmock2](https://www.npmjs.com/package/pgmock2), qui permet de faire cela plus facilement en listant les requêtes et les valeurs de retour.

```javascript
const PgMock2 = require('./pgmock2').default;

it('should call select distinct to get distinct stuff',()=>{
  const expected = ['stuff A','stuff B', 'stuff C'];
  const PgMock2 = require('./pgmock2').default;
  const client = new PgMock2();
  client.add('SELECT stuff FROM myStuff ORDER BY stuff;',[], 
    {
      rowCount: 4,
      rows: [{stuff: 'stuff A'},{stuff: 'stuff B'},{stuff: 'stuff C'}, {stuff: 'stuff C'}]
  });
  client.add('SELECT DISTINCT stuff FROM myStuff ORDER BY stuff;',[], 
    {
      rowCount: 3,
      rows: [{stuff: 'stuff B'},{stuff: 'stuff A'},{stuff: 'stuff C'}]
  });
  const result = getStuff(client);
  expect(result).toEqual(expected);
});
```

On voit pgMock2 permet d'ajouter une liste arbitraire de couple requête-réponse.

- :+1: Cela fonctionne avec les requêtes avec valeur de retour ;
- :-1: Cela suppose de connaître précisément les requêtes utilisées.

## Imiter de pg avec une base de données en mémoire

On peut aussi utiliser une base de données en mémoire et créer un adaptateur à l'interface de pg. Par exemple on crée un adaptateur à [alasql (alasql.org)](http://alasql.org/) qui permet de gérer une base de données SQL en mémoire :

```typescript
import { QueryConfig, QueryResult, QueryResultRow } from 'pg';
const alasql = require('alasql');
export class AlasqlAdapter {
  private db;
  constructor() {
    this.db = new alasql.Database();
  }

  async connect() {

  }

  async query(query: string | QueryConfig, _values: any[] | undefined = undefined): Promise<QueryResult> {
    const command = (typeof query === 'string' ? query : query.text).trim().split(' ')[0];
    const result = this.db.exec(command);
    if (typeof result === 'number'){
      const rows: QueryResultRow[] = [];
      return { rows, oid: 0, command, rowCount: 0, fields: [] };
    }else{
      const rows: QueryResultRow[] = result;
      return { rows, oid: 0, command, rowCount: result.length, fields: [] };
    }
  }
  async end() {

  }
}
it('should return distinct stuff ordered by stuff',()=>{
  const expected = ['stuff A','stuff B', 'stuff C'];
  const client = AlasqlAdapter();
  client.connect();
  client.query('CREATE TABLE mystuff (stuff text);');
  client.query("INSERT INTO mystuff VALUES ('stuff B'),('stuff A'),('stuffC'),('stuffA');");
  const result = getStuff(client);
  expect(result).toEqual(expected);
});
```

Personnellement, je trouve cela impressionnant car cela permet de se détacher de l'implémentation. Si pour une raison quelconque, on décide de faire le tri au niveau de la fonction plutôt que de la requête SQL, le test continuera à fonctionner. :warning: Dans mon implémentation de l'adaptateur, l'utilisation de requête paramétrée ou de fonctions spécifiques à Postgres comme `string_to_array` n'est pas prise en compte.

- :+1: On teste le comportement et pas l'implémentation ;
- :-1: Cela suppose d'avoir un comportement quasi identique à la base de données que l'on cible.

## Conclusion

Voici mon catalogue pour tester en isolation, l'interaction avec la base de données. La dernière approche peut aussi être utilisée si on utilise une bibliothèque qui abstrait les bases de données comme un ORM (**O**bject-**r**elational **m**apping :gb:) en utilisant sqlite. Il faut toutefois se souvenir qu'il peut y avoir des divergences de comportements entre le moteur de base de données de production et le moteur de base de données de tester

Comme les différentes techniques sont complémentaires entre elles et avec des tests plus classiques d'intégration. On peut les combiner pour couvrir ce que l'on souhaite tester.

J'espère que ce partage d'expériences vous appréhender plus sereinement les tests d'interface avec les bases de données Si vous avez des retours ou d'autres conseils, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
