---
title: "Réécriture de code"
date: "2024-07-06"
draft: false
description: "et conséquences"
tags: ["Software Craftmanship"]
author: "Bertrand Madet"
---

La réécriture consiste à modifier entièrement le code d'une application. Sur l'échelle de l'ampleur des travaux, nous sommes bien évidemment au-delà des refactorings (modifications atomiques du code pour améliorer le code) et de la restructuration (changement dans la structure). Pour être parfaitement clair : 

- un renommage de variable ou une extraction de fonction sont des refactorings ;
- l'utilisation d'un ORM à la place d'une bibliothèque dédiée à une base de données spécifique est une restructuration ;
- le passage d'une API en Symfony (PHP) vers Django (Python) est une réécriture.

Il y a évidemment des cas où la frontière est plus diffuse comme le passage d'une **S**ingle **P**age **A**pplication d'Angular à React.

## Causes

Malgré l'ampleur de ces opérations, il est fréquent d'évoquer l'hypothèse d'une réécriture. La réécriture peut être intéressante pour :

- Gérer une dette technique trop importante ;
- Adapter le logiciel aux capacités de développement ou d'exploitation ;
- Apporter des capacités non prévues au logiciel : performance, facilité d'installation, sûreté, passage à l'échelle  ...

Cela peut aussi être pour une combinaison des raisons ci-dessus.

Par exemple pour la [réécriture de mon projet perso]({{< ref "/post/fr/2023-11-21-rewrite-side-project-api-in-go-part0" >}}), le logiciel actuel utilise une version obsolete du framework Symfony (3 alors que la version actuelle est 7), l'installation est complexe (il faut installer un serveur web, PHP, une base de données MySQL). Faire une nouvelle version en Go et applications web permet de sortir du framework Symfony 3 et surtout d'adresser le point de la facilité d'installation.

## Conséquences possibles

Il est tentant de faire table rase du passé mais le fait de recoder a des conséquences importantes. Il est important de les lister avant de se lancer dans ce genre d'aventure. 🚨 Même en faisant la liste on oubliera des choses.

Il faudra continuer à maintenir la solution existante pendant le temps de la réécriture. Cela peut se faire avec la même équipe en ralentissant la dite équipe ou avec une deuxième équipe. Si vous utilisez une deuxième équipe, il faut veiller à ce que les fonctionnalités ne divergent pas. 

Il faut garder une trace des fonctionnalités de la solution, pour : 

- ne pas oublier de fonctionnalités ;
- être sûr de ne réécrire que ce qui est utile.

Cela peut être l'occasion d'ajouter des fonctionnalités qui pouvaient être coûteuses dans l'ancien système. Personnellement, je n'ai jamais pratiqué de réécriture sans modification des fonctionnalités ou d'ajout de fonctionnalités. 

En plus des fonctionnalités, il y a les comportements de votre logiciel auxquels les utilisateurs se sont habitués. Par exemple, le temps de réponse des certains appels, mais aussi les bugs 🐛 ! On peut se conférer à XKCD : [Workflow](https://xkcd.com/1172/) ou à Google avec la [loi d'Hyrum](https://www.hyrumslaw.com/). 

> **Loi d'Hyrum** : Avec un nombre suffisant d'utilisateurs d'une API, peu importe ce que vous promettez dans le contrat : tous les comportements observables de votre système seront utilisés par quelqu'un.

Ce que Google dit d'une API, on peut l'étendre à un exécutable. On peut aussi appeler cela des fonctionnalités induites.

Chaque fonctionnalité désirée va devoir être vérifiée par un test automatique, un test manuel ou des analyses de comportement des utilisateurs. Afin d'avoir une charge de travail raisonnable, il faut garder un nombre de fonctionnalités raisonnables. Il faut supprimer des fonctionnalités. La suppression de fonctionnalité est un exercice assez complexe puisqu'il faut optimiser le ratio entre la valeur ajoutée de la fonctionnalité d'une part et la charge d'écriture et de vérification de l'autre. 

## Réécriture partielle

Nous avons implicitement évoqué les réécritures complètes, c'est à dire les réécritures qui impliquent la totalité du code. Il existe des réécritures partielles - qui se restreignent à une partie du logiciel. On voit des bibliothèques Python dont certaines parties nécessitent de la rapidité ou de la sûreté. Seules les parties critiques sont codées en C ou en Rust 🦀 et intégrées à la bibliothèque Python grâce aux API Python.

Je préfère les réécritures partielles, c'est à dire qui se restreignent à une partie du logiciel. L'étendue des changements reste plus limitée et l'analyse des fonctionnalités est plus restreinte, donc moins risquée. 

## Conclusion

Une réécriture est une occasion de remettre à plat le code mais également les fonctionnalités d'un logiciel. La gestion des fonctionnalités et en particulier la sélection de fonctionnalité est une tâche qui s'éloigne de l'écriture du code mais qui reste dans le domaine de l’ingénierie logicielle. De mon point de vue, ce que l'on attend d'un ingénieur logiciel expérimenté est aussi la capacité à aider la sélection des fonctionner à "coder". Après tout, il est toujours plus efficace de ne pas coder une fonctionnalité inutile.

Si vous avez des conseils, des expériences sur la réécriture de logiciel partagez les avec moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

