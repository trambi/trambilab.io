---
title: "Les générateurs de site statique"
date: "2022-05-28"
draft: false
description: "Faire un site web facilement"
tags: ["outils","blog"]
author: "Bertrand Madet"
---

Dans ce billet, nous allons parler de générateur de site statique. Pourquoi ? Parce que j'aime ces outils.

## Générateur de site statique

Un générateur de site statique est un type d'outil qui à partir de contenu de nature diverses, permet de créer un site web.

```mermaid
graph LR
  subgraph "Site statique"
    html
    css
    js
    img
  end
  data1[Contenu 1]-->generator[Générateur de site statique]
  data2[Contenu 2]-->generator
  datan[Contenu N]-->generator
  generator-->html[Fichiers html]
  generator-->css[Fichiers de style CSS]
  generator-->js[Fichiers JavaScript]
  generator-->img[Images et fichiers média]
```

Le contenu peut être :

- des fichiers markdown ;
- des images ;
- interrogable par API REST ;
- ou même inclus dans une base de données.

La sortie d'un générateur de site statique est un site statique c'est-à-dire un ensemble de fichiers qui peut être servi par un serveur web - Apache http server ou Nginx  - et visité avec un navigateur web comme Firefox, Safari ou Chrome. En général les fichiers de sortie sont des fichiers `html`, images et média pour le contenu, `css` pour le style, `js` pour l'utilisation du JavaScript et donc une personnalisation du comportement de la page.

## Avantages

### Variétés d'outils faciles à prendre en main

Il existe des logiciels libres faciles à prendre en main comme :

- [Jekyll (https://jekyllrb.com/)](https://jekyllrb.com/) en Ruby sous licence [MIT](https://choosealicense.com/licenses/mit/) la référence ;
- [Hugo (https://gohugo.io/)](https://gohugo.io/) en go sous licence [Apache v2](https://choosealicense.com/licenses/apache-2.0/) qui a l'avantage d'être rapide et de générer ce blog :D ;
- [Pelican (https://blog.getpelican.com/)](https://blog.getpelican.com/) en python sous licence [Affero GPL v3](https://choosealicense.com/licenses/agpl-3.0/) ;
- [Gatsby (http://gatsbyjs.org)]( http://gatsbyjs.org) en node.js sous licence [MIT](https://choosealicense.com/licenses/mit/) plus orienté sur la génération de site utilisant React.js.

Tous ces outils permettent de prendre en charge des types de fichiers plus faciles à écrire que du html par exemple le [markdown (https://www.markdownguide.org/getting-started/)](https://www.markdownguide.org/getting-started/) et d'appliquer des gabarits à certaines pages. Ainsi l'ajout d'un lien ne vous oblige pas à réécrire tous vos contenus juste à modifier le gabarit et lancer à nouveau votre générateur de site statique.

### Facile à héberger

Comme les sites résultants sont statiques et nécessitent "juste" un serveur HTTP, il est très facile de les héberger chez soi, chez un hébergeur.

C'est aussi possible avec [GitLab pages (https://docs.gitlab.com/ee/user/project/pages/)](https://docs.gitlab.com/ee/user/project/pages/) ou [Github pages (https://pages.github.com/)](https://pages.github.com/). Dans ces cas, vous pouvez gérer votre contenu en gestion de version dans un dépôt Git et mettre à jour à chaque commit (c'est exactement comme ça que ce blog fonctionne au moment de la rédaction de cet article).

Il est même possible de les [héberger sur un compartiment S3 avec AWS (https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/HostingWebsiteOnS3Setup.html)](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/HostingWebsiteOnS3Setup.html).

### Moindre surface d'attaque

Contrairement à un `CMS` (**C**ontent **M**anagement **S**ystem ou :fr: [système de gestion de contenu - https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu)), il n'y a pas de serveurs applicatifs en `PHP`, `Python`, `Node.js` ou `Ruby` ni de bases de données. La surface d'attaque est donc réduite des composants applicatifs (avec leurs modules et leurs frameworks associés) et des serveurs de bases de données.

## Inconvénients

### Les sites sont statiques

Certaines applications ne sont pas possibles en utilisant uniquement un site statique. Par exemple, un site de commerce en ligne nécessite des interactions spécifiques avec l'utilisateur comme la gestion du panier et la commande de produits.

La gestion des commentaires doit passer par un service tier par exemple Facebook, Twitter ou Disqus.

### Nécéssite l'intégration dans le flux de publication

Le mécanisme de publication doit intégrer l'exécution du générateur de site statique. Dans mon précédent projet, nous utilisons Gatsby. L'utilisation d'un générateur de site statique puisant son contenu dans une API Rest a grandement complexifié l'intégration continue. En effet, il fallait surveiller le contenu servi par l'API REST et déclencher l'intégration continue quand il y avait des changements sur les contenus.

## Conclusion

Les générateurs de site statiques ont l'immense avantage de transformer des informations hétéroclites en documents lisibles par un logiciel extrêmement répandu : un navigateur web.

Je suis content d'utiliser un générateur de site statique comme Hugo pour gérer ce blog (89 billets avec celui-ci). Cela me permet de rédiger hors ligne et de le publier quand je suis prêt.

Si vous avez des préférences en terme de générateurs de site statique ou d'autres outils d'édition, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
