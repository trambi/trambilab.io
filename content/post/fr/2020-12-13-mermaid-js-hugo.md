---
title: "Mermaid.js avec Hugo"
date: "2020-12-13"
draft: false
description: "Utilisez Mermaid.js dans le générateur de site web statique"
tags: ["documentation","mermaid","hugo","blog"]
author: "Bertrand Madet"
---

## Hugo ?

[Hugo](https://gohugo.io/) est un générateur de site statique écrit en Go. Un générateur de site statique va transformer un ensemble de fichiers de formatage comme markdown, html ou autres en un ensemble de fichiers html pour fournir un site web avec des menus, une mise en page cohérente. Par rapport à un blog Wordpress, cela réduit la surface d'attaque vu que le site est statique (pas de base de données entre autre).

```mermaid
graph TB
contenu[Contenu divers]-->hugo[Hugo]
configuration[Configuration]-->hugo[Hugo]
hugo-->site[Ensemble de fichiers d'un site web]
```

Accessoirement, Hugo sert à générer ce blog :wink:.

Initialement Hugo n'intègre pas Mermaid.js. et comme je voulais l'utiliser (on dit qu'une image vaut mille mots), j'ai installé sur Hugo.

## Principe

Il faut ajouter un `shortcode` mermaid et le script JavaScript de mermaid dans les pages. 

Le shortcode permet de faire en sorte que le contenu de la balise `< mermaid >` entre deux accolades `{{` finisse dans une balise `div` avec pour classe "mermaid".

### Shortcode

Il faut ajouter un fichier (en général dans `layouts\shortcodes\mermaid.html` ou le même répertoire pour votre thème).

```
{{ $_hugo_config := `{ "version": 1 }` }}
<div class="mermaid" align="{{ if .Get "align" }}{{ .Get "align" }}{{ else }}center{{ end }}">{{ safeHTML .Inner }}</div>
```

### Script

Il faut ajouter l'inclusion du script et l'exécution du script :

```
{{ if (findRE "mermaid" .Content 1) }}
        <script src="{{ .Site.BaseURL }}js/mermaid.min.js"></script>
        <script>mermaid.initialize({startOnLoad:true, securityLevel: 'loose'});</script>
{{ end }}
```


Pour le thème que j'utilise `ghostwriter`, il s'agit du fichier `themes/ghostwriter/layouts/partials/footer.html`.

### Et c'est tout

Oui c'était rapide, et documenté. Le prochain outil à adapter avec Mermaid.js sera plus compliqué : Mkdocs.
