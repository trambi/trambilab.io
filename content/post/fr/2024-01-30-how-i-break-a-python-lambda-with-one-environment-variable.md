---
title: "Comment j'ai cassé une lambda Python avec une variable d'environnement"
date: "2024-01-30"
draft: false
description: "et sans le détecter avec les tests unitaires"
tags: ["cloud","lambda","python"]
author: "Bertrand Madet"
---

La semaine dernière, affaibli par les virus et maladies qui traînent (d'où l'absence de billet pendant 20 jours), j'ai cassé une lambda Python 🐍. Je ne l'ai évidemment pas fait à dessein. Je corrigeais un problème de vérification d'autorité de certification corporate pour un appel à une API REST corporate.

## Le problème

Le problème que je voulais corriger était l'émission d'un avertissement de non vérification de certificat pour une requête sur une API REST corporate. Par défaut, la bibliothèque Python `requests` comme  accepte les connexions https si les certificats sont valides (bien formés, dans leur période de validité et pas révoqués) et signés par une des autorités de certification "bien connues". La liste des autorités de certification "bien connues" est [fournie par Mozilla (https://wiki.mozilla.org/CA/Included_Certificates)](https://wiki.mozilla.org/CA/Included_Certificates). Le certificat de l'API REST corporate n'est pas signé par une des autorités de certification "bien connues" mais par une autorité de certificat corporate. Il y a des adaptations à faire pour que cela fonctionne.

```python
response = requests.get(urlapirestcorporate, verify=False)
```

L'avertissement est complètement justifié. :rotating_light: C'est très dommage voire contre-productif d'utiliser le protocole HTTPS et ensuite de ne pas vérifier l'identité du serveur. Si vous n'êtes pas convaincu que c'est contre-productif, imaginez les efforts à mettre en place le chiffrement et l'authentification du serveur :

- Il faut créer une clé privée ;
- Il faut créer un certificat associé à cette clé privée ;
- Il faut initier de manière sécurisée chaque connexion.

Le tout pour ne pas vérifier l'identité du serveur et donc risquer d'être remplacé par un autre serveur :exploding_head:.

Je pense que j'avais codé cela comme ça pour tester et que j'avais oublié de mettre la vérification de l'identité du serveur ...

## La première correction

Pour que la vérification se passe, il "suffit" :

- d'enlever le paramètre `verify` à false ;
- d'indiquer la chaîne de certification corporate à requests.

La première partie est simple :

```python
response = requests.get(urlapirestcorporate)
```

La deuxième partie a plusieurs manières de procéder - la documentation sur la [vérifications certificats SSL (https://fr.python-requests.org/en/latest/user/advanced.html#verifications-certificats-ssl)](https://fr.python-requests.org/en/latest/user/advanced.html#verifications-certificats-ssl). J'ai opté pour utiliser la variable d'environnement `REQUESTS_CA_BUNDLE` dans la description de mon conteneur.

```dockerfile
# (...)
COPY corporate-chain.pem ${LAMBDA_TASK_ROOT}
ENV  REQUESTS_CA_BUNDLE ${LAMBDA_TASK_ROOT}/corporate-chain.pem
# (...)
```

Le fichier `corporate-chain.pem` doit être la concaténation des certificats d'autorité de certification au format `pem`. Il faut commencer par l'autorité signature du certificat de serveur puis "remonter" jusqu'à la autorité de certification racine.

## Le problème de la correction

J'ai lancé les tests unitaires et j'ai testé la lambda dans le cas qui m'intéressait. Mais je n'ai pas vu que dans les cas où ma lambda devait interagir avec un bucket S3 (via la bibliothèque `boto`), j'avais l'erreur suivante : `botocore.exceptions.SSLError: SSL validation failed for https://<nom-de-mon-bucket>.s3.eu-west-3.amazonaws.com/ [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: unable to get local issuer certificate (_ssl.c:1007)`.

La variable d'environnement `REQUESTS_CA_BUNDLE` modifiait le comportement de requests mais aussi de `boto` (qui utilise `requests`). Comme la modification a été faite dans le fichier de description du conteneur, les tests unitaires n'ont pas pu lever d'alerte vu qu'ils n'étaient pas exécutés avec cette variable d'environnement.

## Une meilleure correction

J'ai utilisé la possibilité d'indiquer la chaîne de certification à `requests` via le paramètre `verify` suivi d'un chemin de fichier. Je trouve cela plus explicite : 

> Attention cette API est corporate et la vérification de l'identité du serveur nécessite l'usage de la chaîne de certification corporate.

J'ai réutilisé mon fichier de chaîne de certification corporate.

```python
response = requests.get(urlapirestcorporate,verify='./corporate-chain.pem')
```

Et supprimer la variable d'environnement `REQUESTS_CA_BUNDLE` dans la description du conteneur.

```dockerfile
# (...)
COPY corporate-chain.pem ${LAMBDA_TASK_ROOT}
# (...)
```

## Conclusion

Mes tests unitaires n'étaient pas suffisant pour révéler le problème. Il faudrait que j'ajoute un test d'intégration à partir de l'image du conteneur pour me prémunir à l'avenir de telles mésaventures.

En tout cas, il est toujours important de vérifier toujours l'identité du serveur en HTTPS 🔒.

Si vous avez commis des erreurs dans vos implémentations de lambda, vous pouvez les partager avec moi. Ce vous en avait appris m'intéresse aussi. Le tout sur [X](https://x.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
