---
title: "Bilan des deux ans"
date: "2022-11-11"
draft: false
description: "p****n deux ans !"
tags: ["blog"]
author: "Bertrand Madet"
---

J'ai lancé mon blog le 11 novembre 2020. Voici un bilan après ces deux années de publication.

## Données brutes sur les articles

||Première année|Deuxième année|Total|
|---|---|---|---|
|Billets publiés|61|51|112|
|Fiches de lectures :book:|13|7|20|
|Tags plus utilisés|[fiche de lecture (13)]({{< ref "/tags/fiche-de-lecture">}}), [Software Craftmanship (10)]({{< ref "/tags/software-craftmanship">}}) et [base (9)]({{< ref "/tags/base">}})|[Software Craftmanship (14)]({{< ref "/tags/software-craftmanship">}}), [outils (12)]({{< ref "/tags/outils" >}}) et [projet perso (10)]({{< ref "/tags/projet-perso" >}})|[Software Craftmanship (24)]({{< ref "/tags/software-craftmanship">}}), [fiche de lecture (20)]({{< ref "/tags/fiche-de-lecture">}}) et [projet perso (18)]({{< ref "/tags/projet-perso" >}})|

Le premier avantage de ce blog est qu'il m'a permis de lire **et de capitaliser** 20 livres.

On voit mes préférences :smile:. Il n'est pas exclu que les sujets changent vers Terraform et AWS car je m'aperçois que le champ des possibles est très vaste.

## Processus de rédaction

Mon processus de rédaction est assez stable. En général, l'idée de l'article émerge dans la semaine, mature durant le week-end. La rédaction prend entre une heure et deux heures et a lieu en milieu de semaine. La longueur du processus permet de prendre un peu de recul et d'ancrer les connaissances.

## Processus de diffusion

Pour partager mon expérience, j'utilise LinkedIn et Twitter. Mon utilisation de Twitter pour la diffusion a diminué, j'ai l'impression que cela n'a pas d'impact. Je n'ai pas trouvé la forme adéquate pour les tweets.

Côté LinkedIn, j'ai modifié mon approche : j'ai enlevé le lien dans le billet que je publiais pour "signaler" la publication d'un nouveau billet. Aujourd'hui, je prépare un post avec les éléments les plus prégnants et je laisse les extraits de code pour le billet de blog. Le lien vers le billet est dans le premier commentaire.

Un bon ami m'a suggéré cette modification d'approche pour que les posts soient plus mis en valeur par LinkedIn.

|Articles|Nombre d'impression|Nombre de réaction|Nombre de commentaire|
|--------|-------------------|------------------|---------------------|
|Fiche de lecture : Software Craft|157|0|1|
|Journalisation API Gateway|321|1|1|
|Livrables DDD|617|5|5|
|Infrastructures|356|4|1|
|Doc API|489|6|2|
|Formation Developing on AWS|1042|8|2|
|DDD par la pratique|740|8|7|
|Premier mois avec AWS|609|10|1|
|Fiche de lecture Terraform Cookbook|266|3|1|
|Premier mois avec Terraform|504|8|3|
|Import en Javascript|260|2|2|
|Bloqué dans le TDD|819|7|2|
|Fiche de lecture : The Healthy Programmer|741|7|1|
|Pagination des API REST|378|5|2|
|Fiche de lecture : Domain Modeling Made Functional|437|2|3|
|**Médiane avec la nouvelle approche**|**489**|**5**|**2**|
|Gestion des états en React|148|3|0|
|Formation : Migrating to AWS|192|2|0|
|Ou commencer son intégration continue|237|1|1|
|TDD appliqué à un composant React -p2| 252|6|1|
|Creation d'API REST factice|222|2|3|
|Test d'une API REST|258|4|4|
|Importance de l'intégration continue|475|3|4|
|Langage interprété ou langage compilé|133|2|0|
|Bash, pipefail et local|411|6|3|
|Fiche de lecture : Refactoring databases|115|0|0|
|Reprendre un projet après 3 ans -p4|145|4|0|
|Proxy et reverse proxy|374|4|2|
|les différents types de test|957|10|2|
|Reprendre un projet après 3 ans -p3|390|8|0|
|Test clinic|1071|11|6|
|Reprendre un projet après 3 ans -p2|580|6|3|
|Fiche de lecture : Pragmatic Programmer|137|1|0|
|Passage d'une dizaine de dépôts à un -partie 3|280|5|4|
|Coder un module Drupal - p4|169|1|4|
|L'indispensable gestion de version|640|5|8|
|A priori sur le no-code|206|3|3|
|Coder un module Drupal - p3|174|2|1|
|Coder un module Drupal - p2|83|0|0|
|Utilisation des services web en front-end|113|1|0|
|Fiche de lecture : Drupal 9 Module Development|123|1|0|
|Coder un module Drupal - p1|117|1|0|
|A propos de l'encodage|238|3|0|
|Utiliser les sites d'apprentissage par le jeu|319|5|0|
|Passage d'une dizaine de dépôts à un - partie 2|165|2|0|
|Tests sur React JS|143|1|0|
|TDD appliqué à un composant React|283|5|1|
|Docker Swarm vs Kubernetes|170|2|0|
|Revue avec GitLab|311|3|1|
|**Médiane avec l'ancienne approche**|**206**|**3**|**1**|

Avec cette approche, la médiane :

- des impressions LinkedIn a doublé passant de 206 à 489 ;
- des réactions est passé de 3 à 5.

## Conclusion

Les processus et les outils de publication fonctionnent de manière fluide. La stabilité des outils m'a permis de me concentrer sur le contenu. Avec une année plutôt chargée avec des formations, des nouveaux domaines de compétences, j'ai pu capitaliser sur des connaissances et j'espère partager sur mes expériences de cette année.

Merci à tous ceux qui par leurs commentaires ou leurs discussions m'ont donné l'idée de nouveaux billets, d'amélioration ou de rectification. Si vous avez des suggestions concernant mes billets passés ou futurs n'hésitez pas à m'en faire part sur [Twitter](https://twitter.com/trambi_78), [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/) ou autour d'un bon thé.
