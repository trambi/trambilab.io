---
title: "Les tests de heatmap"
date: "2023-01-28"
draft: false
description: "en React et cela peut servir de spécifications"
tags: ["front-end","reactjs","test"]
author: "Bertrand Madet"
---

Dans [mon billet précédent]({{< ref "/post/fr/2023-01-23-heatmap-in-html.md">}}), j'ai décrit les usages des heatmaps. Je vous propose de travailler sur les tests unitaires de ce composant.

## Cas nominaux

Une heatmap va avoir 5 propriétés (ou props en React) :

- `xlabels` pour la liste des étiquettes horizontales ;
- `ylabels` pour la liste des étiquettes verticales ;
- `data` pour la double liste des données ;
- `valueToColor` pour la fonction qui retourne la couleur en fonction d'une valeur ;
- `valueToDisplay` pour la fonction qui formate la valeur.

On peut commencer par s'assurer que les propriétés sont bien affichées :

1. La liste des étiquettes horizontales s'affiche bien dans la dernière ligne ;
1. La liste des étiquettes verticales s'affiche bien dans la premier colonne ;
1. Les données s'affichent bien utilisant la fonction de formatage des données.

## Cas aux limites

On va ensuite s'intéresser aux cas aux limites. On peut s'interroger sur ce qui peut se passer mal. Par exemple, qu'est ce qu'il se passerait s'il y a plus de étiquettes verticales que de lignes de données ? S'il y a moins d'étiquettes verticales que de lignes de données ... Cette étape va obliger à préciser la définition du composant par un prisme inhabituel : les erreurs.

1. S'il y a moins d'étiquettes verticales que de lignes de données, la heatmap va afficher uniquement les données dont les étiquettes verticales sont disponibles ;
1. S'il y a plus d'étiquettes verticales que de lignes de données, la heatmap va afficher uniquement les étiquettes verticales dont les données sont disponibles ;
1. S'il y a moins d'étiquettes horizontales que de colonnes de données, la heatmap va afficher uniquement les données dont les étiquettes horizontales sont disponibles ;
1. S'il y a plus d'étiquettes horizontales que de colonnes de données, la heatmap va afficher uniquement les étiquettes horizontales dont les données sont disponibles ;
1. S'il y a des incohérences dans les longueurs de lignes de données, la heatmap va afficher toutes les données disponibles et les données indisponibles sont représentées sous la forme de cellule vide.

Le choix du comportement à adopter en cas d'erreurs va dépendre de la destination de la heatmap. Les tests des cas aux limites ont peut-être avoir plus d'intérêt pour un composant utilisé dans de multiples situations ou intégré à catalogue.

## En React

Une fois n'est pas coutume, je vous propose de regarder uniquement le code des tests de la heatmap

```typescript
import { render, screen } from "@testing-library/react";
import { Heatmap } from '.';

describe('Heatmap', () => {
  const xlabels = ['x1', 'x2', 'x3'];
  const ylabels = ['y1', 'y2', 'y3'];
  const data = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
  it('displays xlabels in last row', async () => {
    render(<Heatmap xlabels={xlabels} ylabels={ylabels} data={data} />);
    expect(await screen.findByText(xlabels[0], { selector: 'tfoot tr:nth-child(1) th:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText(xlabels[1], { selector: 'tfoot tr:nth-child(1) th:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText(xlabels[2], { selector: 'tfoot tr:nth-child(1) th:nth-child(4)' })).toBeInTheDocument();
  });
  it('displays ylabels in first row and data in last rows', async () => {
    render(<Heatmap xlabels={xlabels}
      ylabels={ylabels}
      data={data} />);
    expect(await screen.findByText(ylabels[0], { selector: 'tr:nth-child(1) th:nth-child(1)' })).toBeInTheDocument();
    expect(screen.getByText(ylabels[1], { selector: 'tr:nth-child(2) th:nth-child(1)' })).toBeInTheDocument();
    expect(screen.getByText(ylabels[2], { selector: 'tr:nth-child(3) th:nth-child(1)' })).toBeInTheDocument();
    expect(screen.getByText('1', { selector: 'tr:nth-child(1) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('2', { selector: 'tr:nth-child(1) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('3', { selector: 'tr:nth-child(1) td:nth-child(4)' })).toBeInTheDocument();
    expect(screen.getByText('4', { selector: 'tr:nth-child(2) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('5', { selector: 'tr:nth-child(2) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('6', { selector: 'tr:nth-child(2) td:nth-child(4)' })).toBeInTheDocument();
    expect(screen.getByText('7', { selector: 'tr:nth-child(3) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('8', { selector: 'tr:nth-child(3) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('9', { selector: 'tr:nth-child(3) td:nth-child(4)' })).toBeInTheDocument();
  });
  it('displays data transformed with valueToDisplay function given', async () => {
    const valueToDisplay = (value: number) => `${(value * 100).toFixed(0)} %`;
    const dividedByTenData = data.map(row=>row.map(value=>value/10));
    render(<Heatmap xlabels={xlabels}
      ylabels={ylabels}
      data={dividedByTenData}
      valueToDisplay={valueToDisplay} />);
    expect(await screen.findByText('10 %', { selector: 'tr:nth-child(1) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('20 %', { selector: 'tr:nth-child(1) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('30 %', { selector: 'tr:nth-child(1) td:nth-child(4)' })).toBeInTheDocument();
    expect(screen.getByText('40 %', { selector: 'tr:nth-child(2) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('50 %', { selector: 'tr:nth-child(2) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('60 %', { selector: 'tr:nth-child(2) td:nth-child(4)' })).toBeInTheDocument();
    expect(screen.getByText('70 %', { selector: 'tr:nth-child(3) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('80 %', { selector: 'tr:nth-child(3) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('90 %', { selector: 'tr:nth-child(3) td:nth-child(4)' })).toBeInTheDocument();
  });
  it('displays only data rows with associated ylabels when given missing ylabels', async()=>{
    const tooFewYlabels = ['y1','y2'];
    render(<Heatmap xlabels={xlabels}
      ylabels={tooFewYlabels}
      data={data} />);
    expect(await screen.findByText(tooFewYlabels[0], { selector: 'tr:nth-child(1) th:nth-child(1)' })).toBeInTheDocument();
    expect(screen.getByText(tooFewYlabels[1], { selector: 'tr:nth-child(2) th:nth-child(1)' })).toBeInTheDocument();
    expect(screen.getByText('1', { selector: 'tr:nth-child(1) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('2', { selector: 'tr:nth-child(1) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('3', { selector: 'tr:nth-child(1) td:nth-child(4)' })).toBeInTheDocument();
    expect(screen.getByText('4', { selector: 'tr:nth-child(2) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('5', { selector: 'tr:nth-child(2) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('6', { selector: 'tr:nth-child(2) td:nth-child(4)' })).toBeInTheDocument();
    expect(screen.queryByText('7', { selector: 'tr:nth-child(3) td:nth-child(2)' })).not.toBeInTheDocument();
    expect(screen.queryByText('8', { selector: 'tr:nth-child(3) td:nth-child(3)' })).not.toBeInTheDocument();
    expect(screen.queryByText('9', { selector: 'tr:nth-child(3) td:nth-child(4)' })).not.toBeInTheDocument();
  });
  it('displays only ylabels associated with data rows', async()=>{
    const tooManyYlabels = ['y1','y2','y3','y4'];
    render(<Heatmap xlabels={xlabels}
      ylabels={tooManyYlabels}
      data={data} />);
    expect(await screen.findByText(tooManyYlabels[0], { selector: 'tr:nth-child(1) th:nth-child(1)' })).toBeInTheDocument();
    expect(screen.getByText(tooManyYlabels[1], { selector: 'tr:nth-child(2) th:nth-child(1)' })).toBeInTheDocument();
    expect(screen.getByText(tooManyYlabels[2], { selector: 'tr:nth-child(3) th:nth-child(1)' })).toBeInTheDocument();
    expect(screen.queryByText(tooManyYlabels[3], { selector: 'tr:nth-child(4) th:nth-child(1)' })).not.toBeInTheDocument();
    expect(screen.getByText('1', { selector: 'tr:nth-child(1) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('2', { selector: 'tr:nth-child(1) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('3', { selector: 'tr:nth-child(1) td:nth-child(4)' })).toBeInTheDocument();
    expect(screen.getByText('4', { selector: 'tr:nth-child(2) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('5', { selector: 'tr:nth-child(2) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('6', { selector: 'tr:nth-child(2) td:nth-child(4)' })).toBeInTheDocument();
    expect(screen.getByText('7', { selector: 'tr:nth-child(3) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('8', { selector: 'tr:nth-child(3) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('9', { selector: 'tr:nth-child(3) td:nth-child(4)' })).toBeInTheDocument();
  });
  it('displays only data columns with associated xlabels when given missing xlabels', async()=>{
    const tooFewXlabels = ['x1','x2'];
    render(<Heatmap xlabels={tooFewXlabels}
      ylabels={ylabels}
      data={data} />);
    expect(await screen.findByText(tooFewXlabels[0], { selector: 'tfoot tr:nth-child(1) th:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText(tooFewXlabels[1], { selector: 'tfoot tr:nth-child(1) th:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('1', { selector: 'tr:nth-child(1) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('2', { selector: 'tr:nth-child(1) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.queryByText('3', { selector: 'tr:nth-child(1) td:nth-child(4)' })).not.toBeInTheDocument();
    expect(screen.getByText('4', { selector: 'tr:nth-child(2) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('5', { selector: 'tr:nth-child(2) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.queryByText('6', { selector: 'tr:nth-child(2) td:nth-child(4)' })).not.toBeInTheDocument();
    expect(screen.getByText('7', { selector: 'tr:nth-child(3) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('8', { selector: 'tr:nth-child(3) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.queryByText('9', { selector: 'tr:nth-child(3) td:nth-child(4)' })).not.toBeInTheDocument();
  });
  it('displays all available data', async()=>{
    const data = [[1,2,3],[4,5,6,6.5],[7,8]]
    render(<Heatmap xlabels={xlabels}
      ylabels={ylabels}
      data={data} />);
    expect(await screen.findByText('1', { selector: 'tr:nth-child(1) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('2', { selector: 'tr:nth-child(1) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('3', { selector: 'tr:nth-child(1) td:nth-child(4)' })).toBeInTheDocument();
    expect(screen.getByText('4', { selector: 'tr:nth-child(2) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('5', { selector: 'tr:nth-child(2) td:nth-child(3)' })).toBeInTheDocument();
    expect(screen.getByText('6', { selector: 'tr:nth-child(2) td:nth-child(4)' })).toBeInTheDocument();
    expect(screen.queryByText('6.5', { selector: 'tr:nth-child(2) td:nth-child(5)' })).not.toBeInTheDocument();
    expect(screen.getByText('7', { selector: 'tr:nth-child(3) td:nth-child(2)' })).toBeInTheDocument();
    expect(screen.getByText('8', { selector: 'tr:nth-child(3) td:nth-child(3)' })).toBeInTheDocument();
  });
});
```

## Conclusion

On voit que les tests peuvent servir de spécifications pour les composants. La partie des cas nominaux est assez directe. Les cas aux limites nécessitent d'avantages de réflexion . Par exemple, en précisant les comportements attendus avec le product owner ou de l'UX designer.

Si vous avez des conseils sur les tests de composants, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
