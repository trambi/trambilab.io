---
title: "Premiers pas avec Terraform"
date: "2022-09-07"
draft: false
description: "Langage déclaratif et AWS"
tags: ["cloud","DevOps","outils","terraform"]
author: "Bertrand Madet"
---

Cette semaine, j'ai commencé à utiliser [Terraform (https://www.terraform.io/)](https://www.terraform.io/) avec AWS. Je propose de partager les enseignements que j'en ai tiré en essayant d'abstraire des problématiques liées à ma non connaissance des réglages fins d'AWS.

Avant de détailler ce que j'en ai appris, parlons un peu de Terraform.

## Terraform

Terraform est un outil de **gestion de l'infrastructure à partir d'un code** créé par [HashiCorp (https://hashicorp.com)]((https://hashicorp.com)). Par gérer, j'entends créer, modifier ou bien détruire des ressources d'infrastructure. Les ressources d'infrastructures peuvent être des machines virtuelles, des réseaux, des sous-réseaux, des bases de données... Cela permet de gérer son infrastructure dans un dépôt Git avec les mêmes processus de contrôle, de revue que le code source applicatif.

> Bref Terraform est un outil `IaS` (:gb: **I**nfrastructure **a**s **C**ode).

Le code est une description de l'infrastructure cible qui prend la forme des fichiers dans un langage déclaratif le `HCL` (:gb: **H**ashi**c**orp **L**anguage). Terraform existe sous forme d'interface en ligne de commande (`CLI`) ou de kit de développement (`CDK`) pour différents langages (TypeScript, Python, Java, Go ou C#).

> :teacher: Un langage déclaratif décrit la situation à atteindre. Le [HTML](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language), l'[Askell](https://fr.wikipedia.org/wiki/Haskell) sont des langages déclaratifs. Les langages déclaratifs sont l'inverse des langages impératifs qui fournissent le mode opératoire.

Terraform s'appuie sur un fichier d'état qui représente l'état actuel du système. Le code représente l'état désiré. Quand on utilise les commandes comme `terraform plan`, `terraform apply` ou `terraform destroy`, il va faire une différence entre l'état actuel et l'état désiré puis appliquer les modifications pour arriver de l'état actuel à l'état désiré. Une fois les modifications réalisés, Terraform va mettre à jour son état actuel.

Cela peut se représenter grossièrement par le diagramme de séquence ci-dessus.

```mermaid
sequenceDiagram
    Utilisateur->>Terraform: terraform apply
    Terraform->>Terraform: Analyse l'état désiré
    Terraform->>Stockage: Quel est l'état actuel ?
    Stockage-->Terraform: L'état actuel est comme ça
    Terraform->>Terraform: Crée un plan pour arriver à l'état désiré à partir de l'état actuel
    Terraform->>Utilisateur : Voici mon plan pour arriver à votre état désiré, êtes-vous d'accord ?
    Utilisateur->>Terraform: Oui
    Terraform->>Cloud : Applique ce plan (créer telles ressources, modifier telles ressources, détruire telles ressources)
    Cloud->>Cloud : Application des changements
    Terraform-->Utilisateur : Évolution des changements
    Cloud-->Terraform : Ok
    Terraform->>Stockage : Voici le nouveau état actuel
    Terraform->>Utilisateur : C'est terminé
```

## Mise en place

J'ai testé uniquement la ligne de commande pour l'instant. L'installation est possible sans droit d'administration sous Windows. Pour que cela fonctionne, il faut configurer les variables d'environnement :

- `PATH` pour ajouter le chemin de l'exécutable `terraform`;
- `HTTP_PROXY` pour configurer le proxy pour le protocole `HTTP`;
- `HTTPS_PROXY` pour configurer le proxy pour le protocole `HTTPS`;

L'extension Visual Code [HashiCorp Terraform](https://marketplace.visualstudio.com/items?itemName=HashiCorp.terraform) permet d'utiliser l'auto complétion, le formatage, la vérification directement dans l'IDE. La ligne de commande fournit une commande de formatage avec `terraform fmt`.

Une fois ces deux étapes réalisées, il faut configurer deux paramètres :

- le lieu de stockage de l'état géré par Terraform - par défaut c'est un répertoire local ;
- le fournisseur de cloud dont nous allons utiliser les ressources en indiquant le `provider` à utiliser.

> Il peut être intéressant d'utiliser un stockage distant pour partager cet état.
> Il est possible d'utiliser plusieurs providers si on veut gérer une infrastructure sur plusieurs fournisseurs cloud.

Vous êtes alors prêt à initialiser terraform avec `terraform init`.

## Modules pour factoriser

La description d'une instance d'un service cloud nécessite parfois de décrire plusieurs ressources cloud. Par exemple, une instance du service `AWS Lambda` (une fonction qui s'exécute sans avoir à gérer des serveurs) contient 4 ressources :

- l'archive du code source de la fonction ;
- la ressource objet dans un compartiment S3 contenant l'archive du code source ;
- la ressource `AWS Lambda` ;
- le rôle autorisant l'exécution de la ressource Lambda.

```mermaid
graph LR;
    lambda[Lambda]--->source(Code source compressé)
    subgraph "Objet dans un compartiment S3"
        source
    end
    lambda-->role[Rôle autorisant l'exécution de la Lambda]
```

Pour d'autres services, le nombre peut être encore plus grand. Terraform propose le concept de module pour regrouper la description de plusieurs ressources liées. On peut importer des modules à partir du dépôt public de HashiCorp, d'un dépôt Git ou dans un répertoire local.

J'ai testé l'utilisation de modules depuis un dépôt Git ou d'en créer un dans un répertoire local. La création d'un module local nécessite la création :

- d'un répertoire ;
- d'un fichier qui décrit les paramètres d'entrées du module ;
- d'un fichier qui décrit les paramètres de sorties du module ;
- d'un fichier de description des ressources du module.

C'est assez facile, j'ai créé un module pour gérer la description d'une Lambda dont le code source est contenu dans un répertoire.

## Conclusion

L'utilisation de Terraform rend la livraison continue encore plus poussée car cela permet de gérer l'infrastructure de la même manière que le code. Cela est particulièrement adapté aux infrastructures sur le cloud car on peut créer et détruire des environnements de manière automatique.

:warning: Le `provider AWS` ne représente pas d'abstraction des ressources : Il faut fournir tous les détails que l'on fournirait pour créer la ressource par l'interface web d'administration. C'est normal, attendu et cela doit être le cas pour tous les `providers` des fournisseurs cloud car il n'est pas question d'abstraction de l'infrastructure mais de description de l'infrastructure. Cela fait qu'il faut connaître les services Cloud que l'on veut décrire, rien de magique en somme.

Les modules fournis par HashiCorp, par votre entreprise ou votre équipe sont là pour permettre un niveau d'abstraction entre votre description et les ressources. Le choix des modules utilisés pour vous faciliter la tâche va dépendre évidemment de votre contexte d'utilisation.

Si vous voulez partager sur l'utilisation de Terraform ou discuter d'outils DevOps, contactez-moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
