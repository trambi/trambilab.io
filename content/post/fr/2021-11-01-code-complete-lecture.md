---
title: "Code complete - Fiche de lecture"
date: "2021-11-01"
draft: false
description: "de Steve McConnell sur la programmation" 
author: "Bertrand Madet" 
tags: ["fiche de lecture","code","base","Software Craftmanship"]
---

## A qui s'adresse ce livre ?

Ce livre s'adresse à toutes les personnes qui programment et qui veulent améliorer leur manière de programmer.

## Ce qui faut retenir

Code complete fournit une revue détaillée de pratiques, de conseils et de checklists sur la programmation. L'approche est holistique : de l'analyse des exigences aux pratiques pour l'amélioration de la qualité du code en passant par des conseils sur le nommage des variables.

## Mon opinion

| Points positifs | Points négatifs |
|---|---|
| :+1: Complet, je n'ai pas trouvé d'aspect de la programmation qui n'est pas mentionné| :-1: Énorme, plus de 800 pages|
| :+1: La description de méthodologie `PPP` (**P**seudocode **P**rogramming **P**rocess) est intéressante quand on n'utilise pas le `TDD` (**T**est **D**riven **D**evelopment)| :-1: On sent la date de cette édition (2004), avec des références qui datent, des langages marquants `Rust` ou `Golang` qui manquent|
| :+1:  La partie sur les inspections formelles de code est bien détaillée et donne envie d'essayer ... ||

Ce livre est difficile à lire de manière linéaire et porte bien son nom c'est vraiment complet sur l'activité de programmation. Ce livre a sa place dans la bibliothèque d'un développeur professionnel. Je conseillerai plutôt de picorer et de parcourir :

- les parties sur les pré-requis et sur les décisions clés de construction avant de se lancer dans un nouveau programme ;
- les parties sur le design avant de se lancer dans un nouveau module ;
- une partie sur la programmation (classe, routine ou programmation défensive ...) après une bonne session de codage.

## Détails

Code Complete de Steve McConnell

ISBN: 978-0735619678

Pas disponible en :fr:
