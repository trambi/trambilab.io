---
title: "Premier pas avec Vue.js"
date: "2024-05-02"
draft: false
description: "pour l'administration de mon projet perso"
tags: ["front-end","monorepository","vuejs","typescript","projet perso"]
author: "Bertrand Madet"
---

Suite à la première version exploitable de mon API REST sur laquelle j'ai écrit quelques billets : [La présentation est ici 👇]({{< ref "/post/fr/2023-11-21-rewrite-side-project-api-in-go-part0" >}}), j'ai choisi d'essayer [Vue.js (https://vuejs.org/)](https://vuejs.org/) pour faire l'application d'administration. L'idée est d'avoir une application web dont les données sont obtenues par appels à l'API REST, les opérations sont aussi réalisées par des appels à l'API REST.

```mermaid
graph LR;
    orga([Organisateur])-->admin[Application d'administration web v2 - TypeScript + VueJs]
    admin-->api[API REST v2 - Go + gorilla/mux]
    api-->db[(Base de données - Sqlite ou PostgreSQL)]
```

Pour rappel, j'avais auparavant une application [Symfony (https://symfony.com/)](https://symfony.com/) (version 3 donc plutôt ancienne) avec [BootStrap (https://getbootstrap.com/)](https://getbootstrap.com/) pour l'affichage.

## Démarrage rapide

Le premier contact a été très agréable avec un lancement de projet facile :

```bash
npm create vue@latest
```

Le script pose des questions sur le projet, l'utilisation du TypeScript est la première question 🎉. On a un projet avec squelette d'application vue.js et tout un écosystème: 

- [vite (https://vitejs.dev)](https://vitejs.dev) pour la construction et le mode de développement ;
- [vitest (https://vitest.dev/)](https://vitest.dev/) pour lancer les tests ;
- [Vue Test Utils (https://test-utils.vuejs.org/)](https://test-utils.vuejs.org/) pour tester les composants Vue ;
- [Vue Router (https://router.vuejs.org/)](https://router.vuejs.org/) pour gérer les changements de "page" via l'url du navigateur...

Vite et Vitest sont vraiment rapides et permettre de ne plus attendre pour voir les résultats de ses modifications soit sur le navigateur pour Vite soit sur les tests pour Vitest. C'est très agréable, cela m'a rappelé l'utilisation de `bun test` peut-être un peu moins rapide, mais il me semble plus stable.

Le fait d'avoir des réponses intégrés aux besoins liés au application mono-page permet de lancer directement dans son application sans devoir étudier les différentes possibilités pour chaque besoin.

L'inconvénient, c'est qu'il y a donc beaucoup de documentation 📚 à parcourir parce l'écosystème était différent de celui que j'utilisais avec React. Donc on arrive à lancer une application mais assez rapidement, on est obligé de lire la documentation.

## Similitudes et différences avec React

En vue.js, il y a deux manières de décrire ses composants en "Option" ou "Composition". Je ai utilisé la composition car c'était la manière recommandée .
Les similitudes et les différences avec React sont déjà sans doute biaisés que je ne connais pas d'autres bibliothèques graphiques ou frameworks front (à part JQuery 🧓).

La logique des composants me semble identique, dans la mesure où tout est un composant, il n'y a pas de catégorie de composants. Ensuite les utilisations des propriétés qui ont le même nom "props". Le côté réactif sur les changements de propriétés et la gestion des états avec `ref` au lieu de `useState`.

Côté différences, il y a : 

- l'utilisation d'un fichier unique 1️⃣ `.vue` avec la logique, le rendu et le style ;
- la séparation entre les propriétés et les évènements d'un composant ;
- la prise en compte des spécificités des formulaires côté vue.js.

L'utilisation d'un fichier unique permet de gérer le style propre au composant alors qu'en React, il faut utiliser une autre bibliothèque pour gérer le style des composants - [Emotion (https://emotion.sh/docs/introduction)](https://emotion.sh/docs/introduction) ou [styled-components (https://styled-components.com/)](https://styled-components.com/). Cela peut être un obstacle à la séparation des responsabilités mais pour les composants que j'ai pu coder, j'ai apprécié la cohésion que cela donne.

La séparation entre les propriétés et les évènements amène de la clarté dans les intentions. De même les attributs `v-if` et `v-for` permettent d'être plus explicites sur ses intentions.

En React, on aurait quelque chose cela :

```jsx
{ ready && <MyComponent />}
```

Alors qu'en vue.js, on aurait quelque chose cela :

```html
<MyComponent v-if="ready" />
```

Et je vous fais grâce des rendus de liste qui sont beaucoup complexes en React 🤯 (je vous laisse regarder le rendu de liste en [vue.js (https://fr.vuejs.org/guide/essentials/list.html](https://fr.vuejs.org/guide/essentials/list.html) et en [React (https://fr.react.dev/learn#rendering-lists)](https://fr.react.dev/learn#rendering-lists).

## Tout n'est pas rose 🌹

Je trouve que les outils de test `Vue Test Utils` sont moins agréables à utiliser que la [Testing Library (https://testing-library.com/)](https://testing-library.com/). Les sélections se font uniquement sur sélecteur CSS, là ou la Testing Library permet de sélection par alias, par texte, par sélecteur CSS... 


Côté TypeScript, j'ai été obligé d'écrire ce genre de conversion pour vérifier la valeur d'un champ ! 😲

```typescript
// le type wrapper.find('input#name').element est un VueNode<HTMLElement> 
// qui ne contient pas la propriété value
expect((wrapper.find('input#name').element as VueNode<HTMLInputElement>).value).toContain(coach.name);
```

J'ai aussi été obligé de faire des conversions brutales pour des exemples dans la documentation de vue-router sur la [réaction aux changements de paramètres (https://router.vuejs.org/guide/essentials/dynamic-matching.html#Reacting-to-Params-Changes)](https://router.vuejs.org/guide/essentials/dynamic-matching.html#Reacting-to-Params-Changes).

La documentation propose uniquement ce code JavaScript.

```javascript
import { watch } from 'vue'
import { useRoute } from 'vue-router'

const route = useRoute()

watch(() => route.params.id, (newId, oldId) => {
  // react to route changes...
})
```

Voilà ce que j'ai dû faire côté TypeScript pour que le compilateur TypeScript fonctionne et que l'application fonctionne comme attendu.

```typescript
import { watch } from 'vue'
import { useRoute } from 'vue-router'

const route = useRoute()

watch(() => route.params.id, async (mistypedId: () => string | string[]) => {
  // mistypedId est vraiment une chaîne de caractère
  // ou un tableau de chaîne de caractère lors de l'exécution
  // et pas une fonction
  const id = mistypedId as unknown as string;
  // react to route changes...
})
```

> 🤏 `as unknown as string` correspond à une conversion brutale du genre tu ne connais pas ce type, fais-moi confiance ...

Côté des types et de l'aspect vérification en amont, on repassera...

## Conclusion

Pour l'instant, je suis plutôt satisfait par l'utilisation de vue.js. Cette bibliothèque semble avoir gommé une partie des défauts de React. Les problèmes de typage ne sont pas trop gênant mais je n'ai pas trouvé de solutions sur Internet, ce qui provient peut-être d'une communauté plus petite que celle de React.

Si vous avez des conseils sur l'utilisation de vue.js, n'hésitez pas à les partager avec moi sur [X](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/). Je vais continuer pour finir mon application d'administration et j'essayerai de rendre compte des évolutions.
