---
title: "L'histoire des coding dojos"
date: "2021-03-14"
draft: false
description: "dans mon entité"
tags: ["dojo","Software Craftmanship","histoire"]
author: "Bertrand Madet"
---

Il y a presque trois ans, j'ai lu Coding Dojo Handbook - [fiche de lecture]({{< ref "/post/fr/2021-01-24-coding-dojo-handbook-lecture.md" >}}). Après cette lecture, j'étais motivé pour organiser des sessions dans mon entité. Dans un [billet précédent - Coding dojo en 3 phrases]({{< ref "/post/fr/2021-01-31-coding-dojo-en-3-phrases.md" >}}) , je vous ai confié mes conseils pour organiser des séances. Et je vous livre maintenant, l'histoire derrière ces conseils.

## Sessions alpha et bêta

J'ai commencé avec une session "alpha" (uniquement des personnes intéressées et bienveillantes) en juillet. J'ai choisi les vacances d'été car j'ai souvent plus de temps et moins la tête dans le guidon. La séance s'est globalement bien passée et j'ai pu recueillir des retours intéressants.  Cela a servi à préparer des supports sur le `TDD` (*programmation pilotée par les tests*) et sur le `pair programmation` (*programmation en binôme*). Le sujet était de coder une fonction qui à partir d'un nombre retournait la chaîne de caractères de ce nombre en numérotation romaine.

J'ai fait une deuxième session en août, la session "bêta", avec le même sujet mais des participants différents (et tout aussi bienveillants), et j'ai pu appliquer les retours de la session de juillet et valider le format global. J'étais prêt pour lancer en septembre les coding dojos.

## Entre le lancement et le premier confinement

Jusqu'au premier confinement, nous avons eu au minimum une séance mensuel en Python (langage le plus pratiqué dans mon entité). Une séance mensuelle en langage libre et niveau libre (la session Python nécessitait de connaître Python) s'est naturellement ajoutée. La variété des thèmes a augmenté avec des sessions d'entraînement à l'utilisation de MongoDB, GraphDB, Docker ou encore l'intégration continue en gardant une session par mois de Python.

La fréquentation variait de quatre à personnes. C'est à ce moment que j'ai commencé à avoir des supports à l'organisation et à l'animation.

Avec le confinement, nos sessions (et nos activités) ont connu un coup d'arrêt brutal. L'activité partielle était la principale cause. Mais plus important, ces sessions n'étaient pas prêtes pour la distance. Ce qui est un inconvénient aujourd'hui mais aussi dans le futur. Je suis en effet convaincu que le télétravail aura plus place à l'avenir.

## Rebond et adaptation au télétravail

Après un arrêt de plusieurs mois, j'ai repris avec un format plus simple : une heure de visio-conférence (avec [Jitsi - https://meet.jit.si](https://meet.jit.si)). Les sessions utilisent :

 - Pour le code : [CodeWars (https://www.codewars.com)](https://www.codewars.com);
 - Pour git : [Learn Git Branch](https://learngitbranching.js.org/);
 - Pour les agrégats MongoDB : [Mongo Playground (https://mongoplayground.net/)](https://mongoplayground.net/);
 - Pour le SQL : [PostgreSQL Exercises (https://pgexercises.com/)](https://pgexercises.com/).

 Le plus dur a été et reste de réinstaller l'habitude de venir, j'ai opté pour un créneau hebdomadaire. La fréquentation est moins importante, mais j'espère que cela remontera avec le temps. Je vais essayer de trouver un créneau d'une heure et demi pour permettre de faire plus de choses. Une heure de visio-conférence peut vite être grignotée par les incidents techniques des uns et des autres.

## Conclusion

J'espère que ce partage d'expérience vous aideront à mettre en place un coding dojo ou à améliorer l'existant. Si vous vous voulez partager votre histoire autour des coding dojos, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).