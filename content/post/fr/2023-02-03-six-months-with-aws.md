---
title: "Six mois sur AWS"
date: "2023-02-03"
draft: false
description: "On s'éloigne de la lune de miel"
tags: ["cloud"]
author: "Bertrand Madet"
---

Depuis six mois, je passe mes journées à essayer d'implémenter des services [AWS (https://aws.amazon.com/fr/)](https://aws.amazon.com/fr/). J'avais fourni une première impression après un mois : [Un mois sur AWS]({{< ref "/post/fr/2022-09-22-first-steps-with-aws.md">}}).

Voici un rappel de mes conseils (que je maintiens) :

- Utilisez la langue anglaise :gb: pour l'interface d'administration AWS ;
- Prenez le temps de comprendre sur les concepts d'IAM, VPC et Cloudwatch.

## Les services simples

Le service de stockage `S3`, les traitements serverless `lambda` et la passerelle d'API `API Gateway` sont très puissants. Même si ces outils permettent des utilisations avancées, ils sont simples d'usage.

En `infrastructure as code`, la description des ressources est plutôt simple. Par exemple, en utilisant le format OpenAPI pour décrire l'API Gateway cf. [mon billet]({{< ref "/post/fr/2022-10-13-document-rest-api-with_openapi.md">}}).

## Le mode d'autorisation est surprenant

Le modèle de permission est basé sur les rôles et le principe de moindre privilège. [IAM (https://aws.amazon.com/fr/iam/)](https://aws.amazon.com/fr/iam/) qui permet de gérer les identités et l'accès aux services AWS. Cela change beaucoup des noms d'utilisateur et des mots de passe.

Le principe est simple : Ce qui n'est pas explicitement autorisé est interdit. Mais dans une situation où il y a une myriade de services, la composition des permissions et des rôles peut devenir complexe et on peut facilement oublier ce nouveau modèle.

## Les VPC sont compliqués

Les [VPC (https://aws.amazon.com/fr/vpc)](https://aws.amazon.com/fr/vpc) permettent d'isoler les ressources Cloud. L'utilisation des VPC amènent des nombreux ressources liés au réseau : sous-réseaux, tables de routage, points de terminaison, groupes de sécurité... Le processus d'isolation complexifie la configuration des ressources associées à un VPC. Par exemple, la configuration d'une ressource varie :

- Pour une API Gateway, le rattachement est fait les VPC endpoints ;
- Pour une lambda, le rattachement est avec les sous-réseaux et les groupes de sécurité ;
- Ce n'est pas possible pour un stockage S3 et le service de registre d'images de conteneur ECR mais les ressources associées à un VPC doivent utiliser un VPC endpoint.

Il faut ajouter qu'il y a des notions réseau moins évidentes pour des développeurs comme la résolution des noms de domaine depuis le VPC. Bref, cela complique grandement l'infrastructure et la description de l'infrastructure.

A noter que l'association d'une ressource à un VPC peut limiter sa capacité de passer à l'échelle, ainsi une lambda verra son nombre d'exécution simultané limité par le adresses disponibles dans ses sous-réseaux de rattachement.

## Conclusion

Je reste impressionné par la multiplicité des services. La logique de composition et de l'extrême configuration impliquent d'assimiler les concepts d'utilisation de plusieurs services pour implémenter une fonctionnalité.

Il n'y a pas de magie, les services complexes impliqueront des infrastructures complexes à décrire. Le défi d'apprentissage et d'assimilation des services AWS est important car même si la documentation est bien faite, les cas les plus complexes sont moins documentés.

Si vous voulez partager sur l'utilisation d'AWS ou d'autres fournisseurs cloud, contactez moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
