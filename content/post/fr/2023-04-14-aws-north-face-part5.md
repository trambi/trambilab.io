---
title: "Décrire une API Gateway privée avec une url personnalisée"
date: "2023-04-14"
draft: false
description: "en trichant un peu"
tags: ["terraform","REST API","cloud","outils"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).

-------

Cette semaine, je vous propose une solution à un problème qui m'a occupé ces derniers temps : l'utilisation d'une API Gateway privée avec une url personnalisée.

## API Gateway privée ?

J'ai déjà souvent parlé des [API Gateway](https://docs.aws.amazon.com/fr_fr/apigateway/), pour résumer c'est un service très puissant de "porte d'entrée" de services Web : API REST, Websockets ou même sites Internet. Il est possible d'affecter cette passerelle à un cloud privé virtuel ou [VPC](https://aws.amazon.com/fr/vpc) pour les habitués à AWS :wink:. Les utilisateurs peuvent accéder aux API Gateway privées via un `point de terminaison VPC` (:gb: `VPC Endpoint`) de type API Gateway.

```mermaid
graph LR;
    subgraph VPC
      vpcendpoint[Point de terminaison VPC]-->apigateway1[API Gateway 1]
      vpcendpoint-->apigateway2[API Gateway 2]
      apigateway1-->others(((Autres ressources du VPC)))
      apigateway2-->others
    end
    users{Utilisateurs}-->vpcendpoint
```

## URL

Dans le cas d'un accès à une API Gateway privée, l'url a le format suivant `https://<api-id>-<vpce-id>.execute-api.<region>.amazonaws.com/<stage>` où :

- `<api-id>` est l'identifiant de l'API Gateway, il est fixé par AWS ;
- `<vpce-id>` est l'identifiant du point de terminaison VPC, il est fixé par AWS ;
- `<region>` est l'identifiant de la région AWS utilisé, il est choisi par le créateur de la ressource ;
- `<stage>` est le nom du `stage` de déploiement, il est choisi par le créateur de la ressource.

Pour la région "eu-west-3" (Paris), avec un stage qui s'appelle "dev", une api-id qui vaut "de4db3e" et un vpce-id qui vaut "vpce-032a826a" cela donne l'url suivante : `https://de4db3e-vpce-032a826a.execute-api.eu-west-3.amazonaws.com/dev`.

On voit que l'url est plutôt longue et elle peut changer. Pour l'url plutôt longue, ce n'est pas très grave avec les liens hypertexte et les favoris. En revanche cela peut être vraiment embêtant d'avoir une url qui change suite à une modification de l'infrastructure. AWS a prévu de pouvoir personnaliser l'url d'une API Gateway avec un nom de domaine **sauf** pour les API Gateway privées. :cry:

Pas de surprise, c'est documenté dans la page [Configuration des noms de domaine personnalisés pour les API REST (https://docs.aws.amazon.com/fr_fr/apigateway/latest/developerguide/how-to-custom-domains.html)](https://docs.aws.amazon.com/fr_fr/apigateway/latest/developerguide/how-to-custom-domains.html).

> (...) Les noms de domaine personnalisés ne sont pas pris en charge pour les API privées. (...)

## Équilibreur de charge à la rescousse

J'étais bloqué sur ce problème quand j'ai trouvé l'article [AWS Private API Gateway with Custom domain names (https://medium.com/codex/aws-private-api-gateway-with-custom-domain-names-350fee48b406)](https://medium.com/codex/aws-private-api-gateway-with-custom-domain-names-350fee48b406). L'idée a donc germé d'utiliser un équilibreur de charge.

L'équilibreur de charge ou [ALB](https://docs.aws.amazon.com/fr_fr/elasticloadbalancing/latest/application/application-load-balancers.html) chez AWS (pour **A**pplication **L**oad **B**alancer) permet normalement de répartir la charge réseau entre différentes machines pour permettre de supporter des charges (nombre de requêtes élevées par exemple). Il a le bon goût de pouvoir être rattaché à un VPC et d'avoir un nom de domaine personnalisé. On peut donc ajouter un ALB entre nos utilisateurs pour permettre de continuer d'utiliser une API Gateway privée et un nom de domaine personnalisé.

Pour présenter un certificat valide pour la connexion HTTPS (et la clé privée associée au protocole TLS), il faut aussi associer un certificat dans [ACM](https://docs.aws.amazon.com/fr_fr/acm/latest/userguide/acm-overview.html) (pour **A**WS **C**ertificate **M**anager). C'est le cas pour tous les noms de domaines personnalisés.

```mermaid
graph LR;
    subgraph VPC
      alb[Équilibreur de charge applicatif]-->vpcendpoint
      vpcendpoint[Point de terminaison VPC]-->apigateway1[API Gateway 1]
      vpcendpoint-->apigateway2[API Gateway 2]
      apigateway1-->others(((Autres ressources du VPC)))
      apigateway2-->others
    end
    alb -...-> certificate[Certificat dans ACM]
    users{Utilisateurs}-->alb
```

## En Terraform ?

L'équilibreur de chargeur AWS utilise les concepts de :

- `listener` qui constitue un point d'entrée de l'ALB ;
- `target group` qui constitue les sorties possibles pour un listener ;
- `target group attachment` qui permet de lier une adresse IP, ou d'autres ressources (mais pas des API Gateway :worried:) à un target group.

Et étrangement même si cela n'est pas possible d'utiliser directement un nom de domaine personnalisé pour l'API Gateway, la description du nom de domaine personnalisé se fait au niveau de l'API Gateway (dans la console d'administration AWS ou avec Terraform).:exploding_head:

La description via Terraform va suivre cette logique.

:warning: Le fait de devoir donner des adresse IP (de points de terminaison VPC) est une mauvaise nouvelle car elles ne sont pas connues au moment de la planification par Terraform, ce qui impose de faire la description en deux étapes.

On va partir du principe que la description des points de terminaison VPC et du certificat dans ACM sont fait dans une première phase avec le reste des fondations réseaux. On commence par récupérer ce qui a était décrit à la première phase. Dans l'idéal, cela se fait au début mais en général sur des cas suffisamment complexe, je corrige toujours cette partie.

```HCL
# Fichier des data par exemple data.tf
# On va récupérer
# le VPC courant
data "aws_vpc" "current" {

}
# les points de terminaison VPC de la region et de type API Gateway
data "aws_vpc_endpoint" "front_api_gw" {
  vpc_id       = data.aws_vpc.current.id
  service_name = "com.amazonaws.${var.region}.execute-api"
}

# les interfaces réseaux associés aux points de terminaison
data "aws_network_interface" "front_api_gw" {
  for_each = data.aws_vpc_endpoint.front_api_gw.network_interface_ids
  id       = each.value
  filter {
    name   = "interface-type"
    values = ["vpc_endpoints"]
  }
}

# le certificat du domaine qui nous intéresse
data "aws_acm_certificate" "front" {
  domain   = "trambi.gitlab.io"
  statuses = ["ISSUED"]
}

# le groupe de sécurité qui nous intéresse
data "aws_security_groups" "allow_https_to_front" {
  name   = "allow_https_to_front"
  vpc_id = data.aws_vpc.current.id
}

# les sous-réseaux où connecter le ALB
data "aws_subnets" "front" {
  tags = {
    Usage = "front"
  }
}

```

La récupération des adresses de points de terminaison VPC est périlleuse et mis pas mal de temps à trouver cela. Ensuite, on peut décrire les ressources de l'API Gateway.

```HCL
# Dans un fichier api-gateway.tf par exemple

# On construit normalement l'API Gateway
resource "aws_api_gateway_rest_api" "front" {
  name             = "front"
  body             = templatefile("./front-openapi3.json", {
    api-gateway-role-arn = aws_iam_role.front-api-gateway.arn,
    s3-basepath-uri      = "arn:aws:apigateway:${var.region}:s3:path/${module.mons3.id}",
  })
  put_rest_api_mode = "merge"
  endpoint_configuration {
    types = ["PRIVATE"]
    vpc_endpoint_ids = [data.aws_vpc_endpoint.front_api_gw.id]
  }
}
resource "aws_api_gateway_deployment" "front" {
  rest_api_id   = aws_api_gateway_rest_api.front.id
  triggers      = {
    redeployment = sha1(jsonencode(aws_api_gateway_rest_api.front.body))
  }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_api_gateway_stage" "front" {
  deployment_id = aws_api_gateway_deployment.front.id
  rest_api_id   = aws_api_gateway_rest_api.front.id
  stage_name    = "dev"
}

# On ajoute le nom de domaine personnalisé
resource "aws_api_gateway_domain_name" "front" {
  regional_certificate_arn = data.aws_acm_certificate.front.arn
  domain_name              = "trambi.gitlab.io"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}
# On l'associe à l'API et au stage
resource "aws_api_gateway_base_path_mapping" "front" {
  api_id      = aws_api_gateway_rest_api.front.id
  stage_name  = aws_api_gateway_stage.front.stage_name
  domain_name = aws_api_gateway_domain_name.front.domain_name
}
```

Finalement, on ajoute notre équilibreur de charge.

```HCL
# Fichier de l’équilibreur de charge par exemple alb.tf ou load-balancer.tf

resource "aws_lb" "front" {
  name = "front"
  internal = true
  load_balancer_type = "application"
  security_groups = data.security_groups.allow_https_to_front.ids
  subnets = data.aws_subnets.front.ids
}

resource "aws_lb_listener" "front" {
  load_balancer_arn = aws_lb.front.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = data.aws_acm_certificate.front.arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.front.arn
  }
}

resource "aws_lb_target_group" "front" {
  name        = "front-lb-target"
  port        = 443
  protocol    = "HTTPS"
  target_type = "ip"
  vpc_id      = data.aws_vpc.current.id
  health_check {
    protocol = "HTTPS"
    matcher  = "200,403"
  }
}

resource "aws_lb_target_group_attachment" "front" {
  for_each         = data.aws_network_interface.front_api_gw
  target_group_arn = aws_lb_target_group.front
  target_id        = each.value.private_ip
  port             = 443
}
```

## Conclusion

Un grand merci à Srinivas kulkarni pour son article qui expliquait comment contourner le problème. Malgré tout la résolution du problème n'a pas été simple. La récupération  des adresses IP des points de terminaison VPC m'a pas mal posé de problème.

C'est le genre d'acrobatie que l'on ne devrait réaliser qu'en étant absolument obligé. En effet, on détourne l'utilisation d'un équilibreur de charge pour permettre cette fonctionnalité et il est dommage de devoir ajouter un composant dont l'API Gateway peut se passer. Pour rappel, le support à la charge fait partie du service API Gateway.

Je pense que cela illustre bien la difficile cohabitation entre les concepts de serverless d'un côté et de VPC de l'autre. L'un est très abstrait des machines l'autre nécessite (et permet) des réglages liés à une des fondations de l'infrastructure : le réseau.

Si vous avez des conseils, des questions ou des retours d'expériences sur l'utilisation d'AWS avec Terraform, je suis intéressé. N'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

-------

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).
