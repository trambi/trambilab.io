---
title: "Sept mois avec Terraform"
date: "2023-04-05"
draft: false
description: "Bonheurs et vicissitudes de l'infrastructure en tant que code"
tags: ["cloud","DevOps","outils","terraform"]
author: "Bertrand Madet"
---

> édité le 11 avril pour corriger des tournures de phrases.

En septembre, je commençais à travailler avec [Terraform (https://www.terraform.io/)](https://www.terraform.io/) et j'ai fait un [billet sur mes premiers pas]({{< ref "/post/fr/2022-09-07-first-steps-with-terraform" >}}). Depuis lors, je travaille dessus régulièrement. Sept mois qui m'ont permis d'affiner mon opinion sur cet outil de `IaS` (:gb: **I**nfrastructure **a**s **C**ode).

## Certaines infrastructures doivent être décrites en plusieurs parties

Le principe de description sous-jacent au Terraform permet de gérer les différences phases de vie d'une infrastructure : la création, la mise à jour et la destruction. J'ai rencontré des cas où Terraform ne peut pas créer une infrastructure en une étape.

Premier cas, un répartiteur de charge applicatif (`ALB` pour **A**pplication **L**oad **B**alancer) qui utilise des cibles de type IP va avoir besoin des adresses IP avant l'exécution de plan. Si les adresses IP sont dynamiques et obtenus après l'exécution du plan (pour des points de terminaisons VPC par exemple), on va être obligé de séparer la description de l'infrastructure en deux parties. Ce qui fait que l'on doit gérer plusieurs étapes d'infrastructure pour un même environnement et conceptuellement une même infrastructure.

```mermaid
graph LR;
subgraph "ALB en Terraform"
    listener(Ecouteur)-->alb[Répartiteur de charge applicatif]
    rule(Règle)-->listener
    rule-->targetgroup(Groupe de cible de type IP)
    attachment(Lien entre le groupe de cible et la cible)-->targetgroup
end
    attachment-->ip(Adresses IP)
```

Deuxième cas, les lambdas basées sur des conteneurs dont l'image est dans un dépôt d'un registre d'images de conteneur (`ECR`). Ce n'est pas possible de décrire le dépôt et la lambda dans le même ensemble de description. La lambda a besoin d'un identifiant précis de l'image et l'image a besoin d'un dépôt où elle sera stockée. De plus dans mon cas, la création de l'image ne pouvait pas être opérée avec Terraform mais avec une étape spécifique d'intégration continue. L'infrastructure est alors vraiment séparée en deux.

Cette séparation complique les étapes d'intégration et de livraison continue de l'usine logicielle :

- la création doit se faire dans un ordre ;
- la destruction doit se faire dans l'ordre inverse ;
- les modifications se passent généralement dans le même ordre que la création. Dans le cas d'un transfert de description de la deuxième partie à la première partie pose des problèmes si les ressources doivent être uniques : la première partie essaye de créer une ressource qui existe déjà. On peut s'en sortir en ordonnant la destruction de la deuxième partie puis en lançant la séquence de création, mais la promesse de simplifier la gestion de l'infrastructure n'est pas toujours tenue.

## Les modules

Les éléments fournis par les `provider` que j'ai pu voir (AWS et Scaleway) sont tellement bas niveau, qu'il faut avoir une connaissance détaillée de chaque ressource que l'on souhaite décrire. Les modules permettent de factoriser du code et sont plutôt simples à écrire. Ils peuvent être locaux ou à distance (via des dépôts Git par exemple). Je pense qu'ils sont encore plus pertinents :

- s'ils sont partagés en utilisant des modules à distance  ;
- s'ils intègrent des bonnes pratiques - comme par exemple utiliser une description OpenAPI 3 pour créer une API Gateway cf. [Décrire une API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3" >}}) ;
- s'ils décrivent des briques plus haut niveau - par exemple, un module de back-end comprenant un cluster de conteneur comme un service (`ECS`), une base de données, les rôles et permissions associées.

Cela suppose d'avoir pu :

- découvrir et éprouver des bonnes pratiques;
- tester et valider des architectures de référence.

Tout cela demande du temps et de la concertation. Les modules sont un formidable moyen de démultiplier d'accélérer la description des infrastructures au sein d'une équipe et même au sein d'une entité. Les modules me paraissent encore plus intéressants quand le niveau de maturité augmente.

> :warning: Si vous partagez des modules à distance, prenez soin de pointer vers une version spécifique pour garder le contrôle sur les évolutions de ce module.

## Les data sources et les boucles

Les [data sources (https://developer.hashicorp.com/terraform/language/data-sources)](https://developer.hashicorp.com/terraform/language/data-sources) permettent d'interroger des ressources pour ensuite récupérer certaines propriétés de ces ressources. La sélection des ressources se fait via des filtres obligatoires ou facultatifs. Cela est particulièrement utile si l'infrastructure n'est pas décrire entièrement par les fichiers Terraform. Par exemple, si des éléments de l'infrastructure sont fournis par une autre entité ou si l'infrastructure est décrite en deux étapes. Je n'ai pas eu encore l'occasion de regarder comment développer des data sources.

Autre grande facilitatrice : l'instruction de boucle `for_each` ! Elle permet de créer une ressource pour chaque élément d'un ensemble de chaînes de caractère ou d'un tableau associatif d'objets. Le plus utile polyvalent est le tableau associatif. Cela permet de décrire des ressources variées à partir d'un bloc de ressource et d'un tableau associatif.

Ainsi le code HCL suivant permet d'exécuter le module `my_lambda` pour chaque entrée du tableau associatif `lambdas_by_id`.

```HCL
locals {
  lambdas_by_id = {
    get_users = {
        engine = "nodejs-14"
        image_name = "get-users"
    }
    put_user = {
        engine = "python-3.8"
        image_name = "put_user"
    }
  }
}

module "my_lambda" {
    for_each = local.lambdas_by_id
    engine = each.value.engine
    image = "goharbor.io/${each.value.iname}:latest"
    # (...)
}
```

Les paramètres de la lambda "get_users" seront accessibles par "module.my_lambda[get_users]".

## Conclusion

La plupart des retours négatifs sont sans doute liés à une mauvaise connaissance de Terraform et de l'infrastructure sur lequel je l'utilise. Il me reste encore beaucoup à apprendre et expérimenter.

Terraform reste un outil puissant pour la gestion des infrastructures. Les efforts pour gérer l'infrastructure comme du code valent d'autant plus le coup dans les architectures dites serverless où l'infrastructure fusionne avec le code : les lambdas sont de l'infrastructure mais portent directement l'applicatif, une API gateway définit les liens entre chemin et les lambdas.

Si vous voulez partager sur l'utilisation de Terraform ou les concepts d'"infrastructure as code", contactez-moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
