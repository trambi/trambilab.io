---
title: "Trier une liste"
date: "2023-09-27"
draft: false
description: "en Python, JavaScript, Typescript et autres langages"
tags: ["python","javascript","typecript","Go"]
author: "Bertrand Madet"
---

Dans la continuation de mes travaux actuels, j'ai été amené à trier une liste en Python. J'avais déjà utilisé le tri en Python mais jamais avec une fonction personnalisée. Je vous propose de faire un tour des fonctions de tri pour différents langages : Python, JavaScript/TypeScript, Golang...

## Python

Python 3 propose deux fonctions pour trier une liste [sorted](https://docs.python.org/fr/3/library/functions.html#sorted) et [list.sort](https://docs.python.org/fr/3/library/stdtypes.html#list.sort). Les deux fonctions ont des comportements légèrement différents :

- `sorted` prend un itérable en premier paramètre et retourne une nouvelle liste triée ;
- `list.sort`, l'autre est une méthode d'une instance de liste (exclusivement) et retourne rien (`None`) mais modifie l'instance de liste.

Au dela de leur nature différente (fonction ou méthode), le premier paramètre de `list.sort` et le deuxième paramètre de `sorted` sont le même : le paramètre `key` qui désigne une fonction clé. La fonction clé permet de définir une clé de tri pour un élément de la liste. Cette fonction est appelée une fois par élément de liste.

```python
tobesorted = [("pas important pour le tri",5),(("peu importe",1))]
result = sorted(tobesorted,key=lambda tuple: tuple[1])
print(result)
#[('peu importe', 1), ('pas important pour le tri', 5)]
```

Il y a plein d'autres explications dans la très bonne documentation sur le [tri en python (https://docs.python.org/fr/3/howto/sorting.html)](https://docs.python.org/fr/3/howto/sorting.html).

## JavaScript et TypeScript

En Javascript et TypeScript, la fonction [sort du prototype Array](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/sort) modifie la liste triée et renvoit aussi la liste triée :exploding_head:. Son premier paramètre est une fonction de comparaison. La fonction de comparaison prend deux paramètres `a` et `b`, si :

- a doit être classé avant b alors la fonction doit renvoyer une valeur strictement négative ;
- a et b sont indifférent alors la fonction doit renvoyer zéro ;
- b doit être classé avant a alors la fonction doit renvoyer une valeur strictement positive.

```javascript
const a = [{a:1,b:2},{a:1000, b:1}];
a.sort((left,right)=> left.b-right.b);
console.log(a);
//[{a:1000, b:1},{a:1,b:2}]
```

## Go

Le Go propose un paquet standard [sort](https://pkg.go.dev/sort). La fonction `Sort` de ce paquet utilise l'interface `sort.Interface`. Pour le type liste des objets à trier, il faut définir les méthodes :

- `Len` qui renvoit la longueur de la liste ;
- `Swap` qui permet d'intervertir les deux éléments de la liste dont les index sont données en paramètres ;
- `Less` qui permet de savoir si l'élément du l'index est le premier paramètre est plus petit que l'élément dont l'index est le deuxième paramètre.

```go
import (
    "fmt"
    "sort"
)

type Something struct {
    A string
    B int
}

func (s Something) String() string {
  return fmt.Sprintf("A: %s, B: %d", s.A, s.B)
}

// ByB implements sort.Interface for []Something on
// the B field.
type ByB []Something

func (a ByB) Len() int           { return len(a) }
func (a ByB) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByB) Less(i, j int) bool { return a[i].B < a[j].B }

func main() {
    myexample := []Something{
        {"A", 3},
        {"B", 1},
        {"C", 2},
    }
    sort.Sort(ByB(myexample))
    fmt.Println(myexample)
    //[A: B, B: 1 A: C, B: 2 A: A, B: 3]
}
```

## Conclusion

En PHP, la fonction [usort](https://www.php.net/manual/en/function.usort.php) modifie le tableau passé en paramètre , le deuxième paramètre est une fonction de comparaison identique à celle de JavaScript. En C++, la bibliothèque standard [std::sort](https://en.cppreference.com/w/cpp/algorithm/sort) peut avoir une fonction de comparaison ressemblant à celle du Golang.

On le voit Python dans sa version 3 est le seul langage que je connais qui utilise une fonction clef pour le tri. Cela peut sembler limité mais on peut faire des tris complexes plus simplement qu'avec des fonctions de comparaison. Si vous voulez partager des découvertes sur un aspect d'un langage, n'hésitez pas à le faire sur [X](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
