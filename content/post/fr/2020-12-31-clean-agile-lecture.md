---
title: "Clean Agile - Fiche de lecture"
date: "2020-12-31"
description: "Livre d'Oncle Bob sur l'Agilité"
tags: ["fiche de lecture","agilité","oncle Bob","livre en français"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

À celles et ceux qui ont une connaissance de l'agilité et qui souhaitent un éclairage sur l'agilité dans le développement logiciel.

Aux développeuses et aux développeurs qui veulent l'éclairage de Robert Martin (aka Oncle Bob) sur l'agilité.

## Ce qu'il faut retenir

Pour Robert Martin, l'agilité dans l'esprit du manifeste Agile se cantonne à la résolution des problèmes de petites équipes de développement logiciel à cause des spécificité du domaine logiciel. 

Les pratiques techniques comme le développement piloté par les tests (TDD) ou le codage par binôme (Pair programming) sont les briques essentielles à l'agilité.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Le point de vue d'un des signataires du manifeste Agile sur qu'est devenue l'agilité vingt ans après.| :-1: La partie rédigée par Robert Martin est très orientée développement logiciel.|
| :+1: Les convictions et les expériences d'un développeur chevronné et reconnu (auteur notamment de Clean Code).| :-1: Il n'y a pas d'article sur des exemples de mise en place de démarche Agile pour un autre domaine que le développement logiciel.|
| :+1: La présence d'articles d'autres auteurs permet d'ouvrir les perspectives du livre.||

C'est hyper intéressant d'avoir la version de Robert Martin sur le cheminement menant à la rédaction du manifeste Agile.

Les explications sur les pratiques du développement logiciel permettent de comprendre en quoi elles sont fondamentales pour le développement logiciel.

Étant convaincu de l'applicabilité des principes agiles sur d'autres domaines que le développement logiciel, je trouve qu'un article (positif ou négatif) sur ce sujet manque.

## Détails

En anglais :gb: :

- Clean Agile - Back to Basics de Robert C. Martin avec des contributions de Jerry Fitzpatrick, Tim Ottinger, Jeff Langr, Eric Crichlow, Damon Poole et Sandro Mancuso
- ISBN-13 978-0135781869

En français :fr: :

- Agile proprement. Retour à l'essentiel de Robert C.Martin
- ISBN-13 978-2326002869

*Fiche de lecture initialement publiée en août 2020 sur LinkedIn et légèrement modifiée pour ce blog*
*Edition de mars 2022, ajout de la version française (sortie en août 2021)*
