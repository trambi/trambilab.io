---
title: "Partage du contenu d'un S3 via API Gateway et Terraform"
date: "2022-10-19"
draft: false
description: "AWS par la face nord"
tags: ["outils","REST API","cloud","terraform"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [Décrire une API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Décrire une Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).

-------

La semaine dernière, j'ai essayé de créer une infrastructure qui partage un site web stocké sur dépôt S3 via API Gateway. Je vais commencer par expliquer la phrase précédente puis nous allons étudier ce que cela implique et les difficultés auxquels j'ai été confronté et finalement comme j'utilise Terraform, le code qui permet de décrire cette infrastructure.

## Partager un site web stocké sur dépôt S3 via API Gateway

Cette expression signifie que :

- l'on stocke les fichiers d'un site web (fichiers html, fichiers javascript, fichiers de style) sur le service de stockage d'AWS `S3`;
- l'on souhaite rendre accessible le site web via HTTPS donc pour un navigateur ;
- l'on souhaite un contrôle fin sur l'accès de ce site web grâce à `API Gateway`.

:warning: Si vous souhaitez partager le site web avec le monde entier, les buckets S3 ont déjà l'option pour être partagé avec le monde entier. Dans le cas qui m'intéresse, le service `API Gateway` a l'avantage de proposer :

- le raccordement à un `VPC` - un cloud virtuel privé ;
- des mécanismes d'autorisation variés comme :
  - les jetons OAuth2,
  - les rôles IAM,
  - les lambdas pour des contrôles plus spécialisés.

## La décomposition en ressources AWS

Nous utilisons de concert deux services AWS. L'intégration avec le service S3 oblige à utiliser une API Gateway de type `REST API`.

```mermaid
graph LR;
    user(Utilisateurs)-->browser[Navigateur]
    browser-->apigateway[API Gateway - REST API]
    apigateway-->s3(Objets dans un bucket S3)
```

Comme je vous l'ai indiqué dans mon billet sur [mon premier mois avec AWS]({{< ref "/post/fr/2022-09-22-first-steps-with-aws.md">}}), les services AWS sont hautement configurables et composés de différentes ressources. 

Pour un endpoint `web` qui serait accessible par l'action HTTP `GET` dans un déploiement `dev`, cela peut se représenter sous cette forme.

```mermaid
graph LR;
    subgraph "API Gateway REST API"
        method[Méthode GET sur la ressource web]
        methodresponse[Réponse à la méthode HTTP GET ou OPTIONS]
        integration[Intégration vers un service AWS]
        integrationresponse[Réponse à l'intégration]
        resource[Ressource web de l'API REST]
        deployment[Déploiement dev d'une API REST]
        restapi[API REST]
    end
    subgraph "S3"
        object[Objet ou fichier]
        bucket[Bucket]
    end
    subgraph "IAM"
        role[Rôle IAM pour autoriser lire les objets]
    end
    user(Utilisateurs)-->browser[Navigateur]
    browser-->method
    integrationresponse-.->integration
    method-->integration
    methodresponse-.->method
    method-.->resource
    resource-.->deployment
    deployment-.->restapi
    integration-->object
    object-.->bucket
    role-.->object
    integration-.->role
```

On voit qu'il y a l'intervention du service AWS `IAM` et de sept (7) types de ressource pour une instance API Gateway de type `REST API`.

## Difficultés rencontrées

### Complexité

Pour qu'une redirection se fasse via l'API Gateway, il faut mobiliser sept types de ressource de l'API Gateway. Nous le verrons cela à un impact sur la longueur de la description en HCL (le langage de Terraform).

L'utilisation de Terraform pour les services AWS est minoritaire par rapport à [CloudFormation (https://aws.amazon.com/fr/cloudformation/)](https://aws.amazon.com/fr/cloudformation/). La mise à disposition d'un site dans un bucket S3 est aussi plus souvent réalisée via les réglages de S3 ou via le service `CloudFront`. Ces deux paramètres rendent la recherche de solution  plus compliquée sur les sites de référence comme StackOverflow ou même les blogs.

### Types de contenu

Les navigateurs modernes comme Firefox, Chrome ou Edge ne reconnaissent les fichiers html et javascript que si le fichier a le bon type. Concrètement, cela signifie que l'entête HTTP `Content-Type` doit être :

- `text/html` pour les fichiers html ;
- `text/css` pour les fichiers css ;
- `application/javascript` pour les fichiers JavaScript.

Or si on n'y prend pas garde, lors du téléversement dans un bucket S3, les fichiers HTML, CSS et JavaScript ont un type `binary/octet-stream`. Ma solution insatisfaisante a été de décomposer le téléversement des différents fichiers par extension et de fixer le type de contenu adéquate.

### Côté JavaScript

Côté JavaScript, il faut configurer la base des liens HTML pour s'adapter à l'url de mise à disposition suffixé par `/dev/web` soit en utilisant :

- la balise [base](https://www.w3schools.com/TAGs/tag_base.asp) dans le langage HTML ;
- l'attribut `basename` dans la configuration du paquet JavaScript - package.json ;
- la configuration `baseHref` pour la configuration du NX ...

Côté React si on utilise un routeur comme `react-router`, il convient aussi de faire attention à utiliser des liens internes (en général `Link`).

## En Terraform s'il vous plait

On commence par décrire la politique et le rôle pour que l'API Gateway puisse accéder aux objets du bucket S3.

```HCL
# S3 Read-only Access Policy
resource "aws_iam_policy" "s3-ui-access" {
  name        = "s3-ui-access"
  description = "Policy for allowing get S3 Actions on ui bucket"

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : ["s3:GetObject"]
        "Resource" : "${module.mons3.arn}/*"
      }
    ]
  })
}

# API Gateway Role
resource "aws_iam_role" "front-api-gateyway" {
  name = "s3-api-gateyway-role"
  # Create Trust Policy for API Gateway
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
} 
  EOF
}

# Link S3 Access Policy and the API Gateway Role
resource "aws_iam_role_policy_attachment" "s3_policy_attach" {
  role       = aws_iam_role.front-api-gateyway.name
  policy_arn = aws_iam_policy.s3-ui-access.arn
}
```

On décrit ensuite l'API Gateway de type `REST API` donc les ressources `aws_api_gateway_`

```HCL
# API Gateway
resource "aws_api_gateway_rest_api" "front" {
  name = "front"
}

resource "aws_api_gateway_resource" "web" {
  rest_api_id = aws_api_gateway_rest_api.front.id
  parent_id   = aws_api_gateway_rest_api.front.root_resource_id
  path_part   = "web"
}

resource "aws_api_gateway_resource" "web-files" {
  rest_api_id = aws_api_gateway_rest_api.front.id
  parent_id   = aws_api_gateway_resource.web.id
  path_part   = "{item}"
}

```

On attaque la méthode HTTP `GET` sur la route `/web/{item}` où `{item}` désigne le nom du fichier à atteindre. Le lien est fait dans la ressource d'intégration.

```HCL
# GET /web/{item}

resource "aws_api_gateway_method" "web-files-get" {
  rest_api_id   = aws_api_gateway_rest_api.front.id
  resource_id   = aws_api_gateway_resource.web-files.id
  http_method   = "GET"
  authorization = "NONE"
  request_parameters = {
    "method.request.path.item" = true
  }
}

resource "aws_api_gateway_method_response" "web-files-get" {
  rest_api_id = aws_api_gateway_rest_api.front.id
  resource_id = aws_api_gateway_resource.web-files.id
  http_method = aws_api_gateway_method.web-files-get.http_method
  status_code = "200"
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true,
    "method.response.header.Content-Type"                = true
  }

  depends_on = [aws_api_gateway_method.web-files-get]
}
# C'est ici que tout se lie
resource "aws_api_gateway_integration" "web-files-get" {
  rest_api_id             = aws_api_gateway_rest_api.front.id
  resource_id             = aws_api_gateway_resource.web-files.id
  http_method             = aws_api_gateway_method.web-files-get.http_method
  integration_http_method = "GET"
  # On utilise un service AWS
  type                    = "AWS"
  # lien entre le chemin d'intégration et l'objet du S3
  uri                     = "arn:aws:apigateway:${var.region}:s3:path/${module.mons3.id}/{item}"
  # role à utiliser
  credentials             = aws_iam_role.front-api-gateyway.arn
  # lien entre le chemin d'intégration et le chemin de la méthode
  request_parameters      = { "integration.request.path.item" = "method.request.path.item" }
}

resource "aws_api_gateway_integration_response" "web-files-get" {
  rest_api_id = aws_api_gateway_rest_api.front.id
  resource_id = aws_api_gateway_resource.web-files.id
  http_method = aws_api_gateway_method.web-files-get.http_method
  status_code = aws_api_gateway_method_response.web-files-get.status_code

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'",
    "method.response.header.Content-Type"                = "integration.response.header.Content-Type"
  }
  depends_on = [aws_api_gateway_method_response.web-files-get, aws_api_gateway_integration.web-files-get]
}
```

On ajoute un traitement de la méthode  HTTP `OPTIONS` qui permet de gérer les [CORS](https://developer.mozilla.org/fr/docs/Web/HTTP/CORS).

```HCL
# OPTIONS /web/{item}

resource "aws_api_gateway_method" "web-files-options" {
  rest_api_id   = aws_api_gateway_rest_api.front.id
  resource_id   = aws_api_gateway_resource.web-files.id
  http_method   = "OPTIONS"
  authorization = "NONE"
  request_parameters = {
    "method.request.header.x-amz-meta-fileinfo" = false
  }
}

resource "aws_api_gateway_method_response" "web-files-options" {
  rest_api_id = aws_api_gateway_rest_api.front.id
  resource_id = aws_api_gateway_resource.web-files.id
  http_method = aws_api_gateway_method.web-files-options.http_method
  status_code = "200"

  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = true,
    "method.response.header.Access-Control-Allow-Methods" = true,
    "method.response.header.Access-Control-Allow-Origin"  = true,
    "method.response.header.Content-Type"                 = true
  }
  depends_on = [aws_api_gateway_method.web-files-options]
}

resource "aws_api_gateway_integration" "web-files-options" {
  rest_api_id = aws_api_gateway_rest_api.front.id
  resource_id = aws_api_gateway_resource.web-files.id
  http_method = aws_api_gateway_method.web-files-options.http_method
  type        = "MOCK"
  depends_on  = [aws_api_gateway_method.web-files-options]
  request_templates = {
    "application/json" = <<EOF
        {
        "statusCode" : 200
        }
    EOF
  }
}

resource "aws_api_gateway_integration_response" "web-files-options" {
  rest_api_id = aws_api_gateway_rest_api.front.id
  resource_id = aws_api_gateway_resource.web-files.id
  http_method = aws_api_gateway_method.web-files-options.http_method
  status_code = aws_api_gateway_method_response.web-files-options.status_code

  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,x-amz-meta-fileinfo'",
    "method.response.header.Access-Control-Allow-Methods" = "'GET,OPTIONS'",
    "method.response.header.Access-Control-Allow-Origin"  = "'*'",
    "method.response.header.Content-Type"                 = "integration.response.header.Content-Type"
  }
  depends_on = [aws_api_gateway_method_response.web-files-options, aws_api_gateway_integration.web-files-options]
}
```

On finit par le déploiement

```HCL
# Deployment

resource "aws_api_gateway_deployment" "front" {
  rest_api_id = aws_api_gateway_rest_api.front.id
  triggers = {
    # Configuration pour indiquer qu'il faut redéployer 
    # si l'identifiant d'une ressource ci-dessous change
    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.web.id,
      aws_api_gateway_resource.web-files.id,
      aws_api_gateway_method.web-files-get.id,
      aws_api_gateway_method_response.web-files-get.id,
      aws_api_gateway_integration.web-files-get.id,
      aws_api_gateway_integration_response.web-files-get.id,
      aws_api_gateway_method.web-files-options.id,
      aws_api_gateway_method_response.web-files-options.id,
      aws_api_gateway_integration.web-files-options.id,
      aws_api_gateway_integration_response.web-files-options.id,
    ]))
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "front" {
  deployment_id = aws_api_gateway_deployment.front.id
  rest_api_id   = aws_api_gateway_rest_api.front.id
  stage_name    = var.stage
}
```

## Conclusion

Le plus dur avant d'arriver à cette solution a été de comprendre les ressources Terraform pour AWS que je devais décrire. J'ai exploré la documentation AWS d'API Gateway et j'ai profité d'un cycle de retour court en déployant sur un compte AWS "bac à sable".

Il reste encore à peaufiner la description par exemple en créant un module, en utilisant une méthode moins naive pour la fixation du type de contenu en fonction de l'extension des fichiers...

Si vous avez des questions ou des retours d'expériences sur l'utilisation d'AWS avec Terraform, je suis intéressé. N'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

-------

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [Décrire une API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Décrire une Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).
