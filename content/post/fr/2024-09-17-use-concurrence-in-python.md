---
title: "Utiliser la concurrence avec Python"
date: "2024-09-17"
draft: false
description: "c'est plus simple que je ne le pensais"
tags: ["python","container","devops"]
author: "Bertrand Madet"
---

Récemment, j'ai résolu un problème de lenteur de traitement sur un script Python 🐍, grâce à la concurrence. Ma connaissance de la concurrence est plutôt faible. J'en suis presque resté à la gestion des processus (🇬🇧 *process*) et des fils d'exécution (🇬🇧 *thread* 🧵) avec des semaphores de mes cours de C. Je me gardais le plus possible d'utiliser la concurrence car je trouvais que cela ajoute de la complexité. Même en Go 🦫, je n'ai jamais utilisé les `goroutines` alors que c'est l'un des points marquants de ce langage. J'ai utilisé le parallélisme en Spark mais le framework gérait tout.

## Contexte

Pour un traitement en Python dans une lambda, j'avais besoin d'appeler *n* fois une API REST où *n* est le nombre de ligne d'un fichier. J'avais fait mes tests unitaires avec une connexion mockée et mes tests d'intégration avec *n* qui valait 100. 

Ce traitement appliqué au fichier utilisateur était parfois trop long pour une lambda. 🧑‍🏫 Pour rappel, une lambda a une **durée de vie maximale de 15 minutes - soit 900 secondes** ⏲️. Pourquoi ? Parce que le fichier contenait 10 000 lignes et que chaque requête HTTPS doit prendre entre 50 et 100 ms. Donc avec 10 000 appels à une API REST, vous allez avoir entre 500 et 1000 s d'attente des réponses.

## Alternatives

### Changer l'API REST appelée

En changeant le point de terminaison de l'API REST pour proposer plusieurs lignes en même temps ou en utilisant le protocole `HTTP/2` pour utiliser la même connexion TCP pour différentes requêtes, j'aurai pu accélérer ce traitement. Mais je n'avais la main sur l'API REST.

🤏 A noter que j'aurai aussi dû modifier le code de ma lambda pour que cela fonctionne avec cette API REST modifiée.

### Traiter autant de requêtes que possible et recommencer

J'aurai aussi pu changer mon traitement pour récupérer autant d'information de l'API REST que possible, indiquer les lignes traitées et recommencer le traitement avec les lignes non traitées. A titre d'information, dans le contexte de la lambda, il existe une méthode qui permet de récupérer le temps restant en milliseconde : `get_remaining_time_in_millis` (cf. [La documentation sur le contexte de lambda en Python](https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/python-context.html)).

Il est donc possible de garder une marge est de terminer proprement un traitement avant la coupure nette de l'expiration de la lambda 🧹. Je n'étais pas convaincu par cette solution, je trouve que cela augmente la complexité sans gain pour l'utilisateur.

### Passer la lambda en tâche ECS

Pour ne plus avoir de durée de vie maximale, j'aurai pu aussi effectuer le traitement dans une tâche ECS (**E**lastic **C**ontainer **S**ervice). `ECS` est un service de lancement de conteneur managé plus simple que Kubernetes. La documentation est [là](https://docs.aws.amazon.com/fr_fr/AmazonECS/latest/developerguide/Welcome.html) et une tâche est un conteneur qui s'exécute à la demande. 

Mes lambdas sont déjà conteneurisées mais la structure d'une tâche ECS est différente de celle d'une lambda. Une lambda est une juste une fonction Python qui prend en paramètres, un évènement et un contexte alors qu'une tâche ECS est juste un conteneur qui s'exécute. Les paramètres de l'exécution sont en général dans l'évènement pour une lambda alors qu'en général les paramètres d'exécution sont définis dans des variables d'environnement ou des secrets pour une tâche ECS.

J'ai pensé que cela amènerait trop de changements, dans le code, le Dockerfile et l'infrastructure 🏗️, pour être la meilleure solution.

## concurrent.futures

Après avoir éliminé (ou au moins rétrograder) les autres solutions, j'ai regardé la solution de lancer en parallèle les requêtes à l'API REST en Python. La bibliothèque standard [concurrent.futures](https://docs.python.org/fr/3/library/concurrent.futures.html) permet de gérer l'exécution de tâches en parallèle à travers de fils d'exécution ou des processus.

J'ai opté pour des fils d'exécution car il s'agit de gérer de l'attente et les processus sont plus lourds à gérer. En modifiant l'[exemple fourni pour l'utilisation de ThreadPoolExecutor](https://docs.python.org/fr/3/library/concurrent.futures.html#threadpoolexecutor-example) et en ajoutant 5 lignes, le traitement continuait à fonctionner dans mes tests unitaires.

```python
# On crée un réserve de fils d'exécution
with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    # Par chaque paramètre de recherche, on crée une association "future" vers paramètre de recherche
    future_to_search_parameter = { executor.submit(call_rest_api,search_parameter): search_parameter for search_parameter in search_parameters }
    # On parcours les "futures" terminées ou annulées
    for future in concurrent.futures.as_completed(future_to_search_parameter):
        # On récupère le paramètre de recherche associé au future
        search_parameter = future_to_search_parameter[future]
        # On récupère le résultat de la requête avec un temps maximal de 3 secondes
        result = future.result(3)
        # On continue ...
```

En lançant le test d'intégration, on diminuait le temps de traitement de 50% et pareil avec les tests sur l'environnement de préproduction 🎉.

## Conclusion

Voilà un exemple de bibliothèque standard de concurrence vraiment simple à utiliser. Les modifications ont été minimes car j'avais déjà une étape de collecte des résultats et ensuite une étape d'agrégation des résultats. Même si l'implémentation de la concurrence est facilitée en Python, j'ai réfléchi à d'autres solutions. On notera aussi que le temps d'exécution n'a pas été divisé par 5 mais par 2 ce qui illustre bien le fait que la concurrence ne divise pas automatiquement le temps de traitement par le nombre de tâches en parallèle.

Si vous souhaitez partager sur Python, contactez moi [sur X](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169).
