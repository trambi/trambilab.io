---
title: "Débuter avec SQLAlchemy"
date: "2023-08-30"
draft: false
description: "niveau d'abstraction bas"
tags: ["python","outils"]
author: "Bertrand Madet"
---

Je continue mon exploration des interactions avec les bases de données en Python. Je vous propose de découvrir la bibliothèque SQLAlchemy et son API dite bas niveau.

## SQLAlchemy

[SQLAlchemy (https://www.sqlalchemy.org/)](https://www.sqlalchemy.org/) est principalement connu en tant qu'ORM (:gb: **O**bject **R**elational **M**apping). Or SQLAlchemy n'est pas que cela et fournit deux niveaux d'abstraction :

- `ORM` qui permet de faire les liens entre des structures dans le code et une base de données relationnelles ;
- `core` plus bas niveau qui permet d'abstraire les requêtes SQL.

Le niveau `core` est intéressant pour commencer car il permet de commencer plus doucement. Cette API fournit une couche d'abstraction entre les requêtes SQL et le code. Les requêtes SQL ont des spécificités propre à chaque moteur de base de données par exemple pour les types de description des tables.

## Moteur

Le concept de moteur ou `engine` dans SQLAlchemy correspond à un gestionnaire de connexion pour un moteur de base de données. Dans mes tests unitaires, j'utilise SQLite pour garder les tests dans le même processus.

```python
import sqlalchemy

engine = sqlalchemy.create_engine("sqlite+pysqlite:///:memory:")
```

Le moteur permet ensuite d'obtenir une connexion avec la méthode `connect`.

## Création de table

La création de table reprend le principe de création de table en SQL. On va utiliser les classes `MetaData` ,`Table` et `Column` et les types d'abstraction de SQLAlchemy.

```python

metadata = sqlalchemy.MetaData()
# Equivalent à 
# CREATE TABLE mytable (
#    atext text,
#    anumber int
#    );
mytable = sqlalchemy.Table(
    "mytable",
    metadata,
    sqlalchemy.Column("atext", sqlalchemy.String(16)),
    sqlalchemy.Column("anumber", sqlalchemy.Integer())
)
engine = sqlalchemy.create_engine("sqlite+pysqlite:///:memory:")
metadata.create_all(engine)
```

## Insertion

Pour l'insertion, on va utiliser la fonction `insert` pour la création de la requête et la table créée plus haut. L'avantage avec les requêtes c'est qu'on peut les imprimer pour voir ce que cela va donner.
L'exécution de la requête se fait avec la méthode `execute` de la connexion.

```python
query = sqlalchemy.insert(mytable).values(atext="text1", anumber=3)
print(query)
# INSERT INTO mytable (atext, anumber) VALUES (:atext, :anumber)
with engine.connect() as conn:
    conn.execute(query)
    conn.commit()
```

:warning: Dans la configuration par défaut, il faut finir la transaction avec la méthode `commit` de la connexion. Sinon la transaction sera annulée à la fermeture de la connexion (à la sortie du bloc `with`).

## Sélection

Pour la sélection, on va utiliser la fonction `select` avec la table créée plus haut pour sélectionner toutes les colonnes. Pour sélectionner des colonnes spécifiques, on utilise la fonction `select` avec les colonnes à utiliser suivie de la méthode `select_from` et la table créée plus haut.

```python
query = sqlalchemy.select(mytable)
print(query)
# SELECT mytable.atext, mytable.anumber
# FROM mytable

# ou

query = sqlalchemy.select(sqlalchemy.column("atext"),sqlalchemy.column("anumber")).select_from(mytable)
print(query)
# SELECT atext, anumber
# FROM mytable
```

La méthode `execute` est un générateur, on peut faire de compréhension de liste pour exploiter les résultats.

```python
with engine.connect() as conn:
    result = conn.execute(query)
print([row for row in result])
# [('text1', 3)]
```

On peut ajouter des clauses avec la méthode `where` et les colonnes grâce à la fonction `column`.

```python
query = sqlalchemy.select(mytable).where(sqlalchemy.column("atext")=="text2")
with engine.connect() as conn:
    conn.execute(sqlalchemy.insert(mytable).values(atext="text2", anumber=4))
    result = [row for row in conn.execute(query)]
print(result)
# [('text2', 4)]
```

## Conclusion

L'effort pour passer de SQL à SQLAlchemy n'est pas nul mais le bénéfice principal est de s'abstraire du moteur de base de données. Cela permet de réduire le couplage avec une bibliothèque comme `psycopg2` et de faire des tests unitaires plus simplement en utilisant la base de données SQLite en mémoire. Il conviendrait de faire des tests d'intégration avec la base de données cible pour détecter les problèmes potentiels d'incompatibilité. Cette API me semble aussi rendre pratiquement impossible l'introduction de vulnérabilités aux injections SQL.

La partie ORM peut être intéressante mais l'effort risque d'être plus conséquent et donc à peser en fonction de la complexité intrinsèque du logiciel que l'on désire produire.

Si vous avez des retours ou des conseils sur les bibliothèques d'interaction avec les bases de données en Python, n'hésitez pas à les partager avec moi sur [X](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
