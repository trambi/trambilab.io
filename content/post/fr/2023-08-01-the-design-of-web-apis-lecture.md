---
title: "The Design of Web APIs - Fiche de lecture"
date: "2023-08-01"
draft: false
description: "Livre d'Arnaud Lauret sur la conception des API web"
tags: ["fiche de lecture", "REST API"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Ce livre s'adresse à ceux qui définissent ou modifient les contrats des API Web publiques ou privées. Cela va des ingénieurs logiciels aux responsables d'API Web en passant par les Data Engineers.

## Ce qu'il faut retenir

- Le terme API Web désigne une interface à destination des développeurs basée sur le protocole HTTP ;
- La conception d'une API Web nécessite de prendre en compte de ses utilisateurs, à savoir des développeurs ; 
- Le type d'API Web la plus commune est l'API REST, utilisant fortement les mécanismes du protocole HTTP ;
- La conception nécessite aussi de prendre en compte beaucoup d'autres dimensions comme la sécurité, les évolutions, les particularités du domaine, l'implémentation...

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Complet et détaillé, il y a même un canvas d'analyse des buts d'une API et des checklists | :-1: La question du cycle est abordée à la fin de l'ouvrage alors qu'elle mériterait d'être au début|
| :+1: Les concepts sont expliqués avec des analogies, des figures, des exemples |:?: Peut-être trop orienté API REST|

Le livre est très complet, l'auteur s'applique à expliquer chaque concept avec des analogies, des figures, des exemples et parfois de la documentation OpenAPI. Les illustrations s'appuient presque exclusivement sur les API REST. Les API de type GraphQL et gRPC sont évoquées mais peu détaillées.

L'ouvrage tient ses promesses et aborde tous les aspects liées à la définition d'une API Web (en tous cas ceux connus de moi). Le canvas et les checklists donnent envie de les utiliser sur des API Web pro ou perso.

Le livre est suffisament précis pour pourquoi les propriétés  `x-amazon-apigateway-integration` utilisés dans la  [création d'une API Gateway à partir d'un document OpenAPI]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md" >}}) ne posent pas de problémes au fichier OepnAPI : toutes propriétés commençant par `x-`sont exclues.

## Détails

- Titre : The Design of Web APIs
- Auteur : Arnaud Lauret
- Langue : :gb:
- ISBN : 978-1-61-729510-2
