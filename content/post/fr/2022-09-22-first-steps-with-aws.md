---
title: "Premier mois sur AWS"
date: "2022-09-22"
draft: false
description: "Immersion en apnée"
tags: ["cloud"]
author: "Bertrand Madet"
---

Depuis un mois, je passe mes journées à essayer d'appréhender des services [AWS (https://aws.amazon.com/fr/)](https://aws.amazon.com/fr/). J'ai essayé à la fois d'instancier et de configurer des ressources Cloud mais aussi de les utiliser via une application Web donc en JavaScript. Je partage des conseils et mon avis, suite à ce premier mois plongé dans les services AWS.

## Français ou anglais ?

Il y a énormément de documentation qui utilise les liens hypertextes et cela peut devenir tentaculaire. Le niveau de qualité des documentations varie en fonction du service proposé. Il existe beaucoup de documentation traduite en français :fr:.

Certaines documentations ne sont disponibles pas en français : en général celles sur les sujets les plus pointus. La documentation est initialement en anglais :gb: et vous êtes assurés d'avoir toutes les documentations en anglais.  Le fait d'avoir pris l'habitude des concepts et la configuration d'un service en français risque de vous gêner pour comprendre ces documentations. Par exemple, les `buckets S3` sont appelés `compartiment S3` en français.

> Utilisez la langue anglaise pour l'interface d'administration AWS

## Services transverses indispensables

Il existe des services AWS transverses qui sont en interaction avec énormément de services :

- [IAM (https://aws.amazon.com/fr/iam/)](https://aws.amazon.com/fr/iam/) qui permet de gérer les identités et l'accès aux services AWS ;
- [VPC (https://aws.amazon.com/fr/vpc)](https://aws.amazon.com/fr/vpc) pour les réseaux virtuels ;
- [Cloudwatch (https://aws.amazon.com/fr/cloudwatch/)](https://aws.amazon.com/fr/cloudwatch/) qui permet de comprendre le fonctionnement de vos services en observant des indicateurs sur vos services.

C'est très tentant de regarder directement les services qui nous intéresse. Je le sais, je l'ai fait :smile: . Les concepts de ces services transverses sont tellement représentés dans les services qu'il est impératif de prendre le temps d'assimiler les concepts de ces services.

> Prenez le temps de comprendre sur les concepts d'IAM, VPC et Cloudwatch

## Configuration des services

Les services AWS que j'ai commencé à utiliser : Aurora DB , Api Gateway, Cognito, Lamdba sont configurables. Certains services permettent de gérer des concepts imbriqués. 

Par exemple Cognito permet de gérer de l'authentification **et** l'autorisation des utilisateurs. Le service permet de composer la solution de gestion des utilisateurs avec une solution d'authentification dédiée ou d'autres solutions d'authentification existantes comme Google ou Facebook. Cela amène l'utilisation des pleins de concepts différents comme :

- `User pool` ou bassin d'utilisateur qui contiennent :
  - des groupes qui peuvent associées à des rôles IAM;
  - des utilisateurs;
  - des applications clients ;
  - des serveurs
- `Identity pool` ou bassin d'identité qui utilise une solution d'authentification (un user pool ou une autre solution d'authentification) ;

La combinatoire des possibilités explose, permettant à la fois de couvrir énormément de cas d'usage et de perdre beaucoup de temps pour mettre au point la solution désirée.

## Kit de développement

Le principe du [SDK version 3 (https://docs.aws.amazon.com/sdk-for-javascript/v3/developer-guide/welcome.html)](https://docs.aws.amazon.com/sdk-for-javascript/v3/developer-guide/welcome.html) est de permettre d'envoyer des commandes aux services AWS. 

Par exemple pour créer un bucket S3.

```javascript
import { S3Client, CreateBucketCommand } from "@aws-sdk/client-s3"; // ES Modules import
const config = {region: 'eu-west-3'};
const client = new S3Client(config);
const command = new CreateBucketCommand({Bucket:'my-example-and-unique-bucket-name'});
const response = await client.send(command);
```

On voit qu'il y a peu d'abstraction vis-à-vis des commandes AWS. En plus de devoir connaître les services que l'on souhaite utiliser, il faut aussi connaître les commandes correspondantes et la documentation du kit de développement pour JavaScript ne nous y aide pas.

## Conclusion

Ma première impression est que les services AWS sont comme la bibliothèque standard Java dans les années 2000 : les services sont très nombreux. Très souvent une fonctionnalité va nécessiter une composition de plusieurs concepts d'un ou plusieurs services AWS, ce qui signifie qu'il faut assimiler les concepts d'utilisation de plusieurs services pour implémenter une fonctionnalité.

Je pense qu'il faut du temps pour intégrer les concepts de chaque nouveau service. Je vous laisse, je dois aller relire les concepts d'IAM.

Si vous voulez partager sur l'utilisation d'AWS ou discuter de bibliothèques facilitant l'usage de services Cloud, contactez moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
