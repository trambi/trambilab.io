---
title: "Coding dojo : Factorisation en nombres premiers - partie 1"
date: "2023-02-23"
draft: false
description: "en TDD avec exemple"
tags: ["dojo","Software Craftmanship","test","tdd"]
author: "Bertrand Madet"
---

La semaine dernière, nous avons fait un Coding dojo ayant pour sujet la factorisation en nombres premier. Je vous propose de parcourir le chemin en [TDD]({{< ref "/tags/tdd">}}), c'est à dire en commençant par écrire un test qui échoue puis en écrivant le minimum de code pour passer le test précédent, de refactorer puis de recommencer à écrire un test.

## Factorisation en nombres premiers

L'idée est de fournir la liste par ordre croissant des facteurs premiers d'un nombre entier. Un nombre premier étant un nombre entier divisible uniquement par un et par lui-même et pour éviter tout débat un (1) n'est pas un nombre premier. Par convention, pour tout autre entrée qu'un nombre entier supérieur à un (1), on retournera un tableau vide.

Ainsi si on donne douze (12), on obtiendra le tableau deux, deux, trois `[2,2,3]`.

## Itérons

### Premier test : Retour pour 1

Je vais commencer par un premier test pour m'assurer que la fonction renvoie un tableau vide pour l'entrée un (1).

```typescript
import {primeNumbersFactorization} from './';

describe('primeNumbersFactorization',()=>{
    it('returns empty array given 1',()=>{
        expect(primeNumbersFactorization(1)).toEqual([]);
    });
});
```

L'implémentation la plus simple pour passer le test est de retourner un tableau vide tout le temps :

```typescript
const primeNumbersFactorization = (input: number)=>[];
```

### Deuxième test : Retour pour un nombre premier

Le cas suivant est le cas des nombres premiers en entrée, cela retourne un tableau avec uniquement l'entrée.

```typescript
import {primeNumbersFactorization} from './';

describe('primeNumbersFactorization',()=>{
    it('returns empty array given 1',()=>{
        expect(primeNumbersFactorization(1)).toEqual([]);
    });
    it('returns the array with a number given prime number',()=>{
        expect(primeNumbersFactorization(2)).toEqual([2]);
    });
});
```

Une implémentation simple pour passer ce test et le précédent est de retourner un tableau vide si le nombre en entrée est inférieur à 2 ou le nombre dans un tableau.

```typescript
const primeNumbersFactorization = (input: number)=>{
    if(input > 1){
        return [input];
    }
    return [];
}
```

### Troisième test : Retour pour un nombre produit de deux nombres premiers

Le cas suivant est le cas des nombres qui sont le produit de deux nombres premiers, cela retourne un tableau avec les nombres premiers.

```typescript
import {primeNumbersFactorization} from './';

describe('primeNumbersFactorization', () => {
    it('returns empty array given 1', () => {
        expect(primeNumbersFactorization(1)).toEqual([]);
    });
    it('returns the array with a number given prime number', () => {
        expect(primeNumbersFactorization(2)).toEqual([2]);
    });
    it('returns the array with a number given factor of two prime numbers',() => {
        expect(primeNumbersFactorization(6)).toEqual([2, 3]);
    });
});
```

L'implémentation pour répondre à ce test nous oblige à chercher des diviseurs non triviaux, on s'en sort avec une boucle et l'opérateur modulo `%` (reste de la division entière).

```typescript
const primeNumbersFactorization = (input: number)=>{
    if(input > 1){
        let divider = 2;
        const result = [];
        while(input !=1){
            for(divider;divider<=input;divider++){
                if(input % divider === 0){
                    result.push(divider);
                    input = input/divider;
                }
            }
        }
        return result;
    }
    return [];
}
```

### Quatrième test : Les autres cas

```typescript
import {primeNumbersFactorization} from './';

describe('primeNumbersFactorization', () => {
    it('returns empty array given 1', () => {
        expect(primeNumbersFactorization(1)).toEqual([]);
    });
    it('returns the array with a number given prime number', () => {
        expect(primeNumbersFactorization(2)).toEqual([2]);
    });
    it('returns the array with a number given factor of two prime numbers', () => {
        expect(primeNumbersFactorization(6)).toEqual([2, 3]);
    });
    it('returns the array of prime numbers for other cases', () => {
        expect(primeNumbersFactorization(12)).toEqual([2, 2, 3]);
    });
});
```

On ajoute d'une interruption de boucle pour permettre d'avoir deux fois le même diviseur et ainsi passer ce test.

```typescript
const primeNumbersFactorization = (input: number)=>{
    if(input > 1){
        let divider = 2;
        const result = [];
        while(input !=1){
            for(divider;divider<=input;divider++){
                if(input % divider === 0){
                    result.push(divider);
                    input = input/divider;
                    break;
                }
            }
        }
        return result;
    }
    return [];
}
```

### Niveau de confiance

Bon, mon implémentation a l'air de fonctionner mais j'ai testé avec quatre nombres :astonished: ! Une manière pourrait être d'ajouter des boucles avec `each`. Cela permet de multiplier les cas de tests sans alourdir le fichier de tests.

```typescript
import {primeNumbersFactorization} from './';

describe('primeNumbersFactorization', () => {
    it.each([
        [-1],
        [0],
        [1],
    ])('returns empty array given unvalid integer %s', (input: number) => {
        expect(primeNumbersFactorization(input)).toEqual([]);
    });
    it.each([
        [2],
        [3],
        [7],
        [17]
    ])('returns the array with a number given prime number %s', (input: number) => {
        expect(primeNumbersFactorization(input)).toEqual([input]);
    });
    it.each([
        [6, [2, 3]],
        [10, [2, 5]],
        [51, [3, 17]]
    ])('returns the array with a number given factor of two prime numbers', (input: number, expected: number[]) => {
        expect(primeNumbersFactorization(input)).toEqual(expected);
    });
    it.each([
        [12, [2, 2, 3]],
        [45, [3, 3, 5]]
    ]('returns the array of prime numbers for other cases', (input: number, expected: number[]) => {
        expect(primeNumbersFactorization(input)).toEqual(expected);
    });
});
```

## Conclusion

Voilà un exemple d'implémentation du coding dojo : Factorisation en nombres premiers, cette implémentation n'est pas optimale mais elle permet d'illustrer la méthode. Pour rester synthétique, j'ai fusionné la phase d'écriture du code et le refactoring. Vous aurez aussi peut-être remarqué que nous n'utilisons pas directement la notion de nombre premier dans le code.

On a aussi pu revoir la fonction `each` qui permet de paramétrer les tests et de les rendre ainsi plus expressifs. Il y a une autre manière de multiplier les cas de tests, basée sur les propriétés. Ce sera dans le [prochain article]({{< ref "/post/fr/2023-02-28-coding-dojo-prime-number-factorization-part2.md">}}).

Si vous souhaitez partager sur des coding dojo ou le TDD, contactez moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
