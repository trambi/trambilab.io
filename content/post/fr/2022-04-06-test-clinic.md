---
title: "Clinique du test"
date: "2022-04-06"
draft: false
description: "ou améliorer ensemble les tests d'un logiciel"
tags: ["test","Software Craftmanship"]
author: "Bertrand Madet"
---

Depuis 4 mois, je teste un nouveau type de session, la clinique du test ou `test clinic` :sunglasses:.

Le principe est simple, il s'agit de coder des tests logiciels en une heure et en collaboration (ou `mob programming` :gb:). Une personne amène un bout de code qu'elle souhaite tester et tous les autres participants vont l'aider à développer des tests. Le tout en ne modifiant pas le composant (ou alors un minimum). Le but n'est pas de faire une revue de code, mais d'aider à tester.

## Lancement

Les tests n'étaient pas assez développés dans ma structure. Plutôt que de faire les gros yeux à de trop nombreuses pauses café, je me suis inspiré de ma [lecture de Software Engineering at Google]({{< ref "/post/fr/2021-05-02-software-engineering-at-google-lecture.md" >}}). Et j'ai lancé la clinique du test.

J'ai commencé avec une session dédiée à React (c'était juste après [le billet sur les TDD en React]({{< ref "/post/fr/2021-12-01-tdd-react-component.md" >}}). J'ai donc invité tous les gens que je savais coder en React. A la première session, j'avais préparé un composant de la base de code de notre projet. L'accueil a été bon mais animer et présenter un de ces propres composants amenait un espèce de one-man show qui n'était pas jouable dans la durée. Nous avons donc convenu d'organiser un deuxième session avec un composant provenant d'un autre projet.

La deuxième séance a eu lieu sur un composant logiciel qui n'était pas prévu pour être testé. Cela a été un exercice périlleux avec des hauts et des bas. Au final, nous avons pu tester un ou deux comportements du composant.

## Déroulé

### Avant

On peut proposer une réunion d'une heure avec visio-conférence en proposant d'aider sur le développement de test automatique d'une partie logicielle. La deuxième étape est de sélectionner la partie logicielle à tester.

Une fois le composant sélectionné, je vous conseille de regarder s'il contient des détails d'implémentation qui le rendront difficiles à tester sans connaissance de son implémentation. Par exemple, l'utilisation de certains hooks en React comme `useContext` ou `useTranslation` (de `react-i18n`). Cela permet de se concentrer sur le comportement du composant pendant la séance afin de tester le plus possible son comportement en non son implémentation.

### Pendant

Une session d’une heure peut consister en :

- Dix (10) minutes d’introduction : présentation de la partie logicielle et des difficultés pour le tester ;
- Quarante-cinq (45) minutes de développement collaboratif des tests;
- Cinq (5) de conclusion : sous la forme d’un tour de table et d’une évaluation de l’intérêt de la session.

:rotating_light: *L'animateur doit veiller d'éviter tout jugement sur le code à tester.*

Cet aspect est capital car :

- la session n'est pas une revue de code ;
- la personne qui a présenté son code a besoin d'aide, a fait la démarche pour demander de l'aide, elle mérite juste du respect.

## Enseignements

Les sessions auxquelles j'ai pu participer m'ont confirmé :

- que le test dans un domaine est une compétence distincte du développement dans ce même domaine - dit autrement on peut très bien coder et ne pas savoir écrire des tests pour son propre code ;
- qu'un code qui n'est pas prévu pour être testé est compliqué à tester ;
- qu'en une heure, il est possible d'écrire au moins un test.

J'ai aussi pu constater des choses surprenantes :

- le développement en groupe permet de trouver plein de solutions ;
- il y a parfois une confusion entre tests automatiques et tests unitaires ;
- C'est aussi très complémentaire des coding dojos :
  - là où le coding dojo se place dans un contexte simple pour s’entraîner, la test clinic se place dans un contexte existant pour appliquer ;
  - le coding dojo se concentre souvent sur le code, la test clinic l'occulte car on teste le comportement en non les détails d'implémentation ;
  - animer une session de test clinic est beaucoup fatiguant qu'animer un coding dojo - surtout quand le code n'est vraiment pas prévu pour être testé :exploding_head:.

## Conclusion

Actuellement, nous organisons deux sessions par mois, une dédiée à React et l'autre dédiée à Python. Deux collègues ont animé des sessions.

Il faudra sûrement adapté le rythme dans la durée ou passer en mode à la demande. J'ai ressenti une bonne dynamique pour les tests : J'ai vu émerger des discussions sur les classiques tests unitaires, tests d'intégration, tests d'acceptation, tests end-to-end mais aussi sur le test basé sur les propriétés (property based testing), le test en boîte noire ou en boîte blanche...

J'espère que nous arriverons à entretenir cette dynamique au travers de ces sessions et les coding dojos ou d'autres initiatives.

Si vous avez des expériences ou des questions sur les tests logiciels, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
