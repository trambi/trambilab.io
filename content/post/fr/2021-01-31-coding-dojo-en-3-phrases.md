---
title: "Coding dojo en 3 phrases"
date: "2021-01-31"
draft: false
description: "et cinq astuces"
tags: ["dojo","Software Craftmanship","3 phrases"]
author: "Bertrand Madet"
---


Dans le billet précédent, je vous livrais ma [fiche de lecture Coding Dojo Handbook]({{< ref "/post/fr/2021-01-24-coding-dojo-handbook-lecture.md" >}}). Cela fait plus de deux ans et demi que j'ai lu ce livre et que j'ai initié le mouvement dans mon entité. Je résume mon expérience en 3 phrases :

1. Lancez vous !
1. Établissez des règles simples.
1. Partagez l’organisation et l’animation.

Explications suivies d'astuces ... :teacher:

## Lancez vous !

Le matériel existe pour vous lancer même aujourd'hui à distance. 

- Pour la vidéo-conférence, vous avez sûrement une solution dédiée à votre entreprise, sinon jetez un oeil du côté de [Jitsi (https://meet.jit.si)](https://meet.jit.si).
- Pour le sujet de la séance, le livre Coding Dojo Handbook est une bonne base (score de tennis, du bowling ou numération romaine), si vous n'avez pas le livre, le [dépôt GitHub (https://github.com/emilybache?tab=repositories)](https://github.com/emilybache?tab=repositories) de l'auteure est plein de katas (GitHub en général est plein de katas) ou le [site CodingDojo (https://codingdojo.org/kata/](https://codingdojo.org/kata/).

Se lancer demande un peu de temps mais cela est largement contrebalancé par ce que vous en retirez. Un peu comme le débogage avec l'ours en peluche, le fait d'expliquer vous permet d'approfondir vos connaissances. Travailler ou regarder travailler d'autre personnes va vous permettre d'apprendre d'autres pratiques, d'autres outils.

Commencez avec une version `alpha` en sélectionnant une demi-douzaine de personnes bienveillantes.

## Établissez des règles simples

Afin de se concentrer sur la pratique, il est important d'avoir des règles pour éviter que l'atelier ne parte en discussion sur le format. Les règles simples sont faciles à retenir. Favorisez un créneau fixe et une fréquence régulière. Écoutez vos collègues sur les horaires ou les règles pour les faire évoluer entre les sessions.

Un exemple de règlement :

 0. Si vous venez, vous acceptez les règles suivantes ;
 1. Si vous avez un problème avec une règle, vous l’exprimez avant ou après la séance ;
 2. Si vous venez, vous venez à toute la séance ;
 3. Si vous venez, vous pratiquez  ;
 4. Une fois les contraintes de la séance validée, vous les respectez ;
 5. Si quelqu’un fait une erreur, vous lui indiquez respectueusement.

## Partagez l’organisation et l’animation

L'organisation des sessions dans le temps est plus exigeante que le lancement. Et il y aura sûrement des moments où une séance se passera moins bien que prévue ou que vous vous retrouvez seul à vos séances.

Afin de garder de la motivation et de l'énergie, il est important d’impliquer d’autres personnes dans l’animation et l’organisation. Parlez-en à des participants réguliers cela leur permettra d’exercer des compétences non techniques, importante dans le métier de développeur et vous, de prendre la place d’un participant.

> Seul on va vite, ensemble on va loin !

## Trucs et astuces

### Plans de secours

Malgré les explications sur la session, il arrive qu'un participant n'est pas les bons outils. Afin de ne pas transformer une session de pratique en "install party", prévoyez un plan de secours.

Si vous faites une session en Python, ayez déjà une ou deux instances de Jupiter lab avec pytest installé (pour le TDD) au cas où les participants n’aient pas les bons outils (IDE et modules) installés.

### Exemple de programme sur 1h30

Une session d’une heure et demie consistait en :

 - Un quart d’heure d’introduction : présentation des principes (TDD, pair programming, ...), constitution des équipes et explication du challenge ;
 - Une heure de codage ;
 - Un quart d’heure de conclusion : sous la forme d’un tour de table et d’une évaluation de l’intérêt de la session.

### Constitution des groupes

Après avoir expliqué les exercices, demandez aux participants de se répartir le long d’une ligne en fonction de leur aisance avec la séance. Le plus proche de vous se sentant le plus à l'aise avec le sujet et les contraintes et le plus loin se sentant le moins à l'aise.

Associez le plus à l'aise avec le moins à l'aise, le deuxième avec l’avant-dernier, et ainsi de suite...

### Préparation

Essayez de faire les katas avant, vous allez certes perdre en fraîcheur. En effet, certains katas semblent simples en lisant l’énoncé mais sont difficiles à coder (par exemple ceux sur le poker ou le placement de théâtre).

### Outils

Il y a plein de bacs à sable (playground pour la recherche sur Duckduckgo) disponibles sur Internet. On peut utiliser le site de challenge de code [codewars (https://www.codewars.com/)](https://www.codewars.com/) très pratique avec les fenêtres de code et de tests. Pour les langages supportés (R, Python, Julia, Scala), on peut aussi utiliser [JupyterLab (http://jupyterlab.io/)](http://jupyterlab.io/).

## Conclusion

J'espère que ces conseils vous aideront à vous lancer ou à améliorer vos sessions. Si vous avez des retours ou d'autres conseils sur les coding-dojos, n'hésitez pas à les partager sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).