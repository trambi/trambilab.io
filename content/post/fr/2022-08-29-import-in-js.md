---
title: "Import en JavaScript et en TypeScript"
date: "2022-08-29"
draft: false
description: "Avec webpack, babel"
tags: ["front-end","javascript","typescript","outils"]
author: "Bertrand Madet"
---

Cette semaine, j'ai été confronté à une grosse difficulté qui m'a permis d'explorer le fonctionnement de la fonction `import` en JavaScript. J'ai souhaité intégrer des fichiers markdown dans les sources d'une application [React.js (https://fr.reactjs.org/)](https://fr.reactjs.org/). Le fonctionnement de `import` pour les fichiers textuels diffère en fonction l'utilisation en développement (et en production) ou en test. En développement et en production, on utilise [Webpack (https://webpack.js.org/)](https://webpack.js.org/) et pour les tests, on utilise [Babel (https://babeljs.io/)](https://babeljs.io/).

> :warning: J'ai constaté cette différente de comportement pour les fichiers textuels (`.txt`) ou Markdown (`.md`) qui ne sont pas des fichiers classiques web. Pour les fichiers classiques comme les fichiers JavaScript, json ou css le comportement est quasi-identique.

J'ai analysé le comportement de `import` avec Webpack v5, Babel v7 et bonus Node.js v14.

## Webpack

Webpack est un `bundler` :gb:. Il génère à partir d'un ensemble de fichiers sources (code, module JavaScript, données, feuilles de style, images...) un ensemble de fichiers utilisable sur un serveur web (appelé le `bundle`). En plus d'optimiser les chargements des fichiers, l'outil est d'autant plus utile que les fichiers sources ont des liens entre eux et le résultat tient en compte des ces liens. 

Webpack refusera l'exécution du code JavaScript suivant lors de la génération du bundle car il ne sait pas quoi intégrer.

```typescript
const myVariable = 'ah';
import (myVariable);
```

Webpack acceptera en revanche le code JavaScript suivant.

```typescript
const myVariable = 'ah';
import (`./directory/${myVariable}.txt`);
```

A la phase de génération, il intégrera tous les fichiers qui sont dans le répertoire "directory" (dans le répertoire du fichier source) et qui finissent par ".txt". Il créera un fichier JavaScript par fichier texte intégré. A l'exécution par le navigateur, les fichiers JavaScript fourniront un objet dont le champ `default` est le chemin où télécharger le fichier texte correspondant. Pour le récupérer, on peut utiliser `fetch` (exemple de code ci-dessous). En développement, webpack va créer un mini serveur web et en production les fichiers seront déposés sur un serveur web donc cela va fonctionner.

```typescript
    const getContent = async (basename: string) => {
        try {
            const path = await import(`./directory/${basename}.md`);
            const response = await fetch(path);
            return response.text();
        } catch (e) {
            console.log("try to import unknown content from ", basename);
            return "unavailable content";
        }
    };
```

|Outil|Import d'une variable|Import d'un fichier texte|Import d'un fichier json|
|-----|---------------------|-------------------------|------------------------|
|Webpack|:no_entry: Lance une exception|:heavy_check_mark: Retourne un objet dont le champ `default` contient le chemin vers le fichier|:heavy_check_mark: Retourne un objet dont le champ `default` contient l'interprétation du fichier json|

## Babel

Babel est un compilateur, il permet de passer de JavaScript ultra moderne (ou du typescript) à un JavaScript plus ancien (pour être utilisé sur des navigateurs qui n'implémentent pas forcément les dernières versions de JavaScript). Pour l'environnement de test, c'est [jest (https://jestjs.io/)](https://jestjs.io/), le moteur de test, qui appelle Babel sur les fichiers avant de les lire pour exécuter les tests.

Babel acceptera l'exécution du code JavaScript suivant et `import` retournera une erreur si le fichier n'existe pas, ou un objet dont le champ `default` avec le nom de base du fichier (nom sans le chemin - donc `ah`).

```typescript
const myVariable = 'ah';
import (myVariable);
```

Pour le code suivant (et si le fichier `ah.txt` existe dans le répertoire local `directory`), `import` retournera `ah.txt`.
```typescript
const myVariable = 'ah';
import (`./directory/${myVariable}.txt`);
```

Comme Babel et jest ne sont pas des serveurs web, le code suivant ne va pas fonctionner.

```typescript
    const getContent = async (basename: string) => {
        try {
            const path = await import(`./directory/${basename}.md`);
            const response = await fetch(path);
            return response.text();
        } catch (e) {
            console.log("try to import unknown content from ", basename);
            return "unavailable content";
        }
    };
```

|Outil|Import d'une variable|Import d'un fichier texte|Import d'un fichier json|
|-----|---------------------|-------------------------|------------------------|
|Babel|:heavy_check_mark: Ok|:warning: Retourne un objet dont le champ `default` contient le nom de base du fichier|:heavy_check_mark: Retourne un objet dont le champ `default` contient l'interprétation du fichier json|

## Bonus : Node.js

[Node.js (https://nodejs.org/fr/)](https://nodejs.org/fr/) est un moteur d'exécution JavaScript utilisé en exécution back-end et en développement.

Il accepte très bien les imports dynamiques. Mais lance une exception si l'extension du fichier importé ne correspond pas à `js`.

|Outil|Import d'une variable|Import d'un fichier texte|Import d'un fichier json|
|-----|---------------------|-------------------------|-------------------------|
|Node.js|:heavy_check_mark: Ok|:no_entry: Lance une exception|:no_entry: Lance une exception|

## Conclusion

|Outil|Import d'une variable|Import d'un fichier texte|Import d'un fichier json|
|-----|---------------------|-------------------------|------------------------|
|Webpack|:no_entry: Lance une exception|:heavy_check_mark: Retourne un objet dont le champ `default` contient le chemin vers le fichier|:heavy_check_mark: Retourne un objet dont le champ `default` contient l'interprétation du fichier json|
|Babel|:heavy_check_mark: Ok|:warning: Retourne un objet dont le champ `default` contient le nom de base du fichier|:heavy_check_mark: Retourne un objet dont le champ `default` contient l'interprétation du fichier json`|
|Node.js|:heavy_check_mark: Ok|:no_entry: Lance une exception|:no_entry: Lance une exception|

Le comportement des différents outils est donc très différent et c'est normal car leur but est également très différent. Comme j'avais pu le découvrir dans mes billets : [Ce que j'ai appris en mettant à jour Gatsby]({{< ref "/post/fr/2021-06-22-appris-en-upgradant-gatsby.md" >}}) et [J'ai créé une page web à l'ancienne]({{< ref "/post/fr/2021-06-29-projet-vanilla-js.md" >}}), l'outillage JavaScript facilite le développement au prix d'une plus grande complexité quand nous sortons du cadre des outils. 

Babel, Jest et Webpack sont intégrés dans l'outillage recommandé pour commencer un application React. En tant qu'artisan développeur, il est important de comprendre le but et les modalités de fonctionnement de nos outils.

L'intégration de fichiers markdown dans les sources d'une application ne semble pas être une bonne idée : 

- cela amène à du code complexe et des tests qui dépendent l'implémentation ;
- cela alourdira le premier chargement de l'application.

Si vous n'avez pas le choix, vous pouvez intégration des fichiers json qui contiendront un champ qui contient le markdown protégé. Cela implique de protéger le markdown mais cela fonctionnera avec Webpack et Babel. Je pense qu'il est plus efficace de charger le contenu des markdowns à partir d'une API REST.



Si vous voulez discuter des outils JavaScript ou d'autres outils de développement, contactez moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
