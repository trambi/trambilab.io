---
title: "Trois semaines sans coder"
date: "2021-08-31"
draft: false
description: "les bénéfices que j'en retire"
tags: ["personal"]
author: "Bertrand Madet"
---

J'ai pris l'habitude d'arrêter de coder pendant mes vacances d'été. Je publie donc un court billet sur cette pratique et les avantages que j'en retire.

## Coupure

Ma coupure consiste uniquement à ne pas coder, aussi bien sur mes projets professionnels (puisque ce sont des vacances) que personnels. J'en profite pour lire des livres de développement logiciel et d'écrire des billets de blog mais surtout pour me reposer et profiter de ma famille.

Je peux même commencer à lire des livres énormes comme Code Complete (800 pages pour la version physique et 2000 pages pour la version électronique, la fiche de lecture n'arrivera pas tout de suite).

## Avantages

### Recul

Le fait d'arrêter de coder me  permet de me détacher de mon code. J'imagine que ne pas être impliqué dans la construction amène une perception plus détachée des codes que je construis (ou co-construis). Ce détachement me permet d'appréhender les conseils récoltés dans les livres de développement différemment et de m'intéresser aux conseils plus "méta" moins concrets.

### Fraîcheur

Le fait d'arrêter permet de coder permet tout simplement une coupure et de libérer du temps du temps pour faire autre chose. Cette coupure permet au moment de la reprise d'aborder mes projets professionnels ou personnels avec un nouveau point de vue nourri des expériences que j'ai pu vivre pendant cette pause - lectures, discussions, bricolage, repos...

### Envie

Je suis assez tourné vers l'action (ou la réalisation pour l'ingénierie logicielle) et une période de trois semaines ou d'un mois permet de rentrer de vacances avec une envie de coder décuplée.

## Conclusion

Si vous codez pendant vos vacances ou que justement vous arrêtez de coder ou si vous avez d'autres astuces  personnelles de gestion de la motivation, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).