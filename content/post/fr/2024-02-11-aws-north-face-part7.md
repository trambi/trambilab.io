---
title: "Ce que je trouve difficile avec les lambdas"
date: "2024-02-11"
draft: false
description: "avec ou sans VPC"
tags: ["python","cloud","lambda"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).

-----

Après plus d'un an à travailler sur les AWS Lambdas, je vous partage mes difficultés. Contrairement à ce que je pensais il y a un an après [six mois sur AWS]({{< ref "/post/fr/2023-02-03-six-months-with-aws.md">}}), les lambdas ne sont pas simples : les lambdas semblent simples 🤏.

## IAM et le modèle de sécurité AWS

Le modèle de permission est basé sur les rôles, des droits très fins sur les services AWS et est géré par [IAM (https://aws.amazon.com/fr/iam/)](https://aws.amazon.com/fr/iam/).

Le principe est simple :
> Ce qui n'est pas explicitement autorisé est interdit.

Je trouve difficile d'appliquer le principe de moindre privilège car :

- la granularité des permissions est très fine sur les permissions et sur les ressources sur lesquelles elles s'appliquent ;
- la documentation sur les permissions des services est hétérogène ;
- les rôles et leurs permissions associées sont décrits avec l'infrastructure, ce qui fait qu'un changement de fonctionnalité modifie le code de la lambda et potentiellement la description de l'infrastructure.

Quand on sait que l'on peut aussi utiliser IAM pour gérer les connexions aux bases de données [RDS](https://aws.amazon.com/rds/) - des bases de données opérés et gérées par AWS. Cela signifie que la gestion des permissions peut s'étendre à la partie en charge de la base de données (un script SQL ou une autre lambda).

Ces éléments donnent l'impression de faire de la dentelle potentiellement à plusieurs endroits, cela nécessite donc beaucoup de discipline et d'organisation.

## Déboguer dans un VPC est difficile

Admettons que votre lambda n'arrive pas à lister les buckets S3. En général, la raison de l'erreur est indiquée dans l'exception levée au moment du problème. Il est donc critique de disposer des journaux d'exécution 📰. Sur AWS, le plus simple est d'utiliser le service [Amazon CloudWatch](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/WhatIsCloudWatch.html). Jusqu'à là rien de choquant, l'utilisation des journaux est un des piliers des applications web (cf. [les 12 facteurs](https://12factor.net/fr/)). Heureusement c'est plutôt simple côté Python puisqu'on peut utiliser la fonction `print` ou le module [logging](https://docs.python.org/3/library/logging.html).

Maintenant, admettons que votre lambda échoue pendant que vous vous connectez au service S3, vous aurez une exception d'échec de connexion. Si la lambda est hors de votre VPC, cela peut être dû à votre code ou à un problème du service S3 contacté. En fonction du service S3 concerné, cela peut vous orienter vers votre code.

Si la lambda est dans votre VPC, cela peut être dû :

- au paramétrage dans votre code d'appel ;
- au service S3 appelé :
- à résolution de nom pour le service S3 ;
- à la configuration sur un point de terminaison VPC S3 ;
- à la configuration des tables de routage ;
- à tout autre problème de configuration de votre VPC.

Cela ajoute beaucoup de piste à explorer pour comprendre l'origine du problème.

Si la lambda est rattachée à plusieurs sous-réseaux (pour la disponibilité), le problème peut apparaître uniquement sur un sous-réseau mal configuré :exploding_head:. Bref cela complique la recherche des causes d'erreur et vous oblige à vous occuper d'aspects très "réseau".

## Fonction à durée de vie limitée

Dans le dernier exemple de la partie précédente, j'ai évoqué l'expiration de lambda. **C'est l'une des spécificités des lambdas, elles ont une durée maximale : 15 minutes**. C'est tant mieux car le prix est calculé avec la longueur d'exécution 💸. Par défaut la durée maximale d'exécution est de 3 secondes ⏲️. Les fonctions d'appel aux services AWS comme de [boto3](https://aws.amazon.com/sdk-for-python/) ont elles aussi des temps de réponses maximaux et des stratégies de reprise. Par exemple pour la connexion au service S3, le temps de réponse maximal par défaut est de 1 minute. Sans configuration spécifique, il est très possible d'avoir souvent le problème d'expiration de la lambda à cause d'une mauvaise connexion au service S3 et aucune autre raison indiquée dans les journaux.

Il peut être tentant de mettre des temps de réponses maximaux de l'ordre de la seconde, mais j'ai déjà expérimenté des latences à la connexion d'un service de l'ordre de 5 secondes (cf. Se connecter avec un rôle IAM sur RDS PostgreSQL ({{< ref "/post/fr/2023-04-26-connect-with-iam-role-pg-node.md">}})). Il faut donc ajuster la durée maximale de la lambda et les temps de réponses maximaux pour les différents appels aux services AWS.

## Conclusion

J'espère avoir démontré que l'utilisation des lambdas n'est pas simple. L'utilisation des lambdas nécessite de connaître les services que l'on utilise et l'environnement d'exécution.

Concernant l'exécution, il reste encore des subtilités que je n'ai qu'effleuré sur le code exécuté au premier lancement de la micro machine virtuelle vm supportant une lambda et le code exécuté à chaque lancement. Car c'est une autre spécificité des lambda : **le même environnement d'exécution peut être utilisé une fois ou plus**. On voit qu'il y a des différences avec l'utilisation de conteneurs dans un orchestrateur et que cela nécessite une montée en compétence.

Si vous avez des conseils, des expériences sur les AWS Lambdas, partagez les avec moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

-----

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).
