---
title: "Bilan des quatre ans"
date: "2024-11-14"
draft: false
description: "Une année avec moins de publication"
tags: ["blog"]
author: "Bertrand Madet"
---

J'ai lancé mon blog le 11 novembre 2020. Je résume ces quatre années de publication.

## Données brutes sur les articles

|               |Première année|Deuxième année|Troisième année|Quatrième année|Total|
|---------------|--------------|--------------|---------------|---------------|-----|
|Billets publiés|61            |51            |50             |31             |162  |
|Fiches de lectures :book:  |13|7             |5              |3              |28   |
|Tags plus utilisés|[fiche de lecture (13)]({{< ref "/tags/fiche-de-lecture">}}), [Software Craftmanship (10)]({{< ref "/tags/software-craftmanship">}}) et [base (9)]({{< ref "/tags/base">}})|[Software Craftmanship (14)]({{< ref "/tags/software-craftmanship">}}), [outils (12)]({{< ref "/tags/outils" >}}) et [projet perso (10)]({{< ref "/tags/projet-perso" >}})|[Test (16)]({{< ref "/tags/test">}}), [Software Craftmanship (12)]({{< ref "/tags/software-craftmanship">}}) et [cloud (12)]({{< ref "/tags/cloud" >}})|[Python (10)]({{< ref "/tags/python">}}), [outils (7)]({{< ref "/tags/outils" >}}) et [projet perso (6)]({{< ref "/tags/projet-perso" >}})| [Software Craftmanship (40)]({{< ref "/tags/software-craftmanship">}}),[projet perso (32)]({{< ref "/tags/projet-perso" >}}) et [test (30)]({{< ref "/tags/test" >}})|

Je trouve que ce découpage par tag représente bien mes centres d'intérêts professionnels. On voit que j'ai pu lire et résumer 3 livres cette année :

- Designing Data-intensive Applications de Martin Kleppmann - [fiche de lecture]({{<ref "2024-02-21-designing-data-intensive-applications-lecture">}});
- 50 Algorithms Every Programmer Should Know d'Imran Ahmad, PhD - [fiche de lecture]({{<ref "2024-08-25-50-algorithms-every-programmer-should-know-lecture">}});
- Terraform: Up and Running d'Yevgeniy Brikman - [fiche de lecture]({{<ref "2024-10-31-terraform-up-and-running-lecture">}}).

## Processus de rédaction

Dans la deuxième partie de l'année, je n'ai pas publié une fois par semaine. J'ai passé beaucoup de temps sur l'écriture du front-end administration de mon projet personnel et j'ai fait d'autre choses pour des jeux de plateau, des jeux de rôle et du modélisme.

J'ai aussi trouvé que la lecture des trois livres avaient été plus longue que d'habitude. en `particulier Designing Data-Intensive Applications` qui était riche en informations.

Pour la vérification d'orthographe, j'utilise l'extension VSCode [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker) et [French - Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-french). J'ai changé d'assistant pour vérifier les fautes et les erreurs :  je suis passé de [ChatGPT](https://chat.openai.com) à [Mistral](https://chat.mistral.ai/). J'ai l'impression que Mistral était plus performant avec du texte en français.

Voici le prompt que j'utilise d'habitude.

```
Tu es un relecteur de texte français

A partir de maintenant, je te donnerai des paragraphes en markdown 
et tu me signalera les erreurs grammaticales,
d'orthographe ou de syntaxe dans ces paragraphes.
```

## Processus de diffusion

J'ai délaissé la diffusion par LinkedIn en partie à cause d'une question de temps pour adapter le format de la publication. Fournir un texte raccourci est un travail peu intéressant et cela n'amène trop peu de réaction.

## Conclusion

Une année plus compliquée pour la rédaction. Je pense qu'une publication bimensuelle sera plus en adéquation avec mon rythme actuel. J'ai acheté, sur [Humble Bundle](https://www.humblebundle.com/books), deux lots de livres en édition numérique. Il y avait une cinquantaine de livres dont au moins une dizaine candidats à la fiche de lecture. Le livre Terraform: Up and Running faisait partie du lot.

Je vais regarder pour la création de média de publication plus sympa sur LinkedIn.

Merci à tous ceux qui m'ont donné l'idée de nouveaux billets, d'amélioration ou de rectification. Si vous avez des suggestions concernant mes billets passés ou futurs faîtes m'en faire part autour d'un thé 🍵, sur [X](https://twitter.com/trambi_78), [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
