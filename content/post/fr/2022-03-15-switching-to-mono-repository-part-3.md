---
title: "Passage d'une dizaine de dépôts à un - partie 3"
date: "2022-03-15"
draft: false
description: "mise en place de rush"
tags: ["outils","workflow","monorepository"]
author: "Bertrand Madet"
---

Suite à [nos réflexions sur les pistes d'amélioration sur le mono dépôt]({{< ref "/post/fr/2021-12-17-switching-to-mono-repository-part-2.md" >}}), nous avons décidé d'utiliser [Rush (https://rushjs.io)](https://rushjs.io).

Les améliorations visées étaient :

- la simplification des opérations ;
- la rapidité de l'intégration continue ;
- la rapidité après un changement de branche.

Voici un retour sur la mise en place du Rush.

## Un peu de contexte

Le service sur lequel je travaille est structuré avec une dizaine de répertoires contenant les différents composants. Il y a des services Web en React, un catalogue de composants React, une bibliothèque de fonctions communes et des applications pour gérer les back office.

Les bibliothèques permettent de partager les composants utilisés et les fonctions dans nos différentes applications Web React.

```mermaid
graph BT
  webapp1[Application web React 1]-->|Utilise des composants|catalog[Catalogue de composants]
  webapp2[Application web React 2]-->|Utilise des composants|catalog
  webapp1-->|Utilise les fonctions|common[Bibliothèque de fonctions communes]
  webapp2-->|Utilise les fonctions|common
```

Nous utilisons npm (version 8) pour gérer les paquets. Pour diminuer la complexité et pour améliorer la rapidité, nous avons choisi d'utiliser [Rush (https://rushjs.io)](https://rushjs.io).

## Mise en place

### Installation de rush

:warning: Avant d'installer rush, il faut configurer npm pour installer les modules globaux dans un endroit où nous avons les droits. Par exemple, on peut utiliser la variable d'environnement `NPM_PREFIX`.

On installe rush en global `npm install -g @microsoft/rush` comme indiqué dans la documentation de rushjs.

### Initialisation

Comme notre dépôt hébergeait déjà plusieurs applications, j'ai été obligé de créer un dépôt vide grâce à la commande `rush init` puis j'ai copié le répertoire `common`, le fichier `rush.json` dans le dépôt cible.

Ensuite, j'ai ajouté les différents composants logiciels dans cet ordre :

- Bibliothèque de fonctions communes ;
- Catalogue de composants ;
- Application web React 1 ;
- Application web React 2.

Après avoir modifié le fichier `rush.json` et lancé `rush update` et avant de passer au composant suivant, je m'assurais que :

- les tests passaient - `rushx test` :white_check_mark: ;
- le projet se lance sans avertissement - `rushx start` ,
- la construction se fait sans avertissement - `rushx build` :package:.

La principale difficulté était d'homogénéiser les versions entre les différents composants.

:warning: Le paquet npm `react-scripts` est particulièrement délicat à intégrer car il fixe les versions exactes de ses dépendances. Certaines dépendances comme `jest`, `eslint` sont utilisées pour tous nos composants donc j'ai dû fixer les versions pour qu'elles correspondent à `react-scripts`.

### Adaptation de l'intégration continue

L'intégration continue était complexe, composée d'une vingtaine de tâches, avec quatre tâches d'intégration continue (installation, tests, compilation de TypeScript, construction de container) pour chaque composant.

Avec l'aide précieuse de notre Ops, nous avons pu :

- regrouper les étapes d'installation et de tests en une seule ;
- regrouper les étapes de compilation de TypeScript pour trois des quatre composants.

Nous sommes passés à une douzaine de tâches.

## Conclusion

Au niveau de la complexité, l'utilisation de rush (et de rushx) permet d'homogénéiser les commandes et les paquets npm.

Au niveau de la rapidité, la CI est accélérée d'environ 3 minutes (de 20 minutes à 17 minutes) et le temps de réinstallation suite à un changement de branche est aussi légèrement diminué.

Il nous reste encore à :

- faciliter le lancement des tests ;
- agréger les résultats de couverture de tests ;
- intégrer les applications de back office.

Si vous avez des retours ou des conseils sur la gestion d'un mono dépôt, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/). De mon côté, j'essayerai de rendre compte des évolutions dans notre processus de développement.
