---
title: "Langage interprété ou langage compilé"
date: "2022-06-09"
draft: false
description: "Connaître les différences"
tags: ["code","base"]
author: "Bertrand Madet"
---

J'ai commencé à coder avec du langage C donc un langage  compilé et lors de ma découverte du PHP, j'étais enthousiaste vis à vis des langages interprétés. Je vous propose un billet sur la différence entre langage compilé et langage interprété.

## Langages compilés

Un langage compilé transforme le code source en langage machine. Un langage compilé utilise un compilateur, qui donc transforme le code source en langage machine exécutable sur un système d'exploitation. Parmi les langages compilés :

- L'assembleur ;
- Le C ;
- Le C++ ;
- Le Go ;
- Le Rust.

```mermaid
graph LR;
    source(Code sources)-->compiler[Compilateur]
    compiler-->exe(Exécutable en langage machine)
    exe-..->execution(Exécution à la demande)
```

### Avantages

Les optimisations possibles pour les langages compilés permettent d'avoir des vitesses d'exécution plus élevées. Sans rentrer dans des détails spécifiques à chaque langage et des architectures cibles, les avantages des langages compilés pour l'optimisation sont :

- l'accès à de plus grandes parties du code ;
- l'adaptation aux systèmes d'exploitations cibles ;
- l'adaptation aux architectures cibles.

En général, le typage des variables est plus statique. Les erreurs de compilation permettent aussi de repérer très tôt des défauts.

### Inconvénients

L'étape de compilation amène :

- une lenteur de développement ;
- une manque de souplesse.

Anciennement le C++ était connu pour avoir un temps de compilation long.

## Langages interprétés

Un langage interprété est directement exécuté. Il met en oeuvre un interpréteur. Les principaux langages interprétés :

- Bash ;
- Perl ;
- Python ;
- JavaScript.

```mermaid
graph LR;
    source(Code sources)-->interpreter[Interpréteur]
    interpreter-->execute(Exécution)
```

### Avantages et inconvénients

:+1: L'utilisation de langages interprétés permet une grande souplesse et plus de rapidité de développement.

:-1: L'exécution des interpréteurs est moins rapide car les optimisations possibles sont moins vastes.

## Alternatives

Il y a des langages entre les deux. Des langages qui sont compilés dans un langage intermédiaire pour être exécutés par une machine d'exécution. Les langages qui fonctionnent avec la machine d'exécution Java (JRE - **J**ava **R**untime **E**xecutable) :

- Java ;
- Scala ;
- Kotlin.

Des langages s'exécutent *in fine* sur la machine d'exécution DotNet, comme :

- C# ;
- VB.net ;
- F#.

Le langage spécifique pour une machine d'exécution est appelé `bytecode`.

```mermaid
graph LR;
    source(Code sources)-->compiler[Compilateur]
    compiler-->bytecode(Exécutable en bytecode)
    bytecode-..->jre(Machine d'exécution à la demande)
```

Un autre langage particulier le TypeScript est transformé (le terme exact est `transpilé`) en JavaScript.

```mermaid
graph LR;
    source(Code sources)-->transpiler[Transpileur]
    transpiler-->js(JavaScript)
    js-..->browser(Exécution sur le navigateur ou Node.js)
```

## Conclusion

Aujourd'hui, la notion de langage compilé ou interprété s'estompe quand on voit que le scala peut être transformé en JavaScript ou que le C et le C++ peuvent être transformé en WebAssembly (un sous ensemble du JavaScript). Interprété ou compilé devient plutôt une caractéristique de l'outillage et de l'écosystème que celle d'un langage.

Les caractéristiques importantes du langage de mon point de vue sont l'expressivité et l'outillage disponible. Si vous avez des opinions sur les caractéristiques importantes des langages, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
