---
title: "4 commandes Git"
date: "2021-01-24"
draft: false
description: "pour améliorer vos commits"
tags: ["outils","git"]
author: "Bertrand Madet"
---

Il est encore temps de vous présenter mes voeux pour l'année 2021. Je vous souhaite plein de bonnes choses, et surtout la santé. Je nous souhaite aussi de mieux utiliser Git. Et surtout de faire des meilleurs commits.

Oui vous le savez si vous avez lu [Améliorer ses compétences sur Git]({{< ref "/post/fr/2020-11-19-ameliorer-ses-competences-sur-git.md" >}}), je suis attaché au gestionnaire de version Git.

## Le bon commit et le mauvais commit

Le commit idéal de mon point de vue embarque les modifications des tests, du code, de documentation, de configuration pour une fonctionnalité ou une correction précise. C'est extrêmement difficile de faire ce genre de commit, à chaud sans retouche. En plus, il doit embarquer un message qui apporte une plus-value au code.

Autant dire que je n'y arrive pas souvent : j'oublie de modifier la documentation, je découvre un bug juste à après avoir commité... Mais heureusement, Git permet de retravailler *a posteriori* le récit de vos développements.

## git rebase -i HEAD~n

Cette commande permet de modifier l'historique jusqu'à n commits. Vous allez pouvoir changer l'ordre des commits, en supprimer, en fusionner...

En général, je l'utilise avant de pousser vers le serveur. Attention toutefois à modifier uniquement les commits locaux sinon vous allez mettre le bazar dans les historiques de vos collègues.

## git add --patch

Cette commande permet de choisir quelles parties d'un fichier ajouter dans un commit. Typiquement, si vous corrigez deux bugs en même temps, vous pouvez utiliser l'option `--patch` pour finalement faire deux commits propres.

## git stash push/pop

`stash` est votre tiroir personnel. Quand vous voulez sauvegarder vos modifications, vous utilisez l'option `push`. Quand vous voulez appliquer les modifications sauvegardées, vous utilisez l'option `pop`.

## git commit --amend

Admettons que vous ne soyez pas satisfait de votre message du dernier commit, vous pouvez utiliser `git commit --amend`. Cette commande peut se combiner avec `git rebase -i HEAD~n` pour modifier les messages de commits locaux.

## Un petit conseil sur le message pour finir

Le message est une partie intégrante du commit. Il doit donc apporter une information utile, une manière d'opérer est de détailler l'intention ou la motivation du commit.

Et vous, quelles sont vos commandes git préférées ? Faites-m'en part sur [Twitter](https://twitter.com/trambi_78) ou [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
