---
title: "Composing Software - Fiche de lecture"
date: "2021-02-21"
draft: false
description: "Livre de Eric Elliot sur la composition logicielle et la programmation fonctionnelle"
tags: ["fiche de lecture","programmation fonctionnelle"]
author: "Bertrand Madet"
---


# A qui s'adresse ce livre

Aux développeuses et aux développeurs qui s'intéressent à la programmation fonctionnelle et qui connaissent le JavaScript.

# Ce qu'il faut retenir

La composition est l'essence même de la programmation. Le processus de programmation peut se résumer ainsi :

- décomposer un problème en sous-problème plus simples ;
- coder une solution pour chaque sous-problème ;
- composer nos solutions pour répondre au problème initial.

# Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Démonstration efficace entre la programmation impérative et la programmation déclarative| :-1: Beaucoup de répétitions, en particulier sur les foncteurs et la fonction map|
| :+1: L'interprétation fonctionnelle de la composition est très présente| :-1: La plupart des exemples sont trop basiques et on ne voit pas comment les appliquer|

J'ai eu l'impression de lire une compilation de très bons billets de blog plutôt qu'un livre. Cela manque de profondeur mais cela m'a incité à creuser sur la programmation fonctionnelle. 

J'aurai aimé y trouver des cas où les choses tournent mal en programmation fonctionnelle par exemple dans les cas d'utilisation d'API dont les fonctions sont impures.

# Détails

En anglais :gb: :
 - Composant Software fr Eric Elliot
 - ISBN-13 :978-1661212568

Pas traduit en français :cry: