---
title: "Exploration de données avec Python"
date: "2023-11-28"
draft: false
description: "pandas et SQLite ou duckdb"
tags: ["python", "outils"]
author: "Bertrand Madet"
---

Je suis parfois amené à explorer des données sous forme de fichier JSON, CSV ou même Excel :exploding_head:. En règle générale, s'il s'agit de fichiers textes avec l'extraction de champs bien identifiés, j'utilise les commandes GNU (cf. [Manipuler les données avec des commandes GNU]({{< ref "/post/fr/2023-06-15-data-manipulation-with-gnu-commands" >}})). Pour le reste j'utilise Python pour fouiller dans ces données.

## Pandas

[Pandas (https://pandas.pydata.org/)](https://pandas.pydata.org/) est une bibliothèque Python de d'analyse de données. Elle a une licence libre et est très utilisée les Data Scientists (en tout cas de ceux que je connais). La grande force que j'y vois est la multitude de formats pour la lecture et l'écriture supportés du `csv` au `parquet` en passant évidemment par `excel`.

```python
import pandas as pd

df = pd.read_csv("mon-fichier.csv")
```

On obtient un DataFrame pandas qui ressemble à un tableau avec des colonnes nommées et typées. On peut ensuite manipuler les résultats comme les filtrer, sélectionner uniquement certaines colonnes, les regrouper.

```python
df[df["colonne-a-filtrer"] > 10][["colonne1-a-selectionner","colonne2-a-selectionner","colonne3-a-selectionner"]]
```

Si on veut mettre deux conditions, il faut comme ceci :

```python
df[(df["colonne1-a-filtrer"] > 10)&(df["colonne2-a-filtrer"] == 4)]
```

Bref, il faut aimer les crochets :hook:. Personnellement, je pratique trop irrégulièrement pour me souvenir de la syntaxe. Je préfère donc le SQL dont je connais bien la syntaxe.

## Pandas + SQLite

[SQLite (https://www.sqlite.org/)](https://www.sqlite.org/) est un moteur de base de données sous la forme du bibliothèque C. Elle appartient au domaine public et existe de sous forme de bibliothèque dans beaucoup de langages, dont Python.

En utilisant la capacité de Pandas de lire un format et d'exporter au format SQL, on peut charger les données dans une base de données SQLite.

```python
import pandas as pd
import sqlalchemy
df = pd.read_excel("mon-fichier.xls")
engine = sqlalchemy.create_engine("sqlite:///nouveau-fichier-sqlite.db")
df.to_sql("table_exploration",con=engine)
```

On peut ensuite explorer la base de données contenue dans le fichier `nouveau-fichier-sqlite.db` avec l'outil en ligne de commande [sqlite3 (https://www.sqlite.org/cli.html)](https://www.sqlite.org/cli.html) ou en Python avec SQLAlchemy et Pandas ou SQLAlchemy seul.

```python
import pandas as pd
import sqlalchemy
engine = sqlalchemy.create_engine("sqlite:///nouveau-fichier-sqlite.db")
query = #requete sql compatible SQLite
pd.read_sql_query(query, con=engine)
```

Dans le cas de SQLAlchemy seul, il y a un palier d'apprentissage de l'API de SQLAlchemy comme j'ai pu le montrer dans le billet [Débuter avec SQLAlchemy]({{< ref "/post/fr/2023-08-30-first-steps-with-sqlalchemy" >}}).

## DuckDB

[DuckDB (duckdb.org)](duckdb.org) est un moteur de base de données sans serveur. Il est sous licence libre MIT et est optimisé pour l'analyse. Il permet de charger directement des fichiers csv, json, parquet.

```python
import duckdb
duckdb.query("SELECT * FROM 'mon-fichier.csv'")
```

La couverture fonctionnelle du SQL est plus large que celle de SQLite. Il est aussi possible d'utiliser directement les dataframes pandas.

```python
import duckdb
import pandas as pd

# df est une variable contenant un dataframe pandas
duckdb.query("SELECT * FROM df")
```

Avec la méthode `df`, il est possible de créer un dataframe Pandas.

## Conclusion

Personnellement, j'ai découvert récemment DuckDB et je suis assez impressionné. Si vous êtes comme moi et que vous connaissez mieux SQL que l'API pandas, je pense que c'est une solution intéressante pour l'exploration de données.

Si vous avez des expériences sur des outils d'analyse ou que vous souhaitez m'en faire connaître d'autres, n'hésitez pas à les partager avec moi sur [X](https://x.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
