---
title: "Jouons avec les serverless functions - partie 2"
date: "2023-04-20"
draft: false
description: "et ça marche"
tags: ["projet perso","Go","jeu","cloud","lambda"]
author: "Bertrand Madet"
---

A la fin de mon [premier billet sur les serverless functions]({{< ref "/post/fr/2023-03-29-explore-serverless-functions-part1" >}}), nous avions une fonction testable mais qui ne fonctionnait pas. Je vous propose de finir cette semaine.

Pour l'instant le système de fichier est local, ce qui ne fonctionnera pas avec ma fonction une fois déployée. En général, les entrées et sorties sont gérées par des interfaces. Ce sont le genre de chose qui peuvent changer souvent dans un programme. Et cela va simplifier les tests. Pas besoin d'avoir un système de fichier distant fonctionnel et accessible pendant les développements ou les tests.

## Fonctions en interfaces

On va donc créer deux interfaces, une pour lire un fichier, et une pour écrire un fichier.

```go
type iReader interface {
  readFile(ctx context.Context, filepath string) (io.Reader, error)
}

type iWriter interface {
  writeFile(ctx context.Context, filename string, object io.Reader, objectSize int64) error
}
```

Pour faciliter la composition entre le fait de charger la configuration et la lecture depuis une système de fichier, je remplace mes trois fonctions de type `loadSettingsFunc`, `loadGamesFunc` et `saveGamesFunc` par des interfaces respectivement `iSettingsLoader`, `iGamesLoader` et `iGamesSaver`.

```go
type iSettingsLoader interface {
  loadSettings(ctx context.Context) (settings.Settings, error)
}

type iGamesLoader interface {
  loadGames(ctx context.Context) ([][]string, error)
}

type iGamesSaver interface {
  saveGames(ctx context.Context, games [][]string) error
}
```

:warning: Il aurait été possible de faire de la composition en gardant des fonctions :

- soit en ajoutant un paramètre à la fonction ;
- soit en retournant des fonctions.

Mais je trouvais l'approche interface plus simple à lire, sans doute un héritage de mon passage par le C++.

## Implémentations

Bon pour l'instant, j'ai ajouté 5 interfaces et pas d'implémentation. Voici donc l'implémentation de chargement de la configuration à partir d'un JSON.

```go
package handler

import (
  "context"
  "fmt"
  "io"

  jsonsettings "gitlab.com/trambi/tournamenttoolbox/pkg/json/settings"
  "gitlab.com/trambi/tournamenttoolbox/pkg/settings"
)

type settingsFromJson struct {
  reader iReader
}

func (s settingsFromJson) loadSettings(ctx context.Context) (settings.Settings, error) {
  const (
    filepath = "settings.json"
  )
  reader, err := s.reader.readFile(ctx, filepath)
  if err != nil {
    return settings.Settings{}, fmt.Errorf("error while reading file %s: %s", filepath, err.Error())
  }
  values, err := io.ReadAll(reader)
  if err != nil {
    return settings.Settings{}, fmt.Errorf("error while extracting content of file %s: %s", filepath, err.Error())
  }
  return jsonsettings.FromJSONBytes(values)
}
```

On voit que l'implémentation de mon interface de lecture de fichier et de mon objet de lecture d'objet métier. Je pourrais tester cette classe en lui injectant une implémentation qui va me retourner une erreur lors de l'appel de readFile ou fonctionner normalement mais en mémoire.

Les deux autres interfaces de chargement et de sauvegardes des matchs sont très ressemblantes. Intéressons-nous plutôt à la classe qui fait le lien entre notre fonction et S3 : ``s3Wrapper`

```go
package handler

import (
  "context"
  "fmt"
  "io"
  "log"
  "os"

  "github.com/minio/minio-go/v7"
  "github.com/minio/minio-go/v7/pkg/credentials"
)

// s3Wrapper is an adapter from minio.Client who implements iReader and iWriter
type s3Wrapper struct {
  client             minio.Client
  bucketName         string
  region             string
  contentTypeToWrite string
}

func (w s3Wrapper) writeFile(ctx context.Context, objectName string, reader io.Reader, objectSize int64) error {
  putInfo, err := w.client.PutObject(ctx,
    w.bucketName,
    objectName,
    reader,
    objectSize,
    minio.PutObjectOptions{ContentType: w.contentTypeToWrite, StorageClass: "ONEZONE_IA"})
  if err != nil {
    return fmt.Errorf("error while putting object to %s.%s: %w", w.bucketName, objectName, err)
  }
  log.Println("putInfo: ", putInfo)
  return nil
}

func (w s3Wrapper) readFile(ctx context.Context, objectName string) (io.Reader, error) {
  bucketExists, err := w.client.BucketExists(ctx, w.bucketName)
  if err != nil {
    return nil, fmt.Errorf("error while file reading unable to know about existence of bucket %s: %w", w.bucketName, err)
  }
  if !bucketExists {
    return nil, fmt.Errorf("error while file reading bucket %s seems to not exist", w.bucketName)
  }
  return w.client.GetObject(ctx, w.bucketName, objectName, minio.GetObjectOptions{})
}

func newS3Wrapper() (s3Wrapper, error) {
  var (
    endpoint        = os.Getenv("S3_ENDPOINT")
    accessKeyID     = os.Getenv("S3_ACCESSKEY")
    secretAccessKey = os.Getenv("S3_SECRET")
    bucketName      = os.Getenv("S3_BUCKET_NAME")
    region          = os.Getenv("S3_REGION")
  )

  minioClient, err := minio.New(endpoint, &minio.Options{
    Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
    Secure: true,
  })
  if err != nil {
    return s3Wrapper{}, fmt.Errorf("error opening s3 connexion to %v : %w", endpoint, err)
  }
  s3Wrapper := s3Wrapper{client: *minioClient, bucketName: bucketName, region: region, contentTypeToWrite: "application/json"}
  return s3Wrapper, nil
}
```

On le voit la classe implémente les deux interfaces de lecture et d'écriture définis plus haut. La fonction principale a donc cette allure :

```go
func Handle(w http.ResponseWriter, r *http.Request) {

  if r.Method != http.MethodGet {
    log.Printf("Used method %v is not GET\n", r.Method)
    http.Error(w, fmt.Sprint("Used method ", r.Method, " is not GET"), http.StatusMethodNotAllowed)
    return
  }
  // Initialize minio client object.
  s3Wrapper, err := newS3Wrapper()
  if err != nil {
    log.Println(err.Error())
    return
  }
  settingsLoader := settingsFromJson{reader: s3Wrapper}
  gamesLoader := gamesFromCsv{reader: s3Wrapper}
  gamesSaver := gamesToCsv{writer: s3Wrapper}
  injectedHandle(r.Context(), w, settingsLoader, gamesLoader, gamesSaver)
}
```

Si je voulais tester automatiquement la classe "d'enrobage" S3, je serais obligé soit de mocker minio soit de faire un test avec un serveur S3 (un test d'intégration en somme).

## Tests

Dans cette exploration, j'ai créé des tests automatiques uniquement sur la fonction principale et juste testé manuellement sans mettre les variables d'environnement puis en mettant les bonnes valeurs. La bibliothèque `github.com/scaleway/serverless-functions-go/local` m'a beaucoup aidé pour faire fonctionner la fonction sans télécharger le code.

```go
package main

import (
  "github.com/scaleway/serverless-functions-go/local"
  "handler"
)

func main() {
  local.ServeHandler(handler.Handle, local.WithPort(8080))
}
```

## Terraform

Un provider Terraform existe pour Scaleway. On peut essayer de décrire l'infrastructure simpliste : serveress function et namespace.

```HCL
terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
    }
  }
  required_version = ">= 0.13"
}

provider "scaleway" {
  zone   = "${var.region}-1"
  region = var.region
}

resource "scaleway_function_namespace" "main" {
  name        = "explo2"
  description = "Exploration namespace"
  project_id = var.project_id
}

data "archive_file" "lambda" {
  type        = "zip"
  source_dir = "Handle"
  excludes    = ["cmd"]
  output_path = "serverless_function_payload.zip"
}

resource "scaleway_function" "main" {
  namespace_id = scaleway_function_namespace.main.id
  runtime      = "go118"
  handler      = "Handle"
  privacy      = "public"
  timeout      = 10
  zip_file     = data.archive_file.lambda.output_path
  zip_hash     = data.archive_file.lambda.output_base64sha256
  deploy       = true
  environment_variables = {
    "S3_ENDPOINT" = "s3.${var.region}.scw.cloud"
    "S3_ACCESSKEY" = var.access_key
    "S3_BUCKET_NAME"      = var.bucket
    "S3_REGION" = var.region
  }
  secret_environment_variables = {
    "S3_SECRET" = var.secret_access_key
  }
}
```

:warning: la variable d'environnement `S3_ENDPOINT` ne doit pas débuter par "http://".

## Conclusion

On a pu arriver au bout, le cas d'usage de lire et d'écrire avec une fonction est intéressant pour explorer. L'instanciation des serverless functions de Scaleway semble plus lente que celle des lambdas AWS et les modalités de déclenchement sont plus restrictives. Mais l'offre évolue sans cesse. De plus c'est une excellente manière de commencer sans être noyé sous la multitude des services des gigantesques acteurs comme AWS, Azure ou GCP.

D'un point de vue plus technique, je me rappellerai :

1. Qu'une `serverless function` Scaleway a la même structure qu'une Lambda AWS : fonction qui prend en paramètres un `http.ResponseWriter` et un pointeur vers une `http.Request` ;
2. Que les pratiques d'architectures logicielles s'appliquent de manière similaire aux exécutables;
3. Que le fait de pouvoir tester avant de déployer est capital, les bibliothèques `net/http/httptest` et `serverless-functions-go/local` permettent de le faire en golang.

Si vous avez envie de raconter vos explorations sur les technologies serverless, partager les avec moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
