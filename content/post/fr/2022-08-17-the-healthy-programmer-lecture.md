---
title: "The Healthy Programmer - Fiche de lecture"
date: "2022-08-17"
draft: false
description: "Livre de Joe Kutner sur la santé"
tags: ["fiche de lecture","santé"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Aux développeuses et aux développeurs qui s'intéressent à leur santé. À ceux et celles aussi qui travaillent devant un clavier toute la journée et qui souhaitent continuer à le faire en bonne santé.

## Ce qu'il faut retenir

- La sédentarité et en particulier la station assisse prolongée est le pire ennemi de notre santé ;

- Il est possible d'améliorer sa santé, en intégrant des petits changements ;

- La discipline nécessaire est intégrable dans les pratiques de l'ingénierie logicielle.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Les éléments santé sont documentés par des études| :-1: Dommage que le deuxième objectif soit d'acheter un podomètre|
| :+1: Les conseils sont très pratiques et détaillés| :-1: Les métaphores sur les pratiques de développement empêchent d'élargir la cible à savoir tous les personnes qui travaillent sur un clavier|
| :+1: La bonne idée de l'intégration de la technique pomodoro et la lutte contre la sédentarité||

J'ai eu du mal à finir ce livre, je l'ai commencé l'été 2021. J'avais arrêté la lecture au deuxième objectif, l'achat du podomètre car je trouvais qu'il y avait autres manières d'encourager la marche.

En reprenant la lecture cet été, j'ai été convaincu par la suite, notamment par l'intégration dans la structure du [pomodoro](https://fr.wikipedia.org/wiki/Technique_Pomodoro) ([que j'ai évoqué dans le billet sur le pair programming et pomodoro]({{< ref "/post/fr/2021-09-14-pair-programming-pomodoro" >}})) d'exercices physiques pour casser la sédentarité (et cela est encore plus facile en télétravail).

Le tout est suffisant intéressant pour donner envie de partager ces multiples informations et pratiques avec toutes et tous.

## Détails

En anglais :gb: :

- The Healthy Programmer de Joe Kutner
- ISBN-13 : 978-1-93778-531-4

Non disponible en français :fr: :cry: