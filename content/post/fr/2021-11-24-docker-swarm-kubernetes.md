---
title: "Docker Swarm vs Kubernetes"
date: "2021-11-24"
draft: false
description: "Une comparaison à l'aune de ma modeste expérience"
tags: ["devops","container","kubernetes","dockerswarm"]
author: "Bertrand Madet"
---

Dans un billet précédent, je vous proposais [une histoire complètement subjective des conteneurs]({{< ref "/post/fr/2021-10-27-container-history.md" >}}) et je vous ai parlé à plusieurs reprises d'orchestrateurs (cf. [Un volume persistant pour Docker Swarm]({{< ref "/post/fr/2021-10-04-replicated-volumes-glusterfs.md" >}}) ou [A propos de Kubernetes]({{< ref "/post/fr/2021-10-18-a-propos-kubernetes.md" >}})).

Je pense être aujourd'hui capable de fournir une comparaison entre Docker Swarm et Kubernetes à l'aune de ma modeste expérience.

## Orchestrateur de conteneur

Avant d'attaquer la comparaison, il faut rappeler à quoi sert un orchestrateur de conteneur.

Il est possible de gérer des déploiements de conteneur sur plusieurs serveur `à la main`. A la main signifiant en lançant les commandes de création ou de suppression de conteneur.

Imaginez devoir :

- relancer deux services dépendant l'un de l'autre sur une (1) machine virtuelle sur vingt (20) après un crash ;
- relancer un processus conteneurisé en peu instable à chaque fois qu'il s'arrête ;
- faire une mise à jour roulante d'une service déployé à dix (10) exemplaires (cela signifie dix fois d'affilié lancer un conteneur avec la nouvelle image et les bons paramètres, s'assurer que le lancement est ok puis éteindre le conteneur de la version ultérieure);
- injecter une clé TLS dans le conteneur web.

Même avec un nombre limité de services et de serveurs cela peut être fastidieux, source d'erreur et nécessiter une surveillance constante des services lancés sous forme de conteneurs .

Un orchestrateur de conteneur va permettre de gérer automatiquement les conteneurs et les problématiques exposés plus haut.

Mais cela n'est pas magique, il va falloir décrire les services et leurs dépendances (réseau, secret ou configuration). La description étant en général sous forme de fichiers YAML, elle est facilement sauvegardable en gestion de configuration.

## Docker Swarm

[Docker Swarm (https://docs.docker.com/engine/swarm/)](https://docs.docker.com/engine/swarm/) est la solution intégrée d'orchestration fournit par Docker, Inc. La solution est sous licence propriétaire.

## Kubernetes

[Kubernetes (https://kubernetes.io/)](https://kubernetes.io/) est une solution initialement développée par Google. Elle a été fournie à la communauté sous [licence Apache 2 (https://spdx.org/licenses/Apache-2.0.html)](https://spdx.org/licenses/Apache-2.0.html).

## Pré requis et installation

### Docker Swarm

Pour utiliser vous avez besoin d'un serveur (physique ou virtuel) sous Linux et de Docker pas trop vieux installé et de lancer la commande `docker swarm init`.

Cela sera mieux avec trois serveurs ou plus en gardant toujours un nombre impair de noeuds manager du cluster.

### Kubernetes

Pour utiliser vous avez besoin d'un serveur (physique ou virtuel) sous Linux. Il faudra choisir l'implémentation de Kubernetes que vous désirez et l'installer.

On conseille en général d'avoir trois (3) serveurs membres du plan de contrôle (:gb: `Control plane`) et au moins deux (2) serveurs d'exécution. A priori les agents Kubernetes seront installés sur les noeuds d'exécution au moment de l'inscription du noeud dans le cluster.

### Résumé

Docker Swarm est moins exigeant pour l'installation.

## Concepts et niveau d'abstraction

### Docker Swarm

Les concepts principaux de Docker Swarm s'organisent principalement concepts sous forme d'une hiérarchie :

- Les conteneurs sont l'unité de base ;
- Les `services` permettent de gérer l'exécution de conteneurs de la même image sur le cluster ;
- Les `stacks` permettent de gérer l'organisation des services.

Autour de ces notions principales gravitent les notions de noeud ( ou `node`), de volume, de réseau, de configuration, de secrets...

La plupart des notions se superposent avec les notions de Docker. Seuls les notions de noeuds, de services, de stacks, de configuration ou de secrets sont à apprendre pour des habitués à Docker.

Les fichiers de description des stacks sont des versions augmentées de fichier `docker-compose`.

### Kubernetes

Les concepts principaux de Kubernetes sont :

- Le `Pod` - abstraction de conteneur et unité d'exécution minimale ;
- Le contrôleur (:gb: `controller`) définit les comportements d'un pod ou d'autres contrôleurs.

Les concepts de noeud, de volume persistant, de réclamation de volume de persistant, de services, de configuration, de secrets...

La plupart des notions sont plus complexes et plus riches que celles fournis par Docker Swarm.

### Résumé

Docker Swarm est plus accessible aux habitués à Docker ou à Docker compose.

## Flexibilité

### Docker Swarm

Docker Swarm permet de gérer facilement des conteneurs sans état ou avec état (exécutés sur des noeuds spécifiques). Il peut gérer des mises à jour roulantes.

Docker Swarm ne permet de gérer de façon intégrée :

- de volume distribué ;
- de mise à échelle automatique (`autoscaling`) ;
- les répartiteurs de charge pour les services.

### Kubernetes

Kubernetes est extrêmement extensible et propose toutes les fonctionnalités de Docker Swarm et plus encore.

Même si toutes les implémentations de Kubernetes ne permettent de gérer les notions les plus complexes (celles manquantes à Docker Swarm). Les implémentations de référence sur le cloud ou on premise fournissent les fonctionnalités les plus importantes.

### Résumé

Kubernetes est plus riche que Docker Swarm.

## Comment choisir ?

### Le choix de Kubernetes

Si votre logiciel s'exécute sous forme de services complexes, je pense qu'il faut considérer sérieusement Kubernetes si :

- si vous avez accès à une implémentation de référence de Kubernetes (celles fournit par les fournisseurs d'informatique distribuée ou OpenShift de RedHat ou Rancher) ;
- ou si avez besoin de fonctionnalités complexes d'automatisation ;
- ou si vous devez proposer une solution d'orchestration pérenne à moyen terme (2 ou 3 ans).

### Le choix de Docker Swarm

Le choix de Docker Swarm devrait se limiter au cas où :

- soit le service est très simple ;
- soit l'installation de Kubernetes est complexe dans votre environnement et vous n'avez peu ou pas besoin de fonctionnalités complexes.

## Conclusion

Pour moi, la balance penche nettement du côté de Kubernetes. Docker Swarm reste un bon produit mais Kubernetes tire parti de l'expertise de Google dans la gestion des conteneurs et du dynamisme de l'écosystème des logiciels libres.

Si vous avez des retours sur les orchestrateurs de conteneurs ou d'autres outils de déploiement, n'hésitez pas à les partager sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
