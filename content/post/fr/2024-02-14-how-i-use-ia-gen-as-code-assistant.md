---
title: "Mon usage de l'IA générative comme assistant de code"
date: "2024-02-14"
draft: false
description: "avec ChatGPT et d'autres LLM"
tags: ["outils"]
author: "Bertrand Madet"
---

La vague des **L**arge **L**anguage **M**odel a déferlé il y a un peu plus d'un an. Et sans être un expert, j'ai pû tester quelques configurations d'assistance au code basées sur des LLM.

Mon approche n'est pas d'obtenir le prompt qui va fournir la sortie idéale mais d'avoir une sortie suffisante pour itérer. Je préfère itérer sur le code que sur le prompt qui permettrait peut-être de générer le code. L'idée est donc plus d'indiquer des méthodes possibles d'utilisation que de donner des prompts à réutiliser.

## ChatGPT et autres chats

J'ai pû tester ChatGPT public et l'expérimentation corporate de chat basé sur ChatGPT.

> :rotating_light: Vous pouvez utiliser ChatGPT sur des codes d’entraînement comme des coding dojos. Les conditions d'utilisation au niveau de la propriété intellectuelle ne sont pas suffisamment claires pour moi pour les utiliser sur du code professionnel ou même sur du code open-source dont vous souhaitez garder la propriété intellectuelle.

### Implémente la fonction qui passe ces tests

Étant très attaché au **T**est **D**riven **D**evelopment, j'ai assez rapidement demandé l'implémentation qui passe un test ou une suite de tests.

```txt
You are a senior software engineer trained to clean code and test driven development. You are pair programming with me.

Implement the function <nom de la fonction> in <langage> that passes the following test.
<code du test>
```

Par exemple avec le calcul du score au tennis en TypeScript. Le test est :

```typescript
import { describe,expect, it } from 'bun:test';
import { score } from 'tennis';

describe('score', ()=>{
  it.each([
    ['0-0',0,0],
    ['15-0',1,0],
    ['30-0',2,0],
    ['40-0',3,0],
    ['Game player1',4,0],
    ['0-15',0,1],
    ['0-30',0,2],
    ['0-40',0,3],
    ['Game player2',0,4]
  ])('should display %s with %d won points for player1 and %d won points for player2',(expected,wonPoints1,wonPoints2) => {
    expect(score('player1','player2',wonPoints1,wonPoints2)).toEqual(expected);
  } )
});
```

En utilisant le prompt avec le nom de la fonction "score", TypeScript et le test ci-dessus. ChatGPT nous fournit un code TypeScript.

```typescript
export function score(player1: string, player2: string, wonPoints1: number, wonPoints2: number): string {
    const pointsToScore = ['0', '15', '30', '40', 'Game'];
    
    if (wonPoints1 >= 3 && wonPoints1 - wonPoints2 >= 2) {
        return 'Game ' + player1;
    }

    if (wonPoints2 >= 3 && wonPoints2 - wonPoints1 >= 2) {
        return 'Game ' + player2;
    }

    return pointsToScore[wonPoints1] + '-' + pointsToScore[wonPoints2];
}
```

Les tests ne passent pas pour les cas `40-0` et `0-40` car il y a une erreur sur les supérieurs ou égales à 3. En remplaçant les supérieurs ou égales à 3 en strictement supérieur à 3, les tests passent.

Ensuite, quand j'ajoute des cas de tests j'obtiens :

- soit une fonction avec certains tests qui ne passent pas ;
- soit une fonction qui couvre d'autres cas que les tests que j'ai donné.

Est-ce à dire qu'il faut tout jeter 🗑️ ? Non, l'avantage de commencer par les tests est que l'on repère les problèmes très rapidement. On a donc une base de travail imparfaite mais avec l'assurance amenée par les tests.

### Code les tests unitaires de cette fonction

On peut aussi faire l'inverse et demander les tests unitaires correspondant 🧪 à la fonction dont le code est donné. Il faut préciser le moteur de test et les bibliothèques à utiliser.

> :warning: Il convient d'être méfiant *vis-a-vis* des bibliothèques proposées par ChatGPT ou autres chatbots, car ils peuvent "halluciner" et vous orienter vers une bibliothèque qui n'existe pas ou pire que des hackers auraient créé à des fins malicieuses.

```txt
You are a senior software engineer trained to clean code and test driven development. You are pair programming with me.

Code the unit tests of the function <nom de la fonction> in <langage>. The code of the function below.
<code de la fonction>
```

Là encore, ce n'est pas parfait, il faudra faire tourner les tests générés et les corriger.

### Proposition de refactoring

Une fois que vous avez vos tests et votre code, vous pouvez faire des refactorings sur votre code.

```txt
You are a senior software engineer trained to clean code. You are pair programming with me. 

Propose step by step refactoring of the function score which has the following code
export function score(
  player1: string,
  player2: string,
  wonPoints1: number,
  wonPoints2: number
): string {
  const scoreMapping = ["0", "15", "30", "40","Game"];
  if (wonPoints1 > 0 && wonPoints1 < 3 && wonPoints1 === wonPoints2) {
    return `${scoreMapping[wonPoints1]} all`;
  } else if (wonPoints1 === wonPoints2 && wonPoints2 >= 3) {
    return `Deuce`;
  } else if (wonPoints1 >= 4 && wonPoints1 - wonPoints2 >= 2) {
    return `Game ${player1}`;
  } else if (wonPoints1 >= 4 && wonPoints1 - wonPoints2 === 1) {
    return `Advantage ${player1}`;
  } else if (wonPoints2 >= 4 && wonPoints2 - wonPoints1 >= 2) {
    return `Game ${player2}`;
  } else if (wonPoints2 >= 4 && wonPoints2 - wonPoints1 === 1) {
    return `Advantage ${player2}`;
  } else {
    return `${scoreMapping[wonPoints1]}-${scoreMapping[wonPoints2]}`;
  }
}
```

Il va proposer :

1. d'extraire les nombres magiques et les chaînes de caractères magiques en constantes nommées ;
1. de simplifier les conditions et les rendre plus expressives;
1. d'utiliser les retours pour réduire les conditions if imbriqués ;
1. d'utiliser des interpolations de chaîne de caractère de manière plus consistante.

Et le code modifié (qui ne passe plus les tests). En continuant en ajoutant le prompt, `step by step`, il détaille chaque étape et vous avez des refactorings et vous pouvez faire passer les tests étape par étape.

## GitHub Copilot

On le voit ChatGPT ou les autres chatbots peuvent être utile. C'est assez pénible de copier un prompt dans son éditeur de code, coller le prompt dans le bon onglet du navigateur, copier le code dans son éditeur de code, coller le code dans le bon onglet du navigateur. On passe son temps à copier-coller et à alterner entre éditer de code et onglet du navigateur.

GitHub Copilot est une solution payante qui permet d'intégrer une LLM dans l'éditeur de code. J'ai pu l'essayer dans le cadre d'une expérimentation. Pour VSCode, il y a deux extensions GitHub Copilot et GitHub Copilot Chat.

### Sous forme de chat

GitHub Copilot Chat permet d'intégrer une zone de chat dans VSCode évitant d'alterner entre éditeur de code et onglet du navigateur, il y a toujours de copier-coller à faire. Les résultats dans le chat sont basés sur la dernière version de ChatGTP, la version 4. Les résultats sont toujours à vérifier.

Je regrette de n'avoir pas trouvé un catalogue de prompt ou des snippets pour le chat pour faciliter l'écriture des prompts.

### Intégré dans l'éditeur de code

GitHut Copilot propose la suite du code directement dans l'éditeur de code. Il suffit de taper sur `Tab` pour accepter la proposition si elle vous convient. Cela permet aussi d'écrire du pseudo code en commentaire en suivant la méthode `PPP` pour **P**seudocode **P**rogramming **P**rocess** décrite dans Code complete (cf. [Fiche de lecture]({{<ref "/post/fr/2021-11-01-code-complete-lecture.md">}})) et ensuite d'avoir des suggestions d'implémentation.

Le menu contextuel contient aussi des options supplémentaires :

- `test` pour demander des tests pour la fonction dont le code est sélectionné ;
- `explain` pour expliquer la fonction dont le code est sélectionné ;
- `doc` pour écrire la documentation de la fonction dont le code est sélectionné ;
- `refactor` pour proposer des refactorings de la fonction dont le code est sélectionné.

On le voit pas de raccourci pour ma méthode préférée l'implémentation de la fonction pour que les tests donnés passent. Cela reste possible en passant par le chat.

## Conclusion

Je n'ai pas pu tester d'autres solutions comme [TabNine](https://marketplace.visualstudio.com/items?itemName=TabNine.tabnine-vscode) ou [llm-vscode d'Hugging Face](https://marketplace.visualstudio.com/items?itemName=HuggingFace.huggingface-vscode). Je devais expérimenter TabNine mais cela a échoué dans le système d'information corporate. J'essayerai peut-être d'utiliser l'extension d'Hugging Face avec un modèle hébergé.

Vous avez pu le constater mon approche n'est pas d'obtenir le "prompt parfait" qui va fournir la sortie idéale. Dans tous les cas ces outils ne sont pas encore matures : il faut relire et comprendre le code généré. Très souvent il faudra aussi le modifier. Avec le gain de rapidité, il est impératif d'utiliser des tests pour éviter de produire plus rapidement du "code legacy".

Les assistants sont très complémentaires des outils plus classiques comme les analyseurs statiques de code.

Si vous avez trouver des usages ou des prompts, vous pouvez les partager avec moi sur [X](https://x.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
