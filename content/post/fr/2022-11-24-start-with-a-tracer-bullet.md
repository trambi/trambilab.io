---
title: "Les balles traçantes"
date: "2022-11-24"
draft: false
description: "une méthode pour commencer un logiciel"
tags: ["Software Craftmanship","méthodologie"]
author: "Bertrand Madet"
---

Par où commencer un logiciel ? Il y a plein de manière de répondre à cette question et cela dépend évidemment de beaucoup facteurs. Aujourd'hui nous parlerons de balle traçante et pourquoi commencer par l'interface utilisateur pour avoir le plus rapidement des retours sur un logiciel.

## Balles traçantes

Le concept de balle traçante (:gb: `tracer bullet`) apparaît dans le livre Pragmatic Programmer de David Thomas et Andrew Hunt (cf. [Fiche de lecture]({{< ref "post/fr/2022-03-23-pragmatic-programmer-lecture" >}})) - il s'agit l'astuce 20 `Use Tracer bullets to find Target`.

:teacher: Le principe s'appuie sur la comparaison avec les balles traçantes qui servent aux soldats de savoir où partent
leurs balles. Pour les munitions, une faible proportion est traçante et cela permet de voir où l'on tire sans sacrifier de l'efficacité - j'imagine que les balles traçantes sont moins puissantes que les balles normales.

L'exemple le plus connu de balle traçante en programmation est le fameux hello world qui permet de voir rapidement le langage et de valider la construction et l'exécution du de ce code minimaliste.

L'étape suivante est d'utiliser une autre balle traçante pour fournir un squelette de logiciel. On se base sur une fonctionnalité qui va être choisie pour lever les interrogations techniques. Le choix de la fonctionnalité va dépendre de la "couverture" des interrogations techniques et de l'utilité perçue. Donc ce n'est pas un choix qui incombe seul aux développeurs mais un choix d'équipe (en scrum, la décision appartiendrait au Product Owner). L'idée est de lever les incertitudes techniques le plus rapidement possible et évaluer ensuite au mieux la trajectoire du projet portant ce logiciel.

A la différence d'un prototype ou d'un preuve de concept (`PoC`), le code est destiné à être intégré dans le projet donc on développe comme d'habitude avec le même niveau d'exigence sur la qualité du code.

## Intégration continue et interface utilisateur

Dans l'implémentation de cette fonctionnalité "balle traçante", qui peut intégrer :

- l'arborescence du projet ;
- l'intégration continue ;
- les interfaces utilisateurs ;
- les traitements métier ;
- les stockages en base de données...

De mon point de vue, il faut commencer peut-être parallèlement par l'intégration continue (parce [l'intégration continue c'est important]({{< ref "post/fr/2022-06-14-continuous-integration">}})) et l'interface utilisateur. Quand je dis interface utilisateur, cela peut être un écran d'un client lourd, une page dans une application web ou même la ligne de commande.

L'intégration continue va vous permettre de vous assurer que vous allez pouvoir livrer quelque chose aux utilisateurs. L'interface utilisateur va vous permettre de concrétiser la promesse de la fonctionnalité. Cela simplifie les discussions autour des fonctionnalités métier et de valider ou d'invalider votre compréhension des besoins métiers. La combinaison des deux rassure sur la capacité à fournir une produit utile aux utilisateurs.

Dans un monde idéal, l'interface utilisateur aura été préparé par des spécialistes `UX` (**U**ser e**X**perience) et les questions sur les informations nécessaires au besoin des utilisateurs seront déjà validées mais malgré tout les discussions autour des traitements métier sera simplifié avec l'interface utilisateur comme média de discussion.

## Conclusion

Pour débuter un logiciel, on le voit quand c'est possible, il faut lever les incertitudes techniques ou métier le plus rapidement possible. Le but est de corriger dès que l'on s'éloigne de la création d'un logiciel qui s'éloigne des besoins utilisateurs. L'approche proposée me permet bien s'associer avec une démarche itérative et incrémentale.

Il y a des cas où les premières étapes ne seront pas l'intégration continue et l'interface utilisateur.  Dans le cas de la Data Science, le logiciel est un outil pour répondre à une question. Il faut évidemment s'assurer d'avoir une question, la compréhension et les données qui permettent de répondre à cette question.

Les balles traçantes peuvent être utiles à d'autres moments d'un projet comme la migration d'une base de données ou un changement de framework front.

Si vous avez une autre manière de débuter un logiciel, des questions ou des suggestions pour des billets futurs n'hésitez pas à m'en faire part sur [Twitter](https://twitter.com/trambi_78), ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
