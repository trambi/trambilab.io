---
title: "Serveless Development on AWS - Fiche de lecture"
date: "2024-12-21"
draft: false
description: "Livre de Sheen Brisals et Luke Hedger sur la création d'application Serverless"
tags: ["fiche de lecture","cloud"]
author: 'Bertrand Madet'
---

## A qui s'adresse ce livre

À ceux qui veulent développer des applications tirant pleinement parti des spécificités du Cloud ☁️ en général ou sur AWS en particulier.  ou à ceux qui veulent comprendre les spécificités du `Serverless`. 

## Ce qu'il faut retenir

Le concept `Serverless` consiste à confier les aspects "serveurs" (matériel, réseau) d'une application ou d'un service à un tier. Ce tier étant un fournisseur de ressources informatiques (ou 🇬🇧 `Cloud Provider`).

On imagine souvent le serverless comme des [AWS Lambda](https://aws.amazon.com/fr/lambda/) enregistrant des données dans des conteneurs [S3](https://aws.amazon.com/fr/s3/) ou des tables [DynamoDB](https://aws.amazon.com/fr/dynamodb/). Mais ce n'est souvent qu'un premier niveau et il existe beaucoup d'autres services sans serveur pour simplifier la mise en oeuvre d'une application.

Les caractéristiques des services Serverless : 

- Paiement 💰 à l'usage ;
- Passage à l'échelle automatique ↗️ ↘️;
- Haute disponibilité.

Pour bénéficier de ces caractéristiques, une application ou un service doit privilégier :

- une construction itérative et incrémentale ;
- une architecture orienté évènement ;
- des équipes pluridisciplinaires.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| 👍 Approche holistique du serverless| 🤏 Qui reprend le AWS Well-architectured Framework|
| 👍 On découvre des services autres que les lambdas |👎 Peu d'ouverture sur autre chose qu'AWS|

Le livre est très complet sur le développement Serverless. L'approche reprend les piliers du [AWS Well-architurected Framework](https://aws.amazon.com/fr/architecture/well-architected/). Les concepts sont illustrés par des cas d'usage, des schémas. Et cela permet de comprendre l'étendue des services sans serveur proposés par AWS et les gains potentiels à adhérer au serverless. 

On se laisse prendre au jeu et à chercher des améliorations possibles à des applications déjà existantes. Je regrette qu'il y ait peu (voire pas) d'ouverture sur d'autres fournisseurs de services. J'ai eu parfois l'impression d'être dans une plaquette commerciale d'AWS 😵‍💫. Même si AWS est, sans doute, le fournisseur de ressources distribuées le plus complet, d'autres fournisseurs ont peut-être des solutions innovantes.

Le livre montre aussi que la frontière entre les spécialistes de l'infrastructure et les spécialistes de l'applicatif s'estompent. J'avais pu le constater pour mon application serverless qu'une partie de la complexité applicative "glisse" vers l'infrastructure. Des parties de la logique applicative se retrouve dans l'enchaînement de services comme les [Steps Functions](https://aws.amazon.com/fr/step-functions/) ou les [EventBridge](https://docs.aws.amazon.com/eventbridge/) donc dans l'infrastructure.

## Détails

- Titre : Serveless Development on AWS - Building Enterprise-Scale Serverless Solutions
- Auteur : Sheen Brisals et Luke Hedger
- Langue : 🇬🇧
- ISBN : 978-1098141936