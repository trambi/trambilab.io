---
title: "Formatage de code, trucs et astuces"
date: "2021-10-12"
draft: false
description: "et pourquoi c'est important"
tags: ["méthodologie","code"]
author: "Bertrand Madet"
---

Aujourd'hui, je vous propose une discussion autour du formatage du code :art: : l'intérêt et les outils pour l'homogénéiser dans une équipe.

## C'est quoi le formatage

Le formatage c'est un ensemble de règles qui permettent d'écrire son code :

- les entêtes de fichiers ;
- le type de caractère utilisé pour l'indentation ;
- l'emplacement des accolades ;
- l'utilisation des espaces pour les affectations de variables;
- l'utilisation des espaces pour les appels de fonctions ...

Par exemple, les deux exemples ci-dessous ont des formatages différents.

```typescript
function myFunction(name:string)
{
    return `hello ${name}`;
}

for name in ['Jean','John']
{
    console.log(myFunction(name));
}
```

```typescript
function myFunction(name: string) {
  return `hello ${name}`;
}

for name in ['Jean', 'John']{
  console.log(myFunction(name));
}
```

## Pourquoi c'est important

Je n'ai jamais été très enthousiaste à propos des règles de formatage. Ce qui est important c'est la cohérence de ce formatage dans le temps. L'impact est flagrant pour inspecter des modifications lors d'une revue de code.

Vous préférez vérifier cette différence :

```diff
function myFunction(name: string) {
  return `hello ${name}`;
}

- for name in ['Jean', 'John']{
+ ['Jean', 'John'].foreach(() => {
  console.log(myFunction(name));
- }
+ });

```

ou celle-là ?

```diff
- function myFunction(name:string)
- {
-     return `hello ${name}`;
- }
-
- for name in ['Jean','John']
- {
-    console.log(myFunction(name));
- }
+ function myFunction(name: string) {
+   return `hello ${name}`;
+ }
+
+ ['Jean', 'John'].foreach(() => {
+  console.log(myFunction(name));
+ });
```

Sur un changement impliquant une petite base de code, c'est irritant. Sur une base de code plus conséquente cela devient à un réel obstacle à l'inspection.

## Outils à la rescousse

Pour garder la cohésion du formatage, il existe des outils. J'ai choisi de distinguer les outils suivant le fait d'être configurable ou non.

### Formateurs configurables

Les environnements intégrés de développement (ou IDE) permettent de configurer les règles de formatage de manière précise suivant le type de fichier (enfin en général suivant l'extension). Il est même possible de de partager la configuration des IDE avec un fichier de configuration et des modules: [EditorConfig (https://editorconfig.org/)](https://editorconfig.org/).

Il existe d'autres outils spécifiques au langage. Il reste alors à configurer le ou les outils et surtout insérer le fichier de configuration dans la gestion de version pour rester cohérent au niveau de l'équipe.

### Formateurs automatiques

Les formateurs automatiques ont zéro configuration ou une configuration minimale.

- Le go en langage moderne fournit un formateur intégré [gofmt (https://pkg.go.dev/cmd/gofmt)](https://pkg.go.dev/cmd/gofmt);
- [prettier (https://prettier.io/)](https://prettier.io/) fournit ce genre de service pour le développement front-end (`JavaScript`, `typescript`, `HTML` et `css`) ;
- Pour python, mon préféré [black (https://github.com/ambv/black)](https://github.com/ambv/black).

Je préfère travailler avec ces outils car on adopte une convention standard et cela évite de débattre sur les conventions.

### Autres outils

Les analyseurs statiques (ou *linter*) permettent aussi de repérer certains problèmes de formatage. On pourra citer:

- [Pylint (https://pylint.org/)](https://pylint.org/) ou [Flake8 (https://flake8.pycqa.org/en/latest/)](https://flake8.pycqa.org/en/latest/) pour python ;
- [ESLint (https://eslint.org/)](https://eslint.org/) pour JavaScript et TypeScript.

En général, il est possible de corriger automatiquement la plupart des problèmes.

## Conclusion

Il est évidemment possible de combiner les outils suivant les usages.

Le formatage, un petit pas pour les développeurs et un grand pas pour les revues de code. On voit encore l'importance d'adopter des standards. Si vous voulez partager vos expériences de développement et ce que vous en avez appris, partagez les à moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
