---
title: "FTPS en 3 phrases"
date: "2020-12-20"
description: "et quatre scrolls d'explication"
tags: ["ftps","protocole","3 phrases"]
author: "Bertrand Madet"
---

*Ce billet est une version largement remaniée d'un billet publié le 25 mars 2019 sur LinkedIn.*

J'ai travaillé pendant plus de trois ans sur des flux de données utilisant entre autre le protocole FTPS. Je résume mon expérience en 3 phrases :

1. FTPS, c'est du FTP avec les mêmes certificats que HTTPS.
1. Le mode passif est plus en accord avec nos usages réseaux actuels.
1. Il est nécessaire de configurer les clients pour utiliser la sécurité induite par le protocole.

Explications ... :teacher:

## FTPS, c'est du FTP avec les mêmes certificats que HTTPS.

`FTPS` est l'acronyme de **F**ile **T**ransfert **P**rotocol over **S**SL, c'est un protocole de transfert de fichier sécurisé dérivé du `FTP` - **F**ile **T**ransfert **P**rotocol.

:warning: le protocole `FTP` est un dinosaure : la première version des spécifications date de 1971, la version finale date de 1985 (à comparer à 1996 pour `HTTP`). Je vous rassure :

- il a connu des mises à jour notamment pour la sécurité ;
- il fonctionne efficacement pour transférer des fichiers.

Il est très facile de le confondre avec un autre de protocole de transfert de fichier sécurisé, le protocole `SFTP` - **S**ecure **F**ile **T**ransfert **P**rotocol.

Oui il faut faire attention en nommant des protocoles, sinon 20 ans après les gens vous maudissent encore.

Mon moyen mnémotechnique c'est : `FTPS`, ça finit par un S comme `HTTPS` (pas sûr qu'il soit super celui-là :wink:).

La sécurisation se fait par le protocole `TLS` (successeur de `SSL`). Le protocole `TLS` est aussi utilisé pour sécuriser `HTTPS`. `TLS` utilise des algorithmes cryptographiques [symétriques](https://fr.wikipedia.org/wiki/Cryptographie_sym%C3%A9trique) et [asymétriques](https://fr.wikipedia.org/wiki/Cryptographie_asym%C3%A9trique) pour garantir entre autre l'identité du serveur et le secret des échanges.

Il existe deux méthodes de sécurisation (implicite et explicite), seule la méthode explicite est encore utilisée aujourd'hui.

## Le mode passif est plus en accord avec nos usages réseaux actuels

Avec un regard actuel, les protocoles FTP et FTPS ont un comportement bizarre : ils utilisent deux ports. Et chose plus étrange encore, le serveur est à l'origine de connexions vers les clients.

```mermaid
sequenceDiagram
  participant clientcmd as Client FTPS partie commande
  participant clientdata as Client FTPS partie données (port X)
  participant servercmd as Serveur FTPS partie commande (port 21)
  participant serverdata as Serveur FTPS partie données (port 20)
  clientcmd->>servercmd: Bonjour, tu peux m'envoyer des données sur le port X
  servercmd->>clientcmd: Ok
  activate clientdata
  serverdata->>clientdata: Je viens pour la donnée
  clientdata->>serverdata: Ok
  loop Chaque fichier
  clientcmd->>servercmd: Peux-tu m'envoyer le fichier X
  servercmd->>clientcmd: Ok
  serverdata->>clientdata: Prend ce fichier, c'est le fichier X
  clientdata->>serverdata: Ok
  end
  deactivate clientdata
```

Comme l'indique le diagramme de séquence ci-dessus, un client va contacter le serveur sur le port 21 pour échanger des commandes (connexion de contrôle). Pour transférer un fichier, client et serveur vont convenir sur la connexion de contrôle d'un port à utiliser pour le transfert de fichier. Le client va ouvrir ce port et le serveur le contactera avec son port 20 pour transférer des fichiers (connexion de données).

C'est peut-être un détail pour vous mais pour les systèmes de protection réseau, comme les pare-feux, c'est l'enfer à gérer. Pourquoi mon serveur va se connecter sur un port exotique d'une machine inconnue ?

Avec le développement des pare-feux, le protocole a évolué en 1994 en proposant un mode dit passif, où le serveur ne prend pas de responsabilités de se connecter aux clients. L'inconvénient est que pour le serveur va ouvrir le port pour la connexion de contrôle et une plage de port pour les connexions de données, typiquement 40000-41000.

## Il est nécessaire de configurer les clients pour utiliser la sécurité induite par le protocole

Il existe plein de clients FTPS et je propose les commandes pour valider la configuration de votre serveur (oui avec la vérification des certificats et de leurs émetteurs sinon le FTPS ne sert à rien).

### cUrl

Il faut créer un fichier trusted.pem contenant la concaténation des certificats des autorités de certification et du serveur (au format `x509` encodés en base64).

```bash
curl --cacert trusted.pem --ftp-ssl --user login:motdepasse ftp://host:port
```

### winSCP

Il faut créer un fichier `cacert.pem` contenant la concaténation des certificats des autorités de certification et du serveur (x509 encodés en base64) dans le répertoire de l'exécutable WinSCP.

### Filezilla

Il n'y a pas de moyen pour faire confiance à une autorité de certification. Il faudra valider chaque première connexion avec un nouveau certificat en cliquant sur un bouton de l'interface.

## Conclusion

Voilà, j'espère que vous avez appris ou réappris quelque chose à la lecture de ce billet. On a souvent l'impression que c'est hyper compliqué et pas très utile de comprendre les protocoles. C'est vrai quand tout va bien.

En cas de problème, c'est toujours utile d'avoir une base pour comprendre et savoir où chercher.

Et vous, si vous deviez résumer un protocole, une technologie en trois phrases ? Quel sujet choisiriez-vous et surtout quelles phrases ? Discutons-en sur [Twitter](https://twitter.com/trambi_78) ou [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
