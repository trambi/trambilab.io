---
title: "Utilisation de Scala 2 en entreprise"
date: "2023-10-04"
draft: false
description: "avec SBT"
tags: ["scala","entreprise","java","outils"]
author: "Bertrand Madet"
---

Lors d'une test clinic récente, nous nous sommes retrouvés sur une problématique de test en [Scala (https://scala-lang.org/)](https://scala-lang.org/). Le problème ne se situait pas dans le code des tests mais dans notre capacité à lancer les tests. C'était même pire, nous étions incapable d'installer et de configurer les outils pour lancer les tests sur une version spécifique de Scala 2 en dehors de l'usine logicielle.  Oui la version 2 de Scala n'est pas la dernière version de Scala mais le code à tester était en Scala 2 et nous n'allions pas exiger de passer en Scala 3 avant de faire quoi que ce soit. Je vous propose un petit bilan de l'installation et la configuration de Scala 2.

**Ce billet a été corrigé avec une nouvelle version du 04 juin 2024, je vous conseille d'aller sur le [billet revu et corrigé du 04 juin 2024 : Utilisation de Scala 2 en entreprise]({{< ref "/post/fr/2024-06-04-use-scala-2-in-corporate-network-fixed" >}})**

## Installation Scala 2

L'installation de notre version de Scala 2 nécessitait l'installation préalable de Java 8 soit la version Oracle, soit la version OpenJDK. Cela parait bizarre qu'une version ancienne de JDK soit nécessaire (la version 21 de JDK est actuellement sortie). Et bien cela dépend de la version cible de Scala dont avez besoin et c'est très bien détaillé dans la page suivante : [JDK Compatibility (https://docs.scala-lang.org/overviews/jdk-compatibility/overview.html)](https://docs.scala-lang.org/overviews/jdk-compatibility/overview.html). Donc pour une version de Scala entre 2.11.0 et 2.11.11, le document indique que la version du JDK doit 8.

:warning: A ce que la variable d'environnement `JAVA_HOME` soit correctement définie.

Pour avoir la version précise que l'on souhaite, il faut :

1. se rendre sur la page [All available versions (https://www.scala-lang.org/download/all.html)](https://www.scala-lang.org/download/all.html) ;
2. sélectionner la version ;
3. sélectionner le support que l'on souhaite dans la page.

Pour être sûr que cela fonctionne, la commande :

```bash
scala -version
```

doit retourner la version attendue.

## Installation de SBT

[SBT (https://www.scala-sbt.org/)](https://www.scala-sbt.org/) signifie **S**cala **B**uild **T**ool et permet de gérer des tâches de développement comme les tests ou la compilation. On peut passer par la dernière version disponible de SBT (la 1.9.6 à l'heure où j'écris cet article), vu que SBT récupéra la version adéquate indiquée dans le fichier de description : `build.sbt`. Car SBT va aussi chercher les dépendances de votre programme.

## Configuration pour passer à travers un proxy

Pour que SBT aille récupérer les dépendances sur Internet, on peut passer par un proxy. Il faut définir les propriétés systèmes :

- `http.proxyHost` au nom d'hôte du proxy pour le trafic HTTP;
- `http.proxyPort` au numéro de port du proxy pour le trafic HTTP;
- `http.nonProxyHosts` avec les noms d'hôte qui ne passeront pas par le proxy séparés par le caractère `|`.

Il est possible de définir des réglages spécifiques pour les protocoles HTTPS ou FTP comme indiqué sur la [page sur l'utilisation des proxies (https://docs.oracle.com/javase/8/docs/technotes/guides/net/proxies.html)](https://docs.oracle.com/javase/8/docs/technotes/guides/net/proxies.html).

Cela permettra d'utiliser le protocole HTTP sans problème mais il y aura potentiellement des problèmes pour vérifier les certificats présentés pour les protocoles utilisant TLS (comme HTTPS). Cela dépend si le certificat présenté par un proxy est signé par une autorité de certification "bien connue". Si le certificat présenté n'est pas reconnu, il faut ajouter les autorités de certifications dans un magasin de confiance (`truststore` :gb:). On utilisera l'outil `keytool` avec une option pour indiquer le fichier (au format X509 PEM), le magasin de confiance, l'alias. Avec OpenJDK8, le magasin de confiance est dans `${JAVA_HOME}/jre/lib/security/cacerts`.

```bash
keytool -importcert -trustcacerts -file <chemin-certificat> -keystore ${JAVA_HOME}/jre/lib/security/cacerts -storepass <mot-de-passe> -alias <name>
```

> 🧐 par défaut le mot de passe du magasin de confiance est `changeit` !

Pour récupérer les certificats, on peut utiliser `openssl s_client -showcerts -connect <host>:<port>`.

Pour fixer les valeurs on peut utiliser la variable d'environnement `JAVA_OPTS`.

```bash
export JAVA_OPTS="-Dhttp.proxyHost=<hote-proxy> -Dhttp.proxyPort=<port-proxy> -Djavax.net.ssl.trustStorePassword=<mot-de-passe>"
```

## Configuration pour utiliser un gestionnaire d'artefact

Pour SBT aille récupérer les dépendances dans un gestionnaire d'artefact, Nexus ou Artifactory par exemple, il faut le configurer en ajoutant dans le répertoire racine de l'utilisateur un fichier `.sbt/repositories`.

```txt
[repositories]
local
my-ivy-proxy-releases: <gabarit-url-pour-ivy>
my-maven-proxy-releases: <gabarit-url-pour-maven>
```

Ensuite la problématique des certificats des autorités de certification signataires du gestionnaire d'artefact seront identiques à ceux du proxy.

Pour fixer les valeurs on peut utiliser la variable d'environnement `JAVA_OPTS`.

```bash
export JAVA_OPTS="-Djavax.net.ssl.trustStorePassword=<mot-de-passe>'
```

## Conclusion

La session de test clinic a été sportive et à la fin de la session nous n'avons pas réussi à lancer un test. On pourrait considérer cela comme un échec mais cette clinique a permis de mettre en évidence nos lacunes sur l'installation de cet environnement de développement. Cela a permis de regrouper les différents savoirs et de creuser sur les explications sur le fonctionnement des proxy, des certificats dans l'écosystème Java.

Si vous avez des retours ou d'autres conseils sur l'écosystème Java comme ce n'est pas mon domaine de prédilection, n'hésitez pas à les partager sur [X](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
