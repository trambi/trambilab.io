---
title: "Ce que vous devez savoir de la Cybersécurité"
date: "2021-03-28"
draft: false
description: "le minimum pour les développeurs"
tags: ["cybersec","base"]
author: "Bertrand Madet"
---

Nous vivons dans un monde où tout est connecté, et nos créations sont très souvent exposées à de nombreuses menaces dont nous (les développeuses et les développeurs) n'avons pas forcément idée. Voici le minimum (de mon point de vue) à connaître en sécurité de systèmes d'information ou en cybersécurité.

## Cybersécurité = gestion de risque

Avant de savoir comment se protéger, il faut savoir de quoi se protéger. Dans la gestion de risque classique, on va entrer dans un processus classique (qui peut paraître long) où va lister ce que l'on souhaite protéger et de quelles menaces. En fonction du risque et de l'impact de chaque menace, on cherche des contre-mesures.

```mermaid
graph LR
subgraph Cybersécurité
  assets[Biens à protéger]-->threats[Menaces]
  threats-->analyse{Analyse du risque et des impacts}
  analyse-->yes[Contre-mesures]
  analyse-->non[Pas de contre-mesures]
end
```

La cybersécurité est l'ensemble alors qu'on confond souvent avec les contre-mesures.

Heureusement aujourd'hui, il y a des listes de menaces disponibles sur Internet pour beaucoup de contextes. Par exemple pour les applications web, l'[OWASP (https://owasp.org)](https://owasp.org) fournit les menaces les plus fréquentes (et plein de choses d'autres choses).

## Les contre-mesures sont matérielles, logicielles, humaines et organisationnelles

Quand on pense aux contre-mesures, on pense souvent à des solutions logicielles, comme l'anti-virus ou matérielles comme un pare-feu. On oublie souvent l'aspect humain.

Par exemple, pour lutter contre les menaces de type infection d'un  parc par un virus, les logiciels anti-virus sont importants. Si les personnes ne sont pas sensibilisées et formées et que le rôle de mise à jour n'est pas identifié dans l'organisation, les virus risquent malgré tout de prospérer.

Il ne suffit pas d'empiler les solutions techniques pour "sécuriser" une application ou un service. Il faut organiser la mise en oeuvre.

## Utilisez les éléments de sécurité de référence

 Vous pouvez essayer d'implémenter les fonctions de sécurité vous-même pour comprendre comment cela fonctionne mais pas en production. Il y a mille et une manière de diminuer l'efficacité d'un algorithme et un détail peut affaiblir un élément de sécurité.

Si vous avez besoin :

- d'une fonction de hachage, utilisez celle de la famille `sha-2` ;
- d'un chiffrement symétrique, utilisez l'[AES (https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard)](https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard) ou le [TwoFish (https://fr.wikipedia.org/wiki/Twofish)](https://fr.wikipedia.org/wiki/Twofish) ;
- d'un chiffrement asymétrique, utilisez le [RSA (https://en.wikipedia.org/wiki/RSA_(cryptosystem))](https://en.wikipedia.org/wiki/RSA_(cryptosystem)) ou les courbes elliptiques.

 Utilisez aussi les implémentations de référence ou reconnues (elles ont subit des études de sécurité) et respectez les consignes d'utilisation.

## Conclusion

On le voit les problématiques sont assez complexes et heureusement il existe des standards. Mon conseil est à moins d'être un expert de cybersécurité d'utiliser les standards. En France, l'ANSSI (**A**gence **n**ationale de la **s**écurité des **s**ystèmes d'**i**nformation) est l'autorité de référence en terme de cybersécurité, son [site (https://www.ssi.gouv.fr/)](https://www.ssi.gouv.fr/) est bien fait et recèle d'informations.

Pour débuter, je vous conseille le MOOC de l'ANSSI : [SecNum Académie (https://secnumacademie.gouv.fr/)](https://secnumacademie.gouv.fr/).

Pour s'initier à la cybersécurité et prendre le rôle de l'attaquant en toute légalité, il y a des sites comme [RootMe (https://www.root-me.org/?lang=fr)](https://www.root-me.org/?lang=fr) qui propose des challenges de sécurité.

Ce billet se veut un point de départ, à vous de voir si vous avez envie d’approfondir. Si vous avez des retours sur la cybersécurité, n'hésitez pas à les partager sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
