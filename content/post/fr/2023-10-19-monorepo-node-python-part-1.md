---
title: "Mono-dépôt Node.js, Python - partie 1"
date: "2023-10-19"
draft: false
description: "Explications sur la structure"
tags: ["python","monorepository","javascript"]
author: "Bertrand Madet"
---

Dans mon projet actuel, je suis amené à travailler avec Terraform, Python et Node.js. Si vous suivez mes billets de blog, cela n'est pas une surprise. Au début du projet, nous avons décidé d'utiliser un mono-dépôt. J'étais content car j'avais utilisé un mono-dépôt avec une certaine satisfaction lors de mon dernier projet.

## Un peu de contexte

Le service sur lequel je travaille est constitué :

- d'une application web;
- d'une API REST ;
- d'une base de données,
- de traitement back-office.

Le tout est déployé sur AWS et utilisant des services AWS.

```mermaid
graph LR
  user((Utilisateurs))-->webapp[Application web]
  user-->aws([Services AWS])
  webapp-->|Consomme|frontend[API REST sous forme de Lambda]
  frontend-->db[(Base de données)]
  aws-->|déclenchent|backend[Lambdas ou tâches ECS]
  backend-->db
```

L'application web est en React.js en JavaScript, les lambdas constituant l'API REST utilisent Node.js et JavaScript. Les tâches de back-office sont en Python car elles doivent s'interfacer avec des bibliothèques Python. La description de l'infrastructure est réalisée avec Terraform.

## Mise en place

L'idée est de gérer tout ce qui permet de reconstruire le projet dans un dépôt Git. On l'a vu plus haut, il y a :

- le code :
  - JavaScript,
  - Python,
  - Terraform ;
- la documentation technique ;
- la documentation de domaine ;
- l'intégration continue ;
- la création des images de conteneur ;
- le déploiement continu.

:exploding_head:

### Organisation

Cela fait beaucoup d'éléments alors autant les structurer un peu. Ayant envie de tester autre chose que rush pour le "mono-dépôt JavaScript" et en me disant que j'allais peut-être utiliser l'aspect extensible de la solution, je suis parti avec [Nx (https://nx.dev/)](https://nx.dev/) pour gérer la partie JavaScript. J'ai pris comme modèle un projet Nx et j'ai ajouté ce dont j'avais besoin. Voici ce que à quoi ressemble l'arborescence du projet :

- `apps`, le dossier où sont les applications JavaScript :
  - `ui`, le dossier de l'application web contenant les sources, les tests,
  - `front`, le dossier des lambdas front contenant les sources, les tests et le Dockerfile pour créer le conteneur ;
- `apps-python`, le dossier où sont les applications Python :
  - `back1`, le dossier des lambdas d'ingestion de données contenant les sources, les tests, les dépendances et le Dockerfile pour créer le conteneur,
  - `back2`, le dossier des traitements statistiques contenant les sources, les tests, les dépendances et le Dockerfile pour créer le conteneur ;
- `ci`, le dossier contenant la description des taches d'intégration continue et de déploiements GitLab CI - les tests, la préparation du JavaScript, les créations d'images de conteneurs...
- `infrastructure`, le dossier contenant la description de l'infrastructure :
  - `code-independant-1`, le dossier contenant les fichiers de description de l'infrastructure indépendante du code - les S3, les rôles IAM, les VPC endpoints...
  - `code-independant-2`, le dossier contenant les fichiers de description de l'infrastructure dépendante de l'étape précédente - le load balancer vers les VPC endpoints d'API gateway,
  - `env`, le dossier contenant les fichiers donnant les valeurs des variables pour chaque environnement,
  - `services`, le dossier contenant les fichiers de description de l'infrastructure dépendante du code - l'API Gateway, les lambdas ...
- `.gitlab-ci.yml`, le fichier de description des étapes d'intégration continue GitLab CI, pointant vers les fichiers de `ci` ;
- `api-doc.openapi.json`, le fichier de description de l'API REST au format OpenAPI 3 utilisé par l'API Gateway ;
- `CONTRIBUTING.md`, le guide de contribution du projet ;
- `domain1.md`, le fichier de description du premier domaine au sens DDD du projet ;
- `package.json`, le fichier décrivant le projet JavaScript ;
- `package-lock.json`, le fichier décrivant les dépendances figées du projet JavaScript ;
- `README.md`, le fichier de description générale du projet.

Et je dois en oublier.

### Usage en JavaScript

Pour mes application JavaScript, j'utilise :

- npm pour :
  - installer un paquet "[paquet]" - `npm install [paquet]`,
  - mettre à jour un paquet "[paquet]" - `npm update [paquet]`,
  - vérifier les paquets obsolètes - `npm outdated`;
- Nx, pour :
  - exécuter les tests sur l'application "[appli]" - `npx nx test [appli]`,
  - servir le code de l'application ui - `npx nx serve ui`,
  - préparer le code de l'application ui à être distribué sous forme de fichiers - `npx nx build ui`.

Je trouve la gestion des dépendances, et en particulier les mises à jour des dépendances, plus simple qu'avec Rush.

### Usage en Python

Pour mes applications Python, c'est un peu plus artisanal puisque j'utilise un environnement virtuel Python [venv](https://docs.python.org/3/library/venv.html) pour chaque application. Cela permet d'installer les dépendances propres aux applications. L'inconvénient est que je dois activer l'environnement Python de l'application "[appli]" via `source apps-python/[appli]/Scripts/activate` pour :

- exécuter les tests (dans le répertoire `tests`) - `pytest apps-python/[appli]/Scripts/tests` ;
- lancer le script principal `python apps-python/[appli]/src/main.py`.

Cela ne pose pas de problème quand on fait des modifications sur une seule application mais cela est un peu fastidieux quand on doit effectuer des modifications sur les deux applications Python.

La fusion des fichiers de couvertures de code est effectuée avec [junitparser](https://pypi.org/project/junitparser/).

### Usage avec Terraform et Docker

L'usage avec Terraform et Docker est plus limité juste tester des tâches qui sont effectués par l'usine logicielle. Pour Terraform, cela consiste à vérifier l'exécution correcte de `apply` sur l'environnement de développement. Pour Docker, cela consiste principalement à construire l'image du conteneur - `docker build .` pour vérifier que le conteneur se construit bien.

## Conclusion

Voilà l'état du dépôt de projet, je suis encore impressionné par la possibilité de faire une modification transverse à tout le projet (comme une correction d'orthographe, ou un ajout de champ) en un commit.

Cela reste perfectible : j'ai commencé à ajouter relativement récemment la partie Python. Je pense que l'on peut améliorer la structure en utilisant :

- des bibliothèques JavaScript pour les éléments communs aux codes JavaScript;
- des bibliothèques Python pour les éléments communs aux codes Python ;
- des fichiers de configuration JSON pour les éléments communs aux codes Python et JavaScript.

Pour avoir une image plus complète de la couverture de code, il faudrait fusionner les couvertures de code JavaScript avec celle Python (peut-être avec junitparser). On pourrait aussi passer le code JavaScript en TypeScript pour avoir une vérification des types.

Si vous avez des retours ou des conseils sur la gestion d'un mono dépôt multi-langages, n'hésitez pas à les partager avec moi sur [X](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/). De mon côté, j'essayerai de rendre compte des évolutions dans notre processus de développement.
