---
title: "TypeScript en 3 phrases"
date: "2021-02-15"
draft: false
description: "Mon point de vue après un peu plus d'un an avec ce langage"
tags: ["front-end","typescript","3 phrases"]
author: "Bertrand Madet"
---

Cela fait un peu plus d'un an que je travaille en TypeScript. Je résume mon expérience en 3 phrases :

1. TypeScript est plus rassurant que JavaScript.
1. TypeScript complique parfois le code.
1. TypeScript reste lié à l'écosystème JavaScript : luxuriant et versatile.

Explications ... :teacher:

## TypeScript, c'est plus rassurant que JavaScript

JavaScript est le langage du web, il est interprété et exécuté par les navigateurs web. On peut donc considérer que c'est un langage avec un potentiel d'exécution énorme. Sa permissivité et son typage dynamique le rend assez intimidant pour les néophytes.

[TypeScript (https://www.typescriptlang.org/)](https://www.typescriptlang.org/) est une surcouche de JavaScript ajoutant des types. Cela permet de faire des vérifications très poussées de typage. La typage proposé par TypeScript est extrêmement puissant. On peut faire des unions de type ou des intersections de types.

On peut décrire un champ `status` qui peut prendre trois valeurs avec l'union.

```typescript
interface Response {
  text: String;
  status: 'Ok' | 'Ko' | 'Pending';
}
```

On peut décrire une type ou une interface comme une union de types, ce qui peut être utile pour définir des types d'actions.

```typescript
type UpdateValueAction = {
  action: 'updateValue';
  value: number;
};

type ResetAction = {
  action: 'reset';
};

type Action = UpdateValueAction | ResetAction;
```

Il y a aussi les types génériques (même si je ne m'en suis pas trop servi), de quoi donner un coup de vieux au C++ et même au Go.

Le TypeScript permet de rattraper des erreurs faciles à faire en JavaScript très tôt dans le cycle de développement. Pour avoir aussi développer un petit peu en JavaScript cette année, je trouve TypeScript très rassurant.

## Le TypeScript complique parfois le code

Il y a déjà des cas qui peuvent pénibles à cause de la vérification de type. Par exemple, dans les tests unitaires, le cas des variables qui peuvent être nulles mais qui sont explicitement testées comme non nulles.

Pour l'interaction avec les paquets tiers, cela peut poser des problèmes. La plupart du temps, il y a des paquets de types associés aux paquets JavaScript. Pour le gestionnaire de paquet `npm`, soit le paquet fournit la définition de typage TypeScript, soit un paquet est présent dans le groupe `@types` (lié au projet GitHub [GitHub DefinitelyTyped (https://github.com/DefinitelyTyped/DefinitelyTyped)](https://github.com/DefinitelyTyped/DefinitelyTyped)). 

Sinon cela devient compliqué, il faut utiliser `any` à tout bout de champ, soit redéfinir les types utilisés et les convertir avec le mot clé `as`.

```typescript 
// On force la conversion de var en MyType
const varInMyType = (var as unknown) as MyType
```

Le typage peut aussi ruiner l'expressivité de JavaScript. Là où un adepte de la programmation fonctionnelle définirait en JavaScript l'identité simplement : 

```JavaScript
const identity = x => x;
```

En TypeScript, on doit utiliser une fonction générique :

```TypeScript
function identity<T>(arg: T): T {
  return arg;
}
```

En fait en TypeScript, on peut aussi écrire :

```TypeScript
const identity = x => x;
```

Mais dans ce cas, il y aura une erreur TypeScript indiquant que le type de retour aura  implicitement le type `any` (et pas le type du paramètre d'entrée).

## TypeScript reste lié à l'écosystème JavaScript : luxuriant et versatile

TypeScript est une surcouche de JavaScript et il conserve l'écosystème luxuriant de JavaScript. En JavaScript, tout me fait penser à une jungle, il y a beaucoup de choix d'outils et la popularité et donc le suivi des bibliothèques sont très versatiles.

Côté moteur d'exécution, il y a bien évidement les moteurs d'exécution des navigateurs, mais aussi [node.js (https://nodejs.org)](https://nodejs.org) et [deno (https://deno.land/) - nouveau venu](https://deno.land/).

Côté gestionnaire de paquet, il y a un [npm.js (https://www.npmjs.com)](https://www.npmjs.com) et aussi [yarn (https://yarnpkg.com/)](https://yarnpkg.com/) et d'autres que je ne connais pas.

Pour la compilation, on peut utiliser TypeScript ou Babel ou utiliser le support natif de deno. 

Pour tous les types d'outillage (l'analyse statique, l'orchestrateur de tests), il y a le choix, ce qui est une preuve du dynamisme de l'écosystème. Mais cela ne facilite pas l'immersion pour quelqu'un qui n'avait pas touché à JavaScript depuis 2004. 

Bref, pour lancer un projet TypeScript, le développeur ou l'équipe de développement doit faire le choix de :

1. l'environnement d'exécution;
1. la compilation ;
1. l'orchestrateur de tests ;
1. le gestionnaire de paquet.

En Go, tout ces choix sont réglés en choisissant ... Go !

## Conclusion

TypeScript est une excellente passerelle pour embarquer dans l'univers du développement front-end car il permet d'avoir des repères dans l'écosystème mouvant de JavaScript. La légère perte d'expressivité est largement compensée de mon point de vue par la sécurité apportée.

Si j'ai commis des erreurs dans ce billet (c'est aussi tout à fait possible dans les autres billets mais j'avoue ne pas me sentir encore complètement à l'aise dans le front-end), n'hésitez pas à me le faire savoir (gentiment) sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

Si vous avez un ressenti différent concernant TypeScript, je serai ravi d'en discuter avec vous.