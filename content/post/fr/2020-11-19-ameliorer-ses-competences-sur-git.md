+++ 
title = "Améliorer ses compétences sur Git"
description = "Ca vous dirait de comprendre les commandes que vous avez apprises par coeur ?" 
tags = [ "git", "amélioration", "outils" ] 
date = "2020-11-19" 
author = "Bertrand Madet" 
+++

*Billet initialement publié sur mon compte LinkedIn le 18 juin 2020 et augmenté*


## Git ?

[Git](https://git-scm.com) est un logiciel de gestion de version distribué. Il a été inventé par Linus Torvald pour le développement du noyau Linux. Il offre une grande liberté de travail et de collaboration.

Je vous propose ces liens pour améliorer votre compréhension de Git et adapter son usage à vos fonctionnements personnels ou en équipe. C'est important, il y a même un [dessin XKCD sur Git](https://xkcd.com/1597/) :wink:.

Plus sérieusement, Git vous permet de donner un historique précis à votre travail. Et cet historique sera très utile à un autre développeur ou une autre développeuse - y compris vous dans trois mois. Donc c'est mieux de comprendre les commandes que l'on tape. 


## Livres

Pour ceux qui préfèrent lire, le manuel officiel Pro Git est disponible en de nombreuses langues dont [l'anglais :gb: ](https://git-scm.com/book/en/v2) et [le français :fr: ](https://git-scm.com/book/fr/v2). Les documents d'Atlassian sur Git sont des références, en particulier sur [les différents modes opératoires - workflows](https://www.atlassian.com/git/tutorials/comparing-workflows).

## Vidéos

Pour les adeptes des vidéos, le site officiel en propose [quelques unes](https://git-scm.com/videos). Si vous n'avez pas peur des trolls, les vidéos de @christopheporteneuve sont plus opérationnelles : [Meilleures pratiques Git & GitHub par Christophe Porteneuve](https://www.youtube.com/watch?v=GH7wJ9voKK4). Une autre vidéo en anglais cette fois sur l'art d'utiliser Git : [Git Fu Developing](https://www.youtube.com/watch?v=f-Br8cud2eI).

## Pratique 

Si vous voulez vous entraîner, il existe des bacs à sable pour s'exercer sans risque pour votre code. Mon préféré est [Learn Git Branch](https://learngitbranching.js.org/), il est trés didactique et ses niveaux (accessible en tapant 'levels' dans la console) permettent un parcours de presque trois heures en enchaînant des défis. Il y a aussi [Visualizing Git](https://git-school.github.io/visualizing-git/).

Pour progresser, il faut pratiquer aussi je vous recommande soit d'installer un serveur Git, soit d'utiliser [GitLab](https://gitlab.com), mon préféré, ou [GitHub](https://github.com) pour vos développements logiciels personnels. Si vous voulez savoir si vos messages de commit sont bons, vous avez les parfaits contre-exemples sur [What the commit](https://whatthecommit.com).

## Partage

Expliquer et discuter d'un concept ou d'une technologie est un bon moyen de questionner ses acquis. Nous avons mis en place avec des collègues des sessions d’entraînement à Git en se basant sur [Learn Git Branch](https://learngitbranching.js.org/) et un logiciel de visioconférence - [Jitsi](https://meet.jit.si) en l’occurrence parce que les logiciels libres c'est bien. J'ai vraiment aimé l'expérience : c'était un bon moyen de valider nos compétences, d’homogénéiser les compétences autour de Git et d'interagir avec ses collègues.

## Cheatsheet

Pour vous rappeler les commandes que vous n'aurez pas utilisé depuis longtemps, il y a les cheatsheets : 

 - [Git branch sur DevHints](https://devhints.io/git-branch)
 - [Git tricks sur DevHints](https://devhints.io/git-tricks)

## Une petite astuce pour finir

Les commentaires sont importants pour moi, j'aime bien avoir de commentaires sur plusieurs lignes. Il est possible de le faire simplement en enchaînant les options `-m`. 
Ainsi la commande : `
git commit -m "Le titre de mon commit" -m "Deuxième ligne avec plus de détails" -m "Troisième ligne avec plus de détails"`, permettra d'obtenir les commentaires suivants : 

```
Le titre de mon commit
Deuxième ligne avec plus de détails
Troisième ligne avec plus de détails
```

Et vous, avez-vous des sources d'information ou d'autres conseils sur Git ? Partagez les avec [moi sur Twitter](https://twitter.com/trambi_78).
