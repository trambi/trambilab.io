---
title: "Bilan au bout d'un an"
date: "2021-11-11"
draft: false
description: "Et joyeux anniversaire"
tags: ["blog"]
author: "Bertrand Madet"
---

J'ai lancé mon blog le 11 novembre 2020. Voici un petit bilan sur les publications et sur le site après une première année de publication.

## Chiffres

62 billets publiés dont :

- 56 inédits, 5 adaptions de précédentes publications et 1 traduction ;
- 13 fiches de lectures :book: ;
- 8 sur des projets perso.

Le tag le plus utilisé est [fiche de lecture]({{< ref "/tags/fiche-de-lecture">}}) (avec 13 billets) suivi de [base]({{< ref "/tags/base">}}) (avec 10 billets).

## Rythme

Pour l'instant, je publie un billet par semaine. J'ai ralenti par rapport au rythme du premier mois afin de garder du temps pour des projets personnels. L'idée est d'avoir un équilibre entre la nécessité de capitaliser régulièrement et de garder le plaisir d'écrire.

Si vous voulez lire un blog de quelqu'un qui publie un billet par jour, [Flavio Copes (https://flaviocopes.com/)](https://flaviocopes.com/) fait ça très bien.

Je trouve qu'il faut déjà une bonne discipline pour publier toute les semaines. Je n'ai pas encore trouvé le moment de la semaine où rédiger mon article : en général, l'idée arrive durant la semaine, et germe durant le week-end. Je n'ai pas encore réussi à me créer un "fond de roulement" de sujets, ce qui me permettrait d'être moins pressé pour publier des articles.

## Modifications du site

Le duo [Hugo](https://gohugo.io/) et [GhostWriter](https://github.com/roryg/ghostwriter) fonctionne très bien. Il a été amélioré avec Mermaid.js (je vous ai dit que [j'adorais Mermaid.js]({{< ref "/post/fr/2020-12-13-mermaid-js.md" >}}) :wink:) et de [KaTeX (https://katex.org/)](https://katex.org/) pour les schémas et les formules mathématiques.

Pour les sujets nécessitant des figures non réalisables avec Mermaid, j'utilise [Excalidraw (https://excalidraw.com/)](https://excalidraw.com/).

J'ai ajouté le partage par LinkedIn des billets, on peut donc partager sur Twitter et LinkedIn facilement les billets. Et j'ai ajouté un lien vers mon utilisateur LinkedIn. Cela me semblait logique dans la mesure où je publie des messages sur ces deux réseaux sociaux. Pour l'instant, j'ai eu peu de demande de billet sur des sujets.

J'ai ajouté un lien vers les billets ayant le tag `base` dans la barre de navigation car ces billets me semblent les plus importants.

## Petit regret

Le seul regret est encore que je n'ai aucune idée de la fréquentation de mes articles. Cela reste un petit regret car : 

1. je suis le premier utilisateur de mon blog : Il m'arrive très souvent de relire un des billets pour retrouver une commande ou une option que j'aurai oublié;
1. j'ai une petite idée de l'intérêt d'un sujet de billet par rapport aux autres avec les réactions LinkedIn et Twitter.

## Conclusion

Hugo et le flux de publication avec GitLab fonctionnent de manière fluide. L'outil du site est stable et j'ai réussi à stabiliser le rythme de publication. Je suis content de m'être lancé car cela permet de prendre conscience du nombre et de la variété des expériences sur une année.

J'aime bien avoir des retours sur mes billets, cela me permet d'améliorer certains passages ou d'ajouter des précisions. Si vous avez des préférences concernant mes billets passés ou futurs n'hésitez pas à m'en faire part sur [Twitter](https://twitter.com/trambi_78) ou [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
