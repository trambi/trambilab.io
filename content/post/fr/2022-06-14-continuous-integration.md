---
title: "L'importance de l'intégration continue"
date: "2022-06-14"
draft: false
description: "Illustrée via un exercice de pensée"
tags: ["test","base","CI","Software Craftmanship"]
author: "Bertrand Madet"
---

Le concept d'intégration continue est apparu dans les années 90, il a été repris par la méthodologie agile XP (e**X**treme **P**rogramming) puis accéléré avec l'apparition d'outils d'intégration très performant. L'intégration continue consiste à effectuer de manière continue (à chaque soumission) la construction et les tests. Martin Fowler explique cela très bien dans [son billet sur l'intégration continue (https://martinfowler.com/articles/continuousIntegration.html)](https://martinfowler.com/articles/continuousIntegration.html).

Afin d'illustrer l'importance de l'intégration continue, je vous propose de réfléchir à une alternative : Est-il plus utile d'avoir beaucoup de tests sans déclenchement automatique ou un test déclenché à chaque commit.

Avant de nous livrer à l'analyse de deux situations, nous allons fixer le contexte de l'expérience théorique, le projet de nos hypothèses est de taille moyenne - une cinquantaine de fichiers - développé par quatre personnes.

*Edition le 18 juin 2022 : correction de la date et d'une phrase de l'hypothèse 2.*

## Hypothèse 1 : Des tests sans déclenchement automatique

Nous avons donc un projet de taille moyenne avec une couverture de code honorable (80%). Ce qui signifie qu'il y a au moins une quarantaine de tests unitaires.

Chaque commit peut casser un test ou plusieurs tests. Il faut penser à lancer les tests fréquemment. L'équipe de développement doit avoir une grande discipline collective pour garder les tests qui passent.

Sinon on entre dans le syndrome de la fenêtre cassée - décrit dans the Pragmatic Programmer (cf. [Fiche de lecture]({{< ref "/post/fr/2022-03-23-pragmatic-programmer-lecture.md" >}})). Pour illustrer ce syndrome, imaginez que vous reveniez de vacances, vous commencez à développer une nouvelle fonctionnalité et vous avez décidé d'utiliser le [TDD - cf. le TDD en 3 phrases]({{< ref "/post/fr/2021-06-15-tdd-en-3-phrases.md" >}}). Vous lancez les tests ...

Et là, c'est le drame, quinze tests en erreur ! :exploding_head: :warning: Il faut évidemment commencer par corriger les tests ou le code lié. Vous avouerez que cela casse le moral pour un retour de vacances. Pire cela peut casser la dynamique de tests.

## Hypothèse 2 : Un test déclenché à chaque soumission au dépôt distant

Nous avons donc un projet de taille moyenne avec un test unitaire qui est lancé à chaque soumission au dépôt distant. L'étape de passage des tests est obligatoire pour la soumission dans la branche principale.

La surprise en retour de vacances est rendue impossible par l'obligation de passer par les tests pour être dans la branche principale. Même en admettant que le test casse, la tâche ne sera pas trop ardue.

De mon point de vue, c'est un bon point de départ pour augmenter sa confiance dans son projet. L'équipe peut commencer à développer en TDD ou d'ajouter des tests unitaires sur des parties périlleuses à son rythme.

## Bonus

En admettant que vous ne disposiez pas d'un serveur d'intégration continue, vous pouvez utiliser les "git hooks" (:fr: crochets git) pour lancer les tester avant chaque soumission au serveur distant.

En attendant la mise en place de l'intégration continue, je vous invite à lire [la documentation officielle Git sur les crochets Git(https://git-scm.com/book/fr/v2/Personnalisation-de-Git-Crochets-Git)](https://git-scm.com/book/fr/v2/Personnalisation-de-Git-Crochets-Git).

Cela éviterait les soumissions au serveur distant qui font échouer les tests.

## Conclusion

J'espère que cet exercice de pensée - honnête mais totalement subjectif - aura illustré l'importance de l'intégration continue.

Si vous avez des questions ou des expériences sur l'intégration continue, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
