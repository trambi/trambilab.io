---
title: "Déployer une lambda avec une archive zip"
date: "2023-05-02"
draft: false
description: "et sans SAM"
tags: ["nodejs","python","cloud","lambda"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).

-----

Ces derniers temps, je travaille beaucoup sur les lambdas. Après le déploiement avec des images de conteneur, je vous propose dans ce billet de nous intéresser au déploiement via des archives zip. En règle générale, l'archive de la lambda doit contenir tout ce à quoi la fonction va accéder. AWS propose un framework pour construire des lambdas :  [SAM - **S**erverless **A**pplication **M**odel](https://docs.aws.amazon.com/serverless-application-model/index.html) mais pour comprendre la mécanique et l'utilité du framework, on va s'en passer dans ce billet.

Regardons cela par complexité croissante.

## La lambda utilise des bibliothèques standards et AWS

Si votre lambda utilise uniquement les bibliothèques standards et AWS, il faut créer une archive zip à partir des différences fichiers sources :

- Pour Python, cela signifie les bibliothèques listées dans [cette page (https://docs.python.org/3/library/index.html)](https://docs.python.org/3/library/index.html) et [boto3 (https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html) ;
- Pour JavaScript, cela signifie les bibliothèques standards de la version de Node.js que vous utilisez, listées dans [cette page (https://nodejs.org/api/)](https://nodejs.org/api/) et toutes les bibliothèques du scope `@aws-sdk`.

C'est assez simple. Pour décrire l'archive zip, on peut utiliser la ressource data "archive" qui décrit une archive zip.

## La lambda utilise des bibliothèques non standard

Si votre lambda utilise une bibliothèque non standard comme [requests (https://pypi.org/project/requests/)](https://pypi.org/project/requests/) en python ou [pg https://www.npmjs.com/package/pg](https://www.npmjs.com/package/pg) en JavaScript, il faut les embarquer dans votre archive et aussi les dépendances.

```mermaid
graph
  function[Code de la fonction]-->dependencies[Dépendances]
  dependencies-->secondDependencies[Dépendances des dépendances]
```

Le *modus operandi* est décrit dans la documentation AWS : [Python - https://docs.aws.amazon.com/lambda/latest/dg/python-package.html#python-package-create-package-with-dependency](https://docs.aws.amazon.com/lambda/latest/dg/python-package.html#python-package-create-package-with-dependency) et pour [Node.js - https://docs.aws.amazon.com/lambda/latest/dg/nodejs-package.html#nodejs-package-dependencies](https://docs.aws.amazon.com/lambda/latest/dg/nodejs-package.html#nodejs-package-dependencies).

### Python sans exécutable natif

Pour Python, on utilise l'option `target` de pip pour installer les bibliothèques dans un répertoire `package`.

```bash
pip install -r requirements.txt --target package
```

Ensuite on va ajouter le répertoire `package` dans l'archive zip.

### JavaScript sans exécutable natif

Pour JavaScript, il y a la méthode AWS qui est d'utiliser `npm` puis d'ajouter le répertoire `node_modules` dans l'archive zip. Personnellement, j'ai utilisé [esbuild (https://esbuild.github.io)](https://esbuild.github.io), pour créer un fichier `index.cjs` et avoir la possibilité d'utiliser l'éditeur en ligne lors de mes explorations. Pour `pg`, j'ai dû exclure `pg-native` (c'était un exécutable natif et je ne m'en servais pas).

## La lambda utilise des bibliothèques non standard avec des exécutables natifs

Là on entre dans le dur mais il faut s'accrocher :climbing:. Si votre fonction a besoin de bibliothèques qui ont elles-mêmes besoin d'exécutables natifs, vous devez ajouter les exécutables natifs ... correspondant à l'environnement d'exécution d'AWS Lambda - un Linux soit `x64` soit `arm64`. Donc si vous êtes sous Windows ou Mac, vous allez devoir télécharger (version chanceuse) ou compiler (version malchanceuse) l'exécutable natif pour Linux.

```mermaid
graph
  function[Code de la fonction]-->dependancies[Dépendances]
  dependancies-->secondDependancies[Dépendances des dépendances]
  dependancies-->nativesBinaries[Binaires natifs]
  nativesBinaries-.->nativesDependencies[Dépendances natives]
```

Si on ajoute les problématiques éventuelles de dépendances des binaires natifs, j'ai personnellement envie d'utiliser les conteneurs et les images de références AWS pour faire une partie du travail. Pour Python et psycopg2, on peut utiliser le Dockerfile ci-dessous :

### Python et psycopg2

```Dockerfile
FROM public.ecr.aws/lambda/python:3.10
# Le nécessaire pour compiler psycopg2
RUN yum install -y gcc python3-devel postgresql-static 
# On installe psypog2 dans le répertoire package
RUN pip install --target package psycopg2
```

Et copier le répertoire `/var/task/package` de l'image du conteneur vers l'archive zip. :muscle: Cela peut paraître démesuré d'utiliser un conteneur mais c'est la manière la plus fiable qui m'est venu en tête. On pourra aussi se servir de l'image résultante pour lancer des tests sans ou même avec base de données.

## Conclusion

Au vu de la complexité, je comprends mieux (et j'espère que vous aussi) l'intérêt d'utiliser SAM pour gérer les déploiements de Lambda. De même l'utilisation des images de conteneurs prend du sens dans les cas où il y a des dépendances complexes à mettre en oeuvre.

Les services "serverless" permettent de s'abstraire des serveurs au moment de l'exécution du code mais au moment du développement, on est souvent amené à s'intéresser à des détails typiquement "serveurs" comme les systèmes d'exploitation, les dépendances binaires de certains paquets.

Une autre manière de gérer les dépendances pourraient être d'utiliser les `layers`, nous aurons peut-être l'occasion de voir dans un autre billet.

Si vous avez des conseils, des expériences sur les AWS Lambdas, partagez les avec moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

-----

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).
