---
title: "Working effectively with legacy code - Fiche de lecture"
date: "2023-05-09"
draft: false
description: "Livre de Michael C. Feathers sur le code legacy"
tags: ["fiche de lecture", "Software Craftmanship","test","code"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Si vous devez modifier un logiciel et que chaque modification vous paraît compliqué, vous travaillez peut-être sur du `legacy code`.Ce livre va apportera une palette de stratégie et de techniques pour travailler sur ce logiciel :

- ajouter une fonctionnalité ;
- corriger un bug :bug: ;
- améliorer les performances ;
- modifier l'architecture du logiciel.

## Ce qu'il faut retenir

- Le `legacy code` c'est du code sans test ;
- Le travail devrait souvent débuter par la mise en place d'un filet de sécurité de tests ;
- Les dépendances sont des freins importants pour une telle mise en place ;
- Il existe plein de techniques pour casser les dépendances.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Très technique, les explications sont très précises | :-1: Très orienté Java, C++, C#|
| :+1: Les chapitres sont structurés pour permettre la lecture en fonction de la difficulté rencontrée | :-1: Pas forcément fait pour être lu entièrement|

Ce livre date de vingt ans et cela se sent un peu dans le choix de langages et dans la style des langages. Mais l'essentiel n'est pas là, travailler sur du code legacy n'est pas facile, l'idée est :

- de fournir un recueil de conseils pratiques et détaillés pour travailler dans ce contexte ;
- d'avoir les stratégies et les tactiques pour continuer à avancer.

L'ouvrage est donc à garder si vous avez la malchance de travailler avec du code sans test . Il me semble plus moins intéressant que `The Software Craftsman: Professionalism, Pragmatism, Pride` (cf. [fiche de lecture]({{< ref "/post/fr/2020-11-11-craftman-software-lecture.md" >}})) de Sandro Mancuso, `Clean Code` (cf. [fiche de lecture]({{< ref "/post/fr/2020-11-16-clean-code-lecture.md" >}}))), `Clean Architecture` (cf. [fiche de lecture]({{< ref "/post/fr/2020-12-23-clean-architecture-lecture.md" >}})) ou `The Clean Coder` (cf. [fiche de lecture]({{< ref "/post/fr/2021-03-21-the-clean-coder-lecture.md" >}})) de Robert C. Martin.

## Détails

- Titre : Working Effectively with legacy code
- Auteur : Michael C. Feathers
- Langue : :gb:
- ISBN : 978-0-13-117705-5
