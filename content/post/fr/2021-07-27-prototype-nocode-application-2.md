---
title: "Coder un prototype une application pour faire du nocode - partie 2"
date: "2021-07-27"
draft: false
description: "ou comment faire du map/reduce async et await"
tags: ["projet perso","javascript","nocode","async"]
author: "Bertrand Madet"
---

Comme je vous l'ai expliqué dans mon [précédent billet]({{< ref "/post/fr/2021-07-22-prototype-nocode-application-1.md" >}}), un collègue et moi avons décidé de prototyper une application `no code`. Le prototype fonctionnait, il y avait même des tests unitaires. Il ne restait plus qu'à coder un bloc qui appelait une API REST plutôt que de retourner un résultat statique. L'API moderne de requête Web est [fetch (https://developer.mozilla.org/fr/docs/Web/API/WindowOrWorkerGlobalScope/fetch)](https://developer.mozilla.org/fr/docs/Web/API/WindowOrWorkerGlobalScope/fetch) et fetch est asynchrone retourne une promesse.

## Asynchrone et promesse

Pour traiter une opération longue, il y a deux manières de procéder :

- synchrone - l'appel à la fonction bloque l'exécution du processus ;
- asynchrone - l'appel à la fonction laisse le processus continuer et à la fin de la fonction, une fonction de retour est appelée.

L'exécution de JavaScript est principalement mono-processus donc le mode asynchrone est privilégié pour la plupart des API JavaScript.

La [promesse (https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Using_promises)](https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Using_promises) est une construction de JavaScript pour sortir de l'enfer des fonctions de retour (ou :gb: callbacks' hell). L'opérateur `await` et la déclaration de fonction asynchrone `async` permettent de rendre plus intelligible l'utilisation des promesses.

## Transformation du traitement synchrone en traitement asynchrone

La méthode d'exécution de traitement initial
```javascript
resolveInput(){
    const resolvedInput = {};
    this.inputObjects.forEach(inputObject=>{
        const result = inputObject.resolveInput();
        this.mappings.filter(mapping => mapping.source === inputObject)
            .forEach(mapping => {
                resolvedInput[mapping.inputPort] = result[mapping.outputPort];
            })
    });
    return this.execute && this.execute(resolvedInput);
}
```

Comme la méthode `execute` doit retourner une promesse et que du coup la méthode `resolveInput` doit retourner une promesse.  Comment gère-t-on les méthodes sur les tableaux avec des fonctions asynchrones ?

On peut se dire qu'il suffit juste d'ajouter `await` à l'affection de result.

```javascript
async resolveInput(){
    const resolvedInput = {};
    this.inputObjects.forEach(async inputObject=>{
        const result = await inputObject.resolveInput();
        this.mappings.filter(mapping => mapping.source === inputObject)
            .forEach(mapping => {
                resolvedInput[mapping.inputPort] = result[mapping.outputPort];
            })
    });
    return this.execute && this.execute(resolvedInput);
}
```

Mais cela ne fonctionne pas car la fonction appelée de `forEach` est asynchrone et appelle `execute` avant les retours de la fonction asynchrone. C'est possible de résoudre le problème cf. [JavaScript: async/await with forEach (https://codeburst.io/javascript-async-await-with-foreach-b6ba62bbf404)](https://codeburst.io/javascript-async-await-with-foreach-b6ba62bbf404).

J'ai utilisé la fonction `map`, il est possible d'utiliser `Promise.all`

```javascript
const result = await Promise.all(this.inputObjects.map(fonction_asynchrone));
```

ou d'utiliser la sortie de map avec `reduce`. Il y a alors des subtilités, il faut que la fonction de réduction soit asynchrone donc il faut prendre en compte que l'accumulateur une promesse.

```javascript
const sum = myArray.map(fonction_asynchrone)
    .resolve(async (acc, value) => {
        return (await acc + await value);
    }, Promise.resolve(0));//ou },0);
```

Je préfère `Promise.all` combinée avec `await` car c'est juste plus lisible.

## Conclusion

L'aspect asynchrone m'a un peu compliqué la tâche mais m'a permis d'explorer le mécanisme `async/await` et son interaction avec `forEach`, `map` et `reduce`.

Si vous voulez partager vos projets personnels et les enseignements que vous en avez tiré, partagez les à moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
