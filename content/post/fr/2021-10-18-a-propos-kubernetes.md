---
title: "A propos de Kubernetes"
date: "2021-10-18"
draft: false
description: "suite à une formation"
tags: ["devops","formation","container","kubernetes"]
author: "Bertrand Madet"
---

La semaine dernière, j'ai suivi une formation de 3 jours sur Kubernetes pour développeur. Je profite de cette occasion pour essayer de formaliser ce que je sais sur Kubernetes.

## Ma petite expérience de Kubernetes

Il y a trois ans, mon ancien projet, nous avons souhaité migrer d'une solution d'orchestration de conteneur qui n'était plus maintenu ([Fleet de coreOS - https://github.com/coreos/fleet](https://github.com/coreos/fleet)). Nous étions sur Azure et n'étant pas expérimentés dans la gestion d'un cluster Kubernetes, nous avons opté pour la solution gérée par Azure (Azure Kubernetes Service). Nous avons essentiellement utilisé des tâches exécutés périodiquement (`CronJob`) et des services déployés sur le long terme (`Deployment`). Depuis, j'ai changé d'équipe et nous utilisons l'orchestrateur Docker Swarm (principalement parce que Kubernetes n'est pas disponible sur la plateforme que nous utilisons). Donc mes connaissances sont très orientées portage d'un existant et sans doute obsolètes.

## Rappel - Kubernetes

Kubernetes est un ensemble d'abstraction et d'API qui permettent de gérer l'exécution de manière distribuée de tâches sous forme conteneurisées. C'est un orchestrateur de conteneur très puissant.

Si le fait d'utiliser des conteneurs permet de régler les problèmes d'empaquetages des exécutables et du célèbre syndrome, `ça marche sur ma machine`. L'utilisation amène des fonctionnalités potentielles et de nouveaux problèmes qu'un orchestrateur permet de régler.

L'unité d'exécution minimale de Kubernetes est un `Pod`, **pas un conteneur**, c'est une abstraction de conteneur avec des fonctionnalités supplémentaires pour permettre à Kubernetes de gérer le cycle de vie de cette unité d'exécution. Sous le capot, il y a un moteur d'exécution de conteneur au choix de l'administrateur du cluster Kubernetes.

## Premier enseignement : ce n'est pas aussi compliqué qu'il n'y paraît

Après ma première expérience avec Kubernetes, j'avais l'impression qu'il y avait de (trop) nombreux degrés d'abstraction :

- `Pod` - abstraction d'un conteneur et d'outils associés ;
- `Job` - tâche basée sur un pod ;
- `CronJob` - job planifié ;
- `ReplicaSet` - ensemble de pod redondés sur le cluster ;
- `Deployment` - déploiement de replicaset prenant compte de la version des pods déployés ;
- `Statefulset` ;
- `IngressControler` ;
- `PersistantVolume` ;
- `PersistentVolumeClaim` ;
- `Service` ...

Ces notions (et bien d'autres encore) existent évidemment, mais le plus important à retenir est qu'il y a :

- `Pod` - unité d'exécution minimale de Kubernetes ;
- Les contrôleurs qui permettent définir les comportements de pod ou d'autres contrôleurs sur le cluster - les contrôleurs comprennent les `Job`, `CronJob`, `ReplicaSet`, `Deployment`, `Statefulset`, `IngressControler`...
- Les services - définissent l'accès réseau à un ensemble de pods.

## Deuxième enseignement - les labels sont essentiels

Le deuxième point qui m'est apparu avec cette formation est que les labels ont une dimension très importante dans Kubernetes. Ils permettent de sélectionner des objets, d'associer des notions les une aux autres.

Par exemple un service `Service` pour offrir est associé à un ensemble de conteneurs répliqués `Replicatset` grâce aux labels.

Au point qu'il a des labels conseillés pour tous les objets (:gb: [https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/](https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/)). Ainsi, il est conseillé d'avoir les labels suivants :

|label|description| exemple|
|---|---|---|
|app.kubernetes.io/name|Le nom de l'application|mysql|
|app.kubernetes.io/instance|Un nom unique identifiant l'instance de l'application|mysql-abcxzy|
|app.kubernetes.io/version|La version courante de l'application|5.7.21|
|app.kubernetes.io/component|Le composant dans l'architecture|database|
|app.kubernetes.io/part-of|Le nom d'application de haut niveau dont la ressource fait partie|wordpress|
|app.kubernetes.io/managed-by|L'outil utilisé pour exploiter l'application|helm|
|app.kubernetes.io/created-by|Le contrôleur ou l'utilisateur qui a créé la ressource|controller-manager|

## Bonus

Une fois que l'on a pu voir les bases, on peut constate la puissance des contrôleurs haut niveau qu'on utilisera la plupart du temps. Ainsi en dehors d'expérimentation, on va déployer des contrôleurs comme des `Deployment` ou des `StatefulSet` voire `CronJob`.

Un contrôleur `Deployment` permet l'exécution à l'échelle et la mise à jour des versions sans interruption de service. C'est une implémentation clairement supérieur à un replicaset (car basé sur un replicaset) ou d'un pod.

## Conclusion

Voici la fin de cette synthèse sur Kubernetes, j'espère que cela vous aura permis de clarifier vos connaissances sur le sujet. Et surtout que cela vous aura donner envie d'en apprendre plus et d'essayer.

Si vous avez des retours sur Kubernetes ou d'autres outils de déploiement, n'hésitez pas à les partager sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
