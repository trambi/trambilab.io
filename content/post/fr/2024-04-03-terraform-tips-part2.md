---
title: "Astuces sur Terraform - 2"
date: "2024-04-03"
draft: false
description: "pour refactorer sans tout casser"
tags: ["cloud","outils","terraform"]
author: "Bertrand Madet"
---

J'avais fourni une [première fournée d'astuces sur Terraform]({{< ref "/post/fr/2023-09-13-terraform-tips-part1.md">}}), destinées à se sortir de problèmes d'exploitation comme le verrouillage d'état ou la prise de contrôle de ressources déjà existantes. [Terraform (https://www.terraform.io/)](https://www.terraform.io/) est un outil de gestion d'infrastructure à partir d'une description de celle-ci.

La description peut réaliser avec un langage spécifique `HCL` (**H**ashi**C**orp **L**anguage) ou avec d'autres langages avec [cdktf (https://developer.hashicorp.com/terraform/cdktf)](https://developer.hashicorp.com/terraform/cdktf). Je vous propose de partager deux astuces supplémentaires sur l'utilisation de Terraform en `HCL`.  

## Renommer une ressource

Si on change le nom d'une ressource dans la description d'infrastructure, Terraform va, en général, détruire la ressource avec l'ancien nom et créer une nouvelle ressource avec le nouveau nom. Parfois, ce n'est pas ce que l'on souhaite et on veut "juste" renommer une ressource pour qu'elle fasse plus sens dans la description de l'infrastructure. Après tout, si on le traite l'infrastructure comme du code, il faut s'autoriser des *refactorings* comme pour du code.

Le langage de Terraform `HCL` fournit cette possibilité à partir de la version 1.1, avec un bloc `moved`.

```hcl
module "nouveau_nom" {
  # valeurs des paramètres du modules
}

moved {
  from = module.ancien_nom
  to   = module.nouveau_nom
}
```

Cette opération permet de planifier les interventions sur l'infrastructure comme si l'état du module "nouveau_nom" était celui de l'état du module "ancien_nom". Cela s'applique aux ressources et aux modules.

Il est aussi possible de le faire en ligne de  commande `terraform state mv module.ancien_nom module.nouveau_nom`. Mais cela s'intègre moins bien dans les étapes de CI/CD.

Concrètement cela peut servir à renommer des noms de ressources qui ont des dépendances sur des ressources décrites à une autre étape de description d'infrastructure.

:warning: Si vous utilisez plusieurs environnements de déploiement, il faut être sûr d'avoir déployé l'infrastructure avec le nouveau nom sur tous les environnements avant de supprimer le bloc `moved`.

Il y a plus de détails dans la [documentation sur les refactorings des modules (https://developer.hashicorp.com/terraform/language/modules/develop/refactoring#moved-block-syntax)](https://developer.hashicorp.com/terraform/language/modules/develop/refactoring#moved-block-syntax).

## Utiliser un alias sur un fournisseur

Il est possible de définir un alias pour une configuration de spécifique de fournisseur avec l'argument `alias`. Le fournisseur sans argument `alias` est le fournisseur par défaut.

```hcl
provider "aws" {
  region = "eu-west-3"
}

provider "aws" {
  alias  = "eire"
  region = "eu-west-1"
}
```

Dans le code ci-dessus le fournisseur aws par défaut est celui qui a la région AWS `eu-west-3` (Paris 🗼), celui nommé `aws.eire` aura la région AWS `eu-west-1` (Irlande ☘️). Pour utiliser l'alias, il faut utiliser le nom du fournisseur suivi d'un point suivi de l'alias dans le champ `provider` de la ressource à décrire.

Alors c'est bien beau, mais à quoi cela peut-il servir ? 🤨

Cela peut servir à décrire une ressource AWS, par exemple un bucket S3 AWS, dans une autre région que le fournisseur par défaut. Car aussi surprenant que cela puisse paraître les ressources aws ont très rarement n'ont pas d'argument `region`. Dans ce cas, on va définir un fournisseur alternatif avec la région cible et l'utiliser comme fournisseur pour les ressources concernées.

```hcl
resource "aws_s3_bucket" "backup" {
  bucket = "my-tf-test-bucket"
  provider = aws.eire
  # reste des arguments
}
```

Dans cet exemple, le bucket S3 utilisera le fournisseur AWS dont l'alias est `eire`.

Plus d'information sur les [alias de provider (https://developer.hashicorp.com/terraform/language/providers/configuration#alias-multiple-provider-configurations)](https://developer.hashicorp.com/terraform/language/providers/configuration#alias-multiple-provider-configurations).

## Conclusion

J'espère que ces deux astuces permettront de décrire au mieux vos infrastructures. Il n'y a pas de surprise, tout est décrit dans la documentation. Comme la documentation est conséquence, il faut, de temps en temps, chercher des fonctionnalités dedans ou par l'intermédiaire de [Stack Overflow (https://stackoverflow.com/questions/tagged/terraform)](https://stackoverflow.com/questions/tagged/terraform).

Si vous avez des retours ou des conseils sur l'utilisation de Terraform, n'hésitez pas à les partager avec moi sur [X](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
