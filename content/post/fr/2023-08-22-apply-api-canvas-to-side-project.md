---
title: "Utiliser les conseils de Design of Web APIs"
date: "2023-08-22"
draft: false
description: "sur un projet perso"
tags: ["REST API", "projet perso"]
author: "Bertrand Madet"
---

La lecture de Design of Web APIs ([fiche de lecture]({{< ref "/post/fr/2023-08-01-the-design-of-web-apis-lecture" >}})) m'a donné envie d'appliquer le canvas proposé dans le livre. Je vous propose ce parcours avec l'API REST pour les tournois de Bloodbowl. L'idée est d'illustrer l'utilisation du canvas pour la conception des API web (et aussi de vous donner envie de lire le livre d'Arnaud Lauret :wink: ).

## Contexte

### Canvas des buts d'API

Pour rappel, le canvas proposé est un simple tableau avec les colonnes suivantes :

- Qui - qui sont les utilisateurs ?
- Quoi - que peuvent-ils faire ?
- Comment - Comment le font-ils ?
- Entrées - De quoi ont-ils besoin et d'où cela vient ?
- Sorties - Qu'est-ce qu'ils obtiennent et comment est-ce utilisé ?
- But - Est-ce les affaires des utilisateurs ?

Il permet de synthétiser les possibilités de l'API. Cela donne un outil d'inspection des points d'accès de l'API.

### API REST pour tournoi

Il s'agit d'un projet de modernisation de mon logiciel de tournoi dont je vous ai parlé dans la série : `Reprendre un projet après 3 ans` [premier billet]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years.md" >}}). C'est une écriture complète, le but étant de :

- ne pas avoir à gérer la mise à jour de Symfony 3 à Symfony 6 ;
- changer le découpage en composant de la solution ;
- avoir une solution plus facile à installer ;
- appliquer le langage Golang sur un cas concret.

La description au format OpenAPI de l'API cible est [ici (https://gitlab.com/trambi/fantasyfootballapi/-/blob/main/api/target-api-openapi3.json)](https://gitlab.com/trambi/fantasyfootballapi/-/blob/main/api/target-api-openapi3.json). Pourquoi faire le canvas alors que la documentation existe déjà ? Parce que j'aimerai passer au crible les points d'accès et m'assurer de la cohérence de mon API.

Actuellement, je cible plus d'une cinquantaine de points d'accès (dont une dizaine pour la compatibilité avec les applications existantes), je vous propose de nous intéresser pour ce billet aux seuls points d'accès qui concernent les éditions soit 10 points d'accès qui :

- retourne la liste des éditions disponibles ;
- retourne une édition particulière ;
- ajoute une édition ;
- modifie une édition ;
- supprime une édition ;
- retourne l'édition actuelle ;
- retourne les coachs de l'édition ;
- retourne les équipes de coach de l'édition ;
- retourne les matchs de l'édition ;
- calcule les nouveaux matchs à jouer de l'édition et les retourne.

## Au travail

Bon il est temps de remplir le tableau dans un premier temps en se basant uniquement sur la documentation.

|Qui|Quoi|Comment|Entrées|Sorties|But|
|---|----|-------|-------|-------|---|
|Développeurs d'application de visualisation|Avoir des informations sur les différentes éditions|Liste les éditions|Aucunes|Les éditions|Liste les éditions|
|Développeurs d'application de visualisation|Avoir des informations sur les différentes éditions|Récupérer les informations d'une édition|Édition (liste ou édition courante)|L'édition|Récupérer une édition en se basant sur son identifiant|
|Développeurs d'application d'administration|Gérer les éditions|Ajouter une édition|Information sur l'édition fournis par les utilisateurs|-|Ajouter une édition|
|Développeurs d'application d'administration|Gérer les éditions|Modifier une édition|Identifiant l'édition (liste ou édition courante) et informations de mise à jour fournis par les utilisateurs|-|Modifier une édition en se basant sur son identifiant|
|Développeurs d'application d'administration|Gérer les éditions|Supprimer une édition|Identifiant l'édition (liste ou édition courante)|-|Supprimer une édition en se basant sur son identifiant|
|Développeurs d'application de visualisation|Avoir des informations sur les différentes éditions|Récupérer les informations de l'édition courante|-|L'édition|Récupérer l'édition dont la date est la plus proche d'aujourd'hui|
|Développeurs d'application de visualisation|Avoir des informations sur les différentes éditions|Récupérer les coachs d'une édition|Édition (liste ou édition courante)-|Les coachs|Liste les coachs de l'édition|
|Développeurs d'application de visualisation|Avoir des informations sur les différentes éditions|Récupérer les équipes de coach d'une édition|Édition (liste ou édition courante)|Les équipes de coach|Liste les équipes de coachs de l'édition|
|Développeurs d'application de visualisation|Avoir des informations sur les différentes éditions|Récupérer les matchs d'une édition|Édition (liste ou édition courante)|Les matchs|Liste les matchs de l'édition|
|Développeurs d'application d'administration|Avoir des informations sur les différentes éditions|Calculer les nouveaux matchs d'une édition|Édition (liste ou édition courante)|Les nouveaux matchs à jouer|Calcule et liste les nouveaux matchs à jouer de l'édition|

A la lecture du tableau, il apparaît une question de catégorisation : Est-ce que récupérer les matchs, les coachs ou les équipes de coach sont liés à l'édition ou plutôt aux matchs aux coachs ou aux équipes de coach ?

On s'aperçoit aussi que l'ajout d'une édition ne retourne pas l'édition ajoutée pour avoir l'identifiant de l'édition ajoutée, on doit lister les éditions et déduire l'identifiant de l'édition ajoutée... Pas terrible.

Enfin le dernier point d'accès fait deux choses : il fait calculer les nouveaux matchs et les retourne. Cela semble un peu bancal car si on accède deux fois à ce point d'accès (avec les mêmes paramètres), l'API va créer les matchs pour une ronde puis pour la suivante, c'est assez contre intuitif. A partir du moment, où l'API proposera les points d'accès nécessaires pour la création d'une nouvelle ronde qui consiste : 

- à créer les matchs en fonction du classement général ;
- à mettre à jour le champ ronde courante de l'édition considérée.

Est-ce que ce point d'accès est nécessaire ? On peut enlever cette dernière ligne.

|Qui|Quoi|Comment|Entrées|Sorties|But|
|---|----|-------|-------|-------|---|
|Développeurs d'application de visualisation|Avoir des informations sur les différentes éditions|Liste les éditions|Aucunes|Les éditions|Liste les éditions|
|Développeurs d'application de visualisation|Avoir des informations sur les différentes éditions|Récupérer les informations d'une édition|Édition (liste **ou ajout** ou édition courante)|L'édition|Récupérer une édition en se basant sur son identifiant|
|Développeurs d'application d'administration|Gérer les éditions|Ajouter une édition|Information sur l'édition fournis par les utilisateurs|**L'édition**|Ajouter une édition|
|Développeurs d'application d'administration|Gérer les éditions|Modifier une édition|Identifiant l'édition (liste **ou ajout** ou édition courante) et informations de mise à jour fournis par les utilisateurs|-|Modifier une édition en se basant sur son identifiant|
|Développeurs d'application d'administration|Gérer les éditions|Supprimer une édition|Identifiant l'édition (liste **ou ajout** ou édition courante)|-|Supprimer une édition en se basant sur son identifiant|
|Développeurs d'application de visualisation|Avoir des informations sur les différentes éditions|Récupérer les informations de l'édition courante|-|L'édition|Récupérer l'édition dont la date est la plus proche d'aujourd'hui|
|Développeurs d'application de visualisation|Avoir des informations sur les différentes éditions|Récupérer les coachs d'une édition|Édition (liste **ou ajout** ou édition courante)-|Les coachs|Liste les coachs de l'édition|
|Développeurs d'application de visualisation|Avoir des informations sur les différentes éditions|Récupérer les équipes de coach d'une édition|Édition (liste **ou ajout** ou édition courante)|Les équipes de coach|Liste les équipes de coachs de l'édition|
|Développeurs d'application de visualisation|Avoir des informations sur les différentes éditions|Récupérer les matchs d'une édition|Édition (liste **ou ajout** ou édition courante)|Les matchs|Liste les matchs de l'édition|

## Conclusion

En prenant le temps d'analyser un sous-ensemble des points d'accès avec le canvas du livre `Design of Web APIs`, j'ai pu améliorer une insertion et supprimer un point d'accès qui pourrait porter à confusion. Je pense aussi que les informations exposées pour la liste des éditions sont trop riches, c'est une liste des objets entiers alors que l'on pourrait fournir une liste de synthèse.

L'application du canvas au reste de l'API m'a aussi permis de me rendre compte que j'expose des détails d'implémentation de mon API dans certains points d'accès : on comprend qu'il y une base de données relationnelle dernière. Or le but d'une API est de masquer ce genre de détails.

J'espère que ce billet vous aura motivé à lire le livre ou à essayer d'appliquer le canvas sur vos API Web. Si vous voyez d'autres outils pour le design d'API Web, n'hésitez pas à les partager avec moi sur [X](https://x.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
