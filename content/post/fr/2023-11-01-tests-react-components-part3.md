---
title: "Tests de composants React - partie 3"
date: "2023-11-01"
draft: false
description: "Etat de mes connaissances en 2023"
tags: ["Software Craftmanship","front-end","test","reactjs"]
author: "Bertrand Madet"
---

En continuant à coder en React.js et en animant des test clinics, j'ai pu apprendre des nouvelles choses sur les tests des composants React.js.

## Structuration des tests

Pour éviter qu'un test ne parte dans tous les sens et faciliter la lecture des tests, il est utile d'utiliser une structure pour les tests. Cela permet surtout d'éviter des tester plusieurs scénarios dans un seul test.

### Given When Then

```typescript
test('something',() => {
  // Given
  // When
  // Then
})
```

`Given when then` est une structure inspirée du BDD (**B**ehaviour **D**riven **D**evelopment). On commence par fixer les conditions initiales du test dans la partie `Given`. L'évènement à tester est décrit dans la partie `When`. Les vérifications des hypothèses est réalisée dans la partie `Then`.

### Arrange Act Assert

`Arrange Act Assert` ou `3A` reprend les mêmes principes que `Given When Then`. 

- `Arrange` prépare les conditions du test ;
- `Act` déclenche le test ;
- `Assert` sert à vérifier les résultats du test.

```typescript
test('something',() => {
  // Arrange
  // Act
  // Assert
})
```

## Niveaux de test

Il existe plusieurs niveaux de complexité de test. On peut avoir des tests d'une ligne ou des tests plus complexes où la structuration du test est très utile.

### Test de non crash au rendu

Le test de non crash au rendu est un test basique qui permet juste de savoir si le composant peut s'afficher sans erreur critique. On peut aussi dire que c'est un smoke test :gb:.

```typescript
import React from 'react';
import { render } from '@testing-library/react';
import { MyComponent } from '.';


describe('MyComponent', () => {
  it('should renders without crash', async () => {
    render(<MyComponent prop1="prop1" />);
  });
});
```

Il permet de valider qu'un exemple fonctionne. Normalement ce test est rendu inutile avec l'utilisation de TypeScript et d'ESLint.

### Test d'association des propriétés

Il s'agit d'un test qui permet de vérifier que chaque propriété est correctement pris en compte par exemple affichée. On va utiliser la bibliothèque `@testing-library/react`, l'objet `screen` et les méthodes `getBy...`, `findBy...` et `queryBy...`.

```typescript
import React from 'react';
import { render, screen } from '@testing-library/react';
import { MyComponent } from '.';

describe('MyComponent',() => {
  it('should match properly properties',async () => {
    // Given
    const prop1 = 'Une valeur possible de prop1';
    const prop2 = 'Une valeur possible de prop2';
    // When
    render(<MyComponent prop1={prop1} prop2={prop2}/>);
    // Then
    expect(await screen.findByText(prop1)).toBeInTheDocument();
    expect(screen.getByText(prop2)).toBeInTheDocument();
  });
});
```

:pinching_hand: On notera que la vérification avec `await screen.findByText` permet d'être sûr que le rendu est fini. Ensuite les vérifications peuvent être réalisées avec `screen.getByText`.

Ce genre de test permet de passer en revue les propriétés pour être sûr qu'elles ne sont affichées qu'une fois. On peut affiner la sélection avec des sélecteurs CSS. Si on a besoin de vérifier la présence d'un texte plusieurs fois, il faut utiliser `getAllByText` puis vérifier la taille du tableau de retour. Si on doit vérifier de l'absence d'un texte, on peut utiliser `queryByText`.

## Test d'interaction

Il s'agit de tester une interaction avec le composant, par exemple un clic sur une zone ou une saisie clavier. On va utiliser la bibliothèque `@testing-library/user-event` et les méthodes d'interaction `click`, `type`, `select`... On utilise en général, les fonctions jest `jest.fn()` et les fonctions de vérification `toBeCalledTimes` ou `toBeCalled`.

```typescript
import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MyComponent } from '.';

describe('MyComponent',() => {
  it('should call onClick when clicked',async () => {
    // Given
    const user = userEvent.setup();
    const onClickHandler = jest.fn();
    const prop1 = 'Une valeur possible de prop1';
    const prop2 = 'Une valeur possible de prop2';
    render(<MyComponent prop1={prop1} prop2={prop2} onClick={onClickHandler}/>);
    // When
    await user.click(await screen.findByText(prop1));
    // Then
    expect(onClickHandler).toBeCalledTimes(1);
  });
});
```

On peut aussi vérifier les paramètres utilisés par la fonction avec la fonction de vérification `toBeCalledWith`.

### Test de capture

Il est aussi possible de s'assurer que le HTML de sortie n'a pas varié depuis la dernière modification acceptée. On utilisera la fonction de vérification `toMatchSnapshot`.

```typescript
 it('should try snapshot', async () => {
    const prop1 = 'Une valeur possible de prop1';
    const prop2 = 'Une valeur possible de prop2';
    const onClickHandler = jest.fn();
    render(<MyComponent prop1={prop1} prop2={prop2} onClick={onClickHandler} />);
    expect((await screen.findByText(prop1)).parentNode).toMatchSnapshot();
  });
```

La fonction va comparer le code HTML correspondant avec un fichier dans le sous-répertoire `__snapshots__`.

:warning: La gestion du style par Jest n'est pas celle d'un navigateur comme j'ai pu le découvrir en Janvier 2023 avec mon billet [Tester le style avec Jest]({{< ref "/post/fr/2023-01-12-learn-by-testing-style-in-jest.md" >}}). Si je devais tester le rendu "visuel" d'un composant, je me baserai sur un moteur de test qui utilise un navigateur comme [Cypress (https://www.cypress.io/)](https://www.cypress.io/) ou sur Jest avec un pilote de navigateur comme [Playwright (https://playwright.dev/)](https://playwright.dev/) ou encore [Puppeteer (https://pptr.dev/)](https://pptr.dev/).

## Conclusion

Le type de test à utiliser dépend du composant à tester mais aussi du projet dans lequel il s'inscrit et plus particulièrement de sa complexité, de sa phase de vie, de sa couverture de test actuelle et de sa stratégie de test. Les niveaux de test sont aussi complémentaires entre eux. On peut commencer par tester par une capture puis ajouter des tests d'interaction.

Il faut aussi noter que les tests automatiques s'intègrent dans un outillage :

- utiliser TypeScript et des tests d'association de propriété permet de s'assurer que les composants affichent ce qu'ils doivent et que les appelants utilisent bien les propriétés ;
- utiliser l'analyse statique comme ESLint et TypeScript permettent de s'abstenir de test de crash au rendu sur des composants relativement simples.

Si vous avez des retours ou d'autres conseils sur les tests, n'hésitez pas à les partager sur [X](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
