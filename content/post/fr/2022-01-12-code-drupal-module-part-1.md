---
title: "Coder un module Drupal - partie 1"
date: "2022-01-12"
draft: false
description: "Un développement qui devait être facile"
tags: ["projet perso","php","jeu", "drupal"]
author: "Bertrand Madet"
---

Dans ce billet, j'explique un développement personnel que je pensais être simple et qui m'a pas mal occupé ces derniers temps : le développement d'un module Drupal.

## Pourquoi ce projet ?

Dans mon association de jeu, il y a des championnat de jeu de plateau. Et actuellement, ces ligues sont gérées de manière artisanale sur le forum du site de mon association.
Le site Internet de mon association de jeu utilise [le CMS Drupal (https://www.drupal.org/)](https://www.drupal.org/).

## Le but

L'idée d'avoir un module Drupal qui permet de générer le classement à partir des parties jouées.

## Développement

### Plan

Après avoir cherché un module qui permettrait d'afficher ces classements, j'ai décidé d'implémenter mon module Drupal. Le plan était simple :

- créer une entité (*nom des types de contenu en Drupal*) `Competition` qui représente une compétition - un ensemble de parties avec des règles de calcul du  classement communes ;
- créer une entité `Game` qui représente une partie opposant deux adversaires ;
- implémenter la logique qui permet à partir des règles de calcul du classement et de matchs d'obtenir un classement ;
- créer une vue qui affiche le classement d'une compétition.

### Choix

J'ai choisi de réutiliser en la simplifiant la partie calcul de classement de [ma boite à outils pour tournoi - cf. partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}). L'idée était de porter le code Golang en PHP et de le simplifier.

Pour décrire une critère de classement, on va utiliser une structure qui contient :

- un nom ;
- une expression par défaut basée sur les scores de deux adversaires  ;
- une liste de structure :
  - une condition basée sur les scores de deux adversaires,
  - l'expression si la condition est vraie.

Par exemple, si on veut décrire le critère de classement des points pour le championnat de football de ligue1 (3 points pour la victoire, 1 point pour le match nul et 0 point pour la défaite), on va remplir la structure avec :

- le nom "Points" ;
- l'expression par défaut "0" ;
- la liste :
  - la condition "score1 = score2" et expression "1",
  - la condition "score1 > score2" et expression "3".

On utilise des expressions plutôt que des valeurs pour gérer les critères comme la différence de buts (deuxième critère de classement pour le championnat de football de ligue1) que l'on pourrait décrire comme ceci :

- le nom "Différence de buts" ;
- l'expression par défaut "score1 - score2";
- la liste de condition et expression associée vide.

### État actuel

Actuellement, j'ai implémenté :

- la logique de classement ;
- un champ `StatementField` permettant de gérer une partie de la configuration d'un classement.

Ce que j'ai identifié qu'il restait à faire :

1. Implémenter une entité qui gère les critères de classement `RankCriteria` et qui contient plusieurs champs `StatementField` ;
1. Implémenter une entité qui décrit une compétition `Competition` et qui contient plusieurs critères de classement.
1. Implémenter une entité qui décrit une partie `Game` et qui fait référence à une compétition.
1. Implémenter la vue pour afficher le classement d'une compétition.

## Ce que j'ai appris

### Drupal c'est complexe

Drupal est un CMS extrêmement puissant qui permet sans coder d'ajouter des entités, des champs, des vues. Cette puissance se paye côté développement car il y a énormément de notions et de couches d'abstraction à comprendre.

Par exemple, pour créer [un champ sur mesure en Drupal (https://www.drupal.org/docs/creating-custom-modules/creating-custom-field-types-widgets-and-formatters/overview-creating-a-custom-field)](https://www.drupal.org/docs/creating-custom-modules/creating-custom-field-types-widgets-and-formatters/overview-creating-a-custom-field), il faut ajouter 3 classes :

- une classe dite `Item` pour implémenter les interfaces de stockage du champ ;
- une classe dite `Widget` pour implémenter la saisie du champ dans un formulaire ;
- une classe dite `Formatter` pour implémenter l'affichage du champ.

Le tout avec des annotations. Cela fait beaucoup d'information à saisir, d'association à renseigner. Le but est de gérer de manière découplée les trois notions de stockage, de saisie et d'affichage.  

### Utilité des tests unitaires

Le fait d'avoir des tests unitaires m'a permis de porter du code de Golang (langage compilé à type statique) vers PHP (langage interprété à typage dynamique) sans trop de difficulté et surtout en couvrant beaucoup de cas.

### Je préfère la syntaxe du Go

J'avais déjà codé en PHP, et le fait de devoir ajouter un `$` devant toutes les variables et `->` pour utiliser un membre d'une classe m'a parut très pénible.

### Je préfère PHPUnit à testing de Go

J'ai utilisé le PHPUnit intégré dans Drupal pour lancer les tests unitaires. Et je trouve que la logique ressemble plus aux standards auxquels j'ai l'habitude (Jest, Pytest ...).

```php
public function testEvaluteWithAddition() {
  $this->assertEquals('3', Interpreter::evaluate('1+2'));
  $this->assertEquals('3.5', Interpreter::evaluate('1+2.5'));
  $this->assertEquals('3.5', Interpreter::evaluate('1.5+2'));
  $this->assertEquals('3.5', Interpreter::evaluate('1.25+2.25'));
  $this->assertEquals('6', Interpreter::evaluate('1+2+3'));
}
```

A comparer à

```golang
func TestEvaluteWithAdditionTwoIntegers(t *testing.T) {
  result, err := Evaluate("1+2", make(Context))
  if err != nil {
    t.Errorf("Unexpected error: %v\n", err.Error())
  } else if result != "3" {
    t.Errorf("result %v is different from expected 3", result)
  }
}
func TestEvaluteWithAdditionAnIntegerAndAnNumber(t *testing.T) {
  result, err := Evaluate("1+2.5", make(Context))
  if err != nil {
    t.Errorf("Unexpected error: %v\n", err.Error())
  } else if result != "3.5" {
    t.Errorf("result %v is different from expected 3.5", result)
  }
}

func TestEvaluteWithAdditionAnNumberAndAnInteger(t *testing.T) {
  result, err := Evaluate("1.5+2", make(Context))
  if err != nil {
    t.Errorf("Unexpected error: %v\n", err.Error())
  } else if result != "3.5" {
    t.Errorf("result %v is different from expected 3.5", result)
  }
}

func TestEvaluteWithAdditionTwoNumbers(t *testing.T) {
  result, err := Evaluate("1.25+2.25", make(Context))
  if err != nil {
    t.Errorf("Unexpected error: %v\n", err.Error())
  } else if result != "3.5" {
    t.Errorf("result %v is different from expected 3.5", result)
  }
}
```

Cela signifie sûrement que je vais devoir regarder côté Golang, s'il y a des bibliothèques de test plus proche de ce que je connais en TypeScript ou en Python.

## Conclusion

Je n'en ai pas fini avec ce module et j'aimerai vraiment pouvoir ajouter cette fonctionnalité dans le site de notre association, donc je vais continuer.

Si vous avez des conseils sur ce module Drupal, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

Si vous souhaitez contribuer, le dépôt est sur Gitlab : [https://gitlab.com/trambi/drupal_module_just_rank_games](https://gitlab.com/trambi/drupal_module_just_rank_games) et les contributions sont les bienvenues.
