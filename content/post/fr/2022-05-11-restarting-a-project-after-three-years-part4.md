---
title: "Reprendre un projet après 3 ans - partie 4"
date: "2022-05-11"
draft: false
description: "Ce que j'ai appris"
tags: ["projet perso","php","jeu"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série :

- [le premier sur les étapes pour réussir à reconstruire le logiciel]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years.md" >}}) ;
- [le deuxième sur l'ajout de fonctionnalité]({{< ref "/post/fr/2022-04-13-restarting-a-project-after-three-years-part2.md" >}}) ;
- [le troisième sur le bilan sur le logiciel]({{< ref "/post/fr/2022-04-22-restarting-a-project-after-three-years-part3.md" >}}) ;
- [le cinquième sur la situation un an après]({{< ref "/post/fr/2023-06-07-restarting-a-project-after-three-years-part5.md" >}}) ;
- [le sixième sur la situation deux après]({{< ref "/post/fr/2024-05-22-restarting-a-project-after-three-years-part6" >}}).

J'ai repris un projet personnel que je n'avais pas touché depuis 3 ans pour un tournoi organisé par mon association. Ce tournoi a eu lieu ce week-end. Cela c'est bien passé sur le plan général et le logiciel a bien fonctionné.


Durant la préparation de l'évènement, j'ai pu apprendre (ou réapprendre) 3 choses, que je vous livre dans ce billet.

## Podman c'est très bien

Docker est le logiciel qui a permis de populariser l'utilisation des conteneurs - cf. mon billet [Une brève histoire des conteneurs]({{< ref "/post/fr/2021-10-27-container-history.md" >}}).

Docker a toujours été un logiciel propriétaire mais il a maintenant un inconvénient majeur : il impose des restrictions sur l'utilisation commerciale.

[Podman (https://podman.io)](https://podman.io) peut être une alternative : un moteur de conteneur libre (avec une licence Apache v2) sans démon et qui peut fonctionner sans droits adminstrateurs.

En plus de ces avantages, il a le bon goût de proposer la même interface de ligne de commande que `Docker`.

J'ai eu l'occasion de découvrir un outil `podman-compose` qui imite `docker-compose` et qui permet d'utiliser un orchestrateur minimal sans déployer Kubernetes. Et comme `podman`, il propose la même interface de ligne de commande que `docker-compose`.

> :+1: Il est possible d'utiliser podman avec le sous système Linux de Windows `WSL2`.

## La bonne surprise Traefik 

Lors de mon précédent billet : [Proxy et reverse proxy]({{< ref "/post/fr/2022-05-04-proxy-reverse-proxy.md" >}}), je partageais avec vous le principe du reverse proxy. D'habitude, j'utilisais Nginx, mais j'ai dû mal à me souvenir de la configuration  et chaque fois, je tâtonne énormément tant la configuration est riche (et parfois obscure).

[Traefik(https://traefik.io/)](https://traefik.io/) est un reverse-proxy configurable de différentes manières par des labels avec Docker Swarm ou Kubernetes par exemple ou simplement avec un fichier yaml.

La configuration suit une logique simple.

```yaml
http:
  routers:
    to-site1:
      rule: "PathPrefix(`/site1/`)"
      middlewares:
        - removeprefixes
      service: site1
    to-site2:
      rule: "PathPrefix(`/site2/`)"
      middlewares:
        - removeprefixes
      service: site2
  middlewares:
    removeprefixes:
      stripPrefix:
        prefixes:
          - "/site1"
          - "/site2"
  services:
    site1:
      loadBalancer:
        servers:
        - url: http://site1
    site2:
      loadBalancer:
        servers:
        - url: http://site2
```

 Le fichier de configuration ci-dessus permet d'associer respectivement les chemins `/site1`, `/site2` aux services `site1` et `site2`.

```mermaid
graph LR
  subgraph traefik
  route1
  route2
  middleware1
  middleware2
  service1
  service2
  end
  url1[url commencant /site1]-->route1[route site1]
  route1-->middleware1[middleware removeprefixes]
  middleware1-->service1[service site1]
  service1-->site1
  url2[url commencant par /site2]-->route2[route site2]
  route2-->middleware2[middlewareremoveprefixes]
  middleware2-->service2[service site2]
  service2-->site2
```

Les entrées sont gérées sont par les `routers` avec Nginx il faut soit modifié la configuration des blocs `server` et `location`.

Les sorties sont symbolisées par des `services`.

Les modifications nommées `middlewares` permettent de modifier les URL ou d'ajouter une authentification.

## La démarche incrémentale et itérative ça marche

J'ai adopté une approche très prudente car le logiciel a fait ces preuves (utilisé pour le tournoi une quinzaine de fois), les modifications possibles étaient vastes et le délai entre la reprise et le tournoi court.

J'ai lancé des tests avec l'organisateur dès que possible. Avant chaque séquence de tests, j'ai figé une nouvelle version des conteneurs des services. Après la session de test, j'ai limité au maximum les changements.

J'ai aussi préparé les services sur le cloud (Scaleway) et sur un PC (couplé avec un routeur Wifi) afin d'être sûr de pouvoir opérer le tournoi avec ou sans accès à internet.

## Conclusion

Le logiciel a bien réalisé son office sans accrocs, les modifications effectuées ont apporté un plus pendant la saisie des résultats. J'ai pu observer l'utilisation par mes comparses organisateurs et cela m'a donné des pistes d'amélioration.

Côté site participants, happé dans l'organisation du tournoi, je n'ai pas pris le temps d'observer les utilisateurs. J'essayerai de poser des questions à certaines personnes pour améliorer l'expérience utilisateur.

Si vous avez des projets personnels, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

Si vous souhaitez contribuer, le dépôt est sur Github : <https://github.com/trambi/FantasyFootballArchitecture>.

Ce billet fait partie d'une série :

- [le premier sur les étapes pour réussir à reconstruire le logiciel]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years.md" >}}) ;
- [le deuxième sur l'ajout de fonctionnalité]({{< ref "/post/fr/2022-04-13-restarting-a-project-after-three-years-part2.md" >}}) ;
- [le troisième sur le bilan sur le logiciel]({{< ref "/post/fr/2022-04-22-restarting-a-project-after-three-years-part3.md" >}}) ;
- [le cinquième sur la situation un an après]({{< ref "/post/fr/2023-06-07-restarting-a-project-after-three-years-part5.md" >}}) ;
- [le sixième sur la situation deux après]({{< ref "/post/fr/2024-05-22-restarting-a-project-after-three-years-part6" >}}).
