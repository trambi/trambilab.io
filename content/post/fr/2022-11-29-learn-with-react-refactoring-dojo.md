---
title: "Appris en préparant un React Refactoring Dojo"
date: "2022-11-29"
draft: false
description: "les apprentissages d'un double échec"
tags: ["front-end","refactoring","dojo","Software Craftmanship","test","reactjs"]
author: "Bertrand Madet"
---

Ces derniers temps, j'ai essayé d'organiser des Refactoring Dojos : le principe est d'utiliser des sessions au format [kata de Coding Dojo]({{< ref "/tags/dojo">}}) pour appliquer les principes de refactoring de Martin Fowler ([cf. la fiche de lecture du livre du même nom]({{< ref "/post/fr/2021-07-06-refactoring-lecture">}})). Pour s'entraîner sur les langages, il y a des très bons dépôts sur GitHub (et en particulier sur ceux d'Emily Bache) :

- [Gilded Rose Refactoring Kata (https://github.com/emilybache/GildedRose-Refactoring-Kata)](https://github.com/emilybache/GildedRose-Refactoring-Kata) ;
- [Racing Car Katas (https://github.com/emilybache/Racing-Car-Katas)](https://github.com/emilybache/Racing-Car-Katas) ;
- [Tennis Refactoring Kata (https://github.com/emilybache/Tennis-Refactoring-Kata)](https://github.com/emilybache/Tennis-Refactoring-Kata).

Pour React.js, il faut trouver des dojos où le code est en JavaScript ou en Typescript et utilise React.js.  Des traitements spécifiques à React sont dans le billet [TDD appliqué à un composant React - partie 2]({{< ref "/post/fr/2022-07-07-tdd-react-component-part-2" >}}). Ce billet retrace la préparation de mon premier React Refactoring Dojo et ce que j'en ai appris.

## Tentative 1 : Composant d'un projet pro

L'amélioration du code d'un composant sur un projet professionnel avait été intéressant avec des refactorings classiques (renommage de variables, extraction de fonctions) et des refactorings propres à React.js comme le changement de hook de `useState` à `useReducer`. Je pensais que c'était un bon candidat. Seulement je n'avais pas de référence du composant sans les refactorings. C'est possible pour deux raisons soit je n'ai pas "commité" avant d'améliorer soit j'ai fusionné les commits avant de pousser ma branche sur le dépôt distant avec `git rebase -i`.

Moralité, il faut impérativement garder au minimum 2 commits :

- un pour la création -  illustré avec :sparkles: si vous utilisez [gitmoji (https://gitmoji.dev/)](https://gitmoji.dev/) ;
- un pour les refactorings - illustré avec :recycle:.

L'exercice de revenir à l'état "initial" m'a semblé trop consommateur de temps. J'ai abandonné cette piste, et reporté la session pour chercher un dépôt GitHub de refactoring.

## Tentative 2 : Dépôt GitHub

Après une recherche sur GitHub, j'ai trouvé ce [dépôt : https://github.com/arolla/react-refactoring-kata](https://github.com/arolla/react-refactoring-kata). Il y a tout ce qu'il faut :

- un fichier de présentation `README.md` qui donne des consignes ;
- un fichier de paquet npm `package.json` qui permet d'installer les dépendances ;
- les sources de l'application React avec le composant à refactorer `CartPage` dans le répertoire `src/components/cartPage/cartPage.tsx`;
- les tests du composant à refactorer `src/__tests__/cartPage.test.tsx`.

Avant la session, j'ai lancé `npm install` pour vérifier que les dépendances s'installaient bien et `npm start` pour voir le composant en action.

Dès le début de la session, on s'aperçoit que les tests ne passent pas avant de commencer le refactoring du composant : `npm test` (la seule commande que je n'avais pas pensé à tester :exploding_head:).

Le reste de la séance est donc dédié à une recherche collective sur les raisons de l'échec des tests qui a la lecture paraissaient valides. 

Moralité, il faut s'assurer que les tests passent avant de se lancer dans une refactoring.

```mermaid
stateDiagram-v2
    oneRefactoringStep: Appliquer un refactoring
    testsSuccessed : S'assurer que les tests passent
    revert: Annuler les changements
    commit: Enregistrer le changement
    [*] --> testsSuccessed
    testsSuccessed --> oneRefactoringStep
    oneRefactoringStep --> testsSuccessed
    testsSuccessed --> commit: Les tests passent
    testsSuccessed --> revert: Un des tests échouent
    commit --> [*]
    revert --> [*]
```

Voici le code du test simplfié pour ne pas alterner entre plusieurs fichiers :

```typescript
describe('CartPage',()=>{
  it("should display correct products table when products are fetched", async () => {
    // Given
    mockRetrivedProductsWith([NEW_PRODUCT, ENDED_PRODUCT, COMMON_PRODUCT]);
    // When
    render(<CartPage shouldPayFees={shouldPayFees} />);
    await waitFor(() => global.fetch);
    // Then
    expect(isRendered({ key: NEW_PRODUCT_ROW, rowNumber: 1 })).toBeTruthy();
    expect(isRendered({ key: ENDED_PRODUCT_ROW, rowNumber: 2 })).toBeTruthy();
    expect(isRendered({ key: COMMON_PRODUCT_ROW, rowNumber: 3 })).toBeTruthy();
  });
});
```

Après une grosse sueur froide, nous identifions le problème venant de la ligne :

```typescript
  await waitFor(() => global.fetch);
```

De ma compréhension, cela attend que la promesse retournée par la fonction `fetch` soit réalisée. Or ce test dépend clairement de l'implémentation du composant (première fragilité) et le test échoue (seconde fragilité). En remplaçant par la ligne suivante, les tests réussissent.

```typescript
  await waitFor(() => expect(screen.queryByText('No Products...')).not.toBeInTheDocument());
```

Je me concentre sur le comportement du composant : en l'absence de produits, il affiche `No Products...`. J'attends donc qu'il n'affiche plus `No Products` sans me soucier de l' implémentation du composant.

## Bonus : mock de fetch

Je m'étais cassé les dents, la dernière fois que j'avais essayé. Même si le fait de devoir mocker `fetch` peut-être une indication de code sale, je pense que cela peut-être utile de savoir comment faire.

```typescript
  const products = [];
  global.fetch = jest
    .fn()
    .mockResolvedValue({ json: () => ({ products }) });
```

Et oui c'est tout, il suffit de se souvenir que fetch est une méthode de l'objet global.

## Conclusion

Le contenu du refactoring Dojo a été grandement impacté par le fait de ne pas avoir lancé la commande de test. Mais cela nous a permis de discuter fragilité des tests. J'ai refactoré les tests (sans toucher le code du composant) pour être plus proche de nos manière de tester.

J'espère pouvoir faire pratiquer le refactoring React sur le composant le mois prochain. J'ai trouvé un bug dans le composant, je l'ai laissé (en l'indiquant dans le README) pour voir si mes camarades le trouveront. J'ai proposé une [pull request à l'auteur du dojo (https://github.com/arolla/react-refactoring-kata/pull/1)](https://github.com/arolla/react-refactoring-kata/pull/1), nous verrons bien si elle est acceptée.

Si vous avez des questions ou des retours sur le coding dojo ou le refactoring, n'hésitez pas à les partager sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
