---
title: "Drupal 9 Module Development - Fiche de lecture"
date: "2022-01-20"
draft: false
description: "de Daniel Sipos" 
author: "Bertrand Madet" 
tags: ["fiche de lecture","drupal","drupal"]
---

## A qui s'adresse ce livre ?

Ce livre s'adresse à toutes les personnes qui veulent créer ou modifier des modules Drupal.

## Ce qui faut retenir

Drupal 9 Module Development fournit un catalogue des différentes aspects de la programmation d'un module - la modification du thème à la création d'objet métier en passant par la création de tâches automatiques.

## Mon opinion

| Points positifs | Points négatifs |
|---|---|
| :+1: La montée en complexité structurée| :-1: Toujours la théorie avant la pratique et avec la complexité de Drupal ça peut faire mal au crâne|
| :+1: Le code est complètement écrit puis justifié| :-1: La partie tests est reléguée au chapitre 17|
| :+1: L'existence d'un dépôt GitHub permet d'avoir une vision synthétique| :-1: Les cas concrets sont parfois trop simples|

Le livre est très complémentaire de la documentation de Drupal. Le fait d'avoir le code dans le [dépôt GitHub Drupal-9-Module-Development-Third-Edition (https://github.com/PacktPublishing/Drupal-9-Module-Development-Third-Edition)](https://github.com/PacktPublishing/Drupal-9-Module-Development-Third-Edition) permet d'appréhender le contenu d'une manière plus intuitive aux codeurs.

Malgré tout, le livre ne permet pas de se dispenser d'arpenter la documentation et le code source de Drupal 9.

## Détails

Drupal 9 Module Development de Daniel Sipos

ISBN: 978-1800204621

Pas disponible en :fr:
