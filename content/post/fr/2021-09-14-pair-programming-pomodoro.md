---
title: "Programmation en paire et Pomodoro"
date: "2021-09-14"
draft: false
description: "allier qualité et efficacité"
tags: ["methodologie","qualité","efficacité"]
author: "Bertrand Madet"
---

Notre équipe a eu la chance de s'agrandir. L'embarquement dans une équipe fonctionnant en Scrum est assez complexe et spécifique à chaque équipe. Dans ce billet, je souhaite décrire un mode opératoire qui peut être utilisé pour embarquer une nouvelle arrivante ou un nouveau arrivant et aussi au jour le jour. Ce mode opératoire combine :

- la programmation en paire ;
- le pomodoro.

## Programmation en paire

La programmation en paire (ou :gb: pair programming) consiste à travailler à deux sur un bout de code, cela peut être une nouvelle fonctionnalité, un bug ou un test. La programmation par paire a été popularisée par la pratique agile [Extreme programming (https://fr.wikipedia.org/wiki/Extreme_programming)](https://fr.wikipedia.org/wiki/Extreme_programming). Cela consiste à partager la responsabilité, en attribuant un rôle à chacune des personnes membres de la paire :

- le secrétaire qui a le clavier ;
- le pilote qui dicte au secrétaire quoi écrire.

Le rôle secrétaire a aussi (et surtout) la responsabilité de questionner ce qui écrit : par exemple, le nom d'une variable, le type de test utilisé ou l'algorithme utilisé.

L'avantage principal que j'y vois est la **qualité du code produit**. On peut dire que c'est une espèce de super revue de code en direct et avec de plus nombreuses interactions.  Il y a aussi :

- le respect des règles de l'équipe - c'est plus difficile de dire à un collègue que l'on ne va pas respecter telle règle que l'enfreindre tranquille à son bureau ;
- la diffusion de la connaissance du code ;
- le renforcement des liens entre les participants.

Pour s'assurer que l'exercice est équilibré, il est conseillé d'alterner les rôles régulièrement et c'est là que la technique nommée *pomodoro* intervient.

## Pomodoro

La [technique dite *pomodoro* (https://fr.wikipedia.org/wiki/Technique_Pomodoro)](https://fr.wikipedia.org/wiki/Technique_Pomodoro) consiste à alterner des périodes de concentration et des périodes de pauses.

```mermaid
stateDiagram-v2
    define: Définir la tâche ou les tâches à exécuter
    focus: Exécution de la tâche - 25min
    shortPause: Pause courte - 5min
    longPause: Pause longue - 20min
    [*] --> define
    define --> focus
    focus --> shortPause: les trois premières fois
    shortPause --> focus
    focus --> longPause: la quatrième fois
    longPause --> focus
    focus --> [*]: la tâche est finie
```

Pendant la période de concentration, on coupe toutes les notifications - emails, sms, slack ... Et pendant les pauses on peut discuter, répondre aux notifications ou se détendre.

Je trouve que cette technique de gestion du temps permet **d'être très concentré sur l'exécution de la tâche** et donc d'avoir **un rendement important**. C'est simple en télétravail, cela peut être plus problématique en openspace où les sollicitations sont plus variées.

## Articulation

L'idée est d'alterner les rôles à chaque phase d'exécution et de s'arrêter au moment de la pause longue. Cela fait un bon bloc de travail de deux heures, où l'on arrive souvent à implémenter les tâches que nous nous sommes fixés. Cette manière de faire est combinable avec le TDD et le refactoring [(cf. mon billet  TDD, refactoring et Git]({{< ref "/post/fr/2021-07-13-tdd-refactoring-git.md" >}})).

## Conclusion

Voilà nous avons réussi à utiliser cette combinaison de technique une fois par demi journée. Ce qui permet de partager beaucoup avec le nouvel arrivant en évitant les gros maux de tête :exploding_head:.

Si vous voulez souhaiter partager des méthodes de travail, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
