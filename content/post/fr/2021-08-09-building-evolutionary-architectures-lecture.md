---
title: "Building evolutionary architectures - Fiche de lecture"
date: "2021-08-09"
draft: false
description: "de Neal Ford, Rebecca Parsons et Patrick Kua sur des architectures faites pour changer constamment" 
author: "Bertrand Madet" 
tags: ["fiche de lecture","architecture"]
---

## A qui s'adresse ce livre ?

Aux personnes qui s'intéressent à l'architecture logicielle et qui souhaitent trouver une manière de faire de l'architecture avec le rythme de l'écosystème informatique actuel.

## Ce qui faut retenir

L'architecture évolutive part du principe que l'écosystème informatique est en perpétuel évolution. L'architecture logicielle doit donc être en adaptation permanente (par changements incrémentaux). 

Pour garantir l'avancée dans le bon sens des fonctions de vérification (`fitness functions` :gb: ) sont définies pour chaque capacité désirée (sécurité, disponibilité, respect du RGPD, performance...) et exécutées le plus régulièrement possible.

## Mon opinion

| Points positifs | Points négatifs |
|---|---|
| :+1: Complet allant de la définition aux conseils pour se lancer en passant par les écueils d'architecture le plus répandus| :-1: La partie sur les fonctions de vérification m'a paru moins concrète que les autres|
| :+1: Une vision de l'architecture logicielle en adéquation avec mes expériences d'agilité. | :-1: Le livre m'a paru moins activable (peut-être car je ne suis pas architecte)|
| :+1:  La partie Écueils et anti-patterns est sympa, drôle et toutes ressemblances avec des projets existants ou ayant existé ... ||

Un livre qui réconcilie l'architecture logicielle et l'agilité. Étant plus adepte de l'architecture émergente, je trouve finalement l'approche proposée plus pragmatique et elle a le mérite de placer les architectes dans le jeu.

## Détails

Building Evolutionary Architectures de Neal Ford, Rebecca Parsons et Patrick Kua

ISBN: 978-1491986363 

Pas disponible en :fr:


