---
title: "Boîte à outils pour tournoi - partie 6"
date: "2023-03-08"
draft: false
description: "Regroupement des lignes"
tags: ["projet perso","Go","jeu","tdd","test"]
author: "Bertrand Madet"
---
Boite à outils pour tournoi :
[partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}),
[partie 2]({{< ref "/post/fr/2020-12-09-tournament-tool-box-part-2.md" >}}),
[partie 3]({{< ref "/post/fr/2020-12-28-tournament-tool-box-part-3.md" >}}),
[partie 4]({{< ref "/post/fr/2021-04-26-tournament-tool-box-part-4.md" >}}),
[partie 5]({{< ref "/post/fr/2021-06-01-tournament-tool-box-part-5.md" >}}),
[partie 7]({{< ref "/post/fr/2023-03-20-tournament-tool-box-part-7.md" >}})

-----

Il manquait quelque chose dans mon outil : la possibilité de regrouper les matchs. Cela permet de gérer des matchs par équipe à partir des matchs bruts.

## Le but

Prendre en entrée des matchs et les regrouper les lignes en fonction de la valeur d'une liste de champs. Si les champs de regroupement en question sont la numéro de ronde, les identifiants des équipes, les lignes ainsi regroupés seront la synthèse des matchs des deux équipes pour une ronde donnée.

|ronde|equipeA|coachA|scoreA|equipeB|coachB|scoreB|
|-----|-------|------|------|-------|------|------|
|1   |Equipe 1|Riri  |1|Equipe 2    |Toto  |0     |
|1   |Equipe 1|Fifi  |0|Equipe 2    |Titi  |1     |
|1   |Equipe 3|Stone |1|Equipe 4    |Starky|1     |
|1   |Equipe 3|Charden|2|Equipe 4   |Hutch |1     |

|ronde|equipeA|equipeB |coachA_1|scoreA_1|coachB_1|scoreB_1|coachA_2|scoreA_2|coachB_2|scoreB_2|
|-----|-------|------  |------  |------- |------  |------  |------  |------- |------  |------  |
|1   |Equipe 1|Equipe 2|Riri    |1       |Toto    |0       |Fifi    |0       |Titi    |1       |
|1   |Equipe 3|Equipe 4|Stone   |1       |Starky  |1       |Charden |2       |Hutch   |1       |

Ce fichier de sortie permet ensuite de calculer des points d'équipe. L'avantage est que cela permet de gérer un seul fichier d'entrée.

## Développement

### Le plan

Enthousiasmé par mon [précédent coding dojo]({{< ref "/post/fr/2023-02-28-coding-dojo-prime-number-factorization-part2.md" >}}), je souhaitais implémenter la fonctionnalité en TDD avec des tests de propriété.

## La réalisation

### Bibliothèque de PBT en Go

Côté Go, j'ai trouvé les bibliothèques :

- [testing/quick (https://pkg.go.dev/testing/quick)](https://pkg.go.dev/testing/quick) - un paquet de bibliothèque standard gelé ;
- [pgregory.net/rapid (https://github.com/flyingmutant/rapid)](https://github.com/flyingmutant/rapid) - un paquet dont la dernière modification date de deux mois avec plus de 300 étoiles sur GitHub;
- [github.com/leanovate/gopter (https://pkg.go.dev/github.com/leanovate/gopter)](https://pkg.go.dev/github.com/leanovate/gopter) - un paquet dont la dernière modification date de trois ans avec plus de 500 étoiles sur GitHub.

Pas de bibliothèque inévitable même si rapid a l'air d'être le choix le plus raisonnable.

### Difficultés

Premier constat, j'ai eu du mal à trouver des propriétés pour ma fonction. Comme je l'ai montré plus haut, c'est plus simple de faire de traiter avec des exemples.

### Paquet compute

J'ai commencé par chercher des propriétés ou des cas à tester. Faute de propriété et surtout de choix d'une bibliothèque de PBT, j'ai continué par essayer d'implémenter la fonction en `PPP` (**P**seudocode **P**rogramming **P**rocess). Le fait de décrire une manière d'implémenter la fonction m'a permis de trouver un cas de test : sans données d'entrée, les entêtes de sortie correspondent à la liste des critères de regroupement. 

J'ai donc ajouté ce cas de test. Le test échouait évidemment. J'ai ensuite transformé le pseudo code pour les entêtes de sortie pour le test passe.

```go
func TestGroupByWithoutRows(t *testing.T) {
  expected := [][]string{
    {"round", "squad_a", "squad_b"},
  }
  fields := []string{"round", "squad_a", "squad_b"}
  headers := []string{"round", "id_a", "team_a", "td_a", "squad_a", "id_b", "team_b", "td_b", "squad_b"}
  rows := [][]string{}
  result, err := GroupBy(fields, headers, rows)
  if err != nil {
    t.Errorf("Unexpected error: %v\n", err.Error())
  } else if cmp.Equal(result, expected) == false {
    t.Errorf("result %v is different from expected %v", result, expected)
  }
}
```

J'ai finalement ajouté un test avec une ligne puis deux lignes à regrouper.

```go
func TestGroupByWithOneRow(t *testing.T) {
  expected := [][]string{
    {"round", "squad_a", "squad_b", "id_a_1", "team_a_1", "td_a_1",
      "id_b_1", "team_b_1", "td_b_1"},
    {"1", "firstSquad", "secondSquad", "1", "first_team", "3", "2", "second_team", "4"},
  }
  fields := []string{"round", "squad_a", "squad_b"}
  headers := []string{"round", "id_a", "team_a", "td_a", "squad_a", "id_b", "team_b", "td_b", "squad_b"}
  rows := [][]string{
    {"1", "1", "first_team", "3", "firstSquad", "2", "second_team", "4", "secondSquad"},
  }
  result, err := GroupBy(fields, headers, rows)
  if err != nil {
    t.Errorf("Unexpected error: %v\n", err.Error())
  } else if cmp.Equal(result, expected) == false {
    t.Errorf("result %v is different from expected %v", result, expected)
  }
}

func TestGroupByWithTwoRows(t *testing.T) {
  expected := [][]string{
    {"round", "squad_a", "squad_b",
      "id_a_1", "team_a_1", "td_a_1", "id_b_1", "team_b_1", "td_b_1",
      "id_a_2", "team_a_2", "td_a_2", "id_b_2", "team_b_2", "td_b_2"},
    {"1", "firstSquad", "secondSquad",
      "1", "first_team", "3", "2", "third_team", "1",
      "3", "second_team", "1", "4", "last_team", "0"},
  }
  fields := []string{"round", "squad_a", "squad_b"}
  headers := []string{"round", "id_a", "team_a", "td_a", "squad_a", "id_b", "team_b", "td_b", "squad_b"}
  rows := [][]string{
    {"1", "1", "first_team", "3", "firstSquad", "2", "third_team", "1", "secondSquad"},
    {"1", "3", "second_team", "1", "firstSquad", "4", "last_team", "0", "secondSquad"},
  }
  result, err := GroupBy(fields, headers, rows)
  if err != nil {
    t.Errorf("Unexpected error: %v\n", err.Error())
  } else if cmp.Equal(result, expected) == false {
    t.Errorf("result %v is different from expected %v", result, expected)
  }
}
```

### Idées de propriétés

Après réflexion, j'ai identifié quelques propriétés à tester :

- si les champs de regroupement appartiennent aux entêtes d'entrée et que les données d'entrée sont valides, les données de sortie sont valides ;
- si les champs de regroupement n'appartiennent pas aux entêtes d'entrée et que les données d'entrée sont valides, une erreur est levée ;
- les entêtes de sortie se répètent en fonction du nombre de regroupement ;

Il faudra créer des générateurs d'entête et de données, je pense en utilisant rapid.

## En résumé

Je retiens de cette séquence :

- Les tests de propriété sont pas forcement facile à trouver ;
- Les idées de propriété peuvent venir *a posteriori*;
- J'ai envie de continuer la fonctionnalité avec des tests de propriétés et la bibliothèque rapid.

Voilà, j'espère que cela vous a plu. Si vous souhaitez partager des projets, contactez [moi sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/). J'accueille les merge requests sur [TournamentToolBox (https://www.gitlab.com/trambi/tournamenttoolbox)](https://www.gitlab.com/trambi/tournamenttoolbox) avec enthousiasme.

-----

Boite à outils pour tournoi :
[partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}),
[partie 2]({{< ref "/post/fr/2020-12-09-tournament-tool-box-part-2.md" >}}),
[partie 3]({{< ref "/post/fr/2020-12-28-tournament-tool-box-part-3.md" >}}),
[partie 4]({{< ref "/post/fr/2021-04-26-tournament-tool-box-part-4.md" >}}),
[partie 5]({{< ref "/post/fr/2021-06-01-tournament-tool-box-part-5.md" >}})
