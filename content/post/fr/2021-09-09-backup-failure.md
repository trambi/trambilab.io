---
title: "Echec de sauvegarde"
date: "2021-09-09"
draft: false
description: "il faut toujours vérifier une sauvegarde"
tags: ["backup","fail","devops"]
author: "Bertrand Madet"
---

Un peu avant que je parte en vacances, nous avions implémenté une fonction de sauvegarde sur mon projet. A mon retour, j'ai eu la désagréable surprise d'avoir besoin d'une sauvegarde. Le retour pimenté s'est transformé en mal de tête quand je me suis aperçu que la sauvegarde dont j'avais besoin était ... une archive compressée de contenu vide :exploding_head:. Ce qui m'intéresse aujourd'hui ce sont les leçons que l'on peut en tirer.

## Contexte

La sauvegarde automatique a été implémentée en s'inspirant de la sauvegarde manuelle documentée. La sauvegarde automatique a été intégrée dans le dépôt donc soumis à une revue de code au moment de l'intégrer dans la branche principale. La revue de code n'a rien révélé de flagrant.

## Problème

L'erreur était dû à un paramètre oublié et cette erreur provoquait l'écriture d'un fichier vide. Le fichier vide était ensuite compressé donnant un fichier de taille non nulle (et oui l'entête de la compression ajoute des octets). On avait donc un fichier de sauvegarde de taille nulle et la compression est valide.

La cause principale du problème est que le test réalisé sur le fichier de sauvegarde n'était pas cohérent avec le but réel de l'histoire utilisateur. Le but de la "sauvegarde" n'est pas de sauvegarder les données mais uniquement de les restaurer.

## Solution

J'ai donc commencé par élaborer une procédure de restauration à partir des sauvegardes manuelles en vérifiant le contenu de la base de données. Une fois la procédure rédigée, j'avais un test pour la "sauvegarde". J'ai pu corriger l'erreur dans le script de sauvegarde et injecter le résultat de la sauvegarde automatique dans le procédure de restauration. J'ai ensuite vérifié le contenu de la base de données.

## Leçon

Je savais déjà qu'il faut tester le résultat d'un processus de sauvegarde. Cette aventure a précisé la nature du test : injecter le résultat de la sauvegarde automatique dans le procédure de restauration.

Le point qui vient ensuite est jusqu'où tester :

- pas d'erreur à la restauration ?
- présence des collections ou tables attendues ?
- les collections ou tables attendues ne sont pas vides ?
- présence de certains documents dans les collections (ou d'enregistrement dans les tables) ...

Cela peut être assez vertigineux, il me semble que c'est un équilibre à trouver en fonction de l'importance des données perdues en cas d'erreur de sauvegarde et d'effort de vérification. Il y a aussi la confiance que l'on a dans les outils utilisés.

## Conclusion

Nous n'avons pas encore ajouté l'étape de vérification automatique. Si vous avez des aventures sur les exploitations de logiciels, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
