---
title: "3 conseils sur l'architecture logicielle"
date: "2021-06-07"
draft: false
description: "Plutôt pratiques"
tags: ["architecture "]
author: "Bertrand Madet"
---

L'architecture logicielle est l'organisation entre les parties logicielles (aussi bien internes ou qu'externes).  

C'est compliqué car :

- il n'y a pas d'architecture idéale, mais des architectures adaptées au contexte de votre logiciel ;
- l'architecture de votre logiciel évolue en même temps que votre logiciel.

Je vous propose, malgré tout trois conseils pratiques, pour avoir une meilleure architecture logicielle, au sens où elle sera plus adaptée au contexte de votre logiciel.

## Lisez les références

Avant de vous livrer mes conseils, les références de l'ingénierie logicielle comme `Clean Code` ([fiche de lecture]({{< ref "post/fr/2020-11-16-clean-code-lecture.md" >}})), `Pragmatic Programmer` ou `Code complete` abordent le sujet. Il y a aussi plein de livres sur le sujet, par exemple `Clean Architecture` ([fiche de lecture]({{< ref "/post/fr/2020-12-23-clean-architecture-lecture.md" >}})) et plein de livres sur un type d'architecture, par exemple `Building Microservices` ([fiche de lecture]({{< ref "/post/fr/2020-11-30-building-microservices-lecture.md" >}})).

Personnellement, je préfère les livres de référence aux livres d'architecture aux livres sur un type d'architecture. En général quand on nous présente un marteau, les problèmes se mettent à ressembler à des clous. Il est aussi important de savoir quand ne pas utiliser une architecture que quand l'utiliser.

Tous ces livres parlent mieux d'architecture logiciel que je ne saurai le faire. Place aux conseils sur l'architecture logicielle.

## Faites des fiches de choix

Faites des fiches de choix pour vos décisions sur votre logiciel :

- type d'architecture ;
- ajout d'une dépendance externe ; 
- sélection d'un composant de stockage de données ...

L'idée n'est pas de justifier vos choix ni de sortir le parapluie mais de prendre le temps d'examiner  différents critères. L'interprétation de ces critères restera subjective, mais cela permet de prendre du recul et de se poser des questions.

Cela permettra aussi d'invalider vos choix et de se rendre compte qu'il est temps de changer. Car vous allez vous tromper, et même si (certains de) vos choix sont judicieux à un moment, le contexte (marché, clientèle, réglementation, équipe...) les invalidera.

Le mieux est de remplir cette fiche de choix à plusieurs en passant au cribble plusieurs solutions possibles. Cela donne vraiment plus de profondeur d'avoir une diversité de points de vue sur la fiche de choix.

Parmi les critères à prendre en compte au minimum selon moi :

- la maintenance (fréquence des mise à jour, qui est derrière...) ;
- le type de licence (cela peut être important pour votre modèle de business cf. [Pourquoi vous devriez vous intéresser aux licences de vos logiciels ?]({{< ref "post/fr/2020-11-14-pourquoi-vous-devriez-vous-interesser-aux-licences-de-vos-logiciels" >}})) ;
- la sécurité (conformité, audit, menaces prises en compte...) ;
- la maturité de votre équipe et de votre entité sur la technologie ;

Essayez de faire une checklist avec votre équipe pour vos fiches de choix. **Ne négligez par l'importance de la maturité de votre équipe sur la technologie !** Si votre équipe est bien formée à SQL et qu'il semble qu'une base de données NoSQL réponde mieux aux critères techniques (latence, consistence, volume, réplication). Se poser la question sur la maturité de votre équipe sur le NoSQL peut être l'occasion de discuter au sein de l'équipe du ressenti de chacun et de voir si c'est un choix durable ou non.

## Présentez votre architecture

Présentez votre architecture et soyez à l'écoute des remarques ou des questions qu'elle soulève. Un regard extérieur permet souvent de souligner les raffinements superflus. Des fois, c'est dur à entendre, mais cela permet de simplifier votre architecture ou de l'adapter à un nouveau contexte. Vous pouvez aussi indiquer les points qui posent problème pour vous dans cette architecture. L'idée est de profiter de l'intelligence collective et de la diversité des points de vue.

En plus avec les fiches de choix, vous devriez être à l'aise pour expliquer vos choix.

## Faites des graphes de dépendances

Le graphe de dépendance ou le schéma qui indique quel module dépend de quel module, cela permet :

- de visualiser les dépendances externes de votre logiciel ;
- d'avoir une idée de simplicité de votre logiciel ;
- de visualiser des problèmes comme des dépendances cycliques (A dépend de B qui dépend de C qui dépend de A :exploding_head:) ;
- de visualiser une dépendance d'un module métier à des détails d'implémentation ;
- de visualiser l'organisation globale de votre logiciel.

Cela est assez facile à faire, les langages ont des instructions comme `import` et un `grep -R import *.tsx` vous permettra d'avoir la liste des imports fichier par fichier. Avec Mermaid, cela permet de faire rapidement un graphe de dépendance et de le mettre dans un `README.md`.

Un exemple de graphe de dépendances (de mon projet perso).
```mermaid
graph TD;
    cmd[cmd/main]
    subgraph "logique métier"
        pair[pkg/pair]
        rank[pkg/rank]
        compute[pkg/compute]
        evaluator[pkg/evaluator]
        settings[pkg/settings]
    end;
    subgraph "import/export en csv"
        csv[pkg/csv]
        pair_csv[pkg/csv/pair]
        rank_csv[pkg/csv/rank]
    end;
    subgraph "import/export en json"
        settings_json[pkg/json/settings]
    end;
    cmd-->compute
  	cmd-->csv
	cmd-->pair
	cmd-->pair_csv
	cmd-->rank
	cmd-->rank_csv
	cmd-->settings_json
    cmd-->settings
	compute-->evaluator
    pair_csv-->pair
    rank_csv-->rank
    settings-->rank
    settings-->compute
    settings_json-->rank
    settings_json-->settings
	settings_json-->settings
```

## Conclusion

J'espère que ces modestes conseils pratiques vous serviront. Si vous souhaitez en partager d'autres, contactez [moi sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
