---
title: "Clean Architecture - Fiche de lecture"
date: "2020-12-23"
description: "Livre d'Oncle Bob sur l'architecture logicielle"
tags: ["fiche de lecture","logiciel","base","oncle Bob","architecture","livre en français"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Aux développeuses, aux développeurs et à tous ceux qui s'intéressent à l'architecture logicielle. De mon point de vue, il faut déjà avoir un peu d'expérience dans le développement logiciel.

## Ce qu'il faut retenir

 1. Une bonne architecture permet au logiciel de rester adaptable aux changements. Le coût d'un changement doit rester faible.
 1. Un architecte permet de différer au maximum les choix dimensionnant pour un logiciel et établit des séparations entre les composants
 1. Les composants métier ne doivent pas s'encombrer de détails d'implémentation comme l'interfaçage à la base de données, les formats des données d'entrée, l'interface homme machine...

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Une définition assez simple et iconoclaste de l'architecture| :-1: Le début m'a semblé long et un peu abstrait|
| :+1: Quand on rentre dans le détail des dépendances des composants, cela devient clair et plus activable| :-1: Les paradigmes de programmation ne sont pas assez développés à mon goût|
| :+1: L'expérience d'oncle Bob||

Le livre est un peu long à se mettre en place, le début m'a semblé abstrait. Il y a peu de principes énoncés mais ils sont puissants. Cela prend corps une fois que l'on rentre dans les diagrammes de dépendances des composants. 

Je place ce livre au même niveau que [Clean Code]({{< ref "/post/fr/2020-11-16-clean-code-lecture.md" >}}) du même auteur avec lequel il est très complémentaire. Si vous aimez mettre les mains dans le cambouis, commencez plutôt par Clean Code (ou coder proprement). Dans la même série, Clean Coder et Clean Agile sont intéressants mais moins essentiels.

J'aurais l'occasion d'expliquer la mise en application sur mon projet personnel prochainement.

## Détails

En anglais :gb: :

- Clean Architecture de Robert C. Martin
- ISBN-13 : 978-0134494166

En français :fr: : 

- Architecture logicielle propre de Robert C. Martin
- ISBN-13 : 978-2326058606
- Sortie en novembre 🎉
