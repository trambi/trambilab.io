---
title: "Passage d'une dizaine de dépôts à un - partie 1"
date: "2021-08-19"
draft: false
description: "Premier retour d'expérience"
tags: ["git","workflow","monorepository"]
author: "Bertrand Madet"
---

En avril, je vous avais indiqué la [problématique de gestion des dépôts Git]({{< ref "/post/fr/2021-04-19-git-work-flow-and-repositories.md" >}}). L'arrivée d'une personne au profil opération sur notre projet nous a permis de concrétiser le passage à un seul dépôt.

## Un peu de contexte

Petit rappel : Le service sur lequel je travaille était structuré avec une dizaine de dépôts Git. Il y a des services Web en React, un catalogue de composants React et des applications pour gérer les back-office. La bibliothèque de composants permet de partager les composants utilisés dans nos différentes applications Web React.

```mermaid
graph BT
  webapp1[Application web React 1]-->|Utilise des composants|catalog[Catalogue de composants]
  webapp2[Application web React 2]-->|Utilise des composants|catalog
```

Nous utilisons npm (version 6) pour gérer les paquets. Afin de limiter les changements dans nos habitudes, nous avons décidé de rester sur les mêmes outils dans un premier temps et de ne pas en ajouter d'autres.

Nous avons aujourd'hui un dépôt pour tous les services avec un répertoire par service et un fichier `.gitlab-ci.yml` qui inclue les différents fichiers d'un répertoire nommé `ci`.

## Le constat

- ⏲️ il a fallu un mois et demi au membre de l'équipe dédié (à mi-temps) aux opérations pour réaliser cette étape ;

- :+1: Ajouter (ou modifier) une fonctionnalité sur différents services est devenue beaucoup plus simple et rapide;

- :100: Analyser et faire la revue du code d'une fonctionnalité est un plaisir;

- :-1: Les pipelines d'intégration continue durent plus longtemps.

## Mode opératoire

Le passage en mono dépôt a été plutôt long car nous avons été très méticuleux. Pour chaque dépôt, notre collègue chargé des opérations a :

1. a copié le contenu dans un répertoire spécifique ;
1. a adapté le processus d'intégration continue au mono dépôt ;
1. a vérifié avec développeurs que les tests fonctionnaient comme convenu ;
1. a adapté le processus de livraison continue au mono dépôt.

Le tout en faisant attention à rester à jour des développements du reste de l'équipe.

## La suite

Pour l'instant, nous avons gardé les mêmes outils (npm 6 et des scripts bash) pour avoir une idée plus précise des problèmatiques induites par le passage en mono-dépôt.

Au-delà des améliorations d’enchaînement des étapes de l'intégration continue, j'envisage que l'on pourrait utiliser :
- les [espaces de travail (`workspace`) de npm(https://docs.npmjs.com/cli/v7/using-npm/workspaces)](https://docs.npmjs.com/cli/v7/using-npm/workspaces);
- [yarn et les espaces de travail(https://yarnpkg.com/features/workspaces)](https://yarnpkg.com/features/workspaces) ;
- [Rush (https://rushjs.io)](https://rushjs.io) ;
- [Lerna (https://lerna.js.org/)](https://lerna.js.org/).

## Conclusion

Voici le première étape de notre passage à un seul dépôt, il reste encore du travail et je vois cela comme de l'amélioration continue de nos processus de développement. Je suis très satisfait par cette modification et cela se ressent en terme de vitesse de développement dans notre projet. 

Si vous avez des retours sur la gestion de vos dépôts de vos projets professionnels ou personnels, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/). De mon côté, j'essayerai de rendre compte des évolutions dans notre processus de développement.
