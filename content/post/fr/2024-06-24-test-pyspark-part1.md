---
title: "Tester un traitement Pyspark"
date: "2024-06-24"
draft: false
description: "avec Python, pytest"
tags: ["python","test","spark"]
author: "Bertrand Madet"
---

Je travaille de nouveau sur un projet qui utilise Spark 2, Python et Scala. L'apprentissage de la semaine concerne le test de traitement PySpark. En schématisant, on a deux ensembles de données d'entrée dans des fichiers parquets qui vont être combinés en un ensemble de données. Le résultat est écrit dans un fichier parquet.

```mermaid
graph LR;
    input1(Données d'entrée 1 dans un fichier parquet)-->transform[Code de la transformation à tester]
    input2(Données d'entrée 2 dans un fichier parquet)-->transform
    transform-->output(Données de sortie dans un fichier parquet)
```

Afin de faciliter les tests et surtout de rendre le traitement indépendant des formats d'entrées et de sortie, on va affiner la structure de notre code.

```mermaid
graph LR;
  subgraph "Traitement"
    read[Lecture des données d'entrées en DataFrame]-->transformFormatIndependant[Traitement des DataFrames]
    transformFormatIndependant-->write[Ecriture du DataFrame de sortie]
  end
    input1(Données d'entrée 1 dans un fichier parquet)-->read
    input2(Données d'entrée 2 dans un fichier parquet)-->read
    write-->output(Données de sortie dans un fichier parquet)
```

En terme de Python et de PySpark2, cela donne le code ci-dessus :

```python
import pyspark

def fusionInput1Input2():
  input1df = // readparquet
  input2df = // readparquet
  outputdf = fusionDataFrames(input1df,input2df)
  //writeparquet(outputdf)

def fusionDataFrames(input1df,input2df):
  // ...

```

Avec ce découpage, on va pouvoir tester en profondeur le traitement sans dépendre des formats d'entrée et de sortie.

## Structure du test

En utilisant la structure `Given, When, Then`, notre test peut s'écrire :

```python
import os
from pyspark import SparkConf
from pyspark import SparkSession
from pyspark_test

from .context import process

def test_fusion_input1_input2_happy_path():
  # Given
  conf = (
    SparkConf()
    .setMaster("local")
    .setAppName("testing")
    .set("spark.driver.host","localhost")
  )
  spark = SparkSession.builder.config(conf=conf).getOrCreate()
  input1 = spark.read.json(get_data_path("input1-happy-path.jsonl"))
  input2 = spark.read.json(get_data_path("input2-happy-path.jsonl"))
  expected = spark.read.json(get_data_path("output-happy-path.jsonl"))
  # When
  result = process.fusion(input1,input2)
  # Then
  pyspark_test.assert_pyspark_df_equal(result,expected)
```

On peut extraire la création de la session Spark en tant que fixture et l'ajouter dans le fichier `conftest.py` :

```python
# conftest.py

from pyspark import SparkConf
from pyspark import SparkSession

import pytest

@pytest.fixture
def spark_session():
  conf = (
    SparkConf()
    .setMaster("local")
    .setAppName("testing")
    .set("spark.driver.host","localhost")
  )
  return SparkSession.builder.config(conf=conf).getOrCreate()
```

Et le test devient : 

```python
import os
from pyspark import SparkConf
from pyspark import SparkSession
from pyspark_test

from .context import process

def test_fusion_input1_input2_happy_path(spark_session):
  # Given
  input1 = spark_session.read.json(get_data_path("input1-happy-path.jsonl"))
  input2 = spark_session.read.json(get_data_path("input2-happy-path.jsonl"))
  expected = spark_session.read.json(get_data_path("output-happy-path.jsonl"))
  # When
  result = process.fusion(input1,input2)
  # Then
  pyspark_test.assert_pyspark_df_equal(result,expected)
```

En général, je mets mes fichiers de test dans un répertoire `test-data`, je fournis donc une fonction `get_data_path` qui permet d'obtenir le chemin absolu des fichiers de tests. La structure du test est assez simple et la difficulté réside dans le nommage de la fonction de test et des fichiers de références.

## Format des DataFrames de référence

Les fichiers parquet ne sont pas lisibles depuis un éditeur de code et l'équipe est beaucoup plus à l'aise avec le format JSON. On va donc utiliser le format "json" pour définir nos données d'entrée et de sortie.

⚠️ Le format attendu par la fonction `spark.read.json` n'est pas le format [json classique](https://www.json.org/json-fr.html), où on aurait un fichier avec un tableau d'objets mais du [jsonlines](https://jsonlines.org/) où chaque ligne est une entité JSON distincte. Pour ne pas confondre, on va utiliser l'extension de fichier `jsonl`.

```json
{ "name":"objet de la ligne 1","property1":"value1A","property2":"valeur2A"}
{ "name":"objet de la ligne 2","property1":"value1B","property2":"valeur2B"}
```

🤏 Il est possible de définir des valeurs nulles, en utilisant le mot clé `null`.

## Vérification des DataFrames

A partir de PySpark 3.5, il y a un sous-module de PySpark [Testing](https://spark.apache.org/docs/latest/api/python/reference/pyspark.testing.html). Avant et en particulier en PySpark 2, il faut soit faire la comparaison à la main, soit utiliser une bibliothèque non officielle. Parmi celles que nous avons testées, [pyspark-test](https://pypi.org/project/pyspark-test/) est la plus facile à utiliser car elle donne les différences entre les deux DataFrames.

## Conclusion

On peut ensuite décliner les tests avec les différents cas du traitement, les cas où tout se déroule correctement et les cas d'erreur ...

Pas de changements fondamentaux par rapport aux tests unitaires classiques avec `pytest` mais quelques subtilités. La partie sur la vérification des dataframes nous a pris quelque temps car nous avons dû jongler 🤹 avec différentes bibliothèques avant de trouver celle qui nous aida véritablement dans nos tests.

Si vous souhaitez partager sur les tests de PySpark, contactez moi [sur X](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169
