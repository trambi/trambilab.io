---
title: "Ajout de logique métier dans une API REST - classement des joueurs"
date: "2023-12-30"
draft: false
description: "on sort du CRUD,"
tags: ["REST API", "projet perso","Go"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série :

- [Présentation de mon projet perso du moment]({{< ref "/post/fr/2023-11-21-rewrite-side-project-api-in-go-part0" >}}) ;
- [Masquer les détails d'implémentation d'une API REST]({{< ref "/post/fr/2023-11-09-rewrite-side-project-api-in-go-part1" >}}) ;
- [Ajout de logique métier dans une API REST - calcul des points]({{< ref "/post/fr/2024-01-09-rewrite-side-project-api-in-go-part3" >}}) ;
- [Confronter une API REST à l'existant]({{< ref "/post/fr/2024-04-17-rewrite-side-project-api-in-go-part4" >}}).

L'API REST en question est la réécriture complète en Go du logiciel de tournoi dont je vous ai parlé dans la série : `Reprendre un projet après 3 ans` [premier billet]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years.md" >}}). Le but de cette API REST est de gérer un tournoi, les participants et les matchs associés. J'avais commencé par implémenter les aspects `CRUD` (:gb: **C**reate **R**ead **U**pdate **D**elete), c'est à dire les aspects de manipulations des objets métier :

- une édition `Edition` ;
- un joueur `Coach` ;
- une équipe de joueur `Squad` ;
- une partie `Game`.

Il y a peu de logique métier et donc peu d'intérêt intrinsèque : la configuration d'une base de données orientée document et fournissant une API de type REST comme [CouchDB (https://couchdb.apache.org/)](https://couchdb.apache.org/) pourrait suffire.

La logique métier réside dans la fourniture des classements des joueurs ou des équipes des joueurs avec différentes configurations.

## Refactoring

Avant de se lancer dans l'ajout de logique métier, j'ai fait une série de refactoring. L'idée est d'améliorer le code avant de le modifier.

J'ai donc :

- utilisé des noms plus pertinents pour les adapteurs :
  - les structures servant à la persistance sont suffixés par `Record` - par exemple `coachAdapter` devient `coachRecord`,
  - les structures servant à la transformation en JSON en lecture et en écriture sont suffixés par `JsonSerializer` - par exemple `coachAdapter` devient `coachJsonSerializer` ;
- créé des constructeurs pour les structures métier pour intégrer des contrôles métier dès la création ;
- renommé les fonctions fromCore des adapteurs en constructeurs.

Pour l'adapteur de serialisation JSON d'un joueur, nous sommes passés de ça :

```go
type coachAdapter struct {
  ID        uint   `json:"id"`
  Name      string `json:"name"`
  TeamName  string `json:"teamName,omitempty"`
  Email     string `json:"email"`
  NafNumber uint   `json:"nafNumber,omitempty"`
  Faction   string `json:"faction,omitempty"`
  Ready     bool   `json:"ready,omitempty"`
}

func coachAdapterFromCore(coach core.Coach) coachAdapter {
  return coachAdapter{ID: coach.ID, Name: coach.Name,
    TeamName: coach.TeamName, Email: coach.Email,
    NafNumber: coach.NafNumber, Faction: string(coach.Faction),
    Ready: coach.Ready,
  }
}

func (c coachAdapter) toCore() core.Coach {
  return core.Coach{ID: c.ID, Name: c.Name,
    TeamName: c.TeamName, Email: c.Email,
    NafNumber: c.NafNumber, Faction: core.Faction(c.Faction),
    Ready: c.Ready,
  }
}
```

```go
// coachJsonSerializer allow to dissociate Json Marshaling and Unmarshaling from core.Coach
type coachJsonSerializer struct {
  ID        uint   `json:"id"`
  Name      string `json:"name"`
  TeamName  string `json:"teamName,omitempty"`
  Email     string `json:"email"`
  NafNumber uint   `json:"nafNumber,omitempty"`
  Faction   string `json:"faction,omitempty"`
  Ready     bool   `json:"ready,omitempty"`
}

func newCoachJsonSerializer(coach core.Coach) coachJsonSerializer {
  return coachJsonSerializer{ID: coach.ID, Name: coach.Name,
    TeamName: coach.TeamName, Email: coach.Email,
    NafNumber: coach.NafNumber, Faction: string(coach.Faction),
    Ready: coach.Ready,
  }
}

func (s coachJsonSerializer) toCore(edition core.Edition) (core.Coach, error) {
  coach, err := core.NewCoach(s.Name, core.Faction(s.Faction), edition)
  if err != nil {
    return coach, err
  }
  coach.ID = s.ID
  coach.TeamName = s.TeamName
  coach.Email = s.Email
  coach.NafNumber = s.NafNumber
  coach.Ready = s.Ready
  return coach, nil
}
```

Les constructeurs sont simplement les fonctions de création de structures, conventionnellement préfixées par `New` (ou `new` si c'est un objet interne au paquet) suivi du nom de la structure. En cas de paramètre incompatible lors de la construction de la structure, une erreur est transmise.

L'utilisation des constructeurs des objets métiers dans les fonctions des adapteurs permet de prendre en compte les vérifications métiers sans les coder dans le paquet non métier.

## Ajout des classements de joueurs

### Métier

J'ai commencé par les classements de joueurs et en particulier les classements sur un critère simple d'une partie comme les nombres d'essais marqués ou encaissés. Il a fallu modifier les structures permettant de configurer les classements dans l'objet métier `Edition`. On avait une simple liste de critères qui permettaient l'affichage des classements, il a fallu créer une structure qui permet leurs calculs **puis** leurs affichages. En m'inspirant de [la boite à outils pour tournois]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1" >}}), j'ai préparé l'arborescence suivante :

```mermaid
classDiagram
  class SortInterface["sort.Interface"]
  Edition "1" *-- "many" RankingCriteria : has
  CoachRank "1" *-- "1" Coach
  CoachRanking "1" *--  "many" CoachRank
  CoachRanking --|> SortInterface
  Edition : +AddCoachRankingTd()
  Edition : +AddCoachRankingDefense()
  Edition :+AddCoachRankingCasualtiesTd()
  Edition :+RankCoach(rankingName, games)
  RankingCriteria : +string Label
  RankingCriteria : +RankingType Type
  RankingCriteria : +RankingField Field
  RankingCriteria : +bool Descending
  SortInterface: +Len() int
  SortInterface: +Swap(i, j int)
  SortInterface: +Less(i, j int) bool
  CoachRanking: +[]string ExtraInfoNames
  CoachRanking: +[]CoachRank CoachRanks
  CoachRanking: +[]bool ExtraInfoOrderIsDescending []bool
  CoachRanking: +Len() int
  CoachRanking: +Swap(i, j int)
  CoachRanking: +Less(i, j int) bool
  CoachRank: +Coach Coach
  CoachRank: +[]int ExtraInfos
```

La méthode `RankCoach` prend un nom de classement et un tableau de partie et renvoie un classement de joueur `CoachRanking`. L'idée est de :

1. Préparer les noms et les ordres de classement pour chaque critère du classement sélectionné ;
2. Analyser toutes les parties fournies et les critères pour ajouter aux joueurs les contenus spécifiques du classement sélectionné ;
3. Utiliser la fonction de tri intégrée au paquet `sort` car `CoachRanking` implémente l'interface de tri `sort.Interface`.

Cela permet de trier suivant des champs simples encaissés ou réalisés. On peut donc calculer le classement de celui qui a marqué le plus d'essai ou celui qui a encaissé le moins d'essai.

### Persistance et API

Une fois la partie métier réalisée, il a fallu modifier la partie persistance pour qu'elle prenne bien en compte les modifications de configuration.

Pour l'API, j'ai créé :

- une classe d'écriture de classement de joueur `CoachRankingJsonWriter` ;
- les endpoints de classement.

## Conclusion

On peut le voir l'étape la plus compliquée a été de prendre en compte la logique métier. Je trouve cela normal car l'application n'est plus un passe plat entre une API REST et une base de données mais apporte une plus-value aux utilisateurs. Ce n'est pas fini, puisqu'il reste le calcul des points. Dans l'application précédente (en PHP) le calcul des points était codée via une dizaine de classe et l'utilisateur choisissait la méthode de calcul avec un nom. En continuant à me baser sur les leçons de la boite à outils pour tournoi, l'idée est de proposer une méthode de calcul configurable.

Si vous souhaitez participer au développement, j'accueille les merge requests avec plaisir. Et si vous avez des projets perso, n'hésitez pas à les partager avec moi sur [X](https://x.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
