---
title: "À propos des nombres décimaux"
date: "2021-05-17"
draft: false
description: "ou pourquoi parfois 0.1 + 0.2 n'est pas égal à 0.3"
tags: ["nombre","base","decimaux"]
author: "Bertrand Madet"
---

Je vous propose aujourd'hui un petit retour au basique des nombres décimaux.

## Une petite expérience pour commencer

Je ne sais pas si vous avez déjà fait l'expérience, ouvrez votre outils de développement web (avec la touche `F12`) de votre navigateur web, allez dans l'onglet `console`.

Saisissez le "code" ci-dessous puis appuyez sur `Entrée`.

```0.15 + 0.15```

L'interprétateur JavaScript de votre navigateur affiche `0.3`.

Saisissez le "code" ci-dessous puis appuyez sur `Entrée`.

```0.1 + 0.2```

L'interprétateur JavaScript de votre navigateur affiche `0.30000000000000004` ! :exploding_head:

En fait, c'est déjà un petit miracle d’ingénierie que 0.1 + 0.1 donne 0.2

## 0.1

`0.1` est un [nombre décimal (https://fr.wikipedia.org/wiki/Nombre_d%C3%A9cimal)](https://fr.wikipedia.org/wiki/Nombre_d%C3%A9cimal), c'est à dire qu'il peut être écrit avec un nombre fini de chiffres après la virgule en base 10.

Seulement, les ordinateurs fonctionnent en base 2. Il faut décomposer les nombres avec des puissances de 2.

`0.25` en base 10 s'écrit en base 2 : `0.01`.

{{< math >}}
0\times 1 + 0\times \frac{1}{2} + 1\times\frac{1}{4}
{{< /math >}}

`0.375` en base 10 s'écrit en base 2 : `0.011`.

{{< math >}}
0\times 1 + 0\times \frac{1}{2} + 1\times\frac{1}{4} + 1\times\frac{1}{8}
{{< /math >}}

Aussi surprenant que cela puisse paraître `0.1` (1/10) ne peut pas s'exprimer exactement en notation binaire. De la même manière, qu'un tier ne peut pas s'exprimer en notation décimal.

## Mais comment on met des nombres à virgules dans des ordinateurs ?

Il y a plein de manière de faire !

### Décimal codé binaire

On pourrait coder chaque chiffre en base 10 avant et après la virgule avec 4 bits. Et on aurait quelquechose comme '0000.0001'. Cela s'appelle [Décimal codé binaire (https://fr.wikipedia.org/wiki/D%C3%A9cimal_cod%C3%A9_binaire)](https://fr.wikipedia.org/wiki/D%C3%A9cimal_cod%C3%A9_binaire). C'est pluôt simple mais cela n'est vraiment pas optimum en terme d'utilisation de la mémoire (pour chaque chiffre en base 10, on gâche 6 possibilités sur 16).

### Nombres à virgule fixe

Le principe est fixer le nombre de bits après la virgule. Cela veut dire que vous codez votre nombre en le multipliant par une puissance de deux fixe. Par exemple, si vous souhaitez utiliser 8 bits après la virgule, vous multiplierez votre nombre par 256 (2⁸). `0.5` donnerait `0000000001000000` (le même encodage que 128 en entier non signé).

{{< math >}}
0.5 \times 256 = 128
{{< /math >}}

Il y a plus de détails sur Wikipédia [:fr:(https://fr.wikipedia.org/wiki/Virgule_fixe)](https://fr.wikipedia.org/wiki/Virgule_fixe) et surtout [:gb: (https://en.wikipedia.org/wiki/Fixed-point_arithmetic)](https://en.wikipedia.org/wiki/Fixed-point_arithmetic).

Le problème c'est que cette représentation a une faible amplitude et une précision arbitraire.

### Nombres à virgule flottante

Les nombres à virgule flottante ont la caractéristique de pouvoir représenter une grande variabilité d'ordre de grandeur avec une précision relative (au sens où la précision est proportionnelle à la grandeur du nombre). Ce format est intégré dans la plupart des processeurs. C'est la solution retenue dans tous les languages de programmation. La représentation utilise la [norme IEEE 754 (https://fr.wikipedia.org/wiki/IEEE_754)](https://fr.wikipedia.org/wiki/IEEE_754). L'encodage des nombres est décomposé en trois parties :

- le signe sur 1 bit;
- l'exposant sur 8 bits pour les flottants sur 32 bits
- la mantisse sur 23 bits pour les flottants sur 32 bits

{{< math >}}
nombre = signe \times mantisse \times 2^{exposant}
{{< /math >}}

Il y a plus de détails sur Wikipédia [:fr:(https://fr.wikipedia.org/wiki/Virgule_flottante)](https://fr.wikipedia.org/wiki/Virgule_flottante) et surtout [:gb: (https://en.wikipedia.org/wiki/Floating-point_arithmetic)](https://en.wikipedia.org/wiki/Floating-point_arithmetic).

Comme la précision est relative, on peut avoir des pertes de précisions, si on ajoute des grands nombres. Par exemple, coder des transactions financières avec des nombres à virgule flottante sur 32 bits, peut amener des erreurs supérieures à un centime pour des centaines de milliers d'euros.

## Conclusion

Voici une introduction sur les nombres à virgules. C'est le genre de détail qu'il faut connaître quand on a des comportements inattendus. Sachant qu'en fonction du problème, il y a plusieurs solutions comme :

- augmenter la taille de la structure (passer des nombres flottants sur 32 bits à 64 bits) ;
- modifier le formatage des chiffres ;
- utiliser des représentations à précision arbitraire.

 Pour aller plus loin sur le sujet, je vous suggère le site [What Every Programmer Should Know About Floating-Point Arithmetic (https://floating-point-gui.de/)](https://floating-point-gui.de/).

Si vous avez des retours sur ce billet ou des découvertes sur les fondamentaux de la programmation, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

*edit du 24 mai 2021 : correction de fautes et autres coquilles.*
