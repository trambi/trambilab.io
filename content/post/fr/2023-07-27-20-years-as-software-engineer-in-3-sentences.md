---
title: "Mes 20 ans en tant que Software Engineer en 3 phrases"
date: "2023-07-27"
draft: false
description: " Forcément c'est résumé"
tags: ["perso","Software Craftmanship"]
author: "Bertrand Madet"
---

En juillet 2003, après neuf mois de recherche, je commençais mon premier emploi en tant qu'ingénieur logiciel. Vingt ans plus tard, mon employeur a changé mais j'occupe toujours le même poste.

Même si mes expériences sont personnelles, voici les enseignements que j'aimerai partager avec vous :

1. Le choix d'une technologie dépend plus de l'équipe et de l'organisation qui la supporte que d'autres critères techniques ;
2. Les tests automatiques vous aident ;
3. La qualité du logiciel est un travail de tous les instants.

## Le choix d'une technologie

Mon rapport aux langages informatiques a évolué. En juillet 2003, je connaissais le C du fait de mon apprentissage et le PHP sur mon initiative personnelle pour développer le site web de mon association. À l'époque dans ma tête, il y avait un classement objectif si ce n'est des langages au moins un classement des langages par gros domaine d'application. Aujourd'hui, après avoir appris plein de langages (le C++, le Java, le Python, le Scala, le Go, le JavaScript et le TypeScript), je suis moins catégorique, il y a certes des langages plus expressifs, mieux outillés, plus modernes. Mais les critères de choix principaux sont selon moi liées à l'équipe et l'organisation : 

- qu'est ce que votre équipe connaît ?
- qu'est ce que votre équipe a envie de faire ?
- qu'est-ce que votre organisation préconise ou autorise ?

Ce qui est vrai pour un langage informatique est vrai pour un framework de développement ou un moteur de base de données.

## Les tests automatiques

Mon rapport aux tests a aussi énormément évolué : de sujet inconnu 
à support en passant par corvée. Pendant très longtemps, je n'ai pas fait de tests automatiques. Ce qui était dommage car j'avais malgré tout tendance, à faire des refactoring voire des restructurations de code assez souvent, cela se finissait par des sessions de debug assez longues avec des printf. Aujourd'hui même pendant un hackathon j'ai envie d'écrire des tests car cela me permet de fixer mentalement les spécifications et l'interface d'une fonctionnalité.

## Qualité et discipline

J'ai eu aussi (et surtout) l'opportunité de voir le temps œuvrer sur des logiciels, j'ai vu :

- dégrader la qualité de certains logiciels super sains ;
- certains logiciels à moitié pourris s'améliorer ;
- des logiciels sains le rester ;
- des logiciels pourris le rester.

Le facteur déterminant est pour moi : la discipline de l'équipe à travailler pour la qualité logicielle. Un logiciel n'est pas condamné à être et à rester un pot de pus, vous pouvez améliorer les choses en suivant les conseils d'Oncle Bob, de Martin Fowler et d'autres sages.

## Conclusion

Depuis ma découverte tardive (en école d'ingénieur), je suis passionné par la programmation. Quand je dis passionné, cela m'arrive de coder le soir pour des projets personnels, si je ne code "pas assez" pendant le travail.

Je reste curieux et j'ai l'impression qu'une vie ne suffira pas à explorer le vaste domaine de l'informatique.

Et vous quelles sont les enseignements que vous tirez de vos années de développement ? Et vous ? N'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

