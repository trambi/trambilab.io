---
title: "Une heatmap en HTML"
date: "2023-01-23"
draft: false
description: "Avec l'élément table"
tags: ["front-end"]
author: "Bertrand Madet"
---

Dans [mon billet précédent]({{< ref "/post/fr/2023-01-12-learn-by-testing-style-in-jest.md">}}), je vous indiquais que j'avais codé une heatmap en React. Au delà de l'implémentation et des difficultés de tester le style, voici ce que j'ai appris sur les heatmaps et leur représentation en HTML.

## Heatmap

Une heatmap (ou carte de chaleur :fr:) est une représentation graphique de trois dimensions de données. Deux dimensions vont être retranscrites sur un plan (un écran, une feuille), la troisième dimension va être retranscrite sous forme de couleur. J'imagine que l'on appelle cela une heatmap car souvent l'échelle de couleur utilisée va du jaune au rouge, les formes rouge et jaune variables rappellent un feu :fire:.

Cela permet d'appréhender les données en distinguant des formes de couleurs. Notre cerveau a une plus grande facilité à repérer les "formes de couleur" plutôt que les "formes de chiffres proches". Une heatmap peut servir pour représenter :

- les clics sur un écran et voir les zones ayant "attirés" les clics ;
- la densité de population en France et voir les zones les moins peuplées et les zones les plus peuplées.

## Comment décrire une heatmap en HTML ?

Dans le cas où les deux dimensions sont discrètes (entiers ou catégories), une heatmap peut être considérée comme un tableau où les valeurs ont été remplacées par des couleurs. On pourrait par exemple le représenter dans un tableur avec une mise en forme conditionnelle de couleur basée sur la valeur des cellules.

L'élément [table](https://developer.mozilla.org/fr/docs/Web/HTML/Element/table) en HTML permet d'afficher un tableau de données. Il accueille d'autres éléments pour représenter des lignes, des cellules, mais aussi le corps du tableau, le pied ... Bref, il est très bien géré par les navigateurs. Le HTML est un langage de balisage qui permet de décrire un document (:teacher: on dit que c'est un langage descriptif). Le "code" HTML décrit un document dont l'affichage du document est de la responsabilité du navigateur.

```mermaid
graph LR;
    html[Fichier HTML]-->rendered[Fichier affiché par le navigateur]
    browsercss[Feuille de style interne du navigateur]-.->rendered
    css[Feuille de style]-->|surcharge|browsercss
```

Comme les éléments constituant un tableau sont bien gérés par les navigateurs, la feuille de style interne pour ces éléments est bien faite et basée sur l'expérience des navigateurs.

## Conclusion

En résumé, l'utilisation de l'élément `table` pour décrire une heatmap "discrète" dans un document HTML ou une application web est une solution efficace. Pour les autres types d'heatmaps comme celles basées sur la géographie, il est nécessaire d'utiliser d'autres éléments tels que les images vectorielles pour représenter correctement les zones.

Bien que les heatmaps puissent également être réalisées avec des éléments tels que `div` et la disposition en grille, cela peut rendre le code HTML moins lisible et plus difficile à maintenir pour les développeurs. Il est donc préférable d'utiliser des éléments qui conviennent le mieux au type de contenu que l'on souhaite créer. Cette idée, où l'on va chercher à trouver la balise que décrit le mieux le sens du contenu, est appelée "HTML sémantique".

Si vous avez des anecdotes ou des conseils sur le HTML sémantique, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
