---
title: "Coder un module Drupal - partie 3"
date: "2022-02-16"
draft: false
description: "Les leçons d'un échec au déploiement"
tags: ["projet perso","php","jeu", "drupal"]
author: "Bertrand Madet"
---

Après le [deuxième billet de cette série]({{< ref "/post/fr/2022-02-09-code-drupal-module-part-2.md" >}}), j'ai essayé d'utiliser ce module sur le site de mon association. J'ai dû m'y reprendre à deux fois pour l'installer. Il m'a fallu quatre modifications pour y parvenir. Je propose de tirer des enseignements de cette mésaventure.

## 4 erreurs

Il m'a fallu modifier ou supprimer quatre (4) fichiers alors que le module s'installait et fonctionnait sur ma configuration de développement.

### Fichier de route en trop

Le fichier de configuration du module `just_rank_games.routing.yml` était obsolète et indiquait un contrôleur inexistant. Il s'agit sans doute d'un oubli de faire `git rm` au lieu de `rm` de ce fichier.

### Dépendances au module PHP bc

J'avais utilisé le module PHP de calcul de précision arbitraire `bc` pour les calculs du classement. Pour éviter d'avoir une dépendance à `bc` (qui pourrait être absente), j'ai réécrit les calculs en utilisant les propriétés surprenantes de PHP.

```php
$result = (str)('4.01' + '2.1');
print("$result");
// Va afficher '6.11'
```

Et en plus étrangement, il n'y pas d'erreur d'arrondi (typique des nombres à virgules flottantes cf. [le billet sur les nombres décimaux]({{< ref "/post/fr/2021-05-17-a-propos-des-nombres-decimaux.md">}}).

A comparer à JavaScript :

```javascript
console.log(Number('4.01')+Number('2.1'));
// Va afficher 6.109999999999999
```

La relecture de l'[excellent site (https://floating-point-gui.de)](https://floating-point-gui.de), me fait penser qu'il faudra que j'utilise la fonction `number_format` pour arrondir.

Il faut noter que j'ai pu identifier la dépendance à l'extension PHP `bc` comme problèmatique grâce à mon deuxième environnement de développement.

### Références constantes dans le bloc

Il a fallu que j'opère cette modification :

```diff
 return [
    '#type' => 'table',
-    '#header' => $rankings[header],
-    '#rows' => $rankings[rows],
+    '#header' => $rankings['header'],
+    '#rows' => $rankings['rows'],
  ];
```

Dans mon environnement de développement, je n'avais même pas d'avertissement. Sur l'environnement cible, cela affiche un avertissement et cela fait échouer l'affichage du bloc.

### Appel à la mauvaise fonction

```diff
        $elements['condition_max_length'] = [
            '#type' => 'number',
            '#title' =>$this->t('Condition maximum length'),
-            '#default_value' => $this->getSettings('condition_max_length'),
+            '#default_value' => $this->getSetting('condition_max_length'),
            '#required' => TRUE,
            '#description' => $this->t('Maximum length for the condition in characters'),
            '#min'=> '1',
```

Les méthodes `getSetting` et `getSettings` existent. La différence c'est que `getSettings` ne prend pas de paramètre et retourne un tableau associatif avec toutes les configurations alors que `getSetting` prend un paramètre et renvoit une chaîne de caractère.

Ce bug apparaît six (6) fois dans le fichier de définition de stockage du champ `Statement`. Une erreur sur l'environnement cible est émise à la création d'une instance de l'entité `Criteria` et non au moment de l'appel du code problématique.

Encore une fois, l'erreur n'apparaissait pas dans l'environnement de développement.

## Leçons

### Environnement de développement et environnement cible

Initialement, j'utilisais pour mes développement Drupal 8.9 avec PHP 8 sur Debian dans Windows Linux Subsystem avec une base de données sqlite. Cela permettait de développer sous Windows avec Visual Code (grâce à l'extension `WSL remote`) et sans utiliser Docker.

L'environnement cible est un serveur mutualisé Linux avec une base de données PostgreSQL.

La dernière erreur est liée au fait d'utiliser sqlite. En effet, il me semble que les contraintes ne sont pas implémentés dans sqlite.

Mon premier environnement de développement était trop éloigné de l'environnement cible :

- Versions différentes de PHP ;
- Extensions différentes de PHP ;
- Bases de données différentes.

J'ai adapté mon environnement de développement pour utiliser podman et l'image officiel Drupal 8.9 de Docker avec une base de données PostgreSQL.

**Il faut un environnement de développement le plus proche possible de l'environnement cible.**

### Limite des tests unitaires

J'avais codé les tests unitaires au niveau de la logique métier et il n'y aucun problème au niveau logique métier. Les problèmes sont apparus là les tests unitaires ne veillaient pas.

Pour tester les éléments incriminés, il faudrait des tests d'intégration ou des tests de bout en bout. J'avoue que je n'avais ni la compétence ni la motivation pour me lancer dans des tests de ce type.

**Les bugs apparaissent là où ce n'est pas testé.**

Mais pour supprimer la dépendance à `bc`, j'étais très content d'avoir opéré un TDD.

**Les tests unitaires ne permettent pas de tout détecter.**

## Conclusion

Le développement n'est pas fini, il reste des raffinements au niveau des accès et des liens pour ajouter facilement des instances d'entités. Cela m'a permis d'affiner mon environnement de développement Drupal.

Si vous avez des conseils sur ce module Drupal ou le développement Drupal, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

Si vous souhaitez contribuer, le dépôt est sur Gitlab : [https://gitlab.com/trambi/drupal_module_just_rank_games](https://gitlab.com/trambi/drupal_module_just_rank_games) et les contributions sont les bienvenues.
