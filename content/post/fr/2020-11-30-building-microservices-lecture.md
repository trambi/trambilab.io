---
title: "Building microservices - Fiche de lecture"
date: "2020-11-30"
draft: false
description: "Livre de Sam NEWMAN sur les micro-services"
tags: ["fiche de lecture","devops","micro-services"]
author: "Bertrand Madet"
---
*Fiche de lecture initialement publiée en juillet 2019 sur LinkedIn et légèrement modifiée pour ce blog*

# A qui s'adresse ce livre

Si vous vous intéressez aux architectures logiciels, ce livre vous apportera une vision de référence sur l'architecture microservices.

# Ce qu'il faut retenir

- L'un des premiers avantages de ce type d'architecture est que les compromis architecturaux se font à petite échelle

- Cela permet d'avoir des solutions adaptées à l'organisation qui l'utilise - c'est à dire que chaque entité de l'organisation gère l'implémentation de son métier.

- Le rôle de l'architecte logiciel doit évoluer pour répondre aux défis des microservices.

# Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: C'est un livre de référence pour les architectures microservices| :-1: De par le sujet, le contenu d'un livre est amené à se périmer rapidement|
| :+1: La structure de chaque chapitre permet de les lire individuellement||
| :+1: Un cas concret est repris sur chaque chapitre||

L'auteur commence par expliquer le concept de microservices puis le rôle d'un architecte dans une telle architecture. Ensuite chaque chapitre décrit les différentes solutions dans un domaine avec leurs avantages et leurs inconvénients. Les différents domaines vont du déploiement à l'organisation en passant par la sécurité.

Les chapitres sur les microservices (1), le rôle de l'architecte (2) et  les tests (7) suffisent à "rentabiliser" l'achat et la lecture du livre.

# Détails

- Building microservices
- Langue : :gb: Anglais
- ISBN-13 : 978-1492034025
- Auteurs : Sam NEWMAN

Pas traduit :cry: en français :fr:.