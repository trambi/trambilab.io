---
title: "Tracer les accès à une API Gateway avec Terraform"
date: "2022-11-01"
draft: false
description: "AWS par la face nord"
tags: ["outils","REST API","cloud","terraform"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Décrire une API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Décrire une Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).

-------

La semaine dernière, j'ai essayé d'ajouter la journalisation à une API Gateway. Les journaux permettent de tracer les appels à l'API Gateway et surtout les erreurs lors du traitement. L'idée est d'utiliser [CloudWatch (https://aws.amazon.com/fr/cloudwatch/)](https://aws.amazon.com/fr/cloudwatch/) pour réceptionner les journaux d'accès.

## Ce que décrit la documentation

La procédure pour ajouter la journalisation CloudWatch avec la console AWS est décrite dans la documentation [Configuration de la journalisation CloudWatch pour une API REST dans API Gateway (https://docs.aws.amazon.com/fr_fr/apigateway/latest/developerguide/set-up-logging.html)](https://docs.aws.amazon.com/fr_fr/apigateway/latest/developerguide/set-up-logging.html).

Pour écrire les journaux dans CloudWatch, il faut avoir des permissions spécifiques. Là où dans mon billet sur l'[accès S3 via API Gateway]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}), le rôle était lié à une intégration d'une méthode, le rôle doit être affecté à l'API Gateway tout entière voire à toutes les API Gateway d'un compte.

## Ressources AWS pour Terraform

Malheureusement, les concepts manipulés par la console AWS sont différents des ressources Terraform pour AWS. Il y a tout d'abord une ressource `Log group` (ou :fr: groupe de journalisation) qui permettent de grouper les journaux. Il faut ensuite trois ressources IAM pour avoir un rôle qui a octroie le droit de pousser les journaux dans CloudWatch (une pour le rôle, une pour les permissions et une pour l'association entre le rôle et les permissions). Pour finir, les paramétrages de journalisation d'API Gateway sont distribués sur deux ressources :

- le `stage` de l'API Gateway qui représente une version d'API (parfois mal traduit en :fr: par `scène` dans la documentation AWS) ;
- `compte`, une nouvelle ressource :exploding_head:.  

```mermaid
graph LR;
    subgraph "API Gateway REST API"
        stage[Stage d'une API REST]
        account[Compte d'API REST]
        restapi[API REST]
    end
    subgraph "CloudWatch"
        loggroup[Groupe de journalisation]
    end
    subgraph "IAM"
        role[Rôle IAM de l'API Gateway]
        policy[Politique IAM pour autoriser à pousser les journaux dans CloudWatch]
    end
    stage-.->restapi
    stage-.->loggroup
    account-.->restapi
    account-.->role
    role-.-policy
```

On a donc sept ressources à associer pour réussir cette journalisation, assez classique pour du Terraform avec AWS.

## En Terraform s'il vous plaît

On commence par l'API Gateway :

```HCL
# API Gateway
resource "aws_api_gateway_rest_api" "front" {
  name = "front"
}

# Déploiement simplifié il faudrait ajouter le trigger
resource "aws_api_gateway_deployment" "front" {
  rest_api_id = aws_api_gateway_rest_api.front.id
  lifecycle {
    create_before_destroy = true
  }
}
```

On ajoute le groupe de journaux CloudWatch :

```HCL
# Cloudwatch log group associated to the API Gateway front
resource "aws_cloudwatch_log_group" "front-api-gw" {
  name              = "/aws/api_gw/${aws_api_gateway_rest_api.front.name}"
  retention_in_days = 30
}
```

On commence par décrire la politique et le rôle pour que l'API Gateway puisse pousser des journaux dans CloudWatch.

```HCL
# Create push to cloudwatch policy
resource "aws_iam_policy" "push-logs" {
  name        = "push-logs"
  description = "Policy to allow push logs to CloudWatch"

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:DescribeLogGroups",
                "logs:DescribeLogStreams",
                "logs:PutLogEvents",
                "logs:GetLogEvents",
                "logs:FilterLogEvents"
            ],
            "Resource": "*"
        }
    ]
  })
}

# API Gateway Role
resource "aws_iam_role" "front-api-gateway" {
  name = "s3-api-gateway-role"
  # Create Trust Policy for API Gateway
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
} 
  EOF
}

# Link Push logs policy and the API Gateway role
resource "aws_iam_role_policy_attachment" "front-api-gateway-push-logs" {
  role       = aws_iam_role.front-api-gateway.name
  policy_arn = aws_iam_policy.push-logs.arn
}
```

On décrit ensuite le compte de l'API Gateway pour lier toutes les API Gateway avec le rôle :

```HCL
resource "aws_api_gateway_account" "front" {
  cloudwatch_role_arn = aws_iam_role.front.arn
}
```

On finit en décrivant la version de l'API Gateway avec le groupe de journalisation cible et le format. J'ai choisi le format qui correspond au format [CLF (https://httpd.apache.org/docs/current/logs.html#common)](https://httpd.apache.org/docs/current/logs.html#common).

```HCL
resource "aws_api_gateway_stage" "front" {
  deployment_id = aws_api_gateway_deployment.front.id
  rest_api_id   = aws_api_gateway_rest_api.front.id
  stage_name    = "monStage"
  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.front-api-gw.arn
    format = "$context.identity.sourceIp $context.identity.caller  $context.identity.user [$context.requestTime] \"$context.httpMethod $context.resourcePath $context.protocol\" $context.status $context.responseLength $context.requestId $context.extendedRequestId"
  }
}
```

## Conclusion

Les documentations sur AWS avec Terraform sont plutôt difficiles à trouver. La gageure a été de faire le lien entre la documentation et les ressources Terraform. J'ai trouvé l'association entre la configuration du rôle AWS dans la console AWS et la ressource `account` Terraform en fouillant chacune des ressources de API Gateway (les ressources commençant par `aws_api_gateway_`). La définition du format précis des lignes de journalisation a aussi été surprenant pour moi. Après réflexion, cela est logique vu que cela permet une maîtrise totale.

La description d'une API Gateway pourrait être factorisé sous forme d'un module Terraform, pour garder la cohérence des infrastructures à l'échelle d'un projet ou d'une entité.

Si vous avez des questions ou des retours d'expériences sur l'utilisation d'AWS avec Terraform, je suis intéressé. N'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

-------

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Décrire une API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Décrire une Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).
