---
title: "Utiliser les sites d'apprentissage par le jeu"
date: "2021-12-23"
draft: false
description: "progresser en jouant"
tags: ["dojo","Software Craftmanship","amélioration"]
author: "Bertrand Madet"
---

Je vous propose d'échanger autour des sites de défi de codes et de ma manière de les utiliser. Il y a une multitude de sites et je vais me cantonner à ceux que j'ai testé :

- [CodeWars (https://www.codewars.com/)](https://www.codewars.com/);
- [CodinGame (https://www.codingame.com/)](https://www.codingame.com/);
- [RootMe (https://www.root-me.org/)](https://www.root-me.org/).

Il suffit de taper de `codewars alternative` dans votre moteur de recherche préféré pour trouver une liste d'autres sites d'apprentissage par le jeu.

## CodeWars

[CodeWars](https://www.codewars.com/) propose des défis de programmation. Le principe est simple vous répondez à deux questions :

- Quel langage de programmation souhaitez-vous utiliser ?
- Quel est le but de ce défi ? Progresser, s'entraîner en répétant ...

Et le site va vous recommander des défis adaptés. L'environnement de développement proposé pour résoudre le challenge contient quatre zones :

- les spécifications du défi ;
- les tests du défi ;
- la zone du code pour répondre au défi ;
- la zone du code pour tester le code ci-dessus.

Vous alternez ensuite les phases de codage, de lancement de vos tests, de lancement des tests du défi, jusqu'à ce que les tests du défi passent et que vous soyez satisfait du résultat.

Les points forts et faibles que j'y vois :

- :+1: Énorme choix de langage de programmation - une trentaine du C au TypeScript en passant par le bash;
- :+1: Progressivité des difficultés - tous les défis ont une valeur `kuy` qui va de 8 kuy (trivial) à 1 kuy (simulation d'un processeur moderne - le genre de problème qui peut ruiner vos vacances ou vos nuits pendant un mois);
- :+1: Possibilité de soumettre des "traductions" de défi ou des défis ;
- :+1: Développement des tests intégré dans l'environnement de développement ;
- :-1: Uniquement en anglais :gb: ;
- :-1: Les tests ne font pas partie de la solution - ils ne sont pas disponibles quand on regarde une solution, c'est dommage parce qu'à mon sens, cela illustre la démarche du développeur.

### Progresser

En jouant le jeu, on apprend beaucoup à essayer différentes approches sur un défi. Il est très fréquent d'avoir un onglet `CodeWars` et un onglet sur la documentation du langage de programmation ou de ses bibliothèques standards.

:warning: la difficulté de certains défis peut être algorithmique, ce qui signifie que le plus dur sera de savoir quoi coder (et pas comment le coder). Ce genre de défi va vous permettre de progresser non pas sur le langage de programmation mais sur vos connaissances des algorithmes et des structures de programmation.

L'environnement de développement permet d'adopter la méthodologie **T**est **D**riven **D**evelopment - cf. les billets où je vous parle du [TDD]({{< ref "/tags/tdd" >}}) donc servez-vous en pour pratiquer.

### Support pour coding dojos à distance

Comme je vous l'avais indiqué dans mon billet [Mon histoire des coding dojos]({{< ref "/post/fr/2021-03-14-my-coding-dojo-history.md" >}}), la variété des défis proposés et l'environnement de développement rend très pratique l'utilisation de CodeWars pour un coding dojo à distance.

Il y a des étapes préparatoires :

- :warning: Il faut veiller aux langages de programmation que l'on souhaite proposer - tous les langages ne sont pas disponibles pour tous les défis ;
- :warning: Il faut faire attention à la difficulté des challenges, même à difficulté `5 kuy` cela peut prendre plus que l'heure dédiée au dojo pour résoudre.

Comme indiqué ci-dessus, l'interface permet d'utiliser le `TDD`, pour le pair programming cela dépend de votre logiciel de visio-conférence. Il faut pouvoir créer un salon pour le dojo et des salons dédiés à chaque duo.

## CodinGame

[CodinGame](https://www.codingame.com/) propose une expérience calée sur les jeux vidéo. Le principe est de suivre un parcours est plus balisé que CodeWars mais plus varié puisqu'il y a :

- des `clashs` - défis opposants jusqu'à douze adversaires relativement simples limités à quinze minutes, il y en a trois types :
  - rapides, le classement est basée sur la rapidité de soumission,
  - courts, le classement est basée sur le nombre de caractère,
  - reverse, il n'y a d'énoncé juste les tests ;
- des `puzzles` qui sont des défis d'implémentations.

Le site vous propose un environnement de développement avec quatre zones :

- les spécifications du défi ;
- l'illustration du défi avec une animation type jeux-vidéo ;
- les tests du défi ;
- la zone du code pour répondre au défi.

Les points forts et faibles que j'y vois :

- :+1: Grand choix de langage de programmation (25);
- :+1: Les clashs qui permettent de limiter son investissement à quinze minutes ;
- :+1: L'illustration des problèmes qui permet d'embarquer des non spécialistes ;
- :+1: Partiellement en français :fr: et anglais :gb: ;
- :-1: Les difficultés sont limités à simple, moyen et difficile ;
- :-1: Les tests ne semblent pas modifiables.

### Progresser

L'approche plus variée permet d'être moins monotone que celle CodeWars. Cela vaut aussi pour l'investissement temporel, là où à certains niveaux de CodeWars, il faudra passer entre deux et cinq heures pour décrocher des points, les clashs et le puzzle du moment peuvent permettre de décrocher des points plus rapidement. Les bénéfices sont à peu près identiques :

- amélioration dans la connaissance des langages de programmation ;
- amélioration de la "culture algorithmique".

### Clash privé

Il est possible d'organiser un clash privé jusqu'à cent invités. Cela permet d'organiser un évènement plus ludique que les dojos. Pour la fin de l'année, nous avons organisé un petit évènement réunissant une dizaine de participants en réel et en visio-conférence et en une heure, nous avons pu faire trois clashs et surtout beaucoup nous amuser.

Il y a des étapes préparatoires :

- :warning: Il faut veiller à ce que les participants soient déjà inscrits, le parcours d'initiation prend quelques minutes ;
- :warning: Les challenges privées semblent vraiment simple ce qui axe énormément le classement sur le critère de classement.

## RootMe

[RootMe](https://www.root-me.org/) est un site plus axée sur la sécurité informatique, en se focalisant sur l'exploitation de faille. Je l'ai utilisé il y a plus de temps, cette section sera moins détaillée.

Je m'étais servi de ce site pour mettre en pratique Python - que je commençais à apprendre. J'avais trouvé intéressant à quel point l'apprentissage d'un langage se nourrissait de la compréhension des failles de sécurité à exploiter.

## Conclusion

Les sites d'apprentissage par le jeu sont des outils très intéressants. Ils peuvent faciliter votre montée en compétence en fournissant un environnement où l'on peut faire des essais. L'aspect communautaire est important aussi : je pense que l'on apprend mieux quand on discute de nos problèmes ou en regardant les solutions d'autrui. :warning: Ils peuvent être très chronophages si l'on se prend au jeu.

Si vous avez des retours ou des conseils sur des sites d'apprentissage par le jeu, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
