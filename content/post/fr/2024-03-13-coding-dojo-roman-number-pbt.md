---
title: "Coding dojo : Nombre romain"
date: "2024-03-13"
draft: false
description: "en testant des propriétés"
tags: ["dojo","Software Craftmanship","test","tdd"]
author: "Bertrand Madet"
---

Je vous propose un kata classique des coding dojos : la transformation en nombre romain. L'idée est de vous proposer des cas de tests pour vous facilitez la mise en pratique du TDD (c'est à vous d'implémenter le code qui passe les tests). Pour rendre la chose plus intéressante, nous allons utiliser les tests de propriétés ou `PBT` (:gb: **P**roperties **B**ased **T**esting).

## Transformation en nombre romain

Le but est de coder une fonction qui prend un entier en entrée et retourne la chaîne de caractères correspondant au nombre en notation romaine. Le nombre en entrée pourra prendre une valeur entre 1 et 3000.

La numérotation romaine suit les règles suivantes :

1. Le symbole 'I' vaut 1 ;
1. Le symbole 'V' vaut 5 ;
1. Le symbole 'X' vaut 10 ;
1. Le symbole 'L' vaut 50 ;
1. Le symbole 'C' vaut 100 ;
1. Le symbole 'D' vaut 500 ;
1. Le symbole 'M' vaut 1000 ;
1. Chaque chiffre d'un nombre doit être traité séparément ;
1. Les symboles "I", "X", "C" et "M" peuvent être répétés au plus trois fois d'affilé ;
1. Les symboles "V" "L" et "D" ne peuvent pas être répétés ;
1. "I" peut être soustrait une seule fois de "V" et "X" ;
1. "X" peut être soustrait une seule fois de "L" et "C" ;
1. "C" peut être soustrait une seule fois de "D" et "M" ;
1. Les symboles "V" "L" et "D" ne peuvent pas être soustraits.

## Properties based testing

Je vous avais déjà parlé du PBT, lors de mon article [Coding dojo : Factorisation en nombres premiers - partie 2]({{< ref "/post/fr/2023-02-28-coding-dojo-prime-number-factorization-part2.md">}}). Le principe est de générer les entrées aléatoirement et de faire passer les tests avec les entrées générées. La définition des propriétés est une gymnastique un peu particulière : il est plus habituel de fonctionner avec des exemples.

Bien que le concept ait été popularisé avec Haskell (et la bibliothèque Quickcheck), j’illustrerai mes propriétés avec des tests TypeScript utilisant, entre autre la bibliothèque [fast-check (https://github.com/dubzzz/fast-check)](https://github.com/dubzzz/fast-check).

```typescript
import {describe, expect, test } from 'bun:test';
import fc from 'fast-check';
import { toRoman } from './roman';

const ALLOWED_VALUE=fc.integer({min:1,max:3000});

describe("toRoman",()=>{
  // Les tests iront là
});
```

## Propriétés de la transformation en nombre romain

La première propriété est énoncée dans la première phrase de description du problème : Pour tout nombre entre 1 et 3000, le résultat est une chaîne de caractères de longueur minimale 1.

```typescript
  test("given integer from 1 to 3000, it should return a string with a length greater than 1",()=>{
    fc.assert(
      fc.property(ALLOWED_VALUE,(data:number)=>{
          const result = toRoman(data);
          expect(result).toBeString();
          expect(result.length).toBeGreaterThanOrEqual(1);
      })
    );
  });
});
```

On va s'attaquer à la conversion des nombres à un symbole : 1, 5, 10, 50, 100, 500 et 1000.

```typescript
test.each([
    [1,'I'],
    [5,'V'],
    [10,'X'],
    [50,'L'],
    [100,'C'],
    [500,'D'],
    [1000,'M'],
  ])("given %d should return %s",(input:number, expected:string)=>{
    expect(toRoman(input)).toEqual(expected);
  });
```

Oui cela ressemble à des tests classiques, mais rassurez-vous nous allons repartir sur des propriétés plus claires (enfin je l'espère). Le résultat doit toujours être composé uniquement des symboles ci-dessus. Ce genre d'invariant est une bonne base de propriété.

```typescript
test("given integer from 1 to 3000, it should return string containing only 'I', 'V', 'X', 'L', 'C', 'D' and 'M'",()=>{
    fc.assert(
      fc.property(ALLOWED_VALUE,(data:number)=>{
        expect(toRoman(data)).toMatch(/^[IVXLCDM]+$/);
      })
    );
  });
```

La règle `Chaque chiffre d'un nombre doit être traité séparément ;` indique de mon point de vue que la conversation d'un nombre est égale à la concaténation :

- de la conversion du chiffre des milliers, multiplié par 1000;
- de la conversion du chiffre des centaines, multiplié par 100;
- de la conversion du chiffre des dizaines, multiplié par 10 ;
- de la conversion du chiffre des unités.

La conversion de 53 (`LIII`) est égale à la conversion de 50 (`L`) suivi de la conversion de 3 (`III`). Le test ci-dessus décompose la valeur en entrée en tableau de millier, centaine, dizaine et unité puis concatène le résultat.

```typescript
test("given integer from 1 to 3000, it should be decomposable by digits",()=>{
    fc.assert(
      fc.property(ALLOWED_VALUE,(data:number)=>{
        const values = (''+data).split('').reverse().map((value,index)=>(+value)*10**index);
        const recomposed = values.reduceRight((acc,cur)=>acc+(cur?toRoman(cur):''),'');
        expect(toRoman(data)).toEqual(recomposed);
      })
    );
  });
```

La règle `Les symboles "I", "X", "C" et "M" peuvent être répétés au plus trois fois d'affilé ;` peut se vérifier avec 4 expressions rationnelles.

```typescript
test("given integer from 1 to 3000, it should return string with no more than three times 'I','X','C','M'",()=>{
    fc.assert(
      fc.property(ALLOWED_VALUE,(data:number)=>{
        const result = toRoman(data)
        expect(result).not.toMatch(/IIII/);
        expect(result).not.toMatch(/CCCC/);
        expect(result).not.toMatch(/XXXX/);
        expect(result).not.toMatch(/MMMM/);
      })
    );
  });
```

La règle `Les symboles "V" "L" et "D" ne peuvent pas être répétés ;` peut se vérifier avec 3 expressions rationnelles et se décline dans le test suivant :

```typescript
test("given integer from 1 to 3000, it should return string without repetition of 'V', 'L' and 'D'",()=>{
    fc.assert(
      fc.property(ALLOWED_VALUE,(data:number)=>{
        const result = toRoman(data)
        expect(result).not.toMatch(/VV/);
        expect(result).not.toMatch(/LL/);
        expect(result).not.toMatch(/DD/);
      })
    );
  });
```

Les règles sur les soustractions peuvent se vérifier avec des expressions rationnelles.

```typescript
test("given integer from 1 to 3000, it should return string with only one subtraction of `I` to `V` or `X`",()=>{
    fc.assert(
      fc.property(ALLOWED_VALUE,(data:number)=>{
        const result = toRoman(data)
        expect(result).not.toMatch(/IIV/);
        expect(result).not.toMatch(/IIX/);
        expect(result).not.toMatch(/IL/);
        expect(result).not.toMatch(/IC/);
        expect(result).not.toMatch(/ID/);
        expect(result).not.toMatch(/IM/);
      })
    );
  });
  test("given integer from 1 to 3000, it should return string with only one subtraction of `X` to `L` or `C`",()=>{
    fc.assert(
      fc.property(ALLOWED_VALUE,(data:number)=>{
        const result = toRoman(data)
        expect(result).not.toMatch(/XXL/);
        expect(result).not.toMatch(/XXC/);
        expect(result).not.toMatch(/XD/);
        expect(result).not.toMatch(/XM/);
      })
    );
  });
  test("given integer from 1 to 3000, it should return string with only one subtraction of `C` to `D` or `M`",()=>{
    fc.assert(
      fc.property(ALLOWED_VALUE,(data:number)=>{
        const result = toRoman(data)
        expect(result).not.toMatch(/CCD/);
        expect(result).not.toMatch(/CCM/);
      })
    );
  });
  test("given integer from 1 to 3000, it should return string without subtraction of `V`",()=>{
    fc.assert(
      fc.property(ALLOWED_VALUE,(data:number)=>{
        const result = toRoman(data)
        expect(result).not.toMatch(/VX/);
        expect(result).not.toMatch(/VL/);
        expect(result).not.toMatch(/VC/);
        expect(result).not.toMatch(/VD/);
        expect(result).not.toMatch(/VM/);
      })
    );
  });
  test("given integer from 1 to 3000, it should return string without subtraction of `L`",()=>{
    fc.assert(
      fc.property(ALLOWED_VALUE,(data:number)=>{
        const result = toRoman(data)
        expect(result).not.toMatch(/LC/);
        expect(result).not.toMatch(/LD/);
        expect(result).not.toMatch(/LM/);
      })
    );
  });
  test("given integer from 1 to 3000, it should return string without subtraction of `D`",()=>{
    fc.assert(
      fc.property(ALLOWED_VALUE,(data:number)=>{
        const result = toRoman(data)
        expect(result).not.toMatch(/DM/);
      })
    );
  });
```

## Conclusion

En suivant très directement les règles de l'énoncé et en les déclinant en test, on teste l'implémentation en 120 lignes et avec des valeurs variées. De quoi nous donner plus confiance qu'avant une prise d'échantillon manuelle. On peut certes envisager d'autres propriétés, comme le fait que la conversion d'un nombre est toujours différente de celle du nombre immédiatement suivant.

Si vous souhaitez partager sur des coding dojo, le TDD ou le PBT, contactez moi [sur X](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169
