---
title: "Tester le style avec Jest"
date: "2023-01-12"
draft: false
description: "A vos risques et périls"
tags: ["front-end","test"]
author: "Bertrand Madet"
---

La semaine dernière, j'ai codé une heatmap en React et j'ai eu pas mal de problème pour tester que la gestion des couleurs était correcte. J'utilise Jest comme moteur de test pour tester mon logiciel. Cela m'a permis de comprendre des choses sur jest et les tests de style.

## Jest

Jest est un moteur de test pour JavaScript et TypeScript, il utilise Node.js pour fonctionner. Ce qui signifie que les tests et le code, que Jest appelle, sont exécutés par Node.js. Pour une application web qui est censée fonctionner sur un navigateur cela entraîne quelques passages périlleux :

- Le contenu et l'organisation du document web à afficher (**D**ocument **O**bject **M**odel) doit être interprété ;
- Les fonctions et les classes qui existent dans un navigateur mais pas dans Node.js doivent être implémentées ;
- La prise en compte des feuilles de style `css`.

Jest a un environnement spécifique pour le test des codes destinés à être exécuté côté navigateur : `jest-environment-jsdom`. `jsdom` est une bibliothèque qui se charge des deux premiers passages dangereux et utilise `csssyle` pour la prise en compte des feuilles de style.

```mermaid
graph LR;
    jest-->ourCode[Notre code]
    jest-->nodeJS(Node.js)
    jest-->many[Plein d'autres bibliothèques]
    jest-->env[jest-environment-jsdom]
    env-->jsdom
    jsdom-->cssstyle
```

## Les problèmes

### Uniquement le style en ligne

Le premier problème vient du fait que jsdom ne parvient à gérer le style que s'il est en ligne dans l'élément HTML.

Le code ci-dessous pourra être testé :ok:.

```jsx
<span style={backgroundColor:'green'} >Mon texte avec fond vert</span>
```

La configuration ci-dessous ne pourra être pas testée ❌.

```css
# file.css
span.isImportant{
    background-color: green;
}
```

```jsx
import 'file.css'
// (...)
<span className="isImportant" >Mon texte avec fond vert</span>
```

### Interprétation partielle des feuilles de style

La bibliothèque [cssstyle](https://github.com/jsdom/cssstyle/) est en charge de l'interprétation du css. Le problème c'est qu'elle n'est pas mis à jour depuis plus de deux ans. Il y a beaucoup de lacunes dans la prise en compte dans de nombreuses fonctionnalités.

Le cas qui m'a occupé la semaine dernière est un problème d'implémentation de l'espace de couleur avec la teinte, la saturation et la luminosité, nommée `hsl` (pour :gb: **H**ue, **S**aturation, **L**ightness) soulevé dans l'[issue 142 - HSL to RGB conversion results in black](https://github.com/jsdom/cssstyle/issues/142). Le problème arrive quand la teinte est non nulle. En regardant les tests situés `libs/parsers.test.js`, on comprend pourquoi ce problème n'a pas été repère. Les tests utilisent uniquement des teintes nulles.

Pour m'assurer que mon problème de test de React venait de cette librairie, j'ai cloné le dépôt et ajouté un test de conversion avec une teinte non nulle. Le test échouait. On peut trouver une [implémentation de référence par la W3.org (https://www.w3.org/TR/css-color-4/#hsl-to-rgb)](https://www.w3.org/TR/css-color-4/#hsl-to-rgb), le lien était déjà le code source de la conversion. En adaptant l'entrée, on arrive à corriger le code pour que tous les tests passent avec succès.

## Conclusion

On comprend mieux pourquoi il faut mieux éviter de tester le style dans des tests unitaires. C'est à utiliser avec parcimonie. En effet, le mieux de tester sur la cible d'exécution, le navigateur et cela implique d'autres outils comme [Selenium (https://www.selenium.dev/)](https://www.selenium.dev/), [Cypress (https://www.cypress.io/)](https://www.cypress.io/), [Puppeteer (https://pptr.dev/)](https://pptr.dev/) ou [Playwright (https://playwright.dev/)](https://playwright.dev/). Ces outils sont plus souvent utilisés des tests de bout en bout et s'associent avec un navigateur dans l'intégration continue. Il est parfois nécessaire d'utiliser des navigateurs sans affichage dont la configuration peut être complexe dans l'intégration continue. Il est à noter que jusqu'au retrait d'Internet Explorer, les différences d'implémentation des standards par les navigateurs, obligeaient à tester les applications web sur chaque navigateur cible.

Cela m'aura permis d'ouvrir une pull request pour corriger le problème - [Fix issue #142 : hsl convertion to rgb #159 ](https://github.com/jsdom/cssstyle/pull/159). Je ne pense pas malheureusement que celle-ci aboutisse vu que de nombreuses pull requests sont laissées sans réponse. On pourrait ajouter les tests proposés dans les spécifications de couleurs CSS et les fonctionnalités associées.

J'aurai aussi découvert les espaces de couleur [HSL](https://fr.wikipedia.org/wiki/Teinte_saturation_lumi%C3%A8re) et [HWB (https://en.wikipedia.org/wiki/HWB_color_model)](https://en.wikipedia.org/wiki/HWB_color_model) qui permettent de faire facilement des dégradés.

Si vous avez des anecdotes ou des conseils sur les tests de composants React ou front-end, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
