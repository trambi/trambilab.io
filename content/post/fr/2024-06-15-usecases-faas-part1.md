---
title: "Cas d'usage des lambdas et autres fonctions à la demande - partie 1"
date: "2024-06-15"
draft: false
description: "les cas plutôt favorables"
tags: ["lambda"]
author: "Bertrand Madet"
---

Aujourd'hui, j'ai envie d'écrire à propos des lambdas, leurs avantages, leurs inconvénients et leurs cas d'usage. J'utilise le terme lambdas, mais je devrais utiliser le terme générique `FaaS` pour **F**unctions **a**s **a** **S**ervice qui regroupe :

- [Lambda](https://aws.amazon.com/fr/lambda/) d'AWS ;
- [Azure Functions](https://azure.microsoft.com/fr-fr/products/functions/) d'Azure ;
- [Cloud Functions](https://cloud.google.com/functions/) de GCP ;
- [Serverless functions](https://www.scaleway.com/fr/serverless-functions/) de Scaleway ...

L'idée est d’exécuter une fonction à la demande sur un événement particulier. L'usage des `FaaS` est promu par les fournisseurs Cloud mais quels sont les tenants et les aboutissants de l'utilisation de telles fonctions. Je vous propose de décrire deux cas d'usage et d'analyser les conséquences du choix d'une ou de plusieurs FaaS pour répondre à ce besoin. Mais avant cela, interrogeons-nous sur les limitations générales des FaaS.

## Limitations générales

### Langages disponibles

Les fournisseurs de fonctions à la demande supportent un certain nombre de langages et de version :

- AWS supporte Node.js, Python, Ruby, C#, Java, Go et Rust ;
- Azure supporte Node.js,Python, C#, Java, Go et Rust ;
- GCP supporte Node.js, Python, Go, Java, C#, Ruby et PHP ;
- Scaleway supporte Node.js, Go, Python, PHP et Rust.

Ce sont globalement les mêmes langages, des langages très populaires. Nous remarquons l'absence de C et C++.

J'ai utilisé le terme "langages", mais il s'agit aussi d'environnement d'exécution : comme Node.js, qui permet d'exécuter du JavaScript avec l'API de Node.js. Il est possible d'utiliser une phase de transpilation (conversion d'un langage vers un autre) pour vous permettre d'exécuter par exemple du TypeScript dans l'environnement Node.js ou du Kotlin dans l'environnement Java.

Bref, l'utilisation d'une fonction à la demande vous limite aux langages et versions supportés par le service.

### Temps d'exécution

La durée de vie classique d'une fonction est de l'ordre de la seconde. Les fonctions ont également une durée de vie maximale (en général 15 minutes), il est impossible de dépasser ce temps, au-delà la fonction échoue avec une expiration. Les fonctions ont un temps de déclenchement qui dépend de plusieurs paramètres et peut varier d'une dizaine de millisecondes à une seconde.

Le principe même de durée de vie ⌛ condamne l'utilisation d'une fonction en tant que service "perpétuel".

Si le temps d'exécution est supérieur à la durée de vie maximale 🐢, vous allez devoir découper votre traitement : c'est possible mais la difficulté dépend du traitement. Si au contraire le temps de traitement est ultra rapide 🕐, vous aurez peut-être intérêt à regrouper les événements pour justifier le temps de déclenchement de votre fonction.

## Traitement sur événement dans un stockage

Si on souhaite effectuer un traitement lors de l'arrivée d'un fichier sur un dispositif de stockage. Pour que cela puisse être traité par une faas, il faut que : 

- l'arrivée d'un fichier puisse déclencher l'exécution de la fonction ;
- la fonction puisse accéder au dispositif de stockage.

Si votre dispositif de stockage est de type `S3`, les contraintes mentionnées ci-dessus sont faciles à lever. Un exemple d'une telle fonction est disponible dans le billet [Rendre plus robuste le traitement d'un événement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).

Si votre stockage est le disque dur de votre ordinateur 💻, cela risque d'être un peu plus sportif 🧗 et moins sain d'ouvrir votre ordinateur au service gérant la fonction. Le code qui va détecter l'événement devrait peut-être juste effectuer le traitement plutôt que déclencher l'exécution d'une faas.

La principale difficulté sera de gérer l'accès au dispositif de stockage depuis le service exécutant la fonction. Pour les lambda AWS, les bibliothèques permettant l'accès aux services AWS sont présentes dans les environnements d'exécution des lambdas et des exemples sont fournis. Donc la difficulté sera relativement faible.

Une autre difficulté est d'intégrer l'infrastructure autour de la fonction :

- les permissions de la fonction sur le stockage ;
- la détection de l'événement ;
- le déploiement du code de la fonction dans le service de fonction à la demande ...

## Réponse à une requête HTTP

Si on souhaite répondre à une requête HTTP en cherchant tous les éléments d'un type dans une base de données. La fonction devra : 

- répondre à un événement HTTP, ce qui est un cas intégré par tous les fournisseurs de fonction as a service ;
- accéder à la base de données.

Si la base de données est hébergée par le même fournisseur que celui des fonctions à la demande, c'est le cas idéal 🛣️ : prévu, très bien documenté. Pour AWS, le plus simple en termes d'intégration sera DynamoDB. Je n'ai pas utilisé cette base de données donc je ne connais pas précisément ses limites. La gestion des permissions sera facilitée. Si la base de données est hébergée ailleurs, il faudra gérer les permissions et les secrets pour accéder à la base.

Le déclenchement aux événements HTTP est intégré. On peut utiliser des solutions intermédiaires comme passerelle API (par exemple API Gateway d'AWS). Et tout cela devra être décrit.

## Conclusion

L'infrastructure pour intégrer les fonctions doit être décrite. Cela peut être sous la forme soit d'une documentation soit d'un script soit d'une description Terraform, CloudFormation ou Ansible. On voit que ce que l'on devra écrire ne limitera pas au code de nos fonctions.

Si vous avez des conseils, des expériences sur les AWS Lambdas, Serverless functions ou autres Function as a service partagez les avec moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

