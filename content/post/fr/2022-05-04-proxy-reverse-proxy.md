---
title: "Proxy et Reverse proxy"
date: "2022-05-04"
draft: false
description: "Deux dispositifs réseaux à connaitre"
tags: ["base","network"]
author: "Bertrand Madet"
---

Même si le métier d'ingénieur logiciel est assez différent d'administrateur réseau, nous sommes amenés à connaitre et à utiliser des éléments réseaux de base.

Cela peut être dû aux choix d'architectures logicielles ou à des contraintes de l'écosystème dans lequel nos logiciels doivent être déployés. Aujourd'hui nous allons découvrir le mandataire et le mandataire inverse :fr: (`proxy` et `reverse proxy` :gb:).

## Proxy

Le `proxy` :gb: ou mandataire :fr: désigne un dispositif physique ou logiciel qui permet de faire la liaison entre deux réseaux.

Le proxy le plus connu est le `mandataire de surf sortant`, il permet aux ordinateurs d'un réseau d'entreprise de naviguer sur internet. Tout le monde (`Visual Studio Code`, `Firefox`, `pip` ou `npm`...) l'appelle improprement `proxy` par raccourci. Il permet de :

- de mettre en cache les pages fréquemment visitées ;
- de garder l'historique de la navigation des utilisateurs ;
- d'inspecter les paquets pour s'assurer qu'il n'y a pas de fuite d'information ;
- de filtrer les sites indésirables ;
- de filtrer les appels aux API Web non désirés...

Dès qu'un outil a un problème de connexion à Internet, je pense au proxy. Il est possible de paramétrer au niveau du système d'exploitation le proxy ou outil par outil.

```mermaid
graph LR
  
  user1[Utilisateur 1]-->proxy[Proxy]
  user2[Utilisateur 2]-->proxy
  usern[Utilisateur n]-->proxy
  proxy-->internet[Internet]
  subgraph "SI interne"
    user1
    user2
    usern
    proxy
  end
```

On peut utiliser entre autre :

- [varnish (https://www.varnish-software.com/)](https://www.varnish-software.com/);
- [squid (http://www.squid-cache.org/)](http://www.squid-cache.org/);
- des serveurs web plus traditionnels comme :
  - [apache (https://httpd.apache.org/)](https://httpd.apache.org/)
  - [nginx (https://www.nginx.com/)](https://www.nginx.com/)

## Reverse proxy

Le `reverse proxy` :gb: ou mandataire inverse :fr: est un proxy qui va se placer entre vos utilisateurs et vos services. Il va donc faire la liaison entre le réseau de vos utilisateurs et le réseau de votre application.

Un reverse proxy permet de :

- d'orienter vers les différentes applications en fonction de l'URL. Par exemple, on peut associer `https://mondomaine.com/app1` à l'application 1 ou `https://app1.mondomaine.com` à cette même application. ;
- de faire une adaptation protocolaire par exemple gérer la partie HTTPS côté utilisateurs (:warning: ce n'est pas forcément recommandé d'un point de vue sécurité) ;
- d'ajouter des étapes d'authentification vers certaines applications ;
- de mettre en cache les URL fréquemment visitées ...

On peut le voir aussi comme un guichet unique et donc d'abstraire l'expérience utilisateur de l'architecture de votre solution. Dans le cas ci-dessus les utilisateurs peuvent ne pas se rendre compte que l'application est composée de différents services.

```mermaid
graph LR
  user1[Utilisateur 1]-->reverse[Reverse proxy]
  user2[Utilisateur 2]-->reverse
  usern[Utilisateur n]-->reverse
  
  subgraph users [Réseau de vos utilisateurs]
    user1
    user2
    usern
  end
  subgraph app[Réseau de votre application]
    reverse-->service1[Service 1 de l'application]
    reverse-->service2[Service 2 de l'application]
    reverse-->servicen[Service n de l'application]
  end
```

On peut utiliser entre autre :

- [varnish (https://www.varnish-software.com/)](https://www.varnish-software.com/);
- [caddy (https://caddyserver.com)](https://caddyserver.com);
- [traefik (https://traefik.io/traefik/)](https://traefik.io/traefik/)
- des serveurs web plus traditionnels comme :
  - [apache (https://httpd.apache.org/)](https://httpd.apache.org/)
  - [nginx (https://www.nginx.com/)](https://www.nginx.com/)

## Conclusion

Le `proxy` est un dispositif réseau générique qui permet de lier deux réseaux. Par déformation et raccourci, on utilise ce terme pour désigner est un dispositif qui permet aux utilisateurs d'accéder à Internet. Il doit alors être configuré au niveau des postes d'utilisateur.

Le `reverse proxy` est un type de proxy qui permet de présenter un point d'entrée unique pour vos applications web surtout si vous utilisez des micro-services (ou juste plusieurs services).

Comme on l'a vu, un même logiciel peut servir de proxy ou de reverse-proxy en fonction de sa configuration.

Si vous souhaitez partager votre expérience avec ces dispositifs réseaux ou d'autres qui vous semblent important à connaitre pour un développeur, cela se passe sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
