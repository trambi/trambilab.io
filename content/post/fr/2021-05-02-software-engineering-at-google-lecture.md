---
title: "Software Engineering at Google - Fiche de lecture"
date: "2021-05-02"
draft: false

description: "Livre de Google sur l'ingénierie logicielle"
tags: ["fiche de lecture","logiciel"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Ce livre s'adresse aux personnes qui veulent en savoir plus sur les pratiques et la culture du logiciel de l'un des plus grandes compagnies de développement logiciel du monde.

## Ce qu'il faut retenir

- L'ingénierie logicielle est la pratique du développement logiciel qui s'étend dans le temps ;
- Google a plusieurs dizaine de milliers de développeuses et de développeurs, les décisions à cette échelle sont souvent des compromis.
- Google a beaucoup investi  dans la culture et l'usine logicielle.

## Mon opinion

|||
|-----------------|-----------------|
| :+1: Description exhaustive du fonctionnement d'une énorme structure de développement logiciel| :-1: Beaucoup d'outils sont spécifiques à Google|
| :+1: Les chapitres sur les tests sont très intéressants et activables en dehors de Google| :-1: Manque d'ouverture sur le reste du monde|
| :+1: Certains outils ont été libérés comme [le système de construction : Bazel (https://bazel.build)](https://bazel.build)||

Ce livre est une claque, on comprend que tous les problèmes sont à l'échelle de Google et de ses effectifs dédiés au développement. Les solutions sont des compromis très sensés et elles illustrent l'aspect itératif.

Le premier chapitre définit très bien l'ingénierie logicielle. Les trois chapitres sur les tests sont bien construits.

Même si on travaille dans une petite structure, l'expérience accumulée et restituée sur les outils et la culture sont appréciables.

## Détails

En anglais :gb: :

- Software Engineering at Google de Titus Winters, Tom Manshreck et Hyrum Wright
- ISBN-13 : 978-1492082798

Pas disponible en français :cry:
