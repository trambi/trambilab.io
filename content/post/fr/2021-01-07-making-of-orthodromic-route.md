---
title: "2 outils utiles pour un blog"
date: "2021-01-07"
draft: false
description: "On découvre des choses en expliquant"
tags: ["documentation","hugo","blog","math","dessin"]
author: "Bertrand Madet"
---

Pendant la rédaction du billet précédent, j'ai été obligé d'afficher des équations (relativement simples) et des schémas. Et j'ai découvert deux outils :

 - {{< math >}}\KaTeX{{< /math >}};
 - Excalidraw.

## KaTeX

[KaTeX (https://katex.org/)](https://katex.org/) est une bibliothèque JavaScript qui permet d'afficher des équations. {{< math >}}\KaTeX{{< /math >}} est aux mathématiques ce que [Mermaid.js](https://mermaid-js.github.io/mermaid/) est aux diagrammes.

A partir de cette formule ci-dessous :
```
c= \frac{a-b}{ \sqrt{a^2+b^2}}
```

Il va afficher : 

{{< math >}}
c= \frac{a-b}{ \sqrt{a^2+b^2}}
{{< /math >}}

KaTeX utilise un sous ensemble de TeX, est présenté comme assez rapide et encore mieux c'est un logiciel libre sous licence MIT.

### Installation sur Hugo

Ce blog utilise [Hugo](https://gohugo.io/) avec le thème [GhostWriter](https://github.com/roryg/ghostwriter).
 
Pour installer KaTeX, il a fallu :
 - modifier l'entête général et le pied général ;
 - ajouter un raccourci math ;
 - créer un entête et pied pour KaTeX.

On commence par ajouter un raccourci math `themes/ghostwriter/layouts/shortcodes/math.html`.
```
{{ if or (hasPrefix .Inner "\n") (hasPrefix .Inner "<p>") }}
  <div class="katex-container">
  {{- $inner := replace (.Inner | htmlUnescape) "\n" "$$\n$$" -}}
  {{- $inner := replace $inner "<p>" "<p>$$" -}}
  {{- $inner := replace $inner "</p>" "$$<p>" -}}
  {{- $inner := replace $inner "\n$$$$\n" "\n" -}}
  {{ trim $inner "$" }}
  </div>
{{ else -}}
  <span class="katex-container">${{ .Inner | htmlUnescape }}$</span>
{{- end }}
```

On crée l'entête pour KaTeX `themes/ghostwriter/layouts/partials/shortcodes/katex/head.html`.

```
{{ if (in (string .Content) "class=\"katex-container\"") }}
<link rel="dns-prefetch" href="https://cdnjs.cloudflare.com/" />
<link rel="preconnect" href="https://cdnjs.cloudflare.com/" />

<link rel="preload" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.8.3/katex.min.js" as="script" />
<link rel="preload" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.8.3/contrib/auto-render.min.js" as="script" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.8.3/katex.min.css" />
{{ end }}
```

On crée le pied pour KaTeX `themes/ghostwriter/layouts/partials/shortcodes/katex/foot.html`.

```
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.8.3/katex.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.8.3/contrib/auto-render.min.js"></script>
<script>
var elems = document.getElementsByClassName('katex-container');
for (var i = 0; i < elems.length; i++) {
  renderMathInElement(
    elems[i],
    { delimiters: [
        {left: "$$", right: "$$", display: true},
        {left: "\\[", right: "\\]", display: true},
        {left: "$", right: "$", display: false},
        {left: "\\(", right: "\\)", display: false}
    ]}
  );
}
</script>
```

On modifie l'entête `themes/ghostwriter/layouts/partials/header.html` (en ajoutant la ligne suivante après la ligne 24).

```
{{ partial "shortcodes/katex/head.html" . }}
```

On modifie le pied `themes/ghostwriter/layouts/partials/footer.html` (en ajoutant la ligne suivante après la ligne 27).

```
{{ partial "shortcodes/katex/foot.html" . }}
```

## Excalidraw

[Excalidraw (https://excalidraw.com/)](https://excalidraw.com/) est un site qui permet de dessiner rapidement avec un style brouillon. Il y a assez d'options pour faire des choses sympas.

Le site se base sur un [logiciel codé en JavaScript (https://github.com/excalidraw/excalidraw)](https://github.com/excalidraw/excalidraw) libre avec une licence MIT codé en Javascript. Il est possible de l'auto-héberger.

Petit bonus que je n'ai pas testé longtemps, il est possible de collaborer en direct avec la version hébergée.

## Conclusion

Si vous souhaitez partager des outils, contactez [moi sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).