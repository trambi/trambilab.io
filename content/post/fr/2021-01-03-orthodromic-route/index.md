---
title: "Routes orthodromiques à la main"
date: "2021-01-03"
description: "où l'on comprend l'intérêt des bibliothèques géographiques"
tags: ["geographie","culture","à la main"]
author: "Bertrand Madet"
---

On a confié récemment à mon équipe le soin de tracer des routes orthodromiques dans une application web, utilisant Leaflet.js. N'ayant pas trouvé de bibliothèque ou de plugin de Leaflet.js, j'ai décidé d'implémenter une approximation de route orthodromique entre deux points.

# Orthoquoi ? 

[L'orthodromie (https://fr.wikipedia.org/wiki/Orthodromie)](https://fr.wikipedia.org/wiki/Orthodromie) : c'est le chemin le plus court entre deux points sur une sphère. Suivre une ligne droite sur une carte s'appelle suivre la route loxodromique. La route orthodromique est plus courte que la route loxodromique. Pour un Paris-New York, la différence est d'environ 250 kilomètres pour environ 6000 kilomètres. Les navigateurs et surtout les aviateurs ont tendance à prendre les routes orthodromiqes (cela varie avec les conditions  météo et le trafic aérien).

La route orthodromique entre deux points est le plus petit arc du `grand cercle` qui passe par le point d'origine et le point de destination.

Le grand cercle est l'intersection entre la sphère représentant la Terre et un plan défini par le point d'origine, le point de destination et le centre de la Terre. Et paradoxalement le grand cercle permet de tracer le chemin le plus court entre deux points.

![Route orthodromique](orthodromic_explaination.png)

# Première approche : déterminer le cap initial et avancer par petit pas

Ma première idée a été de déterminer le cap à suivre en fonction du point d'origine et de destination puis de suivre ce cap pendant un peu de temps, déterminer le nouveau cap à suivre à partir du nouveau point d'origine et du point de destination jusqu'à l'arrivée.

![Caps et petits pas](orthodromic_way_1.png)

Le cap à suivre est indiqué par la formule 
{{< math >}}
cotan(R0) = \frac{\cos{\varphi_a}\tan{\varphi_b}}{\sin{\lambda_a - \lambda_b}} - \frac{\sin{\varphi_a}}{\tan{\lambda_a - \lambda_b}}
{{< /math >}}

où 
 - φa est la latitude du point d'origine ;
 - φb est la latitude du point de destination ;
 - λa est la longitude du point d'origine ;
 - λb est la longitude du point de destination.

## Implémentation


Ce qui peut s'implémenter

```javascript
const headings = (origin, destination) => {
  const phiA = degreeToRad(origin.latitude);
  const phiB = degreeToRad(destination.latitude);
  const lambdaAB = keepInMinusOrPlusPI(degreeToRad(origin.longitude) - degreeToRad(destination.longitude);)
  const term1 = Math.cos(phiA) * Math.tan(phiB) / Math.sin(lambdaAB);
  const term2 = Math.sin(phiA) / Math.sin(lambdaAB);
  return Math.atan(1/(term1 - term2));
};

const nextPoint = (origin, destination) => {
  const initialHeadings = headings(origin, destination);
  const latitude = origin.latitude + radToDegree(Math.sin(initialHeadings));
  const longitude = origin.longitude + radToDegree(Math.cos(initialHeadings));
}
```

## Problèmes

Cette approche a deux problèmes :

- La fonction se comporte étrangement après le point de plus haute latitude de la route ;
- Il est difficile de répartir les points le long de la route.

Même si le premier problème a eu des effets spectaculaires, c'est le deuxième qui m'a fait renoncé à cette approche, car je n'ai pas trouvé de solution pour avoir une répartition uniforme des points le long de la route.

# Wikipédia à la rescousse

Je décide donc de changer d'approche. La page de [wikipédia de l'Orthodromie (https://fr.wikipedia.org/wiki/Orthodromie)](https://fr.wikipedia.org/wiki/Orthodromie) explique d'autre chose mais ne renseigne pas sur une autre approche. Je décide de regarder la page en anglais (rubrique `Dans d'autres langues`), j'arrive sur https://en.wikipedia.org/wiki/As_the_crow_flies et je fouille et j'arrive sur [Great-circle navigation](https://en.wikipedia.org/wiki/Great-circle_navigation).

```mermaid
graph LR
orthodromie-->as_the_crow_flies-->great-cirle_distance-->great-circle_navigation
```

# Deuxième approche : déterminer le grand cercle et choisir des points sur ce cercle

L'idée est de déterminer le `grand cercle` qui passe par le point d'origine et le point de destination et ensuite de répartir des points sur ce grand cercle.

## Les équations

La page Wikipédia est remplie d'équations, voici celles dont nous avons besoin :

{{<math>}}
\tan{\alpha_1}= \frac{\cos{\phi_2}\sin{\lambda_{12}}}{\cos{\phi_1}\sin{\phi_2} - \sin{\phi_1}\cos{\phi_2}\cos{\lambda_{12}}}
\tan{\sigma_{12}}=\frac{\sqrt{(\cos{\phi_1}\sin{\phi_2} - \sin{\phi_1}\cos{\phi_2}\cos{\lambda_{12}})^2+(\cos{\phi_2}\sin{\lambda_{12}})^2}}{\sin{\phi_1}\sin{\phi2}+\cos{\phi_1}\cos{\phi_2}\cos{\lambda_{12}}}
\tan{\alpha_0} = \frac{\sin{\alpha_1}\cos{\phi_1}}{\sqrt{\cos{\alpha_1}^2+(\sin{\alpha_1}\sin{\phi_1})^2}}
\tan{\sigma_{01}} = \frac{\tan{\phi_1}}{\cos{\alpha_1}}
\tan{\lambda_{01}}=\frac{\sin{\alpha_0}\sin{\sigma_{01}}}{\cos{\sigma_{01}}}
{{</math>}}

où :
 - φ1 est la latitude du point d'origine ;
 - φ2 est la latitude du point de destination ;
 - λ12 est la différence entre les longitudes du point de destination et du point d'origine ;
- α1 est l'azimut du grand cercle au point d'origine (cap) ;
- σ12 est l'angle entre le point d'origine et le point de destination au centre de la Terre ;
- α0 est l'azimut du grand cercle à l'équateur ;
- σ01 est l'angle entre le point du grand cercle à l'équateur et le point d'origine au centre de la Terre ;
- λ01 est la différence entre les longitudes du point d'origine et du point du grand cercle à l'équateur.

Après on se sert des équations suivantes pour déterminer les coordonnées de n'importe quel point du grand cercle. 
{{<math>}}
\tan{\phi} = \frac{\cos{\alpha_0}\sin{\sigma}}{\sqrt{\cos{\sigma}^2+(\sin{\alpha_0}\sin{\sigma})^2}}
\tan{(\lambda - \lambda_0)} = \frac{\sin{\alpha_0}\sin{\sigma}}{\cos{\sigma}}
{{</math>}}

Pour notre route, il suffit de choisir le nombre de points intermédiaires. Si on veut 3 points intermédiaires, on divise l'angle par 4.

![Distribution des points](compute_intermediate_point.png)

Je me suis servi de l'exemple de Valparaiso (latitude -33°, longitude -71.6°) à Shanghai (latitude 31.4°, longitude 121.8°) pour implémenter les tests unitaires. Comme indiqué dans la page, j'ai utilisé la fonction `atan2`. Et j'ai controlé sur le trajet Paris-New York.

## atan2

`atan2` est une fonction qui inverse la fonction tangente et qui permet de faire la différence des directions diamétralement opposées et de gérer les angles de 90°. Elle prend deux arguments.

# En résumé

Je retiens de cette séquence :
 - [Wikipedia](https://fr.wikipedia.org) est une source inestimable de connaissances. Je n'ai pas encore donné en 2021, alors j'y pense et vous ?
 - Avec les équations et un peu de temps, il est possible d'implémenter à peu près n'importe quoi y compris des approximations de routes orthodromiques. 
 - Je garde en tête que cette approximation prend la Terre pour une sphère, la réalité est plus compliquée (cf. [Géodisie](https://en.wikipedia.org/wiki/World_Geodetic_System#WGS84)) donc je laisse la géographie aux spécialistes.
 - la fonction `atan2` est très utile pour gérer les directions sans ajustement à la main.
 - On dit `as the crow flies` :gb: pour à vol d'oiseaux.


Voilà, j'espère que cela vous a plu. Si vous souhaitez partager des aventures de développement, contactez [moi sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
