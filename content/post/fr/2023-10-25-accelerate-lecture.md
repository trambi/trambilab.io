---
title: "Accelerate - Fiche de lecture"
date: "2023-10-25"
draft: false
description: "Livre de Nicole Forsgren, Jez Humble et Gene Kim sur la productivité logicielle "
tags: ["fiche de lecture","devops","Software Craftmanship"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Ce livre s'adresse à celles et ceux qui veulent améliorer la productivité logicielle de leur organisation. Il permettra de comprendre rationnellement l'engouement autour du mouvement DevOps.

## Ce qu'il faut retenir

- Ce livre est l'aboutissement d'un travail de recherche sur les données issues de "State of DevOps Report" de 2014 à 2017 ;
- Il y a vingt-quatre fonctionnalités qui améliorent la productivité logicielle ;
- Les modèles de maturité ne sont pas adaptés à décrire l'évolution d'une organisation car le rythme d'évolution est important ;
- L'adoption du DevOps a des impacts économiques importants ;
- les mesures de tempo et de stabilité sont les plus corrélées avec la productivité et sont conciliables.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: La définition de mesures pour évaluer la productivité logicielle | :-1: La partie 3 - Transformation m'a semblé peu activable |
| :+1: Approche très rigoureuse et centrée sur les données | :pinching_hand: qui peut paraître moins enthousiasmante que the DevOps Handbook |
| :+1: L'approche très holistique ||

Ce livre est très intéressant, son approche est centrée sur les données des sondages "State of DevOps Report". Il aborde l'ensemble de la production de logiciel de la gestion de version à la culture de l'organisation en passant par le fait de garder le travail supportable.

La définition des mesures les plus corrélées avec la productivité logicielle est très intéressante pour évaluer rapidement des pratiques. Ces mesures sont :

- le tempo ou la rapidité :
  - le délai de livraison, le temps moyen entre le commit du code et que ce code s'exécute en production,
  - la fréquence de déploiement, la fréquence de fourniture du code en production aux utilisateurs finaux ;
- la stabilité :
  - le temps moyen de reprise d'activité, le temps moyen entre la survenue d'un incident et la reprise d'activité pour les utilisateurs finaux,
  - le taux d'échec des changements, le rapport entre le nombre de changement poussé en production qui amènent une dégradation et nécessite une correction sur le nombre total de changement poussé en production.

Et surtout la démonstration que la rapidité peut impliquer la stabilité et la stabilité peut impliquer la rapidité.

La partie 2 sur la description du travail de recherche est une bonne surprise (en particulier la justification de l'utilisation des sondages pour obtenir des données). Le seul bémol est la troisième partie du livre, un chapitre sur la transformation, m'a paru peu activable.

Ce livre est très complémentaire du `DevOps Handbook` - [fiche de lecture]({{< ref "/post/fr/2020-11-23-devops-handbook-lecture.md" >}}). Je conseillerai de lire `Accelerate` en premier. À noter qu'une seconde édition est prévue pour le 3 janvier 2024, à suivre donc...

## Détails

- Titre : Accelerate - Building and Scaling High Performing Technology Organizations
- Auteur : Nicole Forsgren, PhD, Jez Humble et Gene Kim
- Langue : :gb:
- ISBN : 978-1942788331

*Edité le 28/10/2023 : Corrections suite aux remarques de Pierre Turkiewicz*