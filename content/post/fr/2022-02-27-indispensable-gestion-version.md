---
title: "L'indispensable gestion de version"
date: "2022-02-27"
draft: false
description: "plus utile encore que l'éditeur de texte"
tags: ["base","outils","git"]
author: "Bertrand Madet"
---

La gestion de version est un outil indispensable à un logiciel ayant une durée de vie supérieure à une journée. 

Pourquoi est-ce aussi vite indispensable ? Parce la gestion de version va permettre de conserver l'historique du développement et vous faciliter l'évolution. 

Pour l'instant la solution majeure de gestion de version dans le développement logiciel est [Git](https://git-scm.com), ce billet sera donc illustré avec des commandes et des mécanismes de git.

## Conserver l'historique

La gestion de version permet de garder un historique très précis d'un ensemble de fichiers. Dans le développement logiciel, nous nous intéressons particulièrement au code source d'une ou plusieurs solutions logicielles.

### Simplifier la sauvegarde

Est-ce que cela vous est déjà arrivé de devoir avancer sur plusieurs documents liés et de vous retrouver à faire des copies de répertoires avec des noms comme `mon_projet-2022-02-27-version2` ? Vraisemblablement, suite à des modifications, vous allez ensuite copier ce répertoire et le nommer `mon_projet-2022-02-27-version3`.

La première utilité de la gestion de version est d'assurer cette sauvegarde de l'historique. Cette version peut être créée de manière fine et documentée.

Cela permet aussi d'envoyer ces données sur des machines distantes afin de vous prémunir de la perte de votre travail suit à la chute de votre ordinateur. Ou de récupérer l'ensemble des fichiers sur votre ordinateur depuis une machine distante. On peut mettre une étiquette sur une version particulière.

Git permet :

- d'ajouter des versions dans l'historique avec `commit` et de contrôler précisément le cadre de chaque version avec `add` ;
- d'envoyer vos modifications avec `push` ;
- de récupérer l'ensemble des fichiers et son historique avec `clone`;
- de récupérer les modifications distantes avec `fetch`.
- d’étiqueter une version avec `tag`.

### Parcourir l'historique

La gestion de version vous permet aussi de parcourir l'historique des versions avec les modifications et les commentaires associés. Si le code constitue en lui-même une  documentation, les motivations et l'histoire de modification du code peuvent permettre de comprendre le fonctionnement ou la structure. Cela constitue une documentation sur l'évolution du code - une documentation secondaire très utile pour comprendre certains choix d'implémentations.

Le fait de pouvoir parcourir l'historique permet d'enquêter sur l'apparition d'un bug. En particulier, si on a du mal à cibler la cause du bug, cibler la premiere version manifestant le bug permet de réduire les changements responsables du problème.

Du côté de Git :

- `log` permet d'obtenir les listes de versions ou commit en langage git ;
- `blame` permet d'obtenir le commit responsable de dernière modification de chaque ligne d'un fichier ;
- `bisect` permet de rechercher le commit de l'apparition d'un bug.


## Faciliter l'évolution

La gestion de version facilite l'évolution d'un logiciel en proposant des mécanismes d'exploration et de collaboration.

### Diverger

Une bonne solution de gestion de configuration permet d'explorer des possibilités en créant des embranchements dans l'historique (appelées branches par la suite) et ainsi  permettre d'expérimenter des choses sans risquer de polluer la partie "sure" de l'historique.

Dans l'exemple ci-dessous, la branche d'exploration permet de tester sans empêcher l'évolution de la branche principale. 

```mermaid
graph BT
subgraph "branche principale"
    version1[version 1]-->version2[version 2]
    version2-->version3[version 3]
    version3-->version4[version 4]
end
subgraph "branche d'exploration"
    version3-->versionExplo1[version d'exploration 1]
    versionExplo1-->versionExplo2[version d'exploration 2]
end
```

Git permet : 

- de créer des branches avec `branch`;
- de passer d'une branche à l'autre avec `checkout`.


### Fusionner

La notion de branche permet d'avancer sur différents pan de l'application. Le facteur clé pour la collaboration est la possibilité de fusionner les modifications des différentes branches. Ainsi si l'étape est facile, il sera facile de travailler à plusieurs. La gestion de cette problématique de fusion est propre à chaque gestion de version.

Par exemple si une première équipe travaille sur une fonctionnalité et une deuxième équipe travaille sur une autre fonctionnalité, chacune peut travailler sur une branche spécifique. A la fin de leur travail, chacune va fusionner sa fonctionnalité dans la branche principale.

```mermaid
graph BT
subgraph "branche principale"
    version1[version 1]-->version2[version 2]
    version2-->version3[version 3]
    version3-->version4[fusion entre la branche principale et l'ajout de fonctionnalité 1]
    version4-->version5[fusion entre la branche principale et l'ajout de fonctionnalité 2]
end
subgraph "branche fonctionnalité 1"
    version3-->func1a[Premiere partie de la fonctionnalité 1]
    func1a-->version4
end
subgraph "branche fonctionnalité 2"
    version2-->func2a[Premiere partie de la fonctionnalité 2]
    func2a-->func2b[Deuxième partie de la fonctionnalité 2]
    func2b-->version5
end
```

Git permet de fusionner de manière efficace deux branches grâce à `merge`. La stratégie de fusion est configurable et celle par défaut fonctionne déjà très bien.

## Conclusion

J'espère vous avoir démontré que la gestion de version est un outil indispensable. Cela est devenu tellement naturel que je m'en sers même pour d'autres choses que le code - le contenu de ce blog par exemple.

Git permet de gérer correctement les fonctionnalités attendues d'une gestion de version dans l'immense majorité de cas. Les articles sur Git sont réunis dans [le tag git]({{< ref "/tags/git" >}}).

Si vous avez en tête d'autres avantages à utiliser une gestion de version ou d'autres conseils sur Git, partagez les avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
