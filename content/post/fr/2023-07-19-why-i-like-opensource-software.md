---
title: "Pourquoi j'aime les logiciels libres"
date: "2023-07-19"
draft: false
description: "les marques et les variables d'environnement"
tags: ["open-source"]
author: "Bertrand Madet"
---

Beaucoup de l'écosystème logiciel mondial est basé sur des logiciels libres. Pour les spécialistes des logiciels libres comme la Free Software Foundation, les logiciels libres se caractérisent par quatre libertés accordés à leurs utilisateurs :

- Liberté d'utilisation ;
- Liberté d'analyse ;
- Liberté de modification ;
- Liberté de redistribution.

Je vous propose de voir de manière complètement subjective (j'aime les logiciels libres :heart:), les avantages concrets de ces libertés.

## Liberté d'utilisation

Un logiciel libre doit permettre d'utiliser librement, on ne peut pas restreindre à une utilisation non commerciale par exemple. Il peut y avoir des contre parties comme le fait de citer les auteurs ou de garder la même licence dans les produits dérivés de ce logiciel. Il y a beaucoup de licence, mais les plus répandus sont très bien expliqués dans de nombreux sites comme :

- Le [site Choose an open-source license (https://choosealicense.com/)](https://choosealicense.com/)  fournit des synthèses pour les droits, les limitations et les conditions des licences libres les plus communes.
- La [page licence du site Software Package Data Exchange (https://spdx.org/licenses/)](https://spdx.org/licenses/)  fournit une liste de licence avec leurs identifiants.
- La page [FAQ sur les GNU licenses du site GNU (https://www.gnu.org/licenses/gpl-faq.html)](https://www.gnu.org/licenses/gpl-faq.html) est une mine d'information  sur les licences GNU (GPL, AGPL et LGPL) et leur compatibilité avec d'autres licences.
- L'assistant de [comparaison et de choix de licence (https://joinup.ec.europa.eu/solution/joinup-licensing-assistant/joinup-licensing-assistant-jla)](https://joinup.ec.europa.eu/solution/joinup-licensing-assistant/joinup-licensing-assistant-jla) fournit des outils de recherche de licence et de compatibilité.

Cette liberté permet aussi d'intégrer des bibliothèques libres dans vos logiciels, il faut alors vérifier que leur licence soit compatible de la license souhaitée pour votre logiciel. Oui, il est possible d'avoir deux bibliothèques libres avec des licences incompatibles, j'en avais parlé dans mon billet de blog [Pourquoi vous devriez vous intéresser aux licences de vos logiciels ?]({{< ref "/post/fr/2020-11-14-pourquoi-vous-devriez-vous-interesser-aux-licences-de-vos-logiciels.md" >}}). Mais comme je vous l'ai dit juste au-dessus, l'avantage est que les licences libres sont étudiées et synthétisées. Pour les licences de logiciels propriétaires, cela peut être assez opaque.

La liberté d'utilisation signifie très souvent que nous allons pouvoir développer plus vite, sans "réinventer la roue".

## Liberté d'analyse

Le code source est disponible et on a le droit de l'analyser et de le faire tourner pour comprendre le fonctionnement d'un logiciel. C'est comme ça (et non en lisant la documentation:exploding_head:) que j'ai trouvé la variable d'environnement pour ajouter des autorités de certification à Node.js (cf. [Utilisation de Node.js en entreprise - partie 1]({{< ref "/post/fr/2021-02-28-utilisation-nodejs-en-entreprise-part-1.md" >}})).

Cela permet aussi de lire du code de tiers que l'on a pas codé. C'est intéressant pour accroître sa connaissance d'un langage ou pour voir la solution dans des problématiques similaires par exemple des cas de tests. Comme l'outillage pour générer le logiciel est inclus dans le code source, il est aussi possible de voir l'utilisation de certains outils (compilateur ou bundler JavaScript) dans des cas réels.

## Liberté de modification

Quand on utilise un logiciel libre et que l'on souhaite corriger un bogue :bug: ou ajouter une fonctionnalité, on peut le faire. Cela peut être simple ou très compliqué mais c'est possible de le faire en autonomie. Dans un logiciel propriétaire, vous pouvez ouvrir un ticket et attendre que le problème soit résolu ou pas.

Contrairement à que l'on croit souvent, vous êtes pas obligés de publier les modifications que vous avez faites au monde entier ou sur internet. Aucunes licences libres n'obligent à faire cela mais il y a des subtilités que nous verrons dans la partie suivante.

## Liberté de redistribution

Admettons que vous ayez corrigé un problème dans une bibliothèque et que vous ayez un ami qui a le même problème, vous avez le droit de lui donner le code corrigé.

Certaines licences dites avec copyleft vous obligent à redistribuer suivant la même licence des travaux dérivés. Dans ce cas, vous devez donner les mêmes libertés dont la liberté de redistribution à vos utilisateurs. Donc vous ne maîtrisez pas la diffusion d'une modification par exemple. Personnellement, je trouve cela plutôt juste d'accorder à nos utilisateurs les mêmes droits dont nous avons bénéficié.

Il peut aussi arriver que des problèmes de gouvernance éclate dans un projet libre, cela peut amener à une bifurcation (:gb: `fork`) du projet, comme MySQL :dolphin: et MariaDB :seal:. Cela permet de continuer la maintenance d'un logiciel 'dont vous n'êtes pas propriétaire mais dont vous avez besoin.

## Conclusion

Il est, en général, plus avantageux de proposer de reverser ses modifications dans le logiciel initial. Pour bénéficier des mises à jour et ne pas avoir à patcher votre logiciel à chaque mise à jour. Cela peut être intimidant, mais GitHub et GitLab sont basés sur ce principe (Pull request pour GitHub, Merge Request pour GitLab). GitHub encourage les mainteneurs de logiciels libres à étiqueter des problèmes pour orienter vers les bons premiers problèmes à régler.

Ce billet a justement été inspiré par la correction acceptée d'une extensions pour afficher des barres d'erreur dans Chart.js : `chartjs-chart-error-bars`. La merge request est [là (https://github.com/sgratzl/chartjs-chart-error-bars/pull/65)](https://github.com/sgratzl/chartjs-chart-error-bars/pull/65). Merci à [Samuel Gratzl](https://github.com/sgratzl) pour avoir développé ses extensions et avoir pris le temps de m'aider à améliorer ma merge request.

Et vous ? Avez-vous déjà contribué à des logiciels ? Si vous voyez d'autres avantages aux logiciels libres, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
