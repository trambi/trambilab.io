---
title: "Jouons avec les serverless functions - partie 1"
date: "2023-03-29"
draft: false
description: "cette fois-ci, cela se passe sur Scaleway"
tags: ["projet perso","Go","jeu","cloud","lambda"]
author: "Bertrand Madet"
---

> édité le 11 avril pour corriger des tournures de phrases.

J'ai envie depuis longtemps de tester les [serverless functions](https://www.scaleway.com/fr/serverless-functions/) de [Scaleway (https://www.scaleway.com/fr/)](https://www.scaleway.com/fr/). Les `serverless functions` sont des fonctions en tant que service, il s'agit d'un service où l'on a pas à s'occuper de serveurs, d'où le `serverless`. C'est l'équivalent Scaleway des [Lambdas AWS](https://docs.aws.amazon.com/fr_fr/lambda/).

Je vous propose de regarder ensemble le potentiel des `serverless functions` et essayer de répondre aux questions suivantes :

- A quoi ressemble une serverless function en Go ?
- Comment tester le code des mes serverless functions  ?
- Quelles sont les différences avec les Lambdas ?

## Pourquoi Scaleway ?

Pourquoi Scaleway ? Je vois trois raisons :

1. Cela me change d'AWS que je pratique au travail ;
2. Parce que j'aime bien Scaleway - ils sont français :fr:, leurs data centers sont tous en Europe :eu: j'ai utilisé pas mal leur offre minimale de machine virtuelle pour des projets perso ;
3. Parce l'offre de services est simple et le nombre de services est raisonnable (aller sur du Azure ou GCP m'exposerez à découvrir des dizaines services).

## Le but

Comme l'exploration est plus sympa avec un objectif, nous allons essayer de faire que notre fonction fasse un traitement sur un objet stocké et l'écrive en sortie. Pour s'éloigner d'un simple helloWorld, le traitement devra appeler une fonction d'une bibliothèque externe. Pour joindre l'utile à l'agréable, je vais utiliser l'un des bibliothèques de mon projet perso : Boite à outils pour tournoi ([la première partie est là]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}})).

## La réalisation

Pour ne pas se frotter directement au problème, j'explore la documentation de Scaleway. Je vais m'inspirer de l'exemple Go [Upload file to S3 (https://github.com/scaleway/serverless-examples/tree/main/functions/go-upload-file-s3-multipart/)](https://github.com/scaleway/serverless-examples/tree/main/functions/go-upload-file-s3-multipart/) et du programme de commande de mon projet perso. Je commence avec uniquement des fichiers locaux.

```golang
package handler

import (
  "fmt"
  "net/http"

  "gitlab.com/trambi/tournamenttoolbox/pkg/compute"
  "gitlab.com/trambi/tournamenttoolbox/pkg/csv"
  jsonsettings "gitlab.com/trambi/tournamenttoolbox/pkg/json/settings"
)

func gamesFromCsvFile(csvPath string) ([][]string, error) {
  identity := &csv.Identity{}
  err := csv.FromFile(identity, csvPath)
  return (*identity).Content, err
}

func gamesToCsvFile(games [][]string, csvPath string) error {
  identity := csv.Identity{Content: games}
  return csv.ToFile(identity, csvPath)
}

func Handle(w http.ResponseWriter, r *http.Request) {
  if r.Method != http.MethodGet {
    fmt.Printf("Used method %v is not GET\n", r.Method)
    w.WriteHeader(http.StatusMethodNotAllowed)
    return
  }
  settings, err := jsonsettings.FromJSONFile("./config.json")
  if err != nil {
    fmt.Println("error during loading settings from json file: ", err.Error())
    w.WriteHeader(http.StatusInternalServerError)
    return
  }
  fmt.Println("Settings loaded")
  var games, newgames [][]string
  games, err = gamesFromCsvFile("./games.csv")
  if err != nil {
    fmt.Println("error during loading games from csv file: ", err.Error())
    w.WriteHeader(http.StatusInternalServerError)
    return
  }
  fmt.Println("Games loaded")
  newgames, err = compute.Compute(settings.ComputedFields, games[0], games[1:])
  if err != nil {
    fmt.Println("error during computing fields: ", err.Error())
    w.WriteHeader(http.StatusInternalServerError)
    return
  }
  fmt.Println("Fields computed")
  result := append([][]string{}, compute.AppendComputedFieldsToHeaders(settings.ComputedFields, games[0]))
  result = append(result, newgames...)
  err = gamesToCsvFile(result, "games_with_computed_fields.csv")
  if err != nil {
    fmt.Println("error during writing newgames to csv file: ", err.Error())
    w.WriteHeader(http.StatusInternalServerError)
    return
  }
  w.WriteHeader(http.StatusOK)
}
```

> Je n'ai pas commencé par les tests car j'explore et je serai incapable à ce stade d'écrire le moindre test.

Comment tester cette fonction automatiquement ? Je peux déjà tester que si on utilise pas la méthode GET, le statut de la réponse sera [405 (https://developer.mozilla.org/fr/docs/Web/HTTP/Status/405)](https://developer.mozilla.org/fr/docs/Web/HTTP/Status/405). Pour créer des requêtes et des enregistreurs de réponses de test, il faut utiliser la bibliothèque standard `net/http/httptest`.

```golang
package handler

import (
  "net/http"
  "net/http/httptest"
  "testing"
)

func TestHandlerReturn405WithInvalidMethod(t *testing.T) {
  nonGetMethods := []string{"POST", "PUT", "OPTIONS", "PATCH", "DELETE"}
  for _, nonGetMethod := range nonGetMethods {
    fakeRequest := httptest.NewRequest(nonGetMethod, "http://example.com/foo", nil)
    fakeWriter := httptest.NewRecorder()
    Handle(fakeWriter, fakeRequest)
    resp := fakeWriter.Result()
    expectedStatus := http.StatusMethodNotAllowed
    statusCode := resp.StatusCode
    if statusCode != expectedStatus {
      t.Errorf("unexpected statusCode %v while receiving %v method - expected %v", statusCode, nonGetMethod, expectedStatus)
    }
  }
}

```

Le test passe. Je peux commencer à tester un comportement nominal. J'ajoute donc le test suivant. Je lance le test et je vérifie que le résultat est bien dans le fichier `games_with_computed_fields.csv` et à bien le contenu attendu.

```golang
func TestHandler(t *testing.T) {
 // GIVEN
 outputPath := "games_with_computed_fields.csv"
 expectedResult := []string{
 "id_1,team_1,td_1,id_2,team_2,td_2,td_net_1,td_net_2,points_1,points_2",
  "1,first_team,3,2,third_team,1,2,-2,1,0",
  "3,second_team,1,4,last_team,0,1,-1,1,0",
 }
 expectedStatus := http.StatusOK
 defer os.Remove(outputPath)
 fakeRequest := httptest.NewRequest("GET", "http://example.com/foo", nil)
 fakeWriter := httptest.NewRecorder()
 result := []string{}
 //WHEN
 Handle(fakeWriter, fakeRequest)
 //THEN
 resp := fakeWriter.Result()
 statusCode := resp.StatusCode
 if statusCode != expectedStatus {
  t.Errorf("unexpected statusCode %v while receiving GET method - expected %v", statusCode, expectedStatus)
 }
 f, err := os.Open(outputPath)
 if err != nil {
  t.Errorf("unexpected error in os.Open: %v", err.Error())
 }
 defer f.Close()
 scanner := bufio.NewScanner(f)
 for scanner.Scan() {
  result = append(result, scanner.Text())
 }
 if err := scanner.Err(); err != nil {
  t.Errorf("unexpected error during reading: %v", err.Error())
 }
 if cmp.Equal(result, expectedResult) == false {
  t.Errorf("result %v differs from expected %v", result, expectedResult)
 }
}
```

C'est long mais pourquoi pas. Cela risque encore de se compliquer si je veux tester pour des données d'entrée qui sont incorrectes ou ce qu'il se passe en cas d'erreur d'écriture. On doit procéder autrement. Sans changer les tests, je vais extraire la partie "métier" de la fonction et injecter les fonctions impures (qui ont des effets de bord).

```golang
package handler

import (
 "fmt"
 "net/http"

 "gitlab.com/trambi/tournamenttoolbox/pkg/compute"
 "gitlab.com/trambi/tournamenttoolbox/pkg/csv"
 jsonsettings "gitlab.com/trambi/tournamenttoolbox/pkg/json/settings"
 "gitlab.com/trambi/tournamenttoolbox/pkg/settings"
)

type loadSettingsFunc func() (settings.Settings, error)
type loadGamesFunc func() ([][]string, error)
type saveGamesFunc func([][]string) error

func loadSettingsFromLocalJson() (settings.Settings, error) {
 return jsonsettings.FromJSONFile("./config.json")
}

func loadGamesFromLocalJson() ([][]string, error) {
 identity := &csv.Identity{}
 err := csv.FromFile(identity, "./games.csv")
 return (*identity).Content, err
}

func saveGamesToLocalJson(games [][]string) error {
 identity := csv.Identity{Content: games}
 return csv.ToFile(identity, "./games_with_computed_fields.csv")
}

func Handle(w http.ResponseWriter, r *http.Request) {
 if r.Method != http.MethodGet {
  fmt.Printf("Used method %v is not GET\n", r.Method)
  w.WriteHeader(http.StatusMethodNotAllowed)
  return
 }
 injectedHandle(w, loadSettingsFromLocalJson, loadGamesFromLocalJson, saveGamesToLocalJson)
}

func injectedHandle(w http.ResponseWriter, loadSettings loadSettingsFunc, loadGames loadGamesFunc, saveGames saveGamesFunc) {
 settings, err := loadSettings()
 if err != nil {
  fmt.Println("error while loading settings: ", err.Error())
  w.WriteHeader(http.StatusInternalServerError)
  return
 }
 fmt.Println("Settings loaded")
 var games, newgames [][]string
 games, err = loadGames()
 if err != nil {
  fmt.Println("error while loading games: ", err.Error())
  w.WriteHeader(http.StatusInternalServerError)
  return
 }
 if len(games) == 0 {
  fmt.Println("error while loading games: not headers found")
  w.WriteHeader(http.StatusInternalServerError)
  return
 }
 fmt.Println("Games loaded")
 newgames, err = compute.Compute(settings.ComputedFields, games[0], games[1:])
 if err != nil {
  fmt.Println("error while computing fields: ", err.Error())
  w.WriteHeader(http.StatusInternalServerError)
  return
 }
 fmt.Println("Fields computed")
 result := append([][]string{}, compute.AppendComputedFieldsToHeaders(settings.ComputedFields, games[0]))
 result = append(result, newgames...)
 err = saveGames(result)
 if err != nil {
  fmt.Println("error while saving new games: ", err.Error())
  w.WriteHeader(http.StatusInternalServerError)
  return
 }
 w.WriteHeader(http.StatusOK)
 return
}
```

Pas enchanté par le nom de la fonction 😟 mais maintenant je peux tester les cas d'erreur en injectant des fonctions qui échouent de la manière dont je le souhaite.

```golang
func loadInvalidSettings() (settings.Settings, error) {
 return settings.Settings{}, errors.New("For test")
}

func loadGamesWithoutHeaders() ([][]string, error) {
 return [][]string{}, nil
}

func saveGamesDoNothing(games [][]string) error {
 return nil
}

func TestInjectedHandleReturn500WithInvalidSettings(t *testing.T) {
 fakeWriter := httptest.NewRecorder()
 expectedStatus := http.StatusInternalServerError
 injectedHandle(fakeWriter, loadInvalidSettings, loadGamesWithoutHeaders, saveGamesDoNothing)
 resp := fakeWriter.Result()
 statusCode := resp.StatusCode
 if statusCode != expectedStatus {
  t.Errorf("unexpected statusCode %v while receiving invalid settings - expected %v", statusCode, expectedStatus)
 }
}

func loadInvalidGames() ([][]string, error) {
 return [][]string{}, errors.New("For test")
}

func loadEmptySettings() (settings.Settings, error) {
 return settings.Settings{}, nil
}

func TestInjectedHandleReturn500WithInvalidGames(t *testing.T) {
 fakeWriter := httptest.NewRecorder()
 expectedStatus := http.StatusInternalServerError
 injectedHandle(fakeWriter, loadEmptySettings, loadInvalidGames, saveGamesDoNothing)
 resp := fakeWriter.Result()
 statusCode := resp.StatusCode
 if statusCode != expectedStatus {
  t.Errorf("unexpected statusCode %v while receiving invalid settings - expected %v", statusCode, expectedStatus)
 }
}

func TestInjectedHandleReturn500WithGamesButNoHeaders(t *testing.T) {
 fakeWriter := httptest.NewRecorder()
 expectedStatus := http.StatusInternalServerError
 injectedHandle(fakeWriter, loadEmptySettings, loadGamesWithoutHeaders, saveGamesDoNothing)
 resp := fakeWriter.Result()
 statusCode := resp.StatusCode
 if statusCode != expectedStatus {
  t.Errorf("unexpected statusCode %v while receiving invalid settings - expected %v", statusCode, expectedStatus)
 }
}

func loadGames() ([][]string, error) {
 return [][]string{{"column"}}, nil
}

func saveGamesFail(games [][]string) error {
 return errors.New("For test")
}

func TestInjectedHandleReturn500WithErrorInSavingGames(t *testing.T) {
 fakeWriter := httptest.NewRecorder()
 expectedStatus := http.StatusInternalServerError
 injectedHandle(fakeWriter, loadEmptySettings, loadGames, saveGamesFail)
 resp := fakeWriter.Result()
 statusCode := resp.StatusCode
 if statusCode != expectedStatus {
  t.Errorf("unexpected statusCode %v while receiving invalid settings - expected %v", statusCode, expectedStatus)
 }
}

func loadExampleSettings() (settings.Settings, error) {
 return settings.Settings{
  Name:           "test",
  Organizers:     []string{"Test"},
  ComputedFields: []compute.ComputedField{{Name: "JustForTest", DefaultValue: "42"}},
 }, nil
}

func saveGamesFailsInNotExpected(games [][]string) error {
 expected := [][]string{
  {"id_1", "team_1", "td_1", "id_2", "team_2", "td_2", "JustForTest"},
  {"1", "first_team", "3", "2", "third_team", "1", "42"},
 }

 if cmp.Equal(games, expected) {
  return nil
 }
 return fmt.Errorf("Games to save not as expected: %s", games)
}

func loadExampleGames() ([][]string, error) {
 games := [][]string{
  {"id_1", "team_1", "td_1", "id_2", "team_2", "td_2"},
  {"1", "first_team", "3", "2", "third_team", "1"},
 }
 return games, nil
}

func TestInjectedHandle(t *testing.T) {
 fakeWriter := httptest.NewRecorder()
 expectedStatus := http.StatusOK
 injectedHandle(fakeWriter, loadExampleSettings, loadExampleGames, saveGamesFailsInNotExpected)
 resp := fakeWriter.Result()
 statusCode := resp.StatusCode
 if statusCode != expectedStatus {
  t.Errorf("unexpected statusCode %v while receiving invalid settings - expected %v", statusCode, expectedStatus)
 }
}
```

## Conclusion partielle

C'est tout pour cette fois ! Nous n'avons pas été jusqu'à la lecture et l'écriture dans du stockage objet et ma lambda échoue lamentablement (à cause la lectures et l'écriture dans les fichiers locaux) quand je la déploie. Mais nous avons appris :

1. Qu'une `serverless function` est fonction qui prend en paramètres un `http.ResponseWriter` et un pointeur vers une `http.Request` ;
2. Que l'on peut tester unitairement en injectant les fonctions impures, il reste à voir comment on peut faire des tests sur les fonctions impures.
3. Le principe de déploiement et d'utilisation ressemble aux Lambdas.

Si vous avez envie de raconter vos explorations sur les technologies serverless, partager les avec moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
