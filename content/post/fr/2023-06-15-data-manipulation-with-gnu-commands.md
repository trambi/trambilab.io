---
title: "Manipuler les données avec des commandes GNU"
date: "2023-06-15"
draft: false
description: "head, tail, cut et paste" 
author: "Bertrand Madet" 
tags: ["outils"]
---

A l'occasion de la transformation d'un fichier SQL de requêtes INSERT en requêtes UPDATE, j'ai découvert la commande `paste`. J'en profite pour faire une présentation de certaines commandes de manipulation de données, que l'on retrouve dans les systèmes d'inspiration UNIX : Linux, BSD et même MacOS.

## head et tail

[head](https://www.gnu.org/software/coreutils/manual/html_node/head-invocation.html) permet d'afficher les premières lignes (par défaut les dix premières lignes) de l'entrée standard ou d'un fichier. L'option `-` suivie d'un nombre permet de spécifier le nombre de lignes.

[tail](https://www.gnu.org/software/coreutils/manual/html_node/tail-invocation.htm) permet d'afficher les dernières lignes (par défaut les dix dernières lignes) de l'entrée standard ou d'un fichier. L'option `-` suivie d'un nombre permet de spécifier le nombre de lignes et l'option `+` suivie d'un nombre indique d'afficher les dernières lignes à partir de la ligne indiquée par le nombre. On peut aussi afficher les dernières lignes de manière continue avec l'option `-f`, ce qui est très utile pour surveiller les fichiers de journalisation.

Quand je dis afficher cela signifie que cela sort sur la sortie standard. On va donc pouvoir utiliser `|` (ou pipe :gb:) pour rediriger la sortie standard d'un processus vers l'entrée standard du processus suivant. Si le fichier `1-to-15.txt` contient plus de 5 lignes, la commande suivante va afficher le contenu du fichier de la troisième à la cinquième ligne.

```bash
head -5 1-to-15.txt | tail +3
```

head et tail sont donc des formidables outils pour extraire des lignes particulières d'un fichier.

## cut et paste

[cut](https://www.gnu.org/software/coreutils/manual/html_node/cut-invocation.html) permet de découper chaque ligne de l'entrée standard ou d'un fichier. Il faut utiliser une et une seule option suivante :

- `-b` pour découper la ligne en fonction par octet ;
- `-c` pour découper la ligne en fonction par caractère ;
- `-f` pour découper par champ, le séparateur par défaut est l'espace et peut être spécifié par l'option `-d` suivie du séparateur.

Personnellement, j'utilise toujours le découpage par champ. C'est très pratique pour extraire des informations. Par exemple la commande suivante retournera `ab cd`.

```bash
echo "ab bc cd" | cut -f1,3 -d" "
```

Cela fonctionne pour une ligne mais aussi ligne par ligne.

[paste](https://www.gnu.org/software/coreutils/manual/html_node/paste-invocation.html) permet de coller chaque ligne des fichiers d'entrée ensemble. Si le fichier `fichier1` et `fichier2` contiennent ceci :

```txt
ligne1fichier1
ligne2fichier1
ligne3fichier1
```

```txt
ligne1fichier2
ligne2fichier2
ligne3fichier2
```

La commande suivante

```bash
paste fichier1 fichier2
```

retournera :

```txt
ligne1fichier1  ligne1fichier2
ligne2fichier1  ligne2fichier2
ligne3fichier1  ligne3fichier2
```

cut et paste sont très complémentaires et utiles pour travailler sur le contenu de chaque ligne.

## Conclusion

Il est possible de faire énormément de choses en combinant les commandes simples avec pipe. head, tail, cut et paste font partie avec d'autres commandes comme cp, cd, ls des outils de base (:gb: coreutils). Le projet [GNU (https://www.gnu.org/)](https://www.gnu.org/) avait pour but initial de fournir une implémentation libre de ces commandes, il y est parvenu. Les commandes de base des distributions avec un noyau Linux sont très souvent les implémentations GNU.

Comme ces outils sont indispensables à l'utilisation d'un ordinateur, c'est pour cela que l'on cela que l'on parle du système d'exploitation `GNU/Linux`, les outils GNU avec le noyau Linux.

Et vous avez-vous des commandes préférées ? Si oui, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
