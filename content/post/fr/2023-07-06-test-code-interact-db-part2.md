---
title: "Tests d'interaction avec une base de données - partie 2"
date: "2023-07-06"
draft: false
description: "illustré avec Python et PostgreSQL"
tags: ["python","test"]
author: "Bertrand Madet"
---

Après les [tests d'interaction avec une base de données pour NodeJS](({< ref "/post/fr/2023-05-18-test-code-interact-db-part1.md" >}}), nous allons regarder le cas du Python.

## Injecter la dépendance

Même si en Python le remplacement d'une fonction par une fonction de test (ou :gb: monkey patching :monkey:) est facile, je persiste dans mon conseil d'injecter une instance de connexion dans la fonction.  Cela rend l'intention claire alors que le monkey patching peut l'impression de bidouiller des détails d'implémentation.

Personnellement, j'apprécie avoir une fabrique de fonction à partir d'une connexion :

```python
def update_pg(conn):
    def inner(newText, identifier):
        cur = conn.cursor()
        cur.execute(
            "UPDATE mytable SET atext=%s WHERE anumber=%s", [newText, identifier]
        )

    return inner
```

Cela garde le paramètre de connexion séparée du reste des paramètres et d'utiliser la fonction résultante en masquant le détail d'implémentation d'utilisation d'une base de données. On pourrait imaginer avoir une autre fonction qui utiliserait un client boto3 :

```python
def update_s3(client):
  def inner(new_text,identifier):
    # Suite d'operation qui mette à jour un objet du S3
    pass
  return inner
```

Puis d'injecter indifféremment pour les processus métier, la fonction spécialisée dans la mise à jour en base de donnée `myupdate = update_pg(myconn)` ou dans S3 `myupdate = update_s3(myclient)`.

Dans un logiciel de taille conséquente, on préférera créer une interface pour abstraire la connexion. Changer le mode de stockage est une problématique courante (comme toutes les entrées et sorties).

L'injection de dépendance permet aussi et surtout d'utiliser facilement les techniques suivantes.

## Utiliser une base de données temporaire

[pytest-postgresql (https://pypi.org/project/pytest-postgresql/)](https://pypi.org/project/pytest-postgresql/) permet de faciliter l'utilisation de base de données à usage unique pour un test. On peut peupler la base de données, lancer la fonction et vérifier ensuite les effets potentiels de la fonction dans la base de données.

```python
from pytest_postgresql import factories

postgresql_in_docker = factories.postgresql_noproc()
postgresql = factories.postgresql("postgresql_in_docker", dbname="test")

# Fonction à tester, uniquement dans le même fichier que les fonctions de test pour illustrer le propos
def update_pg(conn):
    def inner(newText, identifier):
        cur = conn.cursor()
        cur.execute(
            "UPDATE mytable SET atext=%s WHERE anumber=%s", [newText, identifier]
        )

    return inner


def test_update(postgresql):
    # GIVEN 
    # Avec la table mytable et un enregistrement
    with postgresql.cursor() as cur:
        cur.execute("CREATE TABLE mytable (atext text, anumber int);")
        cur.execute(
            "INSERT INTO mytable (atext,anumber) VALUES(%s,%s);", ["first text", 1]
        )
    # WHEN
    updated_text = "modifiedText"
    id = 1
    update(postgresql)(updated_text, id)
    # THEN
    # l'enregistrement doit être changé
    with postgresql.cursor() as cur:
        cur.execute("SELECT atext FROM mytable WHERE anumber=%s", [id])
        result = cur.fetchall()
    assert result == [(updated_text)]
```

[pytest-mysql (https://pypi.org/project/pytest-mysql/)](https://pypi.org/project/pytest-mysql/) permet de faire la même chose avec une base de données MySQL.

- :+1: Cela permet de tester le comportement en base et non les requêtes ;
- :-1: Cela nécessite d'avoir un serveur opérationnel au moment des tests.
- :monocle_face: Juste pour être clair, ce n'est pas un test unitaire (et cela n'est pas forcément grave).

## Imiter psycopg2 manuellement

On peut imiter le comportement des modules de connexion, pour l'instant je n'ai pas trouvé de module python qui automatise cela, donc il faut le coder avec le module "unittest.mock" et ses classes "Mock" et "MagicMock". Si les fonctions utilisées retournent quelque chose, il faut implémenter les valeurs retournées.

```python
from unittest.mock import Mock

def test_update2():
    expectedQuery = "UPDATE mytable SET atext=%s WHERE anumber=%s;"
    mock_con = Mock()
    mock_cur = mock_con.cursor.return_value
    updated_text = "modifiedText"
    id = 1
    update(mock_con)(updated_text,id)
    mock_cur.execute.assert_called_with(expectedQuery,[updated_text, id])
```

Le point négatif, c'est que si vous codez tout un module sans tester les requêtes sur une base Postgres, vos tests peuvent indiquer que tout est ok alors que les requêtes ne sont même pas valides. Récemment, j'ai développé une fonction en testant comme cela, j'avais pris l'habitude des requêtes paramétrées Postgres avec Node.js et pg, donc toutes mes indications de paramètres étaient erronées : "?" au lieu de "%s" : exploding_head:

Par contre pour figer une implémentation par exemple avant un refactor ou surtout tester des fonctions avec des logiques complexes mais des requêtes simples et éprouvées cela peut être intéressant.

- :+1: Cela ne nécessite pas d'avoir de serveur de base de données au moment des tests ;
- :-1: Cela teste l'implémentation et non le comportement.

## Conclusion

Voici deux techniques de l'arsenal de Python. Le catalogue est moins fourni car mes besoins étaient moins variées que pour NodeJS. Le manque de bibliothèque python qui vérifie les requêtes pousse clairement à utiliser des modules comme "pytest-postgresql".

Je pense qu'il est possible d'employer des techniques similaires à TypeScript et JavaScript. Il convient donc de composer ses tests en fonction du contexte du logiciel et de la partie à tester. Par exemple, utiliser une technique d'enregistrement de requêtes pour tester une création de base de données fournira un script SQL de création de base validé par les tests.

Si vous avez des retours ou d'autres conseils pour tester les interactions avec les bases de données en Python, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
