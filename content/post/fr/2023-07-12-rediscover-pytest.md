---
title: "Redécouverte de pytest"
date: "2023-07-12"
draft: false
description: "les marques et les variables d'environnement"
tags: ["python","test", "outils"]
author: "Bertrand Madet"
---

*edité le 18 juillet 2023 : correction de l'utilisation de monkeypatch*

Je continue mes développements en Python et avec eux, j'ai redécouvert le module [pytest (https://docs.pytest.org/en/latest/)](https://docs.pytest.org/en/latest/). Pytest est un module en dehors de la bibliothèque standard de Python. Il vient compléter le module de bibliothèque standard [unittest (https://docs.python.org/3/library/unittest.html)](https://docs.python.org/3/library/unittest.html) et simplifier la mise en place des tests. Voici les fonctionnalités utiles qui m'avaient échappé précédemment.

## Marquer les tests

pytest permet de marquer les cas de tests avec le décorateur `pytest.mark`. Cela permet de sélectionner les tests avec une marque avec l'option `-m` suivie de la marque entre guillemets. Pour reprendre l'exemple du billet de blog précédent, on pourrait imaginer marquer le test utilisant un base de données avec "needdatabase".

```python
import pytest
from pytest_postgresql import factories
from unittest.mock import Mock

postgresql_in_docker = factories.postgresql_noproc()
postgresql = factories.postgresql("postgresql_in_docker", dbname="test")

# Fonction à tester, uniquement dans le même fichier que les fonctions de test pour illustrer le propos
def update_pg(conn):
    def inner(newText, identifier):
        cur = conn.cursor()
        cur.execute(
            "UPDATE mytable SET atext=%s WHERE anumber=%s", [newText, identifier]
        )

    return inner

@pytest.mark.needdatabase
def test_update(postgresql):
    # GIVEN 
    # Avec la table mytable et un enregistrement
    with postgresql.cursor() as cur:
        cur.execute("CREATE TABLE mytable (atext text, anumber int);")
        cur.execute(
            "INSERT INTO mytable (atext,anumber) VALUES(%s,%s);", ["first text", 1]
        )
    # WHEN
    updated_text = "modifiedText"
    id = 1
    update(postgresql)(updated_text, id)
    # THEN
    # l'enregistrement doit être changé
    expected = [()]
    with postgresql.cursor() as cur:
        cur.execute("SELECT atext FROM mytable WHERE anumber=%s", [id])
        result = cur.fetchall()
    assert result == [(updated_text,)]

def test_update2():
    expectedQuery = "UPDATE mytable SET atext=%s WHERE anumber=%s;"
    mock_con = Mock()
    mock_cur = mock_con.cursor.return_value
    updated_text = "modifiedText"
    id = 1
    update(mock_con)(updated_text,id)
    mock_cur.execute.assert_called_with(expectedQuery,[updated_text, id])
```

> :warning: Si on utilise les décorateurs pytest, il faut importer pytest avec `import pytest`.

Si on a une base de données disponible, on peut lancer les tests possédant la marque "needdatabase" avec la commande `pytest -m "needdatabase"` et si on a pas de données disponibles (par exemple dans une étape d'intégration continue dédié aux tests avec un seul processus). On peut aussi appeler les tests ne possédant pas la marque "needdatabase" avec la commande `pytest -m "not needdatabase"`.

Les exemples sont présents dans la [documentation (https://docs.pytest.org/en/7.4.x/example/markers.html#mark-examples)](https://docs.pytest.org/en/7.4.x/example/markers.html#mark-examples).

## Gérer les variables d'environnement

Les variables d'environnement sont des éléments essentiels pour ajuster le comportement des programmes. En particulier pour les programmes fonctionnant dans des conteneurs ou dans des lambdas. Lors de l'exécution des tests, il est souvent nécessaire de définir des variables d'environnement spécifiques. La difficulté est de garder les tests indépendants les uns des autres. Il est donc très important de nettoyer les variables d'environnement entre chaque test : on peut le faire d'une manière naïve.

```python
import os
import pytest

def test_throw_exception_if_environment_variables_are_unset():
    # backup
    backenv1 = os.environ["MY_ENV_VAR1"]
    backenv2 = os.environ["MY_ENV_VAR2"]
    del os.environ["MY_ENV_VAR1"]
    del os.environ["MY_ENV_VAR2"]

    with pytest.raises(Exception) as e_info:
        function_env_needed()

    # restore
    os.environ["MY_ENV_VAR1"] = backenv1
    os.environ["MY_ENV_VAR2"] = backenv2
```

Les fonctions dédiées `pytest.monkeypatch` comme `delenv` fournissent une approche plus simple et moins aléatoire en terme de comportement. Voilà le code résultant :

```python
import os
import pytest

def test_throw_exception_if_environment_variables_are_unset(monkeypatch):
    monkeypatch.delenv("MY_ENV_VAR1",raising=False)
    monkeypatch.delenv("MY_ENV_VAR2",raising=False)
    with pytest.raises(Exception) as e_info:
        function_env_needed()
```

La fonction `setenv` permet, elle, de configurer temporairement la valeur  d'une variable d'environnement. [La documentation (https://docs.pytest.org/en/7.4.x/how-to/monkeypatch.html)](https://docs.pytest.org/en/latest/how-to/monkeypatch.html) détaille les autres fonctions du module. On peut, par exemple, utiliser les contextes pour gérer plus finement la durée de modification des variables d'environnement.

## Conclusion

En conclusion, lors de ma redécouverte de pytest, j'ai trouvé particulièrement utile l'utilisation du marquage de tests pour une fonction de recherche qui prenait beaucoup de temps (plus de trois minutes). Cela m'a permis de mettre au point une fonction testée dans un même fichier sans attendre à chaque essai. J'espère que cela permettra aussi à terme de sélectionner les tests en fonction de l'étape d'intégration continue : tests unitaires ou tests d'intégration.

Par ailleurs, la gestion des variables d'environnement a grandement simplifié les tests de mes lambdas et de mes conteneurs. Cela m'a permis de configurer facilement les valeurs nécessaires pour chaque scénario de test, en améliorant l'indépendance et la reproductibilité des résultats.

Je trouve que Python dispose d'un ensemble d'outils complet avec unittest, pytest, hypothesis et moto. Les développeurs Python ont un large choix d'outils pour concevoir et exécuter des tests de manière efficace.

Si vous avez des retours ou d'autres conseils sur les outils de test en Python, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
