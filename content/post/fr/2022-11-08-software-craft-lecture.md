---
title: "Software Craft - Fiche de lecture"
date: "2022-11-08"
draft: false
description: "Livre collectif sur le TDD, le code propre et autres pratiques"
tags: ["fiche de lecture","Software Craftmanship","base","tdd"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Aux développeuses et aux développeurs qui veulent aborder le `Software Craftmanship`  ou s'améliorer en tant que développeur logiciel professionel avec un livre court en français :fr:.

## Ce qu'il faut retenir

L'approche Software Craftmanship (artisanat logiciel :fr:) propose une approche du développement logiciel plus axée sur les pratiques. Le livre détaille les pratiques comme le [TDD - Test driven development]({{< ref "/tags/tdd/" >}}), l'amélioration continue du code ou le binomage ...

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Court (moins de 300 pages)| :-1: Peut-être trop axé sur le TDD|
| :+1: Simple sans être simpliste| :-1:Les aspects plus comportementaux sont un peu survolés|
| :+1: Concret avec du code, des katas||

En utilisant les pratiques comme points d'entrée, l'approche est concrète et activable. Le code est en Java ou C# sans que cela soit handicapant de ne pas connaître les langages (ou d'être rouillé). Il y a deux katas qui sont détaillés.

Le TDD est très bien expliqué, détaillé avec les différentes approches London :gb: ou Chicago :us:. De mon point de vue, TDD reste une manière de faire et d'autres méthodes donnent de bons résultats dans certains contextes. Par exemple, l'approche PPP (**P**seudocode **P**rogramming **P**rocess) avec une bonne suite de tests peut donner de bons résultats également (cf. Code Complete et [ma fiche de lecture]({{< ref "/post/fr/2021-11-01-code-complete-lecture.md" >}})).

La compacité et le parti pris de l'approche orientée pratiques amènent à des survols sur la partie comportementale comme l'égoless programming. Mais on retrouve une liste d'ouvrages de référence du domaine qui abordent plus en détail le domaine. Ce livre est une bonne surprise, et surtout une très bonne introduction sur le Software Craftmanship.

## Détails

- Titre : Software Craft - TDD, Clean Code et autres pratiques essentielles
- Auteur : Cyrille Martraire, Arnaud Thiéfaine, Dorra Bartaguiz, Fabien Hiegel et Houssam Fakih
- Langue : :fr:
- ISBN : 978-2-10-082520-2
