---
title: "Test d'une API REST"
date: "2022-06-22"
draft: false
description: "au dela des tests unitaires"
tags: ["test","Software Craftmanship", "REST API"]
author: "Bertrand Madet"
---

J'ai consacré deux billets sur les tests de composants React (cf. [partie 1]({{< ref "/post/fr/2021-02-07-tests-react-components-part1/index.md" >}}) et [partie 2]({{< ref "/post/fr/2021-12-08-tests-react-components-part2.md" >}})). Aujourd'hui nous allons nous intéresser aux tests d'une API REST. Je vais utiliser certains notions que j'avais défini dans mon billet sur la [classification des tests]({{< ref "post/fr/2022-04-26-test-classification.md" >}}).

On va s'intéresser aux tests d'une API REST en python et en node.js.

## API REST

Une API [REST (https://fr.wikipedia.org/wiki/Representational_state_transfer)](https://fr.wikipedia.org/wiki/Representational_state_transfer) est une interface sous forme de service web. Les ressources sont identifiées un chaîne de caractères l'[URI - **U**niversal **R**essource **I**dentifier](https://fr.wikipedia.org/wiki/Uniform_Resource_Identifier) ou `endpoint`.

Une API REST a l'avantage d'être facile à interroger automatiquement et donc dans des tests automatiques.

## Structure d'un test

Les tests ont souvent la même structure, on commence par la mise en place puis on déclenche un évènement et enfin la vérification.

Il existe un langage spécifique au domaine, le [Gherkin (https://cucumber.io/docs/gherkin/reference/)](https://cucumber.io/docs/gherkin/reference/) qui associe :

- `GIVEN` à la mise en place ;
- `WHEN` au déclencheur ;
- `THEN` à la vérification.

## Tests unitaires

En Python comme en node.js, l'idée est de tester au maximum le comportement des composants élémentaires, c'est plus simple et cela permet de documenter le fonctionnement des composants. Pour Python, j'utilise le moteur de test : [pytest (https://docs.pytest.org/en/7.1.x/)](https://docs.pytest.org/en/7.1.x/) et pour node.js : le moteur de test [jest(https://jestjs.io/)](https://jestjs.io/).

```javascript
describe('add',() => {
  it('should return the add of two inputs',() => {
    // GIVEN
    const [input1, input2] = [1, 2];
    const expected = 3;
    // WHEN
    const result = add(input1,input2);
    // THEN
    expect(result).toBe(expected);
  });
});
```

```python
def test_add_two_integers():
  # GIVEN
  [input1,input2] = [1,2]
  expected = 3
  # WHEN
  output = add(input1,input2)
  # THEN
  assert(expected ==output)
```

Nous le verrons par la suite, les moteurs de test peuvent être utilisés pour d'autres types de test.

## Tests intégration

Le test des endpoints eux-même va vraisemblablement impliquer différentes composantes. On sera pas dans des tests en isolation et on ne peut plus donc parler de tests unitaires mais de tests d'intégration.

Il existe des outils pour tester les API:

- [TestClient de Starlette (https://www.starlette.io/testclient/)](https://www.starlette.io/testclient/) pour Python ;
- [supertest (https://github.com/visionmedia/supertest)](https://github.com/visionmedia/supertest) pour JavaScript.

On peut reprendre la structure des tests. Le déclencheur sera l'appel aux l'API.

En python avec starlette, cela peut prendre cette forme.

```python
from starlette.testclient import TestClient
# Il faut importer l'application app

def test_app():
  # GIVEN
  url = '/'
  expectedstatus = 200
  client = TestClient(app)
  # WHEN
  response = client.get(url)
  # THEN
  assert response.status_code == expectedstatus
```

Avec JavaScript et supertest, on peut faire comme ça (extrait de la documentation de supertest) :

```javascript
describe('GET /user', function() {
  it('responds with json', function(done) {
    request(app)
      .get('/user')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });
});
```

Ses outils ont l'avantage de bien s'intégrer bien aux moteurs de test. On peut par exemple calculer le taux de couverture et l'intégrer avec les tests unitaires.

## Bonus

Il est possible de créer un `Extrait utilisateur` (ou `snippet` :gb:). Quand on va taper `test-jest`,on peut avoir une insertion d'un test.

Pour `Visual Studio Code`, il faut aller dans `Fichier:Préférences:Extraits utilisateur` (`File:Preferences:Configure User Snippets` :gb:) et sélectionner le langage `JavaScript` et il faut ajouter l'entrée :

```json
  "Jest Test file":{
    "prefix": ["jest-test-file","jest-test"],
    "body": [
      "describe('OurComponent',() => {",
      "  it('should do something',() => {",
      "    $LINE_COMMENT GIVEN",
      "    $0",
      "    $LINE_COMMENT WHEN",
      "    $LINE_COMMENT THEN",
      "    expect(false).toBeTruthy();",
      "  });",
      "});"],
    "description": "Create a jest file"
 }
```

La procédure est décrite dans la page [Snippets in Visual Studio Code (https://code.visualstudio.com/docs/editor/userdefinedsnippets)](https://code.visualstudio.com/docs/editor/userdefinedsnippets).

## Conclusion

Il est aussi possible de lancer des tests end-to-end en utilisant des fonctions d'interrogation réseau (requests et fetch par exemple) sur l'API REST une fois déployée. Cela permet de valider la bonne configuration et le bon déploiement de l'API REST.

Voici un résumé de techniques pour tester une API. J'espère que cela vous permettra d'avoir plus d'outils pour tester vos API REST. Pour ma part, cela m'a permis de mettre en place le snippet décrit en bonus.

Si vous avez des expériences ou des questions sur les tests logiciels, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
