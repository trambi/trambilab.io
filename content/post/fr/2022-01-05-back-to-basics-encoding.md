---
title: "A propos de l'encodage"
date: "2022-01-05"
draft: false
description: "Commençons l'année avec un retour aux bases"
tags: ["encodage","base"]
author: "Bertrand Madet"
---

J'espère que vous avez passé une bonne fin d'année. Je vous propose de commencer l'année 2022 avec un concept de base - l'encodage.

## Les billets à lire avant celui-ci

Difficile de parler d'encodage sans parler du billet de blog de Joel Spotsky [The Absolute Minimum Every Software Developer Absolutely, Positively Must Know About Unicode and Character Sets (No Excuses!) - https://www.joelonsoftware.com/2003/10/08/the-absolute-minimum-every-software-developer-absolutely-positively-must-know-about-unicode-and-character-sets-no-excuses/](https://www.joelonsoftware.com/2003/10/08/the-absolute-minimum-every-software-developer-absolutely-positively-must-know-about-unicode-and-character-sets-no-excuses/). Il est disponible uniquement en l'anglais :gb:, il y avait une version française :fr: mais le lien est mort :cry:.

En français, il y a aussi le billet de Sam de Sam & Max : [Le type bytes n'est pas du texte (https://sametmax2.com/le-type-bytes-nest-pas-du-texte/index.html)](https://sametmax2.com/le-type-bytes-nest-pas-du-texte/index.html). :warning: Le sous titre du site Sam & Max est `du code et du cul` et certaines photos sont aussi moches que certains articles sont bons - je vous aurai prévenu :innocent:.

## Tout est binaire

Les processeurs ne comprennent qu'une chose, le binaire. Donc il faut tout convertir en binaire.

Quand je dis tout, cela inclut :

- les nombres entiers ;
- les [nombres flottants (cf. mon billet sur les nombres flottants)]({{< ref "/post/fr/2021-05-17-a-propos-des-nombres-decimaux.md" >}}) ;
- les images ;
- la musique ;
- le code ;
- le texte...

Ces conversions suivent des conventions. Et de la même manière, qu'il existe plusieurs formats d'images - `bitmap`, `jpeg`, `png`, `gif`, `tiff`, il existe plusieurs formats de texte, appelé encodages.

## Encodage

L'encodage permet d'associer à une suite de zéro et de un (que l'on peut représenter par un nombre) à un caractère. Comme indiqué plus haut, il existe plusieurs encodages, le caractère `a` est représenté par :

- `01100001` en ASCII ;
- `01100001` en ISO-8859-1 ;
- `01100001` en UTF-8 ;
- `00000000 01100001` en UTF-16 ;
- `00000000 00000000 00000000 01100001` en UTF-32.

Les caractères de l'alphabet latin sont pratiquement encodés de la même manière, ce qui fait que l'on ne se rend pas forcément compte des différents encodages.

Parmi les encodages, il y a ceux qui encodent chaque caractère avec une taille fixe comme l'ASCII, l'UTF-16 et l'UTF-32. Et ceux qui utilise des tailles variables comme l'UTF-8.

## UTF-8

L'UTF-8 est l'encodage standard aujourd'hui. Il se caractérise par un encodage de taille variable et couvre les caractères Unicode (soit plus de 100 000 caractères). C'est l'encodage par défaut de Golang et Python 3 (et TypeScript aussi il me semble).

## Préconisations

Partant du constat qu'il existe plusieurs encodages pour le format texte et que l'UTF-8 n'est pas encore omniprésent, je préconise :

- Réglez votre environnement de développement pour utiliser l'encodage `UTF-8` dans vos fichiers source ;
- Renseignez-vous sur l'encodage des entrées de vos programmes ;
- Dans vos programmes :

  - Utilisez l'encodage `UTF-8` en interne,
  - Importez vos données textuelles avec l'encodage adéquate,
  - Exportez (si possible) vos données textuelles avec l'encodage `UTF-8`.

Comme contrairement aux encodages encore très répandus qui utilisent un octet par caractère, l'UTF-8 utilise un, deux ou quatre octets. Il est critique de parcourir les chaînes de caractère par caractère (et non par octet).

## Conclusion

Un court billet qui vous laissera le temps d'aller lire le billet de Joel Spotsky [The Absolute Minimum Every Software Developer Absolutely, Positively Must Know About Unicode and Character Sets (No Excuses!)](https://www.joelonsoftware.com/2003/10/08/the-absolute-minimum-every-software-developer-absolutely-positively-must-know-about-unicode-and-character-sets-no-excuses/).

Si vous avez des retours ou des conseils sur des notions de base de la programmation, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
