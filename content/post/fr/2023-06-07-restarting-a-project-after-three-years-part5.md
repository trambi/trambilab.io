---
title: "Reprendre un projet après 3 ans - partie 5"
date: "2023-06-07"
draft: false
description: "Un an de plus et c'est le blocage"
tags: ["projet perso","php","jeu"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série :

- [le premier sur les étapes pour réussir à reconstruire le logiciel]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years" >}}) ;
- [le deuxième sur l'ajout de fonctionnalité]({{< ref "/post/fr/2022-04-13-restarting-a-project-after-three-years-part2" >}}) ;
- [le troisième sur le bilan sur le logiciel]({{< ref "/post/fr/2022-04-22-restarting-a-project-after-three-years-part3" >}}) ;
- [le quatrième sur les leçons que j'en ai tiré]({{< ref "/post/fr/2022-05-11-restarting-a-project-after-three-years-part4" >}}) ;
- [le sixième sur la situation deux après]({{< ref "/post/fr/2024-05-22-restarting-a-project-after-three-years-part6" >}}).

J'ai eu de nouveau l'occasion d'utiliser le logiciel de gestion de tournoi. Le tournoi s'est bien passé et le logiciel a bien fonctionné. En revanche, je n'ai pas réussi à reconstruire le conteneur. J'ai du utiliser l'image de conteneur de l'année précédente. Comme le logiciel était suffisant pour fonctionner sans modification, je me suis cantonné à le déployer et a modifié la configuration pour accueillir une nouvelle édition.

## Situation actuelle bloquée

```mermaid
graph LR;
    users([Utilisateurs])-->front[Application utilisateur]
    front-->api[API REST]
    orga([Organisateur])-->admin[Application d'administration web]
    api-->db[(Base de données - MariaDB)]
    admin-->db
```

La répartition des technologies est la suivante :

- L'application utilisateur est une application monopage en AngularJS ;
- L'API REST est un bundle Symfony3 ;
- L'application d'administration est un bundle Symfony3.

Comme indiqué plus haut, la partie Symfony3 n'est pas possible à reconstruire à cause de l'obsolescence de Symfony3 et de l'impossibilité de créer le projet avec `composer`.

## Évolutions possibles

Pour continuer à apporter des améliorations au logiciel, il va falloir trouver des solutions.

### Migration des applications en Symfony 6

La première solution est de passer les bundles Symfony3 dans une version maintenue (5 ou 6). Pour avoir essayé de migrer la solution de Symfony3 à Symfony4, les efforts de migration seraient importants. Le plus simple serait d'insérer les Bundles dans un projet type Symfony puis de gérer les fonctions dépréciées.

```mermaid
graph LR;
    users([Utilisateurs])-->front[Application utilisateur]
    front-->api["`API REST - **à migrer**`"]
    orga([Organisateur])-->admin["`Application d'administration web - **à migrer**`"]
    api-->db[(Base de données - MariaDB)]
    admin-->db
```

### Finir une application en Go

J'ai déjà commencé de coder une [API REST similaire en Go (https://gitlab.com/trambi/fantasyfootballapi)](https://gitlab.com/trambi/fantasyfootballapi). L'API REST stocke ses données sur le système local de fichier. Cette combinaison permet de simplifier l'installation puisque l'exécutable est autonome et il n'y a pas de serveur de base de données. L'application d'administration web n'est pas implémentée et pourrait être réalisée sous la forme d'application monopage appelant l'API REST.

```mermaid
graph LR;
    users([Utilisateurs])-->front[Application utilisateur]
    front-->api["`API REST - **en Go à finir**`"]
    orga([Organisateur])-->admin["`Application d'administration web - **à implémenter en Monopage**`"]
    admin-->api
    api-->filesystem[(Stockage dans le système de fichier)]
    admin-->filesystem
```

Une variante serait d'utiliser une base de données relationnelle pour accélérer l'implémentation en se basant sur les requêtes utilisées par les bundles Symfony.

## Conclusion

L'implémentation actuelle du logiciel a réalisé son office pendant au moins dix ans. Le fait d'avoir de ne pas pris en compte l'obsolescence des versions successifs de Symfony va amener des efforts considérables pour permettre au logiciel de continuer à évoluer.

Si vous avez des conseils sur la situation, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

Ce billet fait partie d'une série :

- [le premier sur les étapes pour réussir à reconstruire le logiciel]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years" >}}) ;
- [le deuxième sur l'ajout de fonctionnalité]({{< ref "/post/fr/2022-04-13-restarting-a-project-after-three-years-part2" >}}) ;
- [le troisième sur le bilan sur le logiciel]({{< ref "/post/fr/2022-04-22-restarting-a-project-after-three-years-part3" >}}) ;
- [le quatrième sur les leçons que j'en ai tiré]({{< ref "/post/fr/2022-05-11-restarting-a-project-after-three-years-part4" >}}) ;
- [le sixième sur la situation deux après]({{< ref "/post/fr/2024-05-22-restarting-a-project-after-three-years-part6" >}}).
