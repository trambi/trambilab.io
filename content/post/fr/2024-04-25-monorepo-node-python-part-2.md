---
title: "Mono-dépôt Node.js, Python - la couverture de test"
date: "2024-04-25"
draft: false
description: "En se servant d'outils initialement pour Java"
tags: ["python","monorepository","javascript","typescript"]
author: "Bertrand Madet"
---

Il s'est passé quelques mois depuis le [premier billet sur mon projet actuel ]({{< ref "/post/fr/2023-10-19-monorepo-node-python-part-1" >}}). Pour rappel, il s'agit d'un projet déployé sur AWS avec une application web en TypeScript et React.js et avec des traitements en JavaScript et Python. Le premier changement a été de passer à TypeScript 🎉 pour l'application web. Cela été un peu éprouvant mais je ne regrette absolument pas (et cela sera peut-être le sujet d'un prochain billet 🤔).

Dans mon premier article, j'étais resté sur une difficulté avec la couverture de code. Et dans mon précédent projet, j'avais été obligé de coder un script bash pour calculer la couverture de tests des différents éléments en sommant d'un côté les nombres de lignes de code couvertes par les tests et de l'autre les nombres de lignes de code.

## Couverture de code ?

La couverture de code est la quantité de code qui couverte par des tests, elle s'exprime en pourcentage. Les quantités de code peuvent s'exprimer en fonction, en condition, en instruction. Avec certaines normes ou dans certains domaines du développement, c'est un indicateur très important. 

Il y a plusieurs manière de compter la quantité de code. Si on imagine le code simpliste suivant :

```go
func absolute(input int) int {
	if input < 0 {
		return -input
	}
	return input
}
```

Et que l'on exécute la fonction `absolute` pour l'entrée 15, on aura une couverture de test : 

- de 100% (1/1) si on compte les nombres de fonctions couvertes ;
- de 50% (1/2) si on compte les chemins d'exécution ;
- de 66.67% (2/3) si on compte les instructions ou (les lignes).

C'est un indicateur très puissant, mais :rotating_light: :

1. Ce n'est pas parce qu'une instruction, fonction ou chemin d'exécution sont parcourus pendant les tests, que les tests vérifient bien la sortie.
1. De mon point de vue, ce n'est **pas un indicateur absolu de qualité du code**. On peut dire qu'un code avec 0% est un code `legacy` - il n'y a pas de tests. Mais un code couvert à 80% par les tests n'est pas forcément un code de qualité.
1. C'est un indicateur pas un objectif en soi, à part dans certains contextes de développement.

Passées toutes ces réserves, je pense qu'il s'agit d'un **bon indicateur relatif**. Par exemple pour qualifier une `merge request`, une baisse de la couverture de test peut indiquer un oubli de tests.

## Cobertura à la rescousse

[Cobertura (http://cobertura.github.io/cobertura/)](http://cobertura.github.io/cobertura/) est un outil d'analyse de couverture de code pour Java. Il est intéressant dans notre cas car nos moteurs de test pour Python (Pytest) et TypeScript (Jest) peuvent exporter leurs résultats de couverture de test au format cobertura.

Et petit bonus, le format Cobertura peut être utilisé par GitLab.

### Python - xml

Le suivi de la couverture de test avec pytest utilise `coverage` et `pytest-cov`. Le [format de sortie xml (https://coverage.readthedocs.io/en/7.5.0/cmd.html#cmd-xml)](https://coverage.readthedocs.io/en/7.5.0/cmd.html#cmd-xml) est compatible avec Cobertura.


```bash
pytest --cov src --cov-report xml tests
```

L'option `--cov src` permet d'indiquer le répertoire où trouver les sources. `--cov-report xml` permet d'indiquer le format de sortie *XML*. Le fichier de couverture `coverage.xml` au format cobertura sera dans le répertoire courant.

Il est possible d'obtenir le même résultat sans installer `pytest-cov` en utilisant uniquement `coverage`.

```bash
coverage run --source src -m pytest tests
coverage xml
```

Le paquet `coverage` peut combiner des résultats de couverture fournis par `coverage` et les exporter ensuite au format XML. Ce qui peut utile dans un mono-dépôt Python.

### TypeScript - cobertura

Le moteur de test jest permet d'utiliser deux bibliothèques de couverture de tests mais au final les deux sont compatibles avec les formateurs de fichiers de couverture d'[istanbul (https://istanbul.js.org/)](https://istanbul.js.org/). Pour avoir le fichier de couverture au format Cobertura, il faut utiliser les options :

- `--coverage=true` pour indiquer à Jest de collecter la couverture de code ;
- `--coverageReporters=cobertura`  pour indiquer d'utiliser le format Cobertura ;
- `--collectCoverageFrom=src` pour indiquer que les sources sont le répertoire `src`;
- `--coverageDirectory=.` pour indiquer d'écrire le fichier de couverture dans le répertoire courant.

### On additionne le tout

Une fois qu'on a les fichiers de couverture au format Cobertura pour le Python et pour le TypeScript. On peut combiner ces fichiers avec la commande :

```bash
npx merge-cobertura -p -o coverage.xml package1=coverage-python.xml package2=coverage-typescript.xml
```

On utilise le paquet npm `merge-cobertura` qui a l'avantage de s'exécuter sans Python (pratique sur les images de conteneur qui ne contiennent que nodejs). L'option `-p` indique qu'une synthèse se fera sur la sortie standard, `-o coverage.xml` indique que le fichier résultant sera `coverage.xml`, `package1=coverage-python.xml` indique que le premier fichier de couverture à fusionner est `coverage-python.xml` et `package2=coverage-typescript.xml` indique que le deuxième fichier de couverture à fusionner est `coverage-typescript.xml`. J'ai testé jusqu'à 6 paquets à fusionner. Cette commande crée donc un fichier de couverture `coverage.xml` et affiche : 

```txt
Total line Coverage: 83.33%
Total branch Coverage: 0.00%
Total average Coverage: 41.67%
```

### Prise en compte dans GitLab

GitLab permet de prendre en compte la couverture de code de deux manières : 

- le pourcentage pour suivre le taux de couverture de code ;
- les fichiers de couverture pour [afficher dans le navigateur les lignes couvertures par les tests (https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html)](https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html).

Pour l'instant, je n'ai réussi à faire fonctionner que le premier, il faut ajouter dans l'étape d'intégration continue la ligne suivante (pour avoir le taux de couverture par ligne).

```yaml
  coverage: '/^Total line Coverage: (\d+\.\d+\%)$/'
```

## Conclusion

J'ai dû faire plusieurs essais pour arriver à cette configuration. Mais cela fonctionne avec TypeScript et Python. Il reste à faire fonctionner l'affichage des lignes de code dans GitLab, je pense que cela doit une question de chemin relatif des codes sources.

Si vous avez des retours ou des conseils sur la gestion d'un mono dépôt multi-langages, n'hésitez pas à les partager avec moi sur [X](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/). De mon côté, j'essayerai de rendre compte des évolutions dans notre processus de développement.
