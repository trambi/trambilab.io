---
title: "Formation : Migrating to AWS"
date: "2022-07-20"
draft: false
description: "du point de vue d'un développeur"
tags: ["devops","formation","cloud"]
author: "Bertrand Madet"
---

La semaine dernière, j'ai participé à une formation intitulée `Migrating to AWS`. La formation dure trois jours et aborde le processus de migration d'une entité vers AWS. Voici un bref résumé de la formation puis trois points qui m'ont paru significatifs.

## La formation

Basée sur l'expérience acquise en accompagnant leurs clients, AWS opère en trois phases : `Assess`, `Mobilize` et `Migrate and modernize`.

### Évaluer

La première phase vise à :

- fixer un objectif à la migration : diminution des coûts, réduction des temps de mise à disposition, augmenter la disponibilité ...
- évaluer la maturité des applications ;
- fixer un panel d'applications à migrer.

### Mobiliser

La deuxième phase consiste à préparer la migration en :

- cartographiant plus finement l'ensemble des applications ;
- sélectionnant la stratégie de migration pour chaque application ;
- préparant la structure des comptes AWS dédiés à chaque application.

Il y a sept stratégies de migration appelées `7R` :

- `retire` : décommissionner l'application ;
- `retain` : attendre ;
- `rehost` : transformer les machines virtuelles en instances [EC2](https://aws.amazon.com/fr/ec2/) ;
- `repurchase` : remplacer l'application par une solution commerciale en SaaS ou dans le store AWS ;
- `replatform` : utiliser des services managées à la place des services existants : Kubernetes vers EKS ou PostGreSQL vers RDS.
- `relocate` : utiliser VMWare Cloud on AWS pour les instances ESX;
- `refactor` : modifier l'architecture et le code.

### Migrer et moderniser

Là on rentre dans le vif du sujet. Cela consiste à effectuer la migration des données ou des capacités de calcul en suivant la stratégie décidée. L'idée est aussi de faire progresser l'entité pour les futures migrations d'application.

Cette phase intègre la modernisation des architectures guidée par les indicateurs définis dans les phases précédentes et en particulier aux coûts. Les préconisations d'AWS sont d'utiliser l'infrastructure comme code et de veiller aux coûts.

## Ce que j'en ai retenu

### AWS a une offre complète

Il y a une offre de services impressionnantes et la migration n'est pas mis de côté. Les logiciels très utilisés dans les entreprises comme VMWare VSphere, Active Directory ou SAP sont représentés avec des différentes modalités.

Il y a des services sont dédiés à la migration pour les trois phases identifiées par AWS. Certains services d'extension de capacité (de stockage ou de calcul) des installations `on premise` permettent aussi une transition douce. Il y a même des solutions hors ligne mais nous en reparlerons dans le dernier paragraphe.

### La migration est la première étape

La stratégie de migration la plus utilisée est le `rehost`. Cela représente la moitié des migrations. Cette stratégie n'est pas la plus efficiente au niveau prix ou de passage à l'échelle.

C'est une première étape vers l'utilisation de services plus efficaces, l'idée est d'effectuer fréquemment des modifications mineures en se basant sur des objectifs de la migration.

### Avènement du Serverless

Le `serverless` signifie que la responsabilité de l'exploitation du serveur sur lequel est fourni le service est délégué à au fournisseur du service. Ces services représentent l'adaptation ultime au cloud. L'adoption de tels services implique une grande confiance dans le fournisseur de cloud.

En plus des traditionnelles capacités réseau ou de stockage, il y a des fonctions, des containers, des bases de données. Il est donc possible de créer des applications entièrement sans se préoccuper de serveur.

### La famille Snow

La surprise pour moi a été la famille Snow, la solution de migration de données hors ligne, cela permet de transporter de 8 TB jusqu'à 100 PB. La taille va d'une tour informatique au camion :truck: ! Le plus impressionnant est le Snowcone qui permet d'exporter 8 TB dans un appareil de 2,1kg avec les informations d'expédition sur écran à encre électronique. Cela illustre pour moi, le degré de maturité sur la migration de AWS - tout est prévu.

## Conclusion

La formation est hyper centrée sur AWS et cela peut apparaître parfois comme une publicité pour AWS mais il faut bien avouer qu'AWS a pléthore de services et beaucoup de clients. Notre formateur connaissait bien les services proposés. Je pense qu'il est possible de s'inspirer de ces leçons pour migrer vers le cloud Microsoft Azure, Google Cloud Platform ou même vers des acteurs plus petits (et français :fr:) comme [Scaleway](https://www.scaleway.com/fr/).

Si vous avez des questions ou des retours d'expériences sur des migrations vers le cloud, je suis très intéressé. N'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
