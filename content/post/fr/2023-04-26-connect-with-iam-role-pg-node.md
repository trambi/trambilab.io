---
title: "Se connecter avec un rôle IAM sur RDS PostgreSQL"
date: "2023-04-26"
draft: false
description: "une aventure de deux jours ou de cinq secondes ..."
tags: ["nodejs","cloud","lambda"]
author: "Bertrand Madet"
---

Depuis la semaine dernière, j'essaye d'utiliser une fonctionnalité d'AWS qui permet de se connecter une base de données gérée avec un rôle IAM. Nous allons voir que cela n'a pas était une balade de santé.

## Pourquoi vouloir se connecter avec un rôle IAM

Le premier avantage est de ne pas avoir à gérer de mot de passe pour chaque rôle PostgreSQL. Il faut se rendre compte que chaque mot de passe à gérer doit être géré pour chaque environnement. Une fois cette contrainte allégée, cela permet de multiplier les rôles pour gérer plus finement les droits sur les données.

De plus comme les rôles sont, par construction, identiques entre PostgreSQL et IAM, cela devrait rendre la vérification et l'évolution des droits plus simple.

## Principe

En fait, on passe par un intermédiaire que j'ai appelé `RDS Signer` (du nom l'API AWS). J'image que le processus doit ressembler à celui ci-dessus.

```mermaid
sequenceDiagram
  participant code as Code appelant avec le rôle RoleA
  participant rdssigner as RDS
  participant db as Base de données BD1
  code->>rdssigner: Bonjour, j'ai le rôle IAM et je le souhaite le code temporaire pour le rôle RoleA pour me connecter à la base de données BD1
  rdssigner->>code: Voilà le code temporaire, il est valable une fois et quinze minutes
  code->>db: Bonjour, je souhaite me connecter avec le rôle RoleA et voici mon code
  db-->rdssigner: Étape inconnue
  db->>code: Ok bienvenue
```

## Pré requis

Il y a trois pré requis qui sont très décrits dans la documentation AWS - [Authentification de base de données IAM pour MariaDB, MySQL et PostgreSQL (https://docs.aws.amazon.com/fr_fr/AmazonRDS/latest/UserGuide/UsingWithRDS.IAMDBAuth.html)](https://docs.aws.amazon.com/fr_fr/AmazonRDS/latest/UserGuide/UsingWithRDS.IAMDBAuth.html) :

1. Avoir activé l'authentification par IAM sur la base de données :white_check_mark: ;
1. Avoir créé le rôle et la politique IAM pour accéder à la base de données :white_check_mark: ;
1. Avoir créé le rôle dans PostgreSQL :white_check_mark:.

## En JavaScript

L'idée est d'obtenir le mot de passe temporaire avec l'instance `Signer` du paquet `@aws-sdk/rds-signer` puis de faire une connexion classique à PostgreSQL avec `pg`.

```javascript
import { Client } from 'pg';
import { Signer } from '@aws-sdk/rds-signer';

function async connectedClient({region,host,port,user,database}){
    const signer = new Signer({
        region,
        hostname: host,
        port,
        username: user
    });
    const token = await signer.getAuthToken();
    const client = new Client( {
        user,
        port,
        host,
        database
        ssl:true,
        password: token
    });
    await client.connect();
    return client;
}

```

Je mets le code ici car j'ai eu du mal à trouver un exemple de code et j'ai littéralement mis deux jours :cry: à le valider. En effet, j'utilisais une Lambda et à chaque fois que je faisais l'essai de connexion, ma lambda tombait en time-out. Je pensais que les trois secondes prises pour la connexion étaient dues à un problème de configuration - un problème de résolution DNS par exemple. Après avoir vérifié que :

- la connexion réseau de ma lambda était correcte en me connectant à un compte PostgreSQL avec mot de passe compte ;
- la configuration pour IAM de PostgreSQL était correcte en utilisant un mot de passe temporaire pour me connecter avec `psql` ;
- le certificat présenté par PostgreSQL était correct grâce à `openssl s_client -showcerts -starttls postgres -connect` ;
- la vérification du certificat était possible par Node.js avec la variable d'environnement `NODE_EXTRA_CA_CERTS`.

En désespoir de cause, j'ai tenté d'augmenter le temps imparti à la lambda à dix secondes et ça a marché :exploding_head: : dans ma configuration, l'étape de connexion `client.connect()` prenait cinq secondes !

Un peu étonné par ce délai - la connexion avec un mot de passe met 500 millisecondes - j'ai décidé de tester en Python.

## Python

Le cas du python est bien documenté chez : [Connexion à votre instance de base de données à l'aide de l'authentification IAM et de AWS SDK for Python (https://docs.aws.amazon.com/fr_fr/AmazonRDS/latest/UserGuide/UsingWithRDS.IAMDBAuth.Connecting.Python.html)](https://docs.aws.amazon.com/fr_fr/AmazonRDS/latest/UserGuide/UsingWithRDS.IAMDBAuth.Connecting.Python.html). Le principe est identique, on génère un mot de passe temporaire avec la méthode `generate_db_auth_token` du client RDS puis on se connecte avec son mot de passe à PostgreSQL avec psycopg2 et la fonction ... `connect`.

Le délai est lui aussi de plus de cinq secondes.

## Conclusion

Dans la configuration actuelle, le délai de cinq secondes est rédhibitoire pour mon usage. Mais je pense que la fonctionnalité est très utile pour une connexion plus longue. Cette aventure m'aura obligé à parcourir la documentation des différents modules avec lesquels j'ai dû interagir. J'ai même eu le plaisir de préparer une lambda avec psycopg2 mais c'est une autre histoire - peut-être la semaine prochaine.

Si vous avez des éléments complémentaires sur cette fonctionnalité comme par exemple, pourquoi il y a ce délai ou le détail de diagramme de processus, partagez les avec moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
