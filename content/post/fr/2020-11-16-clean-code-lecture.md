---
title: "Clean code - Fiche de lecture"
date: "2020-11-16"
description: "Le livre de Robert C. Martin essentiel pour les développeurs"
tags: ["fiche de lecture","Software Craftmanship","base","oncle Bob","code","livre en français"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre ?

Ce livre s'adresse à toutes les personnes qui programment et qui veulent programmer proprement.

## Ce qui faut retenir

- Le développement piloté par les tests #TDD c'est bien !
- L'écriture d'un code propre est aussi importante que l'écriture d'un code qui marche, le code propre étant un code qui marche sur long terme ;
- L'écriture d'un code propre demande des efforts et de la discipline mais le résultat vaut largement les efforts consentis;
- Ce livre est un des pierres angulaires du #software #craftmanship - après tout le sous-titre est a "handbook of Agile Software Craftmanship".

## Mon opinion

|Points positifs|Points négatifs|
|---|---|
|:+1: Contenu très pertinent|:-1: Tous les codes sont en Java|
|:+1: Bonne progression de l'apprentissage : la structure du livre est la suivante, une première partie décrit les principes de l'écriture d'un code propre, la deuxième partie est constituée d'études de réécriture de code et l'auteur nous livre des indicateurs sur la propreté du code dans la troisième partie|:-1: Les principes sont orientés programmation objet (il est possible de les appliquer à la programmation fonctionnelle mais cela demandera des adaptations)|
|:+1: Si vous jouez le jeu dans la partie études de cas, vous resterez imprégné des principes|:-1: Si vous ne jouez pas le jeu dans la partie études de cas, vous apprendrez moins|

Si vous voulez progresser dans l'art d'écrire un code lisible et maintenable, lisez ce livre.

Si vous ne comprenez pas l'intérêt d'écrire un code propre, lisez ce livre et relisez le !

## Détails

En anglais :gb: :

- Clean code de Robert C. "Uncle Bob" Martin
- ISBN-13 978-0132350884

En français :fr: :

- Coder proprement de Robert C. "Uncle Bob" Martin
- ISBN-13 978-2326002272

## Note

La fiche de lecture "Clean code" fut initialement publiée sur le blog interne de mon entreprise en juillet 2016 et amendée pour prendre en compte la version française
