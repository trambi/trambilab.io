---
title: "The Clean Coder - Fiche de lecture"
date: "2021-03-21"
draft: false
description: "Livre d'Oncle Bob sur le métier de développeur logiciel"
tags: ["fiche de lecture","Software Craftmanship","base","oncle Bob","livre en français"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Aux développeuses et aux développeurs qui souhaitent se professionnaliser (c'est à dire améliorer leurs pratiques de codage et leurs compétences connexes au codage).

## Ce qu'il faut retenir

Le métier de développeur est un métier jeune (à l'échelle des métiers soixante ans, c'est peu). Les praticiens de ce métier ne sont pas vus comme des professionnels. Parfois à juste titre car nous, les développeurs, manquons de discipline et parfois à cause de la nature très abstraite pour nos contemporains du développement logiciel.

Robert C. Martin, appelé aussi Oncle Bob, nous livre les compétences et les pratiques pour gagner en professionnalisme.

## Mon opinion

La façon dont Oncle Bob livre ses expériences fait vibrer les nôtres et leur donne un sens. C'est dans ce livre que j'ai lu pour la première fois bien des concepts comme TDD, coding dojo ...

Dans la [fiche de lecture]({{< ref "/post/fr/2020-11-11-craftman-software-lecture.md" >}}) de `The Software Craftsman: Professionalism, Pragmatism, Pride` de Sandro Mancuso, j'ai indiqué que je pensais que la lecture de `The Software Craftman` était plus prioritaire que `The Clean Coder`, je souhaite nuancer ce propos. J'avais lu ce livre, il y a des années à la suite de `Clean Code` (du même auteur [fiche de lecture]({{< ref "/post/fr/2020-11-16-clean-code-lecture.md" >}})) et `Clean Code` avait plus résonné en moi. J'ai relu ce livre pour rédiger cette fiche de lecture, et je pense que cela va dépendre de vos manières d'apprendre :

- si vous avez plus besoin de ressentir, lisez de préférence `The Clean Coder` ;
- si vous avez plus besoin de conseils pratiques et d'un guide, lisez de préférence `The Software Craftman`.

 Dans l'idéal, prenez le temps de lire les deux et dites moi ce que vous en pensez sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

## Détails

En anglais :gb: :

- The Clean Coder de Robert C. Martin
- ISBN-13 978-0137081073

En français :fr: :

- Proprement Codeur de Robert C. Martin
- ISBN-13 978-2326002890

*Edition de mars 2022, ajout de la version française (sortie en février 2022)*
