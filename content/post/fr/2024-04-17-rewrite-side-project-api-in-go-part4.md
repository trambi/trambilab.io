---
title: "Confronter une API REST à l'existant"
date: "2024-04-17"
draft: false
description: "des données ou une application cliente"
tags: ["REST API", "projet perso","Go"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série :

- [Présentation de mon projet perso du moment]({{< ref "/post/fr/2023-11-21-rewrite-side-project-api-in-go-part0" >}}) ;
- [Masquer les détails d'implémentation d'une API REST]({{< ref "/post/fr/2023-11-09-rewrite-side-project-api-in-go-part1" >}}) ;
- [Ajout de logique métier dans une API REST - classement des joueurs]({{< ref "/post/fr/2023-12-30-rewrite-side-project-api-in-go-part2" >}}) ;
- [Ajout de logique métier dans une API REST - calcul des points]({{< ref "/post/fr/2024-01-09-rewrite-side-project-api-in-go-part3" >}}).

L'API REST en question est la réécriture complète en Go du logiciel de tournoi dont je vous ai parlé dans la série : `Reprendre un projet après 3 ans` [premier billet]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years.md" >}}). Le principe de cette API REST est de gérer un tournoi, les participants et les matchs associés. Les aspects de manipulation des objets métier et les cas simples de classement sont gérés. Après avoir testé les points du tournoi avec la première édition, je me suis attaqué aux autres éditions. Cela m'a tellement occupé, que je n'ai pas pris le temps d'écrire un billet de blog la semaine dernière.

## Fin du calcul des points d'un match

### Simplification de la création de condition

Dans le système que j'ai décrit dans l'épisode précédent, la création d'une condition ou d'une valeur était pénible. Pour écrire un test avec les points du football, il fallait écrire.

```go
	edition, err := NewEdition("myEdition", "2023-05-13", 5, 3, true, false, []Faction{faction}, "something", "someone")
	if err != nil {
		t.Fatalf("error while constructing Edition")
	}
  gameDrawClause := Clause{
		ValueIfTrue: ConstantValue{Value: 1.0},
		Condition: EqualCondition{
			Left:  ValuableVariable{Field: "TdFor"},
			Right: ValuableVariable{Field: "TdAgainst"},
		},
	}
	gameWinClause := Clause{
		ValueIfTrue: ConstantValue{Value: 3.0},
		Condition: GreaterThanCondition{
			ToBeGreater: ValuableVariable{Field: "TdFor"},
			ToBeLesser:  ValuableVariable{Field: "TdAgainst"},
		},
	}
  edition.GamePoints = PointsSettings{
    Default: ConstantValue{Value: 0.0},
    Clauses: []Clause{gameDrawClause, gameWinClause},
  }
```

J'ai utilisé deux fonctions qui me permettaient de stocker le système de calcul des points en base de données sous la forme de chaîne de caractère :

- `NewCondition` qui permet de créer une structure qui implémente `ICondition` ;
- `NewValuable` qui permet de créer une structure qui implémente `IValuable`.

J'ai aussi ajouté une méthode pour faciliter la modification des points de chaque partie, `AddGamePointsClause`. Cela m'a permis de grandement simplifier l'écriture des tests.

```go
  edition, err := NewEdition("myEdition", "2023-05-13", 5, 3, true, false, []Faction{faction}, "something", "someone")
	if err != nil {
		t.Fatalf("error while constructing Edition")
	}
	edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
```

### Valeur de points variables

Nous avions tout cela qu'il fallait pour calculer des points fixes comme ceux utilisés au football :soccer:. Nous pouvions même représenter le système de bonus défensif et offensif. Mais il arrive que les points soient attribués en fonction du nombre d'essais marqués ou d'autres paramètres de la rencontre, par exemple : 

- Une victoire donne 1000 points plus le nombre d'essais marqués ;
- Un match nul donne 400 points plus le nombre d'essais marqués ;
- Une défaite  donne le nombre d'essais marqués.

Un match qui se solde par score de 1-0 donnera 1001 points au vainqueur et 0 point au perdant, alors qu'un match qui se termine sur le score de 4 à 3 donnera 1004 points au vainqueur et 3 points au perdant. Je n'avais pas prévu ce cas-là : les valeurs de points par défaut et les valeurs si une condition est vraie ont une valeur constante.

J'avais intégré la notion de valeur évaluable pour les conditions via l'interface `IValuable`. Il a donc suffi que les valeurs de points est un type qui implémente `IValuable` et que le calcul de points la méthode de cette interface (que j'ai renommée en `Evaluate`).

```go
func (game *Game) ComputeGamePoints() error {
	var err error
	points1 := game.Edition.GamePoints.Default
	reversedGame := game.Reverse()
	points2 := game.Edition.GamePoints.Default

	for _, clause := range game.Edition.GamePoints.Clauses {
		conditionForGame, err := clause.Condition.Value(*game)
		if err != nil {
			return err
		}
		conditionForReversedGame, err := clause.Condition.Value(reversedGame)
		if err != nil {
			return err
		}
		if conditionForGame {
			points1 = clause.ValueIfTrue
		}
		if conditionForReversedGame {
			points2 = clause.ValueIfTrue
		}
	}
  // Le changement est ici
	game.Points1, err = points1.Evaluate(game)
	if err != nil {
		return fmt.Errorf("unable to evaluate points1: %s", err.Error())
	}
  // et là
	game.Points2, err = points2.Evaluate(reversedGame)
	if err != nil {
		return fmt.Errorf("unable to evaluate points2: %s", err.Error())
	}
	return nil
}
```

Il a fallu modifier les adaptateurs des paquets `storage` et `api` pour qu'ils utilisent des chaînes de caractères plutôt que des nombres à virgule flottante.

### Implémentation des points de confrontation

Le logiciel gère aussi les tournois par équipe. Une équipe de coachs rencontre une autre équipe de coachs. Les points des équipes peuvent être calculés en additionnant les points de coachs constituant les équipes. Mais il arrive aussi que les points d'équipe suivent une logique propre. Par exemple, l'équipe gagnante d'un ensemble de parties gagne 3 points et l'équipe perdante 0 point et les équipes gagnent chacune 1 point, en cas d'égalité.

J'ai ajouté un champ `ConfrontationPoints` dans la structure `Edition` suivant le même modèle que le champ dédié au calcul de points de partie. J'ai créé une  structure `Confrontation` qui regroupe les parties d'une rencontre entre équipes et une méthode `ComputePoints` pour calculer les points d'une confrontation. Le développement a été simplifié par l'existence des classes pour le calcul des points de partie.

## Mise au point des endpoints historiques

Une fois les 19 éditions configurées dans cette nouvelle API REST, j'ai pu faire un test important : lancer [l'application cliente de l'API REST précédente (https://github.com/jmaaanu/FantasyFootballWebView)](https://github.com/jmaaanu/FantasyFootballWebView) et regarder son comportement. Il a fallu procéder à des ajustements pour que cela fonctionne. J'étais très content d'avoir utilisé des adaptateurs pour écrire le JSON résultant de l'appel des endpoints de mon API REST car il a fallu changer des choses.

J'ai créé des adaptateurs spécifiques à la version 1 de l'API, par exemple pour les ressources de type `Coach` : 

- `coachJsonAdapter` est l'adaptateur qui permet de décorréler le JSON de la structure métier ;
- `coachV1JsonWriter` est l'adaptateur qui écrit le JSON pour l'API v1 de la structure métier .

Cela permet de séparer la représentation des ressources dans l'API des objets métier. Et par exemple, d'injecter le nom de l'équipe du coach dans la réponse de l'API même si l'objet métier ne le prévoit pas.

Cette réutilisation des adaptateurs m'a permis de mettre à jour ma stratégie de nommage : 

- Pour les enregistrements en base de données (dans le paquet `storage`) il s'agit de `Record`; 
- Pour les champs en base de données (dans le paquet `storage`), il s'agit de `Field`;
- Pour l'écriture des représentations en JSON (dans le paquet `api`), il s'agit de `JsonWriter`;
- Pour la lecture des représentations en JSON (dans le paquet `api`), il s'agit de `JsonReader`;
- Pour la lecture et l'écriture des représentations en JSON (dans le paquet `api`), il s'agit de `JsonAdapter`.

## Conclusion

L'étape de l'API REST est terminée. J'ai l'impression que l'ajout de complexité au niveau du code permet de simplifier la configuration des calculs de points. Il y a aura peut-être des modifications à apporter lors de la prochaine étape.

Comme l'application cliente de la version 1 fonctionne, je vais pouvoir me consacrer à l'écriture de l'application d'administration. En effet, l'application d'administration précédente n'utilisait pas l'API REST mais directement les objets métier dans Symfony. 

Si vous souhaitez participer au développement, j'accueille les merge requests avec plaisir. Et si vous avez des projets perso, n'hésitez pas à les partager avec moi sur [X](https://x.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
