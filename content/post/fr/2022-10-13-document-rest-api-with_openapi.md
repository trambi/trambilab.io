---
title: "Documentation d'une API REST avec OpenAPI"
date: "2022-10-13"
draft: false
description: "et même un peu plus"
tags: ["outils","REST API","documentation"]
author: "Bertrand Madet"
---

La documentation en général et la documentation d'API REST n'est pas un sujet qui fait rêver. Il y a malgré tout suffisamment de matière pour faire un billet de blog. Je vous propose d'aborder l'utilité de la documentation d'une API REST et comment le faire.

## Pourquoi documenter une API REST ?

Une API REST constitue une interface de votre logiciel, cette interface est inutilisable sans la connaissance pour l'utiliser. Or une API REST n'a d'intérêt que si l'on peut l'utiliser.

Le fait d'avoir accès à une implémentation réelle ou factice (cf. [Création d'une API REST factice]({{< ref "/post/fr/2022-06-30-fake-rest-api.md" >}})) permet certes de l'explorer mais un peu comme on compterait les arbres d'une forêt les yeux fermés, c'est long, pénible et incertain. En effet, comment connaître :

- les endpoints disponibles;
- les paramètres de requête possibles ou obligatoires ;
- la signification des codes de retour ;
- le type des réponses possibles ;
- le besoin d'authentification ou d'autorisation sur un endpoint...

Avoir les sources ou les sources des tests permet d'avoir une réponse à ces questions mais cela signifie que vos utilisateurs sont en capacité de lire votre code et cela ajoute un couplage de compétence sur le langage d'implémentation entre le service et les utilisateurs. Et il n'est pas toujours possible de donner accès aux sources.

On peut aussi voir la documentation comme un contrat entre le responsable d'une API et ses utilisateurs (qui inclue le responsable de l'API dans le futur). Le fait de changer le contrat aura plus sûrement des conséquences que de "juste" changer l'implémentation.

## Comment documenter une API REST

Il existe beaucoup de manière de documenter une API, un format qui ressort souvent est [Swagger (https://swagger.io/specification/v2/)](https://swagger.io/specification/v2/) ou OpenAPI v3. OpenAPI est le nouveau nom de Swagger, elle consiste en une description au format [YAML - **YAML**  **A**in't **M**arkup **L**anguage (https://yaml.org/)](https://yaml.org/) ou format JSON.

Voici une description d'API REST avec un endpoint `/something` et une méthode sur cet endpoint.

```yaml
openapi: 3.0.3
info:
  title: Example REST API
  description: This an example of OpenAPI 3 based description
  version: 1.0.1
paths:
  /something:
    get:
      summary: Get something
      description: Get something in more detailled way
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Something'
        '400':
          description: Invalid status value
components:
  schemas:
    Something:
      type: object
      properties:
        id:
          type: integer
          format: int64
          example: 10
        key:
          type: string
          example: myKey
        value:
          type: string
          description: Order Status
          example: approved
```

Les endpoints et les actions associés sont dans le champ `path` et la définition des objets échangés sont dans `components/schemas`.

Il est possible d'ajouter des contraintes sur les paramètres, les champs des objets échangés, les critères d'authentification...

Avec un fichier JavaScript `swagger-js`, il est possible de visualiser cette description directement dans votre navigateur, et cela permet même de faire les requêtes, d'ajouter des paramètres d'authentification, d'essayer des formats d'objets échangés.

### Écrire le fichier

On peut juste documenter en écrivant un fichier au format OpenAPI dans la gestion de configuration à côté du code.

- :+1: Possible de le générer avant de coder pour
   - le transmettre à des partenaires,
   - générer le code du serveur,
   - générer le code du client,
   - configurer l'API Gateway ;
- :-1: Fichier à documenter en parallèle du code, il y a un risque de désynchroniser entre le code et la documentation.

### Générer le fichier à partir du code

Les frameworks de génération d'API REST ont souvent des fonctionnalités en interne ou des greffons d'exposition de l'API REST au format OpenAPI. Cela peut être sous la forme d'un fichier de configuration, dans la définition de l'application ou d'annotation au dessus des gestionnaires d'actions. l'API Gateway propose d'exporter la configuration au format OpenAPI.

- :+1: Synchronicité entre le code et la documentation ;
- :-1: Documentation *a posteriori* sans l'aspect contrat.

## Bonus : GitLab

GitLab prend en charge les fichiers au format Swagger et OpenAPI v3. Vous pouvez voir le fichier de la même manière qu'avec "swagger-js", directement, en explorant votre base de code.

## Conclusion

J'espère vous avoir convaincu de documenter vos API REST. J’ai un faible pour écrire la documentation à côté du code. Je trouve que cela renforce le côté contrat d’interface en évitant de modifier involontairement le contrat. L’aspect documentaire rejoint alors l’aspect conception et on pourrait même imaginer de tester automatiquement l’implémentation du serveur à partir du contrat ou la génération d’API factices à partir du contrat.

Il y a d'autres format pour documenter les API REST comme [RAML (https://raml.org)](https://raml.org) ou [API Blueprint (https://apiblueprint.org/)](https://apiblueprint.org/). Si vous avez des questions ou des retours d'expériences sur l'utilisation avec ces formats, je suis intéressé. N'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
