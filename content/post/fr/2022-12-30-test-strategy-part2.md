---
title: "Stratégie de test - partie 2"
date: "2022-12-30"
draft: false
description: "Et si nous l'écrivions"
tags: ["test","Software Craftmanship"]
author: "Bertrand Madet"
---

Dans [l'article précédent]({{< ref "/post/fr/2022-12-22-test-strategy-part1.md">}}), j'ai décrit les répartitions des types de test et les critères à prendre en compte pour établir une stratégie de test adaptée à votre situation. Je vous propose donc d'en rédiger une ! Pourquoi ? Parce que cela permet de régler et de fixer les détails et comme on dit "Les paroles passent et les écrits restent".

## Plan

Je propose le plan suivant :

- Le but ;
- La répartition des tests ;
- L'intégration dans l'usine logicielle.

L'idée de commencer par le but car les modalités découlent de ceux que l'on souhaite.

## Le but de la stratégie de tests

A quoi servent les tests sur le projet ? Cela dépend de l'équipe, de la maturité du logiciel. On pourrait avoir par exemple :

- Pour un logiciel en maintien en conditions opérationnelle, éviter les régressions ;
- Pour un logiciel de temps libre (side project :gb:), garder trace des implémentations ;
- Pour un logiciel en développement avec une équipe importante, détecter les défauts au plus tôt ;
- Pour un logiciel critique, s'assurer que les exigences sont validées.

Pour mon projet de temps libre, je proposerai le paragraphe d'introduction suivant :

> Comme mon projet est un projet que je mène sur mon temps libre, il est donc important de garder trace de l'implémentation des fonctionnalités. Les tests permettent de garder cette trace dans le code et vérifier de manière continue.

## Répartition des tests

Quel type de tests sera utilisé pour quel usage ? Le choix dépendra du but des tests,de l'équipe et l'environnement. On va entrer plus dans les détails des langages et de l'architecture du logiciel. On pourrait utiliser par exemple :

- Pour éviter les régressions, un plan de tests de bout en bout pour les fonctionnalités et des tests unitaires pour les défauts ;
- Pour garder trace des implémentations, majoritairement des tests unitaires et des tests de bout en bout pour la validation de la ligne de commande  ;
- Pour détecter les défauts au plus tôt, majoritairement des tests unitaires, des tests d'intégration pour les modules et quelques tests de bout en bout pour les fonctionnalités importantes ;
- Pour s'assurer que les exigences sont validées, cela dépendra des exigences.

Pour mon projet de temps libre en Golang, je proposerai le paragraphe suivant :

> Les modules Golang seront testés unitairement de manière extensive grâce aux outils standard de Golang. En complément, les commandes de la ligne de commande seront testés pour couvrir les chemins heureux des différentes options.

## L'intégration de l'usine logicielle

Comment orchestre-t-on les tests dans l'intégration continue ? Cela dépend du but des tests, de l'usine logicielle. Une option pourrait être par exemple :

- Pour éviter les régressions, les tests unitaires seraient lancés à chaque commit et le plan de tests avant chaque version ;
- Pour garder trace des implémentation et avec GitLab, les tests sont lancés à chaque commit dans l'intégration continue ;
- Pour détecter les défauts au plus tôt, les tests unitaires et d'intégration seraient lancés à chaque commit avant le déploiement, les tests de bout en bout seraient lancés après le déploiement ;
- Pour s'assurer que les exigences sont validées, les tests seraient lancés après chaque commit.

Pour mon projet de temps libre sur GitLab, je proposerai le paragraphe suivant :

> Les tests seront lancés à chaque commit. En cas d'échec des tests la priorité est de corriger cela.

## Conclusion

J'espère que ce billet vous aura donner l'envie de rédiger votre stratégie de test. Pour un modeste logiciel de temps libre, trois courts paragraphes permettent d'établir une première version de stratégie de test. La complexité du document augmentera avec la complexité du logiciel et la taille de son équipe.

Cela donne une base de départ pour établir une stratégie. Comme toute stratégie, il faudra veiller à l'ajuster en fonction de la situation.

N'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
