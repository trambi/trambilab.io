---
title: "Tester un module Terraform"
date: "2024-03-29"
draft: false
description: "avec Python, pytest et tftest"
tags: ["python","test","terraform"]
author: "Bertrand Madet"
---

Cette semaine, nous allons étudier un sujet à l'intersection de deux de mes domaines d'intérêt du moment : Terraform et les tests. Plus précisément comment tester un module Terraform ? Pour illustrer la démarche, on va partir du principe que le module Terraform décrit une infrastructure qui écoute le protocole HTTP à une url précise et répond toujours 'Ok'. On va commencer par vérifier que le module n'a pas de problème de syntaxe `HCL` puis qu'il s'applique bien et enfin qu'il propose bien ce qu'il promet.

## Vérifier la syntaxe

On va utiliser la bibliothèque de test `pytest` et [tftest (https://github.com/GoogleCloudPlatform/terraform-python-testing-helper)](https://github.com/GoogleCloudPlatform/terraform-python-testing-helper).

> Pour vérifier la syntaxe, on pourrait utiliser un outil d'analyse statique comme [Checkov (https://checkov.io)](https://checkov.io). Ce genre d'outil permet de vérifier beaucoup d'autre choses comme l'application de bonnes pratiques de sécurité.

Dans un répertoire `tests`, on va créer :

- un fichier `requirements.txt` qui va contenir les modules python nécessaires aux tests ;
- un fichier `test_module.py` qui va contenir le test ;

```python
import pytest
import tftest

def test_that_module_is_well_formed(monkeypatch):
  ## Given
  # Liste des valeurs de variables sous la forme
  # monkeypatch.setenv("TF_VAR_variable_name","variable_value")
  tf_test = tftest.TerraformTest(tfdir="./",binary="terraform")
  # Correspond à terraform init
  tf_test.setup()
  ## When/Then
  # Correspond à terraform plan
  tf_test.plan(output=True)
```

`pytest tests` doit afficher un test passé avec succès si le module à tester est correct.

J'étais parti sur un test incluant d'un fichier Terraform appelant le module terraform à tester. Cela permettait de tester en même temps le fichier d'exemple du module :+1: mais cela obligeait à écrire plus de code :-1:. Cela posait aussi des problèmes pour la suite ...

Je suis passé par les variables d'environnement `TF_VAR_` car cela permet d'avoir des tests autoporteur sans avoir utilisé des fichiers de définition de variables Terraform.

## Vérifier l'applicabilité du module

Pour l'instant on a testé la description d'infrastructure au moment de la planification de Terraform. Nous n'avons pas eu besoin d'avoir accès à un environnement de test. Il est possible qu'une description soit valide à la planification (`terraform plan`) mais invalide à l'application (`terraform apply`).

:warning: Tester l'applicabilité du module signifie concrètement qu'on va le faire installer par Terraform. Il va nous falloir un environnement où déployer et faire attention aux ressources que l'on provisionne.

Pytest gérera le nettoyage. Il faut passer par une fixture. :book: Une fixture est un élément pour fixer les conditions d'un test. Dans pytest, il s'agit de fonction décorée avec `pytest.fixture`.

On va utiliser la fixture intégré `request` pour ajouter une fonction de nettoyage.

```python

## Debut du fichier identique à la partie précédente

def test_module_can_apply(request):
  ## Given
  # Liste des valeurs de variables sous la forme
  # monkeypatch.setenv("TF_VAR_variable_name","variable_value")
  tf_test = tftest.TerraformTest(tfdir="./",binary="terraform")
  # Correspond à terraform init
  tf_test.setup()
  def destroy_infrastructure():
    print('Destroy infrastructure)
    # Correspond à terraform destroy  
    tf_test.destroy(**{"auto_approve":True})
  # Ajoute la fonction comme fonction de nettoyage
  request.addfinalizer(destroy_infrastructure)
  ## When
  # Correspond à terraform apply
  tf_test.apply(output=True)
  ## Then
  output = tf_test.output()
  assert 'ma_cle' in output
```

J'ai ajouté la fonction de nettoyage avant l'appel à `apply` pour permettre de lancer le nettoyage même si l'application échoue. 

C'est la partie de vérification des sorties qui m'a incité à tester uniquement le module et non le module et un fichier d'exemple. En effet, si on passe par un fichier d'exemple, il faut que les sorties du fichier fassent les liens avec les sorties du module.

```hcl


# Suite du fichier Terraform qui utilise le module

output output1 {
  value = module.output1
}

#...

output outputN {
  value = module.outputN
}
```

Vous avouerez que c'est plutôt lourd et sujet à erreur.

## Conclusion

Nous avons pu tester la partie pure Terraform et la partie dépendante du fournisseur de ressources. Il manque le test fonctionnel.

J'ai appris plein de choses sur les fixtures et les fixtures intégrés de pytest en explorant dans la documentation très fournie de [pytest (https://docs.pytest.org)](https://docs.pytest.org).

Si vous souhaitez partager sur Terraform ou les tests ou les deux, contactez moi [sur X](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169
