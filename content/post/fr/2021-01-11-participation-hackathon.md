---
title: "J'ai participé à un hackathon vendredi"
date: "2021-01-11"
draft: false
description: "Et je partage l'expérience"
tags: ["experience","hackathon"]
author: "Bertrand Madet"
---

Vendredi dernier, j'ai participé à un hackathon interne sur le développement de composants  React utilisant les données de notre entreprise.

## Modalités

Afin de caler l'organisation et le déroulé de la journée, une journée de test avait été déroulé avant les fêtes. Et j'avais eu la chance d'y participer.

Vingt cinq personnes réparties en cinq équipes. Chaque équipe avait un porteur de sujet, un spécialiste de l'UX et deux ou trois développeurs. Nous avions jusqu'à 16h30 pour proposer quelque chose : une maquette d'application, un prototype de composant ou autre.

En support de l'organisation, j'avais préparé une machine virtuelle de développement par équipe.

## Déroulement

Nous avons analysé durant une matinée le sujet présenté à la fois d'un point de vue utilisateur et des données. Nous sommes partis sur un composant exploitant une mappemonde avec Leaflet.js en programmant sur mon poste qui avait l'avantage d'être configuré (je viens de finir mon composant avec les routes orthodromiques).

La récupération des données nous a pris une bonne heure et demi. C'est plutôt long pour la récupération de données à partir d'une API REST en JavaScript. Les données nécessitaient de faire une jointure à la main.

Heureusement que Leaflet.js et ses composants [REACT (https://react-leaflet.js.org/)](https://react-leaflet.js.org/) sont simples à utiliser car nous avons réussi à finir notre prototype à temps.

## Ressenti

C'était très rafraîchissant de travailler avec des collègues avec qui je n'ai pas l'habitude de passer du temps.

La limite de temps oblige à faire des choix drastiques, c'est frustrant et à la fois très simulant. Avec une équipe de cinq, les idées foisonnent et la limite de temps permet de se concentrer.

La machine virtuelle n'a semble-t-il pas simplifié la vie des développeurs. J'ai sans doute était trop influencé par mon expérience de développement - `Visual Code` avec le plugin `remote ssh` et très souvent en pair programming.

## Conclusion

Je pense que la journée a été globalement une réussite, et c'était bien (voir essentiel) de dérouler une journée de test.

Un grand merci à l'équipe d'organisateurs, qui a concocté une journée cadencée et enthousiasmante. J'espère que nous pourrons renouveler l'expérience avec un public plus large.

Si vous souhaitez partager avec vos expériences d'hackathon, contactez [moi sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
