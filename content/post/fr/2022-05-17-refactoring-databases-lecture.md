---
title: "Refactoring databases - Fiche de lecture"
date: "2022-05-17"
draft: false
description: "Le pendant pour les bases de données de Refactoring de Martin Fowler"
tags: ["fiche de lecture","refactoring"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Si vous vous devez maintenir dans le temps une base de données, ce livre devrait vous intéresser.

## Ce qu'il faut retenir

- Il est possible de faire évoluer au fil du temps une base de données de la même manière que du code, par une succession de changement atomique ;
- Cette possibilité s'appuie sur l'usage de tests pour valider chacun des changements ;
- Il faut ajouter un indicateur de version dans vos bases de données.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Décrit et analyse plusieurs stratégies de manière pragmatique | :-1: Un peu daté - la date de publication de 2007 se ressent un peu|
| :+1: Plus de cinquante refactorings détaillés| :-1: Consacré aux bases de données relationnelles|
| :+1: Plein de bonnes idées même pour des bases de données NoSQL||

La modification des bases de données est souvent plus périlleuse que la modification de code car il y a des données. Ce livre fournit une méthodologie détaillée pour les bases de données relationnelles pour adapter la structure au fil de l'eau.

Le cas des bases de données utilisées par plusieurs applications plus complexe (et heureusement plus rare avec l'avènement des API) est pris en compte . Comme pour le refactoring de code, l'approche utilise des itérations courtes, des tests automatiques pour valider rapidement, de la gestion de configuration.

Je vous laisse, il faut que j'aille insérer un identifiant de version dans chacune de mes bases de données :wink:.

## Détails

En anglais :gb: :

- Refactoring Databases: Evolutionary Database Design de Scott Ambler et Pramod Sadalage
- ISBN-13 : 978-0321774514

En français :fr: : non disponible :cry:
