---
title: "Rendre plus robuste le traitement d'un évènement S3"
date: "2024-03-21"
draft: false
description: "en utilisant une queue SQS"
tags: ["python","cloud","lambda"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}).

-----

Cette semaine, je vous propose de partager sur l'amélioration de la robustesse du traitement d'évènement S3 avec une lambda Python. Dans mon projet, nous avons besoin de faire des traitements à l'arrivée d'un objet sur un bucket S3. Jusqu'à récemment, les notifications d'évènement S3 déclenchaient directement des lambdas.

## Déclenchement d'une lambda avec une notification S3

```mermaid
graph LR;
    user((utilisateur))-->|dépose un objet|s3(Bucket S3)
    s3-->|émet|s3Notif(Notification S3)
    s3Notif-->|déclenche|lambda[Lambda]
```

C'est assez direct, la description Terraform nécessite d'ajouter une ressource `aws_s3_bucket_notification` et de gérer les permissions à l'aide d'un rôle IAM et d'une ressource `aws_lambda_permission`.

> :warning: Si vous utilisez une lambda dans un VPC et que vous voulez accéder au S3, il faudra aussi avoir un point de terminaison VPC de type S3 attaché au VPC. Mais c'est une autre histoire.

Le problème est que si la lambda échoue pour une raison quelconque (mauvaise configuration, durée de vie trop longue, problème de réseau), la notification S3 est perdue :cry:.

## Déclenchement d'une lambda via SQS

Pour répondre à ce problème de robustesse, on peut ajouter une file d'attente SQS pour stocker les notifications S3. Pour éviter les boucles d'erreur sans fin, on ajoute une deuxième file d'attente SQS d'impasse pour stocker les messages en erreur de manière répétée.

```mermaid
graph LR;
    user((utilisateur))-->|dépose un objet|s3(Bucket S3)
    s3-->|émet|s3Notif(Notification S3)
    s3Notif-->|s'ajoute dans|sqs1(Queue SQS)
    sqs1-->|déclenche|lambda[Lambda]
    lambda-->|retourne les messages non pris en compte|sqs1
    sqs1-->|en cas d'erreurs répétés|sqs2(Queue SQS d'impasse)
```

Pour la description de l'infrastructure, il est nécessaire d'ajouter deux ressources `aws_sqs_queue` et une politique IAM pour autoriser le bucket S3 à ajouter un message dans la file d'attente SQS.

## En terme de code python

### Traitement de notification S3

La fonction de traitement de notification S3 va recevoir un évènement Notification S3 décrit dans la documentation [https://docs.aws.amazon.com/AmazonS3/latest/userguide/notification-content-structure.html](https://docs.aws.amazon.com/AmazonS3/latest/userguide/notification-content-structure.html). Il s'agit d'un objet contenant un champ `Records` qui contient un tableau d'objets, dont le champ le plus intéressant (dans mon cas) est `s3` qui est lui-même un objet ... Bref en enlevant les champs qui n'avaient pas d'intérêt dans mon cas.

```json
{  
   "Records":[  
      {  
         "s3":{  
            "bucket":{  
               "name":"mybucket",
            },
            "object":{  
               "key":"mykey.jpg",
            }
         }
      }
   ]
}
```

Pour gérer ces évènements, j'ai opté pour le découpage ci-dessous.

```python
def s3_event_handler(event, context):
  config = config_from_environment()
  s3_event_handle_with_config(event,context,config)

def s3_event_handle_with_config(event, context, config):
  data = data_from_s3_event(event,context)
  business_logic(data,config)
```

### Traitement de message SQS

La fonction de traitement de message SQS va recevoir un évènement message SQS tel que décrit dans la documentation [https://docs.aws.amazon.com/lambda/latest/dg/with-sqs.html#example-standard-queue-message-event](https://docs.aws.amazon.com/lambda/latest/dg/with-sqs.html#example-standard-queue-message-event). Il s'agit d'un objet contenant un champ `Records` qui contient un tableau d'objets, dont les champs les plus intéressants sont `messageId` et `body`. Dans mon cas, le champ `body` contient un évènement Notification S3. Le champ `messageId` contient l'identifiant unique du message. La file d'attente SQS déclenche la lambda peut grouper les évènements par lot.

En termes d'implémentation, la différence est que la valeur de retour indique à la file d'attente SQS les messages non traités. J'ai adapté la structure en réutilisant au maximum les fonctions existantes.

```python
def sqs_event_handler(event, context):
  batch_item_failures = []
  sqs_batch_response = {}
  if event:
    messageIds = extract_message_id(event)
    try:
      config = config_from_environment()
    except e:
      sqs_batch_response["batchItemFailures"] = messageIds
      return sqs_batch_response
    for record in event["Records"]:
      try:
        s3NotificationEvent = json.dumps(record["body"])
        s3_event_handle_with_config(s3NotificationEvent,context,config)
      except e:
        batch_item_failures.append({"itemIdentifier": record['messageId']})
    return sqs_batch_response


def s3_event_handle_with_config(event, context, config):
  data = data_from_s3_event(event,context)
  business_logic(data,config)
```

## Conclusion

On voit que la robustesse a un coût en terme de complexité. Entre le temps, on m'a fait découvrir les outils [Powertools (https://docs.powertools.aws.dev/lambda/python/latest/)](https://docs.powertools.aws.dev/lambda/python/latest/), qui permettent de suivre des bonnes pratiques sur les lambdas en Python. Cela est aussi valable pour Java, TypeScript et .Net. J'ai pu me rendre compte qu'il me reste des choses à apprendre et à appliquer sur mes lambdas.

Si vous avez des conseils, des expériences sur les AWS Lambdas, partagez les avec moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

-----

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Lambda basée une image de conteneur en Terraform]({{< ref "/post/fr/2023-02-10-aws-north-face-part4.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}).
