---
title: "Utiliser les contextes en React"
date: "2022-12-08"
draft: false
description: "une autre manière de gérer les états"
tags: ["front-end","reactjs"]
author: "Bertrand Madet"
---

Nous avons vu dans un billet précédent ([Gestion des états en React]({{< ref "/post/fr/2022-07-27-state-management-in-react-part1">}})) qu'il existait différentes stratégies  de gestion d'états des composants en React.js. Pour être synthétique les stratégies présentées étaient simplistes. Il existe des outils qui permettent d'affiner la stratégie. Détaillons l'outil "Contexte".

## Stratégie de gestion des états avec un contexte

Si l'état de l'application influence de manière variable les composants de l'application, il peut être intéressant d'utiliser des contextes qui vont permettre de partager un état global entre plusieurs composants sans surcharger les propriétés de tous les composants intermédiaires. 

```mermaid
graph TD;
    context([Contexte])-.->root(Composant racine avec état)
    root-->component1[Composant 1]
    root-->component2[Composant 2]
    component1-->component3[Composant 3]
    component1-->component4[Composant 4]
    component1-->component5[Composant 5]
    component2-->component6[Composant 6]
    component2-->component7[Composant 7 utilisant le contexte]
    context-.->component7
    component2-->component8[Composant 8]
    component5-->component9[Composant 9]
    component5-->component10[Composant 10 mais utilisant le contexte]
    context-.->component10
    component7-->component11[Composant 11]
```

Dans la figure donnée en exemple, seul les composants 7 et 10 ont besoin du contexte. Sans utiliser les contextes, on doit transmettre les propriétés nécessaire aux composants 7 et 10 à chaque composant entre eux et le composant racine : les composants 2, 5 et 1 :exploding_head:.

React propose des outils pour gérer les contextes et avant de faire un bilan des avantages et inconvénients, il convient de regarder les implémentations.

### Implémentation du contexte

Comment créer un contexte ? Avec la fonction `createContext` :

```typescript
// Fichier src/services/my-context/index.ts
import { createContext } from 'react';

export interface IMyContext {
  key1: string;
  key2: number;
}

const defaultContext = {
  key1: 'value1',
  key2: 2,
};

export const MyContext = createContext<IMyContext>(defaultContext);

```

Il faut également créer un ou plusieurs fournisseurs de contexte, avec la méthode `provider` du contexte. Pour garder les états, on va utiliser le hook `useState` comme dans un composant stateful :

```typescript
import { useState } from 'react';
import { MyContext } from '../my-context/';

export interface MyContextProviderProps {
  key1: string;
  key2: number;
  children: React.ReactNode;
}

export const MyContextProvider = (props: MyContextProviderProps) => {
  const [context] = useState({ key1: props.key1, key2: props.key2 });
  return (
    <MyContext.Provider value={context}>{props.children}</MyContext.Provider>
  );
};

```

### Implémentation du composant

Pour créer un composant utilisant la propriété `key1` de mon contexte, on utilise le hook `useContext` :

```typescript
// Fichier src/components/key1-my-context/index.tsx
import { useContext } from 'react';
import { IMyContext, MyContext } from '../../services/my-context';

export const Key1MyContext = () => {
  const { key1 } = useContext<IMyContext>(MyContext);
  return <span>key1 in my context: {JSON.stringify(key1)}</span>;
};

```

Et les tests associés :

```typescript
// Fichier src/components/key1-my-context/key1-my-context.test.tsx
import { render, screen } from '@testing-library/react';
import { MyContext } from '../../services/my-context';
import { Key1MyContext } from './';

describe('Key1MyContext', () => {
  it('should display key1 of my context', async () => {
    // GIVEN
    const value = 'aValue';
    //  WHEN
    render(
      <MyContext.Provider value={{ key1: value, key2: 0 }}>
        <Key1MyContext />
      </MyContext.Provider>
    );
    // THEN
    expect(
      await screen.findByText(value, {
        exact: false,
      })
    ).toBeInTheDocument();
  });
});

```

Les tests nécessitent de savoir des détails d'implémentation du composant à savoir qu'il utilise les contextes React (via `useContext`).

### Bilan

On l'a vu, les composants sont un peu plus complexes à coder et à tester. S'il y plusieurs composants qui utilisent le contexte, il y aura des doublons dans le code et les tests associés.

|Avantages|Inconvénients|
|---------|-------------|
|:+1: Pas de transmission de propriétés à travers tous les composants|:-1: Un doublon de composant implique un doublon dans la gestion du contexte|

## Variante avec décorateur

On peut essayer de revenir à des composants sans état en utilisant un décorateur pour gérer le contexte (pour les composants qui en ont besoin).

```mermaid
graph TD;
    context([Contexte])-.->root(Composant racine avec état)
    context-.->decorator([Décorateur qui transforme le contexte en propriétés])
    decorator-->decoratedComponent7[Composant 7t décoré]
    decorator-->decoratedComponent10[Composant 10 décoré]
    root-->component1[Composant 1]
    root-->component2[Composant 2]
    component1-->component3[Composant 3]
    component1-->component4[Composant 4]
    component1-->component5[Composant 5]
    component2-->component6[Composant 6]
    component2-->decoratedComponent7
    decoratedComponent7-->component7[Composant 7]
    component2-->component8[Composant 8]
    component5-->component9[Composant 9]
    component5-->decoratedComponent10
    decoratedComponent10-->component10[Composant 10]
    component7-->component11[Composant 11]
```

### Implémentation du décorateur

Il faut continuer à créer un contexte et un (ou plusieurs) fournisseur de contexte. Il faut créer un décorateur qui transforme le contexte en propriété.

```typescript
// Fichier src/services/with-my-context/index.tsx
import React, { useContext, ComponentType } from 'react';
import { IMyContext, MyContext } from '../my-context/';

export function withMyContext<T>(DecoratedComponent: ComponentType<T>) {
  return (hocProps: Omit<T, 'key1' | 'key2'>) => {
    const { key1, key2 } = useContext<IMyContext>(MyContext);
    return <DecoratedComponent {...hocProps as T} key1={key1} key2={key2} />;
  };
}

```

```typescript
// Fichier src/services/with-my-context/with-my-context.test.tsx
import { render, screen } from '@testing-library/react';
import { MyContext } from '../my-context/';
import { withMyContext } from './';

describe('WithMyContext', () => {
  it('should transform my context in properties', async () => {
    // GIVEN
    const myContext = {
      key1: 'aString',
      key2: 2,
    };
    const expectedKey1 = `key1: ${myContext.key1}`;
    const expectedKey2 = `key2: ${myContext.key2}`;
    const TestComponent = (props) => {
      return (
        <ul>
          <li>key1: {props.key1}</li>
          <li>key2: {props.key2}</li>
        </ul>
      );
    };
    const TestComponentWithMyContext = withMyContext(TestComponent);
    //  WHEN
    render(
      <MyContext.Provider value={myContext}>
        <TestComponentWithMyContext />
      </MyContext.Provider>
    );
    // THEN
    expect(await screen.findByText(expectedKey1)).toBeInTheDocument();
    expect(screen.getByText(expectedKey2)).toBeInTheDocument();
  });
});

```

### Implémentation d'un composant sans le contexte

Pour créer un composant on utilise juste la propriété `key1`  :

```typescript
// Fichier src/components/key1/index.tsx

interface Key1Props {
  key1: string;
}

export const Key1 = (props: Key1Props) => {
  return <span>key1 in my context: {JSON.stringify(props.key1)}</span>;
};

```

Et le test associé

```typescript
// Fichier src/components/key1/key1.test.tsx
import { render,screen } from '@testing-library/react';
import { Key1 } from './';

describe('Key1', ()=> {
  it('should display key1',async ()=>{
    // GIVEN
    const value = 'aValue';
    const expected = `key1 in my context: "${value}"`;
    //  WHEN
    render(<Key1 key1={value} />);
    // THEN
    expect(await screen.findByText(expected)).toBeInTheDocument();
  });
});

```

On le voit le composant et le test deviennent très simples. L'application devra utiliser le composant décoré avec `const Key1WithMyContext = withMyContext(Key1);`.

### Bilan avec décorateur

L'approche est utile si plusieurs composants utilisent le contexte. La complexité induite par le décorateur est alors compensée par la simplification des composants.

|Avantages|Inconvénients|
|---------|-------------|
|:+1: L'arbitrage est facile à se souvenir|:-1: Code du décorateur un peu compliqué|
|:+1: Les composants restent simples à implémenter et à tester||

## Conclusion

J'espère que cet article aura permis de comprendre l'apport des contextes dans la gestion des états dans une application. Les contextes sont utilisés par des bibliothèques comme `react-router`. L'approche a été décliné avec une stratégie "centralisée" mais on peut décliner cette approche à une stratégie "au cas par cas". Il faudra voir comment gérer la situation où les composants peuvent modifier le contexte.

La partie TypeScript a été grandement simplifié par la lecture de l'article [React Higher Order Components with TypeScript  (https://isamatov.com/react-hoc-typescript/)](https://isamatov.com/react-hoc-typescript/).

Si vous avez des questions ou des retours d'expériences sur React, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
