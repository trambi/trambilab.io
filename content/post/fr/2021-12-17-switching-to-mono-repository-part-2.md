---
title: "Passage d'une dizaine de dépôts à un - partie 2"
date: "2021-12-17"
draft: false
description: "tâtonnements pour améliorer les outils"
tags: ["outils","workflow","monorepository"]
author: "Bertrand Madet"
---

En août, je vous avais parlé de notre [passage d'une dizaine de dépôts à un]({{< ref "/post/fr/2021-08-19-switching-to-mono-repository-part-1.md" >}}). Je vous propose de faire un autre point d'étape sur ce passage.

## Un peu de contexte

Petit rappel : Le service sur lequel je travaille est structuré avec une dizaine de répertoires contenant les différents composants. Il y a des services Web en React, un catalogue de composants React et des applications pour gérer les back office. La bibliothèque de composants permet de partager les composants utilisés dans nos différentes applications Web React.

```mermaid
graph BT
  webapp1[Application web React 1]-->|Utilise des composants|catalog[Catalogue de composants]
  webapp2[Application web React 2]-->|Utilise des composants|catalog
```

Nous utilisons npm (version 6) pour gérer les paquets. Afin de limiter les changements dans nos habitudes, nous avions décidé de rester sur les mêmes outils dans un premier temps et de ne pas en ajouter d'autres.

## Axes d'amélioration

### Complexité

Aujourd'hui grâce au mono-dépôt nous pouvons implémenter dans un seul commit, un changement atomique de fonctionnalité. Ce changement peut amener à modifier le back office, un composant du catalogue et une application Web. Les petites différences, qui passaient inaperçues quand on modifiait les projets à une heure d'écart, sautent aux yeux.

Par exemple, les applications utilisent React scripts qui lancent les tests par défaut sur les fichiers ayant été modifiés depuis le dernier commit alors que les tests du catalogue de composants se lancent sur tous les composants.

Ce phénomène cumulé à la gestion de la dépendance au catalogue rend un peu plus complexe le développement.

### Rapidité

Le changement de contexte comme un changement de branche ou un "git rebase" nécessite de lancer un `npm install` sur chaque répertoire qui est susceptible d'avoir changé. Cela implique une rigueur et parfois une lenteur pour télécharger tous les nouveaux paquets npm. Ce qui est dommage quand on sait qu'on télécharge le paquet `react` par exemple pour les applications Web et le catalogue.

## Pistes

Une étape préalable est de réaliser un effort dans les différents `package.json` afin d'homogénéiser les scripts décrits. Mais cela n'améliorera pas l'articulation des scripts des différents composants.

### Script bash

Il est possible d'utiliser un script bash pour articuler les différents scripts. C'est une solution classique mais cela implique de développer dans des domaines qui sont bien gérés par des outils existants.

### Make

Make est un outil classique de construction dont l'une d'implémentation les plus connus est [GNU Make (https://www.gnu.org/software/make/)](https://www.gnu.org/software/make/) :

- :+1: Outil répandu et documentation abondante ;
- :+1: Facile à installer sur GNU/Linux ou WSL2 de Windows ;
- :-1: Compliqué à installer avec GitBash sur Windows ;
- :-1: C'est un outil de construction, des commandes qui sont lancées de manière répétées sans artefact de sortie comme les tests peuvent être techniques à mettre en place.

### Just

[Just (https://github.com/casey/just)](https://github.com/casey/just) est un utilitaire récent et écrit en Rust de lancement de commandes :

- :+1: Facile à installer sous Windows (sans droit administrateur) et GNU/Linux ;
- :+1: la logique du fichier `justfile` est intuitive ;
- :+1: on peut lancer la commande `just` depuis n’importe quel endroit du mono-dépôt ;
- :-1: point négatif de taille l’antivirus Symantec considère l’exécutable comme peu fiable (car très nouveau et très peu utilisé) et demande de le valider à chaque exécution.

### npm v8 et les workspaces

La version actuelle de [npm](https://docs.npmjs.com/about-npm) (la version 8 à l'heure où j'écris ces lignes) permet d'utiliser des `workspaces` celui permet :

1. D'utiliser de communaliser les modules node.js;
1. D'utiliser des commandes sur les répertoires en précisant le workspace  - `npm run test -w catalog` permet de lancer les tests dans le répertoire catalogue.

Le fait de communaliser les modules permet de ne les télécharger qu'une fois et donc de gagner du temps lors des changements de contexte.

Il y a évidement un coût : une exigence plus stricte des dépendances. Et cela amène à régler un bon nombre de conflits dans les dépendances.

### Rush

[Rush (https://rushjs.io/)](https://rushjs.io/) est un outil de gestion de mono-dépôt. Il a l'air plus rapide que [Lerna (https://lerna.js.org/)](https://lerna.js.org/). Il permet de :

1. Gérer la dépendance entre les composants ;
1. D'utiliser de communaliser les modules node.js;
1. De faciliter la gestion de contexte.

Au moment de l'essayer, j'ai eu un problème pour installer globalement via NPM sur un serveur GNU/Linux. Donc difficile de voir quel est le compromis à effectuer pour utiliser Rush.

## Conclusion

Le script bash, Make et Just permettraient uniquement de réduire la complexité lors du développement :+1: pas la problématique de rapidité :-1:. L'utilisation de npm ou de rush.js permettraient d'adresser les deux problématiques au prix de modification plus complexe.

Actuellement notre équipe n'a pas encore fait de choix dans la piste à suivre.

Si vous avez des retours ou des conseils sur la gestion de vos dépôts de vos projets professionnels ou personnels, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/). De mon côté, j'essayerai de rendre compte des évolutions dans notre processus de développement.
