---
title: "Masquer les détails d'implémentation d'une API REST"
date: "2023-11-09"
draft: false
description: "ce n'est pas aussi simple que ça"
tags: ["REST API", "projet perso"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série :

- [Présentation de mon projet perso du moment]({{< ref "/post/fr/2023-11-21-rewrite-side-project-api-in-go-part0" >}}) ;
- [Ajout de logique métier dans une API REST - classement des joueurs]({{< ref "/post/fr/2023-12-30-rewrite-side-project-api-in-go-part2" >}}) ;
- [Ajout de logique métier dans une API REST - calcul des points]({{< ref "/post/fr/2024-01-09-rewrite-side-project-api-in-go-part3" >}}) ;
- [Confronter une API REST à l'existant]({{< ref "/post/fr/2024-04-17-rewrite-side-project-api-in-go-part4" >}}).

Suite à l'article [Utiliser les conseils de Design of Web APIs]({{< ref "/post/fr/2023-08-01-the-design-of-web-apis-lecture" >}}), j'ai remarqué un point pas pratique dans mon API REST, on voit clairement que le système de stockage est une base de données relationnelle.

Pour rappel, l'API REST en question est la réécriture complète en Go du logiciel de tournoi dont je vous ai parlé dans la série : `Reprendre un projet après 3 ans` [premier billet]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years.md" >}}). La description au format OpenAPI de l'API cible est [ici (https://gitlab.com/trambi/fantasyfootballapi/-/blob/main/api/target-api-openapi3.json)](https://gitlab.com/trambi/fantasyfootballapi/-/blob/main/api/target-api-openapi3.json).

Le principe de cette API REST est de gérer un tournoi, les participants et les matchs associés.

## Deux problèmes d'utilisation

### Endpoints surnuméraires

En écrivant les tests d'API (et de même de stockage), je me suis aperçu, qu'avant d'ajouter un tournoi, j'étais obligé d'ajouter chacune des factions autorisées.

Par exemple, pour que l'API puisse créer un tournoi autorisant les joueurs à jouer les factions elfes et humains, il faut d'abord créer les factions elfes et humains.

```mermaid
sequenceDiagram
    User->>API REST: Créer un tournoi avec les factions suivantes : Elfes et Humains
    API REST-->>User: Non, les factions suivantes n'existent pas: Elfes et Humains!
    User->>API REST: Créer la faction Elfes
    API REST-->>User: Ok, la faction Elfes est créée avec l'identifiant 1
    User->>API REST: Créer la faction Humains
    API REST-->>User: Ok, la faction Humains est créée avec l'identifiant 1
    User->>API REST: Créer un tournoi avec les factions suivantes : Elfes et Humains
    API REST-->>User: Ok, le tournoi est créé avec l'identifiant 1
```

Si je crée un tournoi ensuite où je veux autoriser les factions nains, orques et humains, il faut que je me souvienne que j'ai déjà les factions elfes et humains puis que j'ajoute les factions nains et orques. C'est :

1. pénible surtout quand plus de vingt factions sont jouables;
1. pas présent dans la première API REST ;
1. la manifestation d'un détail d'implémentation : je stocke mes factions dans une table, les factions ont des identifiants numériques qui ne sont pas tout l'identifiant utilisé par les utilisateurs.

### Suppression d'un identifiant vraiment pas métier

Le tournoi représente les factions autorisées sous forme d'un tableau identifiant de faction et nom de faction.

```json
[
    {
        "id":"2",
        "name":"Elfes"
    },{
        "id":"3",
        "name":"Humains"
    }
]
```

L'identifiant n'a aucune plus value pour les utilisateurs. Ce serait bien plus pratique, si cela se présenter sous la forme d'un tableau de noms de faction.

```json
[
    "Elfes", 
    "Humains"
]
```

## Implémentation

### Suppression des endpoints

Une solution consiste tout simplement à enlever les endpoints de gestion des factions. Il faut :

- supprimer les routes associées aux endpoints "faction" de mon paquet `internal/api` ;
- supprimer les tests associés ;
- ajouter la création des factions manquantes durant la création et la mise à jour des tournois.

Le logiciel gère les détails sur les factions. Pour reprendre l'exemple de la partie précédente, je crée l'édition avec les factions elfes et humains et c'est tout.

```mermaid
sequenceDiagram
    User->>API REST: Créer un tournoi avec les factions suivantes : Elfes et Humains
    API REST-->>User: Ok, le tournoi est créé avec l'identifiant 1 :sparkles:
```

Si je veux créer un autre tournoi avec les factions nains, orques et humains, je crée une nouvelle édition avec les factions nains, orques et humains. Je n'embête pas les utilisateurs de l'API avec des détails d'implémentation.

### Utilisation d'un identifiant métier pour les factions

Il y a plusieurs manières d'implémenter la suppression de l'identifiant dans l'objet métier faction.

- soit on ne change pas la manière dont sont stockées les données ;
- soit on simplifie le stockage des factions.

#### Sans changer le stockage des données

```mermaid
graph LR
    mappingeditionfaction[Table d'association tournoi faction]-->edition[Table tournoi]
    mappingeditionfaction-->faction[Table faction]
    coach[Table joueur]-->faction
    coach-->edition
```

Pour ne pas changer le stockage, je suis parti sur une solution compliquée où je me retrouvais à devoir gérer un objet métier sans identifiant technique et un adaptateur ORM (oui j'utilise un adaptateur ORM pour ne pas polluer mon objet métier avec des détails d'implémentation, plus de détails dans [ORM en 3 phrases]({{< ref "/post/fr/2023-06-23-orm-en-3-phrases" >}})) de mon objet faction avec cet identifiant. Pour passer de adaptateur ORM à l'objet métier aucun problème mais pour passer d'un objet métier à adaptateur ORM, j'étais obligé d'aller chercher avant l'identifiant technique. Bref c'était long, pénible et incertain.

Or mon ORM, [GORM (https://gorm.io/)](https://gorm.io/), permet d'utiliser des identifiants sous forme de chaîne de caractère donc je suis arrêté. Et j'ai décidé de faire un bilan des différentes solutions.

|Solution|Avantages|Inconvénients|
|--------|---------|-------------|
|ID technique numérique absent de l'objet métier||:-1:Long, pénible et incertain|
|ID technique chaîne de caractère basée sur le nom|||
|Commun aux deux|:+1:Je garde la possibilité de faire des statistiques sur les factions jouables en tournoi|:+1: Je dois continuer à gérer la création des factions manquantes lors de la création ou de la mise à jour des tournois|
||:+1:Je garde la possibilité d'étendre l'objet faction avec, par exemple, des traductions||

#### Simplifier le stockage des factions

Il est aussi possible de stocker les factions jouables sous forme d'une représentation JSON du tableau des noms de factions. Cela revient à supprimer la table d'association tournoi faction, la table faction et de stocker la faction jouée par un joueur sous forme de son nom.

```mermaid
graph LR
    edition[Table tournoi]
    coach[Table joueur]-->edition
```

Donc de bien simplifier cette région du schéma de données.

|Avantages|Inconvénients|
|---------|-------------|
|:+1: Supprime la nécessité de gérer la création des factions manquantes lors de la création ou de la mise à jour des tournois|:-1:Je perds la possibilité de faire des statistiques sur les factions jouables en tournoi|
| :+1: Simplification du schéma de base de données|:-1:Je perds la possibilité d'étendre l'objet faction avec, par exemple, des traductions|

## Conclusion

J'arrive à la fin de ce billet et je n'ai pas encore décidé de la manière dont je vais opérer. On voit qu'il est parfois très facile de faire des solutions compliquées et que la difficulté est souvent de simplifier nos logiciels pour les utilisateurs. Les API REST ne font pas exception à la règle.

J'espère que ce billet vous aura motivé à simplifier vos API REST. Si vous avez une préférence pour l'implémentation de cette simplification, n'hésitez pas à les partager avec moi sur [X](https://x.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
