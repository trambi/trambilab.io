---
title: "Software Craftman - Fiche de lecture"
date: "2020-11-11"
draft: false
description: "Le livre de Sandro Mancuso qui m'a convaincu de lancer ce blog"
tags: ["fiche de lecture","Software Craftmanship","base"]
author: "Bertrand Madet"
---
## A qui s'adresse ce livre ?

Aux développeuses et aux développeurs qui veulent s'améliorer dans leur pratique du développement logiciel.

## Ce qu'il faut retenir

L'approche Software Craftmanship - artisanat logiciel propose une métaphore du développement différente de celle de l'ingénierie logicielle. Cette approche est plus axée sur les pratiques - principalement issue de l'extreme programming. Elle permet d'intégrer la dimension de qualité du code.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
|:+1: Démarche holistique pour aider sa structure à améliorer le développement logiciel au sein d'une entité : du recrutement et aux évènements de partage|:-1: La vision de la carrière qui me semble dénuée d'affect|
|:+1: Des conseils sur les pratiques de développement logiciel du développement piloté par les tests à la programmation par paire||

Le livre est très complémentaire des livres de Robert Martin - `Clean code`, `the Clean coder` et même `Clean Agile`. Ils sont dans la même collection et forme un ensemble cohérent et très intéressant. Il détaille plus les aspects autour des pratiques : comment convaincre un chef de projet de l'utilité de la programmation par paire ou comment rédiger une annonce pour recruter un développeur. Je dirais qu'il s'agit d'un livre à lire après `Clean code` - [ma fiche de lecture]({{< ref "/post/fr/2020-11-16-clean-code-lecture.md" >}}) mais avant `the Clean coder` et `Clean Agile`.

Et comme le résumé de l'article l'indique, ce livre m'a convaincu de lancer ce blog.

## Détails

- Titre : The Software Craftman - Professionalism, Pragmatism, Pride
- Auteur : Sandro Mancuso
- Langue : :gb:
- ISBN : 978-0-13-405250-2

*16/11/2020 : ajout du lien vers la fiche de lecture de Clean code*
*06/02/2022 : Reformatage*
