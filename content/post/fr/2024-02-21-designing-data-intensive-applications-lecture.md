---
title: "Designing Data-Intensive Applications - Fiche de lecture"
date: "2024-02-21"
draft: false
description: "Livre de Martin Kleppmann sur la gestion des données"
tags: ["fiche de lecture","architecture","outils"]
author: "Bertrand Madet"
---

## A qui s'adresse ce livre

Ce livre s'adresse à celles et ceux qui doivent créer ou maintenir des systèmes de gestion de données, comme les ingénieurs logiciels, les *data engineers* et les architectes logiciels. Il doit permettre de composer les outils de gestion de données pour répondre à une problématique métier.

## Ce qu'il faut retenir

- Les outils de gestion des données sont très nombreux et promettent des fonctionnalités extraordinaires ;
- Les systèmes distribués permettent de garantir le potentiel de mise à l'échelle et la tolérance aux pannes au prix d'une augmentation de la complexité ;
- Pour connaître les compromis nécessaires à l'utilisation de chaque outil, il faut comprendre le fonctionnement intrinsèque des outils et les hypothèses d'usage.
- Si on cible à la fois le potentiel de mise à l'échelle, la fiabilité et la maintenabilité, il y n'a pas d'outil parfait et en général pour répondre à une problématique métier précise, on peut envisager plusieurs composition d'outils.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: De la base aux détails des systèmes de gestion de données | :-1: Très dense 540 pages sans compter le glossaire et l'index |
| :+1: Une très belle démonstration des difficultés à faire fonctionner des systèmes distribués | :pinching_hand: Edition qui date de 2019, u peu datée sur la partie Hadoop et le futur des systèmes de gestion de données |
| :+1: Pas spécifique à une technologie||

Ce livre est dense, très argumenté avec beaucoup de références bibliographiques. Il s'intéresse au fonctionnement des systèmes de gestion de données des bases de données classiques aux agents de messages en passant par les bases de données *NoSQL*. En entrant dans le fonctionnement des différents types d'outils, on comprend les forces et les faiblesses des solutions et comment contre-balancer les faiblesses d'un outil dans un système.

La partie sur les problèmes des systèmes distribués, il fournit des rappels salutaires sur l'incertitude des réseaux et la synchronisation toujours imparfaite des machines.

## Détails

- Titre : Designing Data-Intensive Applications - The big ideas behind reliable, scalable, and maintainable systems
- Auteur : Martin Kleppmann
- Langue : :gb:
- ISBN : 978-1449373320
