---
title: "ORM en 3 phrases"
date: "2023-06-23"
draft: false
description: "Object relational mapping"
tags: ["outils","3 phrases","architecture"]
author: "Bertrand Madet"
---

Un ORM (:gb: **O**bject **R**elational **M**apping) est un outil qui permet de faire les liens (ça c'est le côté `mapping`) entre des structures dans le code (ça c'est le côté `object`) et une base de données relationnelles (ça c'est le côté `relational`). Je résume mon expérience en 3 phrases :

1. Les ORMs vous facilitent la vie ;
1. Les ORMs induisent des tensions dans l'architecture de votre logiciel ;
1. Les ORMs ont des limites.

Explications... :teacher:

## Les ORMs vous facilitent la vie

Pour les manipulations basiques de stockage d'une entité en base de données relationnelle, il faut :

- une requête SQL de type `INSERT` pour la création ;
- une requête SQL de type `SELECT` pour la lecture ;
- une requête SQL de type `UPDATE` pour la mise à jour ;
- une requête SQL de type `DELETE` pour la suppression.

On parle souvent de `CRUD` (pour **C**reate **R**ead **U**pdate **D**elete). Cela fait 4 requêtes SQL par entité sans compter les éléments composites. Il faut aussi ajouter la création de tables et les potentielles migrations de structures de tables. Cela ajoute encore du travail répétitif qu'il faut mener à bien en évitant d'exposer son logiciel à des injections SQL - une des vulnérabilités les plus communes dans les logiciels.

Voilà tout le but des ORM : fournir une couche d'abstraction entre les entités à stocker et le stockage en base de données. En général à partir d'une description de l'entité à stocker, l'ORM gère toutes les interactions avec la base de données.

Donc si vous êtes amené à implémenter le stockage de plusieurs entités, vous avez intérêt à regarder les ORM disponibles. De même si vous voulez pouvoir utiliser différents moteurs de base de données (MariaDB :seal:, PostgreSQL :elephant:), la couche d'abstraction fournie par cet outil peut vous être utile.

## Les ORMs induisent des tensions dans l'architecture de votre logiciel

Les ORMs utilisent des descriptions des entités à stocker. Souvent ces descriptions sont sous la forme d'annotation - des commentaires avec un format spécifique. Par exemple, pour décrire avec GORM en Golang une entité de type `faction` qui contient uniquement un identifiant et un nom.

```golang
type Faction struct {
        ID   uint
        Name string `gorm:"not null"`
}
```

En gorm, l''identifiant et clé primaire est le champ ID et l'annotation `gorm:"not null"` permet d'indiquer que le champ correspondant au nom sera non vide.

Si on réfléchit en terme de dépendance, on a :

```mermaid
graph LR;
    faction(Faction entité métier)-->|dépend de|gorm[GORM]
```

Ce qui n'est pas terrible, vu qu'en général en terme d'architecture logiciel, il vaut mieux éviter pour un objet métier de dépendre de détail d'implémentation comme l'ORM utilisé ou le simple fait que le stockage se fasse sur une base de données relationnelles.

Pour convaincre du caractère délicat de la chose, imaginons que nous devons stocker le nom de la faction en nous assurant de la nom est unique dans la table, nous allons devoir modifier le code source de l'objet métier pour modifier l'annotation. Donc pour modifier un détail d'implémentation, on modifie l'objet métier. Pour aller plus loin, je vous conseille le livre  Clean Architecture - [Fiche de lecture]({{< ref "/post/fr/2020-12-23-clean-architecture-lecture.md" >}}).

Dans le même genre d'idée mais plus pointilleux, le fait d'avoir un identifiant technique sur un objet métier est aussi gênant. Les personnes du domaine identifient souvent les entités métier par leur nom plutôt que par leur identifiant, pour notre exemple, on va plus facilement parler de la faction "orque" que de la faction "1".

Pour résoudre, la première tension, on peut créer une entité au stockage qui fasse l'adaptation en l'entité métier et l'ORM.

```mermaid
graph LR;
    factionAdapter(Entité d'adaptation Faction-ORM)-->|dépend de|gorm[GORM]
    factionAdapter-->|dépend de|faction(Faction entité métier)
```

```golang

//(...) package et import

// factionAdapter adapts a faction to ORM
type factionAdapter struct {
	ID   uint
	Name string `gorm:"not null,unique"`
}

func (adapter *factionAdapter) toBusiness(faction *models.Faction) {
	faction.Name = adapter.Name
	faction.ID = adapter.ID
}

func (adapter *factionAdapter) fromBusiness(faction models.Faction) {
	adapter.Name = faction.Name
	adapter.ID = faction.ID
}
```

Pour l'identifiant, le problème n'apparaîtra que si vous avez plusieurs types de stockage avec des identifiants différents par exemple un nombre entier et un [UUID](https://fr.wikipedia.org/wiki/Universally_unique_identifier).

Dans ce cas, une solution pourrait être d'utiliser l'identification métier côté entités métier et de se baser sur les éléments métier d'identification pour cacher les identifiants techniques dans les entités d'adaptation. Pour une mise à jour, on commencerait par récupérer l'objet d'adaptation à partir des éléments métier d'identification (pour une faction - le nom) puis on injecte l'objet métier dans l'objet d'adaptation et finalement on met à jour en base cet objet d'adaptation. La complexité de la solution la rend peu viable pour juste supprimer le champ ID de l'objet métier mais pourrait valoir le coup dans le cas de différents stockages avec des identifiants de nature différente.

## Les ORMs ont des limites

Les ORMs ont des limites. Par exemple :

- ils ne font pas toujours les requêtes les plus optimisées ;
- ils ne permettent pas forcement de gérer les spécificités d'un moteur de base de données ;
- ils introduisent une API avec une logique propre.

Les deux premières limites apparaissent dans des cas spécifiques et dans les phases avancées d'un projet. De plus, elles sont d'ordre technique et peuvent être contournées avec des développements spécifiques.

La logique propre d'un ORM est de s'abstraire du SQL, il y a donc des concepts différents et cela se concrétise par un temps d'apprentissage au début de l'utilisation de l'ORM.

## Conclusion

Pour la plupart des logiciels en prise avec une base de données relationnelle, il est raisonnable de considérer l'utilisation d'un ORM. J'espère que ce billet vous permettra d'appréhender les avantages et les inconvénients de ces solutions et adapter votre décision aux spécificités de votre logiciel et de son contexte d'utilisation. 

Si vous voulez discuter d'ORM (ou d'autres concepts abordés dans ce billet), contactez moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
