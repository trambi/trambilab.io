---
title: "TDD, refactoring et Git"
date: "2021-07-13"
draft: false
description: "tout se combine pour améliorer la qualité"
tags: ["tdd","git","refactoring","methodologie","Software Craftmanship"]
author: "Bertrand Madet"
---

Après la lecture du livre Refactoring ([ma fiche de lecture]({{< ref "/post/fr/2021-07-06-refactoring-lecture.md" >}})), j'ai décidé de mettre en pratique la méthodologie en l'intégrant dans une autre méthodologie, le développement piloté par les tests (TDD - cf. [TDD en 3 phrases]({{< ref "/post/fr/2021-06-15-tdd-en-3-phrases.md" >}})).

J'ai eu la chance de pouvoir expérimenter sur une fonctionnalité où les tests fonctionnels étaient suffisant nombreux pour que cela soit facile de passer d'écrire des tests unitaires.

## Refactoring déjà dans TDD

Le TDD inclue déjà une phase de `refactoring`, je l'avais compris comme une phase de restructuration de code au sens générique pas celle définie par Martin Fowler (l'auteur du livre Refactoring). Si on combine les diagrammes d'état on obtient le diagramme d'état suivant :

```mermaid
stateDiagram-v2
    think: Réfléchir
    writeTest: Écrire un test
    testFailed: S'assurer que le test échoue
    writeCode: Écrire juste assez de code pour que le test réussisse
    testSuccessed: S'assurer que le test passe
    testsSuccessed : S'assurer que les tests passent
    oneRefactoringStep: Appliquer un refactoring
    revert: Annuler les changements
    commit: Enregistrer le changement
    [*] --> think
    think --> writeTest
    writeTest --> testFailed
    testFailed --> writeCode: Le test échoue
    testFailed --> writeTest: Le test passe
    writeCode --> testSuccessed
    testSuccessed --> commit: Le test passe
    testSuccessed --> writeCode: Le test échoue
    commit --> oneRefactoringStep: Le code sent
    commit --> writeTest: Le code ne sent pas et on veut continuer
    commit --> [*]: On veut s'arrêter
    oneRefactoringStep --> testsSuccessed
    testsSuccessed --> commit: Les tests passent
    testsSuccessed --> revert: Un des tests échouent
    revert--> [*]: On veut s'arrêter
    revert--> writeTest: On veut continuer à implémenter des fonctionnalités
    revert--> oneRefactoringStep: On veut réessayer
```

Ce diagramme peut faire peur, mais il permet d'être sûr d'avoir une augmentation progressive des fonctionnalités et de la qualité du code.

## Git rebase à la rescousse

Le problème c'est que pour ajouter une fonctionnalité, il faut souvent écrire plusieurs tests et souvent faire plusieurs cycles de refactoring. On peut donc se retrouver avec des dizaines de commit.

Si jamais, le nombre de commit pose un problème, vous pouvez utiliser `git rebase -i HEAD~n` (j'avais parlé de cette commande dans mon [billet sur 4 commandes git]({{< ref "/post/fr/2021-01-24-4-commandes-git.md" >}})). Vraisemblablement, les instructions les plus utilisées seront :

- `fixup` qui permet de fusionner le commit considéré avec le précédent en gardant le message du commit précédent ;
- `squash` qui permet de fusionner le commit considéré avec le précédent en modifiant le message de commit.

## Adaptation de la méthode

Pour un ajout de fonctionnalité, on peut regrouper plusieurs étapes de refactoring dans un commit. Il faut veiller à garder un nombre restreint (pas plus de 5 par exemple) d'étapes de refactoring.

On peut aussi attendre d'ajouter plusieurs tests pour effectuer le refactoring. Dans ce cas et d'expérience, le code peut commencer à sentir au bout de 2 ou 3 tests.

## Conclusion

Cette méthodologie peut paraître complexe, elle est en fait confortable parce que l'on peut pousser notre développement même dans la branche mère. Je me demande comment on peut la combiner avec la programmation par paire.

Si vous voulez partager vos méthodes de développement et leurs cas d'usage, partagez les à moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
