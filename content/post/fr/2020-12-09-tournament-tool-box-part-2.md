---
title: "Boîte à outils pour tournoi - partie 2"
date: "2020-12-09"
draft: false
description: "Un interpréteur simple en Go"
tags: ["projet perso","Go","jeu","interpreteur"]
author: "Bertrand Madet"
---

Boite à outils pour tournoi :
[partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}),
[partie 3]({{< ref "/post/fr/2020-12-28-tournament-tool-box-part-3.md" >}}),
[partie 4]({{< ref "/post/fr/2021-04-26-tournament-tool-box-part-4.md" >}}),
[partie 5]({{< ref "/post/fr/2021-06-01-tournament-tool-box-part-5.md" >}}),
[partie 6]({{< ref "/post/fr/2023-03-08-tournament-tool-box-part-6.md" >}})

----
*Modifié le 10 mai 2021, pour améliorer la mise en forme.*

Après vous avoir présenté mon projet personnel du moment ([TournamentToolBox](https://www.gitlab.com/trambi/tournamenttoolbox)) dans un billet précédent [Boîte à outils pour tournoi - partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}), je vais vous parler de mon premier gros défi. L'ajout de colonnes calculées à partir de la valeur d'autres colonnes.

## Le but

Certains paramètres d'un match sont calculables à partir d'autres paramètres de ce match.

Par exemple, en football :soccer:, les points sont calculés ainsi :

- Si une équipe marque plus de but que son adversaire, une victoire - en langage sportif :wink: - elle marque trois (3) points ;
- Si elle marque autant de buts que son adversaire, c'est un match nul et remporte un (1) point ;
- Sinon elle perd et elle remporte zéro (0) points.

Autre exemple issu du football encore, les points de fair-play sont égaux à la somme du nombre de carton jaune et du nombre de carton rouge multiplié par cinq (5).

Si vous avez l'impression que les organisateurs de football sont imaginatifs, je peux vous assurer que ceux de Blood Bowl :football:, le sont plus encore.

Pour faciliter la vie des organisateurs de tournoi, le but est de calculer ces paramètres calculables.

## Présentation du problème

Il y a deux sous-problèmes :

- comment gérer les conditions. Par exemple, donner trois points en cas de victoire, un point en cas de match nul et zéro sinon.
- interpréter une chaîne de caractères avec les variables du match. Par exemple les points de fair-play à partir d'une formule comme celle-ci : `{red_card_1}*5 + {yellow_card_1}`.

## Développement

### Le plan

Le plan était plutôt simple : trouver un interpréteur Lua en Golang et développer le liant entre cet interpréteur Lua et la configuration JSON du tournoi.

### La réalisation

Après une recherche rapide, je n'ai pas trouvé d'interpréteur Lua en Golang.

#### Les conditions

J'ai commencé à résoudre les conditions en les utilisant un formalisme proche de MongoDB. Ainsi plus grand que est formalisée par l'opérateur "[$gt](https://docs.mongodb.com/manual/reference/operator/query/gt/#op._S_gt)", en langage de requête MongoDB `{ qty: { $gt: 20 } }` sera vrai si le champ qty est plus grand que vingt (20). L'ajout du caractère `$` me gênait car il nuisait à mon avis à la lisibilité.

Voilà comment le programme doit être configuré pour calculer les points.

```JSON
{
      "name": "points_1",
      "default_value": "0",
      "statements": [
        {
          "clause": {
            "type": "equal",
            "left": "score_1",
            "right": "score_2"
          },
          "value_if_true": "1"
        },
        {
          "clause": {
            "type": "greater",
            "to_be_greater": "score_1",
            "to_be_lesser": "score_2"
          },
          "value_if_true": "3"
        }
      ]
    }
```

Je ne suis pas entièrement convaincu par cette manière d'écrire les conditions. Il faudra que je teste cette formulation avec des organisateurs.

#### Interpréteur

Je me souvenais de mes cours de C où l'on parlait de compilateur avec les lexers et les parsers. Et ce n'était plus clair du tout.

Une recherche sur [DuckDuckGo](https://www.duckduckgo.com) m'a mené sur l'excellent [blog de Ruslan Pivak](https://ruslanspivak.com) et sa série pour construire un interpréteur Pascal (premier épisode <https://ruslanspivak.com/lsbasi-part1/>). La série est très bien faite et très progressive. Elle a l'avantage de proposer une implémentation en Python.

Tous les développements sont dans le paquet `evaluator`.

##### Découpage en jeton

J'ai commencé par le découpage en jeton (fichiers `tokenizer.go` et `tokenizer_test.go`). Le tokenizer va découper la chaîne de caractère  en jetons (tokens en :gb:). Ainsi `12+3` va être découpé en trois (3) jetons : un jeton entier `12`, un jeton `+` et un jeton entier `3`.

J'ai implémenté dix (10) jetons différents :

- _INTEGER_ qui permet de reconnaître un entier ;
- _PLUS_ qui permet de reconnaître l'addition ;
- _MINUS_ qui permet de reconnaître la soustraction ;
- _MUL_ qui permet de reconnaître la multiplication ;
- _LPAREN_ qui permet de reconnaître l'ouverture de parenthèse ;
- _RPAREN_ qui permet de reconnaître la fermeture de parenthèse ;
- _LCBRACKET_ qui permet de reconnaître l'ouverture d'accolade ;
- _RCBRACKET_ qui permet de reconnaître la fermeture d'accolade ;
- _VARIABLE_ qui permet de reconnaître une variable ;
- _EOF_ qui permet d'arrêter le découpage.

##### Parseur

J'ai continué avec le parseur (fichiers `parser.go` et `parser_test.go`) qui va établir l'organisation entre les jetons. L'expression `{red_card_1}*5 + {yellow_card_1}` donnera l'arbre suivant.

```mermaid
graph TD;
plus("+")--->mult(*)
mult--->red_card([red_card_1])
mult--->five([5])
plus--->yellow_card([yellow_card_1])
```

Cet arbre est un `AST` pour **A**bstract **S**yntax **T**ree.

##### Évaluateur

L'évaluateur (fichiers `interpreter.go` et `interpreter_test.go`) permet ensuite d'interpréter l'arbre avec le contexte. Pour l'arbre plus haut, on va commencer à évaluer `red_card_1` avec le contexte de la ligne, `*`, `yellow_card_1` et `+`.

### En résumé

```mermaid
graph LR;
expr([Expression])-->tokenizer[Découpage en jeton]
tokenizer-->|jetons|parser[Parseur]
parser-->|AST|interpreter[Évaluateur]
context([Contexte])-->interpreter
interpreter-->result([Résultat])
```

## La suite

Pour la suite, j'aimerai :

1. ajouter une fonctionnalité au calcul du classement pour pouvoir prendre en compte la somme des points des adversaires ;
2. ajouter la fonctionnalité d'appariement ;
3. à lecture de Clean Architecture, réarchitecturer le code ;
4. améliorer l'écriture des conditions.

Voilà, j'espère que cela vous a plu. Si vous souhaitez partager des projets, contactez [moi sur Twitter](https://twitter.com/trambi_78). Et j'accueille les merge requests avec enthousiasme.

----

Boite à outils pour tournoi :
[partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}),
[partie 3]({{< ref "/post/fr/2020-12-28-tournament-tool-box-part-3.md" >}}),
[partie 4]({{< ref "/post/fr/2021-04-26-tournament-tool-box-part-4.md" >}}),
[partie 5]({{< ref "/post/fr/2021-06-01-tournament-tool-box-part-5.md" >}}),
[partie 6]({{< ref "/post/fr/2023-03-08-tournament-tool-box-part-6.md" >}})
