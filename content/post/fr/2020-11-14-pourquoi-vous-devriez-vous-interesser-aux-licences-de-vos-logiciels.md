+++ 
title = "Pourquoi vous devriez vous intéresser aux licences de vos logiciels ?"
description = "Librement téléchargeable ne veut pas dire librement utilisable" 
tags = [ "licence","base" ] 
date = "2020-11-14" 
author = "Bertrand Madet" 
+++

*Billet initialement publié sur mon compte LinkedIn le 6 mai 2020, augmenté et remanié*

## license ou licence

En français :fr:, c'est `licence` et en anglais c'est soit `licence` (en anglais britannique :uk:) soit `license` (en américain :us:). Github et Gitlab ont choisi de prendre en compte le fichier `LICENSE`.

## C'est quoi une licence logicielle

Une licence est un accord entre le propriétaire d'un logiciel et ses utilisateurs. La plupart du temps, c'est un texte pas très digeste nommé `LICENSE` ou situé dans la page internet `/license`, `/about/licence` de votre logiciel préféré.

Une licence définit ce que vos utilisateurs peuvent faire avec un logiciel donné. Cela donne des droits mais aussi des devoirs comme des limitations d'usage ou des obligations d'afficher un texte.

## Vous devriez vous intéresser aux licences logicielles parce

Nous utilisons beaucoup de bibliothèques tierces et

- une licence peut contrarier votre business model ;
- deux bibliothèques logicielles peuvent être incompatibles à cause de leurs licences ;
- l'absence de licence est très contraignante...

### Une licence peut-elle réellement contrarier votre business model ?

Et oui c'est possible. Certaines licences logicielles peuvent vous contraindre à autoriser vos utilisateurs de consulter, modifier et distribuer le code du logiciel y compris quand l'usage se fait à travers le réseau. La licence en question `GNU Affero General Public License version 3` est une licence utile pour des logiciels libres et elle est juste peut-être pas adaptée à votre business model.

Si vous voulez distribuer et vendre votre logiciel, l'autorisation de distribution par vos utilisateurs peut avoir un impact sérieux. Vos utilisateurs peuvent devenir ainsi des concurrents.

:warning: Quand j'écris, il faut réfléchir à la compatibilité entre son business model et les licences des logiciels, **ne signifie qu'il faut exclure d'office les logiciels sous licences AGPL ou GPL ou pire tous les logiciels libres**.

Des sociétés gagnent de l'argent autour du logiciel libre par exemple RedHat ou Linagora, elles ont adapté leur business model.

Choisir un logiciel libre a des conséquences (la plupart du temps positif pour l'état global de l'univers) et c'est aussi vrai pour les logiciels propriétaires mais nous y reviendrons plus tard.

### Il est possible que deux bibliothèques logicielles aient des licences incompatibles

Par exemple, vous avez le droit d'utiliser cette bibliothèque si vous utilisez la même licence que cette bibliothèque. La plupart du temps, le cas arrive avec les licences GPL and AGPL. Les licences GPL sont utilisées par :

- noyau Linux ;
- bash ;
- ansible ;
- ggplot2 ;
- MariaDB server ;
- shiny...

Ces licences sont dites contaminantes ou plus exactement elles ont une réciprocité forte (`strong copyleft`).

### L'absence de licence est très contraignante

Si un logiciel n'a pas de licence, cela signifie qu'il fonctionne sur le régime strict de la propriété intellectuelle. Et on ne peut pas faire n'importe avec une œuvre protégée par la propriété intellectuelle. Je vous laisse consulter votre référent pour avoir des explications sur la propriété intellectuelle en France.

## Toutes les licences sont différentes

Malheureusement, il n'existe pas de licence universelle. Dans l'univers du logiciel libre, il existe beaucoup de licences différentes. Certaines licences sont malgré tout très communes :

- `Apache License version 2` ou Apache v2 (<https://choosealicense.com/licenses/apache-2.0/>) ;
- `MIT License` ou MIT (<https://choosealicense.com/licenses/mit/>) ;
- `GNU General Public License version 3 or later` ou GPLv3 (<https://choosealicense.com/licenses/gpl-3.0/>) ;
- `GNU Lesser General Public License version 3 or later` ou LGPLv3 (<https://choosealicense.com/licenses/lgpl-3.0/>) ...

## Les logiciels propriétaires aussi ont des licences

Librement téléchargeable ne signifie pas librement utilisable. Je prends l'exemple de la machine virtuelle Java (Oracle Java - anciennement Java Runtime Environment) librement disponible sur <https://java.com/fr/download/>. Cette machine virtuelle Java dispose d'une licence qui autorise certaines utilisations gratuites (comme l'utilisation à titre personnel et le développement) mais pour le reste, il faut payer.

Tous les logiciels propriétaires ont des licences qui peuvent s'appeler `CLUF` ou `Contrat de la licence de l'utilisateur final` qui vont définir les droits et les devoirs des utilisateurs. Comme nous l'avons vu les licences peuvent avoir un impact important sur l'utilisation de logiciels (ou de bibliothèques) propriétaires.

## Et du coup, comment fait-on ?

La première chose, c'est de prendre un peu de temps pour lire la licence au moins le début. Le but est de voir si la licence est répandue ou spécifique. Vous pouvez chercher la licence sur [choosealicense](https://choosealicense.com/), [spdx](https://spdx.org/licenses/) ou [tldrlegal](https://tldrlegal.com).

Si c'est une licence est spécifique, vous allez devoir lire la licence entièrement et vraisemblablement demander conseil à des spécialistes du droit.

Pour mes projets personnels, je prends souvent la licence `Apache v2`. Et vous, avez-vous une licence préférée ? Partagez la avec [moi sur Twitter](https://twitter.com/trambi_78).

## Ressources

- Le [site Github choosealicense (https://choosealicense.com/)](https://choosealicense.com/)  fournit des synthèses pour les droits, les limitations et les conditions des licences libres les plus communes.
- La [page licence du site Software Package Data Exchange (https://spdx.org/licenses/)](https://spdx.org/licenses/)  fournit une liste de licence et des identifiants courts.
- La page [FAQ sur les GNU licenses du site GNU (https://www.gnu.org/licenses/gpl-faq.html)](https://www.gnu.org/licenses/gpl-faq.html) est une mine d'information  sur les licences GNU licenses (GPL, AGPL and LGPL) et leur compatibilité avec d'autres licences.
- L'assistant de [comparaison et de choix de licence (https://joinup.ec.europa.eu/solution/joinup-licensing-assistant/joinup-licensing-assistant-jla)](https://joinup.ec.europa.eu/solution/joinup-licensing-assistant/joinup-licensing-assistant-jla) fourni par la plateforme JoinUp de la Commission Européenne.
- Le [site tldrlegal (https://tldrlegal.com)](https://tldrlegal.com) fournit des explications en anglais des licences logicielles - moins à jour que les ressources précédentes.

*21/11/2020 : Réécrire de la partie avertissement -il manquait des mots dans la première version et ajout d'un appel à réaction sur Twitter*

*09/01/2021 : Clarification sur les licences et les business models*
