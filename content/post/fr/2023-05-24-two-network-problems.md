---
title: "Deux problèmes réseau"
date: "2023-05-24"
draft: false
description: "deux obstacles pour mes utilisateurs" 
author: "Bertrand Madet" 
tags: ["front-end","base","network","projet perso"]
---

Quoi de plus frustrant qu'un logiciel, qui une fois déployé, ne fonctionne pas ? Les tests unitaires sont bons, les tests d"intégration aussi, les démonstrations auprès des utilisateurs sont pleines de promesse. Mais à la mise en production, certains utilisateurs n'ont pas accès à l'application. Cette mésaventure m'est arrivée deux fois sur deux logiciels associatifs différents côté associatif. Cette semaine, nous allons disséquer ces erreurs de configuration.

## Site web inaccessible sur réseau mobile

Avec d'autres membres, je m'occupe du site web de mon association. Et depuis un certain temps (plus d'un an :cry:), nous avions un problème étrange : impossible d'accéder à notre site pour certaines personnes depuis leur smartphone. Pas de chance, sur mon smartphone tout fonctionner.

Le plus dur a été de comprendre l'origine du problème car cela ne dépendait pas de l'opérateur téléphonique mobile. En recensant les personnes pour qui cela fonctionnait et pour qui cela ne fonctionnait pas, il s'est avéré que seules les personnes en IPv6 n'avaient pas accès au site web. Cette déduction était liée à la modernité des smartphones et confirmés par l'utilisation de site de fourniture d'adresse IP comme [WhatIsMyIp (https://whatismyip.com/)](https://whatismyip.com/).

### IPv6

`IPv6` est la version la plus récente du protocole `IP` (pour **I**nternet **P**rotocol) - le protocole à la base d'Internet. Même si le protocole a été introduit en 1995, il cohabite encore avec la version précédente `IPv4`. Le but de ce protocole est de fournir un mécanisme pour envoyer des données d'une machine à une autre. La manifestation la plus connue du protocole est l'adresse IP :

- 4 octets écrit habituellement en décimal pour IPv4;
- 16 octets écrit en hexadécimal pour IPv6.

IPv4 semble encore majoritaire sur les réseaux fixes en France. C'est sûrement pourquoi nous avions peu ou pas de problème sur les réseaux fixes. Le protocole est expliqué sur [Wikipédia (https://fr.wikipedia.org/wiki/IPv6)]. Le site ne fonctionnait pas en IPv6 alors que notre fournisseur fournissait une adresse IPv4 et une adresse IPv6. Il devait y avoir autre chose.

### DNS

Est-ce que vous vous êtes déjà demandé ce qu'il se passe quand vous entrez une adresse dans votre navigateur ? Comment à partir d'une adresse (ou `url`), votre ordinateur sait quelle machine contacter ? Grâce au [protocole DNS (https://fr.wikipedia.org/wiki/Domain_Name_System)](https://fr.wikipedia.org/wiki/Domain_Name_System) : Ce protocole permet d'associer un nom de domaine (`FQDN` - **F**ully **Q**ualified **D**omain **N**ame) à une adresse IP via des enregistrements. Pour l'association entre un nom de domaine et une adresse IPv4, c'est un enregistrement `A`. Pour l'IPv6, c'est un enregistrement `AAAA`.

Une fois que j'ai trouvé sur l'hébergeur la configuration des enregistrement DNS, l'enregistrement AAAA n'indiquait pas la bonne adresse IPv6. Une fois l'enregistrement corrigé, le site était à nouveau disponible pour tous les smartphones.

La commande qui m'a permis de diagnostiquer le problème :

```bash
dig @<serveur-dns> <nom-de-domaine> AAAA
```

Où :

- `<serveur-dns>` est le serveur DNS à interroger;
- `<nom-de-domaine>` est le domaine de nom dont on veut connaître l'adresse IP.

## Service web inopérant sur IPhone

Pour mon tournoi, j'ai créé une application éphémère dont j'ai parlé dans les billets sur la [reprise d'un projet après 3 ans]({{< ref "/post/fr/2022-04-22-restarting-a-project-after-three-years-part3.md" >}}). Après l'avoir déployé dans une machine virtuelle Scaleway, le site était public (en http). Malheureusement pour les possesseurs d'IPhone, je n'ai pas réussi à corriger le problème mais j'ai une hypothèse.

L'url du service était longue et je n'avais pas encore plongé dans le méandre de la configuration de DNS. J'ai donc utilisé un raccourcisseur d'url.

### Redirection de https vers http

Le raccourcisseur d'url officie en https mais l'url de destination utilise le protocole http. Le changement de protocole de sécurisé vers non sécurisé est peut-être la cause du rejet.

## Conclusion

Les connaissances acquises pour résoudre le premier problème avec le DNS me permettront peut-être d'éviter le second problème. En tout cas, je testerai avec un smartphone sous iOS car ils sont connus pour être très restrictifs vis-à-vis des configurations des sites web.

Si vous avez des explications, des expériences ou des conseils sur des à coté du développement, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).