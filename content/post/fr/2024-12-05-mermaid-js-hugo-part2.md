---
title: "Mermaid.js avec Hugo"
date: "2024-12-05"
draft: false
description: "Version 2024"
tags: ["documentation","mermaid","hugo","blog"]
author: "Bertrand Madet"
---

Depuis 4 ans, j'utilise [Mermaid.js (https://mermaid-js.github.io/mermaid/#/)](https://mermaid-js.github.io/mermaid/#/) dans mon blog. Mermaid.js permet d'afficher différents types de schémas à partir de code.

Par exemple, pour afficher ça :

```mermaid
    gitGraph
       commit
       branch develop
       commit
       checkout main
       commit
```

Il faut écrire ce code :

```text
    gitGraph
       commit
       branch develop
       commit
       checkout main
       commit
```

J'ai déjà parlé de Mermaid.js dans un billet [Boostez vos documentations avec des graphiques]({{<ref "2020-12-13-mermaid-js">}}). La documentation de Mermaid.js est de plus bien fournie.

## Intégration précédente dans Hugo

[Hugo](https://gohugo.io/) est le générateur de site statique et il permet entre autre de générer ce blog

Initialement Hugo n'intègre pas Mermaid.js. et comme je voulais l'utiliser (on dit qu'une image vaut mille mots), j'ai installé sur Hugo.J'ai utilisé un `shortcode`. Il fallait que j'encapsule le code mermaid entre `{{</* mermaid */>}}` et `{{</* /mermaid */>}}`.

Plus de détails dans l'article de l'époque cf. [Mermaid.js avec Hugo]({{<ref "2020-12-13-mermaid-js-hugo">}}).

Cela fonctionnait mais il y avait deux inconvénients :

1. il fallait que je souvienne du shortcode ;
1. cela ne fonctionnait que pour hugo.

## Intégration en tant que bloc de code de type mermaid

La version moderne de l'intégration de mermaid dans Hugo est d'utiliser le bloc de code. L'idée est d'utiliser ` ```mermaid `, de passer la ligne, de mettre le code mermaid, de passer la ligne et de ` ``` `.

Donc

```text
```mermaid
    timeline
    title Mermaid in my blog history
    2020-11 : Création du blog
    2020-12 : Mermaid en shortcode
    2024-11 : Mermaid en bloc de code
``` 
```

permet d'afficher :

```mermaid
    timeline
    title Mermaid dans mon blog
    2020-11 : Création du blog
    2020-12 : Mermaid en shortcode
    2024-11 : Mermaid en bloc de code
```

L'avantage est que GitLab comprend aussi cette syntaxe et affiche correctement le graphique.

## Comment ?

Avec la version du thème Ghostwriter que mon blog utilise, j'ai commencé par ajouter le bloc de code mermaid en créant un fichier `themes/ghostwriter/layouts/_default/_markup/render-codeblock-mermaid.html` avec comme contenu :

```text
<pre class="mermaid">
  {{- .Inner | htmlEscape | safeHTML }}
</pre>
{{ .Page.Store.Set "hasMermaid" true }}  
```

Tout à ajouter les lignes suivantes au fichier de pied du modèle `themes/ghostwriter/layouts/partials/footer.html`.

```text
        {{ if .Store.Get "hasMermaid" }}
        <script type="module">
            import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.esm.min.mjs';
            mermaid.initialize({ startOnLoad: true });
            await mermaid.run({
                querySelector: '.language-mermaid',
            });
        </script>
        {{ end }}
```

Une fois que je me suis assuré que le code fonctionnait en local sur un article, j'ai enlevé le shortcode en supprimer le fichier `themes/ghostwriter/layouts/shortcodes/mermaid.html` puis j'ai enlevé les parties spécifiques au shortcode dans le fichier de pied du modèle.

## Conclusion

La manipulation est bien écrite dans la [partie bloc de code de la documentation d'Hugo (https://gohugo.io/render-hooks/code-blocks/#examples]( https://gohugo.io/render-hooks/code-blocks/#examples). Le plus long a été de modifier tous les billets contenant des shortcodes mermaid et de s'assurer que je n'avais rien oublié.

J'ai donc maintenant un moyen d'intégrer les graphiques Mermaid.js identique pour mon blog et pour GitLab. Comme j'en ai profité pour mettre à jour la version Mermaid.js, je pourrais en bonus utiliser d'autres types de graphique.

Si vous souhaitez partager votre amour ❤️ pour Mermaid.js, sur la gestion d'un blog ou sur Hugo, contactez moi [sur X](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169).
