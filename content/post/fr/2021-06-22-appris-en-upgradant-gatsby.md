---
title: "Ce que j'ai appris en mettant à jour Gatsby"
date: "2021-06-22"
draft: false
description: "cela a été compliqué"
tags: ["mise à jour","npm","nodejs","outils","typescript"]
author: "Bertrand Madet"
---

La semaine dernière (et un peu de cette semaine), nous avons décidé de mettre à jour Gatsby. Nous n'avions pas mis à jour Gatsby depuis la première utilisation une version 2.11.17, la version actuelle est la version 3.7.0 ! La mise à jour n'a pas été simple mais cela m'a permis d'apprendre des choses sur l'écosystème node.js et TypeScript que je vais essayer de d'expliquer dans ce billet.

## npm outdated et npm ls

Je me suis beaucoup servi de ces deux commandes. La première `npm outdated` permet d'afficher les paquets qui peuvent être mise à jour en suivant la politique indiquée dans le fichier `package.json` et aussi la plus grande version publiée.

Le seconde `npm ls paquet` permet d'afficher les versions installés du paquet nommé *paquet* et l'arborescence de dépendances de ce paquet.

## Les outils de développement TypeScript

Devoir faire fonctionner notre moteur de test [Jest (https://jestjs.io)](https://jestjs.io) avec des tests en TypeScript et notre site Gatsby avec Internet Explorer 11 :older_man: m'a permis de mieux appréhender l'outillage TypeScript.

```mermaid
graph LR
typescript[TypeScript files]-->compiler(Compilateur)
compiler-->javascript[Fichiers JavaScript]
javascript-->bundler(Bundler)
bundler-->asset[Fichier JavaScript]
```

Le compilateur TypeScript prend en entrée des fichiers TypeScript et les convertir en fichiers JavaScript. Cela peut être :

- [TypeScript (https://www.typescriptlang.org)](https://www.typescriptlang.org) : Compilateur spécifique à TypeScript ;
- [Babel (https://babeljs.io)](https://babeljs.io) : Compilateur générique, il prend en entrée TypeScript et du JavaScript "moderne" et de générer différentes versions de JavaScript pour qu'il fonctionne sur des navigateurs plus anciens. A noter que pour le TypeScript, il n'effectue pas la vérification de type.

Le groupeur de module ou bundler permet de gérer les différents modules JavaScript côté navigateur et de générer un nombre optimisé de fichier JavaScript. En général, c'est [WebPack (https://webpack.js.org)](https://webpack.js.org). Mais il y a des challengers comme [SnowPack (https://www.snowpack.dev/)](https://www.snowpack.dev/).

## La fréquence de mise à jour dans l'écosystème Node.js

Dans le contexte informatique actuel, je ne suis pas fan de figer les versions alors dans l'écosystème Node.js, c'est mission impossible.

Dans le monde JavaScript les choses vont très vite : les versions de Node.js `LTS` (**L**ong **T**erm **S**upport) ont 3 ans entre leur sortie et la fin de la phase de maintenance (à comparer avec les 10 ans pour les versions de Red Hat Enterprise Linux). Chrome et Firefox sortent une version toutes les 4 semaines.

Il faut trouver la bonne fréquence en fonction de son contexte, mais **cela me semble nécessaire de mettre à jour dès que possible**. Un collègue vérifie les versions des dépendances tous les jours sur un autre projet. Dans notre service qui comporte une demi-douzaine de projet Node.js, je serai adepte de vérifier l'obsolescence pour chaque modification sur un projet.

Cela évite d'avoir des bugs et des failles de sécurité qui persistent et des galères de mises à jour.

## Conclusion

En plus de retarder la parution de ce billet, la mise à jour de Gatsby a été l'occasion de revoir des commandes,des mécaniques et surtout de réfléchir à la stratégie de mise à jour. Si vous voulez partager des galères de développement et ce que vous en avez appris, partagez les à moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
