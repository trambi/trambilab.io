---
title: "Ajout de logique métier dans une API REST - calcul des points"
date: "2024-01-09"
draft: false
description: "configurable"
tags: ["REST API", "projet perso","Go"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série :

- [Présentation de mon projet perso du moment]({{< ref "/post/fr/2023-11-21-rewrite-side-project-api-in-go-part0" >}}) ;
- [Masquer les détails d'implémentation d'une API REST]({{< ref "/post/fr/2023-11-09-rewrite-side-project-api-in-go-part1" >}}) ;
- [Ajout de logique métier dans une API REST - classement des joueurs]({{< ref "/post/fr/2023-12-30-rewrite-side-project-api-in-go-part2" >}}) ;
- [Confronter une API REST à l'existant]({{< ref "/post/fr/2024-04-17-rewrite-side-project-api-in-go-part4" >}}).

L'API REST en question est la réécriture complète en Go du logiciel de tournoi dont je vous ai parlé dans la série : `Reprendre un projet après 3 ans` [premier billet]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years.md" >}}). Le but de cette API REST est de gérer un tournoi, les participants et les matchs associés. Les aspects de manipulation des objets métier sont gérés, on commence à gérer les classements. Cette fois-ci, je me suis attaqué au calcul des points. Le but est d'avoir un calcul configurable. Comme la configuration peut entraîner très loin, je me vais me limiter pour ce billet au calcul des points pour la première édition du tournoi de notre association et le but est de pouvoir configurer les points pour les éditions précédentes du tournoi (19).

## Le calcul des points

Pour calculer les points on part du principe de l'on a une configuration et que pour chaque partie on calcule les points avec cette configuration. En prenant l'exemple  du football :soccer:, les points sont calculés ainsi :

- Si une équipe marque plus de but que son adversaire, elle marque trois (3) points ;
- Si elle marque autant de buts que son adversaire, c'est un match nul et remporte un (1) point ;
- Sinon elle perd et elle remporte zéro (0) points.

Si on analyse cet exemple, on pourrait concevoir la configuration avec une valeur par défaut (0), la valeur 3 si le nombre de but de l'équipe est supérieur à celui de l'équipe adverse et la valeur 1 si le nombre de but de l'équipe est égal à celui de l'équipe adverse. On pourrait le modéliser comme cela :

```mermaid
classDiagram
  class PointsSettings
  class Clause
  class ICondition
  PointsSettings: +float64 DefaultValue
  PointsSettings: +[]Clause Clauses
  PointsSettings *-- "many" Clause
  Clause: +float64 ValueIfTrue
  Clause: +ICondition Condition
  Clause "1" *-- "1" ICondition
  GreaterCondition --|> ICondition
  EqualCondition --|> ICondition
  ICondition: +IsTrue(game Game) bool
  GreaterCondition: +IsTrue(game Game) bool
  GreaterCondition: +string VariableNameToBeGreater
  GreaterCondition: +string VariableNameToBeLesser
  EqualCondition: +IsTrue(game Game) bool
  EqualCondition: +string LeftVariableName
  EqualCondition: +string RightVariableName
```

La première édition de notre tournoi 🏉 ajoutait une petite défaite, ce qui donnait :

- Une victoire donne 5 points ;
- Un match nul donne 2 points ;
- Une défaite avec un essai d'écart donne 1 points ;
- Une défaite avec plus d'un essai d’écart donne 0 point ;

Pour pouvoir compter les points, on est obligé de prendre en compte des choses évaluables (variable de partie, constante et somme d'autres évaluables)  dans les conditions. Ce qui complexifie la modélisation de la configuration :

```mermaid
classDiagram
  class PointsSettings
  class Clause
  class ICondition
  class IValuable
  PointsSettings: +float64 DefaultValue
  PointsSettings: +[]Clause Clauses
  PointsSettings *-- "many" Clause
  Clause: +float64 ValueIfTrue
  Clause: +ICondition Condition
  Clause "1" *-- "1" ICondition
  GreaterCondition --|> ICondition
  EqualCondition --|> ICondition
  GreaterCondition "1" *-- "2" IValuable
  EqualCondition "1" *-- "2" IValuable
  ValuableConstant --|> IValuable
  ValuableGameVariable --|> IValuable
  ValuableSum --|> IValuable
  ValuableSum "1" *-- "2" IValuable
  ICondition: +IsTrue(game Game) bool
  GreaterCondition: +IsTrue(game Game) bool
  GreaterCondition: +IValuable ToBeGreater
  GreaterCondition: +IValuable ToBeLesser
  EqualCondition: +IsTrue(game Game) bool
  EqualCondition: +IValuable Left
  EqualCondition: +IValuable Right
  IValuable: +GetValue(game Game) float64
  ValuableConstant: +GetValue(game Game) float64
  ValuableGameVariable: +GetValue(game Game) float64
  ValuableSum: +GetValue(game Game) float64
  ValuableSum: +IValuable Left
  ValuableSum: +IValuable Right
```

On pourrait ainsi modéliser la configuration des points de notre premier tournoi avec une valeur par défaut à zéro (0) et une liste de clauses :

- clause de "petite défaite" avec une valeur de un (1) et une condition `EqualCondition`
  - champ `Left` la somme de la variable nombre de points et de la constante un (1),
  - champ `Right` la variable du nombre de points adverses ;
- clause de match nul avec une valeur de deux (2) et une condition `EqualCondition`
  - champ `Left` la variable nombre de points,
  - champ `Right` la variable du nombre de points adverses ;
- clause de victoire avec une valeur de cinq (5) et une condition `GreaterCondition`
  - champ `ToBeGreater` la variable nombre de points,
  - champ `ToBeLesser` la variable du nombre de points adverses.

## Implémentation du calcul de points

On reste fidèle à la méthodologie TDD donc on alterne entre test sur les suivantes structures et implémentation :

- `ValuableGameVariable` et la méthode `Value` ;
- `GreaterCondition` et la méthode `IsTrue` ;
- `EqualCondition` et la méthode `IsTrue` ;
- `Clause` ;

On peut alors créer un test pour le calcul standard des points pour le football. On peut ensuite calculer les points de chaque match, en parcourant les clauses en évaluant les conditions les unes après les autres. Et implémenter la méthode de calcul de points comme ci-dessous.

```go
func (game *Game) ComputeCoachPoints() error {
  points1 := game.Edition.CoachPoints.Default
  reversedGame := game.Reverse()
  points2 := game.Edition.CoachPoints.Default
  for _, clause := range game.Edition.CoachPoints.Clauses {
    conditionForGame, err := clause.Condition.IsTrue(*game)
    if err != nil {
      return err
    }
    conditionForReversedGame, err := clause.Condition.IsTrue(reversedGame)
    if err != nil {
      return err
    }
    if conditionForGame {
      points1 = clause.ValueIfTrue
    }
    if conditionForReversedGame {
      points2 = clause.ValueIfTrue
    }
  }
  game.Points1 = points1
  game.Points2 = points2
  return nil
}
```

On ajoute un test avec le calcul de points de la première édition et on peut implémenter les structures :

- `ValuableConstant` pour les constantes ;
- `ValuableSum` pour la somme de structures évaluables.

## Conclusion

Après une phase exploratoire pour faciliter le découpage du problème, on arrive à calculer de manière configurable les points comme pour la première édition de notre tournoi. Par rapport à l'application précédente en PHP nous avons échangé une classe de calcul pour la première édition (suivant le design pattern `Strategy`) par une dizaine de classes. Il faudra comparer avec l'historique intégrale du tournoi pour savoir si la complexité de configuration est contre-balancée par la suppression de classe spécifique à chaque édition ou groupe d'édition.

Les interfaces avec la base de données et l'API REST ne sont pas encore abordés. La partie de désérialisation a été un peu ardue et j'y reviendrai peut-être par la suite.

Si vous souhaitez participer au développement, j'accueille les merge requests avec plaisir. Et si vous avez des projets perso, n'hésitez pas à les partager avec moi sur [X](https://x.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
