---
title: "Boîte à outils pour tournoi - partie 7"
date: "2023-03-20"
draft: false
description: "Regroupement des lignes"
tags: ["projet perso","Go","jeu","tdd","test"]
author: "Bertrand Madet"
---
Boite à outils pour tournoi :
[partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}),
[partie 2]({{< ref "/post/fr/2020-12-09-tournament-tool-box-part-2.md" >}}),
[partie 3]({{< ref "/post/fr/2020-12-28-tournament-tool-box-part-3.md" >}}),
[partie 4]({{< ref "/post/fr/2021-04-26-tournament-tool-box-part-4.md" >}}),
[partie 5]({{< ref "/post/fr/2021-06-01-tournament-tool-box-part-5.md" >}}),
[partie 6]({{< ref "/post/fr/2023-03-08-tournament-tool-box-part-6.md" >}})

-----

Dans l'article précédent, j'avais évoqué l'idée de tester les regroupement avec des tests de propriétés et la bibliothèque [pgregory.net/rapid (https://github.com/flyingmutant/rapid)](https://github.com/flyingmutant/rapid). C'est que nous allons faire dans ce billet.

Pour rappel, le regroupement consiste en prendre en entrée des matchs et les regrouper les lignes en fonction de la valeur d'une liste de champs.

![le regroupement consiste en prendre en entrée des matchs et les regrouper les lignes en fonction de la valeur d'une liste de champs](./group-by.svg)

## Développement

### Le plan

Il s'agissait de décliner en code avec la bibliothèque `rapid` les propriétés suivantes :

- si les champs de regroupement appartiennent aux entêtes d'entrée et que les données d'entrée sont valides, les données de sortie sont valides ;
- si les champs de regroupement n'appartiennent pas aux entêtes d'entrée et que les données d'entrée sont valides, une erreur est levée ;
- les entêtes de sortie se répètent en fonction du nombre de regroupement.

Après réflexion et les conseils d'un collègue, j'ai décliné les propriétés d'une sortie valide :

- la longueur de toutes les lignes constante ;
- les valeurs "groupantes" apparaissent une seule fois ;
- le nombre de valeurs non vides et "non groupantes" est égal au nombre de valeur non vides et "non groupantes" des données d'entrées.

Je vais donc m'atteler aux propriétés ci-dessus.

## La réalisation

### Générateur d'entrées

Pour commencer les tests, il s'agit de définir des données d'entrée valides : c'est à un tableau à deux dimensions de texte aléatoire. On va utiliser le générateur `IntRange` pour avoir les dimensions du tableau. Les générateurs `SliceOfN` et `StringMatching` permettent de générer le contenu.

```go
func generateValidRows(t *rapid.T) [][]string {
  columnNumber := rapid.IntRange(2, 20).Draw(t, "columnNumber")
  rowNumber := rapid.IntRange(1, 20).Draw(t, "rowNumber")
  rowGen := rapid.SliceOfN(rapid.StringMatching(`[ a-zA-Z0-9_-]+`), columnNumber, columnNumber)
  rows := make([][]string, rowNumber)
  for i := 0; i < rowNumber; i++ {
    rows[i] = rowGen.Draw(t, "rows["+strconv.Itoa(i)+"]")
  }
  return rows
}
```

> J'ai utilisé des symboles usuels pour tester mais il est possible de tester avec des caractères unicode plus exotiques.

### Longueur des lignes constante

J'ai ajouté une fonction de contrôle de longueur des lignes.

```go
func rowsHaveSameLength(rows [][]string) bool {
  referenceLength := len(rows[0])
  for _, row := range rows[1:] {
    if len(row) != referenceLength {
      return false
    }
  }
  return true
}
```

Puis le cas de test avec la génération des données d'entrée, les entêtes et les champs de groupement.

```go
func testGroupByGivenValidInputReturnValidOutput(t *rapid.T) {
  rows := rapid.Custom(generateValidRows).Draw(t, "rows")
  columnNumber := len(rows[0])
  headers := rapid.SliceOfNDistinct(rapid.StringMatching(`[ a-zA-Z0-9_-]+`), columnNumber, columnNumber, rapid.ID[string]).Draw(t, "headers")
  permutedHeaders := rapid.Permutation(headers).Draw(t, "permutedHeaders")
  fieldNumber := rapid.IntRange(1, columnNumber).Draw(t, "fieldNumber")
  fields := permutedHeaders[:fieldNumber]
  expectedNotEmptyNumber := (columnNumber - fieldNumber) * len(rows)
  result, err := GroupBy(fields, headers, rows)
  if err != nil {
    t.Fatalf("Unexpected error while grouping valid rows %v", err)
  }
  if rowsHaveSameLength(result) == false {
    t.Log("fields: ", fields)
    for i, row := range result {
      t.Log("i: ", i, " row length: ", len(row))
    }
    t.Fatalf("result should be have rows of the same length %v", result)
  }
}

func TestGroupByGivenValidInputReturnValidOutput(t *testing.T) {
  rapid.Check(t, testGroupByGivenValidInputReturnValidOutput)
}
```

> Je ne suis pas très satisfait de la manière dont les champs de groupement sont déterminés parmi les entêtes.

Le test échouait (ce qui est normal, je n'avais pas prévu dans mon code le cas où les groupements n'ont pas la même taille), j'ai donc modifié la fonction `groupRows`.

```go
func groupRows(rowsByGrouper map[string]KeysAndRows, groupedRowLength int) [][]string {
  var result [][]string
  for _, grouped := range rowsByGrouper {
    resultRow := grouped.keys
    notGroupedFieldsCount := 0
    for _, row := range grouped.rowsWithoutKeys {
      resultRow = append(resultRow, row[:]...)
      notGroupedFieldsCount += len(row)
    }
    for len(resultRow) < groupedRowLength {
      resultRow = append(resultRow, "")
      notGroupedFieldsCount++
    }

    result = append(result, resultRow)
  }
  return result
}
```

Avec cette modification, le test passait.

### Autres propriétés

J'ai ensuite ajouté le contrôle des deux autres propriétés (qui était déjà implémenté pour moi).

```go
func rowsAreUniqByGrouper(rows [][]string, grouperNumber int) bool {
  keys := make(map[string]int)
  for _, row := range rows[1:] {
    key := strings.Join(row[:grouperNumber], "@")
    _, exist := keys[key]
    if exist {
      return false
    }
    keys[key] = 1
  }
  return true
}

func countNotEmptyValues(rows [][]string, grouperNumber int) int {
  count := 0
  for _, row := range rows[1:] {
    for _, value := range row[grouperNumber:] {
      if value != "" {
        count = count + 1
      }
    }
  }
  return count
}

func testGroupByGivenValidInputReturnValidOutput(t *rapid.T) {
  rows := rapid.Custom(generateValidRows).Draw(t, "rows")
  columnNumber := len(rows[0])
  headers := rapid.SliceOfNDistinct(rapid.StringMatching(`[ a-zA-Z0-9_-]+`), columnNumber, columnNumber, rapid.ID[string]).Draw(t, "headers")
  permutedHeaders := rapid.Permutation(headers).Draw(t, "permutedHeaders")
  fieldNumber := rapid.IntRange(1, columnNumber).Draw(t, "fieldNumber")
  fields := permutedHeaders[:fieldNumber]
  expectedNotEmptyNumber := (columnNumber - fieldNumber) * len(rows)
  result, err := GroupBy(fields, headers, rows)
  if err != nil {
    t.Fatalf("Unexpected error while grouping valid rows %v", err)
  }
  if rowsHaveSameLength(result) == false {
    t.Log("fields: ", fields)
    for i, row := range result {
      t.Log("i: ", i, " row length: ", len(row))
    }
    t.Fatalf("result should be have rows of the same length %v", result)
  }
  if rowsAreUniqByGrouper(result, fieldNumber) == false {
    t.Fatalf("result should be have uniq rows by groupers %v", result)
  }
  if countNotEmptyValues(result, fieldNumber) != expectedNotEmptyNumber {
    t.Fatalf("count of not empty values should be %v", expectedNotEmptyNumber)
  }
}
```

Les tests passaient sans modifications du code : ce qui est attendu vu que mes tests par cas couvraient déjà ses propriétés.

## En résumé

Je retiens de cette séquence :

- Les idées de propriété peuvent venir *a posteriori* et raisonner en terme d'invariant aide ;
- La partie la plus compliquée est de définir et générer les données d'entrée ;
- Rapid permet de générer des données d'entrée ;
- Il me reste les cas d'erreur à coder ;
- J'ai envie de continuer les tests de propriétés en Go mais aussi en Typescript et en Python.

Voilà, j'espère que cela vous a plu. Si vous souhaitez partager des projets, contactez [moi sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/). J'accueille les merge requests sur [TournamentToolBox (https://www.gitlab.com/trambi/tournamenttoolbox)](https://www.gitlab.com/trambi/tournamenttoolbox) avec enthousiasme.

-----

Boite à outils pour tournoi :
[partie 1]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}),
[partie 2]({{< ref "/post/fr/2020-12-09-tournament-tool-box-part-2.md" >}}),
[partie 3]({{< ref "/post/fr/2020-12-28-tournament-tool-box-part-3.md" >}}),
[partie 4]({{< ref "/post/fr/2021-04-26-tournament-tool-box-part-4.md" >}}),
[partie 5]({{< ref "/post/fr/2021-06-01-tournament-tool-box-part-5.md" >}}),
[partie 6]({{< ref "/post/fr/2023-03-08-tournament-tool-box-part-6.md" >}})
