---
title: "Pourquoi il faut éviter d'utiliser Alpine comme base d'image Python"
date: "2024-07-16"
draft: false
description: "tout ça à cause d'une bibliothèque C"
tags: ["python","container","devops"]
author: "Bertrand Madet"
---

Trouver la bonne image de conteneur pour ses pipelines d'intégration continue est difficile. Il peut être tentant d'utiliser une image de base ultra populaire : [Alpine](https://hub.docker.com/_/alpine/). Cette image est très légère environ 8 Mio et est basée sur la distribution Linux du même nom. Mais j'ai pu constater qu'en général ce n'est généralement pas une bonne idée d'utiliser cette image pour les pipelines d'intégration continue Python. 

## Alpine

Pour être très légère, cette distribution Linux a fait des choix radicaux dont l'utilisation de la bibliothèque C standard : [musl libc](https://musl.libc.org/).

🤏 Il est intéressant de noter qu'il s'agit juste d'une **distribution Linux**. Contrairement aux autres distributions comme Ubuntu, Debian, Fedora ... qui sont en général considérées comme des **distributions GNU/Linux** car elles s’appuient sur les outils GNU.

Cette bibliothèque, `musl libc`, est minoritaire. La bibliothèque standard C la plus répandue parmi les distributions GNU/Linux est justement la [GNU libc](https://www.gnu.org/software/libc/).

## Python

Les bibliothèques Python intègrent souvent des éléments en C, C++ ou Rust. Par défaut, le logiciel Python d'installation, pip, utilise des paquets `wheel` pour distribuer des versions précompilées de ces éléments C, C++ ou Rust. 

Si le paquet `wheel` n'est pas disponible, on va télécharger les sources et les compiler. On comprend que : 

- cela simplifie l'installation car il n'y a pas besoin d'outils de compilation du langage ;
- cela accélère l'installation des paquets car il n'y a pas besoin de compiler les sources, juste de télécharger les binaires.

L'inconvénient est les binaires sont spécifiques à la version de Python, à l'architecture, au système d'exploitation, au type de bibliothèque standard C et à la version de la bibliothèque standard C de la cible 🤯.

❓ Pourquoi les binaires distribués dans les wheels dépendent de la bibliothèque standard C de la cible ? Pour éviter d'avoir à embarquer la bibliothèque standard C dans les binaires, on utilise un lien dynamique et au lieu d'embarquer les fonctions et structures de la bibliothèque standard C dans les binaires, les binaires "pointent" vers les fonctions de la bibliothèque du système.

## Wheel musllinux

Malheureusement, il y a beaucoup moins de paquets précompilés pour la bibliothèque `musl libc` que pour la bibliothèque `GNU libc`. Cela est probablement dû au fait que la création de paquets précompilés représente une charge supplémentaire pour les mainteneurs de bibliothèques Python. De plus, les wheels pour `GNU libc` sont apparus en 2016 alors que les wheels pour `musl libc` sont apparus en 2021.

🤏 Pour les distributions GNU/Linux utilisant `GNU libc`, le nom des wheels contient `manylinux`. Pour les distributions Linux utilisant `musl libc`, le nom des wheels contiendra `musllinux`. On pourra se reporter aux [PEP 600](https://peps.python.org/pep-0600/) et [PEP 656](https://peps.python.org/pep-0656/) pour les détails. 

Il y aussi que des wheels `musllinux` dans des [dépôts supplémentaires comme https://alpine-wheels.github.io/index/pandas/](https://alpine-wheels.github.io/index/pandas/) mais personnellement je ne trouve pas toujours ce dont j'ai besoin. 

## Conclusion

Ce manque de paquets précompilés rend nécessaire de recompiler les paquets depuis les sources. Cela prend du temps supplémentaire, temps qui sera pris à chaque déroulé de la pipeline d'intégration continue. Si on souhaite créer des images de conteneurs utilisables par l'intégration continue en fonction de bibliothèques Python, on risque de se perdre dans les différentes versions et combinaisons de bibliothèques Python installées sur les images d'intégration continue.

Voilà pourquoi, il faut mieux utiliser l'image officiel [Python (https://hub.docker.com/_/python)](https://hub.docker.com/_/python) ou les images RedHat UBI pour **U**niversal **B**ase **I**mage.

Si vous souhaitez partager sur Python et d'intégration continue, contactez moi [sur X](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169).
