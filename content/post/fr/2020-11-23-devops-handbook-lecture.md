---
title: "DevOps Handbook - Fiche de lecture"
date: "2020-11-23"
draft: false
description: "Livre qui explique le mouvement DevOps de Gene KIM, Jez HUMBLE, Patrick DEBOIS et John WILLIS"
tags: ["fiche de lecture","devops","Software Craftmanship"]
author: "Bertrand Madet"
---
*Fiche de lecture initialement publiée en juin 2020 sur LinkedIn et légèrement modifiée pour ce blog*

## A qui s'adresse ce livre

Aux personnes qui s'intéressent au systèmes d'information et services numériques et à toutes celles qui veulent comprendre ce que le DevOps. 

## Ce qu'il faut retenir

C'est un manuel complet sur le mouvement culturel et entrepreneurial qu'est le DevOps. Il vous accompagne de la découverte du concept aux détails de nombreux concepts en passant par une stratégie pour déployer le DevOps.

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: C'est un manuel complet qui va de l'introduction (partie I) aux détails de nombreux concepts en passant par une stratégie pour déployer le DevOps.| :-1: Trois cent cinquante (350) pages très denses avec certaines parties techniques qui pourraient en perdre certains.|
| :+1: Les parties II, III, IV, V fourmillent de bonnes pratiques intéressantes - au moins une initiative qu'on a envie de mettre en place par chapitre.| :-1: La première partie, introductive, peut sembler magique et incantatoire, `le DevOps va résoudre tous vos problèmes`...|
| :+1: La partie VI décrit la manière d'intégrer la cybersécurité dans le dispositif.||

Si vous êtes intéressé par le DevOps, c'est un livre de référence. Étant passionné par le sujet, je suis peut-être sujet au biais de confirmation. Ayant suivi une formation sur le DevOps, la partie d'introduction m'a paru manqué de substance et incantatoire. J'ai appris énormément de choses dans la suite, je dirais au moins un concept par chapitre et il y en a une vingtaine. La partie II `How to start` est une bonne initiative qui rend le DevOps accessible.

Il y a des concepts que j'aimerai plus creuser sur le fait de rendre la structure plus apprenante. D'autres dont j'aimerai débattre avec mes collègues comme le dépôt de code unique pour une entreprise - `one entreprise repository`.

La lecture de ce livre est enthousiasmante et donne envie de se lancer le mouvement à plein régime.

## Détails

- DevOps Handbook
- Langue : :gb: Anglais
- ISBN-13 : 978-1942788003
- Auteurs : Gene KIM, Jez HUMBLE, Patrick DEBOIS et John WILLIS
- https://itrevolution.com/book/the-devops-handbook/ 

Pas traduit :cry: en français :fr:.
