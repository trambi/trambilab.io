---
title: "Tester des composants avec des input date"
date: "2023-05-31"
draft: false
description: "en React mais cela doit marcher avec d'autres frameworks"
tags: ["front-end","reactjs","test"]
author: "Bertrand Madet"
---

Dans une récente test clinic, nous avons pas mal de difficulté avec un composant comprenant des champs de formulaire de type date. Je vais essayer de synthétiser le problème et les enseignements que j'en ai tiré.

## L'entrée de formulaire de type date

L'entrée de formulaire de type date s'écrit avec la balise `input` dont la propriété `type` vaut `date`. En react, on utilise la même syntaxe. Firefox rend le composant de cette manière :

![Input de type date sous Firefox](./input-type-date-in-firefox.png)

Chrome rend le composant d'une manière similaire. Le champ est plutôt complet, il est déjà possible de saisir la date où de sélectionner une date dans le calendrier.

![Sélection avec le calendrier](./input-type-date-calendar-french-in-firefox.png) et cela est localisé en fonction de la langue du navigateur, le même cas en anglais. ![Sélection avec le calendrier](./input-type-date-calendar-english-in-firefox.png)

C'est donc un composant bien pris en compte par le navigateur même sans JavaScript.

## Première tentative de test

Si on essaye de faire un composant simple qui contient juste un input de type date et le contenu texte de la date.

```typescript
import { useState } from "react";

interface MyDateInputProps {
    label: string,
    initialValue: string,
    onChange: (arg0: string) => void;
};

export const MyDateInput = (props:MyDateInputProps) => {
    const [value, setValue] = useState(props.initialValue);
    const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>)=>{
        props.onChange(event.target.value);
        setValue(event.target.value);
    }
    return (<>
        <label htmlFor="MyDateInput">{props.label}</label>
        <input type="date" id="MyDateInput" value={value} onChange={handleOnChange} />
        <span>Date is {value}</span>
    </>);
}
```

La première manière de tester serait d'écrire un test ressemblant à celui ci-dessous :

```typescript
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MyDateInput } from './';

describe('MyDateInput', () => {
  it('should display initial date', async () => {
    // GIVEN
    const value = '2023-01-01';
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    const handleChange = jest.fn((_arg0: string)=> {});
    const user = userEvent.setup();
    //  WHEN
    render(
        <MyDateInput label={'ForTest'} initialValue={value} onChange={handleChange} />
    );
    // THEN
    expect(
      await screen.findByText(`Date is ${value}`, {
        exact: false,
      })
    ).toBeInTheDocument();
    expect(screen.getByDisplayValue(value)).toBeInTheDocument();
    expect(handleChange).not.toBeCalled();
  });
  it('should display date when changed', async () => {
    // GIVEN
    const value = '2023-01-01';
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    const handleChange = jest.fn((_arg0: string)=> {});
    const user = userEvent.setup();

    render(
        <MyDateInput label={'ForTest'} initialValue={value} onChange={handleChange} />
    );
    const input = screen.getByLabelText('ForTest');
    //  WHEN    
    await user.click(input);
    // On veut changer le 1 janvier 2023 en 2 janvier 2023
    await user.keyboard(input,'02');
    // THEN
    expect(
        await screen.findByText(`Date is 01/02/2023`, {
          exact: false,
        })
      ).toBeInTheDocument();
    expect(screen.getByDisplayValue('01/02/2023')).toBeInTheDocument();
    expect(handleChange).toBeCalled();
  });
});
```

Le premier test fonctionne :white_check_mark: mais le deuxième échoue :x:.

## Deuxième tentative de test

### Étude du comportement

En étudiant le comportement du composant React dans le navigateur et avec Jest (principalement avec des `console.log` et en remplaçant le champ date en champ texte). On peut remarquer :

1. Le champ de type date n'appelle la callback `onChange` qu'une fois une date valide entrée ;
1. Le format attendu d'entrée de date dépend de la langue du navigateur et avec Jest est ISO-8601 - `YYYY-MM-DD` ;
1. La valeur du champ est toujours au format ISO-8601 ;
1. Dans Jest, le champ date ne s'efface pas au moment de la saisie.

### Modification de test

Basé sur ce comportement, il faut modifier le test pour effacer le champ avant la saisie de la date au format ISO-8601. On change le deuxième test ainsi :

```typescript
  it('should display date when changed', async () => {
    // GIVEN
    const value = '2023-01-01';
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    const handleChange = jest.fn((_arg0: string)=> {});
    const user = userEvent.setup();

    render(
        <MyDateInput label={'ForTest'} initialValue={value} onChange={handleChange} />
    );
    const input = screen.getByLabelText('ForTest');
    //  WHEN
    // On efface le champ
    await userEvent.clear(input);
    // On écrit la date cible au format ISO-8601
    await userEvent.type(input,'2023-01-02');
    // THEN
    expect(
        await screen.findByText(`Date is 2023-01-02`, {
          exact: false,
        })
      ).toBeInTheDocument();
    expect(screen.getByDisplayValue('2023-01-02')).toBeInTheDocument();
    expect(handleChange).toBeCalled();
  });
});
```

On utilise `userEvent.clear` pour vider le champ et `userEvent.type` avec le bon format - `YYYY-MM-DD`. Notre deuxième test passe :white_check_mark: !

## Conclusion

Il a fallu adapter le test pour qu'il convienne au champ date, cette adaptation pourrait passer une bidule, juste pour passer le test. Il faut alors repenser à ce en quoi on a confiance et ce que l'on teste. On a confiance dans l'implémentation du champ de type date par les navigateurs. On teste deux choses qu'on a bien utilisé un champ de type date et que la gestion de l'évènement `onChange` est correcte.

Si vous avez des conseils sur les tests de composants, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
