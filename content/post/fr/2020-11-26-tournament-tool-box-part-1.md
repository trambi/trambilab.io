---
title: "Boîte à outils pour tournoi - partie 1"
date: "2020-11-26"
draft: false
description: "Présentation de mon projet (en Go) du moment"
tags: ["projet perso","Go","jeu"]
author: "Bertrand Madet"
---

Boite à outils pour tournoi :
[partie 2]({{< ref "/post/fr/2020-12-09-tournament-tool-box-part-2.md" >}}),
[partie 3]({{< ref "/post/fr/2020-12-28-tournament-tool-box-part-3.md" >}}),
[partie 4]({{< ref "/post/fr/2021-04-26-tournament-tool-box-part-4.md" >}}),
[partie 5]({{< ref "/post/fr/2021-06-01-tournament-tool-box-part-5.md" >}}),
[partie 6]({{< ref "/post/fr/2023-03-08-tournament-tool-box-part-6.md" >}}),
[partie 7]({{< ref "/post/fr/2023-03-20-tournament-tool-box-part-7.md" >}})

----

*Modifié le 7 décembre 2020, pour reformuler le but.*
*Modifié le 10 mai 2021, pour améliorer la mise en forme.*

Aujourd'hui, je vais vous présenter mon projet personnel du moment : [TournamentToolBox (https://www.gitlab.com/trambi/tournamenttoolbox)](https://www.gitlab.com/trambi/tournamenttoolbox). Il s'agit d'un programme en ligne de commande pour gérer un tournoi. Et comme tous les projets perso, c'est plein d'opportunité d'apprendre des choses.

## Pourquoi ce projet ?

Je joue à un jeu de plateau, Blood Bowl (un jeu de football américain :football: avec des créatures fantastiques) depuis très longtemps (~1995 :astonished:). Je pratique entre autre ce jeu en tournoi, et j'ai eu la chance de participer à un gros tournoi plus de 1200 participants. L'organisation de cet évènement a été polluée par des problèmes logiciels.

## Le but

Je me suis dit qu'il manquait un utilitaire simple pour gérer un tournoi ou même servir en cas de problème. L'idée d'avoir un exécutable qui permet de générer :

1. le classement à partir des parties jouées;
2. l'appariement à partir des parties jouées.

Il faut que cela soit installable facilement.

Si je peux ensuite décliner les interfaces pour faire une application web ou une application graphique, ce serait un plus.

## Développement

### Le plan

Le plan était plutôt simple :

```mermaid
graph LR;
    games(Parties)-->rank[Calcul du classement]
    rank-->rankings(Classement)
    rankings-->draw[Appariement]
    games-->draw
    draw-->next_games(Prochaines parties)
```

### Mes choix

J'ai choisi [Go (https://golang.org/)](https://golang.org/) comme langage de programmation car :
1. Il est facile de compiler pour Windows depuis Linux (et même pour un MacOS) ;
1. Les exécutables générés sont sans dépendance donc très simple à installer ;
1. J'avais envie d'apprendre le Go :smile:.

Je suis parti sur une interface en ligne de commande pour garder en tête la simplicité.

### L'état actuel

```mermaid
graph LR;
    games(Parties)-->compute[Calcul des champs]
    settings(Configuration)-->compute
    compute-->games_with_fields(Parties avec des champs calculés)
    games_with_fields-->rank[Calcul du classement]
    settings(Configuration)-->rank
    rank-->rankings(Classement)
```

Je n'ai pas commencé à traiter le module d'appariement.

J'ai commencé par une première version du composant `rank` (qui s'occupe du calcul du classement). J'ai ensuite implémenté le composant `compute` (qui s'occupe des calculs des champs supplémentaires). La configuration du tournoi `settings` m'a pris pas mal de temps.

Il y a 7 modules Go :
`cmd/main` qui contient l'exécutable de la ligne de commande ;
`pkg/compute` qui gère l'ajout de nouveaux champs ;
`pkg/compute/settings` qui gère la configuration de l'ajout de nouveaux champs ;
`pkg/evaluator` qui sert à évaluer les expressions pour les nouveaux champs ;
`pkg/rank` qui gère la gestion du classement ;
`pkg/rank/settings` qui gère la configuration de la gestion du classement ;
`pkg/settings` qui représente la configuration du tournoi ;

```mermaid
graph TD;
    cmd[cmd/main]
    compute[pkg/compute]
    compute_settings[pkg/compute/settings]
    evaluator[pkg/evaluator]
    rank[pkg/rank]
    rank_settings[pkg/rank/settings]
    settings[pkg/settings]
    cmd-->compute-->evaluator
    cmd-->settings
    settings-->compute_settings
    settings-->rank_settings
    cmd-->rank
    rank-->rank_settings
    compute-->compute_settings
```

### Ce que j'ai appris

Ce projet m'a permis de mettre en pratique la documentation de Golang. J'ai appris beaucoup de choses. Voici les plus importantes à mon sens.

#### Go est simple à apprendre

Venant du C, je trouve que Go est simple à apprendre. La [documentation (https://golang.org/doc/)](https://golang.org/doc/) est bien faite et s'appuie sur un bac à sable pour s'approprier les notions aborder.

#### Go est un langage qui vient avec l'outillage

C'est très agréable quand on commence de ne pas devoir se poser la question : quel composant utiliser pour les tests unitaires ? Le langage Go vient avec des facilités pour les développeurs :

- `go test` pour les tests unitaires,
- `go fmt` pour la mise en forme des sources,
- `go build` pour la compilation sur les architectures avec les variables :
  - GOOS=linux et GOARCH=amd64 pour Linux sur processeur x64,
  - GOOS=windows GOARCH=amd64 pour Windows 64 bits,
  - env GOOS=darwin GOARCH=amd64 pour MacOSX (à voir comment cela se passera avec MacOS XI),
- le [bac à sable (https://play.golang.org/)](https://play.golang.org/) fait partie du site officiel.

#### La structure d'un projet Go

Lors de ma précédente expérience avec Go, j'avais eu du mal à structurer les fichiers. Pour ce projet, j'ai la structure de fichier décrite dans https://github.com/golang-standards/project-layout. Cela consiste à mettre le code pour l'exécutable dans le répertoire `cmd`, la documentation dans le répertoire `docs`, le code qui peut être réutilisable dans le répertoire `pkg`, les fichiers de test dans le répertoire `test` ...

Rien de révolutionnaire, mais cela permet de structurer les fichiers de manière attendue.

#### Le modèle "objet" de Go fonctionne

On peut pas définir de classes en Go. On peut définir des types, des fonctions associées aux types (qu'on peut appeler méthode), des interfaces mais il n'y a pas d'héritage. Et cela ne manque pas : "Préférez la composition à l'héritage".

#### Bonus : Extraire une structure de JSON

Il faut écrire une méthode `UnmarshalJSON` pour votre type qui a en entrée une tableau d'octet et renvoit une erreur.

```golang
func (mytype *MyType) UnmarshalJSON(b []byte) error
```

## La suite

Concernant le logiciel, j'aimerai :
1. ajouter une fonctionnalité au calcul du classement pour pouvoir prendre en compte la somme des points des adversaires.
2. ajouter la fonctionnalité d'appariement.
3. à lecture de Clean Architecture, réarchitecturer le code.

Voilà, j'espère que cela vous a plu. Si vous souhaitez partager des projets, contactez [moi sur Twitter](https://twitter.com/trambi_78). Et j'accueille les merge requests avec enthousiasme.

----

Boite à outils pour tournoi :
[partie 2]({{< ref "/post/fr/2020-12-09-tournament-tool-box-part-2.md" >}}),
[partie 3]({{< ref "/post/fr/2020-12-28-tournament-tool-box-part-3.md" >}}),
[partie 4]({{< ref "/post/fr/2021-04-26-tournament-tool-box-part-4.md" >}}),
[partie 5]({{< ref "/post/fr/2021-06-01-tournament-tool-box-part-5.md" >}}),
[partie 6]({{< ref "/post/fr/2023-03-08-tournament-tool-box-part-6.md" >}}),
[partie 7]({{< ref "/post/fr/2023-03-20-tournament-tool-box-part-7.md" >}})
