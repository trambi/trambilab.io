---
title: "Reprendre un projet après 3 ans - partie 1"
date: "2022-03-29"
draft: false
description: "Le plus simple est de ne pas arrêter"
tags: ["projet perso","php","jeu"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série :

- [le deuxième sur l'ajout de fonctionnalité]({{< ref "/post/fr/2022-04-13-restarting-a-project-after-three-years-part2.md" >}}) ;
- [le troisième sur le bilan sur le logiciel]({{< ref "/post/fr/2022-04-22-restarting-a-project-after-three-years-part3.md" >}}) ;
- [le quatrième sur les enseignements de l'expérience]({{< ref "/post/fr/2022-05-11-restarting-a-project-after-three-years-part4.md" >}}) ;
- [le cinquième sur la situation un an après]({{< ref "/post/fr/2023-06-07-restarting-a-project-after-three-years-part5.md" >}}) ;
- [le sixième sur la situation deux après]({{< ref "/post/fr/2024-05-22-restarting-a-project-after-three-years-part6" >}}).

J'ai repris un projet personnel après trois ans de pause. Si vous avez suivi mes [billets à propos des projets perso]({{< ref "/tags/projet-perso/" >}}), vous pouvez vous douter qu'il s'agit d'un logiciel en rapport au Bloodbowl. Ce logiciel a permis de gérer de nombreux tournois de notre association. Avec la pandémie, ce logiciel n'a pas été utilisé depuis juin 2019. La prochaine édition a lieu les 7 et 8 mai, j'ai donc commencé à retravailler sur le logiciel.

Dans cette partie, je vais me limiter à décrire mes efforts, mes tatonnements pour reconstruire le logiciel.

## Description du logiciel

Le logiciel permet de gérer le tournoi de notre association depuis 2003, il a été modernisé en 2015. L'architecture actuelle ressemble à ceci.

```mermaid
graph LR;
    users(Utilisateurs)-->front[Application Web]
    front-->api[API REST]
    orga(Organisateur)-->admin[Application d'administration web]
    api-->db[Base de données]
    admin-->db
```

Je parle d'un logiciel mais on voit qu'il s'agit de trois composants différents :

- L'application Web est une [application Angular sur GitHub (https://github.com/jmaaanu/FantasyFootballWebView)](https://github.com/jmaaanu/FantasyFootballWebView).
- L'administration et l'API REST sont des ["bundles" Symfony3 sur GitHub (https://github.com/trambi/FantasyFootball)](https://github.com/trambi/FantasyFootball)

En complément, la documentation, les fichiers Docker et les scripts de déploiement sont dans un troisième dépôt : <https://github.com/trambi/FantasyFootballArchitecture>. Cette partie permet, en autres,de déployer la solution sur des micro-ordinateurs RaspberryPi.

## Problèmes rencontrés

Avant parler des problèmes, une bonne surprise : le démarrage du RaspberryPi3 hébergeant le logiciel a permis d'accéder aux services. J'étais plutôt content car cela me donner un bon point de départ (et un point de repli en cas d'échec).

La reconstruction des images Docker pour RaspberryPi ("linux/arm") a échoué :

- pour la base de donnée MariaDB;
- pour l'administration et l'API REST.

La reconstruction de l'image Docker pour PC ("linux/amd64") a échoué également pour l'administration et l'API REST.

Cela signifie qu'à ce moment je pouvais juste lancer l'application mais pas la modifier.

## Résolutions

### Réparation de l'image de la base de données pour RaspberryPi

Le ficher de docker compose indiquait `image: "jsurf/rpi-mariadb"`. En fixant la version à "jessie", l'image fonctionnait à nouveau. Cette étape a été assez rapide.

### Réparation des images de l'administration et l'API REST

La construction des images échouait à la ligne `ADD https://symfony.com/installer /usr/local/bin/symfony` normal, l'installateur n'était plus présent sur le site web de Symfony. Il est possible de trouver un logiciel d'installation de Symfony mais pas pour la version 3 utilisée par le logiciel. Je suis tourné vers le projet composer `symfony/framework-standard-edition` qui permet de créer un projet Symfony 3.

Avec ce nouveau projet, il y avait des fichiers absents, il a fallu les ajouter en se basant sur l'image Docker de 2019 qui fonctionnait toujours.

Après ces modifications, l'image se construisait pour les architectures "linux/arm" et "linux/amd64".

Cette étape a été longue et j'ai dû tatonner entre la solution de Symfony et les différents squelettes de projet Symfony de composer.

## Conclusion

J'avais déjà commencé à fixer les versions dans mes Dockerfile. Le fait de fixer les versions des images de base est un comportement qui me paraît très sain pour un développement qui dure.

Pour la problématique de l'installation de Symfony, c'est dommage que l'url et la modalité de fonctionnement aient changé. En même temps, il y a eu trois (3) versions majeurs dans la période. Cela m'a permis d'utiliser Composer qui est la solution standard pour l'écosystème PHP.

J'ai profité de cette correction pour fusionner les dépôts de la documentation et de les sources des bundles Symfony pour simplifier le développement sur RaspberryPi.

Je suis en tout cas content que cette reconstruction car cela permet de débloquer l'ajout des fonctionnalités. J'essayerai de vous tenir au courant de mes avancées (cela se passe dans [la partie 2]({{< ref "/post/fr/2022-04-13-restarting-a-project-after-three-years-part2.md" >}})).

Si vous avez des conseils pour la pérennité dans le temps de logiciel, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

Si vous souhaitez contribuer, le dépôt est sur Github : <https://github.com/trambi/FantasyFootballArchitecture>.

Ce billet fait partie d'une série :

- [le deuxième sur l'ajout de fonctionnalité]({{< ref "/post/fr/2022-04-13-restarting-a-project-after-three-years-part2.md" >}}) ;
- [le troisième sur le bilan sur le logiciel]({{< ref "/post/fr/2022-04-22-restarting-a-project-after-three-years-part3.md" >}}) ;
- [le quatrième sur les enseignements de l'expérience]({{< ref "/post/fr/2022-05-11-restarting-a-project-after-three-years-part4.md" >}}) ;
- [le cinquième sur la situation un an après]({{< ref "/post/fr/2023-06-07-restarting-a-project-after-three-years-part5.md" >}}) ;
- [le sixième sur la situation deux après]({{< ref "/post/fr/2024-05-22-restarting-a-project-after-three-years-part6" >}}).


*Edité le 13/04/2022 : Changement de titre et légères modifications pour prendre en la publication de la partie 2*
