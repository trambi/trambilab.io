---
title: "Tests de composants React - partie 2"
date: "2021-12-08"
draft: false
description: "testing-library et mocks"
tags: ["Software Craftmanship","front-end","test","reactjs"]
author: "Bertrand Madet"
---

Cela fait un peu plus de dix mois que j'ai publié un [premier article sur les tests de composants React]({{< ref "/post/fr/2021-02-07-tests-react-components-part1/index.md" >}}). Voici un résumé de ce j'ai appris sur [Jest (https://jestjs.io)](https://jestjs.io) et [React Testing Library (https://testing-library.com/)](https://testing-library.com/) depuis cet article.

## Un super article de Kent C. Dodds

Première chose, j'ai trouvé un super [billet sur les erreurs communes (https://kentcdodds.com/blog/common-mistakes-with-react-testing-library)](https://kentcdodds.com/blog/common-mistakes-with-react-testing-library) de Kent C. Dodds, un des créateurs de la `testing-library`.

Où l'on apprend entre autre à utiliser :

- `screen` au lieu de la valeur de retour de la fonction render (appelée injustement `wrapper`) ;
- `findByText` au lieu de `waitFor`.

Par exemple pour vérifier l'apparition d'un message d'erreur

```typescript

describe('LoginForm', () => {
  it('should display error message when not submitting password', async () => {
    // Mise en place
    render(<LoginForm submitLabel="Valider"
        passwordError="Le mot de passe est obligatoire" />);
    userEvent.type(screen.findByRole('input',{name:'login'}), 'myLogin');
    // Declencheur
    fireEvent.click(screen.findByRole('button'));
    // Verification
    expect(await screen.findByText('Le mot de passe est obligatoire')).toBeInTheDocument();
  });
});

```

Je vous conseille grandement la lecture de cet article.

## Les cas compliqués

### La disparition d'un élément

Vérifier la disparition d'un élément n'est pas forcément intuitif à réaliser. Cela peut servir, par exemple, pour s'assurer qu'un message disparaît après une saisie correcte. On va utiliser la fonction `waitForElementToBeRemoved` (cf .Le guide de la disparition [(https://testing-library.com/docs/guide-disappearance)](https://testing-library.com/docs/guide-disappearance));

```typescript
await waitForElementToBeRemoved(() => screen.queryByText('Loading...'));
```

*Note* : Si on teste "juste" la non présence d'un élément, on peut utiliser les fonctions `queryBy*` de screen et la fonction de vérification `toBeInTheDocument` :

```typescript
expect(screen.queryByText('Le mot de passe est obligatoire')).not.toBeInTheDocument()
```

### Les composants traduits

Les composants traduits obligent à ruser, normalement en application sur le navigateur, les fichiers de traduction sont récupérés par requête http ou https du navigateur (le module i18next est `i18next-xhr-backend`).

Pour les tests avec jest, il faut :

- installer le module npm `i18next-fs-backend` ;
- créer une configuration de test qui utilise ce module.

L'idée est d'aller chercher les fichiers de traduction par le système de fichier plutôt que par le réseau comme le fera votre application sur le navigateur.

Ensuite dans les tests, il faudra permettre au composant de retrouver les fonctions de `i18n` en l'encadrant d'un élément React `Suspense` et de l'élément react-i18next `I18nextProvider`.

```typescript
// Fichier de test
import React, {Suspense} from 'react';
import i18n from '../i18next_test_configuration';
import {MonComposantTraduit} from './';

  // Plus tard dans le fichier de test
    render(<Suspense fallback="Translation not loaded...">
        <I18nextProvider i18n={i18n}>
          <MonComposantTraduit />
        </I18nextProvider>
      </Suspense>)
```

### Mocker ses propres modules

Cette information est plutôt simple mais j'ai eu beaucoup de mal à la trouver dans la documentation donc cela pourrait aussi être votre cas.

> :warning: En général, utiliser les mock ou les stub pour des tests unitaires est une fragilité car cela implique que le test connaît un détail d'implémentation du composant testé. Je préfère, en général, injecter via une prop la fonction que l'on veut débrayer.

Néanmoins, cela présente l'avantage de tester des composants graphiques qui appellent des API (REST ou GraphQL) sans dépendre de ces API (ce qui est la condition pour le test reste unitaire) et de faire varier les résultats de manière arbitraire et reproductible.

Passés ces avertissements d'usage, l'idée est d'utiliser la fonction `jest.mock` et de créer dans le répertoire du module à mocker un répertoire `__mocks__` contenant l'implémentation de la fonction de remplacement.

Par exemple si l'on veut utiliser la version de test de la fonction `login` située dans le fichier de chemin relatif `../utils/login/index.ts`. Il faut créer un fichier `index.ts` dans le répertoire `../utils/login/__mocks__`.

```typescript
// ../utils/login/__mocks__/index.ts
// Exemple d'implémentation de la fonction mockée login
export function login(id: string, password: string): 'ok'|'unknown_id'|'incorrect_password' {
  if(id === 'unknown'){
    return 'unknown_id';
  }else if(password === 'passwordOk'){
    return 'ok';
  }
  return 'incorrect_password';
}
```

Pour le test, on utilisera `jest.mock`

```typescript
// Fichier de test
// imports du test ...
// (...)
//
import { login }  from '../utils/login';
jest.mock('../utils/login'); 
// dans ce fichier la fonction login sera la fonction de test
// ... suite du fichier de test
```

En parlant d'injection, cela peut être un bon moyen d'assurer du bon fonctionnement d'un module avant de faire un refactoring pour injecter la fonction problématique.

## Conclusion

Le fait de devoir écrire les tests en premier (en **TDD** par exemple), peut être très frustrant au début. Mais en faisant attention à la qualité des tests, cela devient plus facile et le niveau de confiance dans les tests augmente.

J'espère que ce partage d'expériences vous aidera à implémenter des tests plus précis et à améliorer ceux existant. Si vous avez des retours ou d'autres conseils sur les tests, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
