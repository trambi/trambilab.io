---
title: "Une brève histoire des conteneurs"
date: "2021-10-27"
draft: false
description: "ou pourquoi les conteneurs nous facilitent la vie"
tags: ["devops","container"]
author: "Bertrand Madet"
---

Dans le billet précédent, je faisais un [point sur ma compréhension de Kubernetes]({{< ref "/post/fr/2021-10-18-a-propos-kubernetes.md" >}}). Mais il manquait une notion importante, le conteneur. Aussi je vais essayer de réparer cet oubli en vous narrant une brève et incomplète histoire des conteneurs.

## Un serveur par composant

Il y a fort longtemps, on installait un (au minimum) serveur par composante d'un service. Si on avait un service constitué d'un serveur web, d'un back-office et d'une base de données, on se retrouvait au minimum avec la configuration ci-dessous.

```mermaid
graph LR
subgraph "Serveur front-end"
  web[serveur Web]
end
subgraph "Serveur back-end"
  back[service back-office]
end
subgraph "Serveur BD"
  db[(Base de données)]
end
web--->back
back-->db
```

Pourquoi procédions-nous de cette manière ? Une des raisons est que cela permettait de gérer des conflits de dépendances. Par exemple, si mon serveur Web nécessite un système d'exploitation spécifique et que mon service back-office a besoin d'une bibliothèque spécifique. 

Que se passe-t-il si la bibliothèque nécessaire à mon service back-office n'est pas disponible sur le système d'exploitation de mon serveur web ? Il faut soit :

- utiliser un autre serveur Web;
- utiliser une autre bibliothèque;
- installer les services sur de serveurs physiques différents.

L'exemple est peu extrême mais on peut aussi imaginer que les services fonctionnent avec deux versions différentes d'une même bibliothèque.

## Les machines virtuelles à la rescousse

L'approche une machine physique par serveur est chère et nécessite de gérer plein de machines même pour un petit service. Dans la fin de années 1990, la [virtualisation système (https://fr.wikipedia.org/wiki/Virtualisation)](https://fr.wikipedia.org/wiki/Virtualisation) permet de répondre partiellement aux problématiques. Le principe est d'émuler plusieurs machines virtuelles et de leur faire exécuter des applications avec leurs systèmes d'exploitation.

- :+1: L'isolation est presque complète (il existe des failles de sécurité) ;
- :+1: Cela permet d'optimiser l'utilisation des processeurs et de la mémoire dynamiquement ;
- :-1: On fait tourner un système d'exploitation pour faire tourner plein de système d'exploitation, d'où un surcoût certain.

## L'avènement de Docker

La plupart des systèmes d'exploitation fournissent des mécanismes d'isolation :

- Les systèmes d'exploitation BSD fournissent Jail depuis 2000 ;
- Le noyau Linux fournit différent mécanismes d'isolation au niveau du système de fichier, du réseau, des processus ...

Docker permet d'exploiter de manière simplifiée et déclarative les mécanismes d'isolation de Linux (et de Windows) avec un fichier texte : Le `Dockerfile`.

```Dockerfile
FROM nginx:1.21.3-alpine
COPY static-html-directory /usr/share/nginx/html
```

Le fichier ci-dessus prépare une image Nginx utilisant la distribution alpine avec les fichiers situés dans un répertoire (`static-html-directory` en l'occurrence). Si j'ai nommé cette image `[nom-de-mon-image]`, je peux lancer ce processus avec la commande ci-dessous.

```bash
docker run -d -p 80:80 [nom-de-mon-image]
```

Ces mécanismes d'isolation limitent avantageusement le surcoût d'utilisation du processeur et de la mémoire. Il n'y a un qu'un seul système d'exploitation qui s'exécute. L'espace de stockage utilisé peut aller de quelques MiO à un GiO là où la virtualisation va aller de quelques centaines de MiO à une centaine GiO. Le temps de lancement d'un conteneur est de l'ordre de la seconde ou le temps d'une machine virtuelle est de l'ordre d'une minute.

## L'après Docker

Avec la généralisation du système d'exploitation GNU/Linux qui a l'avantage d'être libre et stable, on peut dire que Docker a été un carton. La conteneurisation permet une flexibilité dans le déploiement incroyable. Mais l'approche n'était exempte de défauts et des alternatives plus adaptées aux enjeux de compatibilité, de sécurité sont apparus sur les épaules de Docker :

- [containerd (https://containerd.io/)](https://containerd.io/) ;
- [cri-o (https://cri-o.io/)](https://cri-o.io/) ;
- [podman (https://podman.io/)](https://podman.io/).

Les avantages des conteneurs sont donc :

- l'isolation des processus et du système de fichier associé ;
- la légèreté ;
- la possibilité de figer une version à partir d'un fichier texte.

## Conclusion

Voici une histoire incomplète des conteneurs qui permet je l'espère de comprendre les avantages des conteneurs. Et surtout que cela vous aura donner envie d'en apprendre plus et d'essayer.

Si vous avez des retours sur les conteneurs ou d'autres outils de déploiement, n'hésitez pas à les partager sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
