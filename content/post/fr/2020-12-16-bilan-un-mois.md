---
title: "Bilan au bout d'un mois"
date: "2020-12-16"
draft: false
description: "Avec peu de recul donc"
tags: ["blog"]
author: "Bertrand Madet"
---

Avec un premier mois de publication de mon blog, voici un petit bilan sur les publications et sur le site. 

## Chiffres

15 billets publiés dont :

- 9 inédits, 5 adaptions de précédentes publications et 1 traduction ;
- 12 liés à la technique et 3 non techniques ;
- 4 fiches de lectures ;
- 2 sur mon projet perso.

## Rythme

Pour l'instant, je suis à un peu plus de deux billets par semaine, j'aimerai rester sur cette moyenne (en veillant à ce que cela ne devienne pas une corvée). Un billet le lundi (ou le dimanche soir) et un billet le jeudi (ou le mercredi) cela me semble raisonnable et m'incite à capitaliser ce que j'apprend au jour le jour. 

Si vous voulez lire un blog de quelqu'un qui publie un billet par jour, [Flavio Copes (https://flaviocopes.com/)](https://flaviocopes.com/) fait ça très bien.

## Modifications du site

Après avoir installé Mermaid.js (je vous ai dit que [j'adorais Mermaid.js]({{< ref "/post/fr/2020-12-13-mermaid-js.md" >}}) :wink:), j'ai modifié le détail du site de `où l'on parle de développement logiciel` à `Où l'on parle des aventures d'un développeur`, cela me semble plus en accord avec les billets que je publie.

J'ai ajouté le partage par LinkedIn des billets, on peut donc partager sur Twitter et LinkedIn facilement les billets. Et j'ai ajouté un lien vers mon utilisateur LinkedIn. Cela me semblait logique dans la mesure où je publie des messages sur ces deux réseaux sociaux.

J'ai ajouté un lien vers le billet [Pourquoi vous devriez vous intéresser aux licences de vos logiciels ?]({{< ref "/post/fr/2020-11-14-pourquoi-vous-devriez-vous-interesser-aux-licences-de-vos-logiciels.md" >}}) dans la barre de navigation car ce billet me semble important.

## Conclusion

Le flux de publication avec GitLab est super naturel, et Hugo et le thème GhostWriter fonctionnent bien, je suis donc content de ce premier mois. Si vous avez des préférences concernant mes billets passés ou futurs n'hésitez pas à m'en faire part sur [Twitter](https://twitter.com/trambi_78) ou [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
