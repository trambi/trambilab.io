---
title: "Utiliser Advent of Code pour s'entrainer au TDD - partie 1"
date: "2023-12-09"
draft: false
description: "avec les jours 1 et 2"
tags: ["tdd"]
author: "Bertrand Madet"
---

Le mois de décembre est l'occasion de (dans le désordre) :

- manger des chocolats 🍫;
- passer du temps en famille 👨‍👩‍👧‍👧;
- d'ouvrir des calendriers de l'avent 🎁.

Je vous propose de profiter d'un excellent [calendrier de l'avent du code par Eric Wastl (https://adventofcode.com/)](https://adventofcode.com/) pour nous entraîner au [TDD]({{< ref "/tags/tdd">}}).

Advent of Code (ou `AoC`) propose pour chaque jour du 1 décembre au 25 décembre, une énigme à résoudre avec un petit programme. C'est devenu un vrai institution. Cette année les énigmes que j'ai pris le temps de regarder sont bien équilibrées : ni trop faciles ni trop dures. En particulier, celles des jours 1 et 2 peuvent être des bons sujets pour un [coding dojo]({{< ref "/tags/dojo">}}).

Comme en général en TDD, l'étape la plus dure est d'écrire les tests, je vous propose des tests pour se lancer dans le TDD. Les tests seront décrits sous forme de tableau et on utilisera `each` avec jest (pour JavaScript ou TypeScript) ou `@pytest.mark.parametrize` avec pytest (pour Python). L'idée est de décommenter au fur à les cas de tests. Pour appliquer la démarche :

- le dernier test (à être décommenté) échoue ;
- on écrit le minimum de code pour que tous les tests passent ;
- tous les tests passent.

## Jour 1

L'épreuve du [jour 1](https://adventofcode.com/2023/day/1) consiste à extraire le premier chiffre et le dernier chiffre de chaque ligne d'un texte et d'en faire un nombre et d'additionner les nombres de chaque ligne.

Pour la première partie, les exemples fournis dans la description de l'épreuve sont suffisants.

|#|Fonction|Entrée|Sortie attendue|Explications|
|-|--------|------|---------------|------------|
|1|extractFirstAndLastDigitsFromLine|'1abc2'|12|Le premier chiffre au début, le dernier et deuxième à la fin|
|2|extractFirstAndLastDigitsFromLine|'pqr3stu8vwx'|38|Deux chiffres entourés de lettres|
|3|extractFirstAndLastDigitsFromLine|'a1b2c3d4e5f'|15|Trois chiffres entourés de lettres|
|4|extractFirstAndLastDigitsFromLine|'treb7uchet'|77|Un seul chiffre entouré de lettres|
|5|sumForLinesPart1|['1abc2', 'pqr3stu8vwx', 'a1b2c3d4e5f', 'treb7uchet']|142|On s'assure que sumForLinePart1 utilise la fonction précédente|

La partie 2 est plus subtile, on va reprendre les exemples de la partie 2 et il faudra ajouter trois autres cas de test.

|#|Fonction|Entrée|Sortie attendue|Explications|
|-|--------|------|---------------|------------|
|1|extractFirstAndLastOneToNineFromLine|'two1nine'|29|Le premier mot au début, le dernier à la fin et l'extraction de 2 et 9|
|2|extractFirstAndLastOneToNineFromLine|'eightwothree'|83|Le premier mot au début, le troisième et dernier  à la fin et l'extraction de 3 et 8|
|3|extractFirstAndLastOneToNineFromLine|'abcone2threexyz'|13|Le premier mot et le dernier mot dans l'entrée  et l'extraction de 1|
|4|extractFirstAndLastOneToNineFromLine|'xtwone3four'|12|L'extraction de 4|
|5|extractFirstAndLastOneToNineFromLine|'4nineeightseven2'|12|Le fonctionnement avec les chiffres mêmes quand les mots sont là|
|6|extractFirstAndLastOneToNineFromLine|'zoneight234'|14|L'utilisation de premier chiffre et du dernier mot|
|7|extractFirstAndLastOneToNineFromLine|'7pqrstsixteen'|76|L'extraction de 6|
|8|sumForLinesPart2|['two1nine', 'eightwothree', 'abcone2threexyz', 'xtwone3four, '4nineeightseven2', 'zoneight234','7pqrstsixteen']|281|sumForLinePart2 utilise la fonction précédente|
|9|extractFirstAndLastOneToNineFromLine|'five'|55|L'extraction de 5|
|10|extractFirstAndLastOneToNineFromLine|'seven'|77|L'extraction de 7|
|11|extractFirstAndLastOneToNineFromLine|'eighthree'|83|Le premier mot et le dernier sont intriqués|

Le dernier cas de test permet de régler le cas non testé jusqu'alors d'une ou plusieurs lettres communes entre le premier et le dernier mot.

## Jour 2

L'épreuve du [jour 2](https://adventofcode.com/2023/day/2) tourne autour d'un jeu de tirage de boule rouges, vertes et bleues.

La partie 1 s'intéresse à la possibilité de tirage avec un ensemble déterminé de boules : 12 boules rouges, 13 boules vertes et 14 boules bleues. J'ai découpé plus finement pour simplifier les tests et les codes. Et je n'ai utilisé les cas d'exemple que pour valider la fonction globale.

|#|Fonction|Entrée|Sortie attendue|Explications|
|-|--------|------|---------------|------------|
|1|isSetPossible|'12 red'|true|Extrait le nombre de boules rouges pour une session|
|2|isSetPossible|'13 red'|false|S'assure de renvoyer faux avec plus de 12 boules rouges|
|3|isSetPossible|'13 green'|true|Extrait le nombre de boules vertes pour une session|
|4|isSetPossible|'14 green'|false|S'assure de renvoyer faux avec plus de 13 boules vertes|
|5|isSetPossible|'14 blue'|true|Extrait le nombre de boules bleues pour une session|
|6|isSetPossible|'15 blue'|false|S'assure de renvoyer faux avec plus de 14 boules bleues|
|7|isSetPossible|'5 blue, 4 red, 14 green'|false|Fonctionne bien avec du rouge, du bleu et vert|
|8|resultForLinePart|'Game 7: 12 red; 13 green'|7|Extrait l'identifiant du jeu|
|9|resultForLinePart|'Game 12: 13 red; 13 green'|0|resultForLinePart utilise la fonction isSetPossible|
|10|sumForFilePart1|['Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green', 'Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue', 'Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red','Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red','Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green']|8|sumForLinePart1 utilise la fonction précédente et additionne les résultats.|

La partie 2 s'intéresse aux nombres minimum de boules de chaque couleur pour chaque partie.

|#|Fonction|Entrée|Sortie attendue|Explications|
|-|--------|------|---------------|------------|
|1|minimumNumberForASet|'5 blue, 4 red, 14 green'|{blue: 5, green: 14, red: 4}|Extrait le minimum de boules pour une session|
|2|minimumNumbersForALine|'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green'|{blue: 5, green: 14, red: 4}|Extrait le minimum de boules pour une ligne|
|3|sumForFilePart2|['Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green', 'Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue', 'Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red','Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red','Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green']|2286|sumForLinePart2 utilise la fonction précédente, multiplie les minimum pour une ligne puis additionne les résultats.|

## Conclusion

J'espère que cela vous aura aidé à pratiquer le TDD sur l'Advent of Code. Il reste encore 23 épreuves pour trouver vos tests, coder et s'amuser. Robert Martin `Uncle Bob` - l'auteur de Clean Code - fournit ses solutions chaque jour en [Clojure sur GitHub (https://github.com/unclebob/AOC23)](https://github.com/unclebob/AOC23). Vous pouvez regarder ces cas de test.

Si vous le voulez, partagez votre progression à l'Advent of Code ou sur le TDD avec moi [sur X](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
