---
title: "SSH et passage à Fedora 33"
date: "2020-12-02"
draft: false
description: "Et si on laissait la sécurité activée pour une fois"
tags: ["ssh","fedora","cybersec"]
author: "Bertrand Madet"
---

J'ai mis à jour ma machine avec Fedora 33, et depuis impossible de me connecter en ssh sur des machines distances. En particulier, impossible de synchroniser mes développements sur GitLab ou GitHub.

En cherchant sur [DuckDuckGo](https://duckduckgo.com/), je tombe sur ce sujet StackOverflow [ssh Permission denied (publickey) after upgrafe Fedora 33 (https://stackoverflow.com/questions/64640596/ssh-permission-denied-publickey-after-upgrafe-fedora-33)](https://stackoverflow.com/questions/64640596/ssh-permission-denied-publickey-after-upgrafe-fedora-33). Quelqu'un a eu le problème, la réponse validée indique qu'il y a un changement dans la [configuration de sécurité de base de Fedora (https://fedoraproject.org/wiki/Changes/StrongCryptoSettings2)](https://fedoraproject.org/wiki/Changes/StrongCryptoSettings2) et qu'il y a deux manières de résoudre le problème soit en remettant la sécurité au niveau de Fedora 32 ou en créant une clé de type "ed25519" (et que c'est la solution préférable). La réponse est bien :+1:. Ce qui me choque ensuite c'est la réponse la plus votée, dit qui en synthétisant, `tu lances cette commande et merci pour l'article `, oui la commande qui ramène la sécurité au niveau de Fedora 32 (sans expliquer les tenant et les aboutissements):fearful:.

![Security bypass](https://blog.rootshell.be/wp-content/uploads/2013/03/gate-bypass.jpg)

C'est comme l'image ci-dessus mais en pire car on dégrade la sécurité de son propre ordinateur.

Pour ma part, je considère que les personnes impliquées dans la sécurité de Fedora, sont plus calées que moi en cybersécurité et que la sécurité vaut bien le temps de changer d'une clé SSH. Du coup, j'ai changé ma clef SSH en RSA 2048 bits pour une nouvelle clé SSH de type `ed25519`.

Ah oui, `ed25519`, c'est une variante de cryptographie basée sur les courbes elliptiques : https://fr.wikipedia.org/wiki/EdDSA.

C'est un peu comme quand on a un problème avec [AppArmor (https://apparmor.net/)](https://apparmor.net/) ou [SELinux (https://selinuxproject.org/page/Main_Page)](https://selinuxproject.org/page/Main_Page) sur un service, c'est tentant de désactiver la protection. Sauf qu'au final, on dégrade la sécurité, on prend la mauvaise habitude de le faire et pire, on n'apprend pas comment configurer ses outils. 

Donc la prochaine fois que j'ai un problème avec SELinux (c'est vrai que je trouve cet outil plutôt cryptique), je vais lire la documentation. Et vous, quel est votre réflexe vis-à-vis de la cybersec ? Discutons-en sur [Twitter](https://twitter.com/trambi_78).