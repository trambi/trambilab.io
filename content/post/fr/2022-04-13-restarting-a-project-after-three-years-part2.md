---
title: "Reprendre un projet après 3 ans - partie 2"
date: "2022-04-13"
draft: false
description: "Où on essaye d'ajouter des fonctionnalités"
tags: ["projet perso","php","jeu"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série :

- [le premier sur les étapes pour réussir à reconstruire le logiciel]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years.md" >}}) ;
- [le troisième sur le bilan sur le logiciel]({{< ref "/post/fr/2022-04-22-restarting-a-project-after-three-years-part3.md" >}}) ;
- [le quatrième sur les enseignements de l'expérience]({{< ref "/post/fr/2022-05-11-restarting-a-project-after-three-years-part4.md" >}}) ;
- [le cinquième sur la situation un an après]({{< ref "/post/fr/2023-06-07-restarting-a-project-after-three-years-part5.md" >}}) ;
- [le sixième sur la situation deux après]({{< ref "/post/fr/2024-05-22-restarting-a-project-after-three-years-part6" >}}).

Il y a deux semaines, j'ai repris un projet personnel qui je n'avais pas touché depuis 3 ans. Cette fois-ci, on s'attaque à la mise en place d'un environnement de développement pour la correction d'un bug et l'ajout d'une nouvelle fonctionnalité.

## Résumé de l'épisode précédent

Le logiciel permet de gérer le tournoi de notre association depuis 2003, il a été modernisé en 2015. L'architecture actuelle ressemble à ceci.

```mermaid
graph LR;
    users(Utilisateurs)-->front[Application Web]
    front-->api[API REST]
    orga(Organisateur)-->admin[Application d'administration web]
    api-->db[Base de données]
    admin-->db
```

Je parle d'un logiciel mais on voit qu'il s'agit de trois composants différents :

- L'application Web est une [application Angular sur GitHub (https://github.com/jmaaanu/FantasyFootballWebView)](https://github.com/jmaaanu/FantasyFootballWebView);
- L'application d'administration et l'API REST sont des ["bundles" Symfony3 sur GitHub (https://github.com/trambi/FantasyFootballArchitecture)](https://github.com/trambi/FantasyFootballArchitecture).

A la fin, j'arrivai à recréer et à déployer la solution sur des micro-ordinateurs RaspberryPi ou des PC sous GNU/Linux.

## Mise en place de l'environnement de développement

Je cherchais une configuration qui me permettait :

- de développer depuis GNU/Linux et Windows ;
- de gérer mes versions sans copie manuelle ;
- de lancer des tests le plus rapidement possible.

### Développer et exécuter dans le conteneur

L'idée est d'utiliser l'extension `Remote - Container` de Visual Studio Code pour développer directement dans l'image Docker.

Cela permettait de développer sous GNU/Linux et sous Windows (:warning: cela nécessite d'avoir Docker Desktop et WSL2 - il me semble).

Pour la gestion de version, je n'ai pas trouvé de manière simple. J'ai essayé des montages de volumes sur mes images Docker mais je me suis heurté à des problèmes de droit sur les répertoires.

Comme la gestion de version est importante pour moi ([cf. L'indispensable gestion de version]({{< ref "/post/fr/2022-02-27-indispensable-gestion-version.md" >}})), j'ai abandonné cette solution :no_entry:.

### Développer et exécuter sur sa machine

L'idée est de procéder comme le fichier Docker opère mais au lieu de faire des copies, de faire des liens symboliques (pour gérer les versions sans copie manuelle). Il faut donc installer PHP, Apache2 et MariaDB :cry:. Sous Windows, j'ai utilisé WSL2.

Cela m'a permis de faire fonctionner l'API REST et l'application d'administration et de lancer les tests. Mais le test de la solution en entier nécessite plus d'opérations manuelles.

### Développer sur sa machine et exécuter dans le conteneur

Sur Windows, je n'ai pas essayé de lancer la construction d'images Docker. Je suis passé par l'extension `Remote -SSH` de Visual Studio Code pour développer sur Linux et créer les images Docker à partir de là.

Cela nécessite un peu plus de temps mais cela permet de tester la construction d'image Docker et facilite d'autres tests plus haut niveau.

## Correction d'un bug

Le problème se manifestait par un fonctionnement hasardeux d'un import de fichier CSV.

J'ai développé et exécuté sur sa machine. Après avoir fouillé la document de Symfony 3 pour trouver comment lancer les tests, je me suis aperçu que les tests fonctionnels ne passaient pas. Les tests unitaires eux passaient.

J'ai commencé par modifier au minimum le code pour avoir un test unitaire qui couvre le fonctionnement initial - en extrayant la partie de traitement du contrôleur.

J'ai ensuite modifié le test unitaire pour faire échouer à la manière du bug, le traitement d'un fichier CSV. Une fois que j'avais le test échoué, j'ai modifié le code pour corriger le problème.

J'ai ensuite testé manuellement que cela corrigeait le problème initial (c'est à dire que la manière dont j'avais synthétisé le bug dans mon test unitaire était correcte).

Je vous propose plus de détails sur la méthode de débogage que j'utilise [dans Déboguer en 3 phrases]({{< ref "/post/fr/2021-09-27-debugging-en-3-phrases.md" >}}).

J'ai commité puis j'en ai profité pour rapprocher l'arborescence de mes fichiers source de l'arborescence des fichiers déployés.

### L'ajout d'une sauvegarde de base de données

La solution prévoyait de faire les sauvegardes de la base de données en ligne de commande. L'utilisation des conteneurs a un peu complexifié la tâche de l'administrateur du tournoi, qui a d'autres choses à faire que saisir des lignes de commandes avec des mots de passe et des identifiants de conteneurs Docker.

L'idée était de faire un lien pour télécharger une sauvegarde de la base de donnée.

J'avais déjà les entités et les registres Symfony pour gérer les objets métiers et leur persistance. Comme Symfony3 utilise le [motif d'architecture MVC](https://fr.wikipedia.org/wiki/Mod%C3%A8le-vue-contr%C3%B4leur), j'ai ajouté une action dans un contrôleur et une route associée à cette action et une vue.

Je ne suis pas super satisfait de l'ajout de cette fonctionnalité car c'est assez difficile à tester automatiquement. Oui car un test de sauvegarde implique souvent un test de restauration - cf. [Échec de sauvegarde]({{< ref "/post/fr/2021-09-09-backup-failure.md" >}}) - et cette fonctionnalité n'est pas présente dans la solution.

## Conclusion

Cette étape m'aura permis de me rendre compte :

- que mon environnement de développement n'est pas satisfaisant pour PHP, j'évolue entre exécuter sur sa machine et exécuter dans un conteneur;
- **qu'il faut toujours documenter comment lancer les tests d'un projet**.

Cela m'a donné l'idée des évolutions fonctionnelles comme l'ajout des fonctions d'export et d'import. Je pense que j'utiliserai un format plus commun le CSV, cela me permettra de jeter des passerelles avec mon autre projet perso, [Boîte à outils pour tournoi]({{< ref "/post/fr/2020-11-26-tournament-tool-box-part-1.md" >}}).

Si vous avez des conseils pour la pérennité dans le temps de logiciel, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

Si vous souhaitez contribuer, le dépôt est sur Github : <https://github.com/trambi/FantasyFootballArchitecture>.

Ce billet fait partie d'une série :

- [le premier sur les étapes pour réussir à reconstruire le logiciel]({{< ref "/post/fr/2022-03-29-restarting-a-project-after-three-years.md" >}}) ;
- [le troisième sur le bilan sur le logiciel]({{< ref "/post/fr/2022-04-22-restarting-a-project-after-three-years-part3.md" >}}) ;
- [le quatrième sur les enseignements de l'expérience]({{< ref "/post/fr/2022-05-11-restarting-a-project-after-three-years-part4.md" >}}) ;
- [le cinquième sur la situation un an après]({{< ref "/post/fr/2023-06-07-restarting-a-project-after-three-years-part5.md" >}}) ;
- [le sixième sur la situation deux après]({{< ref "/post/fr/2024-05-22-restarting-a-project-after-three-years-part6" >}}).
