---
title: "Astuces sur Terraform - 1"
date: "2023-09-13"
draft: false
description: "quand cela déraille un peu"
tags: ["cloud","outils","terraform"]
author: "Bertrand Madet"
---

La plupart du temps, l'utilisation de Terraform se passe bien : les ressources sont décrites, le déploiement continu déploie les modifications d'infrastructure. Mais il arrive parfois que cela déraille à cause d'un problème de démarche ou de déploiement continu. Je vous propose aujourd'hui, les astuces que j'ai découvert récemment sur Terraform.

## Verrou sur l'état de Terraform

Pour gérer votre infrastructure Terraform maintient un état de l'infrastructure. Cet état est soit dans un fichier local soit à distance par exemple dans un objet d'un stockage S3. Pour éviter que deux processus Terraform ne modifie l'état de l'infrastructure (et l'infrastructure elle-même) en même temps, il y a un système de verrou :lock:. Dans mon cas personnel, ce verrou se concrétise pour un enregistrement dans une table DynamoDB. Il arrive parfois qu'une commande Terraform échoue à cause d'un verrou sur l'état.

Dans ces cas, mon premier conseil est de regarder si d'autres processus Terraform sont en train d'agir le même état :

- si oui :+1: :
  - d'atteindre qu'ils se terminent ,
  - ou de lancer la commande avec l'option `-lock-timeout` suivie de la valeur en seconde avant de recommencer - par exemple `-lock-timeout=10s`,
- s'il y n'a pas d'autres processus Terraform, on entre dans une zone dangereuse :warning: :
  - soit exécuter la commande avec l'option `-lock=false` pour ne pas prendre en compte le verrou,
  - soit :rotating_light: supprimer le verrou, cela dépend de la manière dont le système de verrou est fait. Dans mon cas, on peut supprimer l'enregistrement dans la table DynamoDB.

## Importer la configuration d'une ressource

Il peut arriver que l'on ait élaboré la configuration d'une ressource avec l'interface d'administration web ou la ligne de commande spécifique du fournisseur de ressources. Car, pour AWS en tout cas, les documentations officielles ne mentionnent pas Terraform et que cela permet une mise au point plus rapide de la ressource. Pour ne pas perdre le côté description d'infrastructure dans le code, on peut utiliser la commande `import` après l'initiation (la commande `init`). La commande `import` utilise les mêmes options de variables que les commandes `plan` ou `apply`.

Pour cela, il faut :

0. Être dans un contexte de de Terraform initialisé avec la commande `terraform init` avec les options habituelles ;
1. Identifier la ressource à importer et l'identifiant associé ;
2. Créer un bloc ressource vide dans un fichier de description `hcl` ;
3. Importer la ressource avec la commande `terraform import`, les options habituelles de `terraform plan` ou `terraform apply` le nom de la ressource et l'identifiant ;
4. Afficher l'état de la ressource avec la commande `terraform state show` et le nom de la ressource ;
5. Copier la sortie de l'étape précédente dans le fichier de description `hcl` de l'étape 2.

> :warning: Si on s'arrête à l'étape 3, on a bien importé la configuration mais on perd la traçabilité de cette configuration.

L'import du bucket S3 dans mon fichier de description se manifestera par l'ajout du code HCL ci-dessous.

```hcl
# Fichier de description avant l'import
resource "aws_s3_bucket" "mybucket" {
}
```

Si je n'ai pas d'option particulières, l'exécution des commandes suivantes permettra d'obtenir l'état de la ressource importée.

```bash
terraform import aws_s3_bucket.mybucket idofmybucket
terraform state show aws_s3_bucket.mybucket
```

On peut ensuite s'en inspirer pour garder une trace de la configuration dans le fichier de description de l'infrastructure.

## Conclusion

J'espère que ces deux astuces vous permettront de vous sortir de situations qui peuvent être décourageantes avec Terraform. L'outil Terraform est un outil très complet qui contient beaucoup d'options. Il existe d'autres options relatives à la gestion de l'état de Terraform mais je n'ai pas eu (encore ?) besoin de les utiliser.

Si vous avez des retours ou des conseils sur l'utilisation de Terraform, n'hésitez pas à les partager avec moi sur [X](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
