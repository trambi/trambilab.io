---
title: "Décrire une Lambda basée sur une image de conteneur en Terraform"
date: "2023-02-10"
draft: false
description: "même dans un VPC"
tags: ["terraform","container","cloud","lambda","outils"]
author: "Bertrand Madet"
---

Ce billet fait partie d'une série de billets de d'exploration de la description de ressources AWS avec Terraform :

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [Décrire une API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).

-------

Je vous propose de nous intéresser à la description de Lambda en Terraform. Pas n'importe quelle Lambda, puisque la lambda que je vais décrire s'appuiera sur une image de conteneur de type Docker.

## Lambda basée sur un image de conteneur ?

Une [Lambda (https://docs.aws.amazon.com/fr_fr/lambda/)](https://docs.aws.amazon.com/fr_fr/lambda/) est une unité de traitement qui s'abstrait des contingences de serveur. Dans les cas classiques (et promus par AWS), une lambda est basée uniquement sur le code source d'un traitement et des `layers` pour éviter le duplication de code.

AWS permet d'utiliser aussi une image de conteneur de type Docker comme base d'exécution. L'exécution de ce type de lambda est un plus lente que les lambdas classiques. Pourquoi ? Peut-être que les conteneurs ajoutent un peu de traitement supplémentaire à la technologie utilisée par les lambdas : les micro machines virtuelles [FireCracker (https://github.com/firecracker-microvm/firecracker)](https://github.com/firecracker-microvm/firecracker).

La lambda pointe vers une image d'un dépôt d'image de conteneur, la version d'AWS dans le cas qui nous intéresse : ECR (pour :gb: **E**lastic **C**ontainer **R**egistry).

```mermaid
graph LR;
    lambda(Lambda)-->|image|ecr[ECR]
    lambda-->|api|ecr
    lambda-.->|stockage|s3[Stockage utilisé par ECR]
```

## Implications

La lambda doit avoir des droits supplémentaires pour lister les images et récupérer l'image cible.

> :warning: Cela amène aussi la problématique de mise à jour des images de conteneur utilisé puisqu'une lambda pointe vers une version spécifique d'une image de conteneur donc dès que cette image contient une faille de sécurité, ma lambda contient potentiellement (juste potentiellement car pas forcement exploitable dans le cadre d'exécution de cette lambda). Là où la mise à jour de l'environnement d'exécution est sous la seule responsabilité d'AWS pour les lambda classique, on devient aussi un peu responsable de notre environnement d'exécution. La surface d'attaque sous notre responsabilité passe du code source à l'image de conteneur utilisée.

Une fois, ces avertissements énoncés, cela peut être intéressant de disposer de Lambdas sur la forme d'une image de conteneur pour permettre :

- d'exécuter sur le poste des développeurs ou dans des environnements non AWS pour les tests par exemple ;
- d'archiver les images exécutables et de les auditer avant le déploiement ou *a posteriori* ;
- d'utiliser des solutions trop difficiles à utiliser avec les layers ;
- d'accompagner le changement de paradigme pour une équipe experte en conteneur.

## Lambda à partir d'un image de conteneur en Terraform

La description d'une lambda basée une image de conteneur va nécessiter uniquement la ressource supplémentaire du registre de l'image.

```mermaid
graph LR;
    lambda(Lambda)-->aws-iam-role[Rôle de la Lambda]
    lambda==>aws-ecr_repository[URI de l'image de conteneur à utiliser]
    aws-iam-role-policy-attachment[Lien entre un rôle et une autorisation]-->aws-iam-policy[Autorisations spécifique à la Lambda]
    aws-iam-role-policy-attachment-->aws-iam-role
```

Ce qui se traduit en HCL pour Terraform et en partant du principe que l'on récupère les données d'une image du nom `test-for-you` et d'un tag `latest` créée lors d'une étape précédente et une autorisation transmise en paramètre.

```hcl

data "aws_ecr_repository" "main" {
  name = "test-for-you"
}

data "aws_ecr_image" "main" {
  repository_name = "test-for-you"
  image_tag       = "latest"
}

resource "aws_iam_role" "main" {
  name = "my-lambda"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action    = "sts:AssumeRole"
      Effect    = "Allow"
      Sid       = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
    }]
  })
}

resource "aws_lambda_function" "main" {
  function_name = "my-lambda"
  role = aws_iam_role.main.arn
  image_uri = "${data.aws_ecr_repository.main.repository_url}:latest"
  source_code_hash = split(":",data.aws_ecr_image.main.id)[1]
}

resource "aws_role_policy_attachment" "main" {
  role       = aws_iam_role.main.arn
  policy_arn = var.policy_arn
}
```

## Dans un VPC

Un [VPC (https://aws.amazon.com/fr/vpc)](https://aws.amazon.com/fr/vpc) (:gb: pour **V**irtual **P**rivate **C**loud) permet de faire de construire les ressources AWS en isolées. Il est possible de lier une Lambda dans un VPC par exemple pour utiliser des ressources à l'intérieur d'un VPC. Les Lambdas doivent avoir des droits supplémentaires pour créer des interfaces réseaux virtuelles (à rattacher à des sous-réseaux). Bref comme indiqué dans mon précédent billet ([Six mois sur AWS]({{< ref "/post/fr/2023-02-03-six-months-with-aws.md">}})), cela complique la situation.

```mermaid
graph LR;
    subgraph VPC
      lambda(Lambda)-->|image|vpc-endpoint-ecr-dkr[Point de terminaison VPC de type ecr.dkr]
      lambda-->|api|vpc-endpoint-ecr-api[Point de terminaison VPC de type ecr.api]
      lambda-.->|stockage|vpc-endpoint-s3[Point de terminaison VPC de type s3]
      lambda-.->eni[Interface virtuelle réseau]
      eni-.->subnetwork[sous-réseau]
    end
    vpc-endpoint-ecr-dkr-->|image|ecr[ECR]
    vpc-endpoint-ecr-api-->|api|ecr
    vpc-endpoint-s3-.->|stockage|s3[Stockage S3 utilisé par ECR]
```

## Lambda dans un VPC à partir d'un image de conteneur en Terraform

Il est impératif d'avoir des points de terminaison VPC (`ecr.dkr`, `ecr.api` et `s3`) avec résolution de nom de domaine (DNS) privée activée. Avec cette configuration, il y a "juste" des autorisations à ajouter (cf. [Configuring a Lambda function to access resources in a VPC (https://docs.aws.amazon.com/lambda/latest/dg/configuration-vpc.html#vpc-permissions)](https://docs.aws.amazon.com/lambda/latest/dg/configuration-vpc.html#vpc-permissions)).

Ce qui donne un fichier de description HCL ressemblant à :

```hcl
data "aws_ecr_repository" "main" {
  name = "test-for-you"
}

data "aws_ecr_image" "main" {
  repository_name = "test-for-you"
  image_tag       = "latest"
}

resource "aws_iam_role" "main" {
  name = var.name
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
      }
    ]
  })
}

resource "aws_lambda_function" "main" {
  function_name    = "my-lambda"
  role             = aws_iam_role.main.arn
  image_uri        = "${data.aws_ecr_repository.main.repository_url}:${var.image_tag}"
  source_code_hash = split(":", data.aws_ecr_image.main.id)[1]
  package_type     = "Image"
  vpc_config {
    subnet_ids         = var.subnets_ids
    security_group_ids = var.security_group_ids
  }
}

resource "aws_iam_policy" "pull-from-ecr" {
  name        = "my-lambda-pull-from-ecr"
  description = "Policy to allow pull from ECR"

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "ecr:BatchCheckLayerAvailability",
          "ecr:GetDownloadUrlForLayer",
          "ecr:GetRepositoryPolicy",
          "ecr:DescribeRepositories",
          "ecr:ListImages",
          "ecr:DescribeImages",
          "ecr:BatchGetImage"
        ],
        "Resource" : data.aws_ecr_repository.main.arn
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "ecr:GetAuthorizationToken"
        ],
        "Resource" : "*"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "pull-from-ecr" {
  role       = aws_iam_role.main.name
  policy_arn = aws_iam_policy.pull-from-ecr.arn
}

resource "aws_iam_role_policy_attachment" "AWSLambdaVPCAccessExecutionRole" {
  role       = aws_iam_role.main.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}
```

## Conclusion

Le plus dur a été de trouver quelle URL de l'image donné avec la configuration des points de terminaison VPC. Il est en général difficile d'obtenir des documentations concrètes sur la description d'architecture AWS non triviale. Avec Terraform, c'est encore plus difficile. Voilà aussi pourquoi ce billet est écrit.

Si vous avez des conseils, des questions ou des retours d'expériences sur l'utilisation d'AWS avec Terraform, je suis intéressé. N'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

-------

- [Partage du contenu d'un S3 via API Gateway et Terraform]({{< ref "/post/fr/2022-10-19-aws-north-face-part1.md">}}) ;
- [Tracer les accès à une API Gateway avec Terraform]({{< ref "/post/fr/2022-11-01-aws-north-face-part2.md">}}) ;
- [Décrire une API Gateway avec OpenAPI et Terraform]({{< ref "/post/fr/2022-12-15-aws-north-face-part3.md">}}) ;
- [Décrire une API Gateway privée avec une url personnalisée]({{< ref "/post/fr/2023-04-14-aws-north-face-part5.md">}}) ;
- [Déployer une lambda avec une archive zip]({{< ref "/post/fr/2023-05-02-aws-north-face-part6.md">}}) ;
- [Ce que je trouve difficile avec les lambdas]({{< ref "/post/fr/2024-02-11-aws-north-face-part7.md">}}) ;
- [Rendre plus robuste le traitement d'un évènement S3]({{< ref "/post/fr/2024-03-21-aws-north-face-part8.md">}}).
