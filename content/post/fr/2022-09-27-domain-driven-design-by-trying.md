---
title: "DDD par la pratique"
date: "2022-09-27"
draft: false
description: "découvrir en groupe le DDD en essayant"
tags: ["DDD","projet perso"]
author: "Bertrand Madet"
---

Après ma [lecture de Domain Modeling Made Functional]({{< ref "/post/fr/2022-08-01-domain-modeling-made-functional-lecture.md">}}), j'ai proposé à des collèges de tester l'approche du `DDD` (**D**omain **D**riven **D**esign ou Conception pilotée par le domaine). L'idée était de concrétiser les concepts et les méthodes exposés par le livre.

Pour avoir un `expert du domaine` disponible, j'ai proposé de se pencher sur le sujet des ... tournois de jeu de plateau :smile:.

## Lancement

J'ai présenté le principe de ces sessions de découverte par la pratique au collègue qui m'avait prêté le livre, il était partant. J'en ai ensuite parlé à quelques personnes. Le sujet étant moins maîtrisé que les [test clinics]({{< ref "/post/fr/2022-04-06-test-clinic.md" >}}), j'ai invité uniquement les personnes qui étaient volontaires lors de mes discussions préalables.

Les sept sessions qui ont suivi étaient dédiées :

1. à l'`event storming` - partie 1 : découverte d'évènement métier en vrac ;
1. à la définition d'un processus métier, c'était une erreur, nous n'avions pas fini l'`event storming` ;
1. à l'`event storming` - partie 2 : tri des événements dans l'ordre chronologique pour avoir une frise des évènements ;
1. au découpage du domaine en contexte délimité en s'appuyant sur la frise des évènements ;
1. à la définition des processus métier d'un contexte délimité ;
1. à la définition des objets métiers d'un contexte délimité ;
1. à l'implémentation des types métiers et des processus métier.

## Déroulé

### Avant

Nous avons organisé cela sous la forme de session d'une heure entre midi et deux une fois par semaine. J'ai aussi présenté la démarche sur les canaux dédiés aux évènements de mes organisations. J'ai ajouté aux invitations les personnes qui ont manisfesté de l'intérêt.

La session était proposée en hybride en visio-conférence et dans un espace physique avec du passage.

### Pendant

Une session d’une heure peut consister en :

- Dix (10) minutes d’introduction : rappel de la démarche et présentation des résultats de la session précédente ;
- Quarante-cinq (45) minutes de pratique ;
- Cinq (5) de conclusion : sous la forme d’un tour de table et d’une évaluation de l’intérêt de la session.

Nous avons utilisé [Excalidraw (https://excalidraw.com/)](https://excalidraw.com/) pour les premières séances, Visual Studio Code pour les dernières.

A la fin de la septième séance, nous avons convenu que l'approche était suffisamment illustrée.

### Après

Étant donné que les sessions avaient lieu pendant les vacances et qu'elles s'enchaînaient, il m'est apparu important de partager les résultats de chaque session sur les canaux dédiées aux évènements et à l'ensemble des participants.

## Enseignements

Ces sept heures nous ont permis d'avoir une vision plus claire sur le `DDD` :

- L'`event storming` mérite de prendre le temps (dans notre cas deux heures) car il sert pour :
  - le découpage du domaine,
  - l'analyse des processus métier,
  - l'analyse des objets métiers ;
- Le découpage du domaine en contextes délimités est un choix de conception ;
- :+1: L'approche est très séduisante car elle permet effectivement de partager un langage commun ;
- :+1: L'apport de personnes extérieurs au domaine permet d'amener plus de recul et d'expliciter l'implicite ;
- :warning: Faire l'analyse du domaine uniquement en français :fr: alors que l'on code en anglais :gb: peut ruiner l'effort pour aller vers un langage commun :exploding_head:.

On peut noter l'approche se combine très avec les langages qui supportent les [types algébriques de données (https://fr.wikipedia.org/wiki/Type_alg%C3%A9brique_de_donn%C3%A9es)](https://fr.wikipedia.org/wiki/Type_alg%C3%A9brique_de_donn%C3%A9es). L'utilisation des types algébriques des données donne l'impression qu'un expert métier pourrait lire et valider l'implémentation des objets métier.

## Conclusion

Entre deux et dix participants ont participé aux sessions (je vous rassure nous avons plutôt commencé à deux et fini à dix). J'ai trouvé l'expérience très intéressante et j'ai envie d'en apprendre plus sur le `DDD`.

En plus, j'ai pu formaliser des éléments précieux pour la définition du domaine des tournois de jeux de plateau. Le découpage en contextes délimités a amené à quatre contextes majeurs donc on peut imaginer qu'il faudrait une dizaine d'heure de plus pour définir tous les processus métier et peut-être la moitié pour les objets métier. Avant d'aller plus loin, je vais présenter le résultat à mes comparses d'organisation de tournoi.

Si vous avez des conseils ou des expériences à partager sur le `DDD`, contactez moi [sur Twitter](https://twitter.com/trambi_78) ou [sur LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
