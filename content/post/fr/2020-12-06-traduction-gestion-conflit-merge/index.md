---
title: "Gérer les conflits de merge"
date: "2020-12-06"
description: "Traduction de Dealing with Merge Conflicts de Tobias Günther"
tags: ["traduction","git"]
author: "Bertrand Madet"
---

*Ce billet est la traduction :fr: du billet [Dealing with Merge Conflicts de Tobias Günther (https://www.git-tower.com/learn/git/ebook/en/command-line/advanced-topics/merge-conflicts/)](https://www.git-tower.com/learn/git/ebook/en/command-line/advanced-topics/merge-conflicts/).*

# Gérer les conflits de merge

Pour beaucoup, les conflits de merge sont aussi effrayants que le formatage accidentiel de leur disque dur. Au cours de ce chapitre, je veux vous oter cette peur.

## Vous ne pouvez pas casser de choses

La première chose à avoir en tête est que vous pouvez toujours annuler une fusion et revenir dans l'état antérieur au conflit. Vous pouvez toujours annuler et recommencer.

Si vous avez l'habitude d'un autre système de gestion de version comme par exemple Subversion vous pourriez être traumatisé : les conflits dans Subversion ont la (légitime) réputation d'être à la fois complexes et désagréables. Une des raisons à cela est que Git fonctionne complètement différement de Subversion. En conséquence, Git est capable de prendre en charge la plupart des choses lors d'une fusion, vous laissant ainsi des scénarios relativement simples à résoudre.

De plus, un conflit ne vous handicapera jamais. Cela n'entraînera pas l'arrêt de toute votre équipe ni ne paralysera votre dépôt central. En effet, dans Git, les conflits ne peuvent se produire que sur la machine locale d'un développeur - et non sur le serveur.

## Comment un conflit de merge arrive

Dans Git, "fusionner" *(note du traducteur : ou merger en mauvais français)* est l'acte d'intégrer une autre branche dans votre branche de travail actuelle. Vous prenez des changements d'un autre contexte (c'est ce qu'est en fait une branche: un contexte) et les combinez avec vos fichiers de travail actuels.

L'avantage d'avoir Git comme système de contrôle de version est que cela rend la fusion extrêmement facile : dans la plupart des cas, Git trouvera comment intégrer de nouvelles modifications.

Cependant, il existe une poignée de situations dans lesquelles vous devrez intervenir et dire à Git quoi faire. Le cas le plus fréquent, c'est lors du changement du même fichier. Même dans ce cas, Git sera probablement capable de le comprendre seul. Mais si deux personnes ont changé les mêmes lignes dans ce même fichier, ou si une personne a décidé de le supprimer alors que l'autre personne a décidé de le modifier, Git ne peut tout simplement pas savoir ce qui est correct. Git marquera alors le fichier comme ayant un conflit et vous devrez le résoudre avant de pouvoir continuer votre travail.

## Comment résoudre un conflit de fusion

Face à un conflit de merge, la première étape consiste à comprendre ce qui s'est passé. Par exemple : Un de vos collègues a-t-il édité le même fichier sur les mêmes lignes que vous ? A-t-il supprimé un fichier que vous avez modifié ? Avez-vous tous les deux ajouté un fichier du même nom ?

Git vous dira que vous avez des "chemins non fusionnés" (ce qui est juste une autre façon de vous dire que vous avez un ou plusieurs conflits) via "git status" :

```bash
$ git status
# On branch contact-form
# You have unmerged paths.
#   (fix conflicts and run "git commit")
#
# Unmerged paths:
#   (use "git add <file>..." to mark resolution)
#
#       both modified:   contact.html
#
no changes added to commit (use "git add" and/or "git commit -a")
```

Examinons en détail comment résoudre le cas le plus courant, lorsque deux modifications sur les mêmes lignes d'un fichier.
Il est temps d'analyser le contenu du fichier en conflit. Git a été assez gentil pour marquer la zone problématique dans le fichier en la balisant dans `<<<<<<< HEAD` et `>>>>>>> [autre / branche / nom]`.

Le contenu après le premier marqueur provient de votre branche de travail actuelle. Après les chevrons, Git nous dit d'où (de quelle branche) les changements sont venus. Une ligne `=======` sépare les deux modifications en conflit.

Notre travail consiste maintenant à nettoyer ces lignes: lorsque nous avons terminé, le fichier doit ressembler exactement à ce que nous voulons. Il peut être nécessaire de consulter le coéquipier qui a écrit les modifications conflictuelles pour décider quel code est finalement correct. Peut-être que c'est le vôtre, peut-être que c'est le sien - ou peut-être un mélange entre les deux.

Ouvrir le fichier brut dans votre éditeur et le nettoyer là-haut est parfaitement valide, mais pas très confortable. L'utilisation d'un outil de fusion dédié peut rendre ce travail beaucoup plus facile (si vous en avez un installé ...). Vous pouvez configurer l'outil de votre choix en utilisant la commande "git config". Consultez la documentation de votre outil pour obtenir des instructions détaillées.
Ensuite, en cas de conflit, vous pourrez l'invoquer ultérieurement en tapant simplement `git mergetool`.

Par exemple, j'ai utilisé `Kaleidoscope.app` sur Mac :
![Kaleidoscope.app sur Mac](./merge-conflict-in-gui.jpg)

Les volets gauche et droit représentent les modifications conflictuelles; une visualisation bien plus élégante que `<<<<<<<`et `>>>>>>>`. Vous pouvez maintenant simplement basculer le changement à prendre. Le volet du milieu montre le code résultant; dans de bons outils, vous pouvez même modifier cela davantage.

Maintenant, après avoir nettoyé le fichier avec le code final, il ne reste plus qu'à l'enregistrer. Pour indiquer à Git que vous en avez terminé avec ce fichier, vous devez quitter l'outil de fusion pour continuer. Dans les coulisses, cela a dit à Git d'exécuter une commande `git add` sur le fichier (maintenant anciennement) en conflit. Cela marque le conflit comme résolu. Si vous décidez de ne pas utiliser d'outil de fusion et de nettoyer le fichier dans votre éditeur, vous devrez marquer le fichier comme résolu à la main (en exécutant `git add`).

Enfin, après avoir résolu tous les conflits, une situation de conflit de fusion doit être conclue par un commit classique.

## Astuce pour résoudre les conflits dans la Tower Git

Si vous utilisez le [client Tower Git](https://www.git-tower.com/windows), son assistant de gestion de conflit vous aidera à résoudre plus facilement les conflits de fusion *(note du traducteur : le billet original étant issu du blog de Tower Git)*.

{{< video width=640 height=480 src="./tower_conflict-wizard.mp4" >}}

## Comment annuler un merge

Comme indiqué plus haut, vous pouvez revenir à l'état antérieur au merge à tout moment. Cela devrait vous donner l'assurance que vous ne pouvez rien casser. Sur la ligne de commande, un simple `git merge --abort` *(note du traducteur : ou `git rebase --abort` si vous étiez en train de faire un rebase)* le fera pour vous.

Si vous avez fait une erreur lors de la résolution d'un conflit et que vous ne vous en rendez compte qu'après avoir terminé la fusion, vous pouvez encore l'annuler facilement: il vous suffit de revenir au commit avant que la fusion ne se produise avec "git reset --hard" et de recommencer.
