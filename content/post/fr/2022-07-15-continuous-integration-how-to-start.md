---
title: "Commencer son intégration continue"
date: "2022-07-15"
draft: false
description: "et les pistes pour la suite"
tags: ["CI","devops","gitlab","Software Craftmanship"]
author: "Bertrand Madet"
---

C'est pas tout de savoir que [l'intégration continue est importante]({{< ref "/post/fr/2022-06-14-continuous-integration.md" >}}), il faut l'utiliser. Dans cet article, nous allons construire la chaîne d'intégration continue d'une application web utilisant Node.js. Et nous illustrerons avec Gitlab-CI.

## Pour où commencer ?

Pour du Node.js, on peut commencer avec 3 étapes :

- installation des dépendances : `npm ci` ;
- construction de l'application : `npm build` ;
- lancement des tests : `npm run test`.

:warning: Pour avoir une reproductibilité des paquets installés, il faut utiliser `npm ci`. A la différence de `npm install` qui installe les dernières versions compatibles avec celles spécifiées dans le fichier `package.json`,  `npm ci` va installer exactement ce qui est indiqué dans le fichier `package-lock.json`. Et donc mettre `package.json` **et** `package-lock.json` en gestion de configuration.

### En une tâche

Dans un premier temps, on peut lancer ces trois étapes dans une seule tâche. Un exemple du fichier `.gitlab-ci.yml` pour décrire cette chaîne de construction.

```yaml
all-in-one:
  image: node:18-slim
  script:
  - npm ci
  - npm build
  - npm run test
  artifacts:
    path:
    - build
```

### En trois tâches

Comme la construction et les tests dépendent de l'installation, on peut envisager de décomposer en trois tâches. Cela nécessitera d'indiquer une dépendance entre deux tâches et de transmettre des données entre les tâches.

```mermaid
graph LR;
    install(Installation des modules)-->build(Construction)
    install-->test(Lancement des tests unitaires)
```

Pour Gitlab-CI, on va se servir de :

- `needs` pour indiquer la dépendance d'une tâche à une autre tâche ;
- `cache` pour partager des données entre tâches.

```yaml
image: node:18-slim

# Use same cache for (node_modules) unless package-lock.json changes
cache:
  key:
    files:
      - package-lock.json
  paths:
    - node_modules/

install:
  script:
  - npm ci
  needs: []

build:
  script:
  - npm build
  needs: ["install"]
  artifacts:
    path:
    - build

test:
  script:
  - npm run test
  needs: ["install"]

```

### Avantages et inconvénients

En une tâche :

- :+1: C'est simple ;
- :-1: Si on doit ajouter des choses à faire pour l'intégration, cela sera de plus en plus long.

Pour les tâches :

- :-1: Le fichier de description de la CI est plus complexe ;
- :+1: Les tests et la construction sont parallélisables ;
- :-1: les performances vont dépendre de la performance du cache ;
- :+1: Le chaîne d'intégration est prête pour ajouter d'autres tâches.

Dans un premier temps, je conseillerais de débuter avec une seule tâche.

## La suite

Il est possible d'effectuer des contrôles supplémentaires sur le code à chaque révision :

- formattage avec [Prettier (https://prettier.io/)](https://prettier.io/), [EditorConfig (https://editorconfig.org/)](https://editorconfig.org/) ;
- analyse statique de code avec [ESLint (https://eslint.org/)](https://eslint.org/) ou CodeClimate ;
- analyse d'obsolescence de dépendance avec npm.

### Formattage

Pour vérifier que le code utilise bien le formattage fourni par `prettier`, il faut utiliser le paramètre `--check`.

La commande suivante retournera la liste des erreurs de formattage.

```bash
prettier --check "src/**/.*.js"
```

Si on a ajouté prettier dans les dépendances, on peut ajouter la tâche suivante au fichier `.gitlab-ci.yml`.

```yaml
# Fin du fichier

format:
  script:
  - npm run prettier -- --check "src/**/*.js"
  needs: ["install"]
```

:information_source: Pour Python, on peut utiliser [Black (https://pypi.org/project/black/)](https://pypi.org/project/black/).

### Analyse statique

La commande suivante retournera la liste des erreurs potentielles dans votre code.

```bash
npx eslint --ext .js src/
```

Si on a ajouté `eslint` dans les dépendances, on peut ajouter la tâche suivante au fichier `.gitlab-ci.yml`.

```yaml
# Fin du fichier

analysis:
  script:
  - npx eslint --ext .js src/
  needs: ["install"]
```

:information_source: Pour python, on peut utiliser [flake8 (https://flake8.pycqa.org/en/latest/)](https://flake8.pycqa.org/en/latest/) ou `pylint`.

### Obsolescence de dépendance

Avec `npm outdated` on peut lister les paquets npm qui sont compatibles avec les versions spécifiées dans le fichier `package.json` et qui pourraient mis à jour.

```yaml
# Fin du fichier

outdated:
  script:
  - npm outdated
  needs: ["install"]
```

:information_source: Il existe la commande `pip list --outdated` pour pip et Python.

## Conclusion

Pas besoin de faire très compliqué, il est possible de commencer l'intégration continue avec une configuration simple et de complexifier par la suite. On peut imaginer d'autres étapes pour les tests d'intégration, de déploiements, de vérification de sécurité...

Les concepts soulevés dans ce billet sont utilisables dans d'autres outils d'intégration continue : [GitHub Actions (https://fr.github.com/features/actions)](https://fr.github.com/features/actions), [Circle CI (https://circleci.com/)](https://circleci.com/), [Jenkins (https://www.jenkins.io/)](https://www.jenkins.io/) ou même [AWS Codepipeline (https://aws.amazon.com/fr/codepipeline/)](https://aws.amazon.com/fr/codepipeline/).

Si vous avez des questions ou des retours d'expériences sur l'intégration continue, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
