---
title: "Coder un module Drupal - partie 2"
date: "2022-02-09"
draft: false
description: "Un développement qui devait être facile"
tags: ["projet perso","php","jeu", "drupal"]
author: "Bertrand Madet"
---

Dans ce billet, j'explique la suite du développement personnel que je pensais être simple et qui m'a pas mal occupé ces derniers temps : le développement d'un module Drupal. Cela m'a occupé suffisamment pour que je ne prenne pas le temps de d'écrire le billet de la semaine dernière :fearful:.

## Développement

### Plan

Je vous invite à lire auparavant la [première partie]({{< ref "/post/fr/2022-01-12-code-drupal-module-part-1.md" >}}). En résumé l'idée était d'avoir un module Drupal qui permet de générer le classement à partir des parties jouées. J'avais préparé mon environnement de développement et un module avec un champ.

### Entités

Finalement pour gérer la cardinalité des objets entre eux, j'ai été obligé de coder trois entités :

- `Competition` qui représente les compétitions;
- `Game` qui représente les parties jouées et à agréger pour effectuer un classement;
- `Criteria` qui représente les critères de classement d'une compétition, il a un nom, une valeur par défaut et une liste de condition et d'expression si cette condition est réalisée - codés avec un champ `StatementField` dans l'épisode précédent;

```mermaid
classDiagram
Competition "*" --> "*" Criteria
Competition "1" --> "*" Game
Criteria "1" --> "*" Statement
```

L'inconvénient c'est qu'il a fallu coder trois entités, l'avantage c'est qu'une compétition peut réutiliser un critère de classement.

Dans mon cas, l'implémentation d'une entité revient à : 

- créer une classe qui dérive de `ContentEntityBase`;
- écrire les annotations; 
- surcharger la méthode `baseFieldDefinitions`.

L'arbitrage entre flexibilité et réutilisabilité d'un côté et complexité du code est favorable à la création de trois entités (plutôt que deux).

### Block

Je pensais implémenter une vue, mais je m'étais trompé j'avais besoin d'un `Block` - une partie d'affichage qui peut être réutilisé dans différentes pages et même en plusieurs exemplaires sur une page.

Comme souvent cela revient à implémenter une classe qui dérive d'une classe de base `BlockBase`, de remplir les annotations et surcharger un petit nombre de méthodes.

Les trois méthodes à surcharger :

- `build` permettent le rendu du bloc;
- `blockForm` permettent l'affichage du formulaire de configuration du bloc;
- `blockSubmit` permettent la sauvegarde de la configuration du bloc.

La méthode `build` gère la liaison entre le stockage des entités et la logique de classement.

## Ce que j'ai appris

### La documentation ne couvre pas les cas complexes

La documentation explique bien le cas simple. Malheureusement, elle évoque peu les cas complexes. Par exemple pour la création d'entité, on trouve facilement une relation avec une cardinalité de un à une autre entité mais pas pour les liaisons un à n. Et les cas pratiques sont rarement des relations uniquement un à un. Cette absence va jusque dans la documentation des API qui fournit des indications minimales sur les classes, les méthodes et les attributs.

### Administration de Drupal est puissante

Avec Drupal, il est possible de créer une entité de manière graphique dans la partie d'administration. Il est ensuite possible d'exporter l'entité ainsi créer. Je pense qu'il faudrait essayer d'utiliser cette possibilité pour accélérer le développement et de ne coder que pour les codes non prévus dans l'interface d'administration.

Ce que je dis est vrai pour les entités, mais aussi les champs. Ce qui fait que Drupal est extrêmement puissant.

## Conclusion

J'avoue que voir le classement s'afficher dans un block m'a fait plaisir même si j'ai passé beaucoup de temps pour ce petit résultat. Cela m'a permis de me reconnecter à l'écosystème PHP que je fréquentais plus depuis des années.

Si vous avez des conseils sur ce module Drupal ou le développement Drupal, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).

Si vous souhaitez contribuer, le dépôt est sur Gitlab : [https://gitlab.com/trambi/drupal_module_just_rank_games](https://gitlab.com/trambi/drupal_module_just_rank_games) et les contributions sont les bienvenues.
