---
title: "Gestion des états en React"
date: "2022-07-27"
draft: false
description: "ou la responsabilité des composants"
tags: ["front-end","reactjs"]
author: "Bertrand Madet"
---

Pour influencer le rendu d'un composant React, il est possible :

- soit de fixer les propriétés transmises par le composant parent (`props` en React) ;
- soit d'utiliser les états internes créés par des fonctions spécifiques (`hooks` en React).

Je pense qu'on peut simplifier cette problématique en indiquant que la gestion de l'état du composant est externe (par les props) ou interne (avec les hooks `useState` et `useReducer`).

## Stratégie de gestion des états

Il y a plusieurs stratégies pour fixer la gestion de l'état des composants.

### Centralisation de l'état dans le composant racine

L'idée est de gérer uniquement l'état au plus haut niveau donc au niveau du composant racine.

```mermaid
graph TD;
    root(Composant racine)-->component1[Composant sans état]
    root-->component2[Composant sans état]
    component1-->component3[Composant sans état]
    component1-->component4[Composant sans état]
    component1-->component5[Composant sans état]
    component2-->component6[Composant sans état]
    component2-->component7[Composant sans état]
    component2-->component8[Composant sans état]
    component5-->component9[Composant sans état]
    component5-->component10[Composant sans état]
    component7-->component11[Composant sans état]
```

|Avantages|Inconvénients|
|---------|-------------|
|:+1: Les composants sont simples à développer et à tester|:-1: Les propriétés sont propagées de la racine vers tous les composants|
|:+1: L'arbitrage est facile à se souvenir|:-1: Un doublon de composant implique un doublon dans la gestion de l'état|

La propagation des propriétés est appelée en anglais :gb: `prop drilling`. Le principal inconvénient est le grand nombre de propriétés pour les composants de haut niveau.

Cette stratégie est particulièrement intéressante sur les petites applications React.

### Centralisation par page

En admettant qu'une **S**ingle **P**age **A**pplication ait plusieurs pages au sens des utilisateurs (cela correspond à des routes pour `react-router`), l'idée est de gérer l'état au niveau de la page.

```mermaid
graph TD;
    root(Composant racine)-->page1[Page 1]
    root-->page2[Page 2]
    page1-->component1[Composant sans état]
    page1-->component2[Composant sans état]
    page1-->component3[Composant sans état]
    page2-->component4[Composant sans état]
    page2-->component5[Composant sans état]
    page2-->component6[Composant sans état]
    component2-->component7[Composant sans état]
    component2-->component8[Composant sans état]
    component6-->component9[Composant sans état]
```

|Avantages|Inconvénients|
|---------|-------------|
|:+1: Les composants sont simples à développer et à tester|:-1: Les propriétés sont propagées de la racine vers tous les composants|
|:+1: L'arbitrage est facile à se souvenir|:-1: Sauf pour les états partagés entre deux pages|
||:-1: Un doublon de composant implique un doublon dans la gestion de l'état|

Cette stratégie est particulièrement intéressante pour une application React avec beaucoup de pages simples.

### Au cas par cas

L'idée est d'arbitrer pour chaque composant

```mermaid
graph TD;
    root(Composant racine)-->component1[Composant sans état]
    root-->component2[Composant avec état]
    component1-->component3[Composant sans état]
    component1-->component4[Composant avec état]
    component1-->component5[Composant avec état]
    component2-->component6[Composant sans état]
    component2-->component7[Composant avec état]
    component2-->component8[Composant sans état]
    component5-->component9[Composant sans état]
    component5-->component10[Composant sans état]
    component7-->component11[Composant avec état]
```

|Avantages|Inconvénients|
|---------|-------------|
|:+1: Les composants avec leur comportement sont réutilisables |:-1: Les composants sont plus compliqués à développer et à tester|
||:-1: Arbitrage à faire pour chaque composant en fonction du parent, des enfants|

Cette stratégie est intéressante sur les applications React avec des composants à réutiliser. La complexité des tests et des développements vient des interactions entre les propriétés et les états internes.

## Exemple d'un composant

Afin d'illustrer les deux approches, je vous propose le code d'un même composant. C'est un composant assez simple, qui permet d'afficher des cases à cocher et le nombre d'éléments cochés.

Il peut être réduit :

![Composant réduit](example-component-1.png)

Ou déplié :

![Composant déplié](example-component-2.png)


### Avec état interne

```typescript
import React, {useCallback, useEffect, useState} from 'react';

interface MultiSelectProps {
  options: string[]
}

export const MultiSelect = ({options}:MultiSelectProps) =>{
  const [selectedOptions,setSelectedOptions] = useState([...options]);
  const handleChange = useCallback((event :React.ChangeEvent<HTMLInputElement>)=>{
    const {value} = event.target;
    setSelectedOptions((previous: string[]) => {
      if (!previous.includes(value)) {
        return [...previous,value];
      } else {
        return previous.filter((current: string)=> current!== value);
      }
    });
  },[setSelectedOptions]);
  useEffect(()=>{
    setSelectedOptions([...options]);
  },[options]);
  return (<details>
    <summary>{`${selectedOptions.length}`} item{selectedOptions.length>1?'s':''}</summary>
    <>
    {options.map((option)=>(
      <label key={option}>
        <input type="checkbox"
          key="input"
          value={option}
          name={option} 
          checked={selectedOptions.includes(option)}
          onChange={handleChange} />
        {option}
      </label>
    )
    )}
    </>
  </details>);
}
```

Les propriétés sont réduites aux cases à cocher possibles. L'état (coché ou non) des cases à cocher est géré grâce au hook `useState`.

> :warning: La prise en compte du changement de propriété `options` est effectuée par le hook `useEffect` avec la dépendance à cette propriété. Sans cela, même si la props `options` évolue, l'état `selectedOptions` n'est pas réinitialisé avec le contenu de `options`.

### Sans état

```typescript
import React, {useCallback} from 'react';

interface option {
  name: string;
  select: boolean;
}

interface StatelessMultiSelectProps {
  options: option[];
  onChange: (option: string)=>void;
}

export const StatelessMultiSelect = ({options,onChange}:StatelessMultiSelectProps) =>{
  const handleChange = useCallback( (event :React.ChangeEvent<HTMLInputElement>) => {
    const {value} = event.target;
    onChange(value);
  }, [onChange]);
  const selectedOptionCount = options.filter((option)=>option.select).length;
  return (<details>
    <summary>{`${selectedOptionCount}`} item{selectedOptionCount>1?'s':''}</summary>
    <>
    {options.map( ({name,select}) => (
      <label key={name}>
        <input type="checkbox"
          value={name}
          name={name} 
          checked={select}
          onChange={handleChange} />
        {name}
      </label>)
    )}
    </>
  </details>);
}
```

Sans état, on voit que l'on est obligé de complexifier les propriétés mais que le code du composant est plus simple.

### Arbitrage

Pour ce genre de composant, on va avoir besoin de récupérer les cases cochées et donc je serai plutôt tenté par l'utilisation de la version sans état. L'état sera géré au niveau du composant qui a besoin des informations sur les cases cochées.

## Conclusion

J'espère que cet article aura permis de comprendre l'enjeu de la gestion des états dans une application. Il existe d'autres outils pour contrebalancer les inconvénients de certaines stratégies. Je les aborderai peut-être dans d'autres billets.

Si vous avez des questions ou des retours d'expériences sur React, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
