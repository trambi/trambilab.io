---
title: "TDD appliqué à un composant React - partie 2"
date: "2022-07-07"
draft: false
description: "une illustration concrète de la méthode"
tags: ["tdd","front-end","refactoring","methodologie","Software Craftmanship","test","reactjs"]
author: "Bertrand Madet"
---

Aujourd'hui, la continuation du billet [TDD appliqué à un composant React]({{< ref "/post/fr/2021-12-01-tdd-react-component.md" >}}) à l'aune de l'expérience lié aux [Test clinics]({{< ref "/post/fr/2022-04-06-test-clinic.md" >}}).

On va refactorer les tests, le composant puis ajouter la gestion des erreurs sur un formulaire de connexion en React. Et on va finir par l'utilisation de `useReducer`

## Refactoring

### Amélioration des tests

```typescript
// src/components/login-form/login-form.test.tsx

import react from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'

import { LoginForm } from '.';

describe('login-form',()=> {
  it('should call onSubmit on valid path',()=>{
    const submitHandle = jest.fn();
    render(
      <LoginForm buttonText="Submit"
      loginLabel="login"
      passwordLabel="password"
      onSubmit={submitHandle}/>);
    userEvent.type(screen.getByLabelText('login'),'mylogin');
    userEvent.type(screen.getByLabelText('password'),'mypassword');
    userEvent.click(screen.getByText('Submit'));
    expect(submitHandle).toHaveBeenCalled();
    expect(submitHandle.mock.calls[0][0]).toEqual('mylogin');
    expect(submitHandle.mock.calls[0][1]).toEqual('mypassword');
  });
});
```

Pour utiliser l'expérience acquise sur les tests, on va :

- utiliser d'une fonction de test asynchrone qui va nous permettre d'utiliser `await`;
- créer un utilisateur avec `userEvent.setup()` et utiliser le clic  (`click`) puis la saisie au clavier (`keyboard`).
- vérifier que la fonction de soumission est appelée une seule fois et les paramètres du dernier appel.

```typescript
// src/components/login-form/login-form.test.tsx

import react from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'

import { LoginForm } from '.';

describe('login-form',()=> {
  it('should call onSubmit on valid path',async () =>{
    const user = userEvent.setup();
    const submitHandle = jest.fn();
    render(
      <LoginForm buttonText="Submit"
      loginLabel="login"
      passwordLabel="password"
      onSubmit={submitHandle}/>);
    await user.click(screen.getByLabelText('login'));
    await user.keyboard('mylogin');
    await user.click(screen.getByLabelText('password'));
    await user.keyboard('mypassword');
    await userEvent.click(screen.getByText('Submit'));
    expect(submitHandle).toHaveBeenCalledTimes(1);
    expect(submitHandle).toHaveBeenLastCalledWith('mylogin','password');
  });
});
```

On lance le test, il réussit.

### Utilisation de useCallback

Le composant est implémenté ainsi :

```typescript
// src/components/login-form/index.tsx
import react, { useState } from 'react';

export interface LoginFormProps{
  buttonText: string;
  loginLabel: string;
  passwordLabel: string;
  onSubmit: (login: string, password:string)=>void;
}

export function LoginForm(props: LoginFormProps):JSX.Element {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const handleLoginChange = (event) => {
    setLogin(event.target.value);
  };
  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };
  const handleButtonClick = (event) => {
    props.onSubmit(login,password);
  };

  return (<>
    <label htmlFor="login-form-login">
      {props.loginLabel}
    </label>
    <input id="login-form-login" 
           value={login}
           onChange={handleLoginChange} />
    <label htmlFor="login-form-password">
      {props.passwordLabel}
    </label>
    <input type="password"
           value={password} 
           id="login-form-password"
           onChange={handlePasswordChange} />
    <button onClick={handleButtonClick}>{props.buttonText}</button>
  </>);
}
```

On va utiliser le hook `useCallback` pour :

- le traitement du changement de login `handleLoginChange`, en indiquant la dépendance à `setLogin`;
- le traitement du changement de mot de passe `handlePasswordChange` en indiquant la dépendance à `setPassword`;
- le clic sur le bouton `handleButtonClick` en indiquant la dépendance à `login` et `password`.

> :warning: L'ajout des dépendances est cruciale pour le bon fonctionnement du composant. Par exemple si on oublie `password` pour la fonction du clic sur le bouton, on va avoir un mot de passe transmis toujours vide car la fonction sera mémorisée au moment où le mot de passe est vide.

A chaque prise en compte d'une fonction, on va lancer le test et s'assurer qu'il réussit.

```typescript
import React, { useCallback, useState } from 'react';

export interface LoginFormProps {
  buttonText: string;
  loginLabel: string;
  passwordLabel: string;
  onSubmit: (login: string, password: string) => void;
}

export function LoginForm(props: LoginFormProps): JSX.Element {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  
  const handleLoginChange = useCallback((event: React.FormEvent<HTMLInputElement>) => {
    setLogin(event.currentTarget.value);
  }, [setLogin]);
  const handlePasswordChange = useCallback((event: React.FormEvent<HTMLInputElement>) => {
    setPassword(event.currentTarget.value);
  },[setPassword]);
  const handleClick = useCallback(() => {
    props.onSubmit(login, password);
  }, [props.onSubmit,login,password])
  return (
    <>
      <label htmlFor="login-form-login">{props.loginLabel}</label>
      <input value={login}
        onChange={handleLoginChange}
        id="login-form-login" />
      <label htmlFor="login-form-password">{props.passwordLabel}</label>
      <input value={password}
        type="password"
        id="login-form-password"
        onChange={handlePasswordChange} />
      <button onClick={handleClick}>{props.buttonText}</button>
    </>
  );
}
```

## Gestion des erreurs

### Erreur d'absence de login

On va commencer par ajouter un test qui vérifie qu'en absence de login quand on clique sur le bouton, une erreur apparaît.

```typescript
// src/components/login-form/login-form.test.tsx
  it('should display loginError when submitting without login',async ()=>{
    const user = userEvent.setup();
    const loginError = 'Login is required';
    const passwordError = 'Password is required';
    const submitHandle = jest.fn();
    render(
      <LoginForm buttonText="Submit"
      loginLabel="login"
      loginError={loginError}
      passwordError={passwordError}
      passwordLabel="password"
      onSubmit={submitHandle}/>);
    await user.click(screen.getByLabelText('password'));
    await user.keyboard('password');
    expect(screen.queryByText(loginError)).not.toBeInTheDocument();
    await user.click(screen.getByText('Submit'));
    expect(submitHandle).toHaveBeenCalledTimes(0);
    expect(screen.getByText(loginError)).toBeInTheDocument();
  });
```

```typescript
// src/components/login-form/index.tsx
import React, { useCallback, useState } from 'react';

export interface LoginFormProps {
  buttonText: string;
  loginLabel: string;
  loginError:string|undefined;
  passwordLabel: string;
  passwordError:string|undefined;
  onSubmit: (login: string, password: string) => void;
}

export function LoginForm(props: LoginFormProps): JSX.Element {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [displayLoginError,setDisplayLoginError] = useState(false);
  
  const handleLoginChange = useCallback((event: React.FormEvent<HTMLInputElement>) => {
    setLogin(event.currentTarget.value);
  }, [setLogin]);
  const handlePasswordChange = useCallback((event: React.FormEvent<HTMLInputElement>) => {
    setPassword(event.currentTarget.value);
  },[setPassword]);
  const handleClick = useCallback(() => {
    if(login === ''){
      setDisplayLoginError(true);
    }else{
      setDisplayLoginError(false);
      props.onSubmit(login, password);
    }
    
  }, [props.onSubmit,login,password,setDisplayLoginError])
  return (
    <>
      <label htmlFor="login-form-login">{props.loginLabel}</label>
      <input value={login}
        onChange={handleLoginChange}
        id="login-form-login" />
      <label htmlFor="login-form-password">{props.passwordLabel}</label>
      {displayLoginError && <span>{props.loginError}</span>}
      <input value={password}
        type="password"
        id="login-form-password"
        onChange={handlePasswordChange} />
      <button onClick={handleClick}>{props.buttonText}</button>
    </>
  );
}
```

### Erreur d'absence de mot de passe

On va commencer par ajouter un test qui vérifie qu'en absence de mot de passe quand on clique sur le bouton, une erreur apparaît.

```typescript
// src/components/login-form/login-form.test.tsx
  it('should display passwordError when submitting without password',async ()=>{
    const user = userEvent.setup();
    const loginError = 'Login is required';
    const passwordError = 'Password is required';
    const submitHandle = jest.fn();
    render(
      <LoginForm buttonText="Submit"
      loginLabel="login"
      loginError={loginError}
      passwordError={passwordError}
      passwordLabel="password"
      onSubmit={submitHandle}/>);
    await user.click(screen.getByLabelText('login'));
    await user.keyboard('login');
    expect(screen.queryByText(passwordError)).not.toBeInTheDocument();
    await user.click(screen.getByText('Submit'));
    expect(submitHandle).toHaveBeenCalledTimes(0);
    expect(screen.getByText(passwordError)).toBeInTheDocument();
  });
```

```typescript
// src/components/login-form/index.tsx
import React, { useCallback, useState } from 'react';

export interface LoginFormProps {
  buttonText: string;
  loginLabel: string;
  loginError:string|undefined;
  passwordLabel: string;
  passwordError:string|undefined;
  onSubmit: (login: string, password: string) => void;
}

export function LoginForm(props: LoginFormProps): JSX.Element {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [displayLoginError,setDisplayLoginError] = useState(false);
  const [displayPasswordError,setDisplayPasswordError] = useState(false);
  
  const handleLoginChange = useCallback((event: React.FormEvent<HTMLInputElement>) => {
    setLogin(event.currentTarget.value);
  }, [setLogin]);
  const handlePasswordChange = useCallback((event: React.FormEvent<HTMLInputElement>) => {
    setPassword(event.currentTarget.value);
  },[setPassword]);
  const handleClick = useCallback(() => {
    if(login === ''){
      setDisplayLoginError(true);
    }else{
      setDisplayLoginError(false);
    }
    if(password === ''){
      setDisplayPasswordError(true);
    }
    else{
      setDisplayPasswordError(false);
    }
    if(login !== '' && password !== ''){
      props.onSubmit(login, password);
    }
    
  }, [props.onSubmit,login,password,setDisplayLoginError,setDisplayPasswordError])
  return (
    <>
      <label htmlFor="login-form-login">{props.loginLabel}</label>
      <input value={login}
        onChange={handleLoginChange}
        id="login-form-login" />
      {displayLoginError && <span>{props.loginError}</span>}
      <label htmlFor="login-form-password">{props.passwordLabel}</label>
      <input value={password}
        type="password"
        id="login-form-password"
        onChange={handlePasswordChange} />
      {displayPasswordError && <span>{props.passwordError}</span>}
      <button onClick={handleClick}>{props.buttonText}</button>
    </>
  );
}
```

### Utilisation de useReducer

On regroupe les différents états dans un état géré par une fonction `reducer`.

Le résultat est plus long mais plus lisible sur la logique de fonctionnement du formulaire : le composant émet des évènements qui sont traités par la fonction. Regardez aussi l'utilisation de l'algèbre de type qui permet de gérer très finement les types d'évènements.

```typescript
// src/components/login-form/index.tsx
import React, { useCallback, useReducer } from 'react';

export interface LoginFormProps {
  buttonText: string;
  loginLabel: string;
  loginError: string | undefined;
  passwordLabel: string;
  passwordError: string | undefined;
  onSubmit: (login: string, password: string) => void;
}

type State = {
  login: string,
  password: string,
  displayLoginError: boolean,
  displayPasswordError: boolean
};

type LoginChangedEvent = {
  type: 'login-changed',
  value: string
};

type PasswordChangedEvent = {
  type: 'password-changed',
  value: string
};

type SubmitEvent = {
  type: 'submitted'
};

type Event = LoginChangedEvent | PasswordChangedEvent | SubmitEvent;

const reducer = (previousState: State, event: Event) => {
  switch (event.type) {
    case 'login-changed': {
      return { ...previousState, login: event.value };
    }
    case 'password-changed': {
      return { ...previousState, password: event.value };
    }
    case 'submitted':{
      return {
        ...previousState,
        displayLoginError: previousState.login === '',
        displayPasswordError: previousState.password === ''
      };
    }
  }
}

export function LoginForm(props: LoginFormProps): JSX.Element {
  const [state, dispatch] = useReducer(reducer, { login: '', password: '', displayLoginError: false, displayPasswordError: false });

  const handleLoginChange = useCallback((event: React.FormEvent<HTMLInputElement>) => {
    dispatch({ type: 'login-changed', value: event.currentTarget.value });
  }, [dispatch]);
  const handlePasswordChange = useCallback((event: React.FormEvent<HTMLInputElement>) => {
    dispatch({ type: 'password-changed', value: event.currentTarget.value });
  }, [dispatch]);
  const handleClick = useCallback(() => {
    dispatch({type:'submitted'});
    if (state.login !== '' && state.password !== '') {
      props.onSubmit(state.login, state.password);
    }
  }, [props.onSubmit, state.login, state.password, dispatch])
  return (
    <>
      <label htmlFor="login-form-login">{props.loginLabel}</label>
      <input value={state.login}
        onChange={handleLoginChange}
        id="login-form-login" />
      {state.displayLoginError && <span>{props.loginError}</span>}
      <label htmlFor="login-form-password">{props.passwordLabel}</label>
      <input value={state.password}
        type="password"
        id="login-form-password"
        onChange={handlePasswordChange} />
      {state.displayPasswordError && <span>{props.passwordError}</span>}
      <button onClick={handleClick}>{props.buttonText}</button>
    </>
  );
}
```

## Conclusion

On a vu une mise à jour des tests, l'ajout d'une fonctionnalité et deux refactorings. Le composant n'est pas parfait, mais il a pu illustrer concrètement la méthode TDD.

Si vous avez des questions ou des retours sur la méthode TDD et les tests unitaires, n'hésitez pas à les partager sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
