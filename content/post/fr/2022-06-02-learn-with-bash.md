---
title: "Bash, pipefail et local"
date: "2022-06-02"
draft: false
description: "Un retour d'expérience sur un script bash"
tags: ["outils","bash"]
author: "Bertrand Madet"
---

La semaine dernière, j'ai rencontré un problème dans un script bash. Ce script permettait de calculer la couverture de code dans notre mono dépôt, j'ai corrigé le problème mais je ne comprenais pas le comportement. Cette semaine, j'ai décidé de comprendre la cause de l'erreur.

## Le problème

Le script, comme indiqué plus haut, permet de calculer la couverture de code de notre projet. Le script calcule bien cette couverture mais il ne retourne pas d'erreur si l'un des projets a des tests qui échouent. C'est dommage car ce script est utilisé dans notre intégration continue et il est important que les développeurs aient un retour indiquant que certains tests échouent.
Les lignes problématiques ressemblaient à ça :

```bash
local coverageOfX=$(jest --coverage|magic-commands-to-extract-coverage)
local errorInX=$?
```

L'idée est de lancer les tests et d'extraire la couverture de code dans la variable `coverageOfX` puis de mettre la valeur de retour dans `errorInX`. Cela ne fonctionne pas, qu'il y ait des erreurs dans jest, la valeur de errorInX est toujours 0 car la valeur de retour de la ligne précédente est toujours 0.

Voilà comment nous avons finalement corrigé le problème.

```bash
jest --coverage > coverageOfX.txt
local errorInX=$?
local coverageOfX=$(cat coverageOfX.txt|magic-commands-to-extract-coverage)
```

Cela fonctionne quand il y a des erreurs dans les tests Jest renvoit un nombre non nul et errorInX le capture permettant ensuite de le signaler.

## Explications

Il y a deux raisons pour laquelle ma première tentative ne fonctionnait pas.

### pipefail

La première raison est que si l'option `pipefail` n'est pas positionnée, la valeur de retour d'une succession de commandes liée par des pipes `|` vaut la valeur de retour de la dernière commande. Donc la ligne bash suivante renverra la valeur de retour de `magic-commands-to-extract-coverage`.
```bash 
$(jest --coverage|magic-commands-to-extract-coverage)
```
Beaucoup de développeurs qui utilisent bash indiquent de positionner l'option `pipefail` en mettant ceci en début de script bash.

```bash
set -o pipefail
```

### local

La deuxième raison vient du comportement de la commande intégrée à bash `local`.

>  The return status is 0 unless local is used outside a function, an invalid name is supplied, or name is a readonly variable.

D'après le manuel de bash, la valeur de retour d'une ligne commençant par local retourne 0 sauf si la déclaration de cette variable est impossible. Il est donc important de déclarer une variable locale dans une commande et de lui assigner dans une autre commande.

Ainsi la partie problématique pourrait aussi être réécrite comme ceci (à condition d'avoir l'option `pipefail` activée) :

```bash
local coverageOfX
local errorInX
coverageOfX=$(jest --coverage|magic-commands-to-extract-coverage)
errorInX=$?
```

## Pour se convaincre

Quoi de plus convainquant qu'un script pour voir les différentes valeurs de retours en fonction de l'utilisation de pipefail et local. Le script part du principe que le chemin `/whatever` n'existe pas et regarde comment détecter l'erreur.

```bash
#! /bin/bash

function useOfLocal(){
    local firstLocal=$(ls /whatever 2&>/dev/null | wc -l)
    echo "return value with local and command subsitution: $?"
    local secondLocal
    secondLocal=$(ls /whatever 2&>/dev/null | wc -l)
    echo "return value with local then command subsitution: $?"
}

ls /whatever 2&>/dev/null | wc -l
echo "return value without pipefail: $?"

echo "---------------------------"
echo "useOfLocal without pipefail"
echo "---------------------------"
useOfLocal

set -o pipefail
ls /whatever 2&>/dev/null | wc -l
echo "return value with pipefail : $?"

echo "------------------------"
echo "useOfLocal with pipefail"
echo "------------------------"
useOfLocal
```

Devrait afficher

```
0
return value without pipefail: 0
---------------------------
useOfLocal without pipefail
---------------------------
return value with local and command subsitution: 0
return value with local then command subsitution: 0
0
return value with pipefail : 2
------------------------
useOfLocal with pipefail
------------------------
return value with local and command subsitution: 0
return value with local then command subsitution: 2
```

## Conclusion

Malgré le fait d'avoir perdu un peu de temps sur cette erreur, j'ai pu apprendre deux choses l'utilité de `pipefail` et l'utilisation correcte de `local`. Je pense par ailleurs que j'aurais mettre le script dans une situation où les tests échouaient afin de ... tester le script d'agrégation de la couverture des tests :exploding_head:

Le bash reste un code comme les autres soumis aux erreurs de ces créateurs. Il y a heureusement des ressources précieuses de bonnes pratiques pour les scripts bash : 

- pour les anglophones :gb: on peut citer le [guide de style pour shell de Google (https://google.github.io/styleguide/shellguide.htm)](https://google.github.io/styleguide/shellguide.htm);
- pour les francophones :fr: le GNU Linux Magazine HS102 était consacré à cela.

Cela sera peut-être l'occasion d'un futur billet dans ce blog. Si vous avez des anecdotes ou des conseils sur la rédaction de script bash, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
