---
title: "Quelques bizarreries sur le développement front"
date: "2024-05-15"
draft: false
description: "à l'usage des développeurs front et back" 
author: "Bertrand Madet" 
tags: ["front-end","JavaScript","programmation","REST API",]
---

Avec le développement de mon application front (en vue.js, mais cela ne devrait pas avoir d'influence), j'ai pu découvrir ou redécouvrir des comportements surprenants 😲 lors d'appels à mon API REST. J'avais déjà fait [un billet sur les particularités de consommer des services Web comme une API REST depuis une application web]({{< ref "/post/fr/2022-01-26-web-services-front-end" >}}).

## Rappels

### La cause des particularités

Le logiciel front-end a la particularité de s'exécuter sur les ordinateurs de vos utilisateurs via un navigateur web 🐝 . Comme nos navigateurs sont utilisés pour exécuter le code de tous les sites web que nous visitons, ils sont à la fois très exposés et très importants. Les navigateurs web ont donc des systèmes de sécurité 🔒 poussés.

### Particularités communes

La première particularité est le [mixed content](https://developer.mozilla.org/fr/docs/Web/Security/Mixed_content) ou contenu mixte : si votre application web utilise le protocole HTTPS, les médias et les API REST doivent être servis en HTTPS. Cela s'applique également aux images, aux feuilles de style...

La deuxième particularité est le [CORS - Cross-origin resource sharing](https://developer.mozilla.org/fr/docs/Web/HTTP/CORS). Les CORS servent à empêcher qu'un code malveillant profite de votre identité sur le navigateur pour appeler n'importe quelle API REST et, par exemple, faciliter le hameçonnage 🎣. En conséquence, si votre API REST est servie depuis un FQDN (**F**ully **Q**alified **D**omain **N**ame - `trambi.gitlab.io` pour ce blog) différent de votre application web, l'API REST doit autoriser le FQDN avec une entête HTTP spécifique. Cela s'applique également aux images, aux feuilles de style...

## Le cas des méthodes autres que GET

En général, les API REST utilisent les méthodes HTTP pour indiquer l'action à effectuer sur la ressource indiquée par l'URL : 

- `GET` pour la lecture ;
- `POST` pour les ajouts ;
- `DELETE` pour les suppressions ;
- `PUT` pour les modifications ;
- `PATCH` pour les modifications partielles.

📚 Plus d'informations sur les [méthodes de requête HTTP ](https://developer.mozilla.org/fr/docs/Web/HTTP/Methods).

Si vous utilisez la méthode `GET`, vous n'avez qu'à vous préoccuper du contenu mixte et de l'entête CORS vu plus haut. 

Si vous appelez une API REST sur un autre FQDN - vous êtes dans un cas de `cross-origin`. Si vous effectuez un appel réseau via votre programme (typiquement avec `fetch`) avec une autre méthode que `GET` comme `POST`, le navigateur va d'abord envoyer une requête HTTP sur la même URL avec la méthode `OPTIONS`. Si l'API REST ne répond pas à ce premier appel avec la méthode `OPTIONS`, une erreur sera levée et le navigateur n’appellera pas l'URL avec la méthode `POST` 😢. Pour que cela fonctionne, il faut répondre  à l'appel avec la méthode `OPTIONS` et la réponse doit contenir l'entête `Access-Control-Allow-Methods` avec la méthode "cible". Dans notre exemple :

```
Access-Control-Allow-Methods: POST, OPTIONS
```

## Les boutons Submit et Firefox 

Avec Firefox 🦊, si vous essayez d'utiliser un bouton de type `submit` pour déclencher un appel réseau avec votre application, le déclenchement de l’événement `onClick` sans empêcher la propagation de l'événement fera échouer l'appel réseau avec une erreur `NetworkError when attempting to fetch resource.`. En regardant dans l'outil de développement réseau, vous verrez l'appel à l'URL avec la bonne méthode en échec avec la colonne `Transfert` à la valeur `NS_BINDING_ABORTED`.

Avec Edge, l'appel réseau fonctionne mais cela recharge votre page.

En pratique, cela signifie qu'il faut empêcher la propagation de l'événement avec [preventDefault](https://developer.mozilla.org/fr/docs/Web/API/Event/preventDefault) :

```javascript
event.preventDefault();
```

En vue.js, cela signifie qu'il faut utiliser le préfixe `.prevent` au gestionnaire d'événement `@click`.

## Conclusion

Même si ces problèmes semblent très orientés application web, ils concernent les serveurs fournissant les données. Ils ne sont donc pas l'apanage des "développeurs front-end". Les personnes en charge de service utilisé par des applications web doivent aussi avoir une connaissance des mécanismes de protection des navigateurs. C'est aussi pour cela que j'apprécie de faire développement "front" **et** du développement "back".

Heureusement, tout cela est très bien expliqué sur le site Mozilla Developer Network, la partie sur [les [CORS déjà mentionnée plus haut est très intéressante à lire](https://developer.mozilla.org/fr/docs/Web/HTTP/CORS).

Si vous avez des explications ou des conseils sur des spécificités d'un domaine de développement particulier, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
