---
title: "Mermaid.js avec Mkdocs"
date: "2020-12-16"
draft: false
description: "Quand la recherche d'un plugin se transforme en aventure..."
tags: ["documentation","mermaid","mkdocs","python"]
author: "Bertrand Madet"
---

## Mkdocs ?

[MkDocs](https://www.mkdocs.org) est un générateur de site statique orienté documentation écrit en Python. Il est plus simple que Hugo mais moins rapide. Il convertit un ensemble de fichiers Markdown en un ensemble de fichiers html pour fournir un site web avec des menus, une mise en page cohérente et surtout un moteur de recherche JavaScript efficace ([lunr.js](https://lunrjs.com/)).

MkDocs nous sert à générer notre site interne de capitalisation, malheureusement MkDocs n'intègre pas Mermaid.js. Toujours convaincu de l'utilité de Mermaid.js, j'ai décidé de l'installer. Cela n'a pas été simple :sweat:.

## Principe

MkDocs encapsule le code mermaid dans une balise `pre` suivie d'une balise `code`. Pour que Mermaid.js puisse faire son office, il faut que le code mermaid soit dans une balise `div` avec l'attribut `class="mermaid"`. 

Les plugins (je vais rester sur plugin :gb: plutôt que greffon :fr:) de MkDocs permettent de modifier les pages générées par MkDocs.

### Plugins mkdocs-mermaid-plugin ou mkdocs-plugin-mermaid

Après une recherche, on trouve deux plugins `mkdocs-mermaid-plugin` et `mkdocs-plugin-mermaid`. Malheureusement aucun ne fonctionne avec MkDocs v1.1.2 et Python 3.

A noter que les deux nécessitent d'installer le paquet `bs4`.

`mkdocs-mermaid-plugin` ne fonctionne pas avec Python 3 - à cause la fonction `unicode` qui doit être remplacée par `str`.

`mkdocs-plugin-mermaid` fonctionne avec Python 3 mais ne sélectionne pas les balises de code mermaid.

Bref aucun plugin idoine ne fonctionne :cry: !

### Plugin mkdocs-plugin-simple

A défaut d'utiliser un plugin spécifique, j'ai décidé d'utiliser un plugin qui permet d'utiliser une fonction Python sur un évènement : `mkdocs-simple-hooks`. En analysant les plugins dédiées, on va déclencher notre fonction sur l'évènement `on_post_page`. On va ensuite ajuster la fonction des plugins imparfaits pour arriver au résultat (à savoir le code mermaid dans une balise `div`).

Le fichier `python/mermaidhook.py` :
```python
from bs4 import BeautifulSoup

def replace_pre_code_div_mermaid(output_content, config, **kwargs):
  """Hook to transform <pre><code class =language-mermaid> to <div class=mermaid>
  
  """
  soup = BeautifulSoup(output_content, 'html.parser')
  mermaids = soup.find_all("code",class_="language-mermaid")
  hasMermaid = 0
  for mermaid in mermaids:
      hasMermaid = 1
      # replace code with div
      mermaid.name="div"
      mermaid['class']="mermaid"
      # suppress <pre> 
      mermaid.parent.replace_with(mermaid)

  if(hasMermaid==1):
      new_tag = soup.new_tag("script")
      new_tag.string="mermaid.initialize();"
      soup.body.append(new_tag)
      
  return str(soup)
```

Le fichier de configuration `mkdocs.yml` :
```yaml
plugins:
  - search
  - mkdocs-simple-hooks:
      hooks:
        on_post_page: "python.mermaidhook:replace_pre_code_div_mermaid"

extra_javascript:
  - javascripts/mermaid.min.js
```

### Et c'est tout

Je vous ai raconté la version synthétique, j'ai pas mal tâtonné pour arriver au résultat. Il y a déjà une demande de merge sur le dépôt : [#7:mkdocs-mermaid-plugin](https://github.com/pugong/mkdocs-mermaid-plugin/pull/7).

Au moment de créer une demande de merge sur le dépôt de mkdocs-plugin-mermaid, je trouve [mkdocs-mermaid2-plugin](https://github.com/fralau/mkdocs-mermaid2-plugin) :cry: , la magie des recherches en changeant un mot ou l'ordre des mots. Je testerai ce plugin prochainement, il existe sur Pypi et il a l'air maintenu.

Si vous aussi, vous avez des vies d'aventurier du logiciel libre, partage-le avec [moi sur Twitter](https://twitter.com/trambi_78).

