---
title: "Ce que j'ai appris en cassant un dépôt Git"
date: "2021-04-04"
draft: false
description: "c'était un accident"
tags: ["git","npm","nodejs"]
author: "Bertrand Madet"
---

Cette semaine, je me suis aperçu que j'avais cassé un des dépôts Git (projet TypeScript utilisant node.js) sur ma machine de développement Windows (et aussi sur les machines de développement Windows de mes collègues :scream:). Mon début de la semaine a donc été consacré à la réparation de l'espace de développement. On pourrait voir cela comme du temps perdu mais cela m'a permis d'apprendre des choses sur Git et sur node.js que je vais essayer de décrire dans ce billet.

## git bisect

Je connaissais la commande pour avoir lu le [manuel de git - Pro Git](https://git-scm.com/book/fr/v2) et je n'avais eu l'occasion de l'utiliser. Le but de `git bisect` ou de la bissection par git est de trouver le commit qui a causé un changement de comportement (en général un bogue). Le principe est assez simple : vous lui indiquez le commit où vous êtes sûr qu'il n'y avait pas le comportement. Ensuite git parcourra l'arbre des commits et vous lui indiquez si le comportement est présent ou non. Rien de magique, il fait un dichotomie, c'est à vous de lui indiquer si le comportement est présent ou non.

Vous pouvez lui donner un exécutable qui retourne zéro, si le comportement est absent et qui retourne un si le comportement est présent, et dans ce cas, git va faire le travail à votre place. La [documentation de la commande (https://git-scm.com/docs/git-bisect/fr)](https://git-scm.com/docs/git-bisect/fr) est bien faite.

Cela m'a permis de trouver le problème enfin la deuxième fois car j'ai fait une erreur pour vérifier la présence du bogue.

## npm et package-lock.json

Nous utilisons le gestionnaire de paquet `npm` (il s'agit du gestionnaire de paquet intégré à node.js). Pour partir sur une installation propre, à chaque commit proposé par git pendant le "bisect", je décide de :

1. supprimer le répertoire `node_modules` ;
1. supprimer le ficher `package-lock.json` ;
1. exécuter `npm install`;
1. lancer mon test pour savoir si mon problème est là.

Je déroule tous les commits proposés pendant la bissection de git et je n'ai que des erreurs et du coup le premier commit introduisant le problème est le premier commit après mon point de départ. Cela m'intrigue car il n'y a pas de changement majeur dans la configuration du projet. Je décide refaire la même opération pour le point de départ où je suis sûr que cela fonctionnait. Cela échoue ! :cry:

Après lecture de la [documentation de npm sur package-lock.json (https://docs.npmjs.com/cli/v7/configuring-npm/package-lock-json)](https://docs.npmjs.com/cli/v7/configuring-npm/package-lock-json), il s'avère que c'est une très mauvaise idée de supprimer le fichier `package-lock.json`. En effet, ce fichier permet de garantir que vous allez installer exactement les mêmes paquets que la CI et vos collègues (incluant le premier d'entre eux, vous d'une semaine auparavant).

:warning: **Le fichier "package-lock.json" est aussi important que le "package.json" !**

D'accord mais que fait-on si Git découvre un conflit sur ce fichier. Une fois que le fichier de description `package.json` est correct, vous pouvez utiliser l'option `--package-lock-only` de npm install. Cela va mettre à jour le fichier `package-lock.json`.

Donc voici la séquence que j'ai finalement exécuté :

1. supprimer le répertoire `node_modules` ;
1. si le fichier `package.json` a changé sans le fichier `package-lock.json`, exécuter `npm install --package-lock.json`;
1. exécuter `npm install`;
1. lancer mon test pour savoir si mon problème est là.

## .gitattributes et les fichiers binaires

J'ai donc trouvé que j'avais modifié un fichier binaire Windows lors d'une de mes sessions de développement sur Linux. Je ne l'ai évidemment pas fait à dessein, c'était la première que je développais sur ma machine virtuelle Linux pour ce dépôt. Il y avait un [fichier `.gitattributes` (https://www.git-scm.com/docs/gitattributes)](https://www.git-scm.com/docs/gitattributes) qui contenait :

```git
* text=auto eol=lf
```

J'ai l'impression l'utilisation sous Linux a fait que git a considéré les fichiers binaires comme des fichiers texte et a modifié les sauts de ligne et a corrompu mon fichier. Je me souviens avoir essayé de le remettre dans son état d'avant modification mais cela échouait à chaque fois. J'avais donc poussé la modification sans comprendre que cela m'embêterait la semaine suivante.

J'ai finalement corrigé le problème en modifiant le fichier `.gitattributes` comme il suit :

```git
* text=auto eol=lf
*.node -text
```

Même si l'ajout de fichiers binaires dans un git n'est pas optimal, cela peut arriver avec les fichiers images par exemple. Après réflexion, j'aurais pu être plus défensif et le modifier pour avoir quelque chose comme ci-dessous :

```git
* text=auto
*.png -text
*.node -text
*.tsx text eol=lf
*.ts text eol=lf
```

## Conclusion

Voilà comment développer pour la première fois sur un dépôt avec une machine virtuelle Linux peut vous casser votre espace de développement Windows. Si vous voulez partager des galères de développement et ce que vous en avez appris, partagez les à moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
