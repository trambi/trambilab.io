---
title: "Création d'une API REST factice"
date: "2022-06-30"
draft: false
description: "utilité et mécanique"
tags: ["REST API","Software Craftmanship"]
author: "Bertrand Madet"
---

Après le [billet sur les tests des API REST]({{< ref "/post/fr/2022-06-22-test-rest-api.md" >}}), étudions cette fois les API [REST (https://fr.wikipedia.org/wiki/Representational_state_transfer)](https://fr.wikipedia.org/wiki/Representational_state_transfer) factices. Pourquoi les implémenter dans une première partie et ensuite comment (en python et en node.js).

## Pourquoi créer des API factices ?

Imaginons que vous deviez programmer une application front-end qui doit utiliser une API REST et que cette API REST ne soit pas encore prête. Afin d'avancer sur l'application front-end, cela serait bien d'avoir une API REST qui se comporte avec la même interface.

Imaginons maintenant que vous devez vous assurer du bon fonctionnement d'un logiciel qui utilise une API REST. Pour s'assurer le bon fonctionnement de ce logiciel quoi de mieux qu'un test automatique. Le mieux serait d'avoir une API REST le plus facile possible à déployer sur les infrastructures. Une API REST qui se comporte comme l'API REST cible mais sans les dépendances (bases de données, accès réseaux...) de cette API.

Voilà deux raisons de créer des API factices. Pour synthétiser, cela permet d'utiliser le découplage apporté par les API REST.

## Comment l'implémenter une API factice

Le mieux est d'utiliser le même langage que l'implémentation réelle. En fonction du moment de développement de cette API factice, cela permet soit de s'entraîner pour l'implémentation soit de capitaliser l'expérience de l'implémentation.

### Python

La bibliothèque [FastAPI (https://fastapi.tiangolo.com)](https://fastapi.tiangolo.com) facilite la création des API REST en python.

Voici un exemple tiré de la documentation du site officiel.

```python
from fastapi import FastAPI

app = fastapi.FastAPI()

@app.get('/')
async def root():
  return {"message": "Hello world"}
```

Pour envoyer une valeur fixe pour un endpoint, cela est très rapide. On peut citer aussi [Flask (https://flask.palletsprojects.com/en/2.1.x/)](https://flask.palletsprojects.com/en/2.1.x/)

### Node.js

Côté node.js, la bibliothèque [Express (https://expressjs.com)](https://expressjs.com) permet aussi de créer des API REST.

```javascript
const express = require('express')
const app = express()
const port = 3000

app.get('/', (req,res)=>{
  res.send('Hello world!')
})

app.listen(port, ()=> {
  console.log(`Example app listening on port ${port}`)
})
```

De la même façon que FastAPI, ce code (issu de la documentation) montre la simplicité d'implémentation d'API factice.

## Approfondir

### Complexité

On peut imaginer plusieurs niveaux de sophistication :

- retour du statut fixe en fonction des endpoints ;
- retour de statut et contenu fixes en fonction des endpoints ;
- retour de statut et contenu spécifique en fonction des requêtes d'appel des endpoints ;
- implémentation de l'API avec une base de données en mémoire (au lieu d'une base de données persistantes).

La sélection du niveau de sophistication doit se faire en fonction du besoin et de la complexité de la API réelle.

### Tests

Pour garder la confiance dans cette API factice, il est important de tester cette API (avec les éléments présentés dans mon [billet précédent sur les tests des API REST]({{< ref "/post/fr/2022-06-22-test-rest-api.md" >}})). Cela représente du travail mais cela peut aider à définir des tests pour l'API.

Les tests les plus simples peuvent même être commun entre l'API factice et l'API réelle afin de garder la cohérence dans l'interface.

## Conclusion

Les API REST factices pérennisent le développement des éléments dépendants des API REST dont elles sont les copies. Il me semble qu'à Google (basé sur la [lecture de Software Engineering at Google]({{< ref "/post/fr/2021-05-02-software-engineering-at-google-lecture.md" >}})), c'est une responsabilité (et un devoir) des mainteneurs d'une API de fournir une implémentation factice.

Si vous avez des expériences ou des questions sur les API REST, n'hésitez pas à les partager avec moi sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).
