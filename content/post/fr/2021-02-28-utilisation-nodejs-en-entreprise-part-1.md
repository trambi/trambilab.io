---
title: "Utilisation de Node.js en entreprise - partie 1"
date: "2021-02-28"
draft: false
description: "ou comment utiliser des certificats non standards"
tags: ["nodejs","entreprise","certificat","typescript"]
author: "Bertrand Madet"
---

Je ne sais pas pour vous, mais dans mon entreprise, nous ne sommes pas directement sur Internet, nous travaillons dans le réseau d'entreprise (aussi appelé *SI* pour **S**ystème d'**i**nformation). C'est ce SI qui nous permet d'avoir accès à Internet.

```mermaid
graph LR

dev(PC de développement) --> proxy[passerelle]
proxy --> internet
subgraph SI
  dev
  proxy
end
```

Forcément, au sein d'un SI d'entreprise, il y a des contraintes et des particularités. Par exemple, comment faire pour que Node.js accepte une connexion https avec des certificats signés par des autorités de certification internes ?

## Fonctionnement par défaut

Par défaut, Node.js  accepte les connexions https si les certificats sont valides (bien formés, dans leur période de validité et pas révoqués) et signés par une des autorités de certification "bien connues". La liste des autorités de certification "bien connues" est fournie par Mozilla (cf. [La documentation de la fonction createSecureContext du module tls de node.js](https://nodejs.org/api/tls.html#tls_tls_createsecurecontext_options)).

## Modification du comportement au coup par coup

Après une recherche sur Stack Overflow, il apparaît qu'il est possible de fournir une liste d'autorités de certification alternatives. Il faut créer un `httpsAgent` le transmettre à la requête.

```javacript
const https = require('https');
const axios = require('axios');
const fs = require('fs');
const ca = fs.readFileSync('./customCa.pem');

const httpsAgent = new https.Agent({ca});
axios.get('https://api.myinternalit/something',{httpsAgent});
// ...
```

Cela fonctionne au coup par coup et surtout il faut que la bibliothèque accepte de ces options - ce n'est pas le cas pour les modules de chargement de [Gatsby (https://www.gatsbyjs.com/)](https://www.gatsbyjs.com/) par exemple :cry:.

## Modification du comportement au lancement de node.js

Il faudrait pouvoir fixer cette liste de certificats d'autorité. Je cherche cet élément (mal on le verra par la suite :disappointed:) et je ne trouve pas de réponse. Je profite donc du fait Node.js soit un logiciel libre pour aller regarder le code source (https://github.com/nodejs/node).
On part de la fonction `createSecureContext` du module tls de l’API (fichier `node/lib/tls.js` puis `node/lib/_tls_common.js`). On voit qu’une fonction `addRootCerts` est appelée. Les autorités de certification par défaut sont inclus en durs dans le code `node/src/node_root_certs.h`.

La fonction `addRootCerts` (`node/src/crypto/crypto_context.cc`) appelle la fonction `NewRootCertStore`. Cette fonction est aussi utilisée dans la fonction `UseExtraCaCerts` (dans le même fichier) semble suggérer qu’il est possible d’ajouter un fichier de certificat aux certificats d’autorité. 
Une recherche sur cette fonction remonte dans la fonction principale de node.js qui associe cette fonction à la variable d'environnement `NODE_EXTRA_CA_CERTS`.

Après une recherche sur cette variable d'environnement, je trouve l'information dans la commande d'aide de node. C'est aussi pour ça que j'aime les logiciels libres, on rate une information dans la documentation on peut tenter sa chance dans les sources.

En fixant cette variable d'environnement sur le chemin qui contient la concaténation des certificats d'autorité (au format PEM), cela fonctionne.

## Et pour le navigateur ou Deno

Pour le navigateur, il est possible d'ajouter des autorités de certification, la balle est donc dans les mains de l'utilisateur. Pour [Deno (https://deno.land/)](https://deno.land/), c'est la variable `DENO_CERT` qui a l'effet de `NODE_EXTRA_CA_CERTS`.

## Conclusion

J'espère que ce partage d'expériences vous aideront à utiliser TypeScript (et JavaScript) dans un réseau d'entreprise. Si vous avez des retours ou d'autres conseils, n'hésitez pas à les partager sur [Twitter](https://twitter.com/trambi_78) ou sur [LinkedIn](https://www.linkedin.com/in/bertrand-madet-043474169/).