
Welcome to the back office of of my [personal blog (trambi.gitlab.io)](https://trambi.gitlab.io). 

## Tools

It uses [Hugo (https://gohugo.io)](https://gohugo.io) - v0.136.5 - and [Ghostwriter theme (https://github.com/jbub/ghostwriter)](https://github.com/jbub/ghostwriter).

I have added : 

- mermaid render codeblock rendered with [Mermaid.js (https://mermaid-js.github.io/mermaid/#/)](https://mermaid-js.github.io/mermaid/#/) as explained in [Mermaid.js avec Hugo](./content/post/fr/2024-12-05-mermaid-js-hugo-part2.md) ;
- math shortcode with [KaTeX (https://katex.org/)](https://katex.org/) as explained in [2 outils utiles pour un blog](content/post/fr/2021-01-07-making-of-orthodromic-route.md).
- video shortcode.

## How to

### Create a new content

Run the command `hugo new content post/fr/YYYY-MM-DD-title.md` to create a classic content.

Run the command `hugo new content --kind lecture post/fr/YYYY-MM-DD-title-lecture.md` to create a lecture content.

### Use a mermaid diagram

Just use a mermaid code block ` ```mermaid `.

### Use a KaTex formula

Just use a `{{< math >}}` opening tag followed by our math formula and closed by the tag {{< /math >}}.

⚠️ It seems not to be the best way to integrate math formulas in Hugo [Mathematics in Markdown](https://gohugo.io/content-management/mathematics/).

### Use a video

Just use a video specific tag `{{< video width=640 height=480 src="my.mp4" >}}` where my.mp4 is the path to file to display.
