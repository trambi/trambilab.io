---
title = '{{ replace .File.ContentBaseName  "-" " "| replaceRE `\d{4} \d{2} \d{2} ` "" | replaceRE ` lecture` "" | title }} - Fiche de lecture'
date: '{{ index (findRE `\d{4}-\d{2}-\d{2}` .File.ContentBaseName) 0 }}'
draft: true
description: "Livre de X sur Y"
tags: ["fiche de lecture"]
author: 'Bertrand Madet'
---

## A qui s'adresse ce livre

*A compléter*

## Ce qu'il faut retenir

*A compléter*

- Premier chose à retenir
- Deuxième chose à retenir
- Troisième chose à retenir

## Mon opinion

| Points positifs | Points négatifs |
|-----------------|-----------------|
| :+1: Point positif 1| :-1: Point négatif 1|
| :+1: Point positif 2| :-1: Point négatif 2|
| :+1: Point positif 3| :-1: Point négatif 3|

*A compléter*

## Détails

- Titre : *Titre*
- Auteur : *Auteur*
- Langue : 🇬🇧 🇫🇷
- ISBN : XXX-XXXXXXXXXX